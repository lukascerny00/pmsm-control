function plugin_gm_config(gmModelName, ~)
% Create library block with AXI interface to programmable logic

% Copyright 2014-2016 The MathWorks, Inc.

    %% Set work folder
    %WorkFolder = evalin('base', 'paths.WorkFolder_full');
    %WorkFolder = [pwd, '/work'];
    
    
    %% Identify source model name
    % Strip off the "gm_" and "_interface" from the targetModelName
    modelName = gmModelName(4:strfind(gmModelName,'_interface')-1);

    callbackNameDisplayString = ['<a href="matlab:edit(''' mfilename('fullpath') ''')">'...
                                                           mfilename '</a>'];
    modelNameDisplayString    = ['<a href="matlab:'        modelName '">'...
                                                           modelName '</a>'];
    disp(['Executing custom callback ' callbackNameDisplayString ' for model ' modelNameDisplayString '.']);

    
    %% Specify library attributes
    %destLibraryFolder = WorkFolder;
    destLibraryModel  = [modelName '_AXI_Interface_Lib'];
    libraryBlockName  = 'AXI_Interface';
    
    
    %% Identify path to source AXI interface block
    sourceFpgaInterfaceBlock = find_system(gmModelName,...
        'RegExp', 'on',...
        'MaskDisplay', 'Xilinx Zynq.*Interface' ...
        );
    if isempty(sourceFpgaInterfaceBlock)
        warning('Could not generate interface library block')
        return
    end
    if length(sourceFpgaInterfaceBlock) > 1
        warning('Found more than one interface block. Selecting the first one');
    end
    sourceFpgaInterfaceBlock = sourceFpgaInterfaceBlock{1};
    
    
    %% Identify source system
    % If name of source interface block is the same as the model, use model
    % name, else determine path to subsystem
    sourceHdlBlockName = get_param(sourceFpgaInterfaceBlock,'Name');
    if strcmp(modelName,sourceHdlBlockName)
        sourceHdlSystem = modelName;
    else
        sourceHdlSystem = strrep(sourceFpgaInterfaceBlock, gmModelName, modelName);
    end

    sourceSystemFullName = sourceFpgaInterfaceBlock;
    
    
    %% Delete library file if it already exists
    if bdIsLoaded(destLibraryModel)
        close_system(destLibraryModel,false)
    end
    
    %destLibraryFullFile = fullfile(destLibraryFolder,[destLibraryModel '.slx']);
    if exist(destLibraryModel,'file') 
        delete(destLibraryModel);
    end
    
    
    %% Create new library model and block
    new_system(destLibraryModel,'Library');
    open_system(destLibraryModel);
    set_param(destLibraryModel,'LibraryLinkDisplay','user');

    libraryBlockFullName = [destLibraryModel '/' libraryBlockName];
    add_block(sourceSystemFullName, [destLibraryModel '/' libraryBlockName]);    
    
    set_param(libraryBlockFullName,'ForegroundColor','black');
    set_param(libraryBlockFullName,'BackgroundColor','white')
    
    
    %% Remove sample times from block and input and output ports
    set_param(libraryBlockFullName,'SystemSampleTime', '-1');
    inportBlockNames = find_system(libraryBlockFullName,...
        'LookUnderMasks','all',...
        'SearchDepth',2,...
        'BlockType', 'Inport');
    outportBlockNames = find_system(libraryBlockFullName,...
        'LookUnderMasks','all',...
        'SearchDepth',2,...
        'BlockType', 'Outport');
%     axiBlockNames = find_system(libraryBlockFullName,...
%         'LookUnderMasks','all',...
%         'RegExp','on',...
%         'BlockType', 'SubSystem',...
%         'Name', 'AXI4Lite_Read_*');
    blockNames = [inportBlockNames; outportBlockNames]; % axiBlockNames];
    for index = 1:length(blockNames)
        set_param(blockNames{index},...
            'SampleTime', '-1');
    end

    %% Remove Inports which are connected to Terminator blocks
    blocks = find_system(libraryBlockFullName,...
        'FindAll', 'on',...
        'LookUnderMasks','all',...
        'BlockType','Terminator');
    for iBlock = 1:length(blocks)
        block   = blocks(iBlock);
        lines   = get_param(block,'LineHandles');
        systemPortHandle = get_param(lines.Inport,'SrcBlockHandle');
        
        delete_block(block);
        delete(lines.Inport);
        delete_block(systemPortHandle);
    end
    
    %% Remove Outports which are connected to Ground blocks
    blocks = find_system(libraryBlockFullName,...
        'FindAll', 'on',...
        'LookUnderMasks','all',...
        'BlockType','Ground');
    for iBlock = 1:length(blocks)
        block   = blocks(iBlock);
        lines   = get_param(block,'LineHandles');
        nextBlockHandle = get_param(lines.Outport,'DstBlockHandle');
        
        if strcmp(get_param(nextBlockHandle,'BlockType'),'DataTypeConversion')
            dataTypeLines = get_param(nextBlockHandle,'LineHandles');
            systemPortHandle = get_param(dataTypeLines.Outport,'DstBlockHandle');
            systemPortNumber = str2double(get_param(systemPortHandle,'Port'));
            systemParent = get_param(systemPortHandle,'Parent');
            
            systemPortStructs = get_param(systemParent,'PortConnectivity');
            systemPort = systemPortStructs(systemPortNumber);
            %topLevelPort = systemPort.DstBlock; % Not present in MATLAB 2019b

            %topLevelLines = get_param(topLevelPort,'LineHandles'); % Not present in MATLAB 2019b
            
            delete_block(block);
            delete(lines.Outport);
            delete(nextBlockHandle);
            delete(dataTypeLines.Outport);
            delete_block(systemPortHandle);
            %delete(topLevelLines.Inport); % Not present in MATLAB 2019b
            %delete_block(topLevelPort); % Not present in MATLAB 2019b
        end
    end
    
   
    %% Update custom code block to initialize the axi_mc_current_monitor IP core
    hBlock = get_param([destLibraryModel '/AXI_Interface/AXI4Reset'], 'Handle');
    set_param(hBlock, 'Position', [210    91   294    156]);
    
    
    %% Create title annotation
    libraryTitle = [strrep(libraryBlockName,'_',' ') ' Library'];
    blockPositionTop = 20;
    blockPositionCenter = 200;

    crLf = sprintf('\n');
    numCrLf = length(strfind(libraryTitle,crLf));
    
    blockPositionTop = blockPositionTop * (1-numCrLf);
    
    annotationHandle = add_block('built-in/Note', [destLibraryModel '/' libraryTitle],...
         'FontSize', 20,...
         'FontWeight', 'Bold',...
         'VerticalAlignment', 'Top',...
         'HorizontalAlignment', 'Center');
    blockPosition = get_param(annotationHandle,'Position');
    blockPosition = [blockPosition(1) + blockPositionCenter/2,...  % Left
                     blockPosition(2) + blockPositionTop,...     % Top
                     blockPosition(3) + blockPositionCenter/2, ... % Right
                     blockPosition(4) + blockPositionTop];       % Bottom
    set_param(annotationHandle,'Position',blockPosition);

    %% Create date annotation
    blockPositionTop = 60;
    annotationText = ['Library created on ' datestr(now) ];
    annotationHandle = add_block('built-in/Note', [destLibraryModel '/' annotationText],...
         'FontSize', 10,...
         'VerticalAlignment', 'Top',...
         'HorizontalAlignment', 'Center');
    blockPosition = get_param(annotationHandle,'Position');
    blockPosition = [blockPosition(1) + blockPositionCenter/2,... % Left
                     blockPosition(2) + blockPositionTop,... % Top
                     blockPosition(3) + blockPositionCenter/2, ... % Right
                     blockPosition(4) + blockPositionTop]; % Bottom
    set_param(annotationHandle,'Position',blockPosition);
    
    %% Resize library block
    distanceBetweenPorts = 35;
    nInports  = size(find_system(libraryBlockFullName,...
                    'LookUnderMasks','all',...
                    'SearchDepth',1,...
                    'BlockType','Inport'),1);
    nOutports = size(find_system(libraryBlockFullName,...
                    'LookUnderMasks','all',...
                    'SearchDepth',1,...
                    'BlockType','Outport'),1);
    maxPorts = max([nInports nOutports]);
    libraryBlockHeight = (maxPorts + 2) * distanceBetweenPorts;    
    libraryBlockWidth = 300;
    blockPositionTop = blockPositionTop + 40;
    blockPosition = [blockPositionCenter - libraryBlockWidth/2, ... % Left
                     blockPositionTop,     ... %Top
                     blockPositionCenter + libraryBlockWidth/2, ... %Right
                     blockPositionTop    + libraryBlockHeight]; %Bottom
    set_param(libraryBlockFullName,'Position',blockPosition);
    set_param(destLibraryModel,'ZoomFactor','Fit')
    set_param(libraryBlockFullName,'MaskDisplay','');  
    
    %% Rearrange blocks inside the library block
    Simulink.BlockDiagram.arrangeSystem([destLibraryModel, '/', libraryBlockName]);
    
    %% Add buttons to mask dialog
    maskObj = get_param(libraryBlockFullName,'MaskObject');

    maskObj.addDialogControl('pushbutton','buttonOpenSourceModel');
    maskObj.getDialogControl('buttonOpenSourceModel').Prompt = 'Open source model';
    maskObj.getDialogControl('buttonOpenSourceModel').Row = 'new';
    maskObj.getDialogControl('buttonOpenSourceModel').Callback = [...
        'load_system(''' modelName  ''');...' crLf ...
        'open_system(''' sourceHdlSystem ''')'];

    maskObj.addDialogControl('pushbutton','buttonDownloadBitstream');
    maskObj.getDialogControl('buttonDownloadBitstream').Prompt = 'Download bitstream';
    maskObj.getDialogControl('buttonDownloadBitstream').Row = 'new';
    maskObj.getDialogControl('buttonDownloadBitstream').Callback = [...
        't6_DownloadBitstream;'];
    
    %% Save model
    save_system(destLibraryModel);
    
    %% Bring model to front
    open_system(destLibraryModel)

end
