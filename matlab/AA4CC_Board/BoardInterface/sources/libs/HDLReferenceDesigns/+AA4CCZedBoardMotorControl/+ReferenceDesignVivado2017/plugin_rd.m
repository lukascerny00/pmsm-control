function hRD = plugin_rd()
% Reference design definition

%   Copyright 2014-2017 The MathWorks, Inc.

% Construct reference design object
hRD = hdlcoder.ReferenceDesign('SynthesisTool', 'Xilinx Vivado');

hRD.ReferenceDesignName = 'AD-FMCMOTCON2-EBZ PMSM Driver Interface Vivado2017';
hRD.BoardName = 'ZedBoard and AA4CC Drivers for PMSM Control';

% Tool information
hRD.SupportedToolVersion = {'2017.2', '2017.4'};


%% Add custom design files
% add custom Vivado design
hRD.addCustomVivadoDesign( ...
    'CustomBlockDesignTcl', 'system_top.tcl', ...
    'VivadoBoardPart',      'em.avnet.com:zed:part0:1.1');


%% Add interfaces
% add clock interface
hRD.addClockInterface( ...
    'ClockConnection',   'clk_wiz_0/clk_out1', ...
    'ResetConnection',   'proc_sys_reset_0/peripheral_aresetn');

% add AXI4 and AXI4-Lite slave interfaces
hRD.addAXI4SlaveInterface( ...
    'InterfaceConnection', 'axi_interconnect_0/M00_AXI', ...
    'BaseAddress',         '0x400D0000', ...
    'MasterAddressSpace',  'processing_system7_0/Data');

% 20 MHz clock signal for measurements
hRD.addInternalIOInterface( ...
    'InterfaceID',    '20 MHz Clock for Measurements', ...
    'InterfaceType',  'IN', ...
    'PortName',       'CLK_20_MHZ', ...
    'PortWidth',      1, ...
    'InterfaceConnection', 'clk_wiz_0/clk_out2', ...
    'IsRequired',     false);

hRD.DeviceTreeName = 'devicetree_axilite.dtb';
