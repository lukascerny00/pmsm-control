%% Init.m
% This script initializes parameters, setups simulink and hdl coder, and
% adds library folders and hdl reference designs to path. You should call
% DeInit.m when you finish working with these models.

%% Clear workspace
clear
close
clc


%% Set paths

% Add sources to path
addpath('sources');

% Add libraries to path
addpath('sources/libs');

% Add hdl reference design to path
addpath('sources/libs/HDLReferenceDesigns');

% Setup hdl tool path to Vivado
hdlsetuptoolpath('ToolName', 'Xilinx Vivado', 'ToolPath','C:\Xilinx\Vivado\2017.4\bin\vivado.bat');
% hdlsetuptoolpath('ToolName', 'Xilinx Vivado', 'ToolPath', '/opt/Xilinx/Vivado/2017.4/bin/vivado');
% hdlsetuptoolpath('ToolName', 'Xilinx Vivado', 'ToolPath', '/tools/Xilinx/Vivado/2019.1/bin/vivado');

% Set paths to the main folder, build folder, and to simulink models
paths.MainFolder = pwd;
paths.BuildFolder = 'build';
paths.FilenamePL = 'PMSM_Control_PL';
paths.FilenamePS = 'PMSM_Control_PS';
paths.FilenamePSTopLevel = 'PMSM_Control_PS_Top_Level';

% Create full path to source files
%paths.BuildFolder_full = fullfile(paths.MainFolder, paths.BuildFolder);
%paths.FilenameHDL_full = fullfile(paths.MainFolder, paths.FilenameHDL);
%paths.FilenameARM_full = fullfile(paths.MainFolder, paths.FilenameARM);
%paths.FilenameTopLevel_full = fullfile(paths.MainFolder, paths.FilenameTopLevel);

% Setup simulink to generate files to build folder
Simulink.fileGenControl('set', 'CacheFolder', paths.BuildFolder, ...
                               'CodeGenFolder', paths.BuildFolder, ...
                               'keepPreviousPath', true, ...
                               'createDir', true);


%% Set sample times (all values are given in seconds)

% Sample times in PL (programmable logic = FPGA)
Ts = struct;
Ts.PL_CLK = 1e-8;         % The main PL clock
Ts.PL_DataLogging = 1e-5; % Sample time of fast data logging via AXI-Stream
Ts.PL_PWM = 4e-5;         % Sample time of PWM, thus, the switching frequency is 25 kHz
Ts.PL_ADC_CLK = 5e-8;     % Clock for sigma-delta sensors

% Sample times in PS (processing system = ARM processor)
Ts.PS_CLK = 1e-3;         % The main PS sample time


%%

