
%% Reset paths

% Remove sources from path
rmpath('sources');

% Remove libraries from path
rmpath('sources/libs');

% Remove hdl reference design from path
rmpath('sources/libs/HDLReferenceDesigns');

% Reset simulink path to original settings
Simulink.fileGenControl('reset', 'keepPreviousPath', true);

% Remove build folder from path
rmpath(paths.BuildFolder);


%% Clear workspace
clear
close
clc

