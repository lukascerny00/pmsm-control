function [Ad, Bd] = CreateDiscreteModel(npp, Rs, Ld, Lq, Kb, J, B, omega, Ts)

% Define continuous-time linear model (simplified linearization)
Ac = [            -Rs/Ld,  (Lq*npp*omega)/Ld,    0, 0;
      -(Ld*npp*omega)/Lq,             -Rs/Lq,    0, 0;
                       0,       (3*Kb)/(2*J), -B/J, 0;
                       0,                  0,    1, 0];

Bc = [ 1/Ld,    0,   0;
          0, 1/Lq,   0;
          0,    0, 1/J;
          0,    0,   0];
 
% Discretize using Euler approximation
Ad = eye(4) + Ts*Ac;
Bd = Ts*Bc;

end

