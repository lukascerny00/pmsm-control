
npp = 2;          % [-]
Rs = 0.45;        % [Ohm]
Ld = 0.0008;      % [H]
Lq = 0.0009;      % [H]
Kb = 0.023;       % [V/(rad/s)]
J = 0.000028;      % [kg*m^2]
B = 0.000013;      % [N*m/rad/s)]
 
s = tf('s');


clc
close all
figure
hold on

poles = [];
zeros = [];
nn = 10;
omegas = -nn:4:nn;
omegas = omegas*1;
%omegas = 200;
input = 2;
output = 3;

for k = 1:length(omegas)
    omega = omegas(k);
    Ac = [            -Rs/Ld,  (Lq*npp*omega)/Ld,    0, 0;
         -(Ld*npp*omega)/Lq,             -Rs/Lq,    0, 0;
                          0,       (3*Kb)/(2*J), -B/J, 0;
                          0,                  0,    1, 0];
    Bc = [ 1/Ld,   0;
             0, 1/Lq;
             0,    0;
             0,    0];
    Cc = eye(4);
%     Ac = [            -Rs/Ld,  (Lq*npp*omega)/Ld,    0;
%           -(Ld*npp*omega)/Lq,             -Rs/Lq,    0;
%                            0,       (3*Kb)/(2*J), -B/J];
%     Bc = [ 1/Ld,    0;
%               0, 1/Lq;
%               0,    0];
%     Cc = eye(3);
    sys = ss(Ac, Bc, Cc, []);
    G = tf(sys);
    G = Cc*(s*eye(4) - Ac)^-1*Bc(:, 1:2);
    step(G(output, input))
    %bode(G(output, input))
    
    [p,z] = pzmap(G);
    poles = [poles; p];
    zeros = [zeros; z];
    
    if rank(obsv(sys)) < 4
        disp('Not observable for omega = ')
        disp(omega)
    end
    if rank(ctrb(sys)) < 4
        disp('Not controllable for omega = ')
        disp(omega)
    end
end

max(real(poles))
max(real(zeros))

figure
hold on
grid on
plot(poles + 1e-9*i, 'ob')
plot(zeros + 1e-9*i, 'xr')


