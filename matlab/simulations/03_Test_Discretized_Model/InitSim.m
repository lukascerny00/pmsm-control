%% Clear workspace
close all
clear all
clc


%% Set Motor Parameters

% Parameters
pmsm1.npp = 2;          % [-]
pmsm1.Rs = 0.45;        % [Ohm]
pmsm1.Ld = 0.0008;      % [H]
pmsm1.Lq = 0.0009;      % [H]
pmsm1.Kb = 0.023;       % [V/(rad/s)]
pmsm1.J = 0.000028;      % [kg*m^2]
pmsm1.B = 0.000013;      % [N*m/rad/s)]

% Initial Conditions
pmsm1.i_d0 = 0;         % [A]
pmsm1.i_q0 = 0;         % [A]
pmsm1.omega0 = 0;       % [rad/s]
pmsm1.theta0 = 0;       % [rad]


%% Set discretization sample time
Ts = 4e-5;              % [s]

