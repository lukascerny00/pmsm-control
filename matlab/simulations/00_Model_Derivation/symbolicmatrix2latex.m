%% Clear command window
clc;


%% Choose symbolic matrix to convert
M = P;


%% Define parameters, variables, and operators and their substitution
S = {};
S = [S; {'*', ' '}];
S = [S; {'sin', '\sin'}];
S = [S; {'cos', '\cos'}];
S = [S; {'theta', '\theta'}];
S = [S; {'thm', '\theta_{\textrm{g}}'}];
S = [S; {'omega', '\omega'}];
S = [S; {'Ls', 'L_{\textrm{s}}'}];
S = [S; {'Ms', 'M_{\textrm{s}}'}];
S = [S; {'Lg', 'L_{\textrm{g}}'}];
S = [S; {'Psim', '\Psi_{\textrm{m}}'}];
S = [S; {'npp', 'n_{\textrm{pp}}'}];


%% Convert
% Get matrix dimensions
m = size(M, 1);
n = size(M, 2);
% Create first line
s = sprintf('    \\begin{bmatrix}\n');
% Add matrix content
for k = 1:m
    for l = 1:n
        s = sprintf('%s        %s', s, char(M(k, l)));
        if l < n
            s = sprintf('%s &\n', s);
        end
    end
    if k < m
        s = sprintf('%s \\cr', s);
    end
    s = sprintf('%s\n', s);
end
% Replace parameters, variables, and operators
for k = 1:size(S, 1)
    s = strrep(s, char(S(k, 1)), char(S(k, 2)));
end
% Add last line
s = sprintf('%s    \\end{bmatrix}\n', s);
% Print the result
disp(s);

