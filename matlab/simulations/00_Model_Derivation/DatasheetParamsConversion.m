

%% Datasheet parameters

% Number of poles
np = 4; % [-]

% Rated speed
omega_r = 418.9; % [rad s^-1]

% Rated torque
tau_r = 0.22; % [N m]

% Line-to-line resistance
Rll = 0.64; % [ohm]

% Line-to-line inductance
Lll = 2.1e-3;  % [H]

% Torque constant
Kt = 0.06;  % [N m /A(rms)]

% Rotor inertia
J = 1.19e-5; % [kg m^2]


%% Star-equivalent PMSM parameters
npp = np/2;
Rs = Rll/2;
L = Lll/2;
Ld = L;
Lq = L;
Kb = 2/3/sqrt(2)*Kt;


%% Get upper bound on the coefficient of viscous friction - very inaccurate method
rated_current = 3.67 + 0.005; % [A(rms)]
rated_torque = 31.2 - 0.05;   % [ozf in]
torque_constant = 8.5 + 0.05; % [ozf in/A(rms)]

% Compute friction torque
electromagnetic_torque = torque_constant*rated_current;
friction_torque = rated_torque - electromagnetic_torque;

% Convert to SI Newton meters
friction_torque_Nm = 0.007061551814*friction_torque;

% Compute B
B = -friction_torque_Nm/omega_r

