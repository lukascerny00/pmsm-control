%% Clear workspace
clear
close
clc


%% Define motor parameters
npp = sym('npp', 'real');
Rs = sym('Rs', 'real');
Ls = sym('Ls', 'real');
Ms = sym('Ms', 'real');
Lg = sym('Lg', 'real');
Psim = sym('Psim', 'real');
J = sym('J', 'real');
B = sym('B', 'real');
Ld = sym('Ld', 'real');
Lq = sym('Lq', 'real');
Lz = sym('Lz', 'real');
Kb = sym('Kb', 'real');

% Star equivalent parameters
Rd_steq = sym('Rd_steq', 'real');
Rq_steq = sym('Rq_steq', 'real');
Rz_steq = sym('Rz_steq', 'real');
Ld_steq = sym('Ld_steq', 'real');
Lq_steq = sym('Lq_steq', 'real');
Lz_steq = sym('Lz_steq', 'real');
Kb_steq = sym('Kb_steq', 'real');


%% Define state variables and inputs
% State variables
ia = sym('ia', 'real');
ib = sym('ib', 'real');
ic = sym('ic', 'real');
omega = sym('omega', 'real');
theta = sym('theta', 'real');
theta2 = sym('theta2', 'real');

% Inputs
va = sym('va', 'real');
vb = sym('vb', 'real');
vc = sym('vc', 'real');
tau_ex = sym('tau_ex', 'real');
tau_cog = sym('tau_cog', 'real');


%% Create vectors i_abc and v_abc
i_abc = [ia; ib; ic];
v_abc = [va; vb; vc];


%% Define theta2
theta2 = sym('theta2', 'real');
theta_as_function_of_theta2 = theta2 - (pi/3 - pi/2)/npp;


%% Define psi_abcr
% Define in terms of theta
psi_abcr = Psim*[cos(npp*theta); cos(npp*theta - 2*pi/3); cos(npp*theta + 2*pi/3)];

% Express in terms of theta2
psi_abcr = subs(psi_abcr, theta, theta_as_function_of_theta2);


%% Compute back EMFs
e_abc = omega*diff(psi_abcr, theta2);


%% Define inductance matrix
% Define in terms of theta
Lg =  0;
Laa = Ls + Lg*cos(2*(npp*theta));
Lbb = Ls + Lg*cos(2*(npp*theta - 2*pi/3));
Lcc = Ls + Lg*cos(2*(npp*theta + 2*pi/3));
Lab = -Ms + Lg*cos(2*(npp*theta - pi/3));
Lac = -Ms + Lg*cos(2*(npp*theta + pi/3));
Lbc = -Ms + Lg*cos(2*(npp*theta));
L = [Laa, Lab, Lac; Lab, Lbb, Lbc; Lac, Lbc, Lcc];

% Express in terms of theta2
L = subs(L, theta, theta_as_function_of_theta2);


%% Compute L_prime (derivative of L with respect to theta)
Lprime = diff(L, theta2);

% Simplify
Lprime = simplify(Lprime);


%% Transform from phase variables to star-equivalent model by transforming it to source variables
% Transformation between source and phase voltages
K_v_s2p_delta = [1 -1 0; 0 1 -1; -1 0 1];
K_v_p2s_delta = 1/3*[1 0 -1; -1 1 0; 0 -1 1];

% Transformation between source and phase currents
K_i_s2p_delta = 1/3*[1 -1 0; 0 1 -1; -1 0 1];
K_i_p2s_delta = [1 0 -1; -1 1 0; 0 -1 1];

% Transform back EMFs
e_abc_steq = K_v_p2s_delta*e_abc;
e_abc_steq = simplify(e_abc_steq);

% Transform resistances
Rs_steq = K_v_p2s_delta*Rs*K_i_s2p_delta;
Rs_steq = simplify(Rs_steq);

% Transform L
L_steq = K_v_p2s_delta*L*K_i_s2p_delta;
L_steq = simplify(L_steq);

% Transform Lprime
Lprime_steq = K_v_p2s_delta*Lprime*K_i_s2p_delta;
Lprime_steq = simplify(Lprime_steq);


%% Print PMSM model in abc variables
disp('--------------------------------------------------------------------');
disp('Mathematical model of delta PMSM expressed as star-equivalent PMSM in abc variables:');
disp('   L_steq*di_abc_steq/dt   =   -  Rs_steq*i_abc_steq  -  w*Lprime_steq*i_abc_steq  -  e_abc_steq  +  v_steq');
disp('       J*dw/dt   =   1/2*i_steq^T*Lprime_steq*i_steq  +  1/w*i_steq^T*e_steq  -  B*w  +  tau_cog  +  tau_ex');
disp(' ');
disp('Rs_steq = ');
disp(Rs_steq);
disp(' ');
disp('L_steq = ');
disp(L_steq);
disp(' ');
disp('Lprime_steq = ');
disp(Lprime_steq);
disp(' ');
disp('e_abc_steq = ');
disp(e_abc_steq);


%% Define Clarke transformation
% Original Clarke transformation (amplitude invariant)
K_clarke = 2/3*[   1,       -1/2,       -1/2;
                   0,  sqrt(3)/2, -sqrt(3)/2;
                 1/2,        1/2,        1/2];

% Concordia transformation (power invariant)
K_concordia = sqrt(2/3)*[   1,       -1/2,       -1/2;
                            0,  sqrt(3)/2, -sqrt(3)/2;
                          1/2,        1/2,        1/2];

% Unitary Clarke transformation (power invariant and unitary)
K_unitary = sqrt(2/3)*[   1,       -1/2,       -1/2;
                          0,  sqrt(3)/2, -sqrt(3)/2;
                  1/sqrt(2),  1/sqrt(2),  1/sqrt(2)];
      
% Choose transformation
K = sym(K_clarke);
%K = sym(K_concordia);
%K = sym(K_unitary);


%% Define transformed resistance matrix
Rs_abg_steq = K*Rs_steq*K^(-1);
Rs_abg_steq = simplify(Rs_abg_steq);


%% Define transformed inductance matrix and its derivative with respect to theta
L_abg_steq = K*L_steq*K^(-1);
L_abg_prime_steq = diff(L_abg_steq, theta2);

% Simplify
L_abg_steq = simplify(L_abg_steq);
L_abg_prime_steq = simplify(L_abg_prime_steq);


%% Transform back EMF
e_abg_steq = K*e_abc_steq;

% Simplify
e_abg_steq = simplify(e_abg_steq);

      
%% Print PMSM model in abg variables (alpha-beta-gamma)
disp('--------------------------------------------------------------------');
disp('Mathematical model of delta PMSM expressed as star-equivalent PMSM in abg variables:');
disp('   L_abg_steq*di_abg_steq/dt   =   -  Rs_abg_steq*i_abg_steq  -  w*L_abg_prime_steq*i_abg_steq  -  e_abg_steq  +  v_abg_steq');
disp('       J*dw/dt   =   1/2*i_abg_steq^T*K^-T*K^-1*L_abg_prime_steq*i_abg_steq  +  1/w*i_abg_steq^T*K^-T*K^-1*e_abg_steq  -  B*w  +  tau_cog  +  tau_ex');
disp(' ');
disp('Rs_abg_steq = ');
disp(Rs_abg_steq);
disp(' ');
disp('L_abg_steq = ');
disp(L_abg_steq);
disp(' ');
disp('L_abg_prime_steq = ');
disp(L_abg_prime_steq);
disp(' ');
disp('e_abg_steq = ');
disp(e_abg_steq);
disp('K^-T*K^-1 = ');
disp(simplify((K^-1)'*K^-1));

   
%% Transform PMSM model to dqz variables (direct-quadrature-zero)
R_theta2 = [cos(npp*theta2), sin(npp*theta2), 0;
           -sin(npp*theta2), cos(npp*theta2), 0;
                          0,               0, 1];
Rs_dqz_steq = R_theta2*Rs_abg_steq*R_theta2^-1;
L_dqz_steq = R_theta2*L_abg_steq*R_theta2^-1;
L_tilde_dqz_steq = R_theta2*L_abg_prime_steq*R_theta2^-1;
L_dtilde_dqz_steq = L_tilde_dqz_steq + R_theta2*L_abg_steq*diff(R_theta2^-1, theta2);
e_dqz_steq = R_theta2*e_abg_steq;

% Simplify
Rs_dqz_steq = simplify(Rs_dqz_steq);
L_dqz_steq = simplify(L_dqz_steq);
L_tilde_dqz_steq = simplify(L_tilde_dqz_steq);
L_dtilde_dqz_steq = simplify(L_dtilde_dqz_steq);
e_dqz_steq = simplify(e_dqz_steq);


%% Print PMSM model to dqz variables
disp('--------------------------------------------------------------------');
disp('Mathematical model of delta PMSM expressed as star-equivalent PMSM in dqz variables:');
disp('   L_dqz_steq*di_dqz_steq/dt   =   -  Rs_dqz_steq*i_dqz  -  w*L_dtilde_dqz_steq*i_dqz  -  e_dqz_steq  +  v_dqz_steq');
disp('       J*dw/dt   =   1/2*i_dqz_steq^T*K^-T*K^-1*L_tilde_dqz_steq*i_dqz_steq  +  1/w*i_dqz_steq^T*K^-T*K^-1*e_dqz_steq  -  B*w  +  tau_cog  +  tau_ex');
disp(' ');
disp('Rs_dqz_steq = ');
disp(Rs_dqz_steq);
disp(' ');
disp('L_dqz_steq = ');
disp(L_dqz_steq);
disp(' ');
disp('L_tilde_dqz_steq = ');
disp(L_tilde_dqz_steq);
disp('L_dtilde_dqz_steq = ');
disp(L_dtilde_dqz_steq);
disp(' ');
disp('e_dqz_steq = ');
disp(e_dqz_steq);
disp('K^-TK^-1 = ');
disp(simplify((K^-1)'*K^-1));


%% Define Ld, Lq, Lz, and Kt and substitute
Rd_steq_def = Rs_dqz_steq(1, 1);
Rq_steq_def = Rs_dqz_steq(2, 2);
Rz_steq_def = Rs_dqz_steq(3, 3);

Ld_steq_def = L_dqz_steq(1, 1);
Lq_steq_def = L_dqz_steq(2, 2);
Lz_steq_def = L_dqz_steq(3, 3);
Lg_steq_def = (Ld_steq - Lq_steq); % == (Ld - Lq)/3
Kb_steq_def = e_dqz_steq(2, 1)/omega;

% Sum up with zero matrix to express the matrix using Ld and Lq instead of Ls, Ms, and Lg
L_dqz_steq = L_dqz_steq + [Ld_steq - Ld_steq_def, 0, 0; 0, Lq_steq - Lq_steq_def, 0; 0, 0, Lz_steq - Lz_steq_def];


% Substitute in for Lg
L_tilde_dqz_steq = subs(L_tilde_dqz_steq, Lg, Lg_steq_def);

% Sum up with zero matrix to express the matrix using Ld, Lq, Lz instead of Ls, Ms, Lg
L_dtilde_dqz_steq = L_dtilde_dqz_steq + npp*[0, -Lq_steq + Lq_steq_def, 0; Ld_steq - Ld_steq_def, 0, 0; 0, 0, 0];

% Substitute in for Kt
e_dqz_steq = subs(e_dqz_steq, Kb_steq_def, Kb);

% Simplify
L_dqz_steq = simplify(L_dqz_steq);
L_tilde_dqz_steq = simplify(L_tilde_dqz_steq);
L_dtilde_dqz_steq = simplify(L_dtilde_dqz_steq);
e_dqz_steq = simplify(e_dqz_steq);


%% Define DQ currents  and voltages as symbolic variables
id_steq = sym('id_steq', 'real');
iq_steq = sym('iq_steq', 'real');
iz_steq = sym('iz_steq', 'real');
i_dqz_steq = [id_steq; iq_steq; iz_steq];
vd_steq = sym('vd_steq', 'real');
vq_steq = sym('vq_steq', 'real');
vz_steq = sym('vz_steq', 'real');
v_dqz_steq = [vd_steq; vq_steq; vz_steq];


%% Compute terms in DQ model

% Coupling term in current equations
omega_L_dtilde_i_dqz_steq = omega*L_dtilde_dqz_steq*i_dqz_steq;

% Reluctance torque
tau_rel = 1/2*i_dqz_steq'*(K^-1)'*K^-1*L_tilde_dqz_steq*i_dqz_steq;

% Synchronous torque
tau_syn = 1/omega*i_dqz_steq'*(K^-1)'*K^-1*e_dqz_steq;

% Total electromagnetic torque
tau_el = tau_rel + tau_syn;

% Simplify
omega_L_dtilde_i_dqz_steq = simplify(omega_L_dtilde_i_dqz_steq);
tau_rel = simplify(tau_rel);
tau_syn = simplify(tau_syn);
tau_el = simplify(tau_el);


%% Print PMSM model to dqz variables (scalar form )
disp('--------------------------------------------------------------------');
disp('Mathematical model of delta PMSM expressed as star-equivalent PMSM in dqz variables (scalar form):');
fprintf('   Ld_steq*did_steq/dt  = -Rd_steq*id_steq - %s - %s + vd\n', char(omega_L_dtilde_i_dqz_steq(1, 1)), char(e_dqz_steq(1, 1)));
fprintf('   Lq_steq*diq_steq/dt  = -Rq_steq*iq_steq - %s - %s + vq\n', char(omega_L_dtilde_i_dqz_steq(2, 1)), char(e_dqz_steq(2, 1)));
fprintf('   Lz_steq*diz_steq/dt  = -Rz_steq*iz_steq - %s - %s + vz\n', char(omega_L_dtilde_i_dqz_steq(3, 1)), char(e_dqz_steq(3, 1)));
fprintf('   J*domega/dt = %s + %s - B*omega + tau_cog + tau_ex\n', char(tau_rel), char(tau_syn));
fprintf('\n');
fprintf('   Rd_steq = %s\n', char(Rd_steq_def));
fprintf('   Rq_steq = %s\n', char(Rq_steq_def));
fprintf('   Rz_steq = %s\n', char(Rz_steq_def));
fprintf('   Ld_steq = %s\n', char(Ld_steq_def));
fprintf('   Lq_steq = %s\n', char(Lq_steq_def));
fprintf('   Lz_steq = %s\n', char(Lz_steq_def));
fprintf('   Kb_steq = %s\n', char(Kb_steq_def));
K_name = 'unknown';
if K == K_clarke
    K_name = 'Clarke';
elseif K == K_concordia
    K_name = 'Concordia';
elseif K == K_unitary
    K_name = 'Unitary';
end
fprintf('   Used alpha-beta-gamma transformation: %s\n', K_name);
fprintf('      K = %s\n', char(K));


