%% Clear workspace
clear all
close all
clc


%% Define relations between units

% Gravitational acceleration
g_in_m_per_s2 = 9.80665;

% Length conversion
ft_to_m = 0.3048;
ft_to_in = 12;

% Mass conversion
lb_to_kg = 0.45359237;
lb_to_oz = 16;

% Force conversion
lbf_to_N = lb_to_kg*g_in_m_per_s2;

% Torque conversion
lbf_ft_to_N_m = lbf_to_N*ft_to_m;
ozf_in_to_N_m = lbf_ft_to_N_m/lb_to_oz/ft_to_in;

% Moment of interia conversion
ozf_in_s2_to_kg_m2 = ozf_in_to_N_m;


