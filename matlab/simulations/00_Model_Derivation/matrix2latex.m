%% Clear command window
clc;

%% Choose symbolic matrix to convert
M = K_v_s2p_delta;


%% Convert
% Get matrix dimensions
m = size(M, 1);
n = size(M, 2);
% Create first line
s = sprintf('    \\begin{bmatrix}\n');
% Add matrix content
for k = 1:m
    for l = 1:n
        s = sprintf('%s        %d', s, M(k, l));
        if l < n
            s = sprintf('%s &', s);
        end
    end
    if k < m
        s = sprintf('%s \\cr', s);
    end
    s = sprintf('%s\n', s);
end
% Add last line
s = sprintf('%s    \\end{bmatrix}\n', s);
% Print the result
disp(s);

