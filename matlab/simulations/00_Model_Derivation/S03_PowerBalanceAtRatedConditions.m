
%% Clear workspace
clear all
close all
clc


%% Define rated conditions
omega_rated = 4000/60*2*pi; % [rad/s]
I_m_rated = 3.67*sqrt(2);   % [A(peak)]
tau_rated = 0.22;           % [N m]
Rs_datasheet = 0.64/2;      % [Ohm]
B_datasheet = 0;            % [N m/(rad/s)]


%% Define symbolic variables
syms omega theta real
syms npp Rs Ls Ms B real


%% Define voltage and current waveforms at rated conditions
syms Im real
iabc = -Im*[sin(npp*theta); sin(npp*theta - 2*pi/3); sin(npp*theta + 2*pi/3)];

syms Vm phi real
phi = 0;
vabc = Vm*[sin(npp*theta - phi); sin(npp*theta - phi - 2*pi/3); sin(npp*theta - phi + 2*pi/3)];


%% Express powers that are nonzero in steady state
Pfr = omega*B*omega;
Pex = -omega*tau_rated;
Pres = simplify(Rs*(iabc')*iabc);
Pel = simplify(iabc'*vabc);


%% Evaluate for rated conditions
Pfr_rated = eval(subs(Pfr, {omega, B}, {omega_rated, B_datasheet}));
Pres_rated = eval(subs(Pres, {Im, Rs}, {I_m_rated, Rs_datasheet}));
Pel_rated = subs(Pel, Im, I_m_rated);
Pex_rated = subs(Pex, omega, omega_rated);


%% Solve for Vm (== vq)
Vm_solved = eval(solve(Pfr_rated + Pres_rated + Pex_rated == Pel_rated))

