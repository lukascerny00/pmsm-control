%% Clear workspace
clear
close
clc


%% Define motor parameters
npp = sym('npp', 'real');
Rs = sym('Rs', 'real');
Ls = sym('Ls', 'real');
Ms = sym('Ms', 'real');
Lg = sym('Lg', 'real');
Psim = sym('Psim', 'real');
J = sym('J', 'real');
B = sym('B', 'real');
Ld = sym('Ld', 'real');
Lq = sym('Lq', 'real');
Lz = sym('Lz', 'real');
Kb = sym('Kb', 'real');


%% Define state variables and inputs
% State variables
ia = sym('ia', 'real');
ib = sym('ib', 'real');
ic = sym('ic', 'real');
omega = sym('omega', 'real');
theta = sym('theta', 'real');

% Inputs
va = sym('va', 'real');
vb = sym('vb', 'real');
vc = sym('vc', 'real');
tau_ex = sym('tau_ex', 'real');
tau_cog = sym('tau_cog', 'real');


%% Create vectors i_abc and v_abc
i_abc = [ia; ib; ic];
v_abc = [va; vb; vc];


%% Define psi_abcr
psi_abcr = Psim*[cos(npp*theta); cos(npp*theta - 2*pi/3); cos(npp*theta + 2*pi/3)];


%% Compute back EMFs
e_abc = omega*diff(psi_abcr, theta);


%% Define inductance matrix
Laa = Ls + Lg*cos(2*(npp*theta));
Lbb = Ls + Lg*cos(2*(npp*theta - 2*pi/3));
Lcc = Ls + Lg*cos(2*(npp*theta + 2*pi/3));
Lab = -Ms + Lg*cos(2*(npp*theta - pi/3));
Lac = -Ms + Lg*cos(2*(npp*theta + pi/3));
Lbc = -Ms + Lg*cos(2*(npp*theta));
L = [Laa, Lab, Lac; Lab, Lbb, Lbc; Lac, Lbc, Lcc];


%% Compute L_prime (derivative of L with respect to theta)
Lprime = diff(L, theta);

% Simplify
Lprime = simplify(Lprime);


%% Print PMSM model in abc variables
disp('--------------------------------------------------------------------');
disp('Mathematical model of PMSM in abc variables:');
disp('   L*di_abc/dt   =   -  Rs*i_abc  -  w*Lprime*i_abc  -  e_abc  +  v_abc');
disp('       J*dw/dt   =   1/2*i_abc^T*Lprime*i_abc  +  1/w*i_abc^T*e_abc  -  B*w  +  tau_cog  +  tau_ex');
disp(' ');
disp('L = ');
disp(L);
disp(' ');
disp('Lprime = ');
disp(Lprime);
disp(' ');
disp('e_abc = ');
disp(e_abc);


%% Define Clarke transformation
% Original Clarke transformation (amplitude invariant)
K_clarke = 2/3*[   1,       -1/2,       -1/2;
                   0,  sqrt(3)/2, -sqrt(3)/2;
                 1/2,        1/2,        1/2];

% Concordia transformation (power invariant)
K_concordia = sqrt(2/3)*[   1,       -1/2,       -1/2;
                            0,  sqrt(3)/2, -sqrt(3)/2;
                          1/2,        1/2,        1/2];

% Unitary Clarke transformation (power invariant and unitary)
K_unitary = sqrt(2/3)*[   1,       -1/2,       -1/2;
                          0,  sqrt(3)/2, -sqrt(3)/2;
                  1/sqrt(2),  1/sqrt(2),  1/sqrt(2)];
      
% Choose transformation
K = sym(K_clarke);
%K = sym(K_concordia);
%K = sym(K_unitary);


%% Define transformed inductance matrix and its derivative with respect to theta
L_abg = K*L*K^(-1);
L_abg_prime = diff(L_abg, theta);

% Simplify
L_abg = simplify(L_abg);
L_abg_prime = simplify(L_abg_prime);


%% Transform back EMF
e_abg = K*e_abc;

% Simplify
e_abg = simplify(e_abg);

      
%% Print PMSM model in abg variables (alpha-beta-gamma)
disp('--------------------------------------------------------------------');
disp('Mathematical model of PMSM in abg variables:');
disp('   L_abg*di_abg/dt   =   -  Rs*i_abg  -  w*L_abg_prime*i_abg  -  e_abg  +  v_abg');
disp('       J*dw/dt   =   1/2*i_abg^T*K^-T*K^-1*L_abg_prime_*i_abg  +  1/w*i_abg^T*K^-T*K^-1*e_abg  -  B*w  +  tau_cog  +  tau_ex');
disp(' ');
disp('L_abg = ');
disp(L_abg);
disp(' ');
disp('L_abg_prime = ');
disp(L_abg_prime);
disp(' ');
disp('e_abg = ');
disp(e_abg);
disp('K^-T*K^-1 = ');
disp(simplify((K^-1)'*K^-1));

   
%% Transform PMSM model to dqz variables (direct-quadrature-zero)
R = [cos(npp*theta), sin(npp*theta), 0;
    -sin(npp*theta), cos(npp*theta), 0;
                  0,              0, 1];
L_dqz = R*L_abg*R^-1;
L_tilde_dqz = R*L_abg_prime*R^-1;
L_dtilde_dqz = L_tilde_dqz + R*L_abg*diff(R^-1, theta);
e_dqz = R*e_abg;

% Simplify
L_dqz = simplify(L_dqz);
L_tilde_dqz = simplify(L_tilde_dqz);
L_dtilde_dqz = simplify(L_dtilde_dqz);
e_dqz = simplify(e_dqz);


%% Print PMSM model to dqz variables
disp('--------------------------------------------------------------------');
disp('Mathematical model of PMSM in dqz variables:');
disp('   L_dqz*di_dqz/dt   =   -  Rs*i_dqz  -  w*L_dtilde_dqz*i_dqz  -  e_dqz  +  v_dqz');
disp('       J*dw/dt   =   1/2*i_dqz^T*K^-T*K^-1*L_tilde_dqz*i_dqz  +  1/w*i_dqz^T*K^-T*K^-1*e_dqz  -  B*w  +  tau_cog  +  tau_ex');
disp(' ');
disp('L_dqz = ');
disp(L_dqz);
disp(' ');
disp('L_tilde_dqz = ');
disp(L_tilde_dqz);
disp('L_dtilde_dqz = ');
disp(L_dtilde_dqz);
disp(' ');
disp('e_dqz = ');
disp(e_dqz);
disp('K^-T*K^-1 = ');
disp(simplify((K^-1)'*K^-1));


%% Define Ld, Lq, Lz, and Kt and substitute
Ld_def = L_dqz(1, 1);
Lq_def = L_dqz(2, 2);
Lz_def = L_dqz(3, 3);
Lg_def = (Ld - Lq)/3;
Kb_def = e_dqz(2, 1)/omega;

% Sum up with zero matrix to express the matrix using Ld and Lq instead of Ls, Ms, and Lg
L_dqz = L_dqz + [Ld - Ld_def, 0, 0; 0, Lq - Lq_def, 0; 0, 0, Lz - Lz_def];

% Substitute in for Lg
L_tilde_dqz = subs(L_tilde_dqz, Lg, Lg_def);

% Sum up with zero matrix to express the matrix using Ld, Lq, Lz instead of Ls, Ms, Lg
L_dtilde_dqz = L_dtilde_dqz + npp*[0, -Lq + Lq_def, 0; Ld - Ld_def, 0, 0; 0, 0, 0];

% Substitute in for Kt
e_dqz = subs(e_dqz, Kb_def, Kb);

% Simplify
L_dqz = simplify(L_dqz);
L_tilde_dqz = simplify(L_tilde_dqz);
L_dtilde_dqz = simplify(L_dtilde_dqz);
e_dqz = simplify(e_dqz);


%% Define DQ currents  and voltages as symbolic variables
id = sym('id', 'real');
iq = sym('iq', 'real');
iz = sym('iz', 'real');
i_dqz = [id; iq; iz];
vd = sym('vd', 'real');
vq = sym('vq', 'real');
vz = sym('vz', 'real');
v_dqz = [vd; vq; vz];


%% Compute terms in DQ model

% Coupling term in current equations
omega_L_dtilde_i_dqz = omega*L_dtilde_dqz*i_dqz;

% Reluctance torque
tau_rel = 1/2*i_dqz'*(K^-1)'*K^-1*L_tilde_dqz*i_dqz;

% Synchronous torque
tau_syn = 1/omega*i_dqz'*(K^-1)'*K^-1*e_dqz;

% Total electromagnetic torque
tau_el = tau_rel + tau_syn;

% Simplify
omega_L_dtilde_i_dqz = simplify(omega_L_dtilde_i_dqz);
tau_rel = simplify(tau_rel);
tau_syn = simplify(tau_syn);
tau_el = simplify(tau_el);


%% Print PMSM model to dqz variables (scalar form )
disp('--------------------------------------------------------------------');
disp('Mathematical model of PMSM in dqz variables (scalar form):');
fprintf('   Ld*did/dt  = -Rs*id - %s - %s + vd\n', char(omega_L_dtilde_i_dqz(1, 1)), char(e_dqz(1, 1)));
fprintf('   Lq*diq/dt  = -Rs*iq - %s - %s + vq\n', char(omega_L_dtilde_i_dqz(2, 1)), char(e_dqz(2, 1)));
fprintf('   Lz*diz/dt  = -Rs*iz - %s - %s + vz\n', char(omega_L_dtilde_i_dqz(3, 1)), char(e_dqz(3, 1)));
fprintf('   J*domega/dt = %s + %s - B*omega + tau_cog + tau_ex\n', char(tau_rel), char(tau_syn));
fprintf('\n');
fprintf('   Ld = %s\n', char(Ld_def));
fprintf('   Lq = %s\n', char(Lq_def));
fprintf('   Lz = %s\n', char(Lz_def));
fprintf('   Kb = %s\n', char(Kb_def));
K_name = 'unknown';
if K == K_clarke
    K_name = 'Clarke';
elseif K == K_concordia
    K_name = 'Concordia';
elseif K == K_unitary
    K_name = 'Unitary';
end
fprintf('   Used alpha-beta-gamma transformation: %s\n', K_name);
fprintf('      K = %s\n', char(K));


%% Linearize the DQZ model

% Define model 
f = [inv(L_dqz)*(-Rs*i_dqz - omega*L_dtilde_dqz*i_dqz - e_dqz + v_dqz);
     (1/2*i_dqz'*(K^-1)'*K^-1*L_tilde_dqz*i_dqz  +  1/omega*i_dqz'*(K^-1)'*K^-1*e_dqz  -  B*omega  +  tau_cog  +  tau_ex)/J;
     omega];
f = simplify(f);

% Linearize
Ac = [diff(f, id), diff(f, iq), diff(f, omega), diff(f, theta)];
Bc = [diff(f, vd), diff(f, vq), diff(f, tau_ex), diff(f, tau_cog)];
Cc = eye(5);
Dc = zeros(5, 5);

% Simplify
Ac = simplify(Ac);
Bc = simplify(Bc);

disp(' ')
disp('--------------------------------------------------------------------');
disp('Linearized DQZ model:');
Ac
Bc
Cc
Dc

