%% Clear workspace
close all
clear all
clc


%% Set Motor Parameters

% Parameters
pmsm1.npp = 2;          % [-]
pmsm1.Ld = 0.00105;     % [H]
pmsm1.Lq = 0.00105;     % [H]
pmsm1.Rs = 0.32;        % [Ohm]
pmsm1.Kb = 0.028284;    % [V/(rad/s)]
pmsm1.J = 1.19e-5;      % [kg*m^2]
pmsm1.B = 1.00e-6;      % [N*m/rad/s)] (This is just a guess)

% Initial Conditions
pmsm1.i_d0 = 0;         % [A]
pmsm1.i_q0 = 0;         % [A]
pmsm1.omega0 = 0;       % [rad/s]
pmsm1.theta0 = 0;       % [rad]

% Rated conditions
omega_rated = 4000/60*2*pi; % [rad/s]
Imax = 3.67*sqrt(2);    % [A]
load_torque = 0.218;     % [N m]
Vmax = 36/sqrt(3);      % [V]

