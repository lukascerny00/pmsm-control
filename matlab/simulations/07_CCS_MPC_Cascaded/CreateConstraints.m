function [Px, px, Pu, pu] = CreateConstraints(Imax, Vmax)
% Create voltage and current constraints - approximated by octagon

Px = [         +1, +sqrt(2)-1, 0, 0;
               +1, -sqrt(2)+1, 0, 0;
               -1, +sqrt(2)-1, 0, 0;
               -1, -sqrt(2)+1, 0, 0;
       +sqrt(2)-1,         +1, 0, 0;
       +sqrt(2)-1,         -1, 0, 0;
       -sqrt(2)+1,         +1, 0, 0;
       -sqrt(2)+1,         -1, 0, 0];
px = [Imax; Imax; Imax; Imax; Imax; Imax; Imax; Imax];

Pu = [         +1, +sqrt(2)-1;
               +1, -sqrt(2)+1;
               -1, +sqrt(2)-1;
               -1, -sqrt(2)+1;
       +sqrt(2)-1,         +1;
       +sqrt(2)-1,         -1;
       -sqrt(2)+1,         +1;
       -sqrt(2)+1,         -1];
pu = [Vmax; Vmax; Vmax; Vmax; Vmax; Vmax; Vmax; Vmax];

end

