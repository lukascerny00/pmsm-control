function [Ad, Bd] = CreateSystemMatricesMechanical(Kb, J, B, Ts)
% Create PMSM system matrices - linearized and discretized (using Euler approximation).

% Get continuous-time linear system dynamics
Ac = [-B/J, 0;
         1, 0];
Bc = [3/2*Kb/J;
             0];

% Get discrete-time linear system dynamcis
Ad = eye(2) + Ts*Ac;
Bd = Ts*Bc;

end

