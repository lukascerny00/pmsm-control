function [Pu, pu] = CreateConstraintsMechanical(Imax)
% Create voltage and current constraints - approximated by octagon

Pu = [1; -1];
pu = [Imax; Imax];

end

