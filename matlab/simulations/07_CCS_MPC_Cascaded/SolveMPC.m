function [z_opt] = SolveMPC(H, F_T, G, W, S, a, x0, maxiter)
% Solves MPC (convex QP with linear constraints) in the following form:
%   min 1/2z'Hz + a'F'z
%  s.t. Gz <= W + Sx0
% (a' = [x0' uprev' r1' ... rN']

K = H^-1*G';
g = H^-1*F_T'*a;
L = 1/max(abs(eig(G*H^-1*G'))) + 1;

iMG = K;
iMc = g;
GL = 1/L*G;
bL = 1/L*(W + S*x0);
epsGL = 0.001;
epsVL = 0.001;

n = size(H, 1);
m = size(G, 1);
k = 1;
y0 = zeros(m, 1);
y = y0;
z = zeros(n, 1);
while k < maxiter
    beta = max((k-1)/(k+2), 0);
    w = y + beta*(y-y0);
    z=-(iMG*w+iMc);
    s=GL*z-bL;
    y0=y;
    % Termination
    if all(s<=epsGL)
        gapL=-w'*s;
        if gapL<=epsVL
            break
        end
    end
    y=w+s;
    k=k+1;
end
z_opt = z;
disp(k)

end

