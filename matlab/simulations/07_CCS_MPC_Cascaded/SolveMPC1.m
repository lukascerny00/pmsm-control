function [z_opt] = SolveMPC1(H, F_T, G, W, S, a, x0, maxiter)
% Solves MPC (convex QP with linear constraints) in the following form:
%   min 1/2z'Hz + a'F'z
%   s.t. Gz <= W + Sx0
% (a' = [x0' uprev' r1' ... rN']
% It uses basic gradient projection method applied to the dual problem.

% Express the dual problem in the form:
%   min 1/2y'Qy + d'y
%   s.t. y >= 0
%   Q = GH^-1G', d = W + Sx0 + GH^-1Fa
Q = G*H^-1*G';
d = W + S*x0 + G*H^-1*F_T'*a;
lambda0 = 0.9/max(abs(eig(Q))); % lambda should be from interval (0,  1/max(eig(Q))]
m = size(Q, 1);

k = 1;
y = zeros(m, 1);

while k <= maxiter
    lambda = lambda0*5;
    y = max(y - lambda*(Q*y+d), 0);
    k = k + 1;
end

% Compare with: y_opt = feval('quadprog', Q, d', -eye(m), zeros(m, 1));
z_opt = -H^-1*(F_T'*a + G'*y);

end

