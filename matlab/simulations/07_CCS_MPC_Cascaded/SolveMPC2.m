function [z_opt] = SolveMPC2(H, F_T, G, W, S, a, x0, maxiter)
% Solves MPC (convex QP with linear constraints) in the following form:
%   min 1/2z'Hz + a'F'z
%  s.t. Gz <= W + Sx0
% (a' = [x0' uprev' r1' ... rN']
% It uses fast gradient projection method.

K = H^-1*G';
g = H^-1*F_T'*a;
lambda = 0.9/max(abs(eig(G*H^-1*G'))); % lambda should be from interval (0,  1/max(eig(Q))]
m = size(G, 1);

y_prev = zeros(m, 1); 
y = y_prev;

k = 1;
while k < maxiter
    beta = (k-1)/(k+2);
    w = y + beta*(y - y_prev);
    x = -K*w - g;
    s = lambda*(G*x - W - S*x0);
    y_prev = y;
    y = max(w+s, 0);
    k = k+1;
end

z_opt = -H^-1*(F_T'*a + G'*y);

end

