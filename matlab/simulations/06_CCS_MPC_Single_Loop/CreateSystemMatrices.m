function [Ad, Bd] = CreateSystemMatrices(Rs, Ld, Lq, Kb, npp, J, B, Ts, omega)
% Create PMSM system matrices - linearized and discretized (using Euler approximation).

% Get continuous-time linear system dynamics
Ac = [            -Rs/Ld, (Lq*npp*omega)/Ld,      0, 0;
      -(Ld*npp*omega)/Lq,            -Rs/Lq, -Kb/Lq, 0;
                       0,          3/2*Kb/J,   -B/J, 0;
                       0,                 0,      1, 0];
Bc = [1/Ld,   0; 
        0, 1/Lq; 
        0,    0;
        0,    0];

% Get discrete-time linear system dynamcis
Ad = eye(4) + Ts*Ac;
Bd = Ts*Bc;

end

