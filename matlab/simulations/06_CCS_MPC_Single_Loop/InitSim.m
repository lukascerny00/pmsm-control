%% Clear workspace
close all
clear all
clc


%% Set Motor Parameters

% Parameters
pmsm1.npp = 2;          % [-]
pmsm1.Rs = 0.45;        % [Ohm]
pmsm1.Ld = 0.0008;      % [H]
pmsm1.Lq = 0.0009;      % [H]
pmsm1.Kb = 0.023;       % [V/(rad/s)]
pmsm1.J = 0.000028;      % [kg*m^2]
pmsm1.B = 0.000013;      % [N*m/rad/s)]

% Initial Conditions
pmsm1.i_d0 = 0;         % [A]
pmsm1.i_q0 = 0;         % [A]
pmsm1.omega0 = 0;       % [rad/s]
pmsm1.theta0 = 0;       % [rad]


%% Set PWM parameters
pwm.DCVoltage = 24;     % [V]
pwm.period = 4e-5;      % [s]
pwm.Ts = 2e-8;          % [s]
pwm.ticks_per_period = pwm.period/pwm.Ts;


%% Set limit values
Imax = 3.67*sqrt(2); % [A]
Vmax = pwm.DCVoltage/sqrt(3);


%% Define CCSMPC controller for PMSM

% CCSMPC parameters
CCSMPC.Ts = 4e-5;                % [s] 
CCSMPC.Cd = [0, 0, 1, 0];        % Output matrix
CCSMPC.Q =  550*diag([1]);       % Weighting matrix
CCSMPC.R =  diag([1, 1]);        % Weighting matrix
CCSMPC.N =  3;                   % Prediction horizon

% Reference
reference = zeros(floor(0.02/CCSMPC.Ts), 1);
reference(floor(0.001/CCSMPC.Ts):end, 1) = 10;
reference_single = zeros(CCSMPC.N, 1) + 20;


