
x = [0.3; 1.5; 20; 1.2];
npp = 4; % [-]
R = 0.45; % [Ohm]
Ld = 0.0006; % [H]
Lq = 0.0006; % [H]
Kt = 0.005620; % [Weber]
J = 4.80185536E-6; % [kg m^2]
B = 8.6365E-3; % [N m s]
Ts = Ts_fcsmpc;
Vdc = 24;
id_ref = 0;
iq_ref = 1;
k_opt_last = 1;

% Get state variables
id = x(1);
iq = x(2);
omega = x(3);
theta = x(4);

% Get continuous-time linear system dynamics
Ac = [                      -R/Ld,                     (Lq*npp*omega)/Ld,           (Lq*iq*npp)/Ld, 0;
               -(Ld*npp*omega)/Lq,                                 -R/Lq, - Kt/Lq - (Ld*id*npp)/Lq, 0;
       (3*iq*npp*(Ld - Lq))/(2*J), (3*npp*(id*(Ld - Lq) + Kt/npp))/(2*J),                     -B/J, 0;
                                0,                                     0,                        1, 0];
Bc = [1/Ld,    0; 
        0, 1/Lq; 
        0,    0;
        0,    0];

% Get discrete-time linear system dynamcis
Ad = eye(4) + Ts*Ac;
Bd = Ts*Bc;

% Generate ABC voltage vectors
vABC_set_size = 8;
vABC_set = zeros(3, 8);
for k = 1:vABC_set_size
   vABC_set(:, k) = [bitand(k-1, 1)~=0; bitand(k-1, 2)~=0; bitand(k-1, 4)~=0];
end
vABC_set_normalized = vABC_set;
vABC_set = Vdc*vABC_set;

% Transform voltage vectors to DQ system
K = 2/3*[   1,       -1/2      , -1/2;
            0,  sqrt(3)/2, -sqrt(3)/2;
          1/2,        1/2,        1/2];
v_alpha_beta_set = K(1:2, :)*vABC_set;
vDQ_set = [cos(npp*theta), sin(npp*theta);
          -sin(npp*theta), cos(npp*theta)]*v_alpha_beta_set;

% Find optimal control
J_opt = inf;
k_opt = 1;
for k = 1:vABC_set_size
    
    % Get DQ voltage vector
    vDQ = vDQ_set(:, k);
    
    % Predict state in next step
    x_next = Ad*x + Bd*vDQ;

    % Compute cost
    J = sqrt((id_ref - x_next(1))^2 + (iq_ref - x_next(2))^2) + lambda*sum(abs(vABC_set_normalized(:, k) - vABC_set_normalized(:, k_opt_last)))
    
    % Check if this control is the best so far
    if J < J_opt
        J_opt = J;
        k_opt = k;
    end

end

vA = vABC_set(1, k_opt);
vB = vABC_set(2, k_opt);
vC = vABC_set(3, k_opt);

J_opt