function [H, F_T, G, W, S] = CreateCondensedMats(A, B, C, Q, R, Px, px, N)
% Takes system matrices A, B, C, weighting matrices and prediction
% horizon N and creates matrices H, F, G, W, S for QP program:
%   min 1/2z'Hz + [x0' uprev' r1' ... rN']F
%  s.t. Gz <= W + Sx0

% Get system dimensions
nx = size(A, 1);
nu = size(B, 2);
ny = size(C, 1);

% Create A_bar and B_bar
A_bar = zeros(N*nx, nx);
B_bar = zeros(N*nx, N*nu);

A_bar(1:nx, :) = A;
B_bar(1:nx, 1:nu) = B;

for k = 2:N
   rows = ((k-1)*nx + 1):(k*nx);
   A_bar(rows, :) = A^k;
   B_bar(rows, :) = [A^(k-1)*B, B_bar(rows-nx, 1:end-nu)];
end

% Create NxN identity matrix
I_NxN = eye(N);

% Create C_bar, Q_bar, R_bar
C_bar = kron(I_NxN, C);
Q_bar = kron(I_NxN, Q);
R_bar = kron(I_NxN, R);

% Create S_bar, E_bar
S_bar = kron(I_NxN, eye(nu)) - [zeros(nu, nu*N); kron(eye(N-1), eye(nu)), zeros(nu*(N-1), nu)];
E_bar = [eye(nu); zeros(nu*(N-1), nu)];

% Create H, F_T
H = 2*(B_bar'*C_bar'*Q_bar*C_bar*B_bar + S_bar'*R_bar*S_bar);
F_T = [2*A_bar'*C_bar'*Q_bar*C_bar*B_bar; -2*E_bar'*R_bar*S_bar; -2*Q_bar*C_bar*B_bar];

% Create Px_bar, px_bar, Pu_bar, pu_bar
Px_bar = kron(I_NxN, Px);
px_bar = repmat(px, N, 1);

% Create G, W, S
G = [Px_bar*B_bar];
W = [px_bar];
S = [-Px_bar*A_bar];

end

