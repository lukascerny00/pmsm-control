function [u_abc_opt] = OneStepFCSMPC(Ad, Bd, Cd, Px, px, U_set, x0, u_abc_prev, y_ref, Q, R)

% Get size of control set
U_set_size = size(U_set, 2);

% Find optimal control
J_opt = inf;
k_opt = 1;
for k = 1:U_set_size
    
    % Get u_abc vector
    u_abc = U_set(:, k);
    
    % Predict state in next step
    x_next = Ad*x0 + Bd*u_abc;

    % Compute cost
    J = (y_ref - Cd*x_next)'*Q*(y_ref - Cd*x_next) + (u_abc-u_abc_prev)'*R*(u_abc-u_abc_prev)
    
    % Check constraints
    feasible = ~any(~(Px*x_next <= px));
    
    % Check if this control is the best so far
    if and(J < J_opt, feasible)
        J_opt = J;
        k_opt = k;
    end

end

% Return optimal voltage vector
u_abc_opt = U_set(:, k_opt);

end

