%% Clear workspace
close all
clear all
clc


%% Set Motor Parameters

% Parameters
pmsm1.npp = 2;          % [-]
pmsm1.Rs = 0.45;        % [Ohm]
pmsm1.Ld = 0.0008;      % [H]
pmsm1.Lq = 0.0009;      % [H]
pmsm1.Kb = 0.023;       % [V/(rad/s)]
pmsm1.J = 0.000028;      % [kg*m^2]
pmsm1.B = 0.000013;      % [N*m/rad/s)]

% Initial Conditions
pmsm1.i_d0 = 0;         % [A]
pmsm1.i_q0 = 0;         % [A]
pmsm1.omega0 = 0;       % [rad/s]
pmsm1.theta0 = 0;       % [rad]


%% Set PWM parameters
pwm.DCVoltage = 24;     % [V]
pwm.period = 4e-5;      % [s]
pwm.Ts = 2e-8;          % [s]
pwm.ticks_per_period = pwm.period/pwm.Ts;


%% Set limit values
Imax = 3.67*sqrt(2); % [A]
Vmax = pwm.DCVoltage/sqrt(3);


%% Define FCSMPC controller for  PMSM

% FCSMPC parameters
FCSMPC.Ts = 1e-6;                % [s] 
FCSMPC.Cd = eye(2, 4);           % Output matrix
FCSMPC.Q =  500*diag([10, 10]);       % Weighting matrix
FCSMPC.R =  diag([1, 1, 1]);        % Weighting matrix
FCSMPC.N = 1;                  % Prediction horizon (not used)




