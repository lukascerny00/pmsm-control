function [Ad, Bd, U_set] = CreateSystemMatricesFCSMPC(Rs, Ld, Lq, Kb, npp, J, B, Vdc, Ts, omega, theta)
% Create PMSM system matrices - linearized and discretized (using Euler approximation).

% Define transformation matrices
T_Clarke = 2/3*[   1,       -1/2      , -1/2;
                   0,  sqrt(3)/2, -sqrt(3)/2;
                 1/2,        1/2,        1/2];
T_Park = [cos(npp*theta), sin(npp*theta);
          -sin(npp*theta), cos(npp*theta)];
      
      
% Get continuous-time linear system dynamics
Ac = [            -Rs/Ld, (Lq*npp*omega)/Ld,      0, 0;
      -(Ld*npp*omega)/Lq,            -Rs/Lq, -Kb/Lq, 0;
                       0,          3/2*Kb/J,   -B/J, 0;
                       0,                 0,      1, 0];
Bc = [1/Ld,   0; 
        0, 1/Lq; 
        0,    0;
        0,    0]*T_Park*T_Clarke(1:2, :)*Vdc;

% Get discrete-time linear system dynamcis
Ad = eye(4) + Ts*Ac;
Bd = Ts*Bc;


% Generate U_abc voltage vectors (normalized to +-1/2)
U_set_size = 8;
U_set = zeros(3, 8);
for k = 1:U_set_size
   U_set(:, k) = [bitand(k-1, 1)~=0; bitand(k-1, 2)~=0; bitand(k-1, 4)~=0];
end
U_set = U_set-1/2;


end

