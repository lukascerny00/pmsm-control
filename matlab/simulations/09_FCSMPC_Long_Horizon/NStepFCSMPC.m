function [u_abc_opt_out, visited_nodes_out] = NStepFCSMPC(Ad, Bd, Cd, Px, px, U_set, x0, u_abc_prev, y_ref, Q, R, N)

% Get number of sysstem outputs
ny = size(Cd, 1);

% Get size of control set
U_set_size = size(U_set, 2);

% Find optimal control
J_opt = inf;

% Initialize stack used for storing vertices to visit
stack = MyStackClass();
x_all = zeros(4, N+1);
x_all(:, 1) = x0;
J_all = zeros(1, N+1);
J_all(1, 1) = 0;
u_abc_all = zeros(3, N+1);
u_abc_all(:, 1) = u_abc_prev;

% Add to stack
for u_index = 1:U_set_size
    stack_item = struct;
    stack_item.u_abc = U_set(:, u_index);
    stack_item.depth = 1;
    stack = stack.push(stack_item);
end

% Seek optimal solution
visited_nodes = 0;
while ~stack.isEmpty()
    
    % Update counter
    visited_nodes = visited_nodes + 1;
    
    % Get item from stack
    [stack_item, stack] = stack.pop();
    u_abc = stack_item.u_abc;
    depth = stack_item.depth;
    
    % Get previous voltage vector index
    u_abc_prev = u_abc_all(:, depth);
    u_abc_all(:, depth+1) = u_abc;
    
    % Get state
    x_cur = x_all(:, depth);

    % Predict state in next step
    x_next = Ad*x_cur + Bd*u_abc;
    x_all(:, depth + 1) = x_next;

    % Compute cost
    ref_indices = (ny*(depth-1)+1):(ny*depth);
    J_all(1, depth+1) = J_all(1, depth) + (y_ref(ref_indices, 1) - Cd*x_next)'*Q*(y_ref(ref_indices, 1) - Cd*x_next) + (u_abc-u_abc_prev)'*R*(u_abc-u_abc_prev);
    
    % Check constraints
    feasible = ~any(~(Px*x_next <= px));
    
    % Check that the cost function does not exceed the optimal value
    if and(J_all(1, depth+1) < J_opt, feasible)      
        if (depth == N)
            J_opt = J_all(1, depth+1);
            u_abc_opt = u_abc_all(:, 2);
        else
            for u_index = 1:U_set_size
                stack_item = struct;
                stack_item.u_abc = U_set(:, u_index);
                stack_item.depth = depth+1;
                stack = stack.push(stack_item);
            end
        end
    end    

end

% Return data
u_abc_opt_out = u_abc_opt;
visited_nodes_out = visited_nodes;


end


