npp = 2;
Rs = 0.45;
Ld = 0.0008;
Lq = 0.0009;
Kb = 0.028;
J = 1e-5;
B = 1e-6;
Vdc = 24;
Imax = 3.57*sqrt(2);
Ts = 1e-6;
Cd = eye(2, 4);
Q = eye(2);
R = eye(3);
N = 3;
x0 = [0; 0; 0; 0];
u_abc_prev =  [-0.5; -0.5; -0.5];
y_ref = [0; 1; 0; 1; 0; 1];

% Create discrete-time linear system matrices
[Ad, Bd, U_set] = CreateSystemMatricesFCSMPC(Rs, Ld, Lq, Kb, npp, J, B, Vdc, Ts, x0(3, 1), x0(4, 1));
%%
% Create constraints
[Px, px] = CreateConstraintsFCSMPC(Imax);

%%
% Create condensed matrices
[H, F_T, G, W, S] = CreateCondensedMatsFCSMPC(Ad, Bd, Cd, Q, R, Px, px, N)

%%
% Get optimal vector
[u_abc_opt] =  feval('NStepFCSMPC', Ad, Bd, Cd, Px, px, U_set, x0, u_abc_prev, y_ref, Q, R, N)

%%
% Apply voltage
u_abc = u_abc_opt;
