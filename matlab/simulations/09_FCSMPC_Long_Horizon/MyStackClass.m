classdef MyStackClass
% Simple implementation of stack data structure. This implementation is not
% an optimal one but it works.
%    
% Created by cernylu6
    properties
        array
    end
    methods
        function obj = MyStackClass()
            obj.array = [];
        end
        function r = push(obj, value)
            obj.array = [obj.array, value];
            r = obj;
        end
        function [value, r] = pop(obj)
            value = obj.array(end);
            obj.array = obj.array(1:end-1);
            r = obj;
        end
        function value = newestValue(obj)
            value = obj.array(end);
        end
        function value = isEmpty(obj)
            value = isempty(obj.array);
        end
   end
end