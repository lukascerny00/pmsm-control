%% Clear workspace
close all
clear all
clc


%% Set Motor Parameters

% Parameters
pmsm1.npp = 2;          % [-]
pmsm1.Rs = 0.45;        % [Ohm]
pmsm1.Ld = 0.0008;      % [H]
pmsm1.Lq = 0.0009;      % [H]
pmsm1.Kb = 0.023;       % [V/(rad/s)]
pmsm1.J = 0.000028;      % [kg*m^2]
pmsm1.B = 0.000013;      % [N*m/rad/s)]

% Initial Conditions
pmsm1.i_d0 = 0;         % [A]
pmsm1.i_q0 = 0;         % [A]
pmsm1.omega0 = 0;       % [rad/s]
pmsm1.theta0 = 0;       % [rad]


%% Set PWM parameters
pwm.DCVoltage = 24;     % [V]
pwm.period = 4e-5;      % [s]
pwm.Ts = 2e-8;          % [s]
pwm.ticks_per_period = pwm.period/pwm.Ts;


%% Set limit values
Imax = 3.67*sqrt(2); % [A]
Vmax = pwm.DCVoltage/sqrt(3);


%% Define PID controller for PMSM

% Sampling periods
Ts_ctrl_pos = 1e-2; % [s]
Ts_ctrl_vel = 1e-3; % [s]
Ts_ctrl_cur = 4e-5; % [s]

% Current loop tuned for control with modulators
Kp_id    = 25;
Ki_id    = 5/Ts_ctrl_cur;
Kp_iq    = 20.3;
Ki_iq    = 0.95/Ts_ctrl_cur;

% Velocity loop
Kp_omega = 1.2; 
Ki_omega = 0.04/Ts_ctrl_vel;
q_current_limit = 5; % [A]

% Position loop
Kp_theta = 50;
Ki_theta = 0/Ts_ctrl_pos;

