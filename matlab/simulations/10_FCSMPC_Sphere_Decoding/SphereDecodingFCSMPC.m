function [u_abc_opt_out, U_opt_out, visited_nodes_out, use_initial_guess] = SphereDecodingFCSMPC(H, F_T, G, W, S, Ad, Bd, Cd, Px, px, U_set, x0, u_abc_prev, U_opt_prev, y_ref, Q, R, N)


% Get number of system inputs and outputs
nu = size(Bd, 2);
ny = size(Cd, 1);

% V is lower triangular and it holds that V'*V = H 
invV = chol(inv(H), 'lower');
V = inv(invV);

% Unonstrained Solution
U_bar_uncon = -V*inv(H)*F_T'*[x0; u_abc_prev; y_ref];


% --- Suboptimal solution ---
% Try suboptimal solution
U_subopt_feasible = GetSuboptimalFCSMPC(Ad, Bd, Cd, Px, px, U_set, x0, u_abc_prev, y_ref, Q, R, N)
radius_subopt_feasible = ((U_bar_uncon - V*U_subopt_feasible)'*(U_bar_uncon - V*U_subopt_feasible))

% Check previous solution augmented by last step
U_subopt = [U_opt_prev(nu+1:end); U_opt_prev(end-nu+1:end)];
radius_subopt = ((U_bar_uncon - V*U_subopt)'*(U_bar_uncon - V*U_subopt));

% Check feasibility and compare to suboptimal
feasible = ~any(~(G*U_subopt <= W + S*x0));
if and(feasible, radius_subopt<radius_subopt_feasible)
    radius_ini = radius_subopt;
    U_ini = U_subopt;
else
    radius_ini = radius_subopt_feasible;
    U_ini = U_subopt_feasible;
end


% --- Optimal Solution ---
% Find optimal control
U_opt = U_ini;
J_opt = radius_ini;
radius = J_opt;

% Initialize stack used for storing vertices to visit
stack = MyStackClass();
U_all = zeros(nu*N, 1);
J_all = zeros(1, nu*N+1);
J_all(1, 1) = 0;

u_set_size = 2;
u_set = [-1/2, 1/2];
for u_index = 1:u_set_size
    stack_item = struct;
    stack_item.u = u_set(u_index);
    stack_item.depth = 1;
    stack = stack.push(stack_item);
end


% Seek optimal solution
visited_nodes = 0;
use_initial_guess = 1;
while ~stack.isEmpty()
    
    % Update counter
    visited_nodes = visited_nodes + 1;
    
    % Get item from stack
    [stack_item, stack] = stack.pop();
    u = stack_item.u;
    depth = stack_item.depth;
    
    U_all(depth, 1) = u;
    
    % Compute cost
    J_all(1, depth+1) = J_all(1, depth) + ((U_bar_uncon(depth, 1) - V(depth, 1:depth)*U_all(1:depth, 1))^2)
    
    % Check that the cost function does not exceed the optimal value
    if J_all(1, depth+1) < radius
        if (depth == nu*N)
            % Check constraints
            feasible = ~any(~(G*U_all <= W + S*x0));
            if feasible
                J_opt = J_all(1, depth+1);
                radius = J_opt;
                U_opt = U_all;
                use_initial_guess = 0;
            end
        else
            for u_index = 1:u_set_size
                stack_item = struct;
                stack_item.u = u_set(u_index);
                stack_item.depth = depth+1;
                stack = stack.push(stack_item);
            end
        end
    end    

end

u_abc_opt_out = U_opt(1:3, 1); 
U_opt_out = U_opt;
use_initial_guess
visited_nodes_out = visited_nodes;


end


