function [U_ini] = GetSuboptimalFCSMPC(Ad, Bd, Cd, Px, px, U_set, x0, u_abc_prev, y_ref, Q, R, N)

nu = size(Bd, 2);
ny = size(Cd, 1);

% Find suboptimal control
U_sub = zeros(nu*N, 1);

u_abc = u_abc_prev
x = x0;
for k = 1:N
    indices = (ny*(k-1)+1):(ny*k)
    u_abc_next = OneStepFCSMPC(Ad, Bd, Cd, Px, px, U_set, x, u_abc, y_ref(indices, 1), Q, R);
    x_next = Ad*x + Bd*u_abc_next;
    u_abc = u_abc_next;
    x = x_next;
    indices = (nu*(k-1)+1):(nu*k)
    U_sub(indices, 1) = u_abc;
end

U_ini = U_sub;

end

