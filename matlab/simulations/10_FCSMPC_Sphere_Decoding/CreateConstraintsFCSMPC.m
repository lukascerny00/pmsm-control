function [Px, px] = CreateConstraints(Imax)
% Create current constraints - approximated by octagon

Px = [         +1, +sqrt(2)-1, 0, 0;
               +1, -sqrt(2)+1, 0, 0;
               -1, +sqrt(2)-1, 0, 0;
               -1, -sqrt(2)+1, 0, 0;
       +sqrt(2)-1,         +1, 0, 0;
       +sqrt(2)-1,         -1, 0, 0;
       -sqrt(2)+1,         +1, 0, 0;
       -sqrt(2)+1,         -1, 0, 0];
px = [Imax; Imax; Imax; Imax; Imax; Imax; Imax; Imax];

end

