// Define parameters
diam_BoltHead1 = 8.8; // ----
diam_Bolt1 = 6.0;
Bolt1_diam = 6.0;
len_BoltHead1  = 6.0; // ----
len_Bolt1  = 100.0;

Bolt2_diam = 6.0;
Bolt2_len = 25;

diam_Nut1 = 9.0; // ---
len_Nut1 = 6.0; //  ----

len_Motor1_l1 = 100.0; // ----
len_Motor1_l2 = 1.52;
diam_Motor1_d1 = 86.36;
Motor1_diam1 = 86.36;
diam_Motor1_d2 = 50;
diam_Motor1_d3 = 9;

len_Motor2_l1 = 80.0; // ----
len_Motor2_l2 = 2.0;
diam_Motor2_d1 = 57;
diam_Motor2_d2 = 25;
diam_Motor2_d3 = 8;

// Select what to render
RenderAssembled = true;
RenderBracket1 = false;
RenderBracket2 = false;
RenderBracket3 = false;
RenderMotor2Bracket1 = false;
RenderMotor2Bracket2 = false;
RenderMotor2Bracket3 = false;
RenderMotor2Bracket4 = false;
RenderMotor2Bracket5 = false;

// Global resolution
$fs = 0.1;  // Don't generate smaller facets than 0.1 mm
$fa = 1;    // Don't generate larger angles than 5 degrees

// Render
if (RenderAssembled) assembled();
if (RenderBracket1) bracket1();
if (RenderBracket2) translate([0, 0, Motor1_diam1/2+10]) rotate([180, 0, 0]) bracket2();
if (RenderBracket3) rotate([90, 0, 0]) bracket3();
if (RenderMotor2Bracket1) Motor2Bracket1();
if (RenderMotor2Bracket2) Motor2Bracket2();
if (RenderMotor2Bracket3)
    translate([0, 0, Motor1_diam1/2+10])
        rotate([180, 0, 0])
            Motor2Bracket3();
if (RenderMotor2Bracket4)
    translate([0, 0, Motor1_diam1/2+10])
        rotate([180, 0, 0])
            Motor2Bracket4();
if (RenderMotor2Bracket5) rotate([90, 0, 0]) Motor2Bracket5();

// Compound objects
module assembled() {
    /*
    translate([0, len_Motor1_l1+30+5, 0])
        rotate([90, 0, 0]) 
            motor1();
    translate([-Motor1_diam1/2-3*14, 35, -Motor1_diam1/2-10-1])
        bracket1();
    translate([-Motor1_diam1/2-14, 35, 1])
        bracket2();
    translate([-Motor1_diam1/2-3*14, 35+70, -Motor1_diam1/2-10-1])
        bracket1();
    translate([-Motor1_diam1/2-14, 35+70, 1])
        bracket2();
    translate([-65/sqrt(2)/2 - 14/2, 35-7-2, -Motor1_diam1/2-10]) 
        bracket3();
    */
    
    
    translate([0, -len_Motor2_l1-30-5, 0])
        rotate([-90, 0, 0]) 
            motor2();
    translate([-diam_Motor2_d1/2-3*14, -35-14, -Motor1_diam1/2-10-1])
        Motor2Bracket1();
    translate([-diam_Motor2_d1/2-14, -35-14, 1])
        Motor2Bracket3();
    translate([-38.88/sqrt(2)/2 - 14/2, -35+2, -diam_Motor2_d1/2-10]) 
        Motor2Bracket5Base();
    
    
    //translate([0, 0, 20]) bolt1();
    //translate([0, 0, 10]) nut1();
}


// Brackets for motor 1
module bracket1() {
    x1 = 14;
    x2 = 14;
    y1 = 14;
    z1 = 10;
    
    screw1_diam = 5.5;
    nut_hole_height = 7;
    nut_hole_diam = 2*10.1/(2*sin(60));
    nut_hole_height2 = 6; 
    
    difference() {
        // Base
        cube([4*x1 + 2*x2 + Motor1_diam1, y1, z1 + Motor1_diam1/2], center=false);
        // Remove motor space
        translate([2*x1 + x2 + Motor1_diam1/2, 0, z1+Motor1_diam1/2]) rotate([-90, 0, 0]) cylinder(h = y1, r = Motor1_diam1/2, center = false);
        // Right side
        translate([0, 0, z1]) cube([x1, y1, Motor1_diam1/2], center=false);
        translate([x1, 0, z1+x2]) cube([x2, y1, Motor1_diam1/2-x2], center=false);
        translate([x1, 0, z1+x2]) rotate([-90, 0, 0]) cylinder(h = y1, r = x2, center = false);
        translate([x1/2, y1/2, 0]) cylinder(h = y1, r = screw1_diam/2, center = false);
        translate([x1 + x1/2 + x2, y1/2, 0]) cylinder(h = y1 + Motor1_diam1/2, r = Bolt1_diam/2, center = false);
        translate([x1 + x1/2 + x2, y1/2, 0]) hexagone(h=nut_hole_height, r=nut_hole_diam/2, center=false);
        translate([x1 + x1/2 + x2, y1/2, nut_hole_height]) hexagone_based_pyramid(h = nut_hole_height2, r = nut_hole_diam/2);
        translate([2*x1 + x2 + Motor1_diam1/2-65/sqrt(2)/2, 0, Bolt2_diam/2 + 4]) rotate([-90, 0, 0]) cylinder(h = y1, r = Bolt2_diam/2, center = false);
        // Left side
        translate([3*x1 + 2*x2 + Motor1_diam1, 0, z1]) cube([x1, y1, Motor1_diam1/2], center=false);
        translate([3*x1 + 1*x2 + Motor1_diam1, 0, z1+x2]) cube([x2, y1, Motor1_diam1/2-x2], center=false);
        translate([3*x1 + 2*x2  + Motor1_diam1, 0, z1+x2]) rotate([-90, 0, 0]) cylinder(h = y1, r = x2, center = false);
        translate([7/2*x1 + 2*x2  + Motor1_diam1, y1/2, 0]) cylinder(h = y1, r = screw1_diam/2, center = false);
        translate([2*x1 + x1/2 + x2 + Motor1_diam1, y1/2, 0]) cylinder(h = y1 + Motor1_diam1/2, r = Bolt1_diam/2, center = false);
        translate([2*x1 + x1/2 + x2+ Motor1_diam1, y1/2, 0]) hexagone(h=nut_hole_height, r=nut_hole_diam/2, center=false);
        translate([2*x1 + x1/2 + x2 + Motor1_diam1, y1/2, nut_hole_height]) hexagone_based_pyramid(h = nut_hole_height2, r = nut_hole_diam/2);
        translate([2*x1 + x2 + Motor1_diam1/2+65/sqrt(2)/2, 0, Bolt2_diam/2 + 4]) rotate([-90, 0, 0]) cylinder(h = y1, r = Bolt2_diam/2, center = false);
    }
}

module bracket2() {
    x1 = 14;
    y1 = 14;
    z1 = 10;
    e = 0.001;
    c = 1.5;
    difference() {
    union() {
    difference() {
        // Base
        cube([2*x1 + Motor1_diam1, y1, z1 + Motor1_diam1/2], center=false);
        // Remove motor space
        translate([x1 + Motor1_diam1/2, 0, 0]) rotate([-90, 0, 0]) cylinder(h = y1+e, r = Motor1_diam1/2, center = false);
        // Holes
        translate([x1/2, y1/2, 0]) cylinder(h = z1 + Motor1_diam1/2, r = Bolt1_diam/2, center = false);
        translate([x1 + x1/2 + Motor1_diam1, y1/2, 0]) cylinder(h = z1 + Motor1_diam1/2, r = Bolt1_diam/2, center = false);
        translate([x1/2, y1/2, z1 + Motor1_diam1/2-5]) cylinder(h = 5, r = 5, center = false);
        translate([x1 + x1/2 + Motor1_diam1, y1/2, z1 + Motor1_diam1/2-5]) cylinder(h = 5, r = 5, center = false);
        translate([x1 + Motor1_diam1/2-65/sqrt(2)/2, 0, Motor1_diam1/2+Bolt2_diam/2]) rotate([-90, 0, 0]) cylinder(h = y1, r = Bolt2_diam/2, center = false);
        translate([x1 + Motor1_diam1/2+65/sqrt(2)/2, 0, Motor1_diam1/2+Bolt2_diam/2]) rotate([-90, 0, 0]) cylinder(h = y1, r = Bolt2_diam/2, center = false);
    }
        translate ([0, 0, -c]) cube([x1, y1, c], center=false);
        translate ([x1+ Motor1_diam1, 0, -c]) cube([x1, y1, c], center=false);
            
        }
        translate([x1/2, y1/2, -c]) cylinder(h = z1 + Motor1_diam1/2, r = Bolt1_diam/2, center = false);
        translate([x1 + x1/2 + Motor1_diam1, y1/2, -c]) cylinder(h = z1 + Motor1_diam1/2, r = Bolt1_diam/2, center = false);
    }
    
}

module bracket3() {
    x1 = 65/sqrt(2) + 14;
    x2 = 14;
    x3 = 65/sqrt(2) - 14;
    y1 = 7;
    z1 = 4;
    z2 = 6;
    z3 = (Motor1_diam1 - 65/sqrt(2))/2;
    z4 = 65/sqrt(2);
    crad = 4;
    e = 0.001;
    c = 1; // Correction
    
    difference() {
        // Base
        cube([x1, y1, 2*z1 + 2*z2 + 2*z3 + z4 + c], center=false);
        // Hole1
        translate([x2, 0, 0])
            cube([x3, y1, z1 + z2 + z3 - 14/2 - crad], center=false);
        translate([x2+crad, 0, 0])
            cube([x3 - 2*crad, y1, z1 + z2 + z3 - 14/2], center=false);
        translate([x2+crad, 0, z1 + z2 + z3 - 14/2 - crad])
            rotate([-90, 0, 0])
                cylinder(h = y1+e, r = crad, center=false);
        translate([x1-x2-crad, 0, z1 + z2 + z3 - 14/2 - crad])
            rotate([-90, 0, 0])
                cylinder(h = y1+e, r = crad, center=false);
        // Hole2
        translate([x2, 0, z1 + z2 + z3 + z4 + 14/2 + crad])
            cube([x3, y1, z1 + z2 + z3 - 14/2 - crad + c], center=false);
        translate([x2+crad, 0, z1 + z2 + z3 + z4 + 14/2])
            cube([x3 - 2*crad, y1, z1 + z2 + z3 - 14/2+c], center=false);
        translate([x2+crad, 0, z1 + z2 + z3 + z4 + 14/2 + crad])
            rotate([-90, 0, 0])
                cylinder(h = y1+e, r = crad, center=false);
        translate([x1-x2-crad, 0, z1 + z2 + z3 + z4 + 14/2 + crad])
            rotate([-90, 0, 0])
                cylinder(h = y1+e, r = crad, center=false);
        // Hole 3
        translate([x2 + crad, 0, z1 + z2 + z3 + 14/2])
            cube([x3-2*crad, y1, x3], center=false);
        translate([x2, 0, z1 + z2 + z3 + 14/2 + crad])
            cube([x3, y1, x3 - 2*crad], center=false);
        translate([x2 + crad, 0, z1 + z2 + z3 + 14/2 + crad])
            rotate([-90, 0, 0])
                cylinder(h = y1, r = crad, center=false);
        translate([x2 + x3 - crad, 0, z1 + z2 + z3 + 14/2 + crad])
            rotate([-90, 0, 0])
                cylinder(h = y1, r = crad, center=false);
        translate([x2 + crad, 0, z1 + z2 + z3 + z4 - 14/2 - crad])
            rotate([-90, 0, 0])
                cylinder(h = y1, r = crad, center=false);
        translate([x2 + x3 - crad, 0, z1 + z2 + z3 + z4 - 14/2 - crad])
            rotate([-90, 0, 0])
                cylinder(h = y1, r = crad, center=false);
        // Bolt Holes
        translate([x2/2, 0, z1+z2/2])
            rotate([-90, 0, 0])
                cylinder(h = y1+e, r = Bolt2_diam/2, center = false);
        translate([x1-x2/2, 0, z1+z2/2])
            rotate([-90, 0, 0])
                cylinder(h = y1+e, r = Bolt2_diam/2, center = false);
        translate([x2/2, 0, z1+z2+z3])
            rotate([-90, 0, 0])
                cylinder(h = y1+e, r = Bolt2_diam/2, center = false);
        translate([x1-x2/2, 0, z1+z2+z3])
            rotate([-90, 0, 0])
                cylinder(h = y1+e, r = Bolt2_diam/2, center = false);
        translate([x2/2, 0, z1+z2+z3+z4])
            rotate([-90, 0, 0])
                cylinder(h = y1+e, r = Bolt2_diam/2, center = false);
        translate([x1-x2/2, 0, z1+z2+z3+z4])
            rotate([-90, 0, 0])
                cylinder(h = y1+e, r = Bolt2_diam/2, center = false);
        translate([x2/2, 0, z1+3/2*z2+2*z3+z4+c])
            rotate([-90, 0, 0])
                cylinder(h = y1+e, r = Bolt2_diam/2, center = false);
        translate([x1-x2/2, 0, z1+3/2*z2+2*z3+z4+c])
            rotate([-90, 0, 0])
                cylinder(h = y1+e, r = Bolt2_diam/2, center = false);
        // Hole for motor ring
        translate([x1/2, y1-len_Motor1_l2 , z1+z2+z3+z4/2])
            rotate([-90, 0, 0])
                cylinder(h=len_Motor1_l2+e, r=diam_Motor1_d2/2 + 0.3, center=false);
    }
    
}


// Brackets for motor 2
module Motor2Bracket1() {
    
    // Given dimensions
    e = 0.001;                  // Small expansion to ensure some holes are throughout
    ec = 0.4/2 + 0.1;           // Small expansion of holes corresponding to print core radius
    
    Motor1Diam1 = 86.36;        // Diameter of motor 1
    Motor2Diam1 = 57;           // Diameter of motor 2
    Motor2Diam2 = 25;           // Diameter of cylinder around shaft of motor 2
    Motor2Diam3 = 38.88;        // Diameter of circle on which bolt holes of motor 2 lay 
    Motor2Diam4 = 55;           // Diameter of motor 2 in the middle
    
    Bolt1Diam = 6;              // Diameter of bolt that holds bracket 1 and 2 together
    Bolt1len = 90;              // Length of bolt that holds bracket 1 and 2 together
    Bolt1HeadDiam = 8.8;        // Diameter of head of bolt that holds bracket 1 and 2 together
    Bolt1HeadLen = 6;           // Length of head of bolt that holds bracket 1 and 2 together
    
    Bolt2Diam = 6;              // Diameter of bolt that holds bracket 1 and 3 together
    
    Bolt3Diam = 4;              // Diameter of bolt that holds bracket 3 and motor 2 together
    
    ScrewHoleDiam = 5.0 + 0.5;  // Screw diameter + 0.5 mm 
    
    NutHoleHeight = 7;          // Height of hole for nut
    NutHoleHeightExp = 6;       // Pyramide expansion of height of hole for nut
    NutHoleDiam = 12;           // Diameter of hexagone hole for nut (cca 10/sin(60) + 2*ec)
    
    
    // Define dimensions of the bracket
    MotorHoleDiam = Motor2Diam1 + 2*ec;
    x1 = 14;
    x2 = 14;
    x3 = 14;
    x4 = MotorHoleDiam;
    x5 = 14;
    x6 = 14;
    x7 = 14;
    y1 = 14;
    z1 = 10;
    z2 = (Motor1Diam1 - Motor2Diam1)/2;
    z3 = MotorHoleDiam/2;
    
    // Define the bracket
    difference() {
        // Base
        cube([x1 + x2 + x3 + x4 + x5 + x6 + x7, y1, z1 + z2 + z3], center=false);
        // Remove motor space
        translate([x1 + x2 + x3 + MotorHoleDiam/2, -e, z1 + z2 + MotorHoleDiam/2])
            rotate([-90, 0, 0])
                cylinder(h = y1 + 2*e, r = MotorHoleDiam/2, center = false);
        // Remove useless material on the right side
        translate([0, 0, z1])
            cube([x1, y1, z2 + z3], center=false);
        translate([x1, 0, z1 + x2])
            cube([x2, y1, z2 + z3], center=false);
        translate([x1, -e, z1 + x2])
            rotate([-90, 0, 0])
                cylinder(h = y1 + 2*e, r = x2, center = false);
        // Remove useless material on the left side
        translate([x1 + x2 + x3 + x4 + x5 + x6, 0, z1])
            cube([x1, y1, z2 + z3], center=false);
        translate([x1 + x2 + x3 + x4 + x5, 0, z1 + x2])
            cube([x2, y1, z2 + z3], center=false);
        translate([x1 + x2 + x3 + x4 + x5 + x6, -e, z1 + x2])
            rotate([-90, 0, 0])
                cylinder(h = y1 + 2*e, r = x2, center = false);
        // Create hole for right bolt and nut
        translate([x1 + x2 + x3/2, y1/2, -e])
            cylinder(h = z1 + z2 + z3 + 2*e, r = Bolt1Diam/2 + ec, center = false);
        translate([x1 + x2 + x3/2, y1/2, 0])
            hexagone(h = NutHoleHeight, r = NutHoleDiam/2, center=false);
        translate([x1 + x2 + x3/2, y1/2, NutHoleHeight])
            hexagone_based_pyramid(h = NutHoleHeightExp, r = NutHoleDiam/2);
        // Create hole for left bolt and nut
        translate([x1 + x2 + x3 + x4 + x5/2, y1/2, -e])
            cylinder(h = z1 + z2 + z3 + 2*e, r = Bolt1Diam/2 + ec, center = false);
        translate([x1 + x2 + + x3 + x4 + x5/2, y1/2, 0])
            hexagone(h = NutHoleHeight, r = NutHoleDiam/2, center=false);
        translate([x1 + x2 + + x3 + x4 + x5/2, y1/2, NutHoleHeight])
            hexagone_based_pyramid(h = NutHoleHeightExp, r = NutHoleDiam/2);
        // Create holes for bolts holding the motor
        translate([x1 + x2 + x3 + x4/2 - Motor2Diam3/sqrt(2)/2, -e, z1 + z2 - Bolt2Diam/2])
            rotate([-90, 0, 0])
                cylinder(h = y1 + 2*e, r = Bolt2Diam/2, center = false);
        translate([x1 + x2 + x3 + x4/2 + Motor2Diam3/sqrt(2)/2, -e, z1 + z2 - Bolt2Diam/2])
            rotate([-90, 0, 0])
                cylinder(h = y1 + 2*e, r = Bolt2Diam/2, center = false);
        // Create holes for screws
        translate([x1/2, y1/2, -e])
            cylinder(h = y1 + 2*e, r = ScrewHoleDiam/2, center = false);
        translate([x1 + x2 + x3 + x4 + x5 + x6 + x7/2, y1/2, -e])
            cylinder(h = y1 + 2*e, r = ScrewHoleDiam/2, center = false);
    }
    
}

module Motor2Bracket2() {
    
    // Given dimensions
    e = 0.001;                  // Small expansion to ensure some holes are throughout
    ec = 0.4/2 + 0.1;           // Small expansion of holes corresponding to print core radius
    
    Motor1Diam1 = 86.36;        // Diameter of motor 1
    Motor2Diam1 = 57;           // Diameter of motor 2
    Motor2Diam2 = 25;           // Diameter of cylinder around shaft of motor 2
    Motor2Diam3 = 38.88;        // Diameter of circle on which bolt holes of motor 2 lay 
    Motor2Diam4 = 55;           // Diameter of motor 2 in the middle
    Motor2len1 =  9;            // Length of the rear part of motor 2
    
    Bolt1Diam = 6;              // Diameter of bolt that holds bracket 1 and 2 together
    Bolt1len = 90;              // Length of bolt that holds bracket 1 and 2 together
    Bolt1HeadDiam = 8.8;        // Diameter of head of bolt that holds bracket 1 and 2 together
    Bolt1HeadLen = 6;           // Length of head of bolt that holds bracket 1 and 2 together
    
    Bolt2Diam = 6;              // Diameter of bolt that holds bracket 1 and 3 together
    
    Bolt3Diam = 4;              // Diameter of bolt that holds bracket 3 and motor 2 together
    
    ScrewHoleDiam = 5.0 + 0.5;  // Screw diameter + 0.5 mm 
    
    NutHoleHeight = 7;          // Height of hole for nut
    NutHoleHeightExp = 6;       // Pyramide expansion of height of hole for nut
    NutHoleDiam = 12;           // Diameter of hexagone hole for nut (cca 10/sin(60) + 2*ec)
    
    
    // Define dimensions of the bracket
    MotorHoleDiam = Motor2Diam1 + 2*ec;
    MotorHoleDiam2 = Motor2Diam4 + 2*ec;
    x1 = 14;
    x2 = 14;
    x3 = 14;
    x4 = MotorHoleDiam;
    x5 = 14;
    x6 = 14;
    x7 = 14;
    y1 = 14;
    z1 = 10;
    z2 = (Motor1Diam1 - Motor2Diam1)/2;
    z3 = MotorHoleDiam/2;
    
    // Define the bracket
    difference() {
        // Base
        cube([x1 + x2 + x3 + x4 + x5 + x6 + x7, y1, z1 + z2 + z3], center=false);
        // Remove motor space
        translate([x1 + x2 + x3 + MotorHoleDiam/2, -e, z1 + z2 + MotorHoleDiam/2])
            rotate([-90, 0, 0])
                cylinder(h = Motor2len1 + e, r = MotorHoleDiam/2, center = false);
        translate([x1 + x2 + x3 + MotorHoleDiam/2, Motor2len1, z1 + z2 + MotorHoleDiam/2])
            rotate([-90, 0, 0])
                cylinder(h = y1 - Motor2len1 + e, r = MotorHoleDiam2/2, center = false);
        // Remove useless material on the right side
        translate([0, 0, z1])
            cube([x1, y1, z2 + z3], center=false);
        translate([x1, 0, z1 + x2])
            cube([x2, y1, z2 + z3], center=false);
        translate([x1, -e, z1 + x2])
            rotate([-90, 0, 0])
                cylinder(h = y1 + 2*e, r = x2, center = false);
        // Remove useless material on the left side
        translate([x1 + x2 + x3 + x4 + x5 + x6, 0, z1])
            cube([x1, y1, z2 + z3], center=false);
        translate([x1 + x2 + x3 + x4 + x5, 0, z1 + x2])
            cube([x2, y1, z2 + z3], center=false);
        translate([x1 + x2 + x3 + x4 + x5 + x6, -e, z1 + x2])
            rotate([-90, 0, 0])
                cylinder(h = y1 + 2*e, r = x2, center = false);
        // Create hole for right bolt and nut
        translate([x1 + x2 + x3/2, y1/2, -e])
            cylinder(h = z1 + z2 + z3 + 2*e, r = Bolt1Diam/2 + ec, center = false);
        translate([x1 + x2 + x3/2, y1/2, 0])
            hexagone(h = NutHoleHeight, r = NutHoleDiam/2, center=false);
        translate([x1 + x2 + x3/2, y1/2, NutHoleHeight])
            hexagone_based_pyramid(h = NutHoleHeightExp, r = NutHoleDiam/2);
        // Create hole for left bolt and nut
        translate([x1 + x2 + x3 + x4 + x5/2, y1/2, -e])
            cylinder(h = z1 + z2 + z3 + 2*e, r = Bolt1Diam/2 + ec, center = false);
        translate([x1 + x2 + + x3 + x4 + x5/2, y1/2, 0])
            hexagone(h = NutHoleHeight, r = NutHoleDiam/2, center=false);
        translate([x1 + x2 + + x3 + x4 + x5/2, y1/2, NutHoleHeight])
            hexagone_based_pyramid(h = NutHoleHeightExp, r = NutHoleDiam/2);
        // Create holes for bolts holding the motor
        translate([x1 + x2 + x3 + x4/2 - Motor2Diam3/sqrt(2)/2, -e, z1 + z2 - Bolt2Diam/2])
            rotate([-90, 0, 0])
                cylinder(h = y1 + 2*e, r = Bolt2Diam/2, center = false);
        translate([x1 + x2 + x3 + x4/2 + Motor2Diam3/sqrt(2)/2, -e, z1 + z2 - Bolt2Diam/2])
            rotate([-90, 0, 0])
                cylinder(h = y1 + 2*e, r = Bolt2Diam/2, center = false);
        // Create holes for screws
        translate([x1/2, y1/2, -e])
            cylinder(h = y1 + 2*e, r = ScrewHoleDiam/2, center = false);
        translate([x1 + x2 + x3 + x4 + x5 + x6 + x7/2, y1/2, -e])
            cylinder(h = y1 + 2*e, r = ScrewHoleDiam/2, center = false);
    }
    
}

module Motor2Bracket3() {
    
    // Given dimensions
    e = 0.001;                  // Small expansion to ensure some holes are throughout
    ec = 0.4/2 + 0.1;           // Small expansion of holes corresponding to print core radius
    
    Motor1Diam1 = 86.36;        // Diameter of motor 1
    Motor2Diam1 = 57;           // Diameter of motor 2
    Motor2Diam2 = 25;           // Diameter of cylinder around shaft of motor 2
    Motor2Diam3 = 38.88;        // Diameter of circle on which bolt holes of motor 2 lay 
    Motor2Diam4 = 55;           // Diameter of motor 2 in the middle
    
    Bolt1Diam = 6;              // Diameter of bolt that holds bracket 1 and 2 together
    Bolt1len = 90;              // Length of bolt that holds bracket 1 and 2 together
    Bolt1HeadDiam = 10;         // Diameter of head of bolt that holds bracket 1 and 2 together
    Bolt1HeadLen = 6;           // Length of head of bolt that holds bracket 1 and 2 together
    
    Bolt2Diam = 6;              // Diameter of bolt that holds bracket 1 and 3 together
    
    Bolt3Diam = 4;              // Diameter of bolt that holds bracket 3 and motor 2 together
    NutHoleDiam = 12;           // Diameter of hexagone hole for nut (cca 10/sin(60) + 2*ec)
    
    // Define dimensions of the bracket
    MotorHoleDiam = Motor2Diam1 + 2*ec;
    x1 = 14;
    x2 = MotorHoleDiam;
    x3 = 14;
    y1 = 14;
    z1 = MotorHoleDiam/2;
    z2 = 10 + 4;
    
    difference() {
        // Base
        cube([x1 + x2 + x3, y1, z1 + z2], center=false);
        // Remove motor space
        translate([x1 + x2/2, -e, 0])
            rotate([-90, 0, 0])
                cylinder(h = y1 + 2*e, r = x2/2, center = false);
        // Holes
        translate([x1/2, y1/2, 0])
            cylinder(h = z1 + z2, r = Bolt1Diam/2 + ec, center = false);
        translate([x1 + x2 + x3/2, y1/2, 0])
            cylinder(h = z1 + z2, r = Bolt1Diam/2 + ec, center = false);
        translate([x1/2, y1/2, z1 + z2 - Bolt1HeadLen])
            cylinder(h = Bolt1HeadLen, r = Bolt1HeadDiam/2 + ec, center = false);
        translate([x1 + x2 + x3/2, y1/2, z1 + z2 - Bolt1HeadLen])
            cylinder(h = Bolt1HeadLen, r = Bolt1HeadDiam/2 + ec, center = false);
        translate([x1 + x2/2 - Motor2Diam3/sqrt(2)/2, 0, z1 + Bolt2Diam/2])
            rotate([-90, 0, 0])
                cylinder(h = y1, r = Bolt2Diam/2, center = false);
        translate([x1 + x2/2 + Motor2Diam3/sqrt(2)/2, 0, z1 + Bolt2Diam/2])
            rotate([-90, 0, 0])
                cylinder(h = y1, r = Bolt2Diam/2, center = false);
    }
    
}


module Motor2Bracket4() {
    
    // Given dimensions
    e = 0.001;                  // Small expansion to ensure some holes are throughout
    ec = 0.4/2 + 0.1;           // Small expansion of holes corresponding to print core radius
    
    Motor1Diam1 = 86.36;        // Diameter of motor 1
    Motor2Diam1 = 57;           // Diameter of motor 2
    Motor2Diam2 = 25;           // Diameter of cylinder around shaft of motor 2
    Motor2Diam3 = 38.88;        // Diameter of circle on which bolt holes of motor 2 lay 
    Motor2Diam4 = 55;           // Diameter of motor 2 in the middle
    Motor2len1 =  9;            // Length of the rear part of motor 2
    
    Bolt1Diam = 6;              // Diameter of bolt that holds bracket 1 and 2 together
    Bolt1len = 90;              // Length of bolt that holds bracket 1 and 2 together
    Bolt1HeadDiam = 10;         // Diameter of head of bolt that holds bracket 1 and 2 together
    Bolt1HeadLen = 6;           // Length of head of bolt that holds bracket 1 and 2 together
    
    Bolt2Diam = 6;              // Diameter of bolt that holds bracket 1 and 3 together
    
    Bolt3Diam = 4;              // Diameter of bolt that holds bracket 3 and motor 2 together
    NutHoleDiam = 12;           // Diameter of hexagone hole for nut (cca 10/sin(60) + 2*ec)
    
    // Define dimensions of the bracket
    MotorHoleDiam = Motor2Diam1 + 2*ec;
    MotorHoleDiam2 = Motor2Diam4 + 2*ec;
    x1 = 14;
    x2 = MotorHoleDiam;
    x3 = 14;
    y1 = 14;
    z1 = MotorHoleDiam/2;
    z2 = 10 + 4;
    
    difference() {
        // Base
        cube([x1 + x2 + x3, y1, z1 + z2], center=false);
        // Remove motor space
        translate([x1 + x2/2, -e, 0])
            rotate([-90, 0, 0])
                cylinder(h = Motor2len1 + e, r = MotorHoleDiam/2, center = false);
        translate([x1 + x2/2, Motor2len1, 0])
            rotate([-90, 0, 0])
                cylinder(h = y1 - Motor2len1 + e, r = MotorHoleDiam2/2, center = false);
        // Holes
        translate([x1/2, y1/2, 0])
            cylinder(h = z1 + z2, r = Bolt1Diam/2 + ec, center = false);
        translate([x1 + x2 + x3/2, y1/2, 0])
            cylinder(h = z1 + z2, r = Bolt1Diam/2 + ec, center = false);
        translate([x1/2, y1/2, z1 + z2 - Bolt1HeadLen])
            cylinder(h = Bolt1HeadLen, r = Bolt1HeadDiam/2 + ec, center = false);
        translate([x1 + x2 + x3/2, y1/2, z1 + z2 - Bolt1HeadLen])
            cylinder(h = Bolt1HeadLen, r = Bolt1HeadDiam/2 + ec, center = false);
        translate([x1 + x2/2 - Motor2Diam3/sqrt(2)/2, 0, z1 + Bolt2Diam/2])
            rotate([-90, 0, 0])
                cylinder(h = y1, r = Bolt2Diam/2, center = false);
        translate([x1 + x2/2 + Motor2Diam3/sqrt(2)/2, 0, z1 + Bolt2Diam/2])
            rotate([-90, 0, 0])
                cylinder(h = y1, r = Bolt2Diam/2, center = false);
    }
    
}


module Motor2Bracket5() {
    
    // Given dimensions
    e = 0.001;                  // Small expansion to ensure some holes are throughout
    ec = 0.4/2 + 0.1;           // Small expansion of holes corresponding to print core radius
    
    Motor1Diam1 = 86.36;        // Diameter of motor 1
    Motor2Diam1 = 57;           // Diameter of motor 2
    Motor2Diam2 = 25;           // Diameter of cylinder around shaft of motor 2
    Motor2Diam3 = 38.88;        // Diameter of circle on which bolt holes of motor 2 lay 
    Motor2Diam4 = 55;           // Diameter of motor 2 in the middle
    Motor2len1 =  9;            // Length of the rear part of motor 2
    
    Bolt1Diam = 6;              // Diameter of bolt that holds bracket 1 and 2 together
    Bolt1len = 90;              // Length of bolt that holds bracket 1 and 2 together
    Bolt1HeadDiam = 10;         // Diameter of head of bolt that holds bracket 1 and 2 together
    Bolt1HeadLen = 6;           // Length of head of bolt that holds bracket 1 and 2 together
    
    Bolt2Diam = 6;              // Diameter of bolt that holds bracket 1 and 3 together
    
    Bolt3Diam = 4;              // Diameter of bolt that holds bracket 3 and motor 2 together
    NutHoleDiam = 12;           // Diameter of hexagone hole for nut (cca 10/sin(60) + 2*ec)
    
    // Define dimensions of the bracket
    MotorHoleDiam = Motor2Diam1 + 2*ec;
    MotorHoleDiam2 = Motor2Diam4 + 2*ec;
    MotorHoleDiam3 = Motor2Diam2 + 2*ec;
    crad = 4;
    mwidth = 8;
    mwc = 14-mwidth;
    x_total = Motor2Diam3/sqrt(2) + 14;
    x1 = 14;
    x2 = crad;
    x3 = x_total - 2*14 - 2*crad;
    x4 = crad;
    x5 = 14;
    y1 = 7;
    z_total = MotorHoleDiam + 2*10;
    z1 = 4;
    z2 = 6;
    z3 = (z_total - 2*10 - Motor2Diam3/sqrt(2))/2;
    z4 = Motor2Diam3/sqrt(2);
    z5 = (z_total - 2*10 - Motor2Diam3/sqrt(2))/2;
    z6 = 6;
    z7 = 4;
    
    difference() {
        union() {
            difference() {
                // Base
                cube([x1 + x2 + x3 + x4 + x5, y1, z1 + z2 + z3 + z4 + z5 + z6 + z7], center=false);
                //Holes
                lowerhole();
                upperhole();
                middlehole();
                boltholes();
                
            }
            brake();
        }
        // Hole for motor ring
        translate([x1 + x2 + x3/2, -e, z1+z2+z3+z4/2])
            rotate([-90, 0, 0])
                cylinder(h=len_Motor2_l2+e, r=Motor2Diam2/2 + 0.3, center=false);
        // Enlarge small holes
        translate([x1 + x2, 0, z1 + z2 + z3 + mwidth/2 - 1])
            cube([x3, y1, 1], center=false);
        translate([x1 + x2, -e, z1 + z2 + z3 + mwidth/2 - 0.5])
            rotate([-90, 0, 0])
                cylinder(h = y1+2*e, r = 0.5, center = false);
        translate([x1 + x2 + x3, -e, z1 + z2 + z3 + mwidth/2 - 0.5])
            rotate([-90, 0, 0])
                cylinder(h = y1+2*e, r = 0.5, center = false);
        translate([x1 + x2, 0, z1 + z2 + z3 + z4 - mwidth/2])
            cube([x3, y1, 1], center=false);
        translate([x1 + x2, -e, z1 + z2 + z3 + z4 - mwidth/2 + 0.5])
            rotate([-90, 0, 0])
                cylinder(h = y1+2*e, r = 0.5, center = false);
        translate([x1 + x2 + x3, -e, z1 + z2 + z3 + z4 - mwidth/2 + 0.5])
            rotate([-90, 0, 0])
                cylinder(h = y1+2*e, r = 0.5, center = false);
    }
    
    module lowerhole() {
        translate([x1 + x2, 0, 0])
            cube([x3, y1, z1 + z2 + z3 - mwidth/2], center=false);
        translate([x1, 0, 0])
            cube([x2 + x3 + x4, y1, z1 + z2 + z3 - mwidth/2 - crad], center=false);
        translate([x1 + x2, -e, z1 + z2 + z3 - mwidth/2 - crad])
            rotate([-90, 0, 0])
                cylinder(h = y1 + 2*e, r = crad, center=false);
        translate([x1 + x2 + x3, -e, z1 + z2 + z3 - mwidth/2 - crad])
            rotate([-90, 0, 0])
                cylinder(h = y1 + 2*e, r = crad, center=false);
    }
    module upperhole() {
        translate([x1 + x2, 0, z1 + z2 + z3 + z4 + mwidth/2])
            cube([x3, y1, z1 + z2 + z3 - mwidth/2], center=false);
        translate([x1, 0, z1 + z2 + z3 + z4 + mwidth/2 + crad])
            cube([x2 + x3 + x4, y1, z1 + z2 + z3 - mwidth/2 - crad], center=false);
        translate([x1 + x2, -e, z1 + z2 + z3 + z4 + mwidth/2 + crad])
            rotate([-90, 0, 0])
                cylinder(h = y1 + 2*e, r = crad, center=false);
        translate([x1 + x2 + x3, -e, z1 + z2 + z3 + z4 + mwidth/2 + crad])
            rotate([-90, 0, 0])
                cylinder(h = y1 + 2*e, r = crad, center=false);
    }
    module middlehole() {
        translate([x1 + x2 - mwc/2, 0, z1 + z2 + z3 + mwidth/2])
            cube([x3 + mwc, y1, z4-mwidth], center=false);
        translate([x1-mwc/2, 0, z1 + z2 + z3 + mwidth/2 + crad])
            cube([x2 + x3 + x4 + mwc, y1, z4 - mwidth - 2*crad], center=false);
        translate([x1 + x2 - mwc/2, -e, z1 + z2 + z3 + mwidth/2 + crad])
            rotate([-90, 0, 0])
                cylinder(h = y1 + 2*e, r = crad, center=false);
        translate([x1 + x2 + x3 +mwc/2, -e, z1 + z2 + z3 + mwidth/2 + crad])
            rotate([-90, 0, 0])
                cylinder(h = y1 + 2*e, r = crad, center=false);
        translate([x1 + x2 - mwc/2, -e, z1 + z2 + z3 + z4 - mwidth/2 - crad])
            rotate([-90, 0, 0])
                cylinder(h = y1 + 2*e, r = crad, center=false);
        translate([x1 + x2 + x3 + mwc/2, -e, z1 + z2 + z3 + z4 - mwidth/2 - crad])
            rotate([-90, 0, 0])
                cylinder(h = y1 + 2*e, r = crad, center=false);
    }
    module boltholes() {
        // Bolt Holes
        translate([x1/2, -e, z1 + z2/2])
            rotate([-90, 0, 0])
                cylinder(h = y1+2*e, r = Bolt2Diam/2, center = false);
        translate([x1 + x2 + x3 + x4 + x5/2, -e, z1 + z2/2])
            rotate([-90, 0, 0])
                cylinder(h = y1+2*e, r = Bolt2Diam/2, center = false);
        translate([x1/2, -e, z1 + z2 + z3 + z4 + z5 + z6/2])
            rotate([-90, 0, 0])
                cylinder(h = y1+2*e, r = Bolt2Diam/2, center = false);
        translate([x1 + x2 + x3 + x4 + x5/2, -e, z1 + z2 + z3 + z4 + z5 + z6/2])
            rotate([-90, 0, 0])
                cylinder(h = y1+2*e, r = Bolt2Diam/2, center = false);
        
        translate([x1/2, -e, z1 + z2 + z3])
            rotate([-90, 0, 0])
                cylinder(h = y1+2*e, r = Bolt3Diam/2, center = false);
        translate([x1 + x2 + x3 + x4 + x5/2, -e, z1 + z2 + z3])
            rotate([-90, 0, 0])
                cylinder(h = y1+2*e, r = Bolt3Diam/2, center = false);
        translate([x1/2, -e, z1 + z2 + z3 + z4])
            rotate([-90, 0, 0])
                cylinder(h = y1+2*e, r = Bolt3Diam/2, center = false);
        translate([x1 + x2 + x3 + x4 + x5/2, -e, z1 + z2 + z3 + z4])
            rotate([-90, 0, 0])
                cylinder(h = y1+2*e, r = Bolt3Diam/2, center = false);
                
    }
    module brake() {
        alpha = 8;
        w = 1.2;
        l = (x2 + x3 + x4 + mwc)*sqrt(2)*0.35;
        h = 0;
        w2 = 0.7;
        w3 = 2*ec;
        w4 = 2*w2 + w3;
        y2 = 7;
        xxx = x2 + x3 + x4 + mwc - 2*w/sin(alpha);
        xxx2 = tan(alpha)*(xxx-w4)/2;
        l2 = x2 + x3 + x4 + mwc - 2*xxx2;
        d5 = 8 + 2*ec;
        lx = (z4 - mwidth - d5 - 2*w2 - 2*xxx2)/2;
        difference() {
            union() {
                translate([x1 - mwc/2, h, z1 + z2 + z3 + z4 - mwidth/2])
                    rotate([0, alpha, 0])
                        cube([l, y1, w], center=false);
                translate([x1 - mwc/2 + w*sin(alpha), h, z1 + z2 + z3 + mwidth/2 -w*cos(alpha)])
                    rotate([0, -alpha, 0])
                        cube([l, y1, w], center=false);
                translate([x1 + x2 + x3 + x4 + mwc/2 -l*cos(alpha) - w*sin(alpha), h, z1 + z2 + z3 + mwidth/2 + l*sin(alpha) - w*cos(alpha)])
                    rotate([0, alpha, 0])
                        cube([l, y1, w], center=false);
                translate([x1 + x2 + x3 + x4 + mwc/2 -l*cos(alpha), h, z1 + z2 + z3 + z4 - mwidth/2 -l*sin(alpha)])
                    rotate([0, -alpha, 0])
                        cube([l, y1, w], center=false);
                translate([x1 + x2 + x3/2 - w4/2, h, z1 + z2 + z3 + mwidth/2 + xxx2])
                    cube([w4, y1 + y2, l2], center=false);
                translate([x1 + x2 + x3/2, h-e, z1 + z2 + z3 + z4/2])
                    rotate([-90, 0, 0])
                        cylinder(h = y1+y2+2*e, r = d5/2 + w2, center = false);
            }
            translate([x1 + x2 + x3/2 - w3/2, h - e, z1 + z2 + z3 + mwidth/2 + xxx2])
                cube([w3, y1 + y2 + 2*e, l2], center=false);
            translate([x1 + x2 + x3/2, h-e, z1 + z2 + z3 + z4/2])
                rotate([-90, 0, 0])
                    cylinder(h = y1+y2+2*e, r = d5/2, center = false);
            translate([x1 + x2 + x3/2, h + y1/2, z1 + z2 + z3 + mwidth/2 - 5/2])
                cube([x2 + x3 + x4 + mwc, y1, 5], center=true);
            translate([x1 + x2 + x3/2, h + y1/2, z1 + z2 + z3 + + z4 - mwidth/2 + 5/2])
                cube([x2 + x3 + x4 + mwc, y1, 5], center=true);
            
            translate([x1 + x2 + x3/2 - w2 - w3/2 -e, h+y1+y2*0.7, z1 + z2 + z3 + z4/2 + d5/2 + w2 + 5.5/2])
                rotate([0, 90, 0])
                    cylinder(h = 2*w2+w3 + 2*e, r = 3/2, center = false);
            translate([x1 + x2 + x3/2 - w2 - w3/2 -e, h+y1+y2*0.7, z1 + z2 + z3 + z4/2 - d5/2 - w2 - 5.5/2])
                rotate([0, 90, 0])
                    cylinder(h = 2*w2+w3 + 2*e, r = 3/2, center = false);
        }
        
        
    }
}

module Motor2Bracket5Base() {
    
    // Given dimensions
    e = 0.001;                  // Small expansion to ensure some holes are throughout
    ec = 0.4/2 + 0.1;           // Small expansion of holes corresponding to print core radius
    
    Motor1Diam1 = 86.36;        // Diameter of motor 1
    Motor2Diam1 = 57;           // Diameter of motor 2
    Motor2Diam2 = 25;           // Diameter of cylinder around shaft of motor 2
    Motor2Diam3 = 38.88;        // Diameter of circle on which bolt holes of motor 2 lay 
    Motor2Diam4 = 55;           // Diameter of motor 2 in the middle
    Motor2len1 =  9;            // Length of the rear part of motor 2
    
    Bolt1Diam = 6;              // Diameter of bolt that holds bracket 1 and 2 together
    Bolt1len = 90;              // Length of bolt that holds bracket 1 and 2 together
    Bolt1HeadDiam = 10;         // Diameter of head of bolt that holds bracket 1 and 2 together
    Bolt1HeadLen = 6;           // Length of head of bolt that holds bracket 1 and 2 together
    
    Bolt2Diam = 6;              // Diameter of bolt that holds bracket 1 and 3 together
    
    Bolt3Diam = 4;              // Diameter of bolt that holds bracket 3 and motor 2 together
    NutHoleDiam = 12;           // Diameter of hexagone hole for nut (cca 10/sin(60) + 2*ec)
    
    // Define dimensions of the bracket
    MotorHoleDiam = Motor2Diam1 + 2*ec;
    MotorHoleDiam2 = Motor2Diam4 + 2*ec;
    MotorHoleDiam3 = Motor2Diam2 + 2*ec;
    crad = 4;
    mwidth = 8;
    mwc = 14-mwidth;
    x_total = Motor2Diam3/sqrt(2) + 14;
    x1 = 14;
    x2 = crad;
    x3 = x_total - 2*14 - 2*crad;
    x4 = crad;
    x5 = 14;
    y1 = 7;
    z_total = MotorHoleDiam + 2*10;
    z1 = 4;
    z2 = 6;
    z3 = (z_total - 2*10 - Motor2Diam3/sqrt(2))/2;
    z4 = Motor2Diam3/sqrt(2);
    z5 = (z_total - 2*10 - Motor2Diam3/sqrt(2))/2;
    z6 = 6;
    z7 = 4;
    difference() {
        // Base
        cube([x1 + x2 + x3 + x4 + x5, y1, z1 + z2 + z3 + z4 + z5 + z6 + z7], center=false);
        //Holes
        lowerhole();
        upperhole();
        middlehole();
        boltholes();
        
    }
    
    module lowerhole() {
        translate([x1 + x2, 0, 0])
            cube([x3, y1, z1 + z2 + z3 - mwidth/2], center=false);
        translate([x1, 0, 0])
            cube([x2 + x3 + x4, y1, z1 + z2 + z3 - mwidth/2 - crad], center=false);
        translate([x1 + x2, -e, z1 + z2 + z3 - mwidth/2 - crad])
            rotate([-90, 0, 0])
                cylinder(h = y1 + 2*e, r = crad, center=false);
        translate([x1 + x2 + x3, -e, z1 + z2 + z3 - mwidth/2 - crad])
            rotate([-90, 0, 0])
                cylinder(h = y1 + 2*e, r = crad, center=false);
    }
    module upperhole() {
        translate([x1 + x2, 0, z1 + z2 + z3 + z4 + mwidth/2])
            cube([x3, y1, z1 + z2 + z3 - mwidth/2], center=false);
        translate([x1, 0, z1 + z2 + z3 + z4 + mwidth/2 + crad])
            cube([x2 + x3 + x4, y1, z1 + z2 + z3 - mwidth/2 - crad], center=false);
        translate([x1 + x2, -e, z1 + z2 + z3 + z4 + mwidth/2 + crad])
            rotate([-90, 0, 0])
                cylinder(h = y1 + 2*e, r = crad, center=false);
        translate([x1 + x2 + x3, -e, z1 + z2 + z3 + z4 + mwidth/2 + crad])
            rotate([-90, 0, 0])
                cylinder(h = y1 + 2*e, r = crad, center=false);
    }
    module middlehole() {
        translate([x1 + x2 - mwc/2, 0, z1 + z2 + z3 + mwidth/2])
            cube([x3 + mwc, y1, z4-mwidth], center=false);
        translate([x1-mwc/2, 0, z1 + z2 + z3 + mwidth/2 + crad])
            cube([x2 + x3 + x4 + mwc, y1, z4 - mwidth - 2*crad], center=false);
        translate([x1 + x2 - mwc/2, -e, z1 + z2 + z3 + mwidth/2 + crad])
            rotate([-90, 0, 0])
                cylinder(h = y1 + 2*e, r = crad, center=false);
        translate([x1 + x2 + x3 +mwc/2, -e, z1 + z2 + z3 + mwidth/2 + crad])
            rotate([-90, 0, 0])
                cylinder(h = y1 + 2*e, r = crad, center=false);
        translate([x1 + x2 - mwc/2, -e, z1 + z2 + z3 + z4 - mwidth/2 - crad])
            rotate([-90, 0, 0])
                cylinder(h = y1 + 2*e, r = crad, center=false);
        translate([x1 + x2 + x3 + mwc/2, -e, z1 + z2 + z3 + z4 - mwidth/2 - crad])
            rotate([-90, 0, 0])
                cylinder(h = y1 + 2*e, r = crad, center=false);
    }
    module boltholes() {
        // Bolt Holes
        translate([x1/2, -e, z1 + z2/2])
            rotate([-90, 0, 0])
                cylinder(h = y1+2*e, r = Bolt2Diam/2, center = false);
        translate([x1 + x2 + x3 + x4 + x5/2, -e, z1 + z2/2])
            rotate([-90, 0, 0])
                cylinder(h = y1+2*e, r = Bolt2Diam/2, center = false);
        translate([x1/2, -e, z1 + z2 + z3 + z4 + z5 + z6/2])
            rotate([-90, 0, 0])
                cylinder(h = y1+2*e, r = Bolt2Diam/2, center = false);
        translate([x1 + x2 + x3 + x4 + x5/2, -e, z1 + z2 + z3 + z4 + z5 + z6/2])
            rotate([-90, 0, 0])
                cylinder(h = y1+2*e, r = Bolt2Diam/2, center = false);
        
        translate([x1/2, -e, z1 + z2 + z3])
            rotate([-90, 0, 0])
                cylinder(h = y1+2*e, r = Bolt3Diam/2, center = false);
        translate([x1 + x2 + x3 + x4 + x5/2, -e, z1 + z2 + z3])
            rotate([-90, 0, 0])
                cylinder(h = y1+2*e, r = Bolt3Diam/2, center = false);
        translate([x1/2, -e, z1 + z2 + z3 + z4])
            rotate([-90, 0, 0])
                cylinder(h = y1+2*e, r = Bolt3Diam/2, center = false);
        translate([x1 + x2 + x3 + x4 + x5/2, -e, z1 + z2 + z3 + z4])
            rotate([-90, 0, 0])
                cylinder(h = y1+2*e, r = Bolt3Diam/2, center = false);
                
    }
}


// Motors
module motor1() {
    difference() {
        union() {
            color("DarkGray") cylinder(h=len_Motor1_l1, r=diam_Motor1_d1/2, center=false);
            color("DarkGray") translate([0, 0, len_Motor1_l1]) cylinder(h=len_Motor1_l2, r=   diam_Motor1_d2/2, center=false);
            color("Gainsboro") translate([0, 0, -10]) cylinder(h=len_Motor1_l1+40, r=diam_Motor1_d3/2, center=false);
        }
        color("DarkGray") translate([65/sqrt(2)/2, 65/sqrt(2)/2, len_Motor1_l1-6+0.001]) cylinder(h=6+0.001, r=3, center=false);
        color("DarkGray") translate([-65/sqrt(2)/2, 65/sqrt(2)/2, len_Motor1_l1-6+0.001]) cylinder(h=6+0.001, r=3, center=false);
        color("DarkGray") translate([65/sqrt(2)/2, -65/sqrt(2)/2, len_Motor1_l1-6+0.001]) cylinder(h=6+0.001, r=3, center=false);
        color("DarkGray") translate([-65/sqrt(2)/2, -65/sqrt(2)/2, len_Motor1_l1-6+0.001]) cylinder(h=6+0.001, r=3, center=false);
    }
}

module motor2() {
    difference() {
        union() {
            color("DarkGray") cylinder(h=len_Motor2_l1, r=diam_Motor2_d1/2, center=false);
            color("DarkGray") translate([0, 0, len_Motor2_l1]) cylinder(h=len_Motor2_l2, r=   diam_Motor2_d2/2, center=false);
            color("Gainsboro") translate([0, 0, -10]) cylinder(h=len_Motor2_l1+40, r=diam_Motor2_d3/2, center=false);
        }
        color("DarkGray") translate([38.88/sqrt(2)/2, 38.88/sqrt(2)/2, len_Motor2_l1-6+0.001]) cylinder(h=6+0.001, r=2, center=false);
        color("DarkGray") translate([-38.88/sqrt(2)/2, 38.88/sqrt(2)/2, len_Motor2_l1-6+0.001]) cylinder(h=6+0.001, r=2, center=false);
        color("DarkGray") translate([38.88/sqrt(2)/2, -38.88/sqrt(2)/2, len_Motor2_l1-6+0.001]) cylinder(h=6+0.001, r=2, center=false);
        color("DarkGray") translate([-38.88/sqrt(2)/2, -38.88/sqrt(2)/2, len_Motor2_l1-6+0.001]) cylinder(h=6+0.001, r=2, center=false);
    }
}

// Bolts and nuts
module bolt1() {
    color("Gainsboro") union() {
        cylinder(h=len_Bolt1, r=diam_Bolt1, center=false);
        translate([0, 0, len_Bolt1]) difference() {
            cylinder(h=len_BoltHead1, r=diam_BoltHead1, center=false);
            translate([0, 0, 0.25*len_BoltHead1+0.001]) hexagone(h=0.75*len_BoltHead1, r=0.75*diam_BoltHead1, center=false);
        }
    }
}

module nut1() {
    color("Gainsboro") difference() {
        hexagone(h=len_Nut1, r=diam_Nut1, center=false);
        translate([0, 0, -0.001]) cylinder(h=len_Nut1+0.002, r=diam_Bolt1, center=false);
    }
}


// Primitives
module hexagone_based_pyramid(h, r) {
    rcos60 = r*cos(60);
    rsin60 = r*sin(60);
    polyhedron(
		points = [
			// 6 points of the base
			[r, 0, 0],
			[rcos60, rsin60, 0],
			[-rcos60, rsin60, 0],
			[-r, 0, 0],
			[-rcos60, -rsin60, 0],
			[rcos60, -rsin60, 0],
            // Apex
			[0, 0, h]
		],
		faces = [
            // Base
			[0, 1, 2, 3, 4, 5],
            // Shell
            [1, 0, 6],
            [2, 1, 6],
            [3, 2, 6],
            [4, 3, 6],
            [5, 4, 6],
            [0, 5, 6],
		]
    );
}

module hexagone(h, r, center) {
    l = 2*r*sin(60);
    if (center) {
        centered_hexagone();
    } else {
        translate([0, 0, h/2]) centered_hexagone();
    }
    module centered_hexagone() union() {
		rotate([0,0,0]) cube([r,l,h],center=true);
		rotate([0,0,60]) cube([r,l,h],center=true);
		rotate([0,0,120]) cube([r,l,h],center=true);
	}
}




