EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 4
Title "PMSM_Driver_MZ_Split_Board"
Date "2020-03-30"
Rev "A"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	3050 6250 3150 6250
Wire Wire Line
	3150 6250 3150 6350
Wire Wire Line
	3050 6150 3150 6150
Wire Wire Line
	3150 6150 3150 6250
Connection ~ 3150 6250
Wire Wire Line
	3050 6050 3150 6050
Wire Wire Line
	3150 6050 3150 6150
Connection ~ 3150 6150
Wire Wire Line
	3050 5950 3150 5950
Wire Wire Line
	3150 5950 3150 6050
Connection ~ 3150 6050
Wire Wire Line
	3050 5850 3150 5850
Wire Wire Line
	3150 5850 3150 5950
Connection ~ 3150 5950
Wire Wire Line
	3050 5750 3150 5750
Wire Wire Line
	3150 5750 3150 5850
Connection ~ 3150 5850
Wire Wire Line
	3050 5650 3150 5650
Wire Wire Line
	3150 5650 3150 5750
Connection ~ 3150 5750
Connection ~ 3150 5650
Wire Wire Line
	3050 5450 3150 5450
Wire Wire Line
	3050 5350 3150 5350
Wire Wire Line
	3150 5350 3150 5450
Connection ~ 3150 5450
Wire Wire Line
	3050 5250 3150 5250
Wire Wire Line
	3150 5250 3150 5350
Connection ~ 3150 5350
Wire Wire Line
	3050 5150 3150 5150
Wire Wire Line
	3150 5150 3150 5250
Connection ~ 3150 5250
Wire Wire Line
	3050 5050 3150 5050
Wire Wire Line
	3150 5050 3150 5150
Connection ~ 3150 5150
Wire Wire Line
	3050 4950 3150 4950
Wire Wire Line
	3150 4950 3150 5050
Connection ~ 3150 5050
Wire Wire Line
	3050 4850 3150 4850
Wire Wire Line
	3150 4850 3150 4950
Connection ~ 3150 4950
Connection ~ 3150 4850
Wire Wire Line
	3050 4650 3150 4650
Wire Wire Line
	3050 4550 3150 4550
Wire Wire Line
	3150 4550 3150 4650
Connection ~ 3150 4650
Wire Wire Line
	3050 4450 3150 4450
Wire Wire Line
	3150 4450 3150 4550
Connection ~ 3150 4550
Wire Wire Line
	3050 4350 3150 4350
Wire Wire Line
	3150 4350 3150 4450
Connection ~ 3150 4450
Wire Wire Line
	3050 4250 3150 4250
Wire Wire Line
	3150 4250 3150 4350
Connection ~ 3150 4350
Wire Wire Line
	3050 4150 3150 4150
Wire Wire Line
	3150 4150 3150 4250
Connection ~ 3150 4250
Wire Wire Line
	3050 4050 3150 4050
Wire Wire Line
	3150 4050 3150 4150
Connection ~ 3150 4150
Wire Wire Line
	3050 3950 3150 3950
Wire Wire Line
	3150 3950 3150 4050
Connection ~ 3150 4050
Wire Wire Line
	3050 3850 3150 3850
Wire Wire Line
	3150 3850 3150 3950
Connection ~ 3150 3950
Wire Wire Line
	3050 2925 3150 2925
Wire Wire Line
	3150 2925 3150 3025
Wire Wire Line
	3050 2825 3150 2825
Wire Wire Line
	3150 2825 3150 2925
Connection ~ 3150 2925
Wire Wire Line
	3050 2725 3150 2725
Wire Wire Line
	3150 2725 3150 2825
Connection ~ 3150 2825
Wire Wire Line
	3050 2625 3150 2625
Wire Wire Line
	3150 2625 3150 2725
Connection ~ 3150 2725
Wire Wire Line
	3050 2525 3150 2525
Wire Wire Line
	3150 2525 3150 2625
Connection ~ 3150 2625
Wire Wire Line
	3050 2425 3150 2425
Wire Wire Line
	3150 2425 3150 2525
Connection ~ 3150 2525
Wire Wire Line
	3050 2325 3150 2325
Wire Wire Line
	3150 2325 3150 2425
Connection ~ 3150 2425
Connection ~ 3150 2325
Wire Wire Line
	3050 2125 3150 2125
Wire Wire Line
	3050 2025 3150 2025
Wire Wire Line
	3150 2025 3150 2125
Connection ~ 3150 2125
Wire Wire Line
	3050 1925 3150 1925
Wire Wire Line
	3150 1925 3150 2025
Connection ~ 3150 2025
Wire Wire Line
	3050 1825 3150 1825
Wire Wire Line
	3150 1825 3150 1925
Connection ~ 3150 1925
Wire Wire Line
	3050 1725 3150 1725
Wire Wire Line
	3150 1725 3150 1825
Connection ~ 3150 1825
Wire Wire Line
	3050 1625 3150 1625
Wire Wire Line
	3150 1625 3150 1725
Connection ~ 3150 1725
Wire Wire Line
	3050 1525 3150 1525
Wire Wire Line
	3150 1525 3150 1625
Connection ~ 3150 1625
Connection ~ 3150 1525
Wire Wire Line
	3050 1325 3150 1325
Wire Wire Line
	3050 1225 3150 1225
Wire Wire Line
	3150 1225 3150 1325
Connection ~ 3150 1325
Wire Wire Line
	3050 1125 3150 1125
Wire Wire Line
	3150 1125 3150 1225
Connection ~ 3150 1225
Wire Wire Line
	3050 1025 3150 1025
Wire Wire Line
	3150 1025 3150 1125
Connection ~ 3150 1125
Wire Wire Line
	3150 4650 3150 4750
Wire Wire Line
	3050 4750 3150 4750
Connection ~ 3150 4750
Wire Wire Line
	3150 4750 3150 4850
Wire Wire Line
	3150 5450 3150 5550
Wire Wire Line
	3050 5550 3150 5550
Connection ~ 3150 5550
Wire Wire Line
	3150 5550 3150 5650
Wire Wire Line
	3150 1325 3150 1425
Wire Wire Line
	3050 1425 3150 1425
Connection ~ 3150 1425
Wire Wire Line
	3150 1425 3150 1525
Wire Wire Line
	3150 2125 3150 2225
Wire Wire Line
	3050 2225 3150 2225
Connection ~ 3150 2225
Wire Wire Line
	3150 2225 3150 2325
Wire Wire Line
	2450 3850 2550 3850
Wire Wire Line
	2450 3950 2550 3950
Wire Wire Line
	2450 4050 2550 4050
Wire Wire Line
	2450 4150 2550 4150
Wire Wire Line
	2450 4250 2550 4250
Wire Wire Line
	2450 4350 2550 4350
Wire Wire Line
	2450 4550 2550 4550
Wire Wire Line
	2450 4650 2550 4650
Wire Wire Line
	2450 4750 2550 4750
Wire Wire Line
	2450 4850 2550 4850
Wire Wire Line
	2450 4950 2550 4950
Wire Wire Line
	2450 5050 2550 5050
Wire Wire Line
	2450 5150 2550 5150
Wire Wire Line
	2450 5250 2550 5250
Wire Wire Line
	2450 5350 2550 5350
Wire Wire Line
	2450 5450 2550 5450
Wire Wire Line
	2450 5550 2550 5550
Wire Wire Line
	2450 5650 2550 5650
Wire Wire Line
	2450 4450 2550 4450
Wire Wire Line
	2450 5850 2550 5850
Wire Wire Line
	2450 1025 2550 1025
Wire Wire Line
	2450 1225 2550 1225
Wire Wire Line
	2450 1325 2550 1325
Wire Wire Line
	2450 1425 2550 1425
Wire Wire Line
	2450 1525 2550 1525
Wire Wire Line
	2450 1625 2550 1625
Wire Wire Line
	2450 1725 2550 1725
Wire Wire Line
	2450 1825 2550 1825
Wire Wire Line
	2450 1925 2550 1925
Wire Wire Line
	2450 2025 2550 2025
Wire Wire Line
	2450 2125 2550 2125
Wire Wire Line
	2450 5950 2550 5950
Wire Wire Line
	2450 6050 2550 6050
Wire Wire Line
	2550 2225 2450 2225
Wire Wire Line
	850  2150 850  2225
Wire Wire Line
	2550 2325 2450 2325
Wire Wire Line
	2450 2325 2450 2225
Wire Wire Line
	2450 2325 2450 2425
Wire Wire Line
	2450 2425 2550 2425
Connection ~ 2450 2325
Wire Wire Line
	2450 2425 2450 2525
Wire Wire Line
	2450 2525 2550 2525
Connection ~ 2450 2425
Wire Wire Line
	2550 2625 2450 2625
Wire Wire Line
	2550 2725 2450 2725
Wire Wire Line
	2450 2725 2450 2625
Wire Wire Line
	2550 2825 2450 2825
Wire Wire Line
	2450 2825 2450 2725
Connection ~ 2450 2725
Wire Wire Line
	2550 2925 2450 2925
Wire Wire Line
	2450 2925 2450 2825
Connection ~ 2450 2825
Wire Wire Line
	2450 5750 2550 5750
Text Notes 675  800  0    100  ~ 0
Motor 1 Control Connector
Wire Notes Line
	3425 3350 3425 600 
Wire Notes Line
	600  600  600  3350
Text Notes 650  3625 0    100  ~ 0
Motor 1 Sensor Connector
Wire Notes Line
	600  3425 600  6675
Wire Notes Line
	3425 3425 3425 6675
Wire Wire Line
	5950 6250 6050 6250
Wire Wire Line
	6050 6250 6050 6350
Wire Wire Line
	5950 6150 6050 6150
Wire Wire Line
	6050 6150 6050 6250
Connection ~ 6050 6250
Wire Wire Line
	5950 6050 6050 6050
Wire Wire Line
	6050 6050 6050 6150
Connection ~ 6050 6150
Wire Wire Line
	5950 5950 6050 5950
Wire Wire Line
	6050 5950 6050 6050
Connection ~ 6050 6050
Wire Wire Line
	5950 5850 6050 5850
Wire Wire Line
	6050 5850 6050 5950
Connection ~ 6050 5950
Wire Wire Line
	5950 5750 6050 5750
Wire Wire Line
	6050 5750 6050 5850
Connection ~ 6050 5850
Wire Wire Line
	5950 5650 6050 5650
Wire Wire Line
	6050 5650 6050 5750
Connection ~ 6050 5750
Connection ~ 6050 5650
Wire Wire Line
	5950 5450 6050 5450
Wire Wire Line
	5950 5350 6050 5350
Wire Wire Line
	6050 5350 6050 5450
Connection ~ 6050 5450
Wire Wire Line
	5950 5250 6050 5250
Wire Wire Line
	6050 5250 6050 5350
Connection ~ 6050 5350
Wire Wire Line
	5950 5150 6050 5150
Wire Wire Line
	6050 5150 6050 5250
Connection ~ 6050 5250
Wire Wire Line
	5950 5050 6050 5050
Wire Wire Line
	6050 5050 6050 5150
Connection ~ 6050 5150
Wire Wire Line
	5950 4950 6050 4950
Wire Wire Line
	6050 4950 6050 5050
Connection ~ 6050 5050
Wire Wire Line
	5950 4850 6050 4850
Wire Wire Line
	6050 4850 6050 4950
Connection ~ 6050 4950
Connection ~ 6050 4850
Wire Wire Line
	5950 4650 6050 4650
Wire Wire Line
	5950 4550 6050 4550
Wire Wire Line
	6050 4550 6050 4650
Connection ~ 6050 4650
Wire Wire Line
	5950 4450 6050 4450
Wire Wire Line
	6050 4450 6050 4550
Connection ~ 6050 4550
Wire Wire Line
	5950 4350 6050 4350
Wire Wire Line
	6050 4350 6050 4450
Connection ~ 6050 4450
Wire Wire Line
	5950 4250 6050 4250
Wire Wire Line
	6050 4250 6050 4350
Connection ~ 6050 4350
Wire Wire Line
	5950 4150 6050 4150
Wire Wire Line
	6050 4150 6050 4250
Connection ~ 6050 4250
Wire Wire Line
	5950 4050 6050 4050
Wire Wire Line
	6050 4050 6050 4150
Connection ~ 6050 4150
Wire Wire Line
	5950 3950 6050 3950
Wire Wire Line
	6050 3950 6050 4050
Connection ~ 6050 4050
Wire Wire Line
	5950 3850 6050 3850
Wire Wire Line
	6050 3850 6050 3950
Connection ~ 6050 3950
Wire Wire Line
	5950 2925 6050 2925
Wire Wire Line
	6050 2925 6050 3025
Wire Wire Line
	5950 2825 6050 2825
Wire Wire Line
	6050 2825 6050 2925
Connection ~ 6050 2925
Wire Wire Line
	5950 2725 6050 2725
Wire Wire Line
	6050 2725 6050 2825
Connection ~ 6050 2825
Wire Wire Line
	5950 2625 6050 2625
Wire Wire Line
	6050 2625 6050 2725
Connection ~ 6050 2725
Wire Wire Line
	5950 2525 6050 2525
Wire Wire Line
	6050 2525 6050 2625
Connection ~ 6050 2625
Wire Wire Line
	5950 2425 6050 2425
Wire Wire Line
	6050 2425 6050 2525
Connection ~ 6050 2525
Wire Wire Line
	5950 2325 6050 2325
Wire Wire Line
	6050 2325 6050 2425
Connection ~ 6050 2425
Connection ~ 6050 2325
Wire Wire Line
	5950 2125 6050 2125
Wire Wire Line
	5950 2025 6050 2025
Wire Wire Line
	6050 2025 6050 2125
Connection ~ 6050 2125
Wire Wire Line
	5950 1925 6050 1925
Wire Wire Line
	6050 1925 6050 2025
Connection ~ 6050 2025
Wire Wire Line
	5950 1825 6050 1825
Wire Wire Line
	6050 1825 6050 1925
Connection ~ 6050 1925
Wire Wire Line
	5950 1725 6050 1725
Wire Wire Line
	6050 1725 6050 1825
Connection ~ 6050 1825
Wire Wire Line
	5950 1625 6050 1625
Wire Wire Line
	6050 1625 6050 1725
Connection ~ 6050 1725
Wire Wire Line
	5950 1525 6050 1525
Wire Wire Line
	6050 1525 6050 1625
Connection ~ 6050 1625
Connection ~ 6050 1525
Wire Wire Line
	5950 1325 6050 1325
Wire Wire Line
	5950 1225 6050 1225
Wire Wire Line
	6050 1225 6050 1325
Connection ~ 6050 1325
Wire Wire Line
	5950 1125 6050 1125
Wire Wire Line
	6050 1125 6050 1225
Connection ~ 6050 1225
Wire Wire Line
	5950 1025 6050 1025
Wire Wire Line
	6050 1025 6050 1125
Connection ~ 6050 1125
Wire Wire Line
	6050 4650 6050 4750
Wire Wire Line
	5950 4750 6050 4750
Connection ~ 6050 4750
Wire Wire Line
	6050 4750 6050 4850
Wire Wire Line
	6050 5450 6050 5550
Wire Wire Line
	5950 5550 6050 5550
Connection ~ 6050 5550
Wire Wire Line
	6050 5550 6050 5650
Wire Wire Line
	6050 1325 6050 1425
Wire Wire Line
	5950 1425 6050 1425
Connection ~ 6050 1425
Wire Wire Line
	6050 1425 6050 1525
Wire Wire Line
	6050 2125 6050 2225
Wire Wire Line
	5950 2225 6050 2225
Connection ~ 6050 2225
Wire Wire Line
	6050 2225 6050 2325
Wire Wire Line
	5350 3850 5450 3850
Wire Wire Line
	5350 3950 5450 3950
Wire Wire Line
	5350 4050 5450 4050
Wire Wire Line
	5350 4150 5450 4150
Wire Wire Line
	5350 4250 5450 4250
Wire Wire Line
	5350 4350 5450 4350
Wire Wire Line
	5350 4550 5450 4550
Wire Wire Line
	5350 4650 5450 4650
Wire Wire Line
	5350 4750 5450 4750
Wire Wire Line
	5350 4850 5450 4850
Wire Wire Line
	5350 4950 5450 4950
Wire Wire Line
	5350 5050 5450 5050
Wire Wire Line
	5350 5150 5450 5150
Wire Wire Line
	5350 5250 5450 5250
Wire Wire Line
	5350 5350 5450 5350
Wire Wire Line
	5350 5450 5450 5450
Wire Wire Line
	5350 5550 5450 5550
Wire Wire Line
	5350 5650 5450 5650
Wire Wire Line
	5350 4450 5450 4450
Wire Wire Line
	5350 5850 5450 5850
Wire Wire Line
	5350 1025 5450 1025
Wire Wire Line
	5350 1125 5450 1125
Wire Wire Line
	5350 1225 5450 1225
Wire Wire Line
	5350 1325 5450 1325
Wire Wire Line
	5350 1425 5450 1425
Wire Wire Line
	5350 1525 5450 1525
Wire Wire Line
	5350 1625 5450 1625
Wire Wire Line
	5350 1725 5450 1725
Wire Wire Line
	5350 1825 5450 1825
Wire Wire Line
	5350 1925 5450 1925
Wire Wire Line
	5350 2025 5450 2025
Wire Wire Line
	5350 2125 5450 2125
Wire Wire Line
	5350 5950 5450 5950
Wire Wire Line
	5350 6050 5450 6050
Wire Wire Line
	5450 2225 5350 2225
Wire Wire Line
	5450 2325 5350 2325
Wire Wire Line
	5350 2325 5350 2225
Wire Wire Line
	5350 2325 5350 2425
Wire Wire Line
	5350 2425 5450 2425
Connection ~ 5350 2325
Wire Wire Line
	5350 2425 5350 2525
Wire Wire Line
	5350 2525 5450 2525
Connection ~ 5350 2425
Wire Wire Line
	5450 2625 5350 2625
Wire Wire Line
	5450 2725 5350 2725
Wire Wire Line
	5350 2725 5350 2625
Wire Wire Line
	5450 2825 5350 2825
Wire Wire Line
	5350 2825 5350 2725
Connection ~ 5350 2725
Wire Wire Line
	5450 2925 5350 2925
Wire Wire Line
	5350 2925 5350 2825
Connection ~ 5350 2825
Wire Wire Line
	5350 6150 5450 6150
Wire Wire Line
	5350 6250 5450 6250
Wire Wire Line
	5350 5750 5450 5750
Text Notes 3575 800  0    100  ~ 0
Motor 2 Control Connector
Wire Notes Line
	6325 3350 6325 600 
Wire Notes Line
	3500 600  3500 3350
Text Notes 3550 3625 0    100  ~ 0
Motor 2 Sensor Connector
Wire Notes Line
	3500 3425 3500 6675
Wire Notes Line
	6325 3425 6325 6675
Wire Wire Line
	2450 6150 2550 6150
Wire Wire Line
	2450 6250 2550 6250
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_3V3 #PWR09
U 1 1 5E91492E
P 3750 2150
F 0 "#PWR09" H 3750 2400 50  0001 C CNN
F 1 "MZ_3V3" H 3751 2323 50  0000 C CNN
F 2 "" H 3750 2150 50  0001 C CNN
F 3 "" H 3750 2150 50  0001 C CNN
	1    3750 2150
	1    0    0    -1  
$EndComp
Wire Wire Line
	3750 2150 3750 2225
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_3V3 #PWR05
U 1 1 5EA5F74F
P 850 2150
F 0 "#PWR05" H 850 2400 50  0001 C CNN
F 1 "MZ_3V3" H 851 2323 50  0000 C CNN
F 2 "" H 850 2150 50  0001 C CNN
F 3 "" H 850 2150 50  0001 C CNN
	1    850  2150
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Connector:Connector_MC-254-50-00-ST-DIP J3
U 1 1 5E9A6CFF
P 2800 5050
F 0 "J3" H 2725 6350 50  0000 C CNN
F 1 "Connector_MC-254-50-00-ST-DIP" H 2225 3725 50  0000 C CNN
F 2 "PMSM_Driver_MZ_Split_Board:IDC-Header_2x25_P2.54mm_Vertical" H 2800 3600 50  0001 C CNN
F 3 "~" H 2750 5050 50  0001 C CNN
F 4 "MC-254-50-00-ST-DIP -  Wire-To-Board Connector, 2.54 mm, 50 Contacts, Header, Through Hole, 2 Rows" H 2800 5050 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc-254-50-00-st-dip/connector-header-50pos-2row-2/dp/2843537" H 2800 5050 50  0001 C CNN "Distributor Link"
	1    2800 5050
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Connector:Connector_MC-254-50-00-ST-DIP J5
U 1 1 5E9AABD2
P 5700 5050
F 0 "J5" H 5650 6350 50  0000 C CNN
F 1 "Connector_MC-254-50-00-ST-DIP" H 5150 3750 50  0000 C CNN
F 2 "PMSM_Driver_MZ_Split_Board:IDC-Header_2x25_P2.54mm_Vertical" H 5700 3600 50  0001 C CNN
F 3 "~" H 5650 5050 50  0001 C CNN
F 4 "MC-254-50-00-ST-DIP -  Wire-To-Board Connector, 2.54 mm, 50 Contacts, Header, Through Hole, 2 Rows" H 5700 5050 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc-254-50-00-st-dip/connector-header-50pos-2row-2/dp/2843537" H 5700 5050 50  0001 C CNN "Distributor Link"
	1    5700 5050
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Connector:Connector_MC-254-40-00-ST-DIP J4
U 1 1 5E9A3DA6
P 5700 1925
F 0 "J4" H 5650 2925 50  0000 C CNN
F 1 "Connector_MC-254-40-00-ST-DIP" H 5175 825 50  0000 C CNN
F 2 "PMSM_Driver_MZ_Split_Board:IDC-Header_2x20_P2.54mm_Vertical" H 5700 725 50  0001 C CNN
F 3 "~" H 5650 1925 50  0001 C CNN
F 4 "MC-254-40-00-ST-DIP -  Wire-To-Board Connector, 2.54 mm, 40 Contacts, Header, Through Hole, 2 Rows" H 5700 1925 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc-254-40-00-st-dip/connector-header-40pos-2row-2/dp/2843536" H 5700 1925 50  0001 C CNN "Distributor Link"
	1    5700 1925
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Connector:Connector_MC-254-40-00-ST-DIP J2
U 1 1 5E99E10D
P 2800 1925
F 0 "J2" H 2725 2925 50  0000 C CNN
F 1 "Connector_MC-254-40-00-ST-DIP" H 2275 800 50  0000 C CNN
F 2 "PMSM_Driver_MZ_Split_Board:IDC-Header_2x20_P2.54mm_Vertical" H 2800 725 50  0001 C CNN
F 3 "~" H 2750 1925 50  0001 C CNN
F 4 "MC-254-40-00-ST-DIP -  Wire-To-Board Connector, 2.54 mm, 40 Contacts, Header, Through Hole, 2 Rows" H 2800 1925 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc-254-40-00-st-dip/connector-header-40pos-2row-2/dp/2843536" H 2800 1925 50  0001 C CNN "Distributor Link"
	1    2800 1925
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_VCCO_35 #PWR?
U 1 1 5ED99D9C
P 4275 2500
AR Path="/5EB87562/5ED99D9C" Ref="#PWR?"  Part="1" 
AR Path="/5E9077E4/5ED99D9C" Ref="#PWR?"  Part="1" 
AR Path="/5E9110E2/5ED99D9C" Ref="#PWR0155"  Part="1" 
F 0 "#PWR0155" H 4275 2750 50  0001 C CNN
F 1 "MZ_VCCO_35" H 4276 2673 50  0000 C CNN
F 2 "" H 4275 2500 50  0001 C CNN
F 3 "" H 4275 2500 50  0001 C CNN
	1    4275 2500
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_VCCO_34 #PWR?
U 1 1 5ED99DA3
P 1350 2500
AR Path="/5E9077E4/5ED99DA3" Ref="#PWR?"  Part="1" 
AR Path="/5E9110E2/5ED99DA3" Ref="#PWR0156"  Part="1" 
F 0 "#PWR0156" H 1350 2750 50  0001 C CNN
F 1 "MZ_VCCO_34" H 1351 2673 50  0000 C CNN
F 2 "" H 1350 2500 50  0001 C CNN
F 3 "" H 1350 2500 50  0001 C CNN
	1    1350 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	1350 2625 1350 2500
Wire Wire Line
	4275 2625 4275 2500
$Comp
L PMSM_Driver_MZ_Split_Board_Device:C_Small C?
U 1 1 5EF8D470
P 850 2400
AR Path="/5EB87562/5EF8D470" Ref="C?"  Part="1" 
AR Path="/5E9077E4/5EF8D470" Ref="C?"  Part="1" 
AR Path="/5E9110E2/5EF8D470" Ref="C31"  Part="1" 
F 0 "C31" H 942 2446 50  0000 L CNN
F 1 "22uF" H 942 2355 50  0000 L CNN
F 2 "PMSM_Driver_MZ_Split_Board:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 850 2400 50  0001 C CNN
F 3 "~" H 850 2400 50  0001 C CNN
F 4 "GRM21BR61A226ME51L -  SMD Multilayer Ceramic Capacitor, 22 µF, 10 V, 0805 [2012 Metric], ± 20%, X5R, GRM Series" H 850 2000 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/murata/grm21br61a226me51l/cap-22-f-10v-20-x5r-0805/dp/2494270" H 850 1900 50  0001 C CNN "Distributor Link"
F 6 "10V" H 1000 2275 50  0000 C CNN "Voltage"
	1    850  2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	850  2300 850  2225
Connection ~ 850  2225
$Comp
L PMSM_Driver_MZ_Split_Board_Device:C_Small C?
U 1 1 5F007854
P 1350 2800
AR Path="/5EB87562/5F007854" Ref="C?"  Part="1" 
AR Path="/5E9077E4/5F007854" Ref="C?"  Part="1" 
AR Path="/5E9110E2/5F007854" Ref="C32"  Part="1" 
F 0 "C32" H 1442 2846 50  0000 L CNN
F 1 "22uF" H 1442 2755 50  0000 L CNN
F 2 "PMSM_Driver_MZ_Split_Board:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 1350 2800 50  0001 C CNN
F 3 "~" H 1350 2800 50  0001 C CNN
F 4 "GRM21BR61A226ME51L -  SMD Multilayer Ceramic Capacitor, 22 µF, 10 V, 0805 [2012 Metric], ± 20%, X5R, GRM Series" H 1350 2400 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/murata/grm21br61a226me51l/cap-22-f-10v-20-x5r-0805/dp/2494270" H 1350 2300 50  0001 C CNN "Distributor Link"
F 6 "10V" H 1500 2675 50  0000 C CNN "Voltage"
	1    1350 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	1350 2700 1350 2625
Connection ~ 1350 2625
Wire Notes Line
	600  600  3425 600 
Wire Notes Line
	600  3350 3425 3350
Wire Wire Line
	1350 2625 2450 2625
Connection ~ 2450 2625
Wire Wire Line
	850  2225 2450 2225
Connection ~ 2450 2225
Wire Notes Line
	3500 600  6325 600 
Wire Notes Line
	3500 3350 6325 3350
$Comp
L PMSM_Driver_MZ_Split_Board_Device:C_Small C?
U 1 1 5F2AF00F
P 3750 2400
AR Path="/5EB87562/5F2AF00F" Ref="C?"  Part="1" 
AR Path="/5E9077E4/5F2AF00F" Ref="C?"  Part="1" 
AR Path="/5E9110E2/5F2AF00F" Ref="C33"  Part="1" 
F 0 "C33" H 3842 2446 50  0000 L CNN
F 1 "22uF" H 3842 2355 50  0000 L CNN
F 2 "PMSM_Driver_MZ_Split_Board:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 3750 2400 50  0001 C CNN
F 3 "~" H 3750 2400 50  0001 C CNN
F 4 "GRM21BR61A226ME51L -  SMD Multilayer Ceramic Capacitor, 22 µF, 10 V, 0805 [2012 Metric], ± 20%, X5R, GRM Series" H 3750 2000 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/murata/grm21br61a226me51l/cap-22-f-10v-20-x5r-0805/dp/2494270" H 3750 1900 50  0001 C CNN "Distributor Link"
F 6 "10V" H 3900 2275 50  0000 C CNN "Voltage"
	1    3750 2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3750 2300 3750 2225
Connection ~ 3750 2225
Wire Wire Line
	3750 2225 5350 2225
Connection ~ 5350 2225
$Comp
L PMSM_Driver_MZ_Split_Board_Device:C_Small C?
U 1 1 5F305922
P 4275 2800
AR Path="/5EB87562/5F305922" Ref="C?"  Part="1" 
AR Path="/5E9077E4/5F305922" Ref="C?"  Part="1" 
AR Path="/5E9110E2/5F305922" Ref="C34"  Part="1" 
F 0 "C34" H 4367 2846 50  0000 L CNN
F 1 "22uF" H 4367 2755 50  0000 L CNN
F 2 "PMSM_Driver_MZ_Split_Board:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4275 2800 50  0001 C CNN
F 3 "~" H 4275 2800 50  0001 C CNN
F 4 "GRM21BR61A226ME51L -  SMD Multilayer Ceramic Capacitor, 22 µF, 10 V, 0805 [2012 Metric], ± 20%, X5R, GRM Series" H 4275 2400 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/murata/grm21br61a226me51l/cap-22-f-10v-20-x5r-0805/dp/2494270" H 4275 2300 50  0001 C CNN "Distributor Link"
F 6 "10V" H 4425 2675 50  0000 C CNN "Voltage"
	1    4275 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	4275 2700 4275 2625
Connection ~ 4275 2625
Wire Wire Line
	4275 2625 5350 2625
Connection ~ 5350 2625
Wire Notes Line
	3500 3425 6325 3425
Wire Notes Line
	3500 6675 6325 6675
Wire Notes Line
	600  3425 3425 3425
Wire Notes Line
	600  6675 3425 6675
Wire Wire Line
	2450 1125 2550 1125
Text GLabel 2450 3950 0    50   Output ~ 0
MZ_M1_V_A
Text GLabel 2450 4050 0    50   Output ~ 0
MZ_M1_V_B
Text GLabel 2450 4150 0    50   Output ~ 0
MZ_M1_V_C
Text GLabel 2450 4250 0    50   Output ~ 0
MZ_M1_CUR_A
Text GLabel 2450 4350 0    50   Output ~ 0
MZ_M1_CUR_B
Text GLabel 2450 4450 0    50   Output ~ 0
MZ_M1_CUR_T
Text GLabel 2450 4750 0    50   Output ~ 0
MZ_M1_ENC_I
Text GLabel 2450 4850 0    50   Output ~ 0
MZ_M1_RES_A
Text GLabel 2450 4950 0    50   Output ~ 0
MZ_M1_RES_B
Text GLabel 2450 5150 0    50   Output ~ 0
MZ_M1_HAL_A
Text GLabel 2450 5250 0    50   Output ~ 0
MZ_M1_HAL_B
Text GLabel 2450 5350 0    50   Output ~ 0
MZ_M1_HAL_C
Text GLabel 2450 5450 0    50   Output ~ 0
MZ_M1_TEMP
Text GLabel 2450 5050 0    50   Output ~ 0
MZ_M1_RES_I
Text GLabel 2450 5650 0    50   Input ~ 0
MZ_M1_SPI_SEL_RDC1
Text GLabel 2450 5550 0    50   Input ~ 0
MZ_M1_RDC_SAMPLE_N
Text GLabel 2450 5850 0    50   Input ~ 0
MZ_M1_SPI_SCK
Text GLabel 2450 4650 0    50   Output ~ 0
MZ_M1_ENC_B
Text GLabel 2450 4550 0    50   Output ~ 0
MZ_M1_ENC_A
Text GLabel 2450 3850 0    50   Output ~ 0
MZ_M1_V_SUP
Text GLabel 2450 6050 0    50   Input ~ 0
MZ_M1_SPI_MOSI
Text GLabel 2450 5950 0    50   Output ~ 0
MZ_M1_SPI_MISO
Text GLabel 2450 5750 0    50   Input ~ 0
MZ_M1_SPI_SEL_AUX1
Text GLabel 2450 6250 0    50   BiDi ~ 0
MZ_M1_SDA
Text GLabel 2450 6150 0    50   Input ~ 0
MZ_M1_SCL
Text GLabel 5350 3950 0    50   Output ~ 0
MZ_M2_V_A
Text GLabel 5350 4050 0    50   Output ~ 0
MZ_M2_V_B
Text GLabel 5350 4150 0    50   Output ~ 0
MZ_M2_V_C
Text GLabel 5350 4250 0    50   Output ~ 0
MZ_M2_CUR_A
Text GLabel 5350 4350 0    50   Output ~ 0
MZ_M2_CUR_B
Text GLabel 5350 4450 0    50   Output ~ 0
MZ_M2_CUR_T
Text GLabel 5350 4750 0    50   Output ~ 0
MZ_M2_ENC_I
Text GLabel 5350 4950 0    50   Output ~ 0
MZ_M2_RES_B
Text GLabel 5350 5150 0    50   Output ~ 0
MZ_M2_HAL_A
Text GLabel 5350 5250 0    50   Output ~ 0
MZ_M2_HAL_B
Text GLabel 5350 5350 0    50   Output ~ 0
MZ_M2_HAL_C
Text GLabel 5350 5450 0    50   Output ~ 0
MZ_M2_TEMP
Text GLabel 5350 5050 0    50   Output ~ 0
MZ_M2_RES_I
Text GLabel 5350 5650 0    50   Input ~ 0
MZ_M2_SPI_SEL_RDC2
Text GLabel 5350 5550 0    50   Input ~ 0
MZ_M2_RDC_SAMPLE_N
Text GLabel 5350 5850 0    50   Input ~ 0
MZ_M2_SPI_SCK
Text GLabel 5350 4650 0    50   Output ~ 0
MZ_M2_ENC_B
Text GLabel 5350 4550 0    50   Output ~ 0
MZ_M2_ENC_A
Text GLabel 5350 3850 0    50   Output ~ 0
MZ_M2_V_SUP
Text GLabel 5350 6050 0    50   Input ~ 0
MZ_M2_SPI_MOSI
Text GLabel 5350 5950 0    50   Output ~ 0
MZ_M2_SPI_MISO
Text GLabel 5350 6250 0    50   BiDi ~ 0
MZ_M2_SDA
Text GLabel 5350 6150 0    50   Input ~ 0
MZ_M2_SCL
Text GLabel 5350 5750 0    50   Input ~ 0
MZ_M2_SPI_SEL_AUX2
Text GLabel 5350 4850 0    50   Output ~ 0
MZ_M2_RES_A
Text GLabel 5350 1025 0    50   Input ~ 0
MZ_M2_ADC_CLK
Text GLabel 5350 1225 0    50   Input ~ 0
MZ_M2_VT_ENABLE
Text GLabel 5350 1425 0    50   Input ~ 0
MZ_M2_PWM_AH
Text GLabel 5350 1525 0    50   Input ~ 0
MZ_M2_PWM_AL
Text GLabel 5350 1625 0    50   Input ~ 0
MZ_M2_PWM_BH
Text GLabel 5350 1725 0    50   Input ~ 0
MZ_M2_PWM_BL
Text GLabel 5350 1825 0    50   Input ~ 0
MZ_M2_PWM_CH
Text GLabel 5350 1925 0    50   Input ~ 0
MZ_M2_PWM_CL
Text GLabel 5350 1325 0    50   Input ~ 0
MZ_M2_EN
Text GLabel 5350 2025 0    50   Input ~ 0
MZ_M2_RST_OVR_CUR
Text GLabel 5350 2125 0    50   Output ~ 0
MZ_M2_NOT_OVR_CUR
Text GLabel 5350 1125 0    50   Input ~ 0
PG_MODULE
Text GLabel 2450 1025 0    50   Input ~ 0
MZ_M1_ADC_CLK
Text GLabel 2450 1225 0    50   Input ~ 0
MZ_M1_VT_ENABLE
Text GLabel 2450 1425 0    50   Input ~ 0
MZ_M1_PWM_AH
Text GLabel 2450 1525 0    50   Input ~ 0
MZ_M1_PWM_AL
Text GLabel 2450 1625 0    50   Input ~ 0
MZ_M1_PWM_BH
Text GLabel 2450 1725 0    50   Input ~ 0
MZ_M1_PWM_BL
Text GLabel 2450 1825 0    50   Input ~ 0
MZ_M1_PWM_CH
Text GLabel 2450 1925 0    50   Input ~ 0
MZ_M1_PWM_CL
Text GLabel 2450 1325 0    50   Input ~ 0
MZ_M1_EN
Text GLabel 2450 2025 0    50   Input ~ 0
MZ_M1_RST_OVR_CUR
Text GLabel 2450 2125 0    50   Output ~ 0
MZ_M1_NOT_OVR_CUR
Text GLabel 2450 1125 0    50   Input ~ 0
PG_MODULE
Text Notes 700  925  0    25   ~ 0
PG_MODULE is 3.3V signal,\nall other signals are VCCO34 signals.
Text Notes 675  3700 0    25   ~ 0
All signals are VCCO34 signals.
Text Notes 3600 925  0    25   ~ 0
PG_MODULE is 3.3V signal,\nall other signals are VCCO35 signals.
Text Notes 3575 3700 0    25   ~ 0
All signals are VCCO35 signals.
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_GND #PWR?
U 1 1 5F0E92FF
P 850 2550
AR Path="/5EB87562/5F0E92FF" Ref="#PWR?"  Part="1" 
AR Path="/5E9110E2/5F0E92FF" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 850 2300 50  0001 C CNN
F 1 "MZ_GND" H 855 2377 50  0000 C CNN
F 2 "" H 850 2550 50  0001 C CNN
F 3 "" H 850 2550 50  0001 C CNN
	1    850  2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	850  2500 850  2550
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_GND #PWR?
U 1 1 5F1328EE
P 1350 2950
AR Path="/5EB87562/5F1328EE" Ref="#PWR?"  Part="1" 
AR Path="/5E9110E2/5F1328EE" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 1350 2700 50  0001 C CNN
F 1 "MZ_GND" H 1355 2777 50  0000 C CNN
F 2 "" H 1350 2950 50  0001 C CNN
F 3 "" H 1350 2950 50  0001 C CNN
	1    1350 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	1350 2900 1350 2950
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_GND #PWR?
U 1 1 5F194F96
P 3150 3025
AR Path="/5EB87562/5F194F96" Ref="#PWR?"  Part="1" 
AR Path="/5E9110E2/5F194F96" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 3150 2775 50  0001 C CNN
F 1 "MZ_GND" H 3155 2852 50  0000 C CNN
F 2 "" H 3150 3025 50  0001 C CNN
F 3 "" H 3150 3025 50  0001 C CNN
	1    3150 3025
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_GND #PWR?
U 1 1 5F1F7AE7
P 3750 2550
AR Path="/5EB87562/5F1F7AE7" Ref="#PWR?"  Part="1" 
AR Path="/5E9110E2/5F1F7AE7" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 3750 2300 50  0001 C CNN
F 1 "MZ_GND" H 3755 2377 50  0000 C CNN
F 2 "" H 3750 2550 50  0001 C CNN
F 3 "" H 3750 2550 50  0001 C CNN
	1    3750 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	3750 2500 3750 2550
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_GND #PWR?
U 1 1 5F2107EA
P 4275 2950
AR Path="/5EB87562/5F2107EA" Ref="#PWR?"  Part="1" 
AR Path="/5E9110E2/5F2107EA" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 4275 2700 50  0001 C CNN
F 1 "MZ_GND" H 4280 2777 50  0000 C CNN
F 2 "" H 4275 2950 50  0001 C CNN
F 3 "" H 4275 2950 50  0001 C CNN
	1    4275 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	4275 2900 4275 2950
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_GND #PWR?
U 1 1 5F2296B5
P 6050 3025
AR Path="/5EB87562/5F2296B5" Ref="#PWR?"  Part="1" 
AR Path="/5E9110E2/5F2296B5" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 6050 2775 50  0001 C CNN
F 1 "MZ_GND" H 6055 2852 50  0000 C CNN
F 2 "" H 6050 3025 50  0001 C CNN
F 3 "" H 6050 3025 50  0001 C CNN
	1    6050 3025
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_GND #PWR?
U 1 1 5F2A5D85
P 6050 6350
AR Path="/5EB87562/5F2A5D85" Ref="#PWR?"  Part="1" 
AR Path="/5E9110E2/5F2A5D85" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 6050 6100 50  0001 C CNN
F 1 "MZ_GND" H 6055 6177 50  0000 C CNN
F 2 "" H 6050 6350 50  0001 C CNN
F 3 "" H 6050 6350 50  0001 C CNN
	1    6050 6350
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_GND #PWR?
U 1 1 5F2D8363
P 3150 6350
AR Path="/5EB87562/5F2D8363" Ref="#PWR?"  Part="1" 
AR Path="/5E9110E2/5F2D8363" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 3150 6100 50  0001 C CNN
F 1 "MZ_GND" H 3155 6177 50  0000 C CNN
F 2 "" H 3150 6350 50  0001 C CNN
F 3 "" H 3150 6350 50  0001 C CNN
	1    3150 6350
	1    0    0    -1  
$EndComp
$EndSCHEMATC
