EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 4
Title "PMSM_Driver_MZ_Split_Board"
Date "2020-03-30"
Rev "A"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L PMSM_Driver_MZ_Split_Board_Mechanical:MountingHole_Pad H2
U 1 1 5EBC1BCE
P 10250 1050
F 0 "H2" H 10350 1099 50  0000 L CNN
F 1 "MntHolePad_M3" H 10350 1008 50  0000 L CNN
F 2 "PMSM_Driver_MZ_Split_Board:MountingHole_3.2mm_M3_Pad" H 10250 1050 50  0001 C CNN
F 3 "~" H 10250 1050 50  0001 C CNN
	1    10250 1050
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Mechanical:MountingHole_Pad H4
U 1 1 5EBC41EC
P 10250 1550
F 0 "H4" H 10350 1599 50  0000 L CNN
F 1 "MntHolePad_M3" H 10350 1508 50  0000 L CNN
F 2 "PMSM_Driver_MZ_Split_Board:MountingHole_3.2mm_M3_Pad" H 10250 1550 50  0001 C CNN
F 3 "~" H 10250 1550 50  0001 C CNN
	1    10250 1550
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Mechanical:MountingHole_Pad H1
U 1 1 5EBC58AE
P 9250 1050
F 0 "H1" H 9350 1099 50  0000 L CNN
F 1 "MntHolePad_M3" H 9350 1008 50  0000 L CNN
F 2 "PMSM_Driver_MZ_Split_Board:MountingHole_3.2mm_M3_Pad" H 9250 1050 50  0001 C CNN
F 3 "~" H 9250 1050 50  0001 C CNN
	1    9250 1050
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Mechanical:MountingHole_Pad H3
U 1 1 5EBC6390
P 9250 1550
F 0 "H3" H 9350 1599 50  0000 L CNN
F 1 "MntHolePad_M3" H 9350 1508 50  0000 L CNN
F 2 "PMSM_Driver_MZ_Split_Board:MountingHole_3.2mm_M3_Pad" H 9250 1550 50  0001 C CNN
F 3 "~" H 9250 1550 50  0001 C CNN
	1    9250 1550
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Mechanical:MountingHole H5
U 1 1 5EBC6568
P 9250 2250
F 0 "H5" H 9350 2296 50  0000 L CNN
F 1 "MntHole_M3" H 9350 2205 50  0000 L CNN
F 2 "PMSM_Driver_MZ_Split_Board:MountingHole_3.2mm_M3_DIN965" H 9250 2250 50  0001 C CNN
F 3 "~" H 9250 2250 50  0001 C CNN
	1    9250 2250
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Mechanical:MountingHole H6
U 1 1 5EBC79E7
P 10250 2250
F 0 "H6" H 10350 2296 50  0000 L CNN
F 1 "MntHole_M3" H 10350 2205 50  0000 L CNN
F 2 "PMSM_Driver_MZ_Split_Board:MountingHole_3.2mm_M3_DIN965" H 10250 2250 50  0001 C CNN
F 3 "~" H 10250 2250 50  0001 C CNN
	1    10250 2250
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Mechanical:MountingHole H7
U 1 1 5EBC7D19
P 9250 2750
F 0 "H7" H 9350 2796 50  0000 L CNN
F 1 "MntHole_M3" H 9350 2705 50  0000 L CNN
F 2 "PMSM_Driver_MZ_Split_Board:MountingHole_3.2mm_M3_DIN965" H 9250 2750 50  0001 C CNN
F 3 "~" H 9250 2750 50  0001 C CNN
	1    9250 2750
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Mechanical:MountingHole H8
U 1 1 5EBC808C
P 10250 2750
F 0 "H8" H 10350 2796 50  0000 L CNN
F 1 "MntHole_M3" H 10350 2705 50  0000 L CNN
F 2 "PMSM_Driver_MZ_Split_Board:MountingHole_3.2mm_M3_DIN965" H 10250 2750 50  0001 C CNN
F 3 "~" H 10250 2750 50  0001 C CNN
	1    10250 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	1700 1400 1800 1400
Wire Wire Line
	1700 1300 1800 1300
Wire Wire Line
	1800 1300 1800 1400
Text Notes 750  1225 0    50   ~ 0
12 V Input
Wire Wire Line
	2400 1200 2800 1200
Wire Wire Line
	3200 1200 3200 1400
Wire Wire Line
	2400 1200 2400 1400
NoConn ~ 3950 1400
Connection ~ 2800 1200
Wire Wire Line
	4850 1300 4950 1300
NoConn ~ 3950 1300
Wire Wire Line
	3950 1200 3600 1200
Wire Wire Line
	4850 1400 5300 1400
Text Notes 9050 800  0    100  ~ 0
Mounting Holes
Wire Notes Line
	11100 600  11100 2975
Wire Notes Line
	8975 2975 8975 600 
Wire Wire Line
	5450 1700 5450 1825
Wire Wire Line
	5150 1300 5300 1300
Wire Wire Line
	5300 1400 5300 1300
Wire Wire Line
	5300 1400 5450 1400
Wire Wire Line
	3600 1200 3600 1400
Wire Wire Line
	3200 1200 3600 1200
Connection ~ 3600 1200
Wire Wire Line
	6100 1725 6100 1825
Wire Wire Line
	5900 1400 6100 1400
Wire Wire Line
	6100 1400 6100 1525
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_5V0 #PWR0117
U 1 1 5EA0E447
P 7550 1100
F 0 "#PWR0117" H 7550 1350 50  0001 C CNN
F 1 "MZ_5V0" H 7551 1273 50  0000 C CNN
F 2 "" H 7550 1100 50  0001 C CNN
F 3 "" H 7550 1100 50  0001 C CNN
	1    7550 1100
	1    0    0    -1  
$EndComp
Connection ~ 6100 1400
Wire Wire Line
	6525 1725 6525 1825
Wire Wire Line
	6925 1725 6925 1825
Wire Wire Line
	6525 1400 6525 1525
Wire Wire Line
	6100 1400 6525 1400
Wire Wire Line
	6525 1400 6925 1400
Wire Wire Line
	6925 1400 6925 1525
Connection ~ 6525 1400
Wire Wire Line
	6925 1400 7025 1400
Wire Wire Line
	4850 1200 7025 1200
Connection ~ 6925 1400
Wire Wire Line
	7025 1400 7125 1400
Connection ~ 7025 1400
Wire Wire Line
	5450 1500 5450 1400
Wire Wire Line
	1700 1200 1800 1200
Wire Wire Line
	2000 1200 2100 1200
Connection ~ 3200 1200
Wire Wire Line
	2300 1200 2400 1200
Connection ~ 2400 1200
Wire Wire Line
	2800 1200 2800 1400
$Comp
L PMSM_Driver_MZ_Split_Board_Connector:Barrel_Jack_Switch J1
U 1 1 5E93141F
P 1400 1300
F 0 "J1" H 1250 1500 50  0000 C CNN
F 1 "Barrel_Jack_Switch" H 1250 1100 50  0000 C CNN
F 2 "PMSM_Driver_MZ_Split_Board:DC_Barrel_Jack_FC681465P" H 1400 1000 50  0001 C CNN
F 3 "~" H 1450 1260 50  0001 C CNN
F 4 "FC681465 -  DC Power Connector, Socket, 5 A, 2.5 mm, PCB Mount, Solder" H 1400 900 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/cliff-electronic-components/fc681465/dc-skt-dual-2-1-2-5mm-solder-dc10l/dp/1854514" H 1400 800 50  0001 C CNN "Distributor Link"
	1    1400 1300
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Device:Fuse_Small F1
U 1 1 5E933859
P 1900 1200
F 0 "F1" H 1900 1275 50  0000 C CNN
F 1 "5A" H 1900 1125 50  0000 C CNN
F 2 "PMSM_Driver_MZ_Split_Board:Fuse_1206_3216Metric" H 1900 1200 50  0001 C CNN
F 3 "~" H 1900 1200 50  0001 C CNN
F 4 "1206SFF500F/32-2 -  Fuse, Surface Mount, 5 A, 1206SFF Series, 32 V, Fast Acting, 1206" H 1900 1050 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/littelfuse/1206sff500f-32-2/fuse-fast-acting-5a-1206/dp/2786419" H 1900 950 50  0001 C CNN "Distributor Link"
	1    1900 1200
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_IC:LM2676S-5.0 U1
U 1 1 5E936F3A
P 4400 1300
F 0 "U1" H 4100 1550 50  0000 C CNN
F 1 "LM2676S-5.0" H 4700 1000 50  0000 C CNN
F 2 "PMSM_Driver_MZ_Split_Board:TO-263-7_TabPin4" H 4400 550 50  0001 C CNN
F 3 "~" H 3600 1600 50  0001 C CNN
F 4 "LM2676S-5.0/NOPB -  Buck (Step Down) Switching Regulator, Fixed, 8V-40V In, 5V And 3A Out, 260 kHz, TO-263-7" H 4400 450 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/texas-instruments/lm2676s-5-0-nopb/ic-step-down-volt-reg-7-to-263/dp/3122908" H 4400 350 50  0001 C CNN "Distributor Link"
	1    4400 1300
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Device:D_Schottky_Small D1
U 1 1 5E937EEA
P 2200 1200
F 0 "D1" H 2200 1100 50  0000 C CNN
F 1 "SK34" H 2200 1300 50  0000 C CNN
F 2 "PMSM_Driver_MZ_Split_Board:D_SMB_Handsoldering" V 2200 1200 50  0001 C CNN
F 3 "~" V 2200 1200 50  0001 C CNN
F 4 "SK34B -  Schottky Rectifier, Miniature, 40 V, 3 A, Single, DO-214AA (SMB), 2 Pins, 500 mV" H 2200 1000 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/taiwan-semiconductor/sk34b/diode-schottky-3a-40v-smb/dp/1299287" H 2200 900 50  0001 C CNN "Distributor Link"
	1    2200 1200
	-1   0    0    1   
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Device:C_Small C2
U 1 1 5E97506E
P 2800 1500
F 0 "C2" H 2892 1546 50  0000 L CNN
F 1 "10uF" H 2892 1455 50  0000 L CNN
F 2 "PMSM_Driver_MZ_Split_Board:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 2800 1500 50  0001 C CNN
F 3 "~" H 2800 1500 50  0001 C CNN
F 4 "TMK316BJ106ML-T -  SMD Multilayer Ceramic Capacitor, 10 µF, 25 V, 1206 [3216 Metric], ± 20%, X5R, M Series" H 2800 1100 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/taiyo-yuden/tmk316bj106ml-t/cap-10-f-25v-20-x5r-1206/dp/2112857" H 2800 1000 50  0001 C CNN "Distributor Link"
	1    2800 1500
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Device:CP1_Small C1
U 1 1 5E97586C
P 2400 1500
F 0 "C1" H 2491 1546 50  0000 L CNN
F 1 "220uF" H 2491 1455 50  0000 L CNN
F 2 "PMSM_Driver_MZ_Split_Board:CP_Elec_10x10.5" H 2400 1500 50  0001 C CNN
F 3 "~" H 2400 1500 50  0001 C CNN
F 4 "MCVVT035M221FB3L -  SMD Aluminium Electrolytic Capacitor, Radial Can - SMD, 220 µF, 35 V, VT, V-Chip Series" H 2400 1100 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcvvt035m221fb3l/cap-220-f-35v-smd/dp/2611386" H 2400 1000 50  0001 C CNN "Distributor Link"
F 6 "35V" H 2550 1375 50  0000 C CNN "Voltage"
	1    2400 1500
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Device:C_Small C3
U 1 1 5E97A6F7
P 3200 1500
F 0 "C3" H 3292 1546 50  0000 L CNN
F 1 "470nF" H 3292 1455 50  0000 L CNN
F 2 "PMSM_Driver_MZ_Split_Board:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 3200 1500 50  0001 C CNN
F 3 "~" H 3200 1500 50  0001 C CNN
F 4 "MC0805B474K250CT -  SMD Multilayer Ceramic Capacitor, 0.47 µF, 25 V, 0805 [2012 Metric], ± 10%, X7R, MC Series" H 3200 1100 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0805b474k250ct/cap-0-47-f-25v-10-x7r-0805/dp/2320831" H 3200 1000 50  0001 C CNN "Distributor Link"
	1    3200 1500
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Device:C_Small C4
U 1 1 5E97BD78
P 3600 1500
F 0 "C4" H 3692 1546 50  0000 L CNN
F 1 "100nF" H 3692 1455 50  0000 L CNN
F 2 "PMSM_Driver_MZ_Split_Board:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 3600 1500 50  0001 C CNN
F 3 "~" H 3600 1500 50  0001 C CNN
F 4 "C0805C104Z3VACTU -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 25 V, 0805 [2012 Metric], +80%, -20%, Y5V, C Series KEMET" H 3600 1100 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/kemet/c0805c104z3vactu/cap-0-1-f-25v-y5v-0805/dp/2522443" H 3600 1000 50  0001 C CNN "Distributor Link"
	1    3600 1500
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Device:D_Schottky_Small D2
U 1 1 5E97E71B
P 5450 1600
F 0 "D2" V 5450 1700 50  0000 C CNN
F 1 "SK34" V 5525 1725 50  0000 C CNN
F 2 "PMSM_Driver_MZ_Split_Board:D_SMB_Handsoldering" V 5450 1600 50  0001 C CNN
F 3 "~" V 5450 1600 50  0001 C CNN
F 4 "SK34B -  Schottky Rectifier, Miniature, 40 V, 3 A, Single, DO-214AA (SMB), 2 Pins, 500 mV" H 5450 1400 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/taiwan-semiconductor/sk34b/diode-schottky-3a-40v-smb/dp/1299287" H 5450 1300 50  0001 C CNN "Distributor Link"
	1    5450 1600
	0    1    1    0   
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Device:C_Small C5
U 1 1 5E980CFF
P 5050 1300
F 0 "C5" V 5100 1225 50  0000 C CNN
F 1 "10nF" V 5000 1175 50  0000 C CNN
F 2 "PMSM_Driver_MZ_Split_Board:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5050 1300 50  0001 C CNN
F 3 "~" H 5050 1300 50  0001 C CNN
F 4 "08051C103KAT2A -  SMD Multilayer Ceramic Capacitor, 10000 pF, 100 V, 0805 [2012 Metric], ± 10%, X7R" H 5050 900 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/avx/08051c103kat2a/cap-0-01-f-100v-10-x7r-0805/dp/1658874" H 5050 800 50  0001 C CNN "Distributor Link"
	1    5050 1300
	0    -1   -1   0   
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Device:L_Core_Ferrite_Small L1
U 1 1 5E9831A5
P 5800 1400
F 0 "L1" V 5900 1400 50  0000 C CNN
F 1 "33uH" V 5725 1400 50  0000 C CNN
F 2 "PMSM_Driver_MZ_Split_Board:L_Bourns_SRR1210A" H 5800 1400 50  0001 C CNN
F 3 "~" H 5800 1400 50  0001 C CNN
F 4 "SRR1210A-330M -  Power Inductor (SMD), 33 µH, 4.4 A, Shielded, 3.7 A, SRR1210A Series, 12mm x 12mm x 10mm" H 5800 1000 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/bourns/srr1210a-330m/inductor-33uh-20-4-4a-shielded/dp/2455078" H 5800 900 50  0001 C CNN "Distributor Link"
	1    5800 1400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5700 1400 5450 1400
Connection ~ 5450 1400
$Comp
L PMSM_Driver_MZ_Split_Board_Device:CP1_Small C6
U 1 1 5E9870DF
P 6100 1625
F 0 "C6" H 6191 1671 50  0000 L CNN
F 1 "220uF" H 6191 1580 50  0000 L CNN
F 2 "PMSM_Driver_MZ_Split_Board:CP_Elec_6.3x7.7" H 6100 1625 50  0001 C CNN
F 3 "~" H 6100 1625 50  0001 C CNN
F 4 "MCVVT016M221EA6L -  SMD Aluminium Electrolytic Capacitor, Radial Can - SMD, 220 µF, 16 V, VT, V-Chip Series" H 6100 1225 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcvvt016m221ea6l/cap-220-f-16v-smd/dp/2611360" H 6100 1125 50  0001 C CNN "Distributor Link"
F 6 "16V" H 6250 1500 50  0000 C CNN "Voltage"
	1    6100 1625
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Device:C_Small C7
U 1 1 5E988E91
P 6525 1625
F 0 "C7" H 6617 1671 50  0000 L CNN
F 1 "10uF" H 6617 1580 50  0000 L CNN
F 2 "PMSM_Driver_MZ_Split_Board:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6525 1625 50  0001 C CNN
F 3 "~" H 6525 1625 50  0001 C CNN
F 4 "CL21A106KOQNNNE -  SMD Multilayer Ceramic Capacitor, 10 µF, 16 V, 0805 [2012 Metric], ± 10%, X5R, CL Series" H 6525 1225 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/samsung-electro-mechanics/cl21a106koqnnne/cap-10uf-16v-mlcc-0805/dp/3013453" H 6525 1125 50  0001 C CNN "Distributor Link"
	1    6525 1625
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Device:C_Small C8
U 1 1 5E98AB9C
P 6925 1625
F 0 "C8" H 7017 1671 50  0000 L CNN
F 1 "100nF" H 7017 1580 50  0000 L CNN
F 2 "PMSM_Driver_MZ_Split_Board:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6925 1625 50  0001 C CNN
F 3 "~" H 6925 1625 50  0001 C CNN
F 4 "MC0805B104K160CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 16 V, 0805 [2012 Metric], ± 10%, X7R, MC Series" H 6925 1225 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0805b104k160ct/cap-0-1-f-16v-10-x7r-0805/dp/1759143" H 6925 1125 50  0001 C CNN "Distributor Link"
	1    6925 1625
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_12V0 #PWR0120
U 1 1 5E95BC9C
P 2400 1100
F 0 "#PWR0120" H 2400 1350 50  0001 C CNN
F 1 "MZ_12V0" H 2401 1273 50  0000 C CNN
F 2 "" H 2400 1100 50  0001 C CNN
F 3 "" H 2400 1100 50  0001 C CNN
	1    2400 1100
	1    0    0    -1  
$EndComp
Wire Wire Line
	2400 1200 2400 1100
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 5E9608C6
P 2875 1100
F 0 "#FLG0101" H 2875 1175 50  0001 C CNN
F 1 "PWR_FLAG" H 2875 1273 50  0000 C CNN
F 2 "" H 2875 1100 50  0001 C CNN
F 3 "~" H 2875 1100 50  0001 C CNN
	1    2875 1100
	1    0    0    -1  
$EndComp
Wire Wire Line
	2875 1200 2875 1100
$Comp
L power:PWR_FLAG #FLG0102
U 1 1 5E962089
P 7125 1100
F 0 "#FLG0102" H 7125 1175 50  0001 C CNN
F 1 "PWR_FLAG" H 7125 1273 50  0000 C CNN
F 2 "" H 7125 1100 50  0001 C CNN
F 3 "~" H 7125 1100 50  0001 C CNN
	1    7125 1100
	1    0    0    -1  
$EndComp
Connection ~ 7125 1400
Connection ~ 2875 1200
Wire Wire Line
	2875 1200 3200 1200
Wire Wire Line
	2800 1200 2875 1200
Wire Wire Line
	7125 1400 7550 1400
Text Notes 675  800  0    100  ~ 0
Main 5V Power Supply
$Comp
L PMSM_Driver_MZ_Split_Board_IC:LMR10510XMF U2
U 1 1 5EA09FD1
P 2250 2950
F 0 "U2" H 2100 3200 50  0000 C CNN
F 1 "LMR10510XMF" H 2550 2700 50  0000 C CNN
F 2 "PMSM_Driver_MZ_Split_Board:SOT-23-5_HandSoldering" H 2200 2250 50  0001 C CNN
F 3 "~" H 1900 3350 50  0001 C CNN
F 4 "LMR10510XMFE/NOPB -  DC-DC Switching Buck (Step Down) Regulator, Adjustable, 3V-5.5Vin, 600mV-4.5Vout, 1Aout, SOT-23-5" H 2200 2150 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/texas-instruments/lmr10510xmfe-nopb/dc-dc-conv-buck-1-6mhz-sot-23/dp/3007240" H 2200 2050 50  0001 C CNN "Distributor Link"
	1    2250 2950
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_5V0 #PWR0121
U 1 1 5EA11FF2
P 925 2750
F 0 "#PWR0121" H 925 3000 50  0001 C CNN
F 1 "MZ_5V0" H 926 2923 50  0000 C CNN
F 2 "" H 925 2750 50  0001 C CNN
F 3 "" H 925 2750 50  0001 C CNN
	1    925  2750
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Device:C_Small C9
U 1 1 5EA1460D
P 925 3250
F 0 "C9" H 1017 3296 50  0000 L CNN
F 1 "22uF" H 1017 3205 50  0000 L CNN
F 2 "PMSM_Driver_MZ_Split_Board:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 925 3250 50  0001 C CNN
F 3 "~" H 925 3250 50  0001 C CNN
F 4 "GRM21BR61A226ME51L -  SMD Multilayer Ceramic Capacitor, 22 µF, 10 V, 0805 [2012 Metric], ± 20%, X5R, GRM Series" H 925 2850 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/murata/grm21br61a226me51l/cap-22-f-10v-20-x5r-0805/dp/2494270" H 925 2750 50  0001 C CNN "Distributor Link"
F 6 "10V" H 1075 3125 50  0000 C CNN "Voltage"
	1    925  3250
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Device:C_Small C10
U 1 1 5EA19970
P 1350 3250
F 0 "C10" H 1442 3296 50  0000 L CNN
F 1 "100nF" H 1442 3205 50  0000 L CNN
F 2 "PMSM_Driver_MZ_Split_Board:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 1350 3250 50  0001 C CNN
F 3 "~" H 1350 3250 50  0001 C CNN
F 4 "MC0603B104K100CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 10 V, 0603 [1608 Metric], ± 10%, X7R, MC Series" H 1350 2850 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0603b104k100ct/cap-0-1-f-10v-10-x7r-0603/dp/2627427" H 1350 2750 50  0001 C CNN "Distributor Link"
	1    1350 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	925  2750 925  2850
Wire Wire Line
	925  2850 1350 2850
Connection ~ 925  2850
Wire Wire Line
	925  2850 925  3150
Wire Wire Line
	1350 3150 1350 2850
Wire Wire Line
	925  3350 925  3450
Wire Wire Line
	1350 3350 1350 3450
Wire Wire Line
	2250 3250 2250 3450
Connection ~ 1350 2850
Wire Wire Line
	2950 2950 2950 2850
$Comp
L PMSM_Driver_MZ_Split_Board_Device:D_Schottky_Small D3
U 1 1 5EA2F3F0
P 2950 3050
F 0 "D3" V 2950 3150 50  0000 C CNN
F 1 "MBR230LSFT1G" V 3025 3375 50  0000 C CNN
F 2 "PMSM_Driver_MZ_Split_Board:D_SOD-123F" V 2950 3050 50  0001 C CNN
F 3 "~" V 2950 3050 50  0001 C CNN
F 4 "MBR230LSFT1G -  Schottky Rectifier, 30 V, 2 A, Single, SOD-123FL, 2 Pins, 430 mV" H 2950 2850 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/on-semiconductor/mbr230lsft1g/diode-schottky-2a-30v-sod-123fl/dp/2317422" H 2950 2750 50  0001 C CNN "Distributor Link"
	1    2950 3050
	0    1    1    0   
$EndComp
Connection ~ 5300 1400
Wire Wire Line
	7025 1200 7025 1400
Wire Wire Line
	7125 1100 7125 1400
Wire Wire Line
	7550 1100 7550 1400
Wire Wire Line
	4400 1650 4400 1825
Wire Wire Line
	3600 1600 3600 1825
Wire Wire Line
	3200 1600 3200 1825
Wire Wire Line
	2800 1600 2800 1825
Wire Wire Line
	2400 1600 2400 1825
Connection ~ 1800 1400
Wire Wire Line
	2950 3150 2950 3450
Wire Wire Line
	2550 2850 2950 2850
Wire Wire Line
	2950 2850 3075 2850
Connection ~ 2950 2850
$Comp
L PMSM_Driver_MZ_Split_Board_Device:C_Small C11
U 1 1 5EA579B9
P 3650 3250
F 0 "C11" H 3742 3296 50  0000 L CNN
F 1 "22uF" H 3742 3205 50  0000 L CNN
F 2 "PMSM_Driver_MZ_Split_Board:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 3650 3250 50  0001 C CNN
F 3 "~" H 3650 3250 50  0001 C CNN
F 4 "GRM21BR61A226ME51L -  SMD Multilayer Ceramic Capacitor, 22 µF, 10 V, 0805 [2012 Metric], ± 20%, X5R, GRM Series" H 3650 2850 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/murata/grm21br61a226me51l/cap-22-f-10v-20-x5r-0805/dp/2494270" H 3650 2750 50  0001 C CNN "Distributor Link"
F 6 "10V" H 3800 3125 50  0000 C CNN "Voltage"
	1    3650 3250
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Device:C_Small C12
U 1 1 5EA579C1
P 4075 3250
F 0 "C12" H 4167 3296 50  0000 L CNN
F 1 "100nF" H 4167 3205 50  0000 L CNN
F 2 "PMSM_Driver_MZ_Split_Board:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4075 3250 50  0001 C CNN
F 3 "~" H 4075 3250 50  0001 C CNN
F 4 "MC0603B104K100CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 10 V, 0603 [1608 Metric], ± 10%, X7R, MC Series" H 4075 2850 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0603b104k100ct/cap-0-1-f-10v-10-x7r-0603/dp/2627427" H 4075 2750 50  0001 C CNN "Distributor Link"
	1    4075 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	3650 2850 3650 3150
Wire Wire Line
	4075 3150 4075 2850
Wire Wire Line
	3650 3350 3650 3450
Wire Wire Line
	4075 3350 4075 3450
Wire Wire Line
	3275 2850 3650 2850
Wire Wire Line
	3650 2850 3800 2850
Connection ~ 3650 2850
$Comp
L PMSM_Driver_MZ_Split_Board_Connector:PinHeader_02x03 JP1
U 1 1 5EA892F7
P 5575 3125
F 0 "JP1" H 5575 3325 50  0000 C CNN
F 1 "PinHeader_02x03" H 5575 2900 50  0000 C CNN
F 2 "PMSM_Driver_MZ_Split_Board:PinHeader_2x03_P2.54mm_Vertical" H 5575 2825 50  0001 C CNN
F 3 "~" H 5575 3125 50  0001 C CNN
F 4 "67996-406HLF -  Board-To-Board Connector, 2.54 mm, 6 Contacts, Header, FCI BergStik 67996 Series, Through Hole" H 5575 2725 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/amphenol-icc-fci/67996-406hlf/connector-header-6pos-2row-2-54mm/dp/2751377" H 5575 2625 50  0001 C CNN "Distributor Link"
	1    5575 3125
	1    0    0    -1  
$EndComp
Wire Wire Line
	5825 3225 5925 3225
Wire Wire Line
	5925 3225 5925 3125
Wire Wire Line
	5925 3125 5825 3125
Wire Wire Line
	5825 3025 5925 3025
Wire Wire Line
	5925 3025 5925 3125
Connection ~ 5925 3125
Wire Wire Line
	4800 3025 5250 3025
Wire Wire Line
	4800 3125 5325 3125
Wire Wire Line
	4800 3225 5325 3225
Wire Wire Line
	5250 3025 5250 2850
Wire Wire Line
	5250 2850 6050 2850
Wire Wire Line
	6050 2850 6050 3025
Wire Wire Line
	6050 3025 5925 3025
Connection ~ 5250 3025
Wire Wire Line
	5250 3025 5325 3025
Connection ~ 5925 3025
$Comp
L PMSM_Driver_MZ_Split_Board_Device:R_Small_US R4
U 1 1 5EAAC833
P 6150 3225
F 0 "R4" H 6075 3125 50  0000 C CNN
F 1 "10k" H 6025 3225 50  0000 C CNN
F 2 "PMSM_Driver_MZ_Split_Board:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 6150 3225 50  0001 C CNN
F 3 "~" H 6150 3225 50  0001 C CNN
F 4 "WF06R1002BTL -  SMD Chip Resistor, 0603 [1608 Metric], 10 kohm, WF06R Series, 75 V, Thin Film, 100 mW" H 6150 2825 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/walsin/wf06r1002btl/res-10k-0-1-75v-0603-thin-film/dp/2502926" H 6150 2725 50  0001 C CNN "Distributor Link"
F 6 "0.1%" H 6025 3325 50  0000 C CNN "Tolerance"
	1    6150 3225
	-1   0    0    1   
$EndComp
Wire Wire Line
	4600 3225 4475 3225
Wire Wire Line
	4475 3225 4475 3125
Wire Wire Line
	4475 3125 4600 3125
Wire Wire Line
	4475 3125 4475 3025
Wire Wire Line
	4475 3025 4600 3025
Connection ~ 4475 3125
$Comp
L PMSM_Driver_MZ_Split_Board_Device:C_Small C13
U 1 1 5EAB9389
P 4700 2850
F 0 "C13" V 4750 2900 50  0000 L CNN
F 1 "0" V 4750 2750 50  0000 L CNN
F 2 "PMSM_Driver_MZ_Split_Board:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 4700 2850 50  0001 C CNN
F 3 "~" H 4700 2850 50  0001 C CNN
F 4 "..." H 4700 2450 50  0001 C CNN "Description"
F 5 "..." H 4700 2350 50  0001 C CNN "Distributor Link"
F 6 "DNI" V 4650 2750 50  0000 C CNN "DNI"
	1    4700 2850
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4800 2850 5250 2850
Connection ~ 5250 2850
Wire Wire Line
	4600 2850 4475 2850
Wire Wire Line
	4475 2850 4475 3025
Connection ~ 4475 3025
Wire Wire Line
	6150 3125 6150 3025
Wire Wire Line
	4075 2850 4275 2850
Connection ~ 4075 2850
Connection ~ 4475 2850
Wire Wire Line
	6150 3325 6150 3450
$Comp
L PMSM_Driver_MZ_Split_Board_Device:C_Small C14
U 1 1 5EAD7B59
P 6475 3225
F 0 "C14" H 6300 3150 50  0000 L CNN
F 1 "0" H 6325 3225 50  0000 L CNN
F 2 "PMSM_Driver_MZ_Split_Board:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 6475 3225 50  0001 C CNN
F 3 "~" H 6475 3225 50  0001 C CNN
F 4 "" H 6475 2825 50  0001 C CNN "Description"
F 5 "" H 6475 2725 50  0001 C CNN "Distributor Link"
F 6 "DNI" H 6375 3300 50  0000 C CNN "DNI"
	1    6475 3225
	-1   0    0    1   
$EndComp
Wire Wire Line
	6050 3025 6150 3025
Connection ~ 6050 3025
Wire Wire Line
	6150 3025 6475 3025
Wire Wire Line
	6475 3025 6475 3125
Connection ~ 6150 3025
Wire Wire Line
	6475 3325 6475 3450
Wire Wire Line
	6050 2850 6050 2675
Wire Wire Line
	6050 2675 2650 2675
Wire Wire Line
	2650 2675 2650 3050
Wire Wire Line
	2650 3050 2550 3050
Connection ~ 6050 2850
Wire Notes Line
	600  600  600  2150
Wire Notes Line
	8900 600  8900 2150
Text Notes 5250 3625 0    25   ~ 0
Short 1-2   3.3V\nShort 3-4   2.5V\nShort 5-6   1.8V\nDefault is 3.3V
Text Notes 5250 3450 0    25   ~ 0
VCCO_34 Voltage Selection:
Text Notes 675  2425 0    100  ~ 0
VCCO_34 Power Supply (Bank 34)
Wire Notes Line
	6900 2225 6900 3775
Wire Notes Line
	600  2225 600  3775
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_VCCO_34 #PWR0130
U 1 1 5EB7A999
P 4275 2550
F 0 "#PWR0130" H 4275 2800 50  0001 C CNN
F 1 "MZ_VCCO_34" H 4276 2723 50  0000 C CNN
F 2 "" H 4275 2550 50  0001 C CNN
F 3 "" H 4275 2550 50  0001 C CNN
	1    4275 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	4275 2850 4275 2550
Connection ~ 4275 2850
Wire Wire Line
	4275 2850 4475 2850
$Comp
L PMSM_Driver_MZ_Split_Board_IC:LMR10510XMF U3
U 1 1 5E967729
P 2250 4575
F 0 "U3" H 2100 4825 50  0000 C CNN
F 1 "LMR10510XMF" H 2550 4325 50  0000 C CNN
F 2 "PMSM_Driver_MZ_Split_Board:SOT-23-5_HandSoldering" H 2200 3875 50  0001 C CNN
F 3 "~" H 1900 4975 50  0001 C CNN
F 4 "LMR10510XMFE/NOPB -  DC-DC Switching Buck (Step Down) Regulator, Adjustable, 3V-5.5Vin, 600mV-4.5Vout, 1Aout, SOT-23-5" H 2200 3775 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/texas-instruments/lmr10510xmfe-nopb/dc-dc-conv-buck-1-6mhz-sot-23/dp/3007240" H 2200 3675 50  0001 C CNN "Distributor Link"
	1    2250 4575
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_5V0 #PWR0131
U 1 1 5E96772F
P 925 4375
F 0 "#PWR0131" H 925 4625 50  0001 C CNN
F 1 "MZ_5V0" H 926 4548 50  0000 C CNN
F 2 "" H 925 4375 50  0001 C CNN
F 3 "" H 925 4375 50  0001 C CNN
	1    925  4375
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Device:C_Small C15
U 1 1 5E96773E
P 925 4875
F 0 "C15" H 1017 4921 50  0000 L CNN
F 1 "22uF" H 1017 4830 50  0000 L CNN
F 2 "PMSM_Driver_MZ_Split_Board:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 925 4875 50  0001 C CNN
F 3 "~" H 925 4875 50  0001 C CNN
F 4 "GRM21BR61A226ME51L -  SMD Multilayer Ceramic Capacitor, 22 µF, 10 V, 0805 [2012 Metric], ± 20%, X5R, GRM Series" H 925 4475 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/murata/grm21br61a226me51l/cap-22-f-10v-20-x5r-0805/dp/2494270" H 925 4375 50  0001 C CNN "Distributor Link"
F 6 "10V" H 1075 4750 50  0000 C CNN "Voltage"
	1    925  4875
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Device:C_Small C16
U 1 1 5E967746
P 1350 4875
F 0 "C16" H 1442 4921 50  0000 L CNN
F 1 "100nF" H 1442 4830 50  0000 L CNN
F 2 "PMSM_Driver_MZ_Split_Board:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 1350 4875 50  0001 C CNN
F 3 "~" H 1350 4875 50  0001 C CNN
F 4 "MC0603B104K100CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 10 V, 0603 [1608 Metric], ± 10%, X7R, MC Series" H 1350 4475 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0603b104k100ct/cap-0-1-f-10v-10-x7r-0603/dp/2627427" H 1350 4375 50  0001 C CNN "Distributor Link"
	1    1350 4875
	1    0    0    -1  
$EndComp
Wire Wire Line
	925  4375 925  4475
Wire Wire Line
	925  4475 1350 4475
Connection ~ 925  4475
Wire Wire Line
	925  4475 925  4775
Wire Wire Line
	1350 4775 1350 4475
Wire Wire Line
	925  4975 925  5075
Wire Wire Line
	1350 4975 1350 5075
Wire Wire Line
	2250 4875 2250 5075
Connection ~ 1350 4475
Wire Wire Line
	2950 4575 2950 4475
$Comp
L PMSM_Driver_MZ_Split_Board_Device:D_Schottky_Small D4
U 1 1 5E967775
P 2950 4675
F 0 "D4" V 2950 4775 50  0000 C CNN
F 1 "MBR230LSFT1G" V 3025 5000 50  0000 C CNN
F 2 "PMSM_Driver_MZ_Split_Board:D_SOD-123F" V 2950 4675 50  0001 C CNN
F 3 "~" V 2950 4675 50  0001 C CNN
F 4 "MBR230LSFT1G -  Schottky Rectifier, 30 V, 2 A, Single, SOD-123FL, 2 Pins, 430 mV" H 2950 4475 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/on-semiconductor/mbr230lsft1g/diode-schottky-2a-30v-sod-123fl/dp/2317422" H 2950 4375 50  0001 C CNN "Distributor Link"
	1    2950 4675
	0    1    1    0   
$EndComp
Wire Wire Line
	2950 4775 2950 5075
Wire Wire Line
	2550 4475 2950 4475
Wire Wire Line
	2950 4475 3075 4475
Connection ~ 2950 4475
$Comp
L PMSM_Driver_MZ_Split_Board_Device:C_Small C17
U 1 1 5E967782
P 3650 4875
F 0 "C17" H 3742 4921 50  0000 L CNN
F 1 "22uF" H 3742 4830 50  0000 L CNN
F 2 "PMSM_Driver_MZ_Split_Board:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 3650 4875 50  0001 C CNN
F 3 "~" H 3650 4875 50  0001 C CNN
F 4 "GRM21BR61A226ME51L -  SMD Multilayer Ceramic Capacitor, 22 µF, 10 V, 0805 [2012 Metric], ± 20%, X5R, GRM Series" H 3650 4475 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/murata/grm21br61a226me51l/cap-22-f-10v-20-x5r-0805/dp/2494270" H 3650 4375 50  0001 C CNN "Distributor Link"
F 6 "10V" H 3800 4750 50  0000 C CNN "Voltage"
	1    3650 4875
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Device:C_Small C18
U 1 1 5E96778A
P 4075 4875
F 0 "C18" H 4167 4921 50  0000 L CNN
F 1 "100nF" H 4167 4830 50  0000 L CNN
F 2 "PMSM_Driver_MZ_Split_Board:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4075 4875 50  0001 C CNN
F 3 "~" H 4075 4875 50  0001 C CNN
F 4 "MC0603B104K100CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 10 V, 0603 [1608 Metric], ± 10%, X7R, MC Series" H 4075 4475 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0603b104k100ct/cap-0-1-f-10v-10-x7r-0603/dp/2627427" H 4075 4375 50  0001 C CNN "Distributor Link"
	1    4075 4875
	1    0    0    -1  
$EndComp
Wire Wire Line
	3650 4475 3650 4775
Wire Wire Line
	4075 4775 4075 4475
Wire Wire Line
	3650 4975 3650 5075
Wire Wire Line
	4075 4975 4075 5075
Wire Wire Line
	3275 4475 3650 4475
Wire Wire Line
	3650 4475 3800 4475
Connection ~ 3650 4475
Wire Wire Line
	5825 4850 5925 4850
Wire Wire Line
	5925 4850 5925 4750
Wire Wire Line
	5925 4750 5825 4750
Wire Wire Line
	5825 4650 5925 4650
Wire Wire Line
	5925 4650 5925 4750
Connection ~ 5925 4750
Wire Wire Line
	4800 4650 5250 4650
Wire Wire Line
	4800 4750 5325 4750
Wire Wire Line
	4800 4850 5325 4850
Wire Wire Line
	5250 4650 5250 4475
Wire Wire Line
	5250 4475 6050 4475
Wire Wire Line
	6050 4475 6050 4650
Wire Wire Line
	6050 4650 5925 4650
Connection ~ 5250 4650
Wire Wire Line
	5250 4650 5325 4650
Connection ~ 5925 4650
$Comp
L PMSM_Driver_MZ_Split_Board_Device:R_Small_US R8
U 1 1 5E9677D0
P 6150 4850
F 0 "R8" H 6075 4750 50  0000 C CNN
F 1 "10k" H 6025 4850 50  0000 C CNN
F 2 "PMSM_Driver_MZ_Split_Board:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 6150 4850 50  0001 C CNN
F 3 "~" H 6150 4850 50  0001 C CNN
F 4 "WF06R1002BTL -  SMD Chip Resistor, 0603 [1608 Metric], 10 kohm, WF06R Series, 75 V, Thin Film, 100 mW" H 6150 4450 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/walsin/wf06r1002btl/res-10k-0-1-75v-0603-thin-film/dp/2502926" H 6150 4350 50  0001 C CNN "Distributor Link"
F 6 "0.1%" H 6025 4950 50  0000 C CNN "Tolerance"
	1    6150 4850
	-1   0    0    1   
$EndComp
Wire Wire Line
	4600 4850 4475 4850
Wire Wire Line
	4475 4850 4475 4750
Wire Wire Line
	4475 4750 4600 4750
Wire Wire Line
	4475 4750 4475 4650
Wire Wire Line
	4475 4650 4600 4650
Connection ~ 4475 4750
$Comp
L PMSM_Driver_MZ_Split_Board_Device:C_Small C19
U 1 1 5E9677E8
P 4700 4475
F 0 "C19" V 4750 4525 50  0000 L CNN
F 1 "0" V 4750 4375 50  0000 L CNN
F 2 "PMSM_Driver_MZ_Split_Board:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 4700 4475 50  0001 C CNN
F 3 "~" H 4700 4475 50  0001 C CNN
F 4 "..." H 4700 4075 50  0001 C CNN "Description"
F 5 "..." H 4700 3975 50  0001 C CNN "Distributor Link"
F 6 "DNI" V 4650 4375 50  0000 C CNN "DNI"
	1    4700 4475
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4800 4475 5250 4475
Connection ~ 5250 4475
Wire Wire Line
	4600 4475 4475 4475
Wire Wire Line
	4475 4475 4475 4650
Connection ~ 4475 4650
Wire Wire Line
	6150 4750 6150 4650
Wire Wire Line
	4075 4475 4275 4475
Connection ~ 4075 4475
Connection ~ 4475 4475
Wire Wire Line
	6150 4950 6150 5075
$Comp
L PMSM_Driver_MZ_Split_Board_Device:C_Small C20
U 1 1 5E967801
P 6475 4850
F 0 "C20" H 6300 4775 50  0000 L CNN
F 1 "0" H 6325 4850 50  0000 L CNN
F 2 "PMSM_Driver_MZ_Split_Board:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 6475 4850 50  0001 C CNN
F 3 "~" H 6475 4850 50  0001 C CNN
F 4 "" H 6475 4450 50  0001 C CNN "Description"
F 5 "" H 6475 4350 50  0001 C CNN "Distributor Link"
F 6 "DNI" H 6375 4925 50  0000 C CNN "DNI"
	1    6475 4850
	-1   0    0    1   
$EndComp
Wire Wire Line
	6050 4650 6150 4650
Connection ~ 6050 4650
Wire Wire Line
	6150 4650 6475 4650
Wire Wire Line
	6475 4650 6475 4750
Connection ~ 6150 4650
Wire Wire Line
	6475 4950 6475 5075
Wire Wire Line
	6050 4475 6050 4300
Wire Wire Line
	6050 4300 2650 4300
Wire Wire Line
	2650 4300 2650 4675
Wire Wire Line
	2650 4675 2550 4675
Connection ~ 6050 4475
Text Notes 5250 5250 0    25   ~ 0
Short 1-2   3.3V\nShort 3-4   2.5V\nShort 5-6   1.8V\nDefault is 3.3V
Text Notes 5250 5075 0    25   ~ 0
VCCO_35 Voltage Selection:
Text Notes 675  4050 0    100  ~ 0
VCCO_35 Power Supply (Bank 35)
Wire Notes Line
	6900 3850 6900 5400
Wire Notes Line
	600  3850 600  5400
Wire Wire Line
	4275 4475 4275 4175
Connection ~ 4275 4475
Wire Wire Line
	4275 4475 4475 4475
$Comp
L PMSM_Driver_MZ_Split_Board_IC:LMR10510XMF U4
U 1 1 5E9A0329
P 2150 6475
F 0 "U4" H 2000 6725 50  0000 C CNN
F 1 "LMR10510XMF" H 2450 6225 50  0000 C CNN
F 2 "PMSM_Driver_MZ_Split_Board:SOT-23-5_HandSoldering" H 2100 5775 50  0001 C CNN
F 3 "~" H 1800 6875 50  0001 C CNN
F 4 "LMR10510XMFE/NOPB -  DC-DC Switching Buck (Step Down) Regulator, Adjustable, 3V-5.5Vin, 600mV-4.5Vout, 1Aout, SOT-23-5" H 2100 5675 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/texas-instruments/lmr10510xmfe-nopb/dc-dc-conv-buck-1-6mhz-sot-23/dp/3007240" H 2100 5575 50  0001 C CNN "Distributor Link"
	1    2150 6475
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_5V0 #PWR0141
U 1 1 5E9A032F
P 825 6275
F 0 "#PWR0141" H 825 6525 50  0001 C CNN
F 1 "MZ_5V0" H 826 6448 50  0000 C CNN
F 2 "" H 825 6275 50  0001 C CNN
F 3 "" H 825 6275 50  0001 C CNN
	1    825  6275
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Device:C_Small C21
U 1 1 5E9A033E
P 825 6775
F 0 "C21" H 917 6821 50  0000 L CNN
F 1 "22uF" H 917 6730 50  0000 L CNN
F 2 "PMSM_Driver_MZ_Split_Board:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 825 6775 50  0001 C CNN
F 3 "~" H 825 6775 50  0001 C CNN
F 4 "GRM21BR61A226ME51L -  SMD Multilayer Ceramic Capacitor, 22 µF, 10 V, 0805 [2012 Metric], ± 20%, X5R, GRM Series" H 825 6375 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/murata/grm21br61a226me51l/cap-22-f-10v-20-x5r-0805/dp/2494270" H 825 6275 50  0001 C CNN "Distributor Link"
F 6 "10V" H 975 6650 50  0000 C CNN "Voltage"
	1    825  6775
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Device:C_Small C22
U 1 1 5E9A0346
P 1250 6775
F 0 "C22" H 1342 6821 50  0000 L CNN
F 1 "100nF" H 1342 6730 50  0000 L CNN
F 2 "PMSM_Driver_MZ_Split_Board:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 1250 6775 50  0001 C CNN
F 3 "~" H 1250 6775 50  0001 C CNN
F 4 "MC0603B104K100CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 10 V, 0603 [1608 Metric], ± 10%, X7R, MC Series" H 1250 6375 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0603b104k100ct/cap-0-1-f-10v-10-x7r-0603/dp/2627427" H 1250 6275 50  0001 C CNN "Distributor Link"
	1    1250 6775
	1    0    0    -1  
$EndComp
Wire Wire Line
	825  6275 825  6375
Wire Wire Line
	825  6375 1250 6375
Connection ~ 825  6375
Wire Wire Line
	825  6375 825  6675
Wire Wire Line
	1250 6675 1250 6375
Wire Wire Line
	825  6875 825  6975
Wire Wire Line
	1250 6875 1250 6975
Wire Wire Line
	2150 6775 2150 6975
Wire Wire Line
	2850 6475 2850 6375
$Comp
L PMSM_Driver_MZ_Split_Board_Device:D_Schottky_Small D5
U 1 1 5E9A0375
P 2850 6575
F 0 "D5" V 2850 6675 50  0000 C CNN
F 1 "MBR230LSFT1G" V 2925 6900 50  0000 C CNN
F 2 "PMSM_Driver_MZ_Split_Board:D_SOD-123F" V 2850 6575 50  0001 C CNN
F 3 "~" V 2850 6575 50  0001 C CNN
F 4 "MBR230LSFT1G -  Schottky Rectifier, 30 V, 2 A, Single, SOD-123FL, 2 Pins, 430 mV" H 2850 6375 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/on-semiconductor/mbr230lsft1g/diode-schottky-2a-30v-sod-123fl/dp/2317422" H 2850 6275 50  0001 C CNN "Distributor Link"
	1    2850 6575
	0    1    1    0   
$EndComp
Wire Wire Line
	2850 6675 2850 6975
Wire Wire Line
	2450 6375 2850 6375
Wire Wire Line
	2850 6375 2975 6375
Connection ~ 2850 6375
$Comp
L PMSM_Driver_MZ_Split_Board_Device:C_Small C23
U 1 1 5E9A0382
P 3550 6775
F 0 "C23" H 3642 6821 50  0000 L CNN
F 1 "22uF" H 3642 6730 50  0000 L CNN
F 2 "PMSM_Driver_MZ_Split_Board:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 3550 6775 50  0001 C CNN
F 3 "~" H 3550 6775 50  0001 C CNN
F 4 "GRM21BR61A226ME51L -  SMD Multilayer Ceramic Capacitor, 22 µF, 10 V, 0805 [2012 Metric], ± 20%, X5R, GRM Series" H 3550 6375 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/murata/grm21br61a226me51l/cap-22-f-10v-20-x5r-0805/dp/2494270" H 3550 6275 50  0001 C CNN "Distributor Link"
F 6 "10V" H 3700 6650 50  0000 C CNN "Voltage"
	1    3550 6775
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Device:C_Small C24
U 1 1 5E9A038A
P 3975 6775
F 0 "C24" H 4067 6821 50  0000 L CNN
F 1 "100nF" H 4067 6730 50  0000 L CNN
F 2 "PMSM_Driver_MZ_Split_Board:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 3975 6775 50  0001 C CNN
F 3 "~" H 3975 6775 50  0001 C CNN
F 4 "MC0603B104K100CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 10 V, 0603 [1608 Metric], ± 10%, X7R, MC Series" H 3975 6375 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0603b104k100ct/cap-0-1-f-10v-10-x7r-0603/dp/2627427" H 3975 6275 50  0001 C CNN "Distributor Link"
	1    3975 6775
	1    0    0    -1  
$EndComp
Wire Wire Line
	3550 6375 3550 6675
Wire Wire Line
	3975 6675 3975 6375
Wire Wire Line
	3550 6875 3550 6975
Wire Wire Line
	3975 6875 3975 6975
Wire Wire Line
	3175 6375 3550 6375
Wire Wire Line
	3550 6375 3700 6375
Connection ~ 3550 6375
Wire Wire Line
	4700 6550 4950 6550
Wire Wire Line
	5150 6550 5150 6375
Wire Wire Line
	4375 6550 4500 6550
$Comp
L PMSM_Driver_MZ_Split_Board_Device:C_Small C25
U 1 1 5E9A03E8
P 4600 6375
F 0 "C25" V 4650 6425 50  0000 L CNN
F 1 "0" V 4650 6275 50  0000 L CNN
F 2 "PMSM_Driver_MZ_Split_Board:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 4600 6375 50  0001 C CNN
F 3 "~" H 4600 6375 50  0001 C CNN
F 4 "..." H 4600 5975 50  0001 C CNN "Description"
F 5 "..." H 4600 5875 50  0001 C CNN "Distributor Link"
F 6 "DNI" V 4550 6275 50  0000 C CNN "DNI"
	1    4600 6375
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4700 6375 5150 6375
Wire Wire Line
	4500 6375 4375 6375
Wire Wire Line
	4375 6375 4375 6550
Wire Wire Line
	4950 6650 4950 6550
Wire Wire Line
	3975 6375 4175 6375
Connection ~ 3975 6375
Connection ~ 4375 6375
Wire Wire Line
	4950 6850 4950 6975
$Comp
L PMSM_Driver_MZ_Split_Board_Device:C_Small C26
U 1 1 5E9A0401
P 5300 6750
F 0 "C26" H 5125 6675 50  0000 L CNN
F 1 "0" H 5150 6750 50  0000 L CNN
F 2 "PMSM_Driver_MZ_Split_Board:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 5300 6750 50  0001 C CNN
F 3 "~" H 5300 6750 50  0001 C CNN
F 4 "" H 5300 6350 50  0001 C CNN "Description"
F 5 "" H 5300 6250 50  0001 C CNN "Distributor Link"
F 6 "DNI" H 5200 6825 50  0000 C CNN "DNI"
	1    5300 6750
	-1   0    0    1   
$EndComp
Wire Wire Line
	5300 6550 5300 6650
Wire Wire Line
	5300 6850 5300 6975
Wire Wire Line
	2550 6200 2550 6575
Wire Wire Line
	2550 6575 2450 6575
Text Notes 675  5675 0    100  ~ 0
3V3 Power Supply
Wire Wire Line
	4175 6375 4175 6075
Connection ~ 4175 6375
Wire Wire Line
	4175 6375 4375 6375
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_3V3 #PWR0150
U 1 1 5E9CA49E
P 4175 6075
F 0 "#PWR0150" H 4175 6325 50  0001 C CNN
F 1 "MZ_3V3" H 4176 6248 50  0000 C CNN
F 2 "" H 4175 6075 50  0001 C CNN
F 3 "" H 4175 6075 50  0001 C CNN
	1    4175 6075
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG0103
U 1 1 5E9EA338
P 3800 2550
F 0 "#FLG0103" H 3800 2625 50  0001 C CNN
F 1 "PWR_FLAG" H 3800 2723 50  0000 C CNN
F 2 "" H 3800 2550 50  0001 C CNN
F 3 "~" H 3800 2550 50  0001 C CNN
	1    3800 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	3800 2550 3800 2850
Connection ~ 3800 2850
Wire Wire Line
	3800 2850 4075 2850
$Comp
L power:PWR_FLAG #FLG0104
U 1 1 5EA14E87
P 3800 4175
F 0 "#FLG0104" H 3800 4250 50  0001 C CNN
F 1 "PWR_FLAG" H 3800 4348 50  0000 C CNN
F 2 "" H 3800 4175 50  0001 C CNN
F 3 "~" H 3800 4175 50  0001 C CNN
	1    3800 4175
	1    0    0    -1  
$EndComp
Wire Wire Line
	3800 4175 3800 4475
Connection ~ 3800 4475
Wire Wire Line
	3800 4475 4075 4475
$Comp
L power:PWR_FLAG #FLG0105
U 1 1 5EA2A997
P 3700 6075
F 0 "#FLG0105" H 3700 6150 50  0001 C CNN
F 1 "PWR_FLAG" H 3700 6248 50  0000 C CNN
F 2 "" H 3700 6075 50  0001 C CNN
F 3 "~" H 3700 6075 50  0001 C CNN
	1    3700 6075
	1    0    0    -1  
$EndComp
Wire Wire Line
	3700 6075 3700 6375
Connection ~ 3700 6375
Wire Wire Line
	3700 6375 3975 6375
Connection ~ 5150 6550
Connection ~ 4950 6550
Wire Wire Line
	4950 6550 5150 6550
Wire Wire Line
	5150 6550 5300 6550
Wire Wire Line
	5300 6550 5300 6200
Wire Wire Line
	5300 6200 2550 6200
Connection ~ 5300 6550
$Comp
L PMSM_Driver_MZ_Split_Board_Device:L_Core_Ferrite_Small L4
U 1 1 5E9A0366
P 3075 6375
F 0 "L4" V 3175 6375 50  0000 C CNN
F 1 "2.2uH" V 3000 6375 50  0000 C CNN
F 2 "PMSM_Driver_MZ_Split_Board:L_Bourns-SRN6028" H 3075 6375 50  0001 C CNN
F 3 "~" H 3075 6375 50  0001 C CNN
F 4 "SRN6028-2R2Y -  Power Inductor (SMD), 2.2 µH, 3.7 A, Semishielded, 4.2 A, SRN6028 Series, 6mm x 6mm x 2.8mm" H 3075 5975 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/bourns/srn6028-2r2y/inductor-2-2uh-30-3-7a-semi-shld/dp/2428242" H 3075 5875 50  0001 C CNN "Distributor Link"
	1    3075 6375
	0    -1   -1   0   
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Device:L_Core_Ferrite_Small L3
U 1 1 5E967766
P 3175 4475
F 0 "L3" V 3275 4475 50  0000 C CNN
F 1 "2.2uH" V 3100 4475 50  0000 C CNN
F 2 "PMSM_Driver_MZ_Split_Board:L_Bourns-SRN6028" H 3175 4475 50  0001 C CNN
F 3 "~" H 3175 4475 50  0001 C CNN
F 4 "SRN6028-2R2Y -  Power Inductor (SMD), 2.2 µH, 3.7 A, Semishielded, 4.2 A, SRN6028 Series, 6mm x 6mm x 2.8mm" H 3175 4075 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/bourns/srn6028-2r2y/inductor-2-2uh-30-3-7a-semi-shld/dp/2428242" H 3175 3975 50  0001 C CNN "Distributor Link"
	1    3175 4475
	0    -1   -1   0   
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Device:L_Core_Ferrite_Small L2
U 1 1 5EA2E0E9
P 3175 2850
F 0 "L2" V 3275 2850 50  0000 C CNN
F 1 "2.2uH" V 3100 2850 50  0000 C CNN
F 2 "PMSM_Driver_MZ_Split_Board:L_Bourns-SRN6028" H 3175 2850 50  0001 C CNN
F 3 "~" H 3175 2850 50  0001 C CNN
F 4 "SRN6028-2R2Y -  Power Inductor (SMD), 2.2 µH, 3.7 A, Semishielded, 4.2 A, SRN6028 Series, 6mm x 6mm x 2.8mm" H 3175 2450 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/bourns/srn6028-2r2y/inductor-2-2uh-30-3-7a-semi-shld/dp/2428242" H 3175 2350 50  0001 C CNN "Distributor Link"
	1    3175 2850
	0    -1   -1   0   
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_VCCO_35 #PWR0140
U 1 1 5EAF74E1
P 4275 4175
F 0 "#PWR0140" H 4275 4425 50  0001 C CNN
F 1 "MZ_VCCO_35" H 4276 4348 50  0000 C CNN
F 2 "" H 4275 4175 50  0001 C CNN
F 3 "" H 4275 4175 50  0001 C CNN
	1    4275 4175
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Connector:PinHeader_02x03 JP2
U 1 1 5E9677A5
P 5575 4750
F 0 "JP2" H 5575 4950 50  0000 C CNN
F 1 "PinHeader_02x03" H 5575 4525 50  0000 C CNN
F 2 "PMSM_Driver_MZ_Split_Board:PinHeader_2x03_P2.54mm_Vertical" H 5575 4450 50  0001 C CNN
F 3 "~" H 5575 4750 50  0001 C CNN
F 4 "67996-406HLF -  Board-To-Board Connector, 2.54 mm, 6 Contacts, Header, FCI BergStik 67996 Series, Through Hole" H 5575 4350 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/amphenol-icc-fci/67996-406hlf/connector-header-6pos-2row-2-54mm/dp/2751377" H 5575 4250 50  0001 C CNN "Distributor Link"
	1    5575 4750
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Device:R_Small_US R10
U 1 1 5E9A03D0
P 4950 6750
F 0 "R10" H 4875 6650 50  0000 C CNN
F 1 "10k" H 4825 6750 50  0000 C CNN
F 2 "PMSM_Driver_MZ_Split_Board:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4950 6750 50  0001 C CNN
F 3 "~" H 4950 6750 50  0001 C CNN
F 4 "WF06R1002BTL -  SMD Chip Resistor, 0603 [1608 Metric], 10 kohm, WF06R Series, 75 V, Thin Film, 100 mW" H 4950 6350 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/walsin/wf06r1002btl/res-10k-0-1-75v-0603-thin-film/dp/2502926" H 4950 6250 50  0001 C CNN "Distributor Link"
F 6 "0.1%" H 4825 6850 50  0000 C CNN "Tolerance"
	1    4950 6750
	-1   0    0    1   
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Device:R_Small_US R9
U 1 1 5E9A03AE
P 4600 6550
F 0 "R9" V 4550 6425 50  0000 C CNN
F 1 "45k3" V 4550 6700 50  0000 C CNN
F 2 "PMSM_Driver_MZ_Split_Board:R_0402_1005Metric" H 4600 6550 50  0001 C CNN
F 3 "~" H 4600 6550 50  0001 C CNN
F 4 "RN73C1E45K3BTDF -  SMD Chip Resistor, Precision, 0402 [1005 Metric], 45.3 kohm, RN73 Series, 25 V, Thin Film, 63 mW" H 4600 6150 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/te-connectivity/rn73c1e45k3btdf/res-45k3-0-1-0-063w-0402-thin/dp/2688369" H 4600 6050 50  0001 C CNN "Distributor Link"
F 6 "0.1%" V 4550 6900 50  0000 C CNN "Tolerance"
	1    4600 6550
	0    1    1    0   
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Device:R_Small_US R3
U 1 1 5EA97C5A
P 4700 3225
F 0 "R3" V 4650 3100 50  0000 C CNN
F 1 "36k" V 4650 3375 50  0000 C CNN
F 2 "PMSM_Driver_MZ_Split_Board:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4700 3225 50  0001 C CNN
F 3 "~" H 4700 3225 50  0001 C CNN
F 4 "MCWF06R3602BTL -  SMD Chip Resistor, 0603 [1608 Metric], 36 kohm, MCWF06R Series, 75 V, Thin Film, 100 mW" H 4700 2825 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwf06r3602btl/res-36k-0-1-0-1w-0603-thin-film/dp/2337155" H 4700 2725 50  0001 C CNN "Distributor Link"
F 6 "0.1%" V 4650 3575 50  0000 C CNN "Tolerance"
	1    4700 3225
	0    1    1    0   
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Device:R_Small_US R2
U 1 1 5EA964E6
P 4700 3125
F 0 "R2" V 4650 3000 50  0000 C CNN
F 1 "107k" V 4650 3275 50  0000 C CNN
F 2 "PMSM_Driver_MZ_Split_Board:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4700 3125 50  0001 C CNN
F 3 "~" H 4700 3125 50  0001 C CNN
F 4 "MCTC0525B1073T5G -  SMD Chip Resistor, 0805 [2012 Metric], 107 kohm, 150 V, Thin Film, 125 mW" H 4700 2725 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mctc0525b1073t5g/res-107k-0-1-0-125w-0805-thin/dp/1576017" H 4700 2625 50  0001 C CNN "Distributor Link"
F 6 "0.1%" V 4650 3475 50  0000 C CNN "Tolerance"
	1    4700 3125
	0    1    1    0   
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Device:R_Small_US R1
U 1 1 5EA90353
P 4700 3025
F 0 "R1" V 4650 2900 50  0000 C CNN
F 1 "45k3" V 4650 3175 50  0000 C CNN
F 2 "PMSM_Driver_MZ_Split_Board:R_0402_1005Metric" H 4700 3025 50  0001 C CNN
F 3 "~" H 4700 3025 50  0001 C CNN
F 4 "RN73C1E45K3BTDF -  SMD Chip Resistor, Precision, 0402 [1005 Metric], 45.3 kohm, RN73 Series, 25 V, Thin Film, 63 mW" H 4700 2625 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/te-connectivity/rn73c1e45k3btdf/res-45k3-0-1-0-063w-0402-thin/dp/2688369" H 4700 2525 50  0001 C CNN "Distributor Link"
F 6 "0.1%" V 4650 3375 50  0000 C CNN "Tolerance"
	1    4700 3025
	0    1    1    0   
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Device:R_Small_US R7
U 1 1 5E9677B7
P 4700 4850
F 0 "R7" V 4650 4725 50  0000 C CNN
F 1 "36k" V 4650 5000 50  0000 C CNN
F 2 "PMSM_Driver_MZ_Split_Board:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4700 4850 50  0001 C CNN
F 3 "~" H 4700 4850 50  0001 C CNN
F 4 "MCWF06R3602BTL -  SMD Chip Resistor, 0603 [1608 Metric], 36 kohm, MCWF06R Series, 75 V, Thin Film, 100 mW" H 4700 4450 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwf06r3602btl/res-36k-0-1-0-1w-0603-thin-film/dp/2337155" H 4700 4350 50  0001 C CNN "Distributor Link"
F 6 "0.1%" V 4650 5200 50  0000 C CNN "Tolerance"
	1    4700 4850
	0    1    1    0   
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Device:R_Small_US R6
U 1 1 5E9677D9
P 4700 4750
F 0 "R6" V 4650 4625 50  0000 C CNN
F 1 "107k" V 4650 4900 50  0000 C CNN
F 2 "PMSM_Driver_MZ_Split_Board:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4700 4750 50  0001 C CNN
F 3 "~" H 4700 4750 50  0001 C CNN
F 4 "MCTC0525B1073T5G -  SMD Chip Resistor, 0805 [2012 Metric], 107 kohm, 150 V, Thin Film, 125 mW" H 4700 4350 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mctc0525b1073t5g/res-107k-0-1-0-125w-0805-thin/dp/1576017" H 4700 4250 50  0001 C CNN "Distributor Link"
F 6 "0.1%" V 4650 5100 50  0000 C CNN "Tolerance"
	1    4700 4750
	0    1    1    0   
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Device:R_Small_US R5
U 1 1 5E9677AE
P 4700 4650
F 0 "R5" V 4650 4525 50  0000 C CNN
F 1 "45k3" V 4650 4800 50  0000 C CNN
F 2 "PMSM_Driver_MZ_Split_Board:R_0402_1005Metric" H 4700 4650 50  0001 C CNN
F 3 "~" H 4700 4650 50  0001 C CNN
F 4 "RN73C1E45K3BTDF -  SMD Chip Resistor, Precision, 0402 [1005 Metric], 45.3 kohm, RN73 Series, 25 V, Thin Film, 63 mW" H 4700 4250 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/te-connectivity/rn73c1e45k3btdf/res-45k3-0-1-0-063w-0402-thin/dp/2688369" H 4700 4150 50  0001 C CNN "Distributor Link"
F 6 "0.1%" V 4650 5000 50  0000 C CNN "Tolerance"
	1    4700 4650
	0    1    1    0   
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_5V0 #PWR017
U 1 1 5F53DA9E
P 7325 2750
F 0 "#PWR017" H 7325 3000 50  0001 C CNN
F 1 "MZ_5V0" H 7326 2923 50  0000 C CNN
F 2 "" H 7325 2750 50  0001 C CNN
F 3 "" H 7325 2750 50  0001 C CNN
	1    7325 2750
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_IC:MCP120T-450ITT U6
U 1 1 5F549181
P 7775 2900
F 0 "U6" H 7575 3100 50  0000 C CNN
F 1 "MCP120T-450ITT" H 8150 2700 50  0000 C CNN
F 2 "PMSM_Driver_MZ_Split_Board:SOT-23" H 7775 2200 50  0001 C CNN
F 3 "~" H 7225 3100 50  0001 C CNN
F 4 "MCP120T-450I/TT -  Voltage Supervisor, 1V-5.5V Supply, 4.5V Threshold, 350ms Delay, Active-Low, SOT-23-3 " H 7775 2100 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/microchip/mcp120t-450i-tt/supervisory-circuit-sot-23-3-120/dp/9758445" H 7775 2000 50  0001 C CNN "Distributor Link"
	1    7775 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	7425 2850 7325 2850
Wire Wire Line
	7325 2750 7325 2850
Wire Wire Line
	7775 3150 7775 3250
Text GLabel 8225 2850 2    50   Output ~ 0
MZ_PWR_EN
Wire Wire Line
	8125 2850 8225 2850
$Comp
L PMSM_Driver_MZ_Split_Board_IC:MCP120T-450ITT U7
U 1 1 5F5AF33A
P 7775 3750
F 0 "U7" H 7575 3950 50  0000 C CNN
F 1 "MCP120T-450ITT" H 8150 3550 50  0000 C CNN
F 2 "PMSM_Driver_MZ_Split_Board:SOT-23" H 7775 3050 50  0001 C CNN
F 3 "~" H 7225 3950 50  0001 C CNN
F 4 "MCP120T-450I/TT -  Voltage Supervisor, 1V-5.5V Supply, 4.5V Threshold, 350ms Delay, Active-Low, SOT-23-3 " H 7775 2950 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/microchip/mcp120t-450i-tt/supervisory-circuit-sot-23-3-120/dp/9758445" H 7775 2850 50  0001 C CNN "Distributor Link"
	1    7775 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	7425 3700 7325 3700
Wire Wire Line
	7775 4000 7775 4100
Text GLabel 8225 3700 2    50   Output ~ 0
VCCIO_EN
Wire Wire Line
	8125 3700 8225 3700
Wire Wire Line
	7325 2850 7325 3700
Connection ~ 7325 2850
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_3V3 #PWR026
U 1 1 5F626946
P 9475 3575
F 0 "#PWR026" H 9475 3825 50  0001 C CNN
F 1 "MZ_3V3" H 9476 3748 50  0000 C CNN
F 2 "" H 9475 3575 50  0001 C CNN
F 3 "" H 9475 3575 50  0001 C CNN
	1    9475 3575
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_IC:MCP120T-315ITT U9
U 1 1 5F632B3F
P 9925 3725
F 0 "U9" H 9725 3925 50  0000 C CNN
F 1 "MCP120T-315ITT" H 10275 3525 50  0000 C CNN
F 2 "PMSM_Driver_MZ_Split_Board:SOT-23" H 9925 3025 50  0001 C CNN
F 3 "~" H 9375 3925 50  0001 C CNN
F 4 "MCP120T-315I/TT -  Supervisory Circuit, Active-Low Reset, 1V-5.5Vin, SOT-23-3" H 9925 2925 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/microchip/mcp120t-315i-tt/supervisory-circuit-sot-23-3-120/dp/9758437" H 9925 2825 50  0001 C CNN "Distributor Link"
	1    9925 3725
	1    0    0    -1  
$EndComp
Wire Wire Line
	9575 3675 9475 3675
Wire Wire Line
	9475 3575 9475 3675
Wire Wire Line
	9925 3975 9925 4075
Text GLabel 10375 3675 2    50   Output ~ 0
PG_MODULE
Wire Wire Line
	10275 3675 10375 3675
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_5V0 #PWR015
U 1 1 5F6DF65D
P 6050 5975
F 0 "#PWR015" H 6050 6225 50  0001 C CNN
F 1 "MZ_5V0" H 6051 6148 50  0000 C CNN
F 2 "" H 6050 5975 50  0001 C CNN
F 3 "" H 6050 5975 50  0001 C CNN
	1    6050 5975
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_IC:TL431A U5
U 1 1 5F6EB162
P 6050 6675
F 0 "U5" V 6146 6587 50  0000 R CNN
F 1 "TL431A" V 6055 6587 50  0000 R CNN
F 2 "PMSM_Driver_MZ_Split_Board:SOT-23" H 6450 6175 50  0001 C CNN
F 3 "~" H 6050 6675 50  0001 C CNN
F 4 "TL431ACDBZR -  Voltage Reference, Shunt - Adjustable, 2.495V to 36V, 1 % Ref, 92ppm/°C, SOT-23-3" H 6550 6025 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/texas-instruments/tl431acdbzr/voltage-ref-shunt-2-495v-36v-sot/dp/3124589" H 6500 5825 50  0001 C CNN "Distributor Link"
	1    6050 6675
	0    1    -1   0   
$EndComp
Wire Wire Line
	6050 6275 6050 6375
Wire Wire Line
	6250 6625 6350 6625
Wire Wire Line
	6350 6625 6350 6375
Wire Wire Line
	6350 6375 6050 6375
Connection ~ 6050 6375
Wire Wire Line
	6050 6375 6050 6475
Wire Wire Line
	6050 6775 6050 6925
Wire Wire Line
	6350 6925 6050 6925
Connection ~ 6050 6925
Wire Wire Line
	6050 6925 6050 7025
Text GLabel 6450 6375 2    50   Output ~ 0
2V5_REF
Wire Wire Line
	6350 6375 6450 6375
Connection ~ 6350 6375
$Comp
L PMSM_Driver_MZ_Split_Board_Device:R_Small_US R11
U 1 1 5F78823C
P 6050 6175
F 0 "R11" H 5975 6075 50  0000 C CNN
F 1 "1k" H 5925 6175 50  0000 C CNN
F 2 "PMSM_Driver_MZ_Split_Board:R_0402_1005Metric" H 6050 6175 50  0001 C CNN
F 3 "~" H 6050 6175 50  0001 C CNN
F 4 "MCWR04X1001FTL -  SMD Chip Resistor, 0402 [1005 Metric], 1 kohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 6050 5775 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x1001ftl/res-1k-1-0-0625w-thick-film/dp/2447120" H 6050 5675 50  0001 C CNN "Distributor Link"
	1    6050 6175
	-1   0    0    1   
$EndComp
Wire Wire Line
	6050 6075 6050 5975
$Comp
L PMSM_Driver_MZ_Split_Board_IC:TL331KDBVR U8
U 1 1 5F801C75
P 8375 5550
F 0 "U8" H 8425 5750 50  0000 L CNN
F 1 "TL331KDBVR" H 8400 5350 50  0000 L CNN
F 2 "PMSM_Driver_MZ_Split_Board:SOT-23-5_HandSoldering" H 8375 4850 50  0001 C CNN
F 3 "~" H 8375 5550 50  0001 C CNN
F 4 "TL331KDBVR . -  Analogue Comparator, Single, Differential, 1 Comparator, 300 ns, 2V to 36V, SOT-23, 5 Pins" H 8375 4750 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/texas-instruments/tl331kdbvr/comparator-single-1-3us-sot-23/dp/2342318" H 8375 4650 50  0001 C CNN "Distributor Link"
	1    8375 5550
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_5V0 #PWR022
U 1 1 5F8037ED
P 8375 5050
F 0 "#PWR022" H 8375 5300 50  0001 C CNN
F 1 "MZ_5V0" H 8376 5223 50  0000 C CNN
F 2 "" H 8375 5050 50  0001 C CNN
F 3 "" H 8375 5050 50  0001 C CNN
	1    8375 5050
	1    0    0    -1  
$EndComp
Wire Wire Line
	8375 5250 8375 5150
$Comp
L PMSM_Driver_MZ_Split_Board_Device:C_Small C37
U 1 1 5F810538
P 8575 5150
F 0 "C37" V 8675 5100 50  0000 L CNN
F 1 "100nF" V 8625 4875 50  0000 L CNN
F 2 "PMSM_Driver_MZ_Split_Board:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 8575 5150 50  0001 C CNN
F 3 "~" H 8575 5150 50  0001 C CNN
F 4 "MC0603B104K100CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 10 V, 0603 [1608 Metric], ± 10%, X7R, MC Series" H 8575 4750 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0603b104k100ct/cap-0-1-f-10v-10-x7r-0603/dp/2627427" H 8575 4650 50  0001 C CNN "Distributor Link"
	1    8575 5150
	0    -1   -1   0   
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Device:C_Small C35
U 1 1 5F73AB8A
P 6350 6775
F 0 "C35" H 6442 6821 50  0000 L CNN
F 1 "100nF" H 6442 6730 50  0000 L CNN
F 2 "PMSM_Driver_MZ_Split_Board:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 6350 6775 50  0001 C CNN
F 3 "~" H 6350 6775 50  0001 C CNN
F 4 "MC0603B104K100CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 10 V, 0603 [1608 Metric], ± 10%, X7R, MC Series" H 6350 6375 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0603b104k100ct/cap-0-1-f-10v-10-x7r-0603/dp/2627427" H 6350 6275 50  0001 C CNN "Distributor Link"
	1    6350 6775
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 6925 6350 6875
Wire Wire Line
	6350 6675 6350 6625
Connection ~ 6350 6625
Wire Wire Line
	8725 5200 8725 5150
Wire Wire Line
	8725 5150 8675 5150
Wire Wire Line
	8475 5150 8375 5150
Wire Wire Line
	8375 5050 8375 5150
Connection ~ 8375 5150
Text Notes 7050 2425 0    100  ~ 0
5V Voltage Supervisor
Wire Notes Line
	8900 2225 8900 4400
Wire Notes Line
	6975 4400 6975 2225
Text Notes 5700 5700 0    100  ~ 0
2.5 V Ref.
Wire Notes Line
	5650 7375 5650 5475
Text Notes 8250 2775 0    25   ~ 0
Keep MZ in reset\nuntil 5 V stabilized.
Text Notes 8200 3625 0    25   ~ 0
Also disable VCCO_34, VCCO_35,\nand 3V3 until 5V stabilized.
$Comp
L PMSM_Driver_MZ_Split_Board_Device:R_Small_US R14
U 1 1 5FA2CC4C
P 7825 5150
F 0 "R14" H 7725 5125 50  0000 C CNN
F 1 "1k" H 7725 5200 50  0000 C CNN
F 2 "PMSM_Driver_MZ_Split_Board:R_0402_1005Metric" H 7825 5150 50  0001 C CNN
F 3 "~" H 7825 5150 50  0001 C CNN
F 4 "MCWR04X1001FTL -  SMD Chip Resistor, 0402 [1005 Metric], 1 kohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 7825 4750 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x1001ftl/res-1k-1-0-0625w-thick-film/dp/2447120" H 7825 4650 50  0001 C CNN "Distributor Link"
F 6 "1%" H 7725 5275 50  0000 C CNN "Tolerance"
	1    7825 5150
	-1   0    0    1   
$EndComp
Text GLabel 7475 5000 0    50   Input ~ 0
2V5_REF
$Comp
L PMSM_Driver_MZ_Split_Board_Device:R_Small_US R12
U 1 1 5FA706FE
P 7675 5000
F 0 "R12" V 7575 4975 50  0000 C CNN
F 1 "1k5" V 7625 5100 50  0000 C CNN
F 2 "PMSM_Driver_MZ_Split_Board:R_0402_1005Metric" H 7675 5000 50  0001 C CNN
F 3 "~" H 7675 5000 50  0001 C CNN
F 4 "MCWR04X1501FTL -  SMD Chip Resistor, 0402 [1005 Metric], 1.5 kohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 7675 4600 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x1501ftl/res-1k5-1-0-0625w-thick-film/dp/2447125" H 7675 4500 50  0001 C CNN "Distributor Link"
F 6 "1%" V 7750 4975 50  0000 C CNN "Tolerance"
	1    7675 5000
	0    1    1    0   
$EndComp
Wire Wire Line
	7475 5000 7575 5000
Wire Wire Line
	8175 5450 8075 5450
Wire Wire Line
	7825 5000 8075 5000
Wire Wire Line
	7825 5000 7825 5050
Wire Wire Line
	8075 5000 8075 5450
Wire Wire Line
	8375 5900 8375 5850
Text GLabel 7475 5825 0    50   Input ~ 0
VCCIO_EN
$Comp
L PMSM_Driver_MZ_Split_Board_Device:R_Small_US R15
U 1 1 5FB11FA3
P 7825 5975
F 0 "R15" H 7725 5950 50  0000 C CNN
F 1 "3k74" H 7675 6025 50  0000 C CNN
F 2 "PMSM_Driver_MZ_Split_Board:R_0402_1005Metric" H 7825 5975 50  0001 C CNN
F 3 "~" H 7825 5975 50  0001 C CNN
F 4 "MCWR04X3741FTL -  SMD Chip Resistor, 0402 [1005 Metric], 3.74 kohm, MCWR04 Series, 50 V, Thick Film, 62.5 mW" H 7825 5575 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x3741ftl/res-3k74-1-0-0625w-0402-thick/dp/2694604" H 7825 5475 50  0001 C CNN "Distributor Link"
F 6 "1%" H 7725 6100 50  0000 C CNN "Tolerance"
	1    7825 5975
	-1   0    0    1   
$EndComp
Wire Wire Line
	7475 5825 7575 5825
Wire Wire Line
	7825 5825 8075 5825
Wire Wire Line
	8075 5650 8175 5650
Wire Notes Line
	600  5475 5575 5475
Wire Notes Line
	600  7375 5575 7375
Wire Notes Line
	5650 5475 6900 5475
Wire Notes Line
	6900 5475 6900 7375
Wire Notes Line
	6900 7375 5650 7375
Wire Notes Line
	8975 2975 11100 2975
Wire Notes Line
	8975 600  11100 600 
Wire Notes Line
	600  2150 8900 2150
Wire Notes Line
	600  600  8900 600 
Wire Notes Line
	600  3775 6900 3775
Wire Notes Line
	600  3850 6900 3850
Wire Notes Line
	600  2225 6900 2225
Wire Notes Line
	600  5400 6900 5400
Wire Notes Line
	5575 5475 5575 7375
Wire Notes Line
	600  5475 600  7375
Wire Notes Line
	6975 2225 8900 2225
Wire Notes Line
	6975 4400 8900 4400
Wire Notes Line
	6975 6475 6975 4475
$Comp
L PMSM_Driver_MZ_Split_Board_Device:C_Small C36
U 1 1 5FE0BC99
P 8375 6275
F 0 "C36" V 8425 6325 50  0000 L CNN
F 1 "100nF" V 8425 6000 50  0000 L CNN
F 2 "PMSM_Driver_MZ_Split_Board:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 8375 6275 50  0001 C CNN
F 3 "~" H 8375 6275 50  0001 C CNN
F 4 "MC0603B104K100CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 10 V, 0603 [1608 Metric], ± 10%, X7R, MC Series" H 8375 5875 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0603b104k100ct/cap-0-1-f-10v-10-x7r-0603/dp/2627427" H 8375 5775 50  0001 C CNN "Distributor Link"
	1    8375 6275
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8275 6275 8075 6275
Wire Wire Line
	8975 6275 8975 5550
$Comp
L PMSM_Driver_MZ_Split_Board_Device:R_Small_US R16
U 1 1 5FE4D85D
P 9075 5250
F 0 "R16" H 8975 5225 50  0000 C CNN
F 1 "3k" H 8975 5300 50  0000 C CNN
F 2 "PMSM_Driver_MZ_Split_Board:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 9075 5250 50  0001 C CNN
F 3 "~" H 9075 5250 50  0001 C CNN
F 4 "MCWR06X3001FTL -  SMD Chip Resistor, 0603 [1608 Metric], 3 kohm, MCWR Series, 75 V, Thick Film, 100 mW" H 9075 4850 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr06x3001ftl/res-3k-1-0-1w-thick-film/dp/2447356" H 9075 4750 50  0001 C CNN "Distributor Link"
	1    9075 5250
	-1   0    0    1   
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Device:R_Small_US R17
U 1 1 5FE5DC49
P 9275 5550
F 0 "R17" V 9200 5550 50  0000 C CNN
F 1 "0R" V 9375 5550 50  0000 C CNN
F 2 "PMSM_Driver_MZ_Split_Board:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 9275 5550 50  0001 C CNN
F 3 "~" H 9275 5550 50  0001 C CNN
F 4 "" H 9275 5150 50  0001 C CNN "Description"
F 5 "" H 9275 5050 50  0001 C CNN "Distributor Link"
	1    9275 5550
	0    1    1    0   
$EndComp
Connection ~ 8975 5550
Wire Wire Line
	7775 5000 7825 5000
Connection ~ 7825 5000
$Comp
L PMSM_Driver_MZ_Split_Board_Device:R_Small_US R13
U 1 1 5FB11FAB
P 7675 5825
F 0 "R13" V 7575 5775 50  0000 C CNN
F 1 "1k" V 7625 5900 50  0000 C CNN
F 2 "PMSM_Driver_MZ_Split_Board:R_0402_1005Metric" H 7675 5825 50  0001 C CNN
F 3 "~" H 7675 5825 50  0001 C CNN
F 4 "MCWR04X1001FTL -  SMD Chip Resistor, 0402 [1005 Metric], 1 kohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 7675 5425 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x1001ftl/res-1k-1-0-0625w-thick-film/dp/2447120" H 7675 5325 50  0001 C CNN "Distributor Link"
F 6 "1%" V 7750 5775 50  0000 C CNN "Tolerance"
	1    7675 5825
	0    1    1    0   
$EndComp
Wire Wire Line
	7825 5300 7825 5250
Wire Wire Line
	8075 5650 8075 5825
Wire Wire Line
	7825 6075 7825 6125
Wire Wire Line
	7825 5875 7825 5825
Wire Wire Line
	7775 5825 7825 5825
Connection ~ 7825 5825
Connection ~ 8075 5825
Wire Wire Line
	8075 5825 8075 6275
Wire Wire Line
	8475 6275 8975 6275
Wire Wire Line
	8775 5550 8975 5550
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_5V0 #PWR025
U 1 1 601221B1
P 9075 5050
F 0 "#PWR025" H 9075 5300 50  0001 C CNN
F 1 "MZ_5V0" H 9076 5223 50  0000 C CNN
F 2 "" H 9075 5050 50  0001 C CNN
F 3 "" H 9075 5050 50  0001 C CNN
	1    9075 5050
	1    0    0    -1  
$EndComp
Wire Wire Line
	8975 5550 9075 5550
Wire Wire Line
	9075 5050 9075 5150
Wire Wire Line
	9075 5350 9075 5550
Connection ~ 9075 5550
Wire Wire Line
	9075 5550 9175 5550
$Comp
L PMSM_Driver_MZ_Split_Board_Device:R_Small_US R18
U 1 1 60181B1C
P 9475 5750
F 0 "R18" H 9375 5725 50  0000 C CNN
F 1 "100k" H 9325 5800 50  0000 C CNN
F 2 "PMSM_Driver_MZ_Split_Board:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 9475 5750 50  0001 C CNN
F 3 "~" H 9475 5750 50  0001 C CNN
F 4 "MCWR06X1003FTL -  SMD Chip Resistor, 0603 [1608 Metric], 100 kohm, MCWR Series, 75 V, Thick Film, 100 mW" H 9475 5350 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr06x1003ftl/res-100k-1-0-1w-0603-thick-film/dp/2447226" H 9475 5250 50  0001 C CNN "Distributor Link"
	1    9475 5750
	-1   0    0    1   
$EndComp
Wire Wire Line
	9475 5950 9475 5850
Wire Wire Line
	9475 5650 9475 5550
Wire Wire Line
	9475 5550 9375 5550
Text Notes 8100 5450 0    25   ~ 0
1V
Text Notes 8075 5650 0    25   ~ 0
1.092V\nif OK
Text GLabel 9575 5550 2    50   Output ~ 0
VCCIO_EN_5V
Wire Wire Line
	9575 5550 9475 5550
Connection ~ 9475 5550
Text Notes 7050 4675 0    100  ~ 0
VCCIO_EN to 5V Level Signal
Wire Notes Line
	10250 6475 10250 4475
Wire Notes Line
	6975 4475 10250 4475
Wire Notes Line
	6975 6475 10250 6475
Text GLabel 1800 6575 0    50   Input ~ 0
VCCIO_EN_5V
Wire Wire Line
	1850 6575 1800 6575
Wire Wire Line
	1250 6375 1850 6375
Connection ~ 1250 6375
Text GLabel 1900 4675 0    50   Input ~ 0
VCCIO_EN_5V
Wire Wire Line
	1950 4675 1900 4675
Text GLabel 1900 3050 0    50   Input ~ 0
VCCIO_EN_5V
Wire Wire Line
	1950 3050 1900 3050
Wire Wire Line
	1350 4475 1950 4475
Wire Wire Line
	1350 2850 1950 2850
Text Notes 9050 3250 0    100  ~ 0
3V3 Voltage Supervisor
Wire Notes Line
	8975 3050 11100 3050
Wire Notes Line
	11100 3050 11100 4400
Wire Notes Line
	11100 4400 8975 4400
Wire Notes Line
	8975 4400 8975 3050
Text Notes 10400 3600 0    25   ~ 0
Zynq will boot after\n3V3 is stabilized.
Wire Wire Line
	1800 1400 1800 1825
$Comp
L power:PWR_FLAG #FLG0106
U 1 1 5F4BF4A1
P 8375 1725
F 0 "#FLG0106" H 8375 1800 50  0001 C CNN
F 1 "PWR_FLAG" H 8375 1898 50  0000 C CNN
F 2 "" H 8375 1725 50  0001 C CNN
F 3 "~" H 8375 1725 50  0001 C CNN
	1    8375 1725
	1    0    0    -1  
$EndComp
Wire Wire Line
	8375 1825 8375 1725
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_GND #PWR?
U 1 1 5EBA1E74
P 1800 1825
F 0 "#PWR?" H 1800 1575 50  0001 C CNN
F 1 "MZ_GND" H 1805 1652 50  0000 C CNN
F 2 "" H 1800 1825 50  0001 C CNN
F 3 "" H 1800 1825 50  0001 C CNN
	1    1800 1825
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_GND #PWR?
U 1 1 5EBC3459
P 2400 1825
F 0 "#PWR?" H 2400 1575 50  0001 C CNN
F 1 "MZ_GND" H 2405 1652 50  0000 C CNN
F 2 "" H 2400 1825 50  0001 C CNN
F 3 "" H 2400 1825 50  0001 C CNN
	1    2400 1825
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_GND #PWR?
U 1 1 5EBD3C3A
P 2800 1825
F 0 "#PWR?" H 2800 1575 50  0001 C CNN
F 1 "MZ_GND" H 2805 1652 50  0000 C CNN
F 2 "" H 2800 1825 50  0001 C CNN
F 3 "" H 2800 1825 50  0001 C CNN
	1    2800 1825
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_GND #PWR?
U 1 1 5EBE4397
P 3200 1825
F 0 "#PWR?" H 3200 1575 50  0001 C CNN
F 1 "MZ_GND" H 3205 1652 50  0000 C CNN
F 2 "" H 3200 1825 50  0001 C CNN
F 3 "" H 3200 1825 50  0001 C CNN
	1    3200 1825
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_GND #PWR?
U 1 1 5EBF4B8B
P 3600 1825
F 0 "#PWR?" H 3600 1575 50  0001 C CNN
F 1 "MZ_GND" H 3605 1652 50  0000 C CNN
F 2 "" H 3600 1825 50  0001 C CNN
F 3 "" H 3600 1825 50  0001 C CNN
	1    3600 1825
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_GND #PWR?
U 1 1 5EC0542B
P 4400 1825
F 0 "#PWR?" H 4400 1575 50  0001 C CNN
F 1 "MZ_GND" H 4405 1652 50  0000 C CNN
F 2 "" H 4400 1825 50  0001 C CNN
F 3 "" H 4400 1825 50  0001 C CNN
	1    4400 1825
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_GND #PWR?
U 1 1 5EC15DC4
P 5450 1825
F 0 "#PWR?" H 5450 1575 50  0001 C CNN
F 1 "MZ_GND" H 5455 1652 50  0000 C CNN
F 2 "" H 5450 1825 50  0001 C CNN
F 3 "" H 5450 1825 50  0001 C CNN
	1    5450 1825
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_GND #PWR?
U 1 1 5EC265C6
P 6100 1825
F 0 "#PWR?" H 6100 1575 50  0001 C CNN
F 1 "MZ_GND" H 6105 1652 50  0000 C CNN
F 2 "" H 6100 1825 50  0001 C CNN
F 3 "" H 6100 1825 50  0001 C CNN
	1    6100 1825
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_GND #PWR?
U 1 1 5EC36D9F
P 6525 1825
F 0 "#PWR?" H 6525 1575 50  0001 C CNN
F 1 "MZ_GND" H 6530 1652 50  0000 C CNN
F 2 "" H 6525 1825 50  0001 C CNN
F 3 "" H 6525 1825 50  0001 C CNN
	1    6525 1825
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_GND #PWR?
U 1 1 5EC47611
P 6925 1825
F 0 "#PWR?" H 6925 1575 50  0001 C CNN
F 1 "MZ_GND" H 6930 1652 50  0000 C CNN
F 2 "" H 6925 1825 50  0001 C CNN
F 3 "" H 6925 1825 50  0001 C CNN
	1    6925 1825
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_GND #PWR?
U 1 1 5EC68888
P 8375 1825
F 0 "#PWR?" H 8375 1575 50  0001 C CNN
F 1 "MZ_GND" H 8380 1652 50  0000 C CNN
F 2 "" H 8375 1825 50  0001 C CNN
F 3 "" H 8375 1825 50  0001 C CNN
	1    8375 1825
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_GND #PWR?
U 1 1 5EC9A14D
P 9250 1150
F 0 "#PWR?" H 9250 900 50  0001 C CNN
F 1 "MZ_GND" H 9255 977 50  0000 C CNN
F 2 "" H 9250 1150 50  0001 C CNN
F 3 "" H 9250 1150 50  0001 C CNN
	1    9250 1150
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_GND #PWR?
U 1 1 5ECAAB1D
P 10250 1150
F 0 "#PWR?" H 10250 900 50  0001 C CNN
F 1 "MZ_GND" H 10255 977 50  0000 C CNN
F 2 "" H 10250 1150 50  0001 C CNN
F 3 "" H 10250 1150 50  0001 C CNN
	1    10250 1150
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_GND #PWR?
U 1 1 5ECBB444
P 9250 1650
F 0 "#PWR?" H 9250 1400 50  0001 C CNN
F 1 "MZ_GND" H 9255 1477 50  0000 C CNN
F 2 "" H 9250 1650 50  0001 C CNN
F 3 "" H 9250 1650 50  0001 C CNN
	1    9250 1650
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_GND #PWR?
U 1 1 5ECCBDBD
P 10250 1650
F 0 "#PWR?" H 10250 1400 50  0001 C CNN
F 1 "MZ_GND" H 10255 1477 50  0000 C CNN
F 2 "" H 10250 1650 50  0001 C CNN
F 3 "" H 10250 1650 50  0001 C CNN
	1    10250 1650
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_GND #PWR?
U 1 1 5ECECB1D
P 7775 3250
F 0 "#PWR?" H 7775 3000 50  0001 C CNN
F 1 "MZ_GND" H 7780 3077 50  0000 C CNN
F 2 "" H 7775 3250 50  0001 C CNN
F 3 "" H 7775 3250 50  0001 C CNN
	1    7775 3250
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_GND #PWR?
U 1 1 5ED0D90E
P 7775 4100
F 0 "#PWR?" H 7775 3850 50  0001 C CNN
F 1 "MZ_GND" H 7780 3927 50  0000 C CNN
F 2 "" H 7775 4100 50  0001 C CNN
F 3 "" H 7775 4100 50  0001 C CNN
	1    7775 4100
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_GND #PWR?
U 1 1 5ED2E740
P 9925 4075
F 0 "#PWR?" H 9925 3825 50  0001 C CNN
F 1 "MZ_GND" H 9930 3902 50  0000 C CNN
F 2 "" H 9925 4075 50  0001 C CNN
F 3 "" H 9925 4075 50  0001 C CNN
	1    9925 4075
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_GND #PWR?
U 1 1 5ED4F77C
P 8725 5200
F 0 "#PWR?" H 8725 4950 50  0001 C CNN
F 1 "MZ_GND" H 8730 5027 50  0000 C CNN
F 2 "" H 8725 5200 50  0001 C CNN
F 3 "" H 8725 5200 50  0001 C CNN
	1    8725 5200
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_GND #PWR?
U 1 1 5ED705E7
P 9475 5950
F 0 "#PWR?" H 9475 5700 50  0001 C CNN
F 1 "MZ_GND" H 9480 5777 50  0000 C CNN
F 2 "" H 9475 5950 50  0001 C CNN
F 3 "" H 9475 5950 50  0001 C CNN
	1    9475 5950
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_GND #PWR?
U 1 1 5ED914EE
P 8375 5900
F 0 "#PWR?" H 8375 5650 50  0001 C CNN
F 1 "MZ_GND" H 8380 5727 50  0000 C CNN
F 2 "" H 8375 5900 50  0001 C CNN
F 3 "" H 8375 5900 50  0001 C CNN
	1    8375 5900
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_GND #PWR?
U 1 1 5EDB24AF
P 7825 6125
F 0 "#PWR?" H 7825 5875 50  0001 C CNN
F 1 "MZ_GND" H 7830 5952 50  0000 C CNN
F 2 "" H 7825 6125 50  0001 C CNN
F 3 "" H 7825 6125 50  0001 C CNN
	1    7825 6125
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_GND #PWR?
U 1 1 5EDD3342
P 7825 5300
F 0 "#PWR?" H 7825 5050 50  0001 C CNN
F 1 "MZ_GND" H 7830 5127 50  0000 C CNN
F 2 "" H 7825 5300 50  0001 C CNN
F 3 "" H 7825 5300 50  0001 C CNN
	1    7825 5300
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_GND #PWR?
U 1 1 5EDF4487
P 925 3450
F 0 "#PWR?" H 925 3200 50  0001 C CNN
F 1 "MZ_GND" H 930 3277 50  0000 C CNN
F 2 "" H 925 3450 50  0001 C CNN
F 3 "" H 925 3450 50  0001 C CNN
	1    925  3450
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_GND #PWR?
U 1 1 5EE04E03
P 1350 3450
F 0 "#PWR?" H 1350 3200 50  0001 C CNN
F 1 "MZ_GND" H 1355 3277 50  0000 C CNN
F 2 "" H 1350 3450 50  0001 C CNN
F 3 "" H 1350 3450 50  0001 C CNN
	1    1350 3450
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_GND #PWR?
U 1 1 5EE156F8
P 2250 3450
F 0 "#PWR?" H 2250 3200 50  0001 C CNN
F 1 "MZ_GND" H 2255 3277 50  0000 C CNN
F 2 "" H 2250 3450 50  0001 C CNN
F 3 "" H 2250 3450 50  0001 C CNN
	1    2250 3450
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_GND #PWR?
U 1 1 5EE371CA
P 2950 3450
F 0 "#PWR?" H 2950 3200 50  0001 C CNN
F 1 "MZ_GND" H 2955 3277 50  0000 C CNN
F 2 "" H 2950 3450 50  0001 C CNN
F 3 "" H 2950 3450 50  0001 C CNN
	1    2950 3450
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_GND #PWR?
U 1 1 5EE478D6
P 3650 3450
F 0 "#PWR?" H 3650 3200 50  0001 C CNN
F 1 "MZ_GND" H 3655 3277 50  0000 C CNN
F 2 "" H 3650 3450 50  0001 C CNN
F 3 "" H 3650 3450 50  0001 C CNN
	1    3650 3450
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_GND #PWR?
U 1 1 5EE78E06
P 4075 3450
F 0 "#PWR?" H 4075 3200 50  0001 C CNN
F 1 "MZ_GND" H 4080 3277 50  0000 C CNN
F 2 "" H 4075 3450 50  0001 C CNN
F 3 "" H 4075 3450 50  0001 C CNN
	1    4075 3450
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_GND #PWR?
U 1 1 5EE8973D
P 6150 3450
F 0 "#PWR?" H 6150 3200 50  0001 C CNN
F 1 "MZ_GND" H 6155 3277 50  0000 C CNN
F 2 "" H 6150 3450 50  0001 C CNN
F 3 "" H 6150 3450 50  0001 C CNN
	1    6150 3450
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_GND #PWR?
U 1 1 5EE99F8F
P 6475 3450
F 0 "#PWR?" H 6475 3200 50  0001 C CNN
F 1 "MZ_GND" H 6480 3277 50  0000 C CNN
F 2 "" H 6475 3450 50  0001 C CNN
F 3 "" H 6475 3450 50  0001 C CNN
	1    6475 3450
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_GND #PWR?
U 1 1 5EEBB4F4
P 925 5075
F 0 "#PWR?" H 925 4825 50  0001 C CNN
F 1 "MZ_GND" H 930 4902 50  0000 C CNN
F 2 "" H 925 5075 50  0001 C CNN
F 3 "" H 925 5075 50  0001 C CNN
	1    925  5075
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_GND #PWR?
U 1 1 5EECBD70
P 1350 5075
F 0 "#PWR?" H 1350 4825 50  0001 C CNN
F 1 "MZ_GND" H 1355 4902 50  0000 C CNN
F 2 "" H 1350 5075 50  0001 C CNN
F 3 "" H 1350 5075 50  0001 C CNN
	1    1350 5075
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_GND #PWR?
U 1 1 5EEDC669
P 2250 5075
F 0 "#PWR?" H 2250 4825 50  0001 C CNN
F 1 "MZ_GND" H 2255 4902 50  0000 C CNN
F 2 "" H 2250 5075 50  0001 C CNN
F 3 "" H 2250 5075 50  0001 C CNN
	1    2250 5075
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_GND #PWR?
U 1 1 5EEECDF4
P 2950 5075
F 0 "#PWR?" H 2950 4825 50  0001 C CNN
F 1 "MZ_GND" H 2955 4902 50  0000 C CNN
F 2 "" H 2950 5075 50  0001 C CNN
F 3 "" H 2950 5075 50  0001 C CNN
	1    2950 5075
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_GND #PWR?
U 1 1 5EEFD5FB
P 3650 5075
F 0 "#PWR?" H 3650 4825 50  0001 C CNN
F 1 "MZ_GND" H 3655 4902 50  0000 C CNN
F 2 "" H 3650 5075 50  0001 C CNN
F 3 "" H 3650 5075 50  0001 C CNN
	1    3650 5075
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_GND #PWR?
U 1 1 5EF0DE8E
P 4075 5075
F 0 "#PWR?" H 4075 4825 50  0001 C CNN
F 1 "MZ_GND" H 4080 4902 50  0000 C CNN
F 2 "" H 4075 5075 50  0001 C CNN
F 3 "" H 4075 5075 50  0001 C CNN
	1    4075 5075
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_GND #PWR?
U 1 1 5EF2ED1A
P 6150 5075
F 0 "#PWR?" H 6150 4825 50  0001 C CNN
F 1 "MZ_GND" H 6155 4902 50  0000 C CNN
F 2 "" H 6150 5075 50  0001 C CNN
F 3 "" H 6150 5075 50  0001 C CNN
	1    6150 5075
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_GND #PWR?
U 1 1 5EF3F4FE
P 6475 5075
F 0 "#PWR?" H 6475 4825 50  0001 C CNN
F 1 "MZ_GND" H 6480 4902 50  0000 C CNN
F 2 "" H 6475 5075 50  0001 C CNN
F 3 "" H 6475 5075 50  0001 C CNN
	1    6475 5075
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_GND #PWR?
U 1 1 5EF60A83
P 825 6975
F 0 "#PWR?" H 825 6725 50  0001 C CNN
F 1 "MZ_GND" H 830 6802 50  0000 C CNN
F 2 "" H 825 6975 50  0001 C CNN
F 3 "" H 825 6975 50  0001 C CNN
	1    825  6975
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_GND #PWR?
U 1 1 5EF71260
P 1250 6975
F 0 "#PWR?" H 1250 6725 50  0001 C CNN
F 1 "MZ_GND" H 1255 6802 50  0000 C CNN
F 2 "" H 1250 6975 50  0001 C CNN
F 3 "" H 1250 6975 50  0001 C CNN
	1    1250 6975
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_GND #PWR?
U 1 1 5EF81B41
P 2150 6975
F 0 "#PWR?" H 2150 6725 50  0001 C CNN
F 1 "MZ_GND" H 2155 6802 50  0000 C CNN
F 2 "" H 2150 6975 50  0001 C CNN
F 3 "" H 2150 6975 50  0001 C CNN
	1    2150 6975
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_GND #PWR?
U 1 1 5EF924E0
P 2850 6975
F 0 "#PWR?" H 2850 6725 50  0001 C CNN
F 1 "MZ_GND" H 2855 6802 50  0000 C CNN
F 2 "" H 2850 6975 50  0001 C CNN
F 3 "" H 2850 6975 50  0001 C CNN
	1    2850 6975
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_GND #PWR?
U 1 1 5EFA2D02
P 3550 6975
F 0 "#PWR?" H 3550 6725 50  0001 C CNN
F 1 "MZ_GND" H 3555 6802 50  0000 C CNN
F 2 "" H 3550 6975 50  0001 C CNN
F 3 "" H 3550 6975 50  0001 C CNN
	1    3550 6975
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_GND #PWR?
U 1 1 5EFB3427
P 3975 6975
F 0 "#PWR?" H 3975 6725 50  0001 C CNN
F 1 "MZ_GND" H 3980 6802 50  0000 C CNN
F 2 "" H 3975 6975 50  0001 C CNN
F 3 "" H 3975 6975 50  0001 C CNN
	1    3975 6975
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_GND #PWR?
U 1 1 5F00565A
P 4950 6975
F 0 "#PWR?" H 4950 6725 50  0001 C CNN
F 1 "MZ_GND" H 4955 6802 50  0000 C CNN
F 2 "" H 4950 6975 50  0001 C CNN
F 3 "" H 4950 6975 50  0001 C CNN
	1    4950 6975
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_GND #PWR?
U 1 1 5F015DE0
P 5300 6975
F 0 "#PWR?" H 5300 6725 50  0001 C CNN
F 1 "MZ_GND" H 5305 6802 50  0000 C CNN
F 2 "" H 5300 6975 50  0001 C CNN
F 3 "" H 5300 6975 50  0001 C CNN
	1    5300 6975
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_GND #PWR?
U 1 1 5F036B4A
P 6050 7025
F 0 "#PWR?" H 6050 6775 50  0001 C CNN
F 1 "MZ_GND" H 6055 6852 50  0000 C CNN
F 2 "" H 6050 7025 50  0001 C CNN
F 3 "" H 6050 7025 50  0001 C CNN
	1    6050 7025
	1    0    0    -1  
$EndComp
$EndSCHEMATC
