EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 4
Title "PMSM_Driver_MZ_Split_Board"
Date "2020-03-30"
Rev "A"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	2150 6150 2150 5950
Wire Wire Line
	2150 5750 2250 5750
Wire Wire Line
	2150 5750 2150 5250
Wire Wire Line
	2150 5250 2250 5250
Connection ~ 2150 5750
Wire Wire Line
	2250 4850 2150 4850
Wire Wire Line
	2150 4850 2150 5250
Connection ~ 2150 5250
Wire Wire Line
	2250 4550 2150 4550
Wire Wire Line
	2150 4550 2150 4850
Connection ~ 2150 4850
Wire Wire Line
	2250 4250 2150 4250
Wire Wire Line
	2150 4250 2150 4550
Connection ~ 2150 4550
Wire Wire Line
	2250 3550 2150 3550
Wire Wire Line
	2150 3550 2150 4250
Connection ~ 2150 4250
Wire Wire Line
	2250 3250 2150 3250
Wire Wire Line
	2150 3250 2150 3550
Connection ~ 2150 3550
Wire Wire Line
	2250 2950 2150 2950
Wire Wire Line
	2150 2950 2150 3250
Connection ~ 2150 3250
Wire Wire Line
	2250 2650 2150 2650
Wire Wire Line
	2150 2650 2150 2950
Connection ~ 2150 2950
Wire Wire Line
	2250 2350 2150 2350
Wire Wire Line
	2150 2350 2150 2650
Connection ~ 2150 2650
Wire Wire Line
	2250 2050 2150 2050
Wire Wire Line
	2150 2050 2150 2350
Connection ~ 2150 2350
Wire Wire Line
	2250 1750 2150 1750
Wire Wire Line
	2150 1750 2150 2050
Connection ~ 2150 2050
Wire Wire Line
	4350 5750 4250 5750
Wire Wire Line
	4350 5750 4350 5250
Wire Wire Line
	4350 5250 4250 5250
Connection ~ 4350 5750
Wire Wire Line
	4350 5250 4350 4550
Wire Wire Line
	4350 4550 4250 4550
Connection ~ 4350 5250
Wire Wire Line
	4350 4550 4350 4250
Wire Wire Line
	4350 4250 4250 4250
Connection ~ 4350 4550
Wire Wire Line
	4350 4250 4350 3550
Wire Wire Line
	4350 3550 4250 3550
Connection ~ 4350 4250
Wire Wire Line
	4350 3550 4350 3250
Wire Wire Line
	4350 3250 4250 3250
Connection ~ 4350 3550
Wire Wire Line
	4350 3250 4350 2950
Wire Wire Line
	4350 2950 4250 2950
Connection ~ 4350 3250
Wire Wire Line
	4350 2950 4350 2650
Wire Wire Line
	4350 2650 4250 2650
Connection ~ 4350 2950
Wire Wire Line
	4350 2650 4350 2350
Wire Wire Line
	4350 2350 4250 2350
Connection ~ 4350 2650
Wire Wire Line
	4350 2350 4350 2050
Wire Wire Line
	4350 2050 4250 2050
Connection ~ 4350 2350
Wire Wire Line
	4350 2050 4350 1750
Wire Wire Line
	4350 1750 4250 1750
Connection ~ 4350 2050
Wire Wire Line
	7600 5550 7500 5550
Wire Wire Line
	7500 5550 7500 6150
Wire Wire Line
	7600 5250 7500 5250
Wire Wire Line
	7500 5250 7500 5550
Connection ~ 7500 5550
Wire Wire Line
	7600 4850 7500 4850
Wire Wire Line
	7500 4850 7500 5250
Connection ~ 7500 5250
Wire Wire Line
	7600 4550 7500 4550
Wire Wire Line
	7500 4550 7500 4850
Connection ~ 7500 4850
Wire Wire Line
	7600 4250 7500 4250
Wire Wire Line
	7500 4250 7500 4550
Connection ~ 7500 4550
Wire Wire Line
	7600 3550 7500 3550
Wire Wire Line
	7500 3550 7500 4250
Connection ~ 7500 4250
Wire Wire Line
	7600 3250 7500 3250
Wire Wire Line
	7500 3250 7500 3550
Connection ~ 7500 3550
Wire Wire Line
	7600 2950 7500 2950
Wire Wire Line
	7500 2950 7500 3250
Connection ~ 7500 3250
Wire Wire Line
	7600 2650 7500 2650
Wire Wire Line
	7500 2650 7500 2950
Connection ~ 7500 2950
Wire Wire Line
	7600 2350 7500 2350
Wire Wire Line
	7500 2350 7500 2650
Connection ~ 7500 2650
Wire Wire Line
	7600 2050 7500 2050
Wire Wire Line
	7500 2050 7500 2350
Connection ~ 7500 2350
Wire Wire Line
	7600 1750 7500 1750
Wire Wire Line
	7500 1750 7500 2050
Connection ~ 7500 2050
Wire Wire Line
	9600 5550 9700 5550
Wire Wire Line
	9700 5550 9700 6150
Wire Wire Line
	9600 5250 9700 5250
Wire Wire Line
	9700 5250 9700 5550
Connection ~ 9700 5550
Wire Wire Line
	9600 4550 9700 4550
Wire Wire Line
	9700 4550 9700 5250
Connection ~ 9700 5250
Wire Wire Line
	9600 4250 9700 4250
Wire Wire Line
	9700 4250 9700 4550
Connection ~ 9700 4550
Wire Wire Line
	9600 3550 9700 3550
Wire Wire Line
	9700 3550 9700 4250
Connection ~ 9700 4250
Wire Wire Line
	9600 3250 9700 3250
Wire Wire Line
	9700 3250 9700 3550
Connection ~ 9700 3550
Wire Wire Line
	9600 2950 9700 2950
Wire Wire Line
	9700 2950 9700 3250
Connection ~ 9700 3250
Wire Wire Line
	9600 2650 9700 2650
Wire Wire Line
	9700 2650 9700 2950
Connection ~ 9700 2950
Wire Wire Line
	9600 2350 9700 2350
Wire Wire Line
	9700 2350 9700 2650
Connection ~ 9700 2650
Wire Wire Line
	9600 2050 9700 2050
Wire Wire Line
	9700 2050 9700 2350
Connection ~ 9700 2350
Wire Wire Line
	9600 1750 9700 1750
Wire Wire Line
	9700 1750 9700 2050
Connection ~ 9700 2050
Wire Wire Line
	2250 3850 1125 3850
Wire Wire Line
	1125 3850 1125 3650
Wire Wire Line
	2250 3950 1125 3950
Wire Wire Line
	1125 3950 1125 3850
Connection ~ 1125 3850
Wire Wire Line
	4250 3850 5375 3850
Wire Wire Line
	5375 3850 5375 3650
Wire Wire Line
	4250 3950 5375 3950
Wire Wire Line
	5375 3950 5375 3850
Connection ~ 5375 3850
Wire Wire Line
	6400 3850 6400 3650
Wire Wire Line
	6400 3950 6400 3850
Connection ~ 6400 3850
Wire Wire Line
	9600 3850 10725 3850
Wire Wire Line
	10725 3850 10725 3650
Wire Wire Line
	9600 3950 10725 3950
Wire Wire Line
	10725 3950 10725 3850
Connection ~ 10725 3850
NoConn ~ 9600 1050
NoConn ~ 9600 1150
NoConn ~ 9600 1250
NoConn ~ 9600 1350
NoConn ~ 7600 1050
NoConn ~ 7600 1150
NoConn ~ 7600 1250
NoConn ~ 7600 1350
Text GLabel 7400 1550 0    50   BiDi ~ 0
PG_MODULE
Wire Wire Line
	7400 1550 7600 1550
$Comp
L PMSM_Driver_MZ_Split_Board_Connector:MicroZed_JX1 JX1
U 1 1 5E93F20A
P 3250 3500
F 0 "JX1" H 2525 6100 50  0000 C CNN
F 1 "61083-104400LF" H 3725 900 50  0000 C CNN
F 2 "PMSM_Driver_MZ_Split_Board:61083-104400LF" H 3250 800 60  0001 C CNN
F 3 "~" H 3250 3500 60  0001 C CNN
F 4 "61083-104400LF -  Stacking Board Connector, Bergstak, FCI BergStak 61083 Series, 100 Contacts, Plug, 0.8 mm" H 3250 700 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/amphenol-icc-fci/61083-104400lf/plug-smd-0-8mm-100way/dp/1103987" H 3250 600 50  0001 C CNN "Distributor Link"
	1    3250 3500
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_5V0 #PWR?
U 1 1 5E9DFAE8
P 1125 3650
AR Path="/5EB87562/5E9DFAE8" Ref="#PWR?"  Part="1" 
AR Path="/5E9077E4/5E9DFAE8" Ref="#PWR0101"  Part="1" 
F 0 "#PWR0101" H 1125 3900 50  0001 C CNN
F 1 "MZ_5V0" H 1126 3823 50  0000 C CNN
F 2 "" H 1125 3650 50  0001 C CNN
F 3 "" H 1125 3650 50  0001 C CNN
	1    1125 3650
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_5V0 #PWR?
U 1 1 5E9E5CBD
P 5375 3650
AR Path="/5EB87562/5E9E5CBD" Ref="#PWR?"  Part="1" 
AR Path="/5E9077E4/5E9E5CBD" Ref="#PWR0102"  Part="1" 
F 0 "#PWR0102" H 5375 3900 50  0001 C CNN
F 1 "MZ_5V0" H 5376 3823 50  0000 C CNN
F 2 "" H 5375 3650 50  0001 C CNN
F 3 "" H 5375 3650 50  0001 C CNN
	1    5375 3650
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_5V0 #PWR?
U 1 1 5E9E8D4F
P 6400 3650
AR Path="/5EB87562/5E9E8D4F" Ref="#PWR?"  Part="1" 
AR Path="/5E9077E4/5E9E8D4F" Ref="#PWR0103"  Part="1" 
F 0 "#PWR0103" H 6400 3900 50  0001 C CNN
F 1 "MZ_5V0" H 6401 3823 50  0000 C CNN
F 2 "" H 6400 3650 50  0001 C CNN
F 3 "" H 6400 3650 50  0001 C CNN
	1    6400 3650
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_5V0 #PWR?
U 1 1 5E9EEA0F
P 10725 3650
AR Path="/5EB87562/5E9EEA0F" Ref="#PWR?"  Part="1" 
AR Path="/5E9077E4/5E9EEA0F" Ref="#PWR0104"  Part="1" 
F 0 "#PWR0104" H 10725 3900 50  0001 C CNN
F 1 "MZ_5V0" H 10726 3823 50  0000 C CNN
F 2 "" H 10725 3650 50  0001 C CNN
F 3 "" H 10725 3650 50  0001 C CNN
	1    10725 3650
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_VCCO_35 #PWR?
U 1 1 5ED5C76B
P 6400 4775
AR Path="/5EB87562/5ED5C76B" Ref="#PWR?"  Part="1" 
AR Path="/5E9077E4/5ED5C76B" Ref="#PWR0151"  Part="1" 
F 0 "#PWR0151" H 6400 5025 50  0001 C CNN
F 1 "MZ_VCCO_35" H 6401 4948 50  0000 C CNN
F 2 "" H 6400 4775 50  0001 C CNN
F 3 "" H 6400 4775 50  0001 C CNN
	1    6400 4775
	1    0    0    -1  
$EndComp
Wire Wire Line
	6400 4950 6400 4775
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_VCCO_35 #PWR?
U 1 1 5ED63129
P 10725 4675
AR Path="/5EB87562/5ED63129" Ref="#PWR?"  Part="1" 
AR Path="/5E9077E4/5ED63129" Ref="#PWR0152"  Part="1" 
F 0 "#PWR0152" H 10725 4925 50  0001 C CNN
F 1 "MZ_VCCO_35" H 10726 4848 50  0000 C CNN
F 2 "" H 10725 4675 50  0001 C CNN
F 3 "" H 10725 4675 50  0001 C CNN
	1    10725 4675
	-1   0    0    -1  
$EndComp
Wire Wire Line
	9600 4850 10725 4850
Wire Wire Line
	9600 4950 10725 4950
Wire Wire Line
	10725 4675 10725 4850
Connection ~ 10725 4850
Wire Wire Line
	10725 4850 10725 4950
Wire Wire Line
	4250 4850 5375 4850
Wire Wire Line
	4250 4950 5375 4950
Wire Wire Line
	5375 4675 5375 4850
Connection ~ 5375 4850
Wire Wire Line
	5375 4850 5375 4950
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_VCCO_34 #PWR0153
U 1 1 5ED71740
P 5375 4675
F 0 "#PWR0153" H 5375 4925 50  0001 C CNN
F 1 "MZ_VCCO_34" H 5376 4848 50  0000 C CNN
F 2 "" H 5375 4675 50  0001 C CNN
F 3 "" H 5375 4675 50  0001 C CNN
	1    5375 4675
	1    0    0    -1  
$EndComp
Wire Wire Line
	2250 4950 1125 4950
Wire Wire Line
	1125 4775 1125 4950
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_VCCO_34 #PWR0154
U 1 1 5ED7995E
P 1125 4775
F 0 "#PWR0154" H 1125 5025 50  0001 C CNN
F 1 "MZ_VCCO_34" H 1126 4948 50  0000 C CNN
F 2 "" H 1125 4775 50  0001 C CNN
F 3 "" H 1125 4775 50  0001 C CNN
	1    1125 4775
	-1   0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Device:C_Small C?
U 1 1 5EEB7842
P 1125 4125
AR Path="/5EB87562/5EEB7842" Ref="C?"  Part="1" 
AR Path="/5E9077E4/5EEB7842" Ref="C27"  Part="1" 
F 0 "C27" H 1217 4171 50  0000 L CNN
F 1 "22uF" H 1217 4080 50  0000 L CNN
F 2 "PMSM_Driver_MZ_Split_Board:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 1125 4125 50  0001 C CNN
F 3 "~" H 1125 4125 50  0001 C CNN
F 4 "GRM21BR61A226ME51L -  SMD Multilayer Ceramic Capacitor, 22 µF, 10 V, 0805 [2012 Metric], ± 20%, X5R, GRM Series" H 1125 3725 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/murata/grm21br61a226me51l/cap-22-f-10v-20-x5r-0805/dp/2494270" H 1125 3625 50  0001 C CNN "Distributor Link"
F 6 "10V" H 1275 4000 50  0000 C CNN "Voltage"
	1    1125 4125
	1    0    0    -1  
$EndComp
Wire Wire Line
	1125 4025 1125 3950
Connection ~ 1125 3950
$Comp
L PMSM_Driver_MZ_Split_Board_Device:C_Small C?
U 1 1 5EECB95E
P 1125 5125
AR Path="/5EB87562/5EECB95E" Ref="C?"  Part="1" 
AR Path="/5E9077E4/5EECB95E" Ref="C28"  Part="1" 
F 0 "C28" H 1217 5171 50  0000 L CNN
F 1 "22uF" H 1217 5080 50  0000 L CNN
F 2 "PMSM_Driver_MZ_Split_Board:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 1125 5125 50  0001 C CNN
F 3 "~" H 1125 5125 50  0001 C CNN
F 4 "GRM21BR61A226ME51L -  SMD Multilayer Ceramic Capacitor, 22 µF, 10 V, 0805 [2012 Metric], ± 20%, X5R, GRM Series" H 1125 4725 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/murata/grm21br61a226me51l/cap-22-f-10v-20-x5r-0805/dp/2494270" H 1125 4625 50  0001 C CNN "Distributor Link"
F 6 "10V" H 1275 5000 50  0000 C CNN "Voltage"
	1    1125 5125
	1    0    0    -1  
$EndComp
Wire Wire Line
	1125 5025 1125 4950
Connection ~ 1125 4950
$Comp
L PMSM_Driver_MZ_Split_Board_Device:C_Small C?
U 1 1 5EEDDFBC
P 6400 4125
AR Path="/5EB87562/5EEDDFBC" Ref="C?"  Part="1" 
AR Path="/5E9077E4/5EEDDFBC" Ref="C29"  Part="1" 
F 0 "C29" H 6492 4171 50  0000 L CNN
F 1 "22uF" H 6500 4075 50  0000 L CNN
F 2 "PMSM_Driver_MZ_Split_Board:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6400 4125 50  0001 C CNN
F 3 "~" H 6400 4125 50  0001 C CNN
F 4 "GRM21BR61A226ME51L -  SMD Multilayer Ceramic Capacitor, 22 µF, 10 V, 0805 [2012 Metric], ± 20%, X5R, GRM Series" H 6400 3725 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/murata/grm21br61a226me51l/cap-22-f-10v-20-x5r-0805/dp/2494270" H 6400 3625 50  0001 C CNN "Distributor Link"
F 6 "10V" H 6550 4000 50  0000 C CNN "Voltage"
	1    6400 4125
	1    0    0    -1  
$EndComp
Wire Wire Line
	6400 4025 6400 3950
$Comp
L PMSM_Driver_MZ_Split_Board_Device:C_Small C?
U 1 1 5EEE246E
P 6400 5125
AR Path="/5EB87562/5EEE246E" Ref="C?"  Part="1" 
AR Path="/5E9077E4/5EEE246E" Ref="C30"  Part="1" 
F 0 "C30" H 6492 5171 50  0000 L CNN
F 1 "22uF" H 6492 5080 50  0000 L CNN
F 2 "PMSM_Driver_MZ_Split_Board:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6400 5125 50  0001 C CNN
F 3 "~" H 6400 5125 50  0001 C CNN
F 4 "GRM21BR61A226ME51L -  SMD Multilayer Ceramic Capacitor, 22 µF, 10 V, 0805 [2012 Metric], ± 20%, X5R, GRM Series" H 6400 4725 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/murata/grm21br61a226me51l/cap-22-f-10v-20-x5r-0805/dp/2494270" H 6400 4625 50  0001 C CNN "Distributor Link"
F 6 "10V" H 6550 5000 50  0000 C CNN "Voltage"
	1    6400 5125
	1    0    0    -1  
$EndComp
Wire Wire Line
	6400 5025 6400 4950
Connection ~ 6400 3950
Connection ~ 6400 4950
NoConn ~ 4250 1250
Wire Wire Line
	2250 5850 2150 5850
Connection ~ 2150 5850
Wire Wire Line
	2150 5850 2150 5750
Wire Wire Line
	2250 5950 2150 5950
Connection ~ 2150 5950
Wire Wire Line
	2150 5950 2150 5850
Wire Wire Line
	4250 5850 4350 5850
Connection ~ 4350 5850
Wire Wire Line
	4350 5850 4350 5750
NoConn ~ 4250 1050
NoConn ~ 4250 1150
NoConn ~ 2250 1050
NoConn ~ 2250 1150
NoConn ~ 7600 1450
Wire Wire Line
	2050 1250 2250 1250
Text GLabel 2050 1250 0    50   Input ~ 0
MZ_PWR_EN
Text GLabel 9800 1450 2    50   Output ~ 0
VCCIO_EN
Wire Wire Line
	9600 1450 9800 1450
Wire Wire Line
	9600 1550 10725 1550
Wire Wire Line
	10725 1550 10725 1350
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_5V0 #PWR?
U 1 1 604A6999
P 10725 1350
AR Path="/5EB87562/604A6999" Ref="#PWR?"  Part="1" 
AR Path="/5E9077E4/604A6999" Ref="#PWR0161"  Part="1" 
F 0 "#PWR0161" H 10725 1600 50  0001 C CNN
F 1 "MZ_5V0" H 10726 1523 50  0000 C CNN
F 2 "" H 10725 1350 50  0001 C CNN
F 3 "" H 10725 1350 50  0001 C CNN
	1    10725 1350
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_VCCO_35 #PWR?
U 1 1 604B076D
P 10725 5675
AR Path="/5EB87562/604B076D" Ref="#PWR?"  Part="1" 
AR Path="/5E9077E4/604B076D" Ref="#PWR0162"  Part="1" 
F 0 "#PWR0162" H 10725 5925 50  0001 C CNN
F 1 "MZ_VCCO_35" H 10726 5848 50  0000 C CNN
F 2 "" H 10725 5675 50  0001 C CNN
F 3 "" H 10725 5675 50  0001 C CNN
	1    10725 5675
	-1   0    0    -1  
$EndComp
Wire Wire Line
	9600 5850 10725 5850
Wire Wire Line
	10725 5675 10725 5850
Text GLabel 4450 3050 2    50   Output ~ 0
MZ_M1_ADC_CLK
Text GLabel 4450 4750 2    50   Output ~ 0
MZ_M1_VT_ENABLE
Text GLabel 2050 4350 0    50   Output ~ 0
MZ_M1_PWM_AH
Text GLabel 2050 4450 0    50   Output ~ 0
MZ_M1_PWM_AL
Text GLabel 2050 4650 0    50   Output ~ 0
MZ_M1_PWM_BH
Text GLabel 2050 4750 0    50   Output ~ 0
MZ_M1_PWM_BL
Text GLabel 2050 5050 0    50   Output ~ 0
MZ_M1_PWM_CH
Text GLabel 2050 5150 0    50   Output ~ 0
MZ_M1_PWM_CL
Text GLabel 2050 4150 0    50   Output ~ 0
MZ_M1_EN
Text GLabel 4450 5150 2    50   Output ~ 0
MZ_M1_RST_OVR_CUR
Text GLabel 4450 5050 2    50   Input ~ 0
MZ_M1_NOT_OVR_CUR
Text GLabel 7400 4650 0    50   Output ~ 0
MZ_M2_PWM_AH
Text GLabel 7400 4750 0    50   Output ~ 0
MZ_M2_PWM_AL
Text GLabel 7400 5350 0    50   Output ~ 0
MZ_M2_PWM_BH
Text GLabel 7400 5450 0    50   Output ~ 0
MZ_M2_PWM_BL
Text GLabel 9800 5450 2    50   Output ~ 0
MZ_M2_PWM_CH
Text GLabel 9800 5350 2    50   Output ~ 0
MZ_M2_PWM_CL
Text GLabel 7400 4450 0    50   Output ~ 0
MZ_M2_EN
Text GLabel 9800 5150 2    50   Output ~ 0
MZ_M2_RST_OVR_CUR
Text GLabel 9800 5050 2    50   Input ~ 0
MZ_M2_NOT_OVR_CUR
Text GLabel 2050 1650 0    50   Input ~ 0
MZ_M1_V_A
Text GLabel 2050 1850 0    50   Input ~ 0
MZ_M1_V_B
Text GLabel 2050 1950 0    50   Input ~ 0
MZ_M1_V_C
Text GLabel 2050 2150 0    50   Input ~ 0
MZ_M1_CUR_A
Text GLabel 2050 2250 0    50   Input ~ 0
MZ_M1_CUR_B
Text GLabel 2050 2450 0    50   Input ~ 0
MZ_M1_CUR_T
Text GLabel 2050 2850 0    50   Input ~ 0
MZ_M1_ENC_I
Text GLabel 4450 2450 2    50   Input ~ 0
MZ_M1_RES_A
Text GLabel 4450 2550 2    50   Input ~ 0
MZ_M1_RES_B
Text GLabel 2050 3450 0    50   Input ~ 0
MZ_M1_HAL_A
Text GLabel 2050 3650 0    50   Input ~ 0
MZ_M1_HAL_B
Text GLabel 2050 3750 0    50   Input ~ 0
MZ_M1_HAL_C
Text GLabel 2050 4050 0    50   Input ~ 0
MZ_M1_TEMP
Text GLabel 2050 3350 0    50   Input ~ 0
MZ_M1_RES_I
Text GLabel 4450 3450 2    50   Output ~ 0
MZ_M1_SPI_SCK
Text GLabel 2050 2750 0    50   Input ~ 0
MZ_M1_ENC_B
Text GLabel 2050 2550 0    50   Input ~ 0
MZ_M1_ENC_A
Text GLabel 2050 1550 0    50   Input ~ 0
MZ_M1_V_SUP
Text GLabel 4450 3650 2    50   Output ~ 0
MZ_M1_SPI_MOSI
Text GLabel 4450 3750 2    50   Input ~ 0
MZ_M1_SPI_MISO
Text GLabel 4450 3350 2    50   Input ~ 0
MZ_M1_SPI_SEL_AUX1
Text GLabel 4450 4450 2    50   BiDi ~ 0
MZ_M1_SDA
Text GLabel 2050 3050 0    50   Output ~ 0
MZ_M1_SCL
Text GLabel 9800 4650 2    50   Input ~ 0
MZ_M2_V_A
Text GLabel 9800 4450 2    50   Input ~ 0
MZ_M2_V_B
Text GLabel 9800 4350 2    50   Input ~ 0
MZ_M2_V_C
Text GLabel 9800 4150 2    50   Input ~ 0
MZ_M2_CUR_A
Text GLabel 9800 4050 2    50   Input ~ 0
MZ_M2_CUR_B
Text GLabel 9800 3750 2    50   Input ~ 0
MZ_M2_CUR_T
Text GLabel 9800 3350 2    50   Input ~ 0
MZ_M2_ENC_I
Text GLabel 9800 3050 2    50   Input ~ 0
MZ_M2_RES_B
Text GLabel 9800 2750 2    50   Input ~ 0
MZ_M2_HAL_A
Text GLabel 9800 2550 2    50   Input ~ 0
MZ_M2_HAL_B
Text GLabel 9800 2450 2    50   Input ~ 0
MZ_M2_HAL_C
Text GLabel 9800 2250 2    50   Input ~ 0
MZ_M2_TEMP
Text GLabel 9800 2850 2    50   Input ~ 0
MZ_M2_RES_I
Text GLabel 9800 2150 2    50   Output ~ 0
MZ_M2_RDC_SAMPLE_N
Text GLabel 9800 3450 2    50   Input ~ 0
MZ_M2_ENC_B
Text GLabel 9800 3650 2    50   Input ~ 0
MZ_M2_ENC_A
Text GLabel 9800 4750 2    50   Input ~ 0
MZ_M2_V_SUP
Text GLabel 9800 3150 2    50   Input ~ 0
MZ_M2_RES_A
NoConn ~ 4250 1350
NoConn ~ 2250 1350
NoConn ~ 2250 1450
NoConn ~ 4250 1450
NoConn ~ 4250 5650
NoConn ~ 4250 5550
NoConn ~ 4250 5450
NoConn ~ 4250 5350
NoConn ~ 2250 5650
NoConn ~ 2250 5550
NoConn ~ 2250 5450
NoConn ~ 2250 5350
NoConn ~ 7600 5950
NoConn ~ 7600 5850
NoConn ~ 7600 5750
NoConn ~ 7600 5650
NoConn ~ 9600 5950
NoConn ~ 9600 5750
NoConn ~ 9600 5650
NoConn ~ 9600 1650
NoConn ~ 7600 1650
Wire Wire Line
	4450 2450 4250 2450
Wire Wire Line
	4450 2550 4250 2550
Wire Wire Line
	2050 3350 2250 3350
Wire Wire Line
	2050 3450 2250 3450
Wire Wire Line
	2050 3650 2250 3650
Wire Wire Line
	2050 3750 2250 3750
Wire Wire Line
	2050 4050 2250 4050
Wire Wire Line
	2050 4150 2250 4150
Wire Wire Line
	2050 1550 2250 1550
Wire Wire Line
	2050 1650 2250 1650
Wire Wire Line
	2050 1850 2250 1850
Wire Wire Line
	2050 1950 2250 1950
Wire Wire Line
	2050 2150 2250 2150
Wire Wire Line
	2050 2250 2250 2250
Wire Wire Line
	2050 2450 2250 2450
Wire Wire Line
	2050 2550 2250 2550
Wire Wire Line
	2050 2750 2250 2750
Wire Wire Line
	2050 2850 2250 2850
Wire Wire Line
	2050 4350 2250 4350
Wire Wire Line
	2050 4450 2250 4450
Wire Wire Line
	2050 4650 2250 4650
Wire Wire Line
	2050 4750 2250 4750
Wire Wire Line
	2050 5050 2250 5050
Wire Wire Line
	2050 5150 2250 5150
Wire Wire Line
	4250 5050 4450 5050
Wire Wire Line
	4250 5150 4450 5150
Wire Wire Line
	9800 1850 9600 1850
Wire Wire Line
	9800 1950 9600 1950
Wire Wire Line
	9800 2150 9600 2150
Wire Wire Line
	9800 2250 9600 2250
Wire Wire Line
	9800 2450 9600 2450
Wire Wire Line
	9800 2550 9600 2550
Wire Wire Line
	9600 2750 9800 2750
Wire Wire Line
	9600 2850 9800 2850
Wire Wire Line
	9600 3050 9800 3050
Wire Wire Line
	9600 3150 9800 3150
Wire Wire Line
	9600 3350 9800 3350
Wire Wire Line
	9600 3450 9800 3450
Wire Wire Line
	9600 3650 9800 3650
Wire Wire Line
	9600 3750 9800 3750
Wire Wire Line
	9600 4050 9800 4050
Wire Wire Line
	9600 4150 9800 4150
Wire Wire Line
	9600 4350 9800 4350
Wire Wire Line
	9600 4450 9800 4450
Wire Wire Line
	9600 4650 9800 4650
Wire Wire Line
	9600 4750 9800 4750
Wire Wire Line
	9600 5050 9800 5050
Wire Wire Line
	9600 5150 9800 5150
Wire Wire Line
	9600 5350 9800 5350
Wire Wire Line
	9600 5450 9800 5450
Wire Wire Line
	7400 5350 7600 5350
Wire Wire Line
	7400 5450 7600 5450
Wire Wire Line
	7400 4750 7600 4750
Wire Wire Line
	7400 4650 7600 4650
Wire Wire Line
	7400 4450 7600 4450
Text GLabel 9800 1950 2    50   Input ~ 0
MZ_M2_SPI_SEL_RDC2
Text GLabel 7400 3350 0    50   Output ~ 0
MZ_M2_SPI_SCK
Text GLabel 7400 2250 0    50   Output ~ 0
MZ_M2_SPI_MOSI
Text GLabel 7400 2150 0    50   Input ~ 0
MZ_M2_SPI_MISO
Text GLabel 9800 1850 2    50   Input ~ 0
MZ_M2_SPI_SEL_AUX2
Text GLabel 7400 2850 0    50   BiDi ~ 0
MZ_M2_SDA
Text GLabel 7400 3450 0    50   Output ~ 0
MZ_M2_SCL
Wire Wire Line
	4250 4750 4450 4750
Wire Wire Line
	2250 3050 2050 3050
Wire Wire Line
	4250 3050 4450 3050
Wire Wire Line
	4250 4450 4450 4450
Wire Wire Line
	4250 3750 4450 3750
Wire Wire Line
	4250 3650 4450 3650
Wire Wire Line
	4250 3450 4450 3450
Wire Wire Line
	4250 3150 4450 3150
Wire Wire Line
	4250 2850 4450 2850
Text GLabel 4450 3150 2    50   Input ~ 0
MZ_M1_SPI_SEL_RDC1
Text GLabel 4450 2850 2    50   Input ~ 0
MZ_M1_RDC_SAMPLE_N
Wire Wire Line
	4250 3350 4450 3350
Text GLabel 7400 4050 0    50   Output ~ 0
MZ_M2_VT_ENABLE
Text GLabel 7400 3750 0    50   Output ~ 0
MZ_M2_ADC_CLK
Wire Wire Line
	7400 3750 7600 3750
Wire Wire Line
	7400 4050 7600 4050
Wire Wire Line
	6400 3850 7600 3850
Wire Wire Line
	6400 3950 7600 3950
Wire Wire Line
	7600 3350 7400 3350
Wire Wire Line
	7600 2850 7400 2850
Wire Wire Line
	7600 2150 7400 2150
Wire Wire Line
	7600 2250 7400 2250
Wire Wire Line
	7600 3450 7400 3450
NoConn ~ 7600 3050
NoConn ~ 7600 3150
NoConn ~ 7600 3650
NoConn ~ 7600 2550
NoConn ~ 7600 2450
NoConn ~ 7600 1950
NoConn ~ 7600 4350
NoConn ~ 7600 5150
NoConn ~ 7600 5050
NoConn ~ 4250 2750
NoConn ~ 4250 2250
NoConn ~ 4250 2150
NoConn ~ 4250 1950
NoConn ~ 4250 1850
NoConn ~ 4250 1650
NoConn ~ 4250 1550
NoConn ~ 4250 4050
NoConn ~ 4250 4350
Wire Wire Line
	4350 5950 4350 5850
Wire Wire Line
	4350 6150 4350 5950
Connection ~ 4350 5950
Wire Wire Line
	4250 5950 4350 5950
Wire Wire Line
	6400 4950 7600 4950
Wire Notes Line
	11100 6475 11100 600 
Wire Notes Line
	5875 6475 5875 600 
Text Notes 5950 800  0    100  ~ 0
JX2 MicroZed Connector (Motor 2)
Wire Notes Line
	600  6475 600  600 
Wire Notes Line
	5800 600  5800 6475
Wire Notes Line
	5875 6475 11100 6475
Wire Notes Line
	5800 6475 600  6475
Wire Notes Line
	5800 600  600  600 
Wire Notes Line
	5875 600  11100 600 
Text Notes 675  800  0    100  ~ 0
JX1 MicroZed Connector (Motor 1)
NoConn ~ 4250 4650
NoConn ~ 4250 4150
NoConn ~ 2250 3150
NoConn ~ 7600 4150
$Comp
L PMSM_Driver_MZ_Split_Board_Connector:MicroZed_JX2 JX2
U 1 1 5E94CFEE
P 8600 3500
F 0 "JX2" H 7850 6100 50  0000 C CNN
F 1 "61083-104400LF" H 9075 900 50  0000 C CNN
F 2 "PMSM_Driver_MZ_Split_Board:61083-104400LF" H 8350 800 60  0001 C CNN
F 3 "~" H 8600 3500 60  0001 C CNN
F 4 "61083-104400LF -  Stacking Board Connector, Bergstak, FCI BergStak 61083 Series, 100 Contacts, Plug, 0.8 mm" H 8600 700 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/amphenol-icc-fci/61083-104400lf/plug-smd-0-8mm-100way/dp/1103987" H 8600 600 50  0001 C CNN "Distributor Link"
	1    8600 3500
	1    0    0    -1  
$EndComp
NoConn ~ 7600 2750
NoConn ~ 7600 1850
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_GND #PWR?
U 1 1 5F2FF56A
P 2150 6150
F 0 "#PWR?" H 2150 5900 50  0001 C CNN
F 1 "MZ_GND" H 2155 5977 50  0000 C CNN
F 2 "" H 2150 6150 50  0001 C CNN
F 3 "" H 2150 6150 50  0001 C CNN
	1    2150 6150
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_GND #PWR?
U 1 1 5F30C705
P 4350 6150
F 0 "#PWR?" H 4350 5900 50  0001 C CNN
F 1 "MZ_GND" H 4355 5977 50  0000 C CNN
F 2 "" H 4350 6150 50  0001 C CNN
F 3 "" H 4350 6150 50  0001 C CNN
	1    4350 6150
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_GND #PWR?
U 1 1 5F326C98
P 7500 6150
F 0 "#PWR?" H 7500 5900 50  0001 C CNN
F 1 "MZ_GND" H 7505 5977 50  0000 C CNN
F 2 "" H 7500 6150 50  0001 C CNN
F 3 "" H 7500 6150 50  0001 C CNN
	1    7500 6150
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_GND #PWR?
U 1 1 5F333D79
P 9700 6150
F 0 "#PWR?" H 9700 5900 50  0001 C CNN
F 1 "MZ_GND" H 9705 5977 50  0000 C CNN
F 2 "" H 9700 6150 50  0001 C CNN
F 3 "" H 9700 6150 50  0001 C CNN
	1    9700 6150
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_GND #PWR?
U 1 1 5F34DE3C
P 6400 5275
F 0 "#PWR?" H 6400 5025 50  0001 C CNN
F 1 "MZ_GND" H 6405 5102 50  0000 C CNN
F 2 "" H 6400 5275 50  0001 C CNN
F 3 "" H 6400 5275 50  0001 C CNN
	1    6400 5275
	1    0    0    -1  
$EndComp
Wire Wire Line
	6400 5275 6400 5225
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_GND #PWR?
U 1 1 5F37561B
P 6400 4275
F 0 "#PWR?" H 6400 4025 50  0001 C CNN
F 1 "MZ_GND" H 6405 4102 50  0000 C CNN
F 2 "" H 6400 4275 50  0001 C CNN
F 3 "" H 6400 4275 50  0001 C CNN
	1    6400 4275
	1    0    0    -1  
$EndComp
Wire Wire Line
	6400 4275 6400 4225
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_GND #PWR?
U 1 1 5F38FB33
P 1125 4275
F 0 "#PWR?" H 1125 4025 50  0001 C CNN
F 1 "MZ_GND" H 1130 4102 50  0000 C CNN
F 2 "" H 1125 4275 50  0001 C CNN
F 3 "" H 1125 4275 50  0001 C CNN
	1    1125 4275
	1    0    0    -1  
$EndComp
Wire Wire Line
	1125 4275 1125 4225
$Comp
L PMSM_Driver_MZ_Split_Board_Power:MZ_GND #PWR?
U 1 1 5F3AA468
P 1125 5275
F 0 "#PWR?" H 1125 5025 50  0001 C CNN
F 1 "MZ_GND" H 1130 5102 50  0000 C CNN
F 2 "" H 1125 5275 50  0001 C CNN
F 3 "" H 1125 5275 50  0001 C CNN
	1    1125 5275
	1    0    0    -1  
$EndComp
Wire Wire Line
	1125 5275 1125 5225
$EndSCHEMATC
