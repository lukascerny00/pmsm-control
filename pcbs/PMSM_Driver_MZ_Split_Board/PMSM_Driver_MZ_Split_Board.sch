EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 4
Title "PMSM_Driver_MZ_Split_Board"
Date "2020-03-30"
Rev "A"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 1000 1000 2000 1000
U 5E9077E4
F0 "MicroZed_Connector" 50
F1 "schematics/PMSM_Driver_MZ_Split_Board_MZ_Conn.sch" 50
$EndSheet
Text Notes 1050 1150 0    50   ~ 0
Page 2
Text Notes 3550 1150 0    50   ~ 0
Page 3
Text Notes 6050 1150 0    50   ~ 0
Page 4
$Sheet
S 3500 1000 2000 1000
U 5E9110E2
F0 "Motor_Connectors" 50
F1 "schematics/PMSM_Driver_MZ_Split_Board_Mot_Conn.sch" 50
$EndSheet
$Sheet
S 6000 1000 2000 1000
U 5EB87562
F0 "Power" 50
F1 "schematics/PMSM_Driver_MZ_Split_Board_Power.sch" 50
$EndSheet
$EndSCHEMATC
