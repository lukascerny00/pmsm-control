EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 3
Title "PMSM_Driver_ZB_Split_Board"
Date "2020-03-30"
Rev "A"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L PMSM_Driver_ZB_Split_Board_Power:ZB_3V3_AUX #PWR?
U 1 1 5E8BCD6B
P 4525 6050
AR Path="/5E8BCD6B" Ref="#PWR?"  Part="1" 
AR Path="/5E893D08/5E8BCD6B" Ref="#PWR0101"  Part="1" 
F 0 "#PWR0101" H 4525 6300 50  0001 C CNN
F 1 "ZB_3V3_AUX" H 4526 6223 50  0000 C CNN
F 2 "" H 4525 6050 50  0001 C CNN
F 3 "" H 4525 6050 50  0001 C CNN
	1    4525 6050
	1    0    0    -1  
$EndComp
Wire Wire Line
	5025 6600 5025 6500
Wire Wire Line
	5025 7100 5025 6800
Wire Wire Line
	5025 6200 5025 6300
Wire Wire Line
	4525 6200 4525 6700
$Comp
L PMSM_Driver_ZB_Split_Board_Connector:Jumper_NC_Small JP?
U 1 1 5E8BCD43
P 5025 6700
AR Path="/5E8BCD43" Ref="JP?"  Part="1" 
AR Path="/5E893D08/5E8BCD43" Ref="JP1"  Part="1" 
F 0 "JP1" V 4975 6850 50  0000 C CNN
F 1 "JUMPER" V 5075 6900 50  0000 C CNN
F 2 "PMSM_Driver_ZB_Split_Board:PinHeader_1x02_P2.54mm_Vertical" H 4925 6550 50  0001 C CNN
F 3 "~" H 5025 6700 50  0001 C CNN
F 4 "2211S-02G -  Board-To-Board Connector, 2.54 mm, 2 Contacts, Header, 2211S Series, Through Hole, 1 Rows " H 5025 6700 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/2211s-02g/header-1-row-vert-2way/dp/1593411" H 5025 6700 50  0001 C CNN "Distributor Link"
	1    5025 6700
	0    1    1    0   
$EndComp
Wire Wire Line
	4425 6200 4525 6200
Wire Wire Line
	4225 6200 4125 6200
Wire Wire Line
	4125 6300 4125 6200
Wire Wire Line
	4525 6050 4525 6200
$Comp
L PMSM_Driver_ZB_Split_Board_Device:C_Small C?
U 1 1 5E8BCD39
P 4325 6200
AR Path="/5E8BCD39" Ref="C?"  Part="1" 
AR Path="/5E893D08/5E8BCD39" Ref="C6"  Part="1" 
F 0 "C6" V 4225 6200 50  0000 C CNN
F 1 "100nF" V 4425 6200 50  0000 C CNN
F 2 "PMSM_Driver_ZB_Split_Board:C_0402_1005Metric" H 4325 6200 50  0001 C CNN
F 3 "~" H 4325 6200 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series " H 4325 6200 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 4325 6200 50  0001 C CNN "Distributor Link"
	1    4325 6200
	0    1    1    0   
$EndComp
Connection ~ 4525 6200
Wire Wire Line
	4525 6200 5025 6200
$Comp
L PMSM_Driver_ZB_Split_Board_Device:R_Small_US R?
U 1 1 5E8BCD2B
P 5025 6400
AR Path="/5E8BCD2B" Ref="R?"  Part="1" 
AR Path="/5E893D08/5E8BCD2B" Ref="R3"  Part="1" 
F 0 "R3" H 5125 6400 50  0000 C CNN
F 1 "4k7" H 5125 6300 50  0000 C CNN
F 2 "PMSM_Driver_ZB_Split_Board:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 5025 6400 50  0001 C CNN
F 3 "~" H 5025 6400 50  0001 C CNN
F 4 "MC0603SAF4701T5E -  RES, THICK FILM, 4K7, 1%, 0.1W, 0603" H 5025 6400 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0603saf4701t5e/res-thick-film-4k7-1-0-1w-0603/dp/1632439" H 5025 6400 50  0001 C CNN "Distributor Link"
	1    5025 6400
	1    0    0    -1  
$EndComp
Wire Wire Line
	4925 7100 5025 7100
Wire Wire Line
	3525 7250 3625 7250
Wire Wire Line
	3525 6900 3625 6900
$Comp
L PMSM_Driver_ZB_Split_Board_Device:R_Small_US R?
U 1 1 5E8BCD22
P 3725 7250
AR Path="/5E8BCD22" Ref="R?"  Part="1" 
AR Path="/5E893D08/5E8BCD22" Ref="R2"  Part="1" 
F 0 "R2" V 3800 7250 50  0000 C CNN
F 1 "1k" V 3625 7250 50  0000 C CNN
F 2 "PMSM_Driver_ZB_Split_Board:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 3725 7250 50  0001 C CNN
F 3 "~" H 3725 7250 50  0001 C CNN
F 4 "ERJ3EKF1001V -  SMD Chip Resistor, 0603 [1608 Metric], 1 kohm, ERJ3EK Series, 75 V, Thick Film, 100 mW" H 3725 7250 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/panasonic/erj3ekf1001v/res-1k-1-0-1w-0603-thick-film/dp/2303145?st=ERJ-3EKF1001V" H 3725 7250 50  0001 C CNN "Distributor Link"
	1    3725 7250
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3925 7000 3925 7250
Wire Wire Line
	3825 7250 3925 7250
Wire Wire Line
	4125 7000 3925 7000
Text GLabel 3525 7250 0    50   Input ~ 0
FMC_GA0
Text GLabel 3525 6900 0    50   Input ~ 0
FMC_GA1
Wire Wire Line
	4025 7100 4025 7400
Wire Wire Line
	4025 7100 4125 7100
Wire Wire Line
	4525 7400 4525 7300
NoConn ~ 9825 1100
Wire Wire Line
	8075 4600 8325 4600
Wire Wire Line
	10075 4500 9825 4500
Wire Wire Line
	8075 4400 8325 4400
Wire Wire Line
	10075 3200 9825 3200
Wire Wire Line
	10075 3300 9825 3300
Wire Wire Line
	8075 1200 8325 1200
Wire Wire Line
	8075 1700 8325 1700
Wire Wire Line
	5825 1900 6075 1900
Wire Wire Line
	6075 1800 5825 1800
Wire Wire Line
	8075 2000 8325 2000
Wire Wire Line
	8075 1600 8325 1600
Wire Wire Line
	6075 2100 5825 2100
Wire Wire Line
	4075 2100 4325 2100
Text GLabel 10075 1500 2    50   Input ~ 0
ZB_M1_TEMP
Text GLabel 8075 1700 0    50   Input ~ 0
ZB_M1_HAL_B
Text GLabel 10075 1700 2    50   Input ~ 0
ZB_M1_HAL_A
Text GLabel 8075 2000 0    50   Input ~ 0
ZB_M1_ENC_I
Text GLabel 6075 1900 2    50   Input ~ 0
ZB_M1_RES_A
Text GLabel 6075 1800 2    50   Input ~ 0
ZB_M1_RES_B
Wire Wire Line
	4325 3700 4075 3700
Wire Wire Line
	5825 3300 6075 3300
Wire Wire Line
	9825 3600 10075 3600
Wire Wire Line
	5825 3000 6075 3000
Wire Wire Line
	5825 2200 6075 2200
Wire Wire Line
	9825 2300 10075 2300
Wire Wire Line
	8325 2300 8075 2300
Wire Wire Line
	9825 2400 10075 2400
Wire Wire Line
	4075 2400 4325 2400
Wire Wire Line
	4075 2500 4325 2500
Wire Wire Line
	6075 2800 5825 2800
Wire Wire Line
	10075 2600 9825 2600
Wire Wire Line
	4075 2800 4325 2800
NoConn ~ 4325 1700
NoConn ~ 4325 1600
NoConn ~ 4325 1300
NoConn ~ 4325 1200
NoConn ~ 5825 1500
NoConn ~ 5825 1400
Wire Wire Line
	5825 1100 6075 1100
Wire Wire Line
	4325 3300 4075 3300
Wire Wire Line
	4075 3200 4325 3200
$Comp
L PMSM_Driver_ZB_Split_Board_Power:ZB_VADJ #PWR0105
U 1 1 5EA27B29
P 7400 4800
F 0 "#PWR0105" H 7400 5050 50  0001 C CNN
F 1 "ZB_VADJ" H 7401 4973 50  0000 C CNN
F 2 "" H 7400 4800 50  0001 C CNN
F 3 "" H 7400 4800 50  0001 C CNN
	1    7400 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	10825 5000 10825 4900
$Comp
L PMSM_Driver_ZB_Split_Board_Power:ZB_VADJ #PWR0106
U 1 1 5EA1DC22
P 10825 4900
F 0 "#PWR0106" H 10825 5150 50  0001 C CNN
F 1 "ZB_VADJ" H 10826 5073 50  0000 C CNN
F 2 "" H 10825 4900 50  0001 C CNN
F 3 "" H 10825 4900 50  0001 C CNN
	1    10825 4900
	1    0    0    -1  
$EndComp
NoConn ~ 5825 4400
NoConn ~ 5825 4300
NoConn ~ 5825 3900
Wire Wire Line
	4075 4100 4325 4100
Wire Wire Line
	4075 4000 4325 4000
Text GLabel 4075 4000 0    50   Output ~ 0
ZB_FMC_SCL
Text GLabel 4075 4100 0    50   BiDi ~ 0
ZB_FMC_SDA
Text GLabel 6075 3400 2    50   Input ~ 0
ZB_SPI_MISO
Text GLabel 10075 3600 2    50   Output ~ 0
ZB_M2_RDC_SAMPLE_N
Text GLabel 4075 3600 0    50   Output ~ 0
ZB_SPI_SEL_RDC2
Text GLabel 6075 3000 2    50   Output ~ 0
ZB_M2_PWM_CL
Text GLabel 10075 3000 2    50   Output ~ 0
ZB_M2_PWM_CH
Text GLabel 10075 3200 2    50   Output ~ 0
ZB_M2_PWM_BH
Text GLabel 10075 3300 2    50   Output ~ 0
ZB_M2_PWM_AH
Text GLabel 10075 1400 2    50   Output ~ 0
ZB_M1_RDC_SAMPLE_N
Text GLabel 8075 1300 0    50   Output ~ 0
ZB_SPI_SEL_RDC1
Text GLabel 10075 1800 2    50   Input ~ 0
ZB_M1_RES_I
Text GLabel 4075 2500 0    50   Output ~ 0
ZB_M1_RST_OVR_CUR
Text GLabel 4075 2100 0    50   Input ~ 0
ZB_M1_CUR_A
Text GLabel 4075 2400 0    50   Input ~ 0
ZB_M1_NOT_OVR_CUR
Text GLabel 6075 2200 2    50   Input ~ 0
ZB_M1_V_C
Text GLabel 10075 2300 2    50   Input ~ 0
ZB_M1_V_B
Text GLabel 8075 2300 0    50   Input ~ 0
ZB_M1_V_A
Text GLabel 10075 2400 2    50   Input ~ 0
ZB_M1_V_SUP
Text GLabel 6075 2800 2    50   Output ~ 0
ZB_M1_PWM_BL
Text GLabel 10075 2600 2    50   Output ~ 0
ZB_M1_PWM_BH
Text GLabel 4075 2800 0    50   Output ~ 0
ZB_M1_PWM_AL
Text GLabel 4075 3300 0    50   Output ~ 0
ZB_VT_ENABLE
Text GLabel 4075 3200 0    50   Output ~ 0
ZB_ADC_CLK
Wire Wire Line
	5825 4200 6375 4200
Wire Wire Line
	5825 5000 6575 5000
Wire Wire Line
	5825 4800 6575 4800
Wire Wire Line
	5825 4600 6575 4600
Wire Wire Line
	3275 4900 4325 4900
Wire Wire Line
	4075 4400 4325 4400
Text GLabel 4075 4400 0    50   Output ~ 0
FMC_GA0
Wire Wire Line
	5825 4500 6075 4500
Text GLabel 6075 4500 2    50   Output ~ 0
FMC_GA1
Connection ~ 6575 4800
Wire Wire Line
	6575 5000 6575 4800
Connection ~ 6575 4600
Wire Wire Line
	6575 4800 6575 4600
Wire Wire Line
	6575 4600 6575 4450
$Comp
L PMSM_Driver_ZB_Split_Board_Power:ZB_3V3 #PWR0107
U 1 1 5E8ABCED
P 6575 4450
F 0 "#PWR0107" H 6575 4700 50  0001 C CNN
F 1 "ZB_3V3" H 6576 4623 50  0000 C CNN
F 2 "" H 6575 4450 50  0001 C CNN
F 3 "" H 6575 4450 50  0001 C CNN
	1    6575 4450
	1    0    0    -1  
$EndComp
Wire Wire Line
	3275 4900 3275 4800
$Comp
L PMSM_Driver_ZB_Split_Board_Power:ZB_3V3 #PWR0109
U 1 1 5E8ABCDD
P 3275 4800
F 0 "#PWR0109" H 3275 5050 50  0001 C CNN
F 1 "ZB_3V3" H 3276 4973 50  0000 C CNN
F 2 "" H 3275 4800 50  0001 C CNN
F 3 "" H 3275 4800 50  0001 C CNN
	1    3275 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	4175 5000 4325 5000
Wire Wire Line
	4325 4800 4175 4800
Wire Wire Line
	4325 4600 4175 4600
Wire Wire Line
	4175 4600 4175 4800
Connection ~ 4175 4800
Wire Wire Line
	4325 4300 4175 4300
Wire Wire Line
	4175 4300 4175 4600
Connection ~ 4175 4600
Wire Wire Line
	4325 4200 4175 4200
Connection ~ 4175 4300
Wire Wire Line
	4325 3900 4175 3900
Wire Wire Line
	4175 3900 4175 4200
Connection ~ 4175 4200
Wire Wire Line
	4175 4200 4175 4300
Wire Wire Line
	4325 3800 4175 3800
Wire Wire Line
	4175 3800 4175 3900
Connection ~ 4175 3900
Wire Wire Line
	4325 3500 4175 3500
Connection ~ 4175 3800
Wire Wire Line
	4325 3400 4175 3400
Wire Wire Line
	4175 3400 4175 3500
Connection ~ 4175 3500
Wire Wire Line
	4175 3500 4175 3800
Wire Wire Line
	4325 3100 4175 3100
Wire Wire Line
	4175 3100 4175 3400
Connection ~ 4175 3400
Wire Wire Line
	4325 3000 4175 3000
Wire Wire Line
	4175 3000 4175 3100
Connection ~ 4175 3100
Wire Wire Line
	4325 2700 4175 2700
Wire Wire Line
	4175 2700 4175 3000
Connection ~ 4175 3000
Wire Wire Line
	4325 2600 4175 2600
Wire Wire Line
	4175 2600 4175 2700
Connection ~ 4175 2700
Wire Wire Line
	4325 2300 4175 2300
Wire Wire Line
	4175 2300 4175 2600
Connection ~ 4175 2600
Wire Wire Line
	4325 2200 4175 2200
Connection ~ 4175 2300
Wire Wire Line
	4325 1900 4175 1900
Connection ~ 4175 2200
Wire Wire Line
	4175 2200 4175 2300
Wire Wire Line
	4325 1800 4175 1800
Wire Wire Line
	4175 1800 4175 1900
Connection ~ 4175 1900
Wire Wire Line
	4175 1900 4175 2200
Wire Wire Line
	4325 1500 4175 1500
Wire Wire Line
	4175 1500 4175 1800
Connection ~ 4175 1800
Wire Wire Line
	4325 1400 4175 1400
Connection ~ 4175 1500
Wire Wire Line
	4325 1100 4175 1100
Wire Wire Line
	4175 1100 4175 1400
Connection ~ 4175 1400
Wire Wire Line
	4175 1400 4175 1500
Wire Wire Line
	4175 4800 4175 5000
Wire Wire Line
	5975 4900 5825 4900
Wire Wire Line
	5825 4700 5975 4700
Wire Wire Line
	5975 4700 5975 4900
Wire Wire Line
	5825 3800 5975 3800
Wire Wire Line
	5975 3800 5975 4700
Connection ~ 5975 4700
Wire Wire Line
	5825 3500 5975 3500
Wire Wire Line
	5975 3500 5975 3800
Connection ~ 5975 3800
Wire Wire Line
	5825 3200 5975 3200
Wire Wire Line
	5975 3200 5975 3500
Connection ~ 5975 3500
Wire Wire Line
	5825 2900 5975 2900
Wire Wire Line
	5975 2900 5975 3200
Connection ~ 5975 3200
Wire Wire Line
	5825 2600 5975 2600
Wire Wire Line
	5975 2600 5975 2900
Connection ~ 5975 2900
Wire Wire Line
	5825 2300 5975 2300
Wire Wire Line
	5975 2300 5975 2600
Connection ~ 5975 2600
Wire Wire Line
	5825 2000 5975 2000
Wire Wire Line
	5975 2000 5975 2300
Connection ~ 5975 2300
Wire Wire Line
	5825 1700 5975 1700
Wire Wire Line
	5975 1700 5975 2000
Connection ~ 5975 2000
Wire Wire Line
	5825 1600 5975 1600
Wire Wire Line
	5975 1600 5975 1700
Connection ~ 5975 1700
Wire Wire Line
	5825 1300 5975 1300
Wire Wire Line
	5975 1300 5975 1600
Connection ~ 5975 1600
Wire Wire Line
	5825 1200 5975 1200
Wire Wire Line
	5975 1200 5975 1300
Connection ~ 5975 1300
Wire Wire Line
	5825 4100 6075 4100
Wire Wire Line
	6075 4100 6075 4000
Wire Wire Line
	6075 4000 5825 4000
Wire Wire Line
	6375 4200 6375 4100
$Comp
L PMSM_Driver_ZB_Split_Board_Power:ZB_3V3_AUX #PWR0112
U 1 1 5E8ABC60
P 6375 4100
F 0 "#PWR0112" H 6375 4350 50  0001 C CNN
F 1 "ZB_3V3_AUX" H 6376 4273 50  0000 C CNN
F 2 "" H 6375 4100 50  0001 C CNN
F 3 "" H 6375 4100 50  0001 C CNN
	1    6375 4100
	1    0    0    -1  
$EndComp
Connection ~ 9975 1300
Wire Wire Line
	9975 1200 9825 1200
Wire Wire Line
	9975 1300 9975 1200
Connection ~ 9975 1600
Wire Wire Line
	9975 1300 9975 1600
Wire Wire Line
	9825 1300 9975 1300
Connection ~ 9975 1900
Wire Wire Line
	9975 1600 9975 1900
Wire Wire Line
	9825 1600 9975 1600
Connection ~ 9975 2200
Wire Wire Line
	9975 1900 9975 2200
Connection ~ 9975 2500
Wire Wire Line
	9975 2200 9975 2500
Connection ~ 9975 2800
Wire Wire Line
	9975 2500 9975 2800
Connection ~ 9975 3100
Wire Wire Line
	9975 2800 9975 3100
Connection ~ 9975 3400
Wire Wire Line
	9975 3100 9975 3400
Connection ~ 9975 3700
Wire Wire Line
	9975 3400 9975 3700
Connection ~ 9975 4000
Wire Wire Line
	9975 3700 9975 4000
Wire Wire Line
	9825 3700 9975 3700
Connection ~ 9975 4300
Wire Wire Line
	9975 4000 9975 4300
Wire Wire Line
	9825 4000 9975 4000
Connection ~ 9975 4600
Wire Wire Line
	9975 4300 9975 4600
Wire Wire Line
	9825 4300 9975 4300
Wire Wire Line
	9975 4600 9975 4900
Wire Wire Line
	9825 4600 9975 4600
Wire Wire Line
	9975 4900 9825 4900
Connection ~ 8175 1400
Wire Wire Line
	8175 1100 8175 1400
Wire Wire Line
	8325 1100 8175 1100
Connection ~ 8175 1500
Wire Wire Line
	8175 1400 8175 1500
Wire Wire Line
	8325 1400 8175 1400
Connection ~ 8175 1800
Wire Wire Line
	8175 1500 8175 1800
Wire Wire Line
	8325 1500 8175 1500
Connection ~ 8175 2100
Wire Wire Line
	8175 1800 8175 2100
Wire Wire Line
	8325 1800 8175 1800
Connection ~ 8175 2400
Wire Wire Line
	8175 2100 8175 2400
Wire Wire Line
	8325 2100 8175 2100
Connection ~ 8175 2700
Wire Wire Line
	8175 2400 8175 2700
Wire Wire Line
	8325 2400 8175 2400
Connection ~ 8175 3000
Wire Wire Line
	8175 2700 8175 3000
Wire Wire Line
	8325 2700 8175 2700
Connection ~ 8175 3300
Wire Wire Line
	8175 3000 8175 3300
Wire Wire Line
	8325 3000 8175 3000
Connection ~ 8175 3600
Wire Wire Line
	8175 3300 8175 3600
Wire Wire Line
	8325 3300 8175 3300
Connection ~ 8175 3900
Wire Wire Line
	8175 3600 8175 3900
Wire Wire Line
	8325 3600 8175 3600
Connection ~ 8175 4200
Wire Wire Line
	8175 3900 8175 4200
Wire Wire Line
	8325 3900 8175 3900
Connection ~ 8175 4500
Wire Wire Line
	8175 4200 8175 4500
Wire Wire Line
	8325 4200 8175 4200
Connection ~ 8175 4800
Wire Wire Line
	8175 4500 8175 4800
Wire Wire Line
	8325 4500 8175 4500
Wire Wire Line
	8175 4800 8325 4800
Wire Wire Line
	8175 5000 8175 4800
Wire Wire Line
	8175 5000 8325 5000
$Comp
L PMSM_Driver_ZB_Split_Board_IC:M24C02-WMN6TP U2
U 1 1 5E82567A
P 4525 7000
AR Path="/5E82567A" Ref="U2"  Part="1" 
AR Path="/5E893D08/5E82567A" Ref="U2"  Part="1" 
F 0 "U2" H 4325 7250 50  0000 C CNN
F 1 "M24C02-WMN6TP" H 4875 6750 50  0000 C CNN
F 2 "PMSM_Driver_ZB_Split_Board:SOIC-8_3.9x4.9mm_P1.27mm" H 4625 6300 50  0001 C CNN
F 3 "~" H 3875 7750 50  0001 C CNN
F 4 "M24C02-WMN6TP -  EEPROM, 2 Kbit, 256 x 8bit, Serial I2C (2-Wire), 400 kHz, NSOIC, 8 Pins" H 4525 7000 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/stmicroelectronics/m24c02-wmn6tp/eeprom-2kbit-40-to-85deg-c/dp/3335794" H 4525 7000 50  0001 C CNN "Distributor Link"
	1    4525 7000
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_ZB_Split_Board_Connector:Connector_ASP-134604-01 J1
U 1 1 5E86BC5B
P 5075 3000
F 0 "J1" H 4575 5100 50  0000 C CNN
F 1 "Connector_ASP-134604-01" H 5025 800 50  0000 C CNN
F 2 "PMSM_Driver_ZB_Split_Board:Samtec_FMC_ASP-134604-01_4x40_Vertical" H 5175 700 50  0001 C CNN
F 3 "~" H 4575 5100 50  0001 C CNN
F 4 "ASP-134604-01 -  Board-To-Board Connector, Vita 57 XMC, 1.27 mm, 160 Contacts, Header, ASP Series, Surface Mount" H 5075 3000 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/samtec/asp-134604-01/conn-vita-57-fmc-header-160pos/dp/2433507" H 5075 3000 50  0001 C CNN "Distributor Link"
	1    5075 3000
	1    0    0    -1  
$EndComp
Text Notes 3125 750  0    100  ~ 0
FMC Connector - part A
Text Notes 7175 750  0    100  ~ 0
FMC Connector - part B
Text Notes 3125 5775 0    100  ~ 0
FMC EEPROM
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 5E9E89C5
P 1875 2200
F 0 "#FLG0101" H 1875 2275 50  0001 C CNN
F 1 "PWR_FLAG" H 1875 2373 50  0000 C CNN
F 2 "" H 1875 2200 50  0001 C CNN
F 3 "~" H 1875 2200 50  0001 C CNN
	1    1875 2200
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_ZB_Split_Board_Power:ZB_GND #PWR0123
U 1 1 5E9E8F13
P 1875 2300
AR Path="/5E9E8F13" Ref="#PWR0123"  Part="1" 
AR Path="/5E893D08/5E9E8F13" Ref="#PWR0123"  Part="1" 
F 0 "#PWR0123" H 1875 2050 50  0001 C CNN
F 1 "ZB_GND" H 1880 2127 50  0000 C CNN
F 2 "" H 1875 2300 50  0001 C CNN
F 3 "" H 1875 2300 50  0001 C CNN
	1    1875 2300
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_ZB_Split_Board_Power:ZB_3V3 #PWR0124
U 1 1 5E9F8F8F
P 1100 1350
F 0 "#PWR0124" H 1100 1600 50  0001 C CNN
F 1 "ZB_3V3" H 1101 1523 50  0000 C CNN
F 2 "" H 1100 1350 50  0001 C CNN
F 3 "" H 1100 1350 50  0001 C CNN
	1    1100 1350
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_ZB_Split_Board_Power:ZB_3V3_AUX #PWR0126
U 1 1 5EA190A0
P 1875 1350
F 0 "#PWR0126" H 1875 1600 50  0001 C CNN
F 1 "ZB_3V3_AUX" H 1876 1523 50  0000 C CNN
F 2 "" H 1875 1350 50  0001 C CNN
F 3 "" H 1875 1350 50  0001 C CNN
	1    1875 1350
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_ZB_Split_Board_Power:ZB_VADJ #PWR0127
U 1 1 5EA28F16
P 1100 2200
F 0 "#PWR0127" H 1100 2450 50  0001 C CNN
F 1 "ZB_VADJ" H 1101 2373 50  0000 C CNN
F 2 "" H 1100 2200 50  0001 C CNN
F 3 "" H 1100 2200 50  0001 C CNN
	1    1100 2200
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG0102
U 1 1 5EAA9E0F
P 1100 2300
F 0 "#FLG0102" H 1100 2375 50  0001 C CNN
F 1 "PWR_FLAG" H 1100 2473 50  0000 C CNN
F 2 "" H 1100 2300 50  0001 C CNN
F 3 "~" H 1100 2300 50  0001 C CNN
	1    1100 2300
	-1   0    0    1   
$EndComp
Wire Wire Line
	1100 2300 1100 2200
$Comp
L power:PWR_FLAG #FLG0104
U 1 1 5EADAA3A
P 1100 1450
F 0 "#FLG0104" H 1100 1525 50  0001 C CNN
F 1 "PWR_FLAG" H 1100 1623 50  0000 C CNN
F 2 "" H 1100 1450 50  0001 C CNN
F 3 "~" H 1100 1450 50  0001 C CNN
	1    1100 1450
	-1   0    0    1   
$EndComp
Wire Wire Line
	1100 1450 1100 1350
$Comp
L power:PWR_FLAG #FLG0105
U 1 1 5EAEB0E7
P 1875 1450
F 0 "#FLG0105" H 1875 1525 50  0001 C CNN
F 1 "PWR_FLAG" H 1875 1623 50  0000 C CNN
F 2 "" H 1875 1450 50  0001 C CNN
F 3 "~" H 1875 1450 50  0001 C CNN
	1    1875 1450
	-1   0    0    1   
$EndComp
Wire Wire Line
	1875 1450 1875 1350
Wire Wire Line
	1875 2200 1875 2300
Text Notes 625  750  0    100  ~ 0
Flag Power Symbols
Text Notes 625  850  0    50   ~ 0
(for Electrical Rules Checker)
Text GLabel 6075 1100 2    50   Output ~ 0
FMC_PG_C2M
Wire Notes Line
	2500 575  2500 2850
Wire Notes Line
	575  2850 575  575 
Wire Notes Line
	3075 5600 3075 7675
Text Notes 625  5775 0    100  ~ 0
Power Good Sequencing
Text Notes 625  5875 0    50   ~ 0
(adds time delay to power good signal leading edge)
Wire Wire Line
	4175 5250 4175 5000
Connection ~ 4175 5000
Wire Wire Line
	5975 5250 5975 4900
Connection ~ 5975 4900
Wire Wire Line
	8175 5250 8175 5000
Connection ~ 8175 5000
Wire Wire Line
	9975 5250 9975 4900
Connection ~ 9975 4900
Wire Notes Line
	3075 5525 7050 5525
Wire Notes Line
	3075 550  3075 5525
Wire Notes Line
	7050 550  7050 5525
Wire Notes Line
	3075 550  7050 550 
Wire Notes Line
	7125 5525 7125 550 
Wire Notes Line
	11125 5525 11125 550 
Wire Notes Line
	7125 5525 11125 5525
Wire Notes Line
	7125 550  11125 550 
Wire Wire Line
	1200 6800 1300 6800
Text GLabel 1200 6800 0    50   Input ~ 0
FMC_PG_C2M
Wire Wire Line
	1700 7100 1700 7300
Wire Wire Line
	1200 7300 1200 7200
Wire Wire Line
	1200 6900 1300 6900
$Comp
L PMSM_Driver_ZB_Split_Board_Device:C_Small C?
U 1 1 5E91940B
P 1200 7100
AR Path="/5E91940B" Ref="C?"  Part="1" 
AR Path="/5E893D08/5E91940B" Ref="C5"  Part="1" 
F 0 "C5" H 1075 7100 50  0000 C CNN
F 1 "100nF" H 1075 7175 50  0000 C CNN
F 2 "PMSM_Driver_ZB_Split_Board:C_0402_1005Metric" H 1200 7100 50  0001 C CNN
F 3 "~" H 1200 7100 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 1200 7100 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 1200 7100 50  0001 C CNN "Distributor Link"
	1    1200 7100
	-1   0    0    1   
$EndComp
Wire Wire Line
	1200 7000 1200 6900
Wire Wire Line
	2100 6800 2200 6800
Text GLabel 2200 6800 2    50   Output ~ 0
ZB_POWER_GOOD
Wire Wire Line
	1700 6400 1700 6500
Connection ~ 1700 6400
Wire Wire Line
	1200 6700 1300 6700
Wire Wire Line
	1200 6400 1200 6700
Wire Wire Line
	1700 6400 1200 6400
Wire Wire Line
	1700 6300 1700 6400
$Comp
L PMSM_Driver_ZB_Split_Board_Power:ZB_3V3_AUX #PWR?
U 1 1 5E89F263
P 1700 6300
AR Path="/5E89F263" Ref="#PWR?"  Part="1" 
AR Path="/5E893D08/5E89F263" Ref="#PWR0129"  Part="1" 
F 0 "#PWR0129" H 1700 6550 50  0001 C CNN
F 1 "ZB_3V3_AUX" H 1701 6473 50  0000 C CNN
F 2 "" H 1700 6300 50  0001 C CNN
F 3 "" H 1700 6300 50  0001 C CNN
	1    1700 6300
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_ZB_Split_Board_IC:ADM1086AKSZ U1
U 1 1 5E87C371
P 1700 6800
F 0 "U1" H 1400 7050 50  0000 L CNN
F 1 "ADM1086AKSZ" H 1750 6550 50  0000 L CNN
F 2 "PMSM_Driver_ZB_Split_Board:SC70-6" H 1800 6100 50  0001 C CNN
F 3 "~" H 1050 7550 50  0001 C CNN
F 4 "ADM1086AKSZ-REEL7 -  Sequencing Circuit, 2.25 V to 3.6 Vsupp., 600 mV threshold/0.5 µs delay, Active-High, SC-70-6" H 1700 6800 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/analog-devices/adm1086aksz-reel7/supervisor-0-6v-sc-70-6/dp/2727558" H 1700 6800 50  0001 C CNN "Distributor Link"
	1    1700 6800
	1    0    0    -1  
$EndComp
Wire Notes Line
	575  7675 3000 7675
Wire Notes Line
	575  5600 3000 5600
$Comp
L PMSM_Driver_ZB_Split_Board_Device:C_Small C?
U 1 1 5E861DEA
P 1875 6400
AR Path="/5E861DEA" Ref="C?"  Part="1" 
AR Path="/5E893D08/5E861DEA" Ref="C4"  Part="1" 
F 0 "C4" V 1975 6400 50  0000 C CNN
F 1 "100nF" V 1775 6400 50  0000 C CNN
F 2 "PMSM_Driver_ZB_Split_Board:C_0402_1005Metric" H 1875 6400 50  0001 C CNN
F 3 "~" H 1875 6400 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 1875 6400 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 1875 6400 50  0001 C CNN "Distributor Link"
	1    1875 6400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1700 6400 1775 6400
Wire Wire Line
	1975 6400 2225 6400
Wire Wire Line
	2225 6400 2225 6450
$Comp
L PMSM_Driver_ZB_Split_Board_Device:C_Small C?
U 1 1 5E8C3344
P 1150 3750
AR Path="/5E8C3344" Ref="C?"  Part="1" 
AR Path="/5E893D08/5E8C3344" Ref="C1"  Part="1" 
F 0 "C1" H 1035 3750 50  0000 C CNN
F 1 "47uF" H 1035 3820 50  0000 C CNN
F 2 "PMSM_Driver_ZB_Split_Board:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 1150 3750 50  0001 C CNN
F 3 "~" H 1150 3750 50  0001 C CNN
F 4 "GRM31CR60J476ME19L -  SMD Multilayer Ceramic Capacitor, 47 µF, 6.3 V, 1206 [3216 Metric], ± 20%, X5R, GRM Series" H 1150 3750 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/murata/grm31cr60j476me19l/cap-47-f-6-3v-20-x5r-1206/dp/1735534" H 1150 3750 50  0001 C CNN "Distributor Link"
F 6 "6.3V" H 1045 3890 50  0000 C CNN "Voltage"
	1    1150 3750
	-1   0    0    1   
$EndComp
$Comp
L PMSM_Driver_ZB_Split_Board_Device:C_Small C?
U 1 1 5E91666D
P 1500 3750
AR Path="/5E91666D" Ref="C?"  Part="1" 
AR Path="/5E893D08/5E91666D" Ref="C2"  Part="1" 
F 0 "C2" H 1375 3750 50  0000 C CNN
F 1 "100nF" H 1375 3825 50  0000 C CNN
F 2 "PMSM_Driver_ZB_Split_Board:C_0402_1005Metric" H 1500 3750 50  0001 C CNN
F 3 "~" H 1500 3750 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 1500 3750 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 1500 3750 50  0001 C CNN "Distributor Link"
	1    1500 3750
	-1   0    0    1   
$EndComp
$Comp
L PMSM_Driver_ZB_Split_Board_Device:C_Small C?
U 1 1 5E92B75E
P 1850 3750
AR Path="/5E92B75E" Ref="C?"  Part="1" 
AR Path="/5E893D08/5E92B75E" Ref="C3"  Part="1" 
F 0 "C3" H 1725 3750 50  0000 C CNN
F 1 "100nF" H 1725 3825 50  0000 C CNN
F 2 "PMSM_Driver_ZB_Split_Board:C_0402_1005Metric" H 1850 3750 50  0001 C CNN
F 3 "~" H 1850 3750 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series " H 1850 3750 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 1850 3750 50  0001 C CNN "Distributor Link"
	1    1850 3750
	-1   0    0    1   
$EndComp
$Comp
L PMSM_Driver_ZB_Split_Board_Power:ZB_3V3_AUX #PWR?
U 1 1 5EA06382
P 1500 3500
AR Path="/5EA06382" Ref="#PWR?"  Part="1" 
AR Path="/5E893D08/5EA06382" Ref="#PWR0115"  Part="1" 
F 0 "#PWR0115" H 1500 3750 50  0001 C CNN
F 1 "ZB_3V3_AUX" H 1501 3673 50  0000 C CNN
F 2 "" H 1500 3500 50  0001 C CNN
F 3 "" H 1500 3500 50  0001 C CNN
	1    1500 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	1150 3650 1150 3600
Wire Wire Line
	1500 3600 1500 3650
Wire Wire Line
	1850 3600 1850 3650
Connection ~ 1500 3600
Wire Wire Line
	1150 3600 1500 3600
Wire Wire Line
	1500 3600 1850 3600
Wire Notes Line
	3000 5600 3000 7675
Wire Notes Line
	575  5600 575  7675
Wire Wire Line
	1150 3850 1150 3950
Wire Wire Line
	1500 3850 1500 3950
Wire Wire Line
	1850 3850 1850 3950
Text Notes 625  3100 0    100  ~ 0
Bulk Capacitors
Wire Notes Line
	575  2925 2500 2925
Wire Notes Line
	575  2850 2500 2850
Wire Notes Line
	575  575  2500 575 
NoConn ~ 4325 4500
NoConn ~ 4325 4700
Wire Wire Line
	1500 3500 1500 3600
Wire Notes Line
	575  4325 2500 4325
Wire Notes Line
	6800 5600 6800 7675
Text Notes 5075 6850 0    25   ~ 0
Lock EEPROM jumper
Connection ~ 5025 6200
Text GLabel 6175 6900 2    50   BiDi ~ 0
ZB_FMC_SDA
Text GLabel 6175 7000 2    50   Input ~ 0
ZB_FMC_SCL
$Comp
L PMSM_Driver_ZB_Split_Board_Device:R_Small_US R?
U 1 1 5E8BCD49
P 5525 6400
AR Path="/5E8BCD49" Ref="R?"  Part="1" 
AR Path="/5E893D08/5E8BCD49" Ref="R4"  Part="1" 
F 0 "R4" H 5625 6400 50  0000 C CNN
F 1 "4k7" H 5625 6300 50  0000 C CNN
F 2 "PMSM_Driver_ZB_Split_Board:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 5525 6400 50  0001 C CNN
F 3 "~" H 5525 6400 50  0001 C CNN
F 4 "MC0603SAF4701T5E -  RES, THICK FILM, 4K7, 1%, 0.1W, 0603" H 5525 6400 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0603saf4701t5e/res-thick-film-4k7-1-0-1w-0603/dp/1632439" H 5525 6400 50  0001 C CNN "Distributor Link"
F 6 "DNI" H 5625 6225 50  0000 C CNN "DNI"
	1    5525 6400
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_ZB_Split_Board_Device:R_Small_US R?
U 1 1 5E8BCD50
P 5825 6400
AR Path="/5E8BCD50" Ref="R?"  Part="1" 
AR Path="/5E893D08/5E8BCD50" Ref="R5"  Part="1" 
F 0 "R5" H 5925 6400 50  0000 C CNN
F 1 "4k7" H 5925 6300 50  0000 C CNN
F 2 "PMSM_Driver_ZB_Split_Board:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 5825 6400 50  0001 C CNN
F 3 "~" H 5825 6400 50  0001 C CNN
F 4 "MC0603SAF4701T5E -  RES, THICK FILM, 4K7, 1%, 0.1W, 0603" H 5825 6400 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0603saf4701t5e/res-thick-film-4k7-1-0-1w-0603/dp/1632439" H 5825 6400 50  0001 C CNN "Distributor Link"
F 6 "DNI" H 5900 6225 50  0000 C CNN "DNI"
	1    5825 6400
	1    0    0    -1  
$EndComp
Wire Wire Line
	5025 6200 5525 6200
Wire Wire Line
	5525 6200 5825 6200
Connection ~ 5525 6200
Wire Wire Line
	5525 6300 5525 6200
Wire Wire Line
	5825 6300 5825 6200
Wire Wire Line
	4925 7000 5525 7000
Wire Wire Line
	5525 7000 6175 7000
Connection ~ 5525 7000
Wire Wire Line
	5525 6500 5525 7000
Wire Wire Line
	4925 6900 5825 6900
Wire Wire Line
	5825 6900 6175 6900
Connection ~ 5825 6900
Wire Wire Line
	5825 6500 5825 6900
Text GLabel 6075 3300 2    50   Output ~ 0
ZB_SPI_MOSI
Wire Notes Line
	575  2925 575  4325
Wire Notes Line
	2500 2925 2500 4325
Text GLabel 8075 1200 0    50   Output ~ 0
ZB_SPI_SEL_AUX1
Text GLabel 4075 3700 0    50   Output ~ 0
ZB_SPI_SEL_AUX2
Text GLabel 10075 2100 2    50   Input ~ 0
ZB_M1_CUR_T
Text GLabel 6075 2100 2    50   Input ~ 0
ZB_M1_CUR_B
Wire Wire Line
	6075 3400 5825 3400
Wire Wire Line
	4325 3600 4075 3600
Text GLabel 10075 2900 2    50   Output ~ 0
ZB_M2_RST_OVR_CUR
Text GLabel 10075 4800 2    50   Input ~ 0
ZB_M2_V_SUP
Wire Wire Line
	10075 4800 9825 4800
Wire Wire Line
	9825 3400 9975 3400
Wire Wire Line
	9825 3100 9975 3100
Wire Wire Line
	9825 2800 9975 2800
Wire Wire Line
	9825 2500 9975 2500
Wire Wire Line
	9825 2200 9975 2200
Wire Wire Line
	9825 1900 9975 1900
Text GLabel 10075 3800 2    50   Input ~ 0
ZB_M2_HAL_B
Wire Wire Line
	9825 1800 10075 1800
Wire Wire Line
	9825 2000 10075 2000
Wire Wire Line
	9825 2100 10075 2100
Wire Wire Line
	9825 3800 10075 3800
Wire Wire Line
	9825 1400 10075 1400
Wire Wire Line
	9825 1500 10075 1500
Wire Wire Line
	9825 1700 10075 1700
$Comp
L PMSM_Driver_ZB_Split_Board_Connector:Connector_ASP-134604-01 J1
U 2 1 5E8745EA
P 9075 3000
F 0 "J1" H 8575 5100 50  0000 C CNN
F 1 "Connector_ASP-134604-01" H 9025 800 50  0000 C CNN
F 2 "PMSM_Driver_ZB_Split_Board:Samtec_FMC_ASP-134604-01_4x40_Vertical" H 9175 700 50  0001 C CNN
F 3 "~" H 8575 5100 50  0001 C CNN
F 4 "ASP-134604-01 -  Board-To-Board Connector, Vita 57 XMC, 1.27 mm, 160 Contacts, Header, ASP Series, Surface Mount" H 9075 3000 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/samtec/asp-134604-01/conn-vita-57-fmc-header-160pos/dp/2433507" H 9075 3000 50  0001 C CNN "Distributor Link"
	2    9075 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	3825 6900 4125 6900
$Comp
L PMSM_Driver_ZB_Split_Board_Device:R_Small_US R?
U 1 1 5E8BCD1C
P 3725 6900
AR Path="/5E8BCD1C" Ref="R?"  Part="1" 
AR Path="/5E893D08/5E8BCD1C" Ref="R1"  Part="1" 
F 0 "R1" V 3800 6900 50  0000 C CNN
F 1 "1k" V 3625 6900 50  0000 C CNN
F 2 "PMSM_Driver_ZB_Split_Board:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 3725 6900 50  0001 C CNN
F 3 "~" H 3725 6900 50  0001 C CNN
F 4 "ERJ3EKF1001V -  SMD Chip Resistor, 0603 [1608 Metric], 1 kohm, ERJ3EK Series, 75 V, Thick Film, 100 mW" H 3725 6900 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/panasonic/erj3ekf1001v/res-1k-1-0-1w-0603-thick-film/dp/2303145?st=ERJ-3EKF1001V" H 3725 6900 50  0001 C CNN "Distributor Link"
	1    3725 6900
	0    -1   -1   0   
$EndComp
Text GLabel 8075 4600 0    50   Input ~ 0
ZB_M2_V_C
Text GLabel 10075 4500 2    50   Input ~ 0
ZB_M2_CUR_A
Text GLabel 8075 4400 0    50   Input ~ 0
ZB_M2_CUR_B
Text GLabel 10075 4400 2    50   Input ~ 0
ZB_M2_CUR_T
Text GLabel 10075 3500 2    50   Output ~ 0
ZB_M2_EN
Wire Wire Line
	9825 5000 10825 5000
Wire Wire Line
	10075 4400 9825 4400
Text GLabel 8075 4300 0    50   Input ~ 0
ZB_M2_ENC_A
Wire Wire Line
	8325 4300 8075 4300
Text GLabel 10075 4200 2    50   Input ~ 0
ZB_M2_ENC_B
Wire Wire Line
	9825 4200 10075 4200
Text GLabel 8075 4100 0    50   Input ~ 0
ZB_M2_ENC_I
Wire Wire Line
	8325 4100 8075 4100
Text GLabel 10075 4100 2    50   Input ~ 0
ZB_M2_RES_A
Wire Wire Line
	9825 4100 10075 4100
Wire Wire Line
	8075 4700 8325 4700
Text GLabel 8075 4700 0    50   Input ~ 0
ZB_M2_V_A
Wire Wire Line
	10075 4700 9825 4700
Text GLabel 10075 4700 2    50   Input ~ 0
ZB_M2_V_B
Wire Wire Line
	8325 4000 8075 4000
Text GLabel 8075 4000 0    50   Input ~ 0
ZB_M2_RES_B
Wire Wire Line
	9825 3900 10075 3900
Text GLabel 10075 3900 2    50   Input ~ 0
ZB_M2_RES_I
Text GLabel 8075 3800 0    50   Input ~ 0
ZB_M2_HAL_A
Wire Wire Line
	8325 3800 8075 3800
Wire Wire Line
	8325 3700 8075 3700
Text GLabel 8075 3700 0    50   Input ~ 0
ZB_M2_HAL_C
Text GLabel 6075 3700 2    50   Input ~ 0
ZB_M2_TEMP
Wire Wire Line
	5825 3700 6075 3700
Wire Wire Line
	9825 3500 10075 3500
NoConn ~ 8325 3400
Wire Wire Line
	9825 3000 10075 3000
Text GLabel 10075 2700 2    50   Output ~ 0
ZB_M1_PWM_AH
Wire Wire Line
	10075 2700 9825 2700
Wire Wire Line
	9825 2900 10075 2900
Wire Wire Line
	8075 1300 8325 1300
Text GLabel 8075 1600 0    50   Input ~ 0
ZB_M1_HAL_C
Text GLabel 8075 2900 0    50   Input ~ 0
ZB_M2_NOT_OVR_CUR
Wire Wire Line
	8075 2900 8325 2900
Wire Wire Line
	8075 2500 8325 2500
Text GLabel 8075 2500 0    50   Output ~ 0
ZB_M1_PWM_CL
Wire Wire Line
	4325 2900 4075 2900
Text GLabel 4075 2900 0    50   Output ~ 0
ZB_M1_EN
Text GLabel 8075 2600 0    50   Output ~ 0
ZB_M1_PWM_CH
Wire Wire Line
	8075 2600 8325 2600
Text GLabel 8075 3200 0    50   Output ~ 0
ZB_M2_PWM_AL
Wire Wire Line
	8075 3200 8325 3200
NoConn ~ 5825 2700
NoConn ~ 5825 2400
NoConn ~ 5825 2500
NoConn ~ 8325 2200
Text GLabel 10075 2000 2    50   Input ~ 0
ZB_M1_ENC_B
Wire Wire Line
	4075 2000 4325 2000
Text GLabel 4075 2000 0    50   Input ~ 0
ZB_M1_ENC_A
Wire Wire Line
	7400 4800 7400 4900
Wire Wire Line
	7400 4900 8325 4900
NoConn ~ 8325 1900
Wire Notes Line
	575  5525 575  4400
Wire Notes Line
	575  4400 2500 4400
Wire Notes Line
	2500 4400 2500 5525
Wire Notes Line
	2500 5525 575  5525
Text Notes 625  4600 0    100  ~ 0
Mounting Holes
$Comp
L PMSM_Driver_ZB_Split_Board_Mechanical:MountingHole H1
U 1 1 5E9260A7
P 1000 5125
F 0 "H1" H 947 4996 50  0000 L CNN
F 1 "MountingHoleM2.5" H 658 4924 50  0000 L CNN
F 2 "PMSM_Driver_ZB_Split_Board:MountingHole_2.7mm_M2.5_DIN965" H 1000 5125 50  0001 C CNN
F 3 "~" H 1000 5125 50  0001 C CNN
	1    1000 5125
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_ZB_Split_Board_Mechanical:MountingHole H2
U 1 1 5E926CD8
P 2000 5125
F 0 "H2" H 1947 4995 50  0000 L CNN
F 1 "MountingHoleM2.5" H 1658 4925 50  0000 L CNN
F 2 "PMSM_Driver_ZB_Split_Board:MountingHole_2.7mm_M2.5_DIN965" H 2000 5125 50  0001 C CNN
F 3 "~" H 2000 5125 50  0001 C CNN
	1    2000 5125
	1    0    0    -1  
$EndComp
Text Notes 625  4800 0    50   ~ 0
(FMC connector footprint contains two more\nM2.5 mounting holes)
Text GLabel 8075 3100 0    50   Output ~ 0
ZB_M2_PWM_BL
Wire Wire Line
	8075 3100 8325 3100
NoConn ~ 8325 2800
Wire Wire Line
	5825 3100 6075 3100
Text GLabel 6075 3100 2    50   Output ~ 0
ZB_SPI_SCK
NoConn ~ 8325 3500
NoConn ~ 5825 3600
Text Notes 6025 6700 0    25   ~ 0
I2C pull-up resistors are\npresent on Zedboard.\nThus, these are not\nnecessary here. Place\nthem if you use this\nboard with a carrier \nboard without the\npull-ups.
$Comp
L PMSM_Driver_ZB_Split_Board_Power:ZB_GND #PWR?
U 1 1 5EBBAD15
P 1150 3950
AR Path="/5EBBAD15" Ref="#PWR?"  Part="1" 
AR Path="/5E893D08/5EBBAD15" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 1150 3700 50  0001 C CNN
F 1 "ZB_GND" H 1155 3777 50  0000 C CNN
F 2 "" H 1150 3950 50  0001 C CNN
F 3 "" H 1150 3950 50  0001 C CNN
	1    1150 3950
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_ZB_Split_Board_Power:ZB_GND #PWR?
U 1 1 5EBCE5B0
P 1500 3950
AR Path="/5EBCE5B0" Ref="#PWR?"  Part="1" 
AR Path="/5E893D08/5EBCE5B0" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 1500 3700 50  0001 C CNN
F 1 "ZB_GND" H 1505 3777 50  0000 C CNN
F 2 "" H 1500 3950 50  0001 C CNN
F 3 "" H 1500 3950 50  0001 C CNN
	1    1500 3950
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_ZB_Split_Board_Power:ZB_GND #PWR?
U 1 1 5EBE1E4C
P 1850 3950
AR Path="/5EBE1E4C" Ref="#PWR?"  Part="1" 
AR Path="/5E893D08/5EBE1E4C" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 1850 3700 50  0001 C CNN
F 1 "ZB_GND" H 1855 3777 50  0000 C CNN
F 2 "" H 1850 3950 50  0001 C CNN
F 3 "" H 1850 3950 50  0001 C CNN
	1    1850 3950
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_ZB_Split_Board_Power:ZB_GND #PWR?
U 1 1 5EC08AF0
P 2225 6450
AR Path="/5EC08AF0" Ref="#PWR?"  Part="1" 
AR Path="/5E893D08/5EC08AF0" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 2225 6200 50  0001 C CNN
F 1 "ZB_GND" H 2230 6277 50  0000 C CNN
F 2 "" H 2225 6450 50  0001 C CNN
F 3 "" H 2225 6450 50  0001 C CNN
	1    2225 6450
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_ZB_Split_Board_Power:ZB_GND #PWR?
U 1 1 5EC2F8D7
P 1200 7300
AR Path="/5EC2F8D7" Ref="#PWR?"  Part="1" 
AR Path="/5E893D08/5EC2F8D7" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 1200 7050 50  0001 C CNN
F 1 "ZB_GND" H 1205 7127 50  0000 C CNN
F 2 "" H 1200 7300 50  0001 C CNN
F 3 "" H 1200 7300 50  0001 C CNN
	1    1200 7300
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_ZB_Split_Board_Power:ZB_GND #PWR?
U 1 1 5EC4300A
P 1700 7300
AR Path="/5EC4300A" Ref="#PWR?"  Part="1" 
AR Path="/5E893D08/5EC4300A" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 1700 7050 50  0001 C CNN
F 1 "ZB_GND" H 1705 7127 50  0000 C CNN
F 2 "" H 1700 7300 50  0001 C CNN
F 3 "" H 1700 7300 50  0001 C CNN
	1    1700 7300
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_ZB_Split_Board_Power:ZB_GND #PWR?
U 1 1 5EC69F2A
P 4025 7400
AR Path="/5EC69F2A" Ref="#PWR?"  Part="1" 
AR Path="/5E893D08/5EC69F2A" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 4025 7150 50  0001 C CNN
F 1 "ZB_GND" H 4030 7227 50  0000 C CNN
F 2 "" H 4025 7400 50  0001 C CNN
F 3 "" H 4025 7400 50  0001 C CNN
	1    4025 7400
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_ZB_Split_Board_Power:ZB_GND #PWR?
U 1 1 5EC91571
P 4525 7400
AR Path="/5EC91571" Ref="#PWR?"  Part="1" 
AR Path="/5E893D08/5EC91571" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 4525 7150 50  0001 C CNN
F 1 "ZB_GND" H 4530 7227 50  0000 C CNN
F 2 "" H 4525 7400 50  0001 C CNN
F 3 "" H 4525 7400 50  0001 C CNN
	1    4525 7400
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_ZB_Split_Board_Power:ZB_GND #PWR?
U 1 1 5ECB82F8
P 4125 6300
AR Path="/5ECB82F8" Ref="#PWR?"  Part="1" 
AR Path="/5E893D08/5ECB82F8" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 4125 6050 50  0001 C CNN
F 1 "ZB_GND" H 4130 6127 50  0000 C CNN
F 2 "" H 4125 6300 50  0001 C CNN
F 3 "" H 4125 6300 50  0001 C CNN
	1    4125 6300
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_ZB_Split_Board_Power:ZB_GND #PWR?
U 1 1 5ECF2FA3
P 4175 5250
AR Path="/5ECF2FA3" Ref="#PWR?"  Part="1" 
AR Path="/5E893D08/5ECF2FA3" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 4175 5000 50  0001 C CNN
F 1 "ZB_GND" H 4180 5077 50  0000 C CNN
F 2 "" H 4175 5250 50  0001 C CNN
F 3 "" H 4175 5250 50  0001 C CNN
	1    4175 5250
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_ZB_Split_Board_Power:ZB_GND #PWR?
U 1 1 5ED19E68
P 5975 5250
AR Path="/5ED19E68" Ref="#PWR?"  Part="1" 
AR Path="/5E893D08/5ED19E68" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5975 5000 50  0001 C CNN
F 1 "ZB_GND" H 5980 5077 50  0000 C CNN
F 2 "" H 5975 5250 50  0001 C CNN
F 3 "" H 5975 5250 50  0001 C CNN
	1    5975 5250
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_ZB_Split_Board_Power:ZB_GND #PWR?
U 1 1 5ED5426A
P 9975 5250
AR Path="/5ED5426A" Ref="#PWR?"  Part="1" 
AR Path="/5E893D08/5ED5426A" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 9975 5000 50  0001 C CNN
F 1 "ZB_GND" H 9980 5077 50  0000 C CNN
F 2 "" H 9975 5250 50  0001 C CNN
F 3 "" H 9975 5250 50  0001 C CNN
	1    9975 5250
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_ZB_Split_Board_Power:ZB_GND #PWR?
U 1 1 5ED67B5B
P 8175 5250
AR Path="/5ED67B5B" Ref="#PWR?"  Part="1" 
AR Path="/5E893D08/5ED67B5B" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 8175 5000 50  0001 C CNN
F 1 "ZB_GND" H 8180 5077 50  0000 C CNN
F 2 "" H 8175 5250 50  0001 C CNN
F 3 "" H 8175 5250 50  0001 C CNN
	1    8175 5250
	1    0    0    -1  
$EndComp
Wire Notes Line
	3075 5600 6800 5600
Wire Notes Line
	3075 7675 6800 7675
$EndSCHEMATC
