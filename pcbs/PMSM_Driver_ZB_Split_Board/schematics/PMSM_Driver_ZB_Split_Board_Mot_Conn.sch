EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 3
Title "PMSM_Driver_ZB_Split_Board"
Date "2020-03-30"
Rev "A"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L PMSM_Driver_ZB_Split_Board_Connector:Connector_MC-254-50-00-ST-DIP J3
U 1 1 5EF2D0A6
P 2250 5050
F 0 "J3" H 2200 6350 50  0000 C CNN
F 1 "Connector_MC-254-50-00-ST-DIP" H 1800 3750 50  0000 C CNN
F 2 "PMSM_Driver_ZB_Split_Board:IDC-Header_2x25_P2.54mm_Vertical" H 2200 5050 50  0001 C CNN
F 3 "~" H 2200 5050 50  0001 C CNN
F 4 "MC-254-50-00-ST-DIP -  Wire-To-Board Connector, 2.54 mm, 50 Contacts, Header, Through Hole, 2 Rows" H 2250 5050 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc-254-50-00-st-dip/connector-header-50pos-2row-2/dp/2843537" H 2250 5050 50  0001 C CNN "Distributor Link"
	1    2250 5050
	1    0    0    -1  
$EndComp
Wire Wire Line
	2500 6250 2600 6250
Wire Wire Line
	2600 6250 2600 6350
Wire Wire Line
	2500 6150 2600 6150
Wire Wire Line
	2600 6150 2600 6250
Connection ~ 2600 6250
Wire Wire Line
	2500 6050 2600 6050
Wire Wire Line
	2600 6050 2600 6150
Connection ~ 2600 6150
Wire Wire Line
	2500 5950 2600 5950
Wire Wire Line
	2600 5950 2600 6050
Connection ~ 2600 6050
Wire Wire Line
	2500 5850 2600 5850
Wire Wire Line
	2600 5850 2600 5950
Connection ~ 2600 5950
Wire Wire Line
	2500 5750 2600 5750
Wire Wire Line
	2600 5750 2600 5850
Connection ~ 2600 5850
Wire Wire Line
	2500 5650 2600 5650
Wire Wire Line
	2600 5650 2600 5750
Connection ~ 2600 5750
Connection ~ 2600 5650
Wire Wire Line
	2500 5450 2600 5450
Wire Wire Line
	2500 5350 2600 5350
Wire Wire Line
	2600 5350 2600 5450
Connection ~ 2600 5450
Wire Wire Line
	2500 5250 2600 5250
Wire Wire Line
	2600 5250 2600 5350
Connection ~ 2600 5350
Wire Wire Line
	2500 5150 2600 5150
Wire Wire Line
	2600 5150 2600 5250
Connection ~ 2600 5250
Wire Wire Line
	2500 5050 2600 5050
Wire Wire Line
	2600 5050 2600 5150
Connection ~ 2600 5150
Wire Wire Line
	2500 4950 2600 4950
Wire Wire Line
	2600 4950 2600 5050
Connection ~ 2600 5050
Wire Wire Line
	2500 4850 2600 4850
Wire Wire Line
	2600 4850 2600 4950
Connection ~ 2600 4950
Connection ~ 2600 4850
Wire Wire Line
	2500 4650 2600 4650
Wire Wire Line
	2500 4550 2600 4550
Wire Wire Line
	2600 4550 2600 4650
Connection ~ 2600 4650
Wire Wire Line
	2500 4450 2600 4450
Wire Wire Line
	2600 4450 2600 4550
Connection ~ 2600 4550
Wire Wire Line
	2500 4350 2600 4350
Wire Wire Line
	2600 4350 2600 4450
Connection ~ 2600 4450
Wire Wire Line
	2500 4250 2600 4250
Wire Wire Line
	2600 4250 2600 4350
Connection ~ 2600 4350
Wire Wire Line
	2500 4150 2600 4150
Wire Wire Line
	2600 4150 2600 4250
Connection ~ 2600 4250
Wire Wire Line
	2500 4050 2600 4050
Wire Wire Line
	2600 4050 2600 4150
Connection ~ 2600 4150
Wire Wire Line
	2500 3950 2600 3950
Wire Wire Line
	2600 3950 2600 4050
Connection ~ 2600 4050
Wire Wire Line
	2500 3850 2600 3850
Wire Wire Line
	2600 3850 2600 3950
Connection ~ 2600 3950
Wire Wire Line
	2500 2925 2600 2925
Wire Wire Line
	2600 2925 2600 3025
Wire Wire Line
	2500 2825 2600 2825
Wire Wire Line
	2600 2825 2600 2925
Connection ~ 2600 2925
Wire Wire Line
	2500 2725 2600 2725
Wire Wire Line
	2600 2725 2600 2825
Connection ~ 2600 2825
Wire Wire Line
	2500 2625 2600 2625
Wire Wire Line
	2600 2625 2600 2725
Connection ~ 2600 2725
Wire Wire Line
	2500 2525 2600 2525
Wire Wire Line
	2600 2525 2600 2625
Connection ~ 2600 2625
Wire Wire Line
	2500 2425 2600 2425
Wire Wire Line
	2600 2425 2600 2525
Connection ~ 2600 2525
Wire Wire Line
	2500 2325 2600 2325
Wire Wire Line
	2600 2325 2600 2425
Connection ~ 2600 2425
Connection ~ 2600 2325
Wire Wire Line
	2500 2125 2600 2125
Wire Wire Line
	2500 2025 2600 2025
Wire Wire Line
	2600 2025 2600 2125
Connection ~ 2600 2125
Wire Wire Line
	2500 1925 2600 1925
Wire Wire Line
	2600 1925 2600 2025
Connection ~ 2600 2025
Wire Wire Line
	2500 1825 2600 1825
Wire Wire Line
	2600 1825 2600 1925
Connection ~ 2600 1925
Wire Wire Line
	2500 1725 2600 1725
Wire Wire Line
	2600 1725 2600 1825
Connection ~ 2600 1825
Wire Wire Line
	2500 1625 2600 1625
Wire Wire Line
	2600 1625 2600 1725
Connection ~ 2600 1725
Wire Wire Line
	2500 1525 2600 1525
Wire Wire Line
	2600 1525 2600 1625
Connection ~ 2600 1625
Connection ~ 2600 1525
Wire Wire Line
	2500 1325 2600 1325
Wire Wire Line
	2500 1225 2600 1225
Wire Wire Line
	2600 1225 2600 1325
Connection ~ 2600 1325
Wire Wire Line
	2500 1125 2600 1125
Wire Wire Line
	2600 1125 2600 1225
Connection ~ 2600 1225
Wire Wire Line
	2500 1025 2600 1025
Wire Wire Line
	2600 1025 2600 1125
Connection ~ 2600 1125
Wire Wire Line
	2600 4650 2600 4750
Wire Wire Line
	2500 4750 2600 4750
Connection ~ 2600 4750
Wire Wire Line
	2600 4750 2600 4850
Wire Wire Line
	2600 5450 2600 5550
Wire Wire Line
	2500 5550 2600 5550
Connection ~ 2600 5550
Wire Wire Line
	2600 5550 2600 5650
Wire Wire Line
	2600 1325 2600 1425
Wire Wire Line
	2500 1425 2600 1425
Connection ~ 2600 1425
Wire Wire Line
	2600 1425 2600 1525
Wire Wire Line
	2600 2125 2600 2225
Wire Wire Line
	2500 2225 2600 2225
Connection ~ 2600 2225
Wire Wire Line
	2600 2225 2600 2325
Text GLabel 1900 3950 0    50   Output ~ 0
ZB_M1_V_A
Text GLabel 1900 4050 0    50   Output ~ 0
ZB_M1_V_B
Text GLabel 1900 4150 0    50   Output ~ 0
ZB_M1_V_C
Text GLabel 1900 4250 0    50   Output ~ 0
ZB_M1_CUR_A
Text GLabel 1900 4350 0    50   Output ~ 0
ZB_M1_CUR_B
Text GLabel 1900 4450 0    50   Output ~ 0
ZB_M1_CUR_T
Text GLabel 1900 4750 0    50   Output ~ 0
ZB_M1_ENC_I
Text GLabel 1900 4850 0    50   Output ~ 0
ZB_M1_RES_A
Text GLabel 1900 4950 0    50   Output ~ 0
ZB_M1_RES_B
Text GLabel 1900 5150 0    50   Output ~ 0
ZB_M1_HAL_A
Text GLabel 1900 5250 0    50   Output ~ 0
ZB_M1_HAL_B
Text GLabel 1900 5350 0    50   Output ~ 0
ZB_M1_HAL_C
Text GLabel 1900 5450 0    50   Output ~ 0
ZB_M1_TEMP
Text GLabel 1900 5050 0    50   Output ~ 0
ZB_M1_RES_I
Wire Wire Line
	1900 3850 2000 3850
Wire Wire Line
	1900 3950 2000 3950
Wire Wire Line
	1900 4050 2000 4050
Wire Wire Line
	1900 4150 2000 4150
Wire Wire Line
	1900 4250 2000 4250
Wire Wire Line
	1900 4350 2000 4350
Wire Wire Line
	1900 4550 2000 4550
Wire Wire Line
	1900 4650 2000 4650
Wire Wire Line
	1900 4750 2000 4750
Wire Wire Line
	1900 4850 2000 4850
Wire Wire Line
	1900 4950 2000 4950
Wire Wire Line
	1900 5050 2000 5050
Wire Wire Line
	1900 5150 2000 5150
Wire Wire Line
	1900 5250 2000 5250
Wire Wire Line
	1900 5350 2000 5350
Wire Wire Line
	1900 5450 2000 5450
Wire Wire Line
	1900 5550 2000 5550
Wire Wire Line
	1900 5650 2000 5650
Wire Wire Line
	1900 4450 2000 4450
Text GLabel 1900 5650 0    50   Input ~ 0
ZB_SPI_SEL_RDC1
Text GLabel 1900 5550 0    50   Input ~ 0
ZB_M1_RDC_SAMPLE_N
Text GLabel 1900 5850 0    50   Input ~ 0
ZB_SPI_SCK
Wire Wire Line
	1900 5850 2000 5850
Text GLabel 1900 4650 0    50   Output ~ 0
ZB_M1_ENC_B
Text GLabel 1900 4550 0    50   Output ~ 0
ZB_M1_ENC_A
Text GLabel 1900 3850 0    50   Output ~ 0
ZB_M1_V_SUP
Text GLabel 1900 1025 0    50   Input ~ 0
ZB_ADC_CLK
Text GLabel 1900 1225 0    50   Input ~ 0
ZB_VT_ENABLE
Text GLabel 1900 1425 0    50   Input ~ 0
ZB_M1_PWM_AH
Text GLabel 1900 1525 0    50   Input ~ 0
ZB_M1_PWM_AL
Text GLabel 1900 1625 0    50   Input ~ 0
ZB_M1_PWM_BH
Text GLabel 1900 1725 0    50   Input ~ 0
ZB_M1_PWM_BL
Text GLabel 1900 1825 0    50   Input ~ 0
ZB_M1_PWM_CH
Text GLabel 1900 1925 0    50   Input ~ 0
ZB_M1_PWM_CL
Text GLabel 1900 1325 0    50   Input ~ 0
ZB_M1_EN
Text GLabel 1900 2025 0    50   Input ~ 0
ZB_M1_RST_OVR_CUR
Wire Wire Line
	1900 1025 2000 1025
Wire Wire Line
	1900 1125 2000 1125
Wire Wire Line
	1900 1225 2000 1225
Wire Wire Line
	1900 1325 2000 1325
Wire Wire Line
	1900 1425 2000 1425
Wire Wire Line
	1900 1525 2000 1525
Wire Wire Line
	1900 1625 2000 1625
Wire Wire Line
	1900 1725 2000 1725
Wire Wire Line
	1900 1825 2000 1825
Wire Wire Line
	1900 1925 2000 1925
Wire Wire Line
	1900 2025 2000 2025
Text GLabel 1900 1125 0    50   Input ~ 0
ZB_POWER_GOOD
Wire Wire Line
	1900 2125 2000 2125
Text GLabel 1900 2125 0    50   Output ~ 0
ZB_M1_NOT_OVR_CUR
Text GLabel 1900 6050 0    50   Input ~ 0
ZB_SPI_MOSI
Text GLabel 1900 5950 0    50   Output ~ 0
ZB_SPI_MISO
Wire Wire Line
	1900 5950 2000 5950
Wire Wire Line
	1900 6050 2000 6050
$Comp
L PMSM_Driver_ZB_Split_Board_Power:ZB_3V3 #PWR?
U 1 1 5ED2B815
P 800 2150
AR Path="/5ED2B815" Ref="#PWR?"  Part="1" 
AR Path="/5E8CABBA/5ED2B815" Ref="#PWR0121"  Part="1" 
F 0 "#PWR0121" H 800 2400 50  0001 C CNN
F 1 "ZB_3V3" H 801 2323 50  0000 C CNN
F 2 "" H 800 2150 50  0001 C CNN
F 3 "" H 800 2150 50  0001 C CNN
	1    800  2150
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2000 2225 1900 2225
Wire Wire Line
	800  2150 800  2225
Wire Wire Line
	2000 2325 1900 2325
Wire Wire Line
	1900 2325 1900 2225
Connection ~ 1900 2225
Wire Wire Line
	1900 2225 800  2225
Wire Wire Line
	1900 2325 1900 2425
Wire Wire Line
	1900 2425 2000 2425
Connection ~ 1900 2325
Wire Wire Line
	1900 2425 1900 2525
Wire Wire Line
	1900 2525 2000 2525
Connection ~ 1900 2425
$Comp
L PMSM_Driver_ZB_Split_Board_Power:ZB_VADJ #PWR?
U 1 1 5EE2A57E
P 825 2500
AR Path="/5EE2A57E" Ref="#PWR?"  Part="1" 
AR Path="/5E8CABBA/5EE2A57E" Ref="#PWR0122"  Part="1" 
F 0 "#PWR0122" H 825 2750 50  0001 C CNN
F 1 "ZB_VADJ" H 826 2673 50  0000 C CNN
F 2 "" H 825 2500 50  0001 C CNN
F 3 "" H 825 2500 50  0001 C CNN
	1    825  2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	2000 2625 1900 2625
Wire Wire Line
	825  2500 825  2625
Wire Wire Line
	2000 2725 1900 2725
Wire Wire Line
	1900 2725 1900 2625
Connection ~ 1900 2625
Wire Wire Line
	1900 2625 825  2625
Wire Wire Line
	2000 2825 1900 2825
Wire Wire Line
	1900 2825 1900 2725
Connection ~ 1900 2725
Wire Wire Line
	2000 2925 1900 2925
Wire Wire Line
	1900 2925 1900 2825
Connection ~ 1900 2825
Text GLabel 1900 5750 0    50   Input ~ 0
ZB_SPI_SEL_AUX1
Wire Wire Line
	1900 5750 2000 5750
Text Notes 650  800  0    100  ~ 0
Motor 1 Control Connector
Wire Notes Line
	600  3350 2875 3350
Wire Notes Line
	2875 3350 2875 600 
Wire Notes Line
	600  600  600  3350
Wire Notes Line
	600  600  2875 600 
Text Notes 650  3625 0    100  ~ 0
Motor 1 Sensor Connector
Wire Notes Line
	600  3425 2875 3425
Wire Notes Line
	600  6675 2875 6675
Wire Notes Line
	600  3425 600  6675
Wire Notes Line
	2875 3425 2875 6675
$Comp
L PMSM_Driver_ZB_Split_Board_Connector:Connector_MC-254-40-00-ST-DIP J4
U 1 1 5F2FECDF
P 4600 1925
F 0 "J4" H 4550 2925 50  0000 C CNN
F 1 "Connector_MC-254-40-00-ST-DIP" H 4125 825 50  0000 C CNN
F 2 "PMSM_Driver_ZB_Split_Board:IDC-Header_2x20_P2.54mm_Vertical" H 4550 1925 50  0001 C CNN
F 3 "~" H 4550 1925 50  0001 C CNN
F 4 "MC-254-40-00-ST-DIP -  Wire-To-Board Connector, 2.54 mm, 40 Contacts, Header, Through Hole, 2 Rows" H 4600 1925 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc-254-40-00-st-dip/connector-header-40pos-2row-2/dp/2843536" H 4600 1925 50  0001 C CNN "Distributor Link"
	1    4600 1925
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_ZB_Split_Board_Connector:Connector_MC-254-50-00-ST-DIP J5
U 1 1 5F2FECE5
P 4600 5050
F 0 "J5" H 4550 6350 50  0000 C CNN
F 1 "Connector_MC-254-50-00-ST-DIP" H 4150 3750 50  0000 C CNN
F 2 "PMSM_Driver_ZB_Split_Board:IDC-Header_2x25_P2.54mm_Vertical" H 4550 5050 50  0001 C CNN
F 3 "~" H 4550 5050 50  0001 C CNN
F 4 "MC-254-50-00-ST-DIP -  Wire-To-Board Connector, 2.54 mm, 50 Contacts, Header, Through Hole, 2 Rows" H 4600 5050 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc-254-50-00-st-dip/connector-header-50pos-2row-2/dp/2843537" H 4600 5050 50  0001 C CNN "Distributor Link"
	1    4600 5050
	1    0    0    -1  
$EndComp
Wire Wire Line
	4850 6250 4950 6250
Wire Wire Line
	4950 6250 4950 6350
Wire Wire Line
	4850 6150 4950 6150
Wire Wire Line
	4950 6150 4950 6250
Connection ~ 4950 6250
Wire Wire Line
	4850 6050 4950 6050
Wire Wire Line
	4950 6050 4950 6150
Connection ~ 4950 6150
Wire Wire Line
	4850 5950 4950 5950
Wire Wire Line
	4950 5950 4950 6050
Connection ~ 4950 6050
Wire Wire Line
	4850 5850 4950 5850
Wire Wire Line
	4950 5850 4950 5950
Connection ~ 4950 5950
Wire Wire Line
	4850 5750 4950 5750
Wire Wire Line
	4950 5750 4950 5850
Connection ~ 4950 5850
Wire Wire Line
	4850 5650 4950 5650
Wire Wire Line
	4950 5650 4950 5750
Connection ~ 4950 5750
Connection ~ 4950 5650
Wire Wire Line
	4850 5450 4950 5450
Wire Wire Line
	4850 5350 4950 5350
Wire Wire Line
	4950 5350 4950 5450
Connection ~ 4950 5450
Wire Wire Line
	4850 5250 4950 5250
Wire Wire Line
	4950 5250 4950 5350
Connection ~ 4950 5350
Wire Wire Line
	4850 5150 4950 5150
Wire Wire Line
	4950 5150 4950 5250
Connection ~ 4950 5250
Wire Wire Line
	4850 5050 4950 5050
Wire Wire Line
	4950 5050 4950 5150
Connection ~ 4950 5150
Wire Wire Line
	4850 4950 4950 4950
Wire Wire Line
	4950 4950 4950 5050
Connection ~ 4950 5050
Wire Wire Line
	4850 4850 4950 4850
Wire Wire Line
	4950 4850 4950 4950
Connection ~ 4950 4950
Connection ~ 4950 4850
Wire Wire Line
	4850 4650 4950 4650
Wire Wire Line
	4850 4550 4950 4550
Wire Wire Line
	4950 4550 4950 4650
Connection ~ 4950 4650
Wire Wire Line
	4850 4450 4950 4450
Wire Wire Line
	4950 4450 4950 4550
Connection ~ 4950 4550
Wire Wire Line
	4850 4350 4950 4350
Wire Wire Line
	4950 4350 4950 4450
Connection ~ 4950 4450
Wire Wire Line
	4850 4250 4950 4250
Wire Wire Line
	4950 4250 4950 4350
Connection ~ 4950 4350
Wire Wire Line
	4850 4150 4950 4150
Wire Wire Line
	4950 4150 4950 4250
Connection ~ 4950 4250
Wire Wire Line
	4850 4050 4950 4050
Wire Wire Line
	4950 4050 4950 4150
Connection ~ 4950 4150
Wire Wire Line
	4850 3950 4950 3950
Wire Wire Line
	4950 3950 4950 4050
Connection ~ 4950 4050
Wire Wire Line
	4850 3850 4950 3850
Wire Wire Line
	4950 3850 4950 3950
Connection ~ 4950 3950
Wire Wire Line
	4850 2925 4950 2925
Wire Wire Line
	4950 2925 4950 3025
Wire Wire Line
	4850 2825 4950 2825
Wire Wire Line
	4950 2825 4950 2925
Connection ~ 4950 2925
Wire Wire Line
	4850 2725 4950 2725
Wire Wire Line
	4950 2725 4950 2825
Connection ~ 4950 2825
Wire Wire Line
	4850 2625 4950 2625
Wire Wire Line
	4950 2625 4950 2725
Connection ~ 4950 2725
Wire Wire Line
	4850 2525 4950 2525
Wire Wire Line
	4950 2525 4950 2625
Connection ~ 4950 2625
Wire Wire Line
	4850 2425 4950 2425
Wire Wire Line
	4950 2425 4950 2525
Connection ~ 4950 2525
Wire Wire Line
	4850 2325 4950 2325
Wire Wire Line
	4950 2325 4950 2425
Connection ~ 4950 2425
Connection ~ 4950 2325
Wire Wire Line
	4850 2125 4950 2125
Wire Wire Line
	4850 2025 4950 2025
Wire Wire Line
	4950 2025 4950 2125
Connection ~ 4950 2125
Wire Wire Line
	4850 1925 4950 1925
Wire Wire Line
	4950 1925 4950 2025
Connection ~ 4950 2025
Wire Wire Line
	4850 1825 4950 1825
Wire Wire Line
	4950 1825 4950 1925
Connection ~ 4950 1925
Wire Wire Line
	4850 1725 4950 1725
Wire Wire Line
	4950 1725 4950 1825
Connection ~ 4950 1825
Wire Wire Line
	4850 1625 4950 1625
Wire Wire Line
	4950 1625 4950 1725
Connection ~ 4950 1725
Wire Wire Line
	4850 1525 4950 1525
Wire Wire Line
	4950 1525 4950 1625
Connection ~ 4950 1625
Connection ~ 4950 1525
Wire Wire Line
	4850 1325 4950 1325
Wire Wire Line
	4850 1225 4950 1225
Wire Wire Line
	4950 1225 4950 1325
Connection ~ 4950 1325
Wire Wire Line
	4850 1125 4950 1125
Wire Wire Line
	4950 1125 4950 1225
Connection ~ 4950 1225
Wire Wire Line
	4850 1025 4950 1025
Wire Wire Line
	4950 1025 4950 1125
Connection ~ 4950 1125
Wire Wire Line
	4950 4650 4950 4750
Wire Wire Line
	4850 4750 4950 4750
Connection ~ 4950 4750
Wire Wire Line
	4950 4750 4950 4850
Wire Wire Line
	4950 5450 4950 5550
Wire Wire Line
	4850 5550 4950 5550
Connection ~ 4950 5550
Wire Wire Line
	4950 5550 4950 5650
Wire Wire Line
	4950 1325 4950 1425
Wire Wire Line
	4850 1425 4950 1425
Connection ~ 4950 1425
Wire Wire Line
	4950 1425 4950 1525
Wire Wire Line
	4950 2125 4950 2225
Wire Wire Line
	4850 2225 4950 2225
Connection ~ 4950 2225
Wire Wire Line
	4950 2225 4950 2325
Text GLabel 4250 3950 0    50   Output ~ 0
ZB_M2_V_A
Text GLabel 4250 4050 0    50   Output ~ 0
ZB_M2_V_B
Text GLabel 4250 4150 0    50   Output ~ 0
ZB_M2_V_C
Text GLabel 4250 4250 0    50   Output ~ 0
ZB_M2_CUR_A
Text GLabel 4250 4350 0    50   Output ~ 0
ZB_M2_CUR_B
Text GLabel 4250 4450 0    50   Output ~ 0
ZB_M2_CUR_T
Text GLabel 4250 4750 0    50   Output ~ 0
ZB_M2_ENC_I
Text GLabel 4250 4950 0    50   Output ~ 0
ZB_M2_RES_B
Text GLabel 4250 5150 0    50   Output ~ 0
ZB_M2_HAL_A
Text GLabel 4250 5250 0    50   Output ~ 0
ZB_M2_HAL_B
Text GLabel 4250 5350 0    50   Output ~ 0
ZB_M2_HAL_C
Text GLabel 4250 5450 0    50   Output ~ 0
ZB_M2_TEMP
Text GLabel 4250 5050 0    50   Output ~ 0
ZB_M2_RES_I
Wire Wire Line
	4250 3850 4350 3850
Wire Wire Line
	4250 3950 4350 3950
Wire Wire Line
	4250 4050 4350 4050
Wire Wire Line
	4250 4150 4350 4150
Wire Wire Line
	4250 4250 4350 4250
Wire Wire Line
	4250 4350 4350 4350
Wire Wire Line
	4250 4550 4350 4550
Wire Wire Line
	4250 4650 4350 4650
Wire Wire Line
	4250 4750 4350 4750
Wire Wire Line
	4250 4850 4350 4850
Wire Wire Line
	4250 4950 4350 4950
Wire Wire Line
	4250 5050 4350 5050
Wire Wire Line
	4250 5150 4350 5150
Wire Wire Line
	4250 5250 4350 5250
Wire Wire Line
	4250 5350 4350 5350
Wire Wire Line
	4250 5450 4350 5450
Wire Wire Line
	4250 5550 4350 5550
Wire Wire Line
	4250 5650 4350 5650
Wire Wire Line
	4250 4450 4350 4450
Text GLabel 4250 5650 0    50   Input ~ 0
ZB_SPI_SEL_RDC2
Text GLabel 4250 5550 0    50   Input ~ 0
ZB_M2_RDC_SAMPLE_N
Text GLabel 4250 5850 0    50   Input ~ 0
ZB_SPI_SCK
Wire Wire Line
	4250 5850 4350 5850
Text GLabel 4250 4650 0    50   Output ~ 0
ZB_M2_ENC_B
Text GLabel 4250 4550 0    50   Output ~ 0
ZB_M2_ENC_A
Text GLabel 4250 3850 0    50   Output ~ 0
ZB_M2_V_SUP
Text GLabel 4250 1025 0    50   Input ~ 0
ZB_ADC_CLK
Text GLabel 4250 1225 0    50   Input ~ 0
ZB_VT_ENABLE
Text GLabel 4250 1425 0    50   Input ~ 0
ZB_M2_PWM_AH
Text GLabel 4250 1525 0    50   Input ~ 0
ZB_M2_PWM_AL
Text GLabel 4250 1625 0    50   Input ~ 0
ZB_M2_PWM_BH
Text GLabel 4250 1725 0    50   Input ~ 0
ZB_M2_PWM_BL
Text GLabel 4250 1825 0    50   Input ~ 0
ZB_M2_PWM_CH
Text GLabel 4250 1925 0    50   Input ~ 0
ZB_M2_PWM_CL
Text GLabel 4250 1325 0    50   Input ~ 0
ZB_M2_EN
Text GLabel 4250 2025 0    50   Input ~ 0
ZB_M2_RST_OVR_CUR
Wire Wire Line
	4250 1025 4350 1025
Wire Wire Line
	4250 1125 4350 1125
Wire Wire Line
	4250 1225 4350 1225
Wire Wire Line
	4250 1325 4350 1325
Wire Wire Line
	4250 1425 4350 1425
Wire Wire Line
	4250 1525 4350 1525
Wire Wire Line
	4250 1625 4350 1625
Wire Wire Line
	4250 1725 4350 1725
Wire Wire Line
	4250 1825 4350 1825
Wire Wire Line
	4250 1925 4350 1925
Wire Wire Line
	4250 2025 4350 2025
Text GLabel 4250 1125 0    50   Input ~ 0
ZB_POWER_GOOD
Wire Wire Line
	4250 2125 4350 2125
Text GLabel 4250 2125 0    50   Output ~ 0
ZB_M2_NOT_OVR_CUR
Text GLabel 4250 6050 0    50   Input ~ 0
ZB_SPI_MOSI
Text GLabel 4250 5950 0    50   Output ~ 0
ZB_SPI_MISO
Wire Wire Line
	4250 5950 4350 5950
Wire Wire Line
	4250 6050 4350 6050
$Comp
L PMSM_Driver_ZB_Split_Board_Power:ZB_3V3 #PWR?
U 1 1 5F2FEDC0
P 3150 2150
AR Path="/5F2FEDC0" Ref="#PWR?"  Part="1" 
AR Path="/5E8CABBA/5F2FEDC0" Ref="#PWR0133"  Part="1" 
F 0 "#PWR0133" H 3150 2400 50  0001 C CNN
F 1 "ZB_3V3" H 3151 2323 50  0000 C CNN
F 2 "" H 3150 2150 50  0001 C CNN
F 3 "" H 3150 2150 50  0001 C CNN
	1    3150 2150
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4350 2225 4250 2225
Wire Wire Line
	3150 2150 3150 2225
Wire Wire Line
	4350 2325 4250 2325
Wire Wire Line
	4250 2325 4250 2225
Connection ~ 4250 2225
Wire Wire Line
	4250 2225 3150 2225
Wire Wire Line
	4250 2325 4250 2425
Wire Wire Line
	4250 2425 4350 2425
Connection ~ 4250 2325
Wire Wire Line
	4250 2425 4250 2525
Wire Wire Line
	4250 2525 4350 2525
Connection ~ 4250 2425
$Comp
L PMSM_Driver_ZB_Split_Board_Power:ZB_VADJ #PWR?
U 1 1 5F2FEDD2
P 3175 2500
AR Path="/5F2FEDD2" Ref="#PWR?"  Part="1" 
AR Path="/5E8CABBA/5F2FEDD2" Ref="#PWR0134"  Part="1" 
F 0 "#PWR0134" H 3175 2750 50  0001 C CNN
F 1 "ZB_VADJ" H 3176 2673 50  0000 C CNN
F 2 "" H 3175 2500 50  0001 C CNN
F 3 "" H 3175 2500 50  0001 C CNN
	1    3175 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	4350 2625 4250 2625
Wire Wire Line
	3175 2500 3175 2625
Wire Wire Line
	4350 2725 4250 2725
Wire Wire Line
	4250 2725 4250 2625
Connection ~ 4250 2625
Wire Wire Line
	4250 2625 3175 2625
Wire Wire Line
	4350 2825 4250 2825
Wire Wire Line
	4250 2825 4250 2725
Connection ~ 4250 2725
Wire Wire Line
	4350 2925 4250 2925
Wire Wire Line
	4250 2925 4250 2825
Connection ~ 4250 2825
Text GLabel 4250 6250 0    50   BiDi ~ 0
ZB_FMC_SDA
Text GLabel 4250 6150 0    50   Input ~ 0
ZB_FMC_SCL
Wire Wire Line
	4250 6150 4350 6150
Wire Wire Line
	4250 6250 4350 6250
Text GLabel 4250 5750 0    50   Input ~ 0
ZB_SPI_SEL_AUX2
Wire Wire Line
	4250 5750 4350 5750
Text Notes 3000 800  0    100  ~ 0
Motor 2 Control Connector
Wire Notes Line
	2950 3350 5225 3350
Wire Notes Line
	5225 3350 5225 600 
Wire Notes Line
	2950 600  2950 3350
Wire Notes Line
	2950 600  5225 600 
Text Notes 3000 3625 0    100  ~ 0
Motor 2 Sensor Connector
Wire Notes Line
	2950 3425 5225 3425
Wire Notes Line
	2950 6675 5225 6675
Wire Notes Line
	2950 3425 2950 6675
Wire Notes Line
	5225 3425 5225 6675
Text GLabel 4250 4850 0    50   Output ~ 0
ZB_M2_RES_A
Text GLabel 1900 6250 0    50   BiDi ~ 0
ZB_FMC_SDA
Text GLabel 1900 6150 0    50   Input ~ 0
ZB_FMC_SCL
Wire Wire Line
	1900 6150 2000 6150
Wire Wire Line
	1900 6250 2000 6250
$Comp
L PMSM_Driver_ZB_Split_Board_Connector:Connector_MC-254-40-00-ST-DIP J2
U 1 1 5EF2B854
P 2250 1925
F 0 "J2" H 2200 2925 50  0000 C CNN
F 1 "Connector_MC-254-40-00-ST-DIP" H 1775 825 50  0000 C CNN
F 2 "PMSM_Driver_ZB_Split_Board:IDC-Header_2x20_P2.54mm_Vertical" H 2200 1925 50  0001 C CNN
F 3 "~" H 2200 1925 50  0001 C CNN
F 4 "MC-254-40-00-ST-DIP -  Wire-To-Board Connector, 2.54 mm, 40 Contacts, Header, Through Hole, 2 Rows" H 2250 1925 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc-254-40-00-st-dip/connector-header-40pos-2row-2/dp/2843536" H 2250 1925 50  0001 C CNN "Distributor Link"
	1    2250 1925
	1    0    0    -1  
$EndComp
Text Notes 675  3725 0    25   ~ 0
FMC_SCL and FMC_SDA are 3.3V signals,\nall other signals are VADJ signals.
Text Notes 3025 3725 0    25   ~ 0
FMC_SCL and FMC_SDA are 3.3V signals,\nall other signals are VADJ signals.
Text Notes 3025 900  0    25   ~ 0
ZB_POWER_GOOD is 3.3V signal,\nall other signals are VADJ signals.
Text Notes 675  900  0    25   ~ 0
ZB_POWER_GOOD is 3.3V signal,\nall other signals are VADJ signals.
$Comp
L PMSM_Driver_ZB_Split_Board_Power:ZB_GND #PWR?
U 1 1 5EDAA976
P 2600 3025
AR Path="/5EDAA976" Ref="#PWR?"  Part="1" 
AR Path="/5E893D08/5EDAA976" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 2600 2775 50  0001 C CNN
F 1 "ZB_GND" H 2605 2852 50  0000 C CNN
F 2 "" H 2600 3025 50  0001 C CNN
F 3 "" H 2600 3025 50  0001 C CNN
	1    2600 3025
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_ZB_Split_Board_Power:ZB_GND #PWR?
U 1 1 5EDD9953
P 4950 3025
AR Path="/5EDD9953" Ref="#PWR?"  Part="1" 
AR Path="/5E893D08/5EDD9953" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 4950 2775 50  0001 C CNN
F 1 "ZB_GND" H 4955 2852 50  0000 C CNN
F 2 "" H 4950 3025 50  0001 C CNN
F 3 "" H 4950 3025 50  0001 C CNN
	1    4950 3025
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_ZB_Split_Board_Power:ZB_GND #PWR?
U 1 1 5EE088FF
P 4950 6350
AR Path="/5EE088FF" Ref="#PWR?"  Part="1" 
AR Path="/5E893D08/5EE088FF" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 4950 6100 50  0001 C CNN
F 1 "ZB_GND" H 4955 6177 50  0000 C CNN
F 2 "" H 4950 6350 50  0001 C CNN
F 3 "" H 4950 6350 50  0001 C CNN
	1    4950 6350
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_ZB_Split_Board_Power:ZB_GND #PWR?
U 1 1 5EE376DA
P 2600 6350
AR Path="/5EE376DA" Ref="#PWR?"  Part="1" 
AR Path="/5E893D08/5EE376DA" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 2600 6100 50  0001 C CNN
F 1 "ZB_GND" H 2605 6177 50  0000 C CNN
F 2 "" H 2600 6350 50  0001 C CNN
F 3 "" H 2600 6350 50  0001 C CNN
	1    2600 6350
	1    0    0    -1  
$EndComp
$EndSCHEMATC
