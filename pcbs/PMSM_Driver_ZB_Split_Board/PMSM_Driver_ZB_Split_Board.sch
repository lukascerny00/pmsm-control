EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 3
Title "PMSM_Driver_ZB_Split_Board"
Date "2020-03-30"
Rev "A"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 1000 1000 2000 1000
U 5E893D08
F0 "FMC_Connector" 50
F1 "schematics/PMSM_Driver_ZB_Split_Board_FMC.sch" 50
$EndSheet
$Sheet
S 3500 1000 2000 1000
U 5E8CABBA
F0 "Motor_Connectors" 50
F1 "schematics/PMSM_Driver_ZB_Split_Board_Mot_Conn.sch" 50
$EndSheet
Text Notes 1025 1100 0    50   ~ 0
Page 2
Text Notes 3525 1100 0    50   ~ 0
Page 3
$EndSCHEMATC
