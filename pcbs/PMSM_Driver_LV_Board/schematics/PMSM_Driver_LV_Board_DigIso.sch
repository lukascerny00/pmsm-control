EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 16
Title "PMSM_Driver_LV_Board"
Date "2020-03-30"
Rev "A"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L PMSM_Driver_LV_Board_IC:ADUM1400CRWZ U7
U 1 1 5F086148
P 2150 4400
F 0 "U7" H 1875 4900 50  0000 C CNN
F 1 "ADUM1400CRWZ" H 2150 3900 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:SOIC-16W_7.5x10.3mm_P1.27mm" H 2150 3800 31  0001 C CNN
F 3 "~" H 2150 4400 31  0001 C CNN
F 4 "ADUM1400CRWZ -  Digital Isolator, Quad, 4 Channel, 32 ns, 2.7 V, 5.5 V, SOIC, 16 Pins" H 2150 3750 31  0001 C CNN "Description"
F 5 "https://uk.farnell.com/analog-devices/adum1400crwz/digital-isolator-4ch-smd-adum1400/dp/1226219" H 2150 3700 31  0001 C CNN "Distributor Link"
F 6 "https://www.mouser.co.uk/ProductDetail/Analog-Devices/ADUM1400CRWZ?qs=sGAEpiMZZMssyD0wnx%2FymCCDPdsQyP%2F2ZykM%2FITW%252BEw%3D" H 2150 3650 31  0001 C CNN "Distributor Link 2"
	1    2150 4400
	-1   0    0    -1  
$EndComp
Text GLabel 1450 2100 0    50   Input ~ 0
TR_MOT_EN
Text GLabel 1450 2200 0    50   Input ~ 0
TR_RST_OVR_CUR
Text GLabel 1450 2300 0    50   Output ~ 0
TR_NOT_OVR_CUR
Text GLabel 1450 2000 0    50   Input ~ 0
TR_ADC_CLK
Text GLabel 1450 4450 0    50   Output ~ 0
TR_V_A
Text GLabel 1450 4250 0    50   Output ~ 0
TR_V_B
Text GLabel 1450 6800 0    50   Output ~ 0
TR_V_C
Text GLabel 1450 4550 0    50   Output ~ 0
TR_CUR_A
Text GLabel 1450 4350 0    50   Output ~ 0
TR_CUR_B
Text GLabel 1450 6700 0    50   Output ~ 0
TR_CUR_T
Text GLabel 1450 6500 0    50   Output ~ 0
TR_TEMP
Text GLabel 1450 6600 0    50   Output ~ 0
TR_V_SUP
Text GLabel 4600 4375 0    50   Output ~ 0
TR_HAL_A
Text GLabel 4600 4475 0    50   Output ~ 0
TR_HAL_B
Wire Wire Line
	1700 4750 1600 4750
Wire Wire Line
	1600 4750 1600 4950
Wire Wire Line
	2600 4750 2700 4750
Wire Wire Line
	2700 4750 2700 4950
Wire Wire Line
	2700 4750 2700 4150
Wire Wire Line
	2700 4150 2600 4150
Connection ~ 2700 4750
Wire Wire Line
	1600 4750 1600 4150
Wire Wire Line
	1600 4150 1700 4150
Connection ~ 1600 4750
Wire Wire Line
	1600 3800 1500 3800
Wire Wire Line
	1200 3800 1200 3850
Wire Wire Line
	1300 3800 1200 3800
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5F0A9742
P 1400 3800
AR Path="/5E9C6910/5F0A9742" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5F0A9742" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5F0A9742" Ref="C?"  Part="1" 
AR Path="/5F05E355/5F0A9742" Ref="C?"  Part="1" 
AR Path="/5EA5DEB5/5F0A9742" Ref="C19"  Part="1" 
F 0 "C19" V 1525 3750 50  0000 L CNN
F 1 "100nF" V 1450 3525 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 1400 3800 50  0001 C CNN
F 3 "~" H 1400 3800 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 1400 3400 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 1400 3300 50  0001 C CNN "Distributor Link"
	1    1400 3800
	0    1    -1   0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_3V3 #PWR075
U 1 1 5F0AB1B7
P 1600 3700
F 0 "#PWR075" H 1600 3950 50  0001 C CNN
F 1 "CTRL_3V3" H 1601 3873 50  0000 C CNN
F 2 "" H 1600 3700 50  0001 C CNN
F 3 "" H 1600 3700 50  0001 C CNN
	1    1600 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	2600 4050 2700 4050
Wire Wire Line
	2700 3800 2800 3800
Wire Wire Line
	3100 3800 3100 3850
Wire Wire Line
	3000 3800 3100 3800
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5F0ADD7B
P 2900 3800
AR Path="/5E9C6910/5F0ADD7B" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5F0ADD7B" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5F0ADD7B" Ref="C?"  Part="1" 
AR Path="/5F05E355/5F0ADD7B" Ref="C?"  Part="1" 
AR Path="/5EA5DEB5/5F0ADD7B" Ref="C22"  Part="1" 
F 0 "C22" V 3025 3750 50  0000 L CNN
F 1 "100nF" V 2950 3525 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 2900 3800 50  0001 C CNN
F 3 "~" H 2900 3800 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 2900 3400 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 2900 3300 50  0001 C CNN "Distributor Link"
	1    2900 3800
	0    -1   -1   0   
$EndComp
Connection ~ 1600 3800
Wire Wire Line
	1600 3800 1600 4050
Wire Wire Line
	1600 3700 1600 3800
Connection ~ 2700 3800
Wire Wire Line
	2700 3800 2700 4050
Wire Wire Line
	2700 3700 2700 3800
Wire Wire Line
	1700 4050 1600 4050
Wire Wire Line
	1700 4650 1550 4650
Wire Wire Line
	1550 4650 1550 4050
Wire Wire Line
	1550 4050 1600 4050
Connection ~ 1600 4050
Wire Wire Line
	1700 2500 1600 2500
Wire Wire Line
	1600 2500 1600 2700
Wire Wire Line
	2600 2500 2700 2500
Wire Wire Line
	2700 2500 2700 2700
Wire Wire Line
	2700 2500 2700 1900
Wire Wire Line
	2700 1900 2600 1900
Connection ~ 2700 2500
Wire Wire Line
	1600 2500 1600 1900
Wire Wire Line
	1600 1900 1700 1900
Connection ~ 1600 2500
Wire Wire Line
	1600 1550 1500 1550
Wire Wire Line
	1200 1550 1200 1600
Wire Wire Line
	1300 1550 1200 1550
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5F0B978F
P 1400 1550
AR Path="/5E9C6910/5F0B978F" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5F0B978F" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5F0B978F" Ref="C?"  Part="1" 
AR Path="/5F05E355/5F0B978F" Ref="C?"  Part="1" 
AR Path="/5EA5DEB5/5F0B978F" Ref="C18"  Part="1" 
F 0 "C18" V 1525 1500 50  0000 L CNN
F 1 "100nF" V 1450 1275 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 1400 1550 50  0001 C CNN
F 3 "~" H 1400 1550 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 1400 1150 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 1400 1050 50  0001 C CNN "Distributor Link"
	1    1400 1550
	0    1    -1   0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_3V3 #PWR073
U 1 1 5F0B9795
P 1600 1450
F 0 "#PWR073" H 1600 1700 50  0001 C CNN
F 1 "CTRL_3V3" H 1601 1623 50  0000 C CNN
F 2 "" H 1600 1450 50  0001 C CNN
F 3 "" H 1600 1450 50  0001 C CNN
	1    1600 1450
	1    0    0    -1  
$EndComp
Wire Wire Line
	2600 1800 2700 1800
Wire Wire Line
	2700 1550 2800 1550
Wire Wire Line
	3100 1550 3100 1600
Wire Wire Line
	3000 1550 3100 1550
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5F0B97A7
P 2900 1550
AR Path="/5E9C6910/5F0B97A7" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5F0B97A7" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5F0B97A7" Ref="C?"  Part="1" 
AR Path="/5F05E355/5F0B97A7" Ref="C?"  Part="1" 
AR Path="/5EA5DEB5/5F0B97A7" Ref="C21"  Part="1" 
F 0 "C21" V 3025 1500 50  0000 L CNN
F 1 "100nF" V 2950 1275 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 2900 1550 50  0001 C CNN
F 3 "~" H 2900 1550 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 2900 1150 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 2900 1050 50  0001 C CNN "Distributor Link"
	1    2900 1550
	0    -1   -1   0   
$EndComp
Connection ~ 1600 1550
Wire Wire Line
	1600 1550 1600 1800
Wire Wire Line
	1600 1450 1600 1550
Connection ~ 2700 1550
Wire Wire Line
	2700 1550 2700 1800
Wire Wire Line
	2700 1450 2700 1550
Wire Wire Line
	1700 1800 1600 1800
NoConn ~ 2600 4650
$Comp
L PMSM_Driver_LV_Board_IC:ADUM1400CRWZ U8
U 1 1 5F0EC47D
P 2150 6650
F 0 "U8" H 1875 7150 50  0000 C CNN
F 1 "ADUM1400CRWZ" H 2150 6150 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:SOIC-16W_7.5x10.3mm_P1.27mm" H 2150 6050 31  0001 C CNN
F 3 "~" H 2150 6650 31  0001 C CNN
F 4 "ADUM1400CRWZ -  Digital Isolator, Quad, 4 Channel, 32 ns, 2.7 V, 5.5 V, SOIC, 16 Pins" H 2150 6000 31  0001 C CNN "Description"
F 5 "https://uk.farnell.com/analog-devices/adum1400crwz/digital-isolator-4ch-smd-adum1400/dp/1226219" H 2150 5950 31  0001 C CNN "Distributor Link"
F 6 "https://www.mouser.co.uk/ProductDetail/Analog-Devices/ADUM1400CRWZ?qs=sGAEpiMZZMssyD0wnx%2FymCCDPdsQyP%2F2ZykM%2FITW%252BEw%3D" H 2150 5900 31  0001 C CNN "Distributor Link 2"
	1    2150 6650
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1700 7000 1600 7000
Wire Wire Line
	1600 7000 1600 7200
Wire Wire Line
	2600 7000 2700 7000
Wire Wire Line
	2700 7000 2700 7200
Wire Wire Line
	2700 7000 2700 6400
Wire Wire Line
	2700 6400 2600 6400
Connection ~ 2700 7000
Wire Wire Line
	1600 7000 1600 6400
Wire Wire Line
	1600 6400 1700 6400
Connection ~ 1600 7000
Wire Wire Line
	1600 6050 1500 6050
Wire Wire Line
	1200 6050 1200 6100
Wire Wire Line
	1300 6050 1200 6050
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5F0EC4A4
P 1400 6050
AR Path="/5E9C6910/5F0EC4A4" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5F0EC4A4" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5F0EC4A4" Ref="C?"  Part="1" 
AR Path="/5F05E355/5F0EC4A4" Ref="C?"  Part="1" 
AR Path="/5EA5DEB5/5F0EC4A4" Ref="C20"  Part="1" 
F 0 "C20" V 1525 6000 50  0000 L CNN
F 1 "100nF" V 1450 5775 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 1400 6050 50  0001 C CNN
F 3 "~" H 1400 6050 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 1400 5650 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 1400 5550 50  0001 C CNN "Distributor Link"
	1    1400 6050
	0    1    -1   0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_3V3 #PWR077
U 1 1 5F0EC4AA
P 1600 5950
F 0 "#PWR077" H 1600 6200 50  0001 C CNN
F 1 "CTRL_3V3" H 1601 6123 50  0000 C CNN
F 2 "" H 1600 5950 50  0001 C CNN
F 3 "" H 1600 5950 50  0001 C CNN
	1    1600 5950
	1    0    0    -1  
$EndComp
Wire Wire Line
	2600 6300 2700 6300
Wire Wire Line
	2700 6050 2800 6050
Wire Wire Line
	3100 6050 3100 6100
Wire Wire Line
	3000 6050 3100 6050
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5F0EC4BC
P 2900 6050
AR Path="/5E9C6910/5F0EC4BC" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5F0EC4BC" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5F0EC4BC" Ref="C?"  Part="1" 
AR Path="/5F05E355/5F0EC4BC" Ref="C?"  Part="1" 
AR Path="/5EA5DEB5/5F0EC4BC" Ref="C23"  Part="1" 
F 0 "C23" V 3025 6000 50  0000 L CNN
F 1 "100nF" V 2950 5775 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 2900 6050 50  0001 C CNN
F 3 "~" H 2900 6050 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 2900 5650 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 2900 5550 50  0001 C CNN "Distributor Link"
	1    2900 6050
	0    -1   -1   0   
$EndComp
Connection ~ 1600 6050
Wire Wire Line
	1600 6050 1600 6300
Wire Wire Line
	1600 5950 1600 6050
Connection ~ 2700 6050
Wire Wire Line
	2700 6050 2700 6300
Wire Wire Line
	2700 5950 2700 6050
Wire Wire Line
	1700 6300 1600 6300
Wire Wire Line
	1700 6900 1550 6900
Wire Wire Line
	1550 6900 1550 6300
Wire Wire Line
	1550 6300 1600 6300
Connection ~ 1600 6300
NoConn ~ 2600 6900
Wire Wire Line
	1450 4250 1700 4250
Wire Wire Line
	1450 4350 1700 4350
Wire Wire Line
	1450 4450 1700 4450
Wire Wire Line
	1450 4550 1700 4550
Wire Wire Line
	1450 6500 1700 6500
Wire Wire Line
	1450 6600 1700 6600
Wire Wire Line
	1450 6700 1700 6700
Wire Wire Line
	1450 6800 1700 6800
Text GLabel 2850 4450 2    50   Input ~ 0
ISO_V_A
Text GLabel 2850 4250 2    50   Input ~ 0
ISO_V_B
Text GLabel 2850 6800 2    50   Input ~ 0
ISO_V_C
Text GLabel 2850 6600 2    50   Input ~ 0
ISO_V_SUP
Wire Wire Line
	2600 4250 2850 4250
Wire Wire Line
	2600 4350 2850 4350
Wire Wire Line
	2600 4450 2850 4450
Wire Wire Line
	2600 4550 2850 4550
Wire Wire Line
	2600 6500 2850 6500
Wire Wire Line
	2600 6600 2850 6600
Wire Wire Line
	2600 6700 2850 6700
Wire Wire Line
	2600 6800 2850 6800
Text GLabel 2850 4550 2    50   Input ~ 0
ISO_CUR_A
Text GLabel 2850 4350 2    50   Input ~ 0
ISO_CUR_B
Text GLabel 2850 6700 2    50   Input ~ 0
ISO_CUR_T
Text GLabel 2850 6500 2    50   Input ~ 0
ISO_TEMP
Text GLabel 4600 4775 0    50   Input ~ 0
TR_SPI_SEL_AUX
Text GLabel 4600 6800 0    50   Output ~ 0
TR_SPI_MISO
Text GLabel 4600 4175 0    50   Input ~ 0
TR_RDC_SAMPLE_N
Text GLabel 4600 4275 0    50   Input ~ 0
TR_SPI_SEL_RDC
Text GLabel 4600 4675 0    50   Output ~ 0
TR_HAL_C
$Comp
L PMSM_Driver_LV_Board_IC:ADUM1400CRWZ U12
U 1 1 5F151675
P 8425 2150
F 0 "U12" H 8150 2650 50  0000 C CNN
F 1 "ADUM1400CRWZ" H 8425 1650 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:SOIC-16W_7.5x10.3mm_P1.27mm" H 8425 1550 31  0001 C CNN
F 3 "~" H 8425 2150 31  0001 C CNN
F 4 "ADUM1400CRWZ -  Digital Isolator, Quad, 4 Channel, 32 ns, 2.7 V, 5.5 V, SOIC, 16 Pins" H 8425 1500 31  0001 C CNN "Description"
F 5 "https://uk.farnell.com/analog-devices/adum1400crwz/digital-isolator-4ch-smd-adum1400/dp/1226219" H 8425 1450 31  0001 C CNN "Distributor Link"
F 6 "https://www.mouser.co.uk/ProductDetail/Analog-Devices/ADUM1400CRWZ?qs=sGAEpiMZZMssyD0wnx%2FymCCDPdsQyP%2F2ZykM%2FITW%252BEw%3D" H 8425 1400 31  0001 C CNN "Distributor Link 2"
	1    8425 2150
	1    0    0    -1  
$EndComp
Wire Wire Line
	7975 2500 7875 2500
Wire Wire Line
	7875 2500 7875 2700
Wire Wire Line
	8875 2500 8975 2500
Wire Wire Line
	8975 2500 8975 2700
Wire Wire Line
	8975 2500 8975 1900
Wire Wire Line
	8975 1900 8875 1900
Connection ~ 8975 2500
Wire Wire Line
	7875 2500 7875 1900
Wire Wire Line
	7875 1900 7975 1900
Connection ~ 7875 2500
Wire Wire Line
	7875 1550 7775 1550
Wire Wire Line
	7475 1550 7475 1600
Wire Wire Line
	7575 1550 7475 1550
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_3V3 #PWR0109
U 1 1 5F1516A2
P 7875 1450
F 0 "#PWR0109" H 7875 1700 50  0001 C CNN
F 1 "CTRL_3V3" H 7876 1623 50  0000 C CNN
F 2 "" H 7875 1450 50  0001 C CNN
F 3 "" H 7875 1450 50  0001 C CNN
	1    7875 1450
	1    0    0    -1  
$EndComp
Wire Wire Line
	8875 1800 8975 1800
Wire Wire Line
	8975 1550 9075 1550
Wire Wire Line
	9375 1550 9375 1600
Wire Wire Line
	9275 1550 9375 1550
Connection ~ 7875 1550
Wire Wire Line
	7875 1550 7875 1800
Wire Wire Line
	7875 1450 7875 1550
Connection ~ 8975 1550
Wire Wire Line
	8975 1550 8975 1800
Wire Wire Line
	8975 1450 8975 1550
Wire Wire Line
	7975 1800 7875 1800
NoConn ~ 7975 2400
Wire Wire Line
	7725 2000 7975 2000
Wire Wire Line
	7725 2100 7975 2100
Wire Wire Line
	9125 2000 8875 2000
Wire Wire Line
	9125 2100 8875 2100
$Comp
L PMSM_Driver_LV_Board_IC:ADUM1400CRWZ U13
U 1 1 5F180DDC
P 8425 4150
F 0 "U13" H 8150 4650 50  0000 C CNN
F 1 "ADUM1400CRWZ" H 8425 3650 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:SOIC-16W_7.5x10.3mm_P1.27mm" H 8425 3550 31  0001 C CNN
F 3 "~" H 8425 4150 31  0001 C CNN
F 4 "ADUM1400CRWZ -  Digital Isolator, Quad, 4 Channel, 32 ns, 2.7 V, 5.5 V, SOIC, 16 Pins" H 8425 3500 31  0001 C CNN "Description"
F 5 "https://uk.farnell.com/analog-devices/adum1400crwz/digital-isolator-4ch-smd-adum1400/dp/1226219" H 8425 3450 31  0001 C CNN "Distributor Link"
F 6 "https://www.mouser.co.uk/ProductDetail/Analog-Devices/ADUM1400CRWZ?qs=sGAEpiMZZMssyD0wnx%2FymCCDPdsQyP%2F2ZykM%2FITW%252BEw%3D" H 8425 3400 31  0001 C CNN "Distributor Link 2"
	1    8425 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	7975 4500 7875 4500
Wire Wire Line
	7875 4500 7875 4700
Wire Wire Line
	8875 4500 8975 4500
Wire Wire Line
	8975 4500 8975 4700
Wire Wire Line
	8975 4500 8975 3900
Wire Wire Line
	8975 3900 8875 3900
Connection ~ 8975 4500
Wire Wire Line
	7875 4500 7875 3900
Wire Wire Line
	7875 3900 7975 3900
Connection ~ 7875 4500
Wire Wire Line
	7875 3550 7775 3550
Wire Wire Line
	7475 3550 7475 3600
Wire Wire Line
	7575 3550 7475 3550
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_3V3 #PWR0111
U 1 1 5F180E09
P 7875 3450
F 0 "#PWR0111" H 7875 3700 50  0001 C CNN
F 1 "CTRL_3V3" H 7876 3623 50  0000 C CNN
F 2 "" H 7875 3450 50  0001 C CNN
F 3 "" H 7875 3450 50  0001 C CNN
	1    7875 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	8875 3800 8975 3800
Wire Wire Line
	8975 3550 9075 3550
Wire Wire Line
	9375 3550 9375 3600
Wire Wire Line
	9275 3550 9375 3550
Connection ~ 7875 3550
Wire Wire Line
	7875 3550 7875 3800
Wire Wire Line
	7875 3450 7875 3550
Connection ~ 8975 3550
Wire Wire Line
	8975 3550 8975 3800
Wire Wire Line
	8975 3450 8975 3550
Wire Wire Line
	7975 3800 7875 3800
NoConn ~ 7975 4400
Wire Wire Line
	7725 4000 7975 4000
Wire Wire Line
	7725 4100 7975 4100
Wire Wire Line
	9125 4000 8875 4000
Wire Wire Line
	9125 4100 8875 4100
$Comp
L PMSM_Driver_LV_Board_IC:ADUM1401CRWZ U6
U 1 1 5F1D4B6C
P 2150 2150
F 0 "U6" H 1850 2650 50  0000 C CNN
F 1 "ADUM1401CRWZ" H 2150 1650 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:SOIC-16W_7.5x10.3mm_P1.27mm" H 2150 1550 31  0001 C CNN
F 3 "~" H 2150 2150 31  0001 C CNN
F 4 "ADUM1401CRWZ -  Digital Isolator, Quad, 4 Channel, 32 ns, 2.7 V, 5.5 V, SOIC, 16 Pins" H 2150 1500 31  0001 C CNN "Description"
F 5 "https://uk.farnell.com/analog-devices/adum1401crwz/digital-isolator-4ch-smd-adum1401/dp/1226222" H 2150 1450 31  0001 C CNN "Distributor Link"
F 6 "https://www.mouser.co.uk/ProductDetail/Analog-Devices/ADUM1401CRWZ?qs=sGAEpiMZZMssyD0wnx%2FymCCDPdsQyP%2F2TFG8enADGAw%3D" H 2150 1400 31  0001 C CNN "Distributor Link 2"
	1    2150 2150
	1    0    0    -1  
$EndComp
Wire Wire Line
	1450 2000 1700 2000
Wire Wire Line
	1450 2100 1700 2100
Wire Wire Line
	1450 2200 1700 2200
Wire Wire Line
	1450 2300 1700 2300
Text GLabel 2850 2100 2    50   Output ~ 0
ISO_MOT_EN
Text GLabel 2850 2200 2    50   Output ~ 0
ISO_RST_OVR_CUR
Text GLabel 2850 2300 2    50   Input ~ 0
ISO_NOT_OVR_CUR
Text GLabel 2850 2000 2    50   Output ~ 0
ISO_ADC_CLK
Wire Wire Line
	2850 2000 2600 2000
Wire Wire Line
	2850 2100 2600 2100
Wire Wire Line
	2850 2200 2600 2200
Wire Wire Line
	2850 2300 2600 2300
Wire Wire Line
	1700 2400 1550 2400
Wire Wire Line
	1550 2400 1550 1800
Wire Wire Line
	1550 1800 1600 1800
Connection ~ 1600 1800
Wire Wire Line
	2600 2400 2750 2400
Wire Wire Line
	2750 2400 2750 1800
Wire Wire Line
	2750 1800 2700 1800
Connection ~ 2700 1800
Text GLabel 9125 2400 2    50   Input ~ 0
ISO_MOT_EN
Wire Wire Line
	9125 2400 8875 2400
Text GLabel 9125 4400 2    50   Input ~ 0
ISO_MOT_EN
Wire Wire Line
	9125 4400 8875 4400
Text GLabel 7775 5775 0    50   BiDi ~ 0
CTRL_SDA
Text GLabel 7775 5875 0    50   Input ~ 0
CTRL_SCL
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_3V3 #PWR092
U 1 1 5F583497
P 4750 3450
F 0 "#PWR092" H 4750 3700 50  0001 C CNN
F 1 "CTRL_3V3" H 4751 3623 50  0000 C CNN
F 2 "" H 4750 3450 50  0001 C CNN
F 3 "" H 4750 3450 50  0001 C CNN
	1    4750 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	4900 4875 4800 4875
Wire Wire Line
	4800 4875 4800 5075
Connection ~ 4800 4875
Wire Wire Line
	4800 4075 4900 4075
Wire Wire Line
	4800 4075 4800 4875
Wire Wire Line
	4750 3450 4750 3550
Wire Wire Line
	4800 3975 4900 3975
Wire Wire Line
	4750 3550 4700 3550
Wire Wire Line
	4700 4575 4700 3650
Connection ~ 4750 3550
Wire Wire Line
	4800 3975 4800 3825
Wire Wire Line
	4800 3550 4750 3550
Wire Wire Line
	4600 3650 4700 3650
Connection ~ 4700 3650
Wire Wire Line
	4700 3650 4700 3550
Wire Wire Line
	4600 3825 4800 3825
Connection ~ 4800 3825
Wire Wire Line
	4800 3825 4800 3550
Wire Wire Line
	4200 3825 4200 3875
Wire Wire Line
	4400 3825 4200 3825
Wire Wire Line
	4200 3650 4200 3825
Wire Wire Line
	4400 3650 4200 3650
Connection ~ 4200 3825
Wire Wire Line
	4700 4575 4900 4575
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5F5834BD
P 4500 3650
AR Path="/5E9C6910/5F5834BD" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5F5834BD" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5F5834BD" Ref="C?"  Part="1" 
AR Path="/5F05E355/5F5834BD" Ref="C?"  Part="1" 
AR Path="/5EA5DEB5/5F5834BD" Ref="C26"  Part="1" 
F 0 "C26" V 4550 3675 50  0000 L CNN
F 1 "100nF" V 4550 3375 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 4500 3650 50  0001 C CNN
F 3 "~" H 4500 3650 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 4500 3250 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 4500 3150 50  0001 C CNN "Distributor Link"
	1    4500 3650
	0    1    -1   0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5F5834C5
P 4500 3825
AR Path="/5E9C6910/5F5834C5" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5F5834C5" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5F5834C5" Ref="C?"  Part="1" 
AR Path="/5F05E355/5F5834C5" Ref="C?"  Part="1" 
AR Path="/5EA5DEB5/5F5834C5" Ref="C27"  Part="1" 
F 0 "C27" V 4550 3850 50  0000 L CNN
F 1 "100nF" V 4550 3550 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 4500 3825 50  0001 C CNN
F 3 "~" H 4500 3825 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 4500 3425 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 4500 3325 50  0001 C CNN "Distributor Link"
	1    4500 3825
	0    1    -1   0   
$EndComp
Wire Wire Line
	4900 4175 4600 4175
Wire Wire Line
	4900 4275 4600 4275
Wire Wire Line
	4900 4375 4600 4375
Wire Wire Line
	4900 4475 4600 4475
Wire Wire Line
	4900 4675 4600 4675
Wire Wire Line
	4600 4775 4900 4775
Wire Wire Line
	5800 4875 5900 4875
Wire Wire Line
	5900 4875 5900 5075
Connection ~ 5900 4875
Wire Wire Line
	5900 4075 5800 4075
Wire Wire Line
	5900 4075 5900 4875
Wire Wire Line
	5950 3450 5950 3550
Wire Wire Line
	5900 3975 5800 3975
Wire Wire Line
	5950 3550 6000 3550
Wire Wire Line
	5800 4575 6000 4575
Wire Wire Line
	6000 4575 6000 3650
Connection ~ 5950 3550
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5F5A27D2
P 6200 3825
AR Path="/5E9C6910/5F5A27D2" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5F5A27D2" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5F5A27D2" Ref="C?"  Part="1" 
AR Path="/5F05E355/5F5A27D2" Ref="C?"  Part="1" 
AR Path="/5EA5DEB5/5F5A27D2" Ref="C33"  Part="1" 
F 0 "C33" V 6250 3850 50  0000 L CNN
F 1 "100nF" V 6250 3550 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 6200 3825 50  0001 C CNN
F 3 "~" H 6200 3825 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 6200 3425 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 6200 3325 50  0001 C CNN "Distributor Link"
	1    6200 3825
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5900 3975 5900 3825
Wire Wire Line
	5900 3550 5950 3550
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5F5A27DC
P 6200 3650
AR Path="/5E9C6910/5F5A27DC" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5F5A27DC" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5F5A27DC" Ref="C?"  Part="1" 
AR Path="/5F05E355/5F5A27DC" Ref="C?"  Part="1" 
AR Path="/5EA5DEB5/5F5A27DC" Ref="C32"  Part="1" 
F 0 "C32" V 6250 3675 50  0000 L CNN
F 1 "100nF" V 6250 3375 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 6200 3650 50  0001 C CNN
F 3 "~" H 6200 3650 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 6200 3250 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 6200 3150 50  0001 C CNN "Distributor Link"
	1    6200 3650
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6100 3650 6000 3650
Connection ~ 6000 3650
Wire Wire Line
	6000 3650 6000 3550
Wire Wire Line
	6100 3825 5900 3825
Connection ~ 5900 3825
Wire Wire Line
	5900 3825 5900 3550
Wire Wire Line
	6500 3825 6500 3875
Wire Wire Line
	6300 3825 6500 3825
Wire Wire Line
	6500 3650 6500 3825
Wire Wire Line
	6300 3650 6500 3650
Connection ~ 6500 3825
Wire Wire Line
	5800 4175 6100 4175
Wire Wire Line
	5800 4275 6100 4275
Wire Wire Line
	5800 4375 6100 4375
Wire Wire Line
	5800 4475 6100 4475
Wire Wire Line
	5800 4675 6100 4675
Wire Wire Line
	5800 4775 6100 4775
$Comp
L PMSM_Driver_LV_Board_IC:ADUM7643CRQZ U10
U 1 1 5F51D30E
P 5350 4425
F 0 "U10" H 5075 5025 50  0000 C CNN
F 1 "ADUM7643CRQZ" H 5350 3825 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:QSOP-20_3.9x8.7mm_P0.635mm" H 5350 3725 31  0001 C CNN
F 3 "~" H 4500 5075 31  0001 C CNN
F 4 "ADUM7643CRQZ -  Digital Isolator, 6 Channel, 42 ns, 3 V, 5.5 V, QSOP, 20 Pins" H 5350 3675 31  0001 C CNN "Description"
F 5 "https://uk.farnell.com/analog-devices/adum7643crqz/digital-isolator-6-channel-20qsop/dp/2254977" H 5350 3625 31  0001 C CNN "Distributor Link"
F 6 "https://www.mouser.co.uk/ProductDetail/Analog-Devices/ADUM7643CRQZ?qs=sGAEpiMZZMssyD0wnx%2FymCCDPdsQyP%2F2WoNVy9Vi%2Fec%3D" H 5350 3575 31  0001 C CNN "Distributor Link 2"
	1    5350 4425
	1    0    0    -1  
$EndComp
Text GLabel 6100 4375 2    50   Input ~ 0
ISO_HAL_A
Text GLabel 6100 4475 2    50   Input ~ 0
ISO_HAL_B
Text GLabel 6100 4775 2    50   Output ~ 0
ISO_SPI_SEL_AUX
Text GLabel 6100 4175 2    50   Output ~ 0
ISO_RDC_SAMPLE_N
Text GLabel 6100 4275 2    50   Output ~ 0
ISO_SPI_SEL_RDC
Text GLabel 6100 4675 2    50   Input ~ 0
ISO_HAL_C
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_3V3 #PWR093
U 1 1 5F6D4F14
P 4750 5775
F 0 "#PWR093" H 4750 6025 50  0001 C CNN
F 1 "CTRL_3V3" H 4751 5948 50  0000 C CNN
F 2 "" H 4750 5775 50  0001 C CNN
F 3 "" H 4750 5775 50  0001 C CNN
	1    4750 5775
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 6400 4900 6400
Wire Wire Line
	4800 6400 4800 7000
Wire Wire Line
	4750 5775 4750 5875
Wire Wire Line
	4800 6300 4900 6300
Wire Wire Line
	4750 5875 4700 5875
Wire Wire Line
	4700 6900 4700 5975
Connection ~ 4750 5875
Wire Wire Line
	4800 6300 4800 6150
Wire Wire Line
	4800 5875 4750 5875
Wire Wire Line
	4600 5975 4700 5975
Connection ~ 4700 5975
Wire Wire Line
	4700 5975 4700 5875
Wire Wire Line
	4600 6150 4800 6150
Connection ~ 4800 6150
Wire Wire Line
	4800 6150 4800 5875
Wire Wire Line
	4200 6150 4200 6200
Wire Wire Line
	4400 6150 4200 6150
Wire Wire Line
	4200 5975 4200 6150
Wire Wire Line
	4400 5975 4200 5975
Connection ~ 4200 6150
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5F6D4F30
P 4500 5975
AR Path="/5E9C6910/5F6D4F30" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5F6D4F30" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5F6D4F30" Ref="C?"  Part="1" 
AR Path="/5F05E355/5F6D4F30" Ref="C?"  Part="1" 
AR Path="/5EA5DEB5/5F6D4F30" Ref="C28"  Part="1" 
F 0 "C28" V 4550 6000 50  0000 L CNN
F 1 "100nF" V 4550 5700 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 4500 5975 50  0001 C CNN
F 3 "~" H 4500 5975 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 4500 5575 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 4500 5475 50  0001 C CNN "Distributor Link"
	1    4500 5975
	0    1    -1   0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5F6D4F38
P 4500 6150
AR Path="/5E9C6910/5F6D4F38" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5F6D4F38" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5F6D4F38" Ref="C?"  Part="1" 
AR Path="/5F05E355/5F6D4F38" Ref="C?"  Part="1" 
AR Path="/5EA5DEB5/5F6D4F38" Ref="C29"  Part="1" 
F 0 "C29" V 4550 6175 50  0000 L CNN
F 1 "100nF" V 4550 5875 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 4500 6150 50  0001 C CNN
F 3 "~" H 4500 6150 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 4500 5750 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 4500 5650 50  0001 C CNN "Distributor Link"
	1    4500 6150
	0    1    -1   0   
$EndComp
Wire Wire Line
	4700 6900 4900 6900
Wire Wire Line
	4900 7000 4800 7000
Connection ~ 4800 7000
Wire Wire Line
	4800 7000 4800 7200
Wire Wire Line
	5900 6400 5800 6400
Wire Wire Line
	5900 6400 5900 7000
Wire Wire Line
	5950 5775 5950 5875
Wire Wire Line
	5900 6300 5800 6300
Wire Wire Line
	5950 5875 6000 5875
Wire Wire Line
	6000 6900 6000 5975
Connection ~ 5950 5875
Wire Wire Line
	5900 6300 5900 6150
Wire Wire Line
	5900 5875 5950 5875
Wire Wire Line
	6100 5975 6000 5975
Connection ~ 6000 5975
Wire Wire Line
	6000 5975 6000 5875
Wire Wire Line
	6100 6150 5900 6150
Connection ~ 5900 6150
Wire Wire Line
	5900 6150 5900 5875
Wire Wire Line
	6500 6150 6500 6200
Wire Wire Line
	6300 6150 6500 6150
Wire Wire Line
	6500 5975 6500 6150
Wire Wire Line
	6300 5975 6500 5975
Connection ~ 6500 6150
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5F764A5C
P 6200 5975
AR Path="/5E9C6910/5F764A5C" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5F764A5C" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5F764A5C" Ref="C?"  Part="1" 
AR Path="/5F05E355/5F764A5C" Ref="C?"  Part="1" 
AR Path="/5EA5DEB5/5F764A5C" Ref="C34"  Part="1" 
F 0 "C34" V 6250 6000 50  0000 L CNN
F 1 "100nF" V 6250 5700 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 6200 5975 50  0001 C CNN
F 3 "~" H 6200 5975 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 6200 5575 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 6200 5475 50  0001 C CNN "Distributor Link"
	1    6200 5975
	0    -1   -1   0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5F764A64
P 6200 6150
AR Path="/5E9C6910/5F764A64" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5F764A64" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5F764A64" Ref="C?"  Part="1" 
AR Path="/5F05E355/5F764A64" Ref="C?"  Part="1" 
AR Path="/5EA5DEB5/5F764A64" Ref="C35"  Part="1" 
F 0 "C35" V 6250 6175 50  0000 L CNN
F 1 "100nF" V 6250 5875 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 6200 6150 50  0001 C CNN
F 3 "~" H 6200 6150 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 6200 5750 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 6200 5650 50  0001 C CNN "Distributor Link"
	1    6200 6150
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6000 6900 5800 6900
Wire Wire Line
	5800 7000 5900 7000
Connection ~ 5900 7000
Wire Wire Line
	5900 7000 5900 7200
Wire Wire Line
	4600 6500 4900 6500
Wire Wire Line
	4600 6600 4900 6600
Wire Wire Line
	4600 6800 4900 6800
Text GLabel 6100 6800 2    50   Input ~ 0
ISO_SPI_MISO
Wire Wire Line
	6100 6500 5800 6500
Wire Wire Line
	6100 6600 5800 6600
Wire Wire Line
	6100 6800 5800 6800
$Comp
L PMSM_Driver_LV_Board_IC:ADUM1250ARZ U14
U 1 1 5F9947A8
P 8425 5825
F 0 "U14" H 8200 6125 50  0000 C CNN
F 1 "ADUM1250ARZ" H 8425 5525 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:SOIC-8_3.9x4.9mm_P1.27mm" H 8425 5425 31  0001 C CNN
F 3 "~" H 7775 6125 31  0001 C CNN
F 4 "ADUM1250ARZ -  Digital Isolator, Dual, 2 Channel, 150 ns, 3 V, 5.5 V, SOIC, 8 Pins" H 8425 5375 31  0001 C CNN "Description"
F 5 "https://uk.farnell.com/analog-devices/adum1250arz/ic-isolator-digital-dual-8soic/dp/1814353" H 8425 5325 31  0001 C CNN "Distributor Link"
F 6 "https://www.mouser.co.uk/ProductDetail/Analog-Devices/ADUM1250ARZ?qs=sGAEpiMZZMv9Q1JI0Mo%2FtZ4z1bUw0VBv" H 8425 5275 31  0001 C CNN "Distributor Link 2"
	1    8425 5825
	-1   0    0    -1  
$EndComp
Wire Wire Line
	7925 6175 7925 5975
Wire Wire Line
	7925 5975 8025 5975
Wire Wire Line
	8925 6175 8925 5975
Wire Wire Line
	8925 5975 8825 5975
Wire Wire Line
	7925 5425 7825 5425
Wire Wire Line
	7525 5425 7525 5475
Wire Wire Line
	7625 5425 7525 5425
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5FA718F8
P 7725 5425
AR Path="/5E9C6910/5FA718F8" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5FA718F8" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5FA718F8" Ref="C?"  Part="1" 
AR Path="/5F05E355/5FA718F8" Ref="C?"  Part="1" 
AR Path="/5EA5DEB5/5FA718F8" Ref="C38"  Part="1" 
F 0 "C38" V 7850 5375 50  0000 L CNN
F 1 "100nF" V 7775 5150 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 7725 5425 50  0001 C CNN
F 3 "~" H 7725 5425 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 7725 5025 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 7725 4925 50  0001 C CNN "Distributor Link"
	1    7725 5425
	0    1    -1   0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_3V3 #PWR0113
U 1 1 5FA718FE
P 7925 5325
F 0 "#PWR0113" H 7925 5575 50  0001 C CNN
F 1 "CTRL_3V3" H 7926 5498 50  0000 C CNN
F 2 "" H 7925 5325 50  0001 C CNN
F 3 "" H 7925 5325 50  0001 C CNN
	1    7925 5325
	1    0    0    -1  
$EndComp
Connection ~ 7925 5425
Wire Wire Line
	7925 5425 7925 5675
Wire Wire Line
	7925 5325 7925 5425
Wire Wire Line
	7925 5675 8025 5675
Wire Wire Line
	8925 5425 9025 5425
Wire Wire Line
	9325 5425 9325 5475
Wire Wire Line
	9225 5425 9325 5425
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5FABE6E6
P 9125 5425
AR Path="/5E9C6910/5FABE6E6" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5FABE6E6" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5FABE6E6" Ref="C?"  Part="1" 
AR Path="/5F05E355/5FABE6E6" Ref="C?"  Part="1" 
AR Path="/5EA5DEB5/5FABE6E6" Ref="C39"  Part="1" 
F 0 "C39" V 9250 5375 50  0000 L CNN
F 1 "100nF" V 9175 5150 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 9125 5425 50  0001 C CNN
F 3 "~" H 9125 5425 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 9125 5025 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 9125 4925 50  0001 C CNN "Distributor Link"
	1    9125 5425
	0    -1   -1   0   
$EndComp
Connection ~ 8925 5425
Wire Wire Line
	8925 5425 8925 5675
Wire Wire Line
	8925 5325 8925 5425
Wire Wire Line
	8925 5675 8825 5675
Wire Wire Line
	7775 5775 8025 5775
Wire Wire Line
	7775 5875 8025 5875
Text GLabel 10050 5775 2    50   BiDi ~ 0
ISO_SDA
Text GLabel 10050 5875 2    50   Output ~ 0
ISO_SCL
Text Notes 750  875  0    100  ~ 0
Digital Isolators
Connection ~ 6500 1450
Wire Wire Line
	6500 1275 6500 1450
Wire Wire Line
	6500 1450 6500 1500
Wire Wire Line
	5800 2400 6100 2400
Wire Wire Line
	5800 2300 6100 2300
Wire Wire Line
	5800 2100 6100 2100
Wire Wire Line
	5800 2000 6100 2000
Wire Wire Line
	5800 1900 6100 1900
Wire Wire Line
	5800 1800 6100 1800
Wire Wire Line
	4600 2400 4900 2400
Wire Wire Line
	4900 2300 4600 2300
Wire Wire Line
	4900 2100 4600 2100
Wire Wire Line
	4900 2000 4600 2000
Wire Wire Line
	4900 1900 4600 1900
Wire Wire Line
	4900 1800 4600 1800
Text GLabel 4600 2400 0    50   Output ~ 0
TR_ENC_A
Text GLabel 4600 2300 0    50   Output ~ 0
TR_ENC_B
Text GLabel 4600 1800 0    50   Output ~ 0
TR_RES_I
Text GLabel 4600 1900 0    50   Output ~ 0
TR_RES_B
Text GLabel 4600 2000 0    50   Output ~ 0
TR_RES_A
Text GLabel 4600 2100 0    50   Output ~ 0
TR_ENC_I
Text GLabel 6100 2400 2    50   Input ~ 0
ISO_ENC_A
Text GLabel 6100 2300 2    50   Input ~ 0
ISO_ENC_B
Text GLabel 6100 1800 2    50   Input ~ 0
ISO_RES_I
Text GLabel 6100 1900 2    50   Input ~ 0
ISO_RES_B
Text GLabel 6100 2000 2    50   Input ~ 0
ISO_RES_A
Text GLabel 6100 2100 2    50   Input ~ 0
ISO_ENC_I
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5F400C11
P 4500 1450
AR Path="/5E9C6910/5F400C11" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5F400C11" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5F400C11" Ref="C?"  Part="1" 
AR Path="/5F05E355/5F400C11" Ref="C?"  Part="1" 
AR Path="/5EA5DEB5/5F400C11" Ref="C25"  Part="1" 
F 0 "C25" V 4550 1475 50  0000 L CNN
F 1 "100nF" V 4550 1175 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 4500 1450 50  0001 C CNN
F 3 "~" H 4500 1450 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 4500 1050 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 4500 950 50  0001 C CNN "Distributor Link"
	1    4500 1450
	0    1    -1   0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5F400C1B
P 4500 1275
AR Path="/5E9C6910/5F400C1B" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5F400C1B" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5F400C1B" Ref="C?"  Part="1" 
AR Path="/5F05E355/5F400C1B" Ref="C?"  Part="1" 
AR Path="/5EA5DEB5/5F400C1B" Ref="C24"  Part="1" 
F 0 "C24" V 4550 1300 50  0000 L CNN
F 1 "100nF" V 4550 1000 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 4500 1275 50  0001 C CNN
F 3 "~" H 4500 1275 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 4500 875 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 4500 775 50  0001 C CNN "Distributor Link"
	1    4500 1275
	0    1    -1   0   
$EndComp
Wire Wire Line
	4700 2200 4900 2200
Connection ~ 4200 1450
Wire Wire Line
	4400 1275 4200 1275
Wire Wire Line
	4200 1275 4200 1450
Wire Wire Line
	4400 1450 4200 1450
Wire Wire Line
	4200 1450 4200 1500
Wire Wire Line
	4800 1450 4800 1175
Connection ~ 4800 1450
Wire Wire Line
	4600 1450 4800 1450
Wire Wire Line
	4700 1275 4700 1175
Connection ~ 4700 1275
Wire Wire Line
	4600 1275 4700 1275
Wire Wire Line
	4800 1175 4750 1175
Wire Wire Line
	4800 1600 4800 1450
Connection ~ 4750 1175
Wire Wire Line
	4700 2200 4700 1275
Wire Wire Line
	4750 1175 4700 1175
Wire Wire Line
	4800 1600 4900 1600
Wire Wire Line
	4750 1075 4750 1175
Wire Wire Line
	6300 1275 6500 1275
Wire Wire Line
	6300 1450 6500 1450
Wire Wire Line
	5900 1450 5900 1175
Connection ~ 5900 1450
Wire Wire Line
	6100 1450 5900 1450
Wire Wire Line
	6000 1275 6000 1175
Connection ~ 6000 1275
Wire Wire Line
	6100 1275 6000 1275
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5F2932BB
P 6200 1275
AR Path="/5E9C6910/5F2932BB" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5F2932BB" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5F2932BB" Ref="C?"  Part="1" 
AR Path="/5F05E355/5F2932BB" Ref="C?"  Part="1" 
AR Path="/5EA5DEB5/5F2932BB" Ref="C30"  Part="1" 
F 0 "C30" V 6250 1300 50  0000 L CNN
F 1 "100nF" V 6250 1000 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 6200 1275 50  0001 C CNN
F 3 "~" H 6200 1275 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 6200 875 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 6200 775 50  0001 C CNN "Distributor Link"
	1    6200 1275
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5900 1175 5950 1175
Wire Wire Line
	5900 1600 5900 1450
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5F31AFDE
P 6200 1450
AR Path="/5E9C6910/5F31AFDE" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5F31AFDE" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5F31AFDE" Ref="C?"  Part="1" 
AR Path="/5F05E355/5F31AFDE" Ref="C?"  Part="1" 
AR Path="/5EA5DEB5/5F31AFDE" Ref="C31"  Part="1" 
F 0 "C31" V 6250 1475 50  0000 L CNN
F 1 "100nF" V 6250 1175 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 6200 1450 50  0001 C CNN
F 3 "~" H 6200 1450 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 6200 1050 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 6200 950 50  0001 C CNN "Distributor Link"
	1    6200 1450
	0    -1   -1   0   
$EndComp
Connection ~ 5950 1175
Wire Wire Line
	6000 2200 6000 1275
Wire Wire Line
	5800 2200 6000 2200
Wire Wire Line
	5950 1175 6000 1175
Wire Wire Line
	5900 1600 5800 1600
Wire Wire Line
	5950 1075 5950 1175
Wire Wire Line
	4800 1700 4800 2500
Wire Wire Line
	4800 1700 4900 1700
Connection ~ 4800 2500
Wire Wire Line
	4800 2500 4800 2700
Wire Wire Line
	4900 2500 4800 2500
Wire Wire Line
	5900 1700 5900 2500
Wire Wire Line
	5900 1700 5800 1700
Connection ~ 5900 2500
Wire Wire Line
	5900 2500 5900 2700
Wire Wire Line
	5800 2500 5900 2500
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_3V3 #PWR091
U 1 1 5F25A013
P 4750 1075
F 0 "#PWR091" H 4750 1325 50  0001 C CNN
F 1 "CTRL_3V3" H 4751 1248 50  0000 C CNN
F 2 "" H 4750 1075 50  0001 C CNN
F 3 "" H 4750 1075 50  0001 C CNN
	1    4750 1075
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_IC:ADUM7640CRQZ U9
U 1 1 5F08D908
P 5350 2050
F 0 "U9" H 5075 2650 50  0000 C CNN
F 1 "ADUM7640CRQZ" H 5350 1450 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:QSOP-20_3.9x8.7mm_P0.635mm" H 5350 1350 31  0001 C CNN
F 3 "~" H 4500 2700 31  0001 C CNN
F 4 "ADUM7640CRQZ -  Digital Isolator, 6 Channel, 42 ns, 3 V, 5.5 V, QSOP, 20 Pins" H 5350 1300 31  0001 C CNN "Description"
F 5 "https://uk.farnell.com/analog-devices/adum7640crqz/digital-isolator-6-channel-20qsop/dp/2254974" H 5350 1250 31  0001 C CNN "Distributor Link"
F 6 "https://www.mouser.co.uk/ProductDetail/Analog-Devices/ADUM7640CRQZ?qs=sGAEpiMZZMssyD0wnx%2FymCCDPdsQyP%2F2tYwAj2efOwU%3D" H 5350 1200 31  0001 C CNN "Distributor Link 2"
	1    5350 2050
	-1   0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R87
U 1 1 5FEBEB85
P 9650 5575
F 0 "R87" H 9775 5575 50  0000 C CNN
F 1 "4k7" H 9750 5500 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 9650 5575 50  0001 C CNN
F 3 "~" H 9650 5575 50  0001 C CNN
F 4 "MC0603SAF4701T5E -  RES, THICK FILM, 4K7, 1%, 0.1W, 0603" H 9650 5175 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0603saf4701t5e/res-thick-film-4k7-1-0-1w-0603/dp/1632439" H 9650 5075 50  0001 C CNN "Distributor Link"
	1    9650 5575
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R88
U 1 1 5FF944CC
P 9900 5575
F 0 "R88" H 10025 5575 50  0000 C CNN
F 1 "4k7" H 10000 5500 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 9900 5575 50  0001 C CNN
F 3 "~" H 9900 5575 50  0001 C CNN
F 4 "MC0603SAF4701T5E -  RES, THICK FILM, 4K7, 1%, 0.1W, 0603" H 9900 5175 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0603saf4701t5e/res-thick-film-4k7-1-0-1w-0603/dp/1632439" H 9900 5075 50  0001 C CNN "Distributor Link"
	1    9900 5575
	1    0    0    -1  
$EndComp
Wire Wire Line
	8825 5875 9900 5875
Wire Wire Line
	9650 5775 9650 5675
Wire Wire Line
	9900 5675 9900 5875
Connection ~ 9900 5875
Wire Wire Line
	9900 5875 10050 5875
Wire Wire Line
	9900 5475 9900 5400
Wire Wire Line
	9900 5400 9775 5400
Wire Wire Line
	9775 5400 9775 5325
Wire Wire Line
	9650 5475 9650 5400
Connection ~ 9650 5775
Wire Wire Line
	9650 5775 10050 5775
Wire Wire Line
	8825 5775 9650 5775
Wire Wire Line
	9650 5400 9775 5400
Connection ~ 9775 5400
$Comp
L PMSM_Driver_LV_Board_Power:ISO_DGND #PWR085
U 1 1 602F2049
P 3100 1600
F 0 "#PWR085" H 3100 1350 50  0001 C CNN
F 1 "ISO_DGND" H 3105 1427 50  0000 C CNN
F 2 "" H 3100 1600 50  0001 C CNN
F 3 "" H 3100 1600 50  0001 C CNN
	1    3100 1600
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:ISO_DGND #PWR080
U 1 1 602F2BD4
P 2700 2700
F 0 "#PWR080" H 2700 2450 50  0001 C CNN
F 1 "ISO_DGND" H 2705 2527 50  0000 C CNN
F 2 "" H 2700 2700 50  0001 C CNN
F 3 "" H 2700 2700 50  0001 C CNN
	1    2700 2700
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:ISO_DGND #PWR0103
U 1 1 6030E4A5
P 6500 1500
F 0 "#PWR0103" H 6500 1250 50  0001 C CNN
F 1 "ISO_DGND" H 6505 1327 50  0000 C CNN
F 2 "" H 6500 1500 50  0001 C CNN
F 3 "" H 6500 1500 50  0001 C CNN
	1    6500 1500
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:ISO_DGND #PWR097
U 1 1 60329B78
P 5900 2700
F 0 "#PWR097" H 5900 2450 50  0001 C CNN
F 1 "ISO_DGND" H 5905 2527 50  0000 C CNN
F 2 "" H 5900 2700 50  0001 C CNN
F 3 "" H 5900 2700 50  0001 C CNN
	1    5900 2700
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:ISO_DGND #PWR0104
U 1 1 603452E6
P 6500 3875
F 0 "#PWR0104" H 6500 3625 50  0001 C CNN
F 1 "ISO_DGND" H 6505 3702 50  0000 C CNN
F 2 "" H 6500 3875 50  0001 C CNN
F 3 "" H 6500 3875 50  0001 C CNN
	1    6500 3875
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:ISO_DGND #PWR098
U 1 1 60360A55
P 5900 5075
F 0 "#PWR098" H 5900 4825 50  0001 C CNN
F 1 "ISO_DGND" H 5905 4902 50  0000 C CNN
F 2 "" H 5900 5075 50  0001 C CNN
F 3 "" H 5900 5075 50  0001 C CNN
	1    5900 5075
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:ISO_DGND #PWR0105
U 1 1 6037C2B8
P 6500 6200
F 0 "#PWR0105" H 6500 5950 50  0001 C CNN
F 1 "ISO_DGND" H 6505 6027 50  0000 C CNN
F 2 "" H 6500 6200 50  0001 C CNN
F 3 "" H 6500 6200 50  0001 C CNN
	1    6500 6200
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:ISO_DGND #PWR099
U 1 1 60397B3D
P 5900 7200
F 0 "#PWR099" H 5900 6950 50  0001 C CNN
F 1 "ISO_DGND" H 5905 7027 50  0000 C CNN
F 2 "" H 5900 7200 50  0001 C CNN
F 3 "" H 5900 7200 50  0001 C CNN
	1    5900 7200
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:ISO_DGND #PWR084
U 1 1 603B31F6
P 2700 7200
F 0 "#PWR084" H 2700 6950 50  0001 C CNN
F 1 "ISO_DGND" H 2705 7027 50  0000 C CNN
F 2 "" H 2700 7200 50  0001 C CNN
F 3 "" H 2700 7200 50  0001 C CNN
	1    2700 7200
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:ISO_DGND #PWR087
U 1 1 603CE917
P 3100 6100
F 0 "#PWR087" H 3100 5850 50  0001 C CNN
F 1 "ISO_DGND" H 3105 5927 50  0000 C CNN
F 2 "" H 3100 6100 50  0001 C CNN
F 3 "" H 3100 6100 50  0001 C CNN
	1    3100 6100
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:ISO_DGND #PWR082
U 1 1 603E9FDD
P 2700 4950
F 0 "#PWR082" H 2700 4700 50  0001 C CNN
F 1 "ISO_DGND" H 2705 4777 50  0000 C CNN
F 2 "" H 2700 4950 50  0001 C CNN
F 3 "" H 2700 4950 50  0001 C CNN
	1    2700 4950
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:ISO_DGND #PWR086
U 1 1 6040571B
P 3100 3850
F 0 "#PWR086" H 3100 3600 50  0001 C CNN
F 1 "ISO_DGND" H 3105 3677 50  0000 C CNN
F 2 "" H 3100 3850 50  0001 C CNN
F 3 "" H 3100 3850 50  0001 C CNN
	1    3100 3850
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:ISO_DGND #PWR0122
U 1 1 60420E4D
P 9375 1600
F 0 "#PWR0122" H 9375 1350 50  0001 C CNN
F 1 "ISO_DGND" H 9380 1427 50  0000 C CNN
F 2 "" H 9375 1600 50  0001 C CNN
F 3 "" H 9375 1600 50  0001 C CNN
	1    9375 1600
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:ISO_DGND #PWR0118
U 1 1 6043C4D1
P 8975 2700
F 0 "#PWR0118" H 8975 2450 50  0001 C CNN
F 1 "ISO_DGND" H 8980 2527 50  0000 C CNN
F 2 "" H 8975 2700 50  0001 C CNN
F 3 "" H 8975 2700 50  0001 C CNN
	1    8975 2700
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:ISO_DGND #PWR0123
U 1 1 60457D53
P 9375 3600
F 0 "#PWR0123" H 9375 3350 50  0001 C CNN
F 1 "ISO_DGND" H 9380 3427 50  0000 C CNN
F 2 "" H 9375 3600 50  0001 C CNN
F 3 "" H 9375 3600 50  0001 C CNN
	1    9375 3600
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:ISO_DGND #PWR0120
U 1 1 604734A2
P 8975 4700
F 0 "#PWR0120" H 8975 4450 50  0001 C CNN
F 1 "ISO_DGND" H 8980 4527 50  0000 C CNN
F 2 "" H 8975 4700 50  0001 C CNN
F 3 "" H 8975 4700 50  0001 C CNN
	1    8975 4700
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:ISO_DGND #PWR0116
U 1 1 6048EBCC
P 8925 6175
F 0 "#PWR0116" H 8925 5925 50  0001 C CNN
F 1 "ISO_DGND" H 8930 6002 50  0000 C CNN
F 2 "" H 8925 6175 50  0001 C CNN
F 3 "" H 8925 6175 50  0001 C CNN
	1    8925 6175
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:ISO_DGND #PWR0121
U 1 1 604AA370
P 9325 5475
F 0 "#PWR0121" H 9325 5225 50  0001 C CNN
F 1 "ISO_DGND" H 9330 5302 50  0000 C CNN
F 2 "" H 9325 5475 50  0001 C CNN
F 3 "" H 9325 5475 50  0001 C CNN
	1    9325 5475
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_IC:ADUM7441CRQZ U11
U 1 1 60FCBEC9
P 5350 6650
F 0 "U11" H 5050 7150 50  0000 C CNN
F 1 "ADUM7441CRQZ" H 5350 6150 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:QSOP-16_3.9x4.9mm_P0.635mm" H 5350 6050 31  0001 C CNN
F 3 "~" H 5350 6650 31  0001 C CNN
F 4 "ADUM7441ARQZ -  Digital Isolator, 4 Channel, 75 ns, 3 V, 5.5 V, QSOP, 16 Pins" H 5350 5950 31  0001 C CNN "Description"
F 5 "https://uk.farnell.com/analog-devices/adum7441arqz/ic-isolator-digital-quad-16qsop/dp/1827343" H 5350 6000 31  0001 C CNN "Distributor Link"
F 6 "https://www.mouser.co.uk/ProductDetail/Analog-Devices/ADUM7441ARQZ?qs=sGAEpiMZZMssyD0wnx%2FymCCDPdsQyP%2F2w1c1Qk6Gjgg%3D" H 5350 5900 31  0001 C CNN "Distributor Link 2"
	1    5350 6650
	1    0    0    -1  
$EndComp
Text GLabel 4600 6700 0    50   Input ~ 0
TR_VT_ENABLE
Wire Wire Line
	4600 6700 4900 6700
Text GLabel 6100 6700 2    50   Output ~ 0
ISO_VT_ENABLE
Wire Wire Line
	6100 6700 5800 6700
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_DGND #PWR?
U 1 1 5F0F4DAC
P 1600 2700
AR Path="/5E9C6910/5F0F4DAC" Ref="#PWR?"  Part="1" 
AR Path="/5F05E355/5F0F4DAC" Ref="#PWR?"  Part="1" 
AR Path="/5EA5DEB5/5F0F4DAC" Ref="#PWR074"  Part="1" 
F 0 "#PWR074" H 1600 2450 50  0001 C CNN
F 1 "CTRL_DGND" H 1605 2527 50  0000 C CNN
F 2 "" H 1600 2700 50  0001 C CNN
F 3 "" H 1600 2700 50  0001 C CNN
	1    1600 2700
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_DGND #PWR?
U 1 1 5F110CE7
P 1200 1600
AR Path="/5E9C6910/5F110CE7" Ref="#PWR?"  Part="1" 
AR Path="/5F05E355/5F110CE7" Ref="#PWR?"  Part="1" 
AR Path="/5EA5DEB5/5F110CE7" Ref="#PWR070"  Part="1" 
F 0 "#PWR070" H 1200 1350 50  0001 C CNN
F 1 "CTRL_DGND" H 1205 1427 50  0000 C CNN
F 2 "" H 1200 1600 50  0001 C CNN
F 3 "" H 1200 1600 50  0001 C CNN
	1    1200 1600
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_DGND #PWR?
U 1 1 5F164429
P 1600 4950
AR Path="/5E9C6910/5F164429" Ref="#PWR?"  Part="1" 
AR Path="/5F05E355/5F164429" Ref="#PWR?"  Part="1" 
AR Path="/5EA5DEB5/5F164429" Ref="#PWR076"  Part="1" 
F 0 "#PWR076" H 1600 4700 50  0001 C CNN
F 1 "CTRL_DGND" H 1605 4777 50  0000 C CNN
F 2 "" H 1600 4950 50  0001 C CNN
F 3 "" H 1600 4950 50  0001 C CNN
	1    1600 4950
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_DGND #PWR?
U 1 1 5F1801F2
P 1200 3850
AR Path="/5E9C6910/5F1801F2" Ref="#PWR?"  Part="1" 
AR Path="/5F05E355/5F1801F2" Ref="#PWR?"  Part="1" 
AR Path="/5EA5DEB5/5F1801F2" Ref="#PWR071"  Part="1" 
F 0 "#PWR071" H 1200 3600 50  0001 C CNN
F 1 "CTRL_DGND" H 1205 3677 50  0000 C CNN
F 2 "" H 1200 3850 50  0001 C CNN
F 3 "" H 1200 3850 50  0001 C CNN
	1    1200 3850
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_DGND #PWR?
U 1 1 5F1D3A2A
P 1600 7200
AR Path="/5E9C6910/5F1D3A2A" Ref="#PWR?"  Part="1" 
AR Path="/5F05E355/5F1D3A2A" Ref="#PWR?"  Part="1" 
AR Path="/5EA5DEB5/5F1D3A2A" Ref="#PWR078"  Part="1" 
F 0 "#PWR078" H 1600 6950 50  0001 C CNN
F 1 "CTRL_DGND" H 1605 7027 50  0000 C CNN
F 2 "" H 1600 7200 50  0001 C CNN
F 3 "" H 1600 7200 50  0001 C CNN
	1    1600 7200
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_DGND #PWR?
U 1 1 5F1EF885
P 1200 6100
AR Path="/5E9C6910/5F1EF885" Ref="#PWR?"  Part="1" 
AR Path="/5F05E355/5F1EF885" Ref="#PWR?"  Part="1" 
AR Path="/5EA5DEB5/5F1EF885" Ref="#PWR072"  Part="1" 
F 0 "#PWR072" H 1200 5850 50  0001 C CNN
F 1 "CTRL_DGND" H 1205 5927 50  0000 C CNN
F 2 "" H 1200 6100 50  0001 C CNN
F 3 "" H 1200 6100 50  0001 C CNN
	1    1200 6100
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_DGND #PWR?
U 1 1 5F2272AC
P 4200 1500
AR Path="/5E9C6910/5F2272AC" Ref="#PWR?"  Part="1" 
AR Path="/5F05E355/5F2272AC" Ref="#PWR?"  Part="1" 
AR Path="/5EA5DEB5/5F2272AC" Ref="#PWR088"  Part="1" 
F 0 "#PWR088" H 4200 1250 50  0001 C CNN
F 1 "CTRL_DGND" H 4205 1327 50  0000 C CNN
F 2 "" H 4200 1500 50  0001 C CNN
F 3 "" H 4200 1500 50  0001 C CNN
	1    4200 1500
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_DGND #PWR?
U 1 1 5F25ED3E
P 4800 2700
AR Path="/5E9C6910/5F25ED3E" Ref="#PWR?"  Part="1" 
AR Path="/5F05E355/5F25ED3E" Ref="#PWR?"  Part="1" 
AR Path="/5EA5DEB5/5F25ED3E" Ref="#PWR094"  Part="1" 
F 0 "#PWR094" H 4800 2450 50  0001 C CNN
F 1 "CTRL_DGND" H 4805 2527 50  0000 C CNN
F 2 "" H 4800 2700 50  0001 C CNN
F 3 "" H 4800 2700 50  0001 C CNN
	1    4800 2700
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_DGND #PWR?
U 1 1 5F2B2277
P 4800 5075
AR Path="/5E9C6910/5F2B2277" Ref="#PWR?"  Part="1" 
AR Path="/5F05E355/5F2B2277" Ref="#PWR?"  Part="1" 
AR Path="/5EA5DEB5/5F2B2277" Ref="#PWR095"  Part="1" 
F 0 "#PWR095" H 4800 4825 50  0001 C CNN
F 1 "CTRL_DGND" H 4805 4902 50  0000 C CNN
F 2 "" H 4800 5075 50  0001 C CNN
F 3 "" H 4800 5075 50  0001 C CNN
	1    4800 5075
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_DGND #PWR?
U 1 1 5F2CE06E
P 4200 3875
AR Path="/5E9C6910/5F2CE06E" Ref="#PWR?"  Part="1" 
AR Path="/5F05E355/5F2CE06E" Ref="#PWR?"  Part="1" 
AR Path="/5EA5DEB5/5F2CE06E" Ref="#PWR089"  Part="1" 
F 0 "#PWR089" H 4200 3625 50  0001 C CNN
F 1 "CTRL_DGND" H 4205 3702 50  0000 C CNN
F 2 "" H 4200 3875 50  0001 C CNN
F 3 "" H 4200 3875 50  0001 C CNN
	1    4200 3875
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_DGND #PWR?
U 1 1 5F305AD8
P 4200 6200
AR Path="/5E9C6910/5F305AD8" Ref="#PWR?"  Part="1" 
AR Path="/5F05E355/5F305AD8" Ref="#PWR?"  Part="1" 
AR Path="/5EA5DEB5/5F305AD8" Ref="#PWR090"  Part="1" 
F 0 "#PWR090" H 4200 5950 50  0001 C CNN
F 1 "CTRL_DGND" H 4205 6027 50  0000 C CNN
F 2 "" H 4200 6200 50  0001 C CNN
F 3 "" H 4200 6200 50  0001 C CNN
	1    4200 6200
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_DGND #PWR?
U 1 1 5F359172
P 4800 7200
AR Path="/5E9C6910/5F359172" Ref="#PWR?"  Part="1" 
AR Path="/5F05E355/5F359172" Ref="#PWR?"  Part="1" 
AR Path="/5EA5DEB5/5F359172" Ref="#PWR096"  Part="1" 
F 0 "#PWR096" H 4800 6950 50  0001 C CNN
F 1 "CTRL_DGND" H 4805 7027 50  0000 C CNN
F 2 "" H 4800 7200 50  0001 C CNN
F 3 "" H 4800 7200 50  0001 C CNN
	1    4800 7200
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_DGND #PWR?
U 1 1 5F390D76
P 7475 1600
AR Path="/5E9C6910/5F390D76" Ref="#PWR?"  Part="1" 
AR Path="/5F05E355/5F390D76" Ref="#PWR?"  Part="1" 
AR Path="/5EA5DEB5/5F390D76" Ref="#PWR0106"  Part="1" 
F 0 "#PWR0106" H 7475 1350 50  0001 C CNN
F 1 "CTRL_DGND" H 7480 1427 50  0000 C CNN
F 2 "" H 7475 1600 50  0001 C CNN
F 3 "" H 7475 1600 50  0001 C CNN
	1    7475 1600
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_DGND #PWR?
U 1 1 5F3C86AD
P 7875 2700
AR Path="/5E9C6910/5F3C86AD" Ref="#PWR?"  Part="1" 
AR Path="/5F05E355/5F3C86AD" Ref="#PWR?"  Part="1" 
AR Path="/5EA5DEB5/5F3C86AD" Ref="#PWR0110"  Part="1" 
F 0 "#PWR0110" H 7875 2450 50  0001 C CNN
F 1 "CTRL_DGND" H 7880 2527 50  0000 C CNN
F 2 "" H 7875 2700 50  0001 C CNN
F 3 "" H 7875 2700 50  0001 C CNN
	1    7875 2700
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_DGND #PWR?
U 1 1 5F4000D5
P 7475 3600
AR Path="/5E9C6910/5F4000D5" Ref="#PWR?"  Part="1" 
AR Path="/5F05E355/5F4000D5" Ref="#PWR?"  Part="1" 
AR Path="/5EA5DEB5/5F4000D5" Ref="#PWR0107"  Part="1" 
F 0 "#PWR0107" H 7475 3350 50  0001 C CNN
F 1 "CTRL_DGND" H 7480 3427 50  0000 C CNN
F 2 "" H 7475 3600 50  0001 C CNN
F 3 "" H 7475 3600 50  0001 C CNN
	1    7475 3600
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_DGND #PWR?
U 1 1 5F45374A
P 7875 4700
AR Path="/5E9C6910/5F45374A" Ref="#PWR?"  Part="1" 
AR Path="/5F05E355/5F45374A" Ref="#PWR?"  Part="1" 
AR Path="/5EA5DEB5/5F45374A" Ref="#PWR0112"  Part="1" 
F 0 "#PWR0112" H 7875 4450 50  0001 C CNN
F 1 "CTRL_DGND" H 7880 4527 50  0000 C CNN
F 2 "" H 7875 4700 50  0001 C CNN
F 3 "" H 7875 4700 50  0001 C CNN
	1    7875 4700
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_DGND #PWR?
U 1 1 5F4A7089
P 7925 6175
AR Path="/5E9C6910/5F4A7089" Ref="#PWR?"  Part="1" 
AR Path="/5F05E355/5F4A7089" Ref="#PWR?"  Part="1" 
AR Path="/5EA5DEB5/5F4A7089" Ref="#PWR0114"  Part="1" 
F 0 "#PWR0114" H 7925 5925 50  0001 C CNN
F 1 "CTRL_DGND" H 7930 6002 50  0000 C CNN
F 2 "" H 7925 6175 50  0001 C CNN
F 3 "" H 7925 6175 50  0001 C CNN
	1    7925 6175
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_DGND #PWR?
U 1 1 5F4C2EA1
P 7525 5475
AR Path="/5E9C6910/5F4C2EA1" Ref="#PWR?"  Part="1" 
AR Path="/5F05E355/5F4C2EA1" Ref="#PWR?"  Part="1" 
AR Path="/5EA5DEB5/5F4C2EA1" Ref="#PWR0108"  Part="1" 
F 0 "#PWR0108" H 7525 5225 50  0001 C CNN
F 1 "CTRL_DGND" H 7530 5302 50  0000 C CNN
F 2 "" H 7525 5475 50  0001 C CNN
F 3 "" H 7525 5475 50  0001 C CNN
	1    7525 5475
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5F180E15
P 9175 3550
AR Path="/5E9C6910/5F180E15" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5F180E15" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5F180E15" Ref="C?"  Part="1" 
AR Path="/5F05E355/5F180E15" Ref="C?"  Part="1" 
AR Path="/5EA5DEB5/5F180E15" Ref="C41"  Part="1" 
F 0 "C41" V 9300 3500 50  0000 L CNN
F 1 "100nF" V 9225 3275 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 9175 3550 50  0001 C CNN
F 3 "~" H 9175 3550 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 9175 3150 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 9175 3050 50  0001 C CNN "Distributor Link"
	1    9175 3550
	0    -1   -1   0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5F180E03
P 7675 3550
AR Path="/5E9C6910/5F180E03" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5F180E03" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5F180E03" Ref="C?"  Part="1" 
AR Path="/5F05E355/5F180E03" Ref="C?"  Part="1" 
AR Path="/5EA5DEB5/5F180E03" Ref="C37"  Part="1" 
F 0 "C37" V 7800 3500 50  0000 L CNN
F 1 "100nF" V 7725 3275 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 7675 3550 50  0001 C CNN
F 3 "~" H 7675 3550 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 7675 3150 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 7675 3050 50  0001 C CNN "Distributor Link"
	1    7675 3550
	0    1    -1   0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5F1516B4
P 9175 1550
AR Path="/5E9C6910/5F1516B4" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5F1516B4" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5F1516B4" Ref="C?"  Part="1" 
AR Path="/5F05E355/5F1516B4" Ref="C?"  Part="1" 
AR Path="/5EA5DEB5/5F1516B4" Ref="C40"  Part="1" 
F 0 "C40" V 9300 1500 50  0000 L CNN
F 1 "100nF" V 9225 1275 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 9175 1550 50  0001 C CNN
F 3 "~" H 9175 1550 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 9175 1150 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 9175 1050 50  0001 C CNN "Distributor Link"
	1    9175 1550
	0    -1   -1   0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5F15169C
P 7675 1550
AR Path="/5E9C6910/5F15169C" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5F15169C" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5F15169C" Ref="C?"  Part="1" 
AR Path="/5F05E355/5F15169C" Ref="C?"  Part="1" 
AR Path="/5EA5DEB5/5F15169C" Ref="C36"  Part="1" 
F 0 "C36" V 7800 1500 50  0000 L CNN
F 1 "100nF" V 7725 1275 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 7675 1550 50  0001 C CNN
F 3 "~" H 7675 1550 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 7675 1150 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 7675 1050 50  0001 C CNN "Distributor Link"
	1    7675 1550
	0    1    -1   0   
$EndComp
Text GLabel 6100 6600 2    50   Output ~ 0
ISO_SPI_MOSI
Text GLabel 6100 6500 2    50   Output ~ 0
ISO_SPI_SCK
Text GLabel 4600 6600 0    50   Input ~ 0
TR_SPI_MOSI
Text GLabel 4600 6500 0    50   Input ~ 0
TR_SPI_SCK
$Comp
L PMSM_Driver_LV_Board_Power:ISO_DI1_5V0 #PWR?
U 1 1 603693CC
P 5950 1075
AR Path="/5ED47D19/603693CC" Ref="#PWR?"  Part="1" 
AR Path="/5EA5DEB5/603693CC" Ref="#PWR0101"  Part="1" 
F 0 "#PWR0101" H 5950 1325 50  0001 C CNN
F 1 "ISO_DI1_5V0" H 5951 1248 50  0000 C CNN
F 2 "" H 5950 1075 50  0001 C CNN
F 3 "" H 5950 1075 50  0001 C CNN
	1    5950 1075
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:ISO_DI2_5V0 #PWR?
U 1 1 603693D2
P 8975 1450
AR Path="/5ED47D19/603693D2" Ref="#PWR?"  Part="1" 
AR Path="/5EA5DEB5/603693D2" Ref="#PWR0102"  Part="1" 
F 0 "#PWR0102" H 8975 1700 50  0001 C CNN
F 1 "ISO_DI2_5V0" H 8976 1623 50  0000 C CNN
F 2 "" H 8975 1450 50  0001 C CNN
F 3 "" H 8975 1450 50  0001 C CNN
	1    8975 1450
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:ISO_DI2_5V0 #PWR?
U 1 1 6049B634
P 8975 3450
AR Path="/5ED47D19/6049B634" Ref="#PWR?"  Part="1" 
AR Path="/5EA5DEB5/6049B634" Ref="#PWR0115"  Part="1" 
F 0 "#PWR0115" H 8975 3700 50  0001 C CNN
F 1 "ISO_DI2_5V0" H 8976 3623 50  0000 C CNN
F 2 "" H 8975 3450 50  0001 C CNN
F 3 "" H 8975 3450 50  0001 C CNN
	1    8975 3450
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:ISO_DI2_5V0 #PWR?
U 1 1 604B7B4E
P 2700 1450
AR Path="/5ED47D19/604B7B4E" Ref="#PWR?"  Part="1" 
AR Path="/5EA5DEB5/604B7B4E" Ref="#PWR0117"  Part="1" 
F 0 "#PWR0117" H 2700 1700 50  0001 C CNN
F 1 "ISO_DI2_5V0" H 2701 1623 50  0000 C CNN
F 2 "" H 2700 1450 50  0001 C CNN
F 3 "" H 2700 1450 50  0001 C CNN
	1    2700 1450
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:ISO_DI1_5V0 #PWR?
U 1 1 604EFC4F
P 2700 3700
AR Path="/5ED47D19/604EFC4F" Ref="#PWR?"  Part="1" 
AR Path="/5EA5DEB5/604EFC4F" Ref="#PWR0119"  Part="1" 
F 0 "#PWR0119" H 2700 3950 50  0001 C CNN
F 1 "ISO_DI1_5V0" H 2701 3873 50  0000 C CNN
F 2 "" H 2700 3700 50  0001 C CNN
F 3 "" H 2700 3700 50  0001 C CNN
	1    2700 3700
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:ISO_DI1_5V0 #PWR?
U 1 1 6050BAAA
P 2700 5950
AR Path="/5ED47D19/6050BAAA" Ref="#PWR?"  Part="1" 
AR Path="/5EA5DEB5/6050BAAA" Ref="#PWR0404"  Part="1" 
F 0 "#PWR0404" H 2700 6200 50  0001 C CNN
F 1 "ISO_DI1_5V0" H 2701 6123 50  0000 C CNN
F 2 "" H 2700 5950 50  0001 C CNN
F 3 "" H 2700 5950 50  0001 C CNN
	1    2700 5950
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:ISO_DI1_5V0 #PWR?
U 1 1 60527952
P 5950 3450
AR Path="/5ED47D19/60527952" Ref="#PWR?"  Part="1" 
AR Path="/5EA5DEB5/60527952" Ref="#PWR0419"  Part="1" 
F 0 "#PWR0419" H 5950 3700 50  0001 C CNN
F 1 "ISO_DI1_5V0" H 5951 3623 50  0000 C CNN
F 2 "" H 5950 3450 50  0001 C CNN
F 3 "" H 5950 3450 50  0001 C CNN
	1    5950 3450
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:ISO_DI1_5V0 #PWR?
U 1 1 605438DE
P 5950 5775
AR Path="/5ED47D19/605438DE" Ref="#PWR?"  Part="1" 
AR Path="/5EA5DEB5/605438DE" Ref="#PWR0507"  Part="1" 
F 0 "#PWR0507" H 5950 6025 50  0001 C CNN
F 1 "ISO_DI1_5V0" H 5951 5948 50  0000 C CNN
F 2 "" H 5950 5775 50  0001 C CNN
F 3 "" H 5950 5775 50  0001 C CNN
	1    5950 5775
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:ISO_DI1_5V0 #PWR?
U 1 1 6055F791
P 8925 5325
AR Path="/5ED47D19/6055F791" Ref="#PWR?"  Part="1" 
AR Path="/5EA5DEB5/6055F791" Ref="#PWR0508"  Part="1" 
F 0 "#PWR0508" H 8925 5575 50  0001 C CNN
F 1 "ISO_DI1_5V0" H 8926 5498 50  0000 C CNN
F 2 "" H 8925 5325 50  0001 C CNN
F 3 "" H 8925 5325 50  0001 C CNN
	1    8925 5325
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:ISO_DI1_5V0 #PWR?
U 1 1 6059A6CE
P 9775 5325
AR Path="/5ED47D19/6059A6CE" Ref="#PWR?"  Part="1" 
AR Path="/5EA5DEB5/6059A6CE" Ref="#PWR0124"  Part="1" 
F 0 "#PWR0124" H 9775 5575 50  0001 C CNN
F 1 "ISO_DI1_5V0" H 9776 5498 50  0000 C CNN
F 2 "" H 9775 5325 50  0001 C CNN
F 3 "" H 9775 5325 50  0001 C CNN
	1    9775 5325
	1    0    0    -1  
$EndComp
NoConn ~ 8875 2200
NoConn ~ 8875 2300
NoConn ~ 7975 2200
NoConn ~ 7975 2300
Wire Wire Line
	7725 4300 7975 4300
Wire Wire Line
	7725 4200 7975 4200
Text GLabel 7725 4300 0    50   Input ~ 0
TR_PWM_BL
Text GLabel 7725 4200 0    50   Input ~ 0
TR_PWM_BH
Wire Wire Line
	9125 4300 8875 4300
Wire Wire Line
	9125 4200 8875 4200
Text GLabel 9125 4300 2    50   Output ~ 0
ISO_PWM_BL
Text GLabel 9125 4200 2    50   Output ~ 0
ISO_PWM_BH
Text GLabel 7725 2100 0    50   Input ~ 0
TR_PWM_CL
Text GLabel 7725 2000 0    50   Input ~ 0
TR_PWM_CH
Text GLabel 9125 2100 2    50   Output ~ 0
ISO_PWM_CL
Text GLabel 9125 2000 2    50   Output ~ 0
ISO_PWM_CH
Text GLabel 9125 4100 2    50   Output ~ 0
ISO_PWM_AL
Text GLabel 9125 4000 2    50   Output ~ 0
ISO_PWM_AH
Text GLabel 7725 4100 0    50   Input ~ 0
TR_PWM_AL
Text GLabel 7725 4000 0    50   Input ~ 0
TR_PWM_AH
$EndSCHEMATC
