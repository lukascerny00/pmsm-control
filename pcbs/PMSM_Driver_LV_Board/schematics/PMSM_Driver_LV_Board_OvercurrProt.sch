EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 12 16
Title "PMSM_Driver_LV_Board"
Date "2020-03-30"
Rev "A"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L PMSM_Driver_LV_Board_Device:R_Shunt_US R?
U 1 1 5FE6FAC2
P 1300 1250
AR Path="/5EA41912/5FE6FAC2" Ref="R?"  Part="1" 
AR Path="/5FD7CF82/5FE6FAC2" Ref="R?"  Part="1" 
AR Path="/5EA5E295/5FE6FAC2" Ref="R181"  Part="1" 
F 0 "R181" V 1125 1350 50  0000 R CNN
F 1 "0.01" V 1200 1325 50  0000 R CNN
F 2 "PMSM_Driver_LV_Board:WSL3637R0100FEA" V 1230 1250 50  0001 C CNN
F 3 "~" H 1300 1250 50  0001 C CNN
F 4 "WSL3637R0100FEA -  SMD Current Sense Resistor, 0.01 ohm, WSL3637 Series, 3637 [9194 Metric], 3 W, ± 1%, 4-Terminal" H 1300 750 31  0001 C CNN "Description"
F 5 "https://uk.farnell.com/vishay/wsl3637r0100fea/current-sense-res-0r01-1-3-w-3637/dp/2395994" H 1300 650 31  0001 C CNN "Distributor Link"
	1    1300 1250
	0    -1   1    0   
$EndComp
Text GLabel 1450 1700 2    50   Output ~ 0
MOT_SHUNT_T
Wire Wire Line
	1400 1700 1450 1700
$Comp
L PMSM_Driver_LV_Board_Power:MOT_SENS_T_AGND #PWR0339
U 1 1 5FE8113B
P 1475 1950
F 0 "#PWR0339" H 1475 1700 50  0001 C CNN
F 1 "MOT_SENS_T_AGND" H 1480 1777 50  0000 C CNN
F 2 "" H 1475 1950 50  0001 C CNN
F 3 "" H 1475 1950 50  0001 C CNN
	1    1475 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	1200 1825 1475 1825
Text Notes 1500 1875 0    25   ~ 0
Measured voltage is +/- 0.25V at +/- 25A.\nFull scale is +/-32A.\n(See AD7403BRIZ datasheet.)
Text Notes 1150 1025 0    25   ~ 0
Total current shunt\nfor measuring \npower consumption
Wire Wire Line
	1200 1400 1200 1825
Wire Wire Line
	1400 1400 1400 1700
Text Notes 3475 1000 0    25   ~ 0
Enable MOT_VDC if overcurrent not detected.
Wire Wire Line
	1975 1550 1975 1400
Wire Wire Line
	2225 1550 1975 1550
Wire Wire Line
	2175 1450 2175 1400
Wire Wire Line
	2225 1450 2175 1450
Text GLabel 2225 1550 2    50   Output ~ 0
MOT_OVRC_SHUNT+
Text Notes 1925 1025 0    25   ~ 0
Total current shunt\nfor overcurrent\ndetection
Text GLabel 2225 1450 2    50   Output ~ 0
MOT_OVRC_SHUNT-
Connection ~ 3625 1700
Wire Wire Line
	3575 1700 3625 1700
Text GLabel 3575 1700 0    50   Input ~ 0
MOT_VDC_EN
Connection ~ 4025 1700
Connection ~ 3925 1700
Wire Wire Line
	3925 1700 4025 1700
Wire Wire Line
	3625 1700 3675 1700
Wire Wire Line
	3625 1900 3625 1700
Wire Wire Line
	4825 1000 4825 1250
Connection ~ 4825 1250
Connection ~ 4425 1250
Wire Wire Line
	4825 1250 4575 1250
Wire Wire Line
	4775 1700 4825 1700
Connection ~ 4425 1700
Wire Wire Line
	4425 1700 4575 1700
$Comp
L PMSM_Driver_LV_Board_Device:D_Small D?
U 1 1 608106EA
P 3775 1900
AR Path="/5EA41912/608106EA" Ref="D?"  Part="1" 
AR Path="/5EA90258/608106EA" Ref="D?"  Part="1" 
AR Path="/5FD7CF82/608106EA" Ref="D?"  Part="1" 
AR Path="/5EA5E295/608106EA" Ref="D31"  Part="1" 
F 0 "D31" H 3775 2000 50  0000 C CNN
F 1 "1N4148W-7-F" H 3975 1950 25  0000 C CNN
F 2 "PMSM_Driver_LV_Board:D_SOD-123" V 3775 1900 50  0001 C CNN
F 3 "~" V 3775 1900 50  0001 C CNN
F 4 "1N4148W-7-F -  Small Signal Diode, Single, 100 V, 300 mA, 1.25 V, 4 ns, 2 A" H 3775 1900 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/diodes-inc/1n4148w-7-f/diode-switch-300ma-100v-sod123/dp/1776392" H 3775 1900 50  0001 C CNN "Distributor Link"
	1    3775 1900
	1    0    0    1   
$EndComp
Wire Wire Line
	3625 1900 3675 1900
Wire Wire Line
	3925 1900 3875 1900
Wire Wire Line
	3925 1700 3925 1900
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 608106D2
P 4675 1700
AR Path="/5EA41912/608106D2" Ref="R?"  Part="1" 
AR Path="/5FD7CF82/608106D2" Ref="R?"  Part="1" 
AR Path="/5EA5E295/608106D2" Ref="R187"  Part="1" 
F 0 "R187" V 4750 1700 50  0000 C CNN
F 1 "100k" V 4575 1700 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 4675 1700 50  0001 C CNN
F 3 "~" H 4675 1700 50  0001 C CNN
F 4 "MCWR04X1003FTL -  SMD Chip Resistor, 0402 [1005 Metric], 100 kohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 4675 1300 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x1003ftl/res-100k-1-0-0625w-0402-thick/dp/2447094" H 4675 1200 50  0001 C CNN "Distributor Link"
	1    4675 1700
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3875 1700 3925 1700
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 608106C6
P 3775 1700
AR Path="/5EA41912/608106C6" Ref="R?"  Part="1" 
AR Path="/5FD7CF82/608106C6" Ref="R?"  Part="1" 
AR Path="/5EA5E295/608106C6" Ref="R184"  Part="1" 
F 0 "R184" V 3850 1725 50  0000 C CNN
F 1 "22" V 3725 1775 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 3775 1700 50  0001 C CNN
F 3 "~" H 3775 1700 50  0001 C CNN
F 4 "MCWR04X22R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 22 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 3775 1300 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x22r0ftl/res-22r-1-0-0625w-0402-thick-film/dp/2447138" H 3775 1200 50  0001 C CNN "Distributor Link"
	1    3775 1700
	0    1    -1   0   
$EndComp
Wire Wire Line
	4025 1700 4425 1700
Wire Wire Line
	4225 1250 4425 1250
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 6080BB3C
P 4425 1475
AR Path="/5E9C6910/6080BB3C" Ref="C?"  Part="1" 
AR Path="/5EBC6654/6080BB3C" Ref="C?"  Part="1" 
AR Path="/5EBC681B/6080BB3C" Ref="C?"  Part="1" 
AR Path="/5F05E355/6080BB3C" Ref="C?"  Part="1" 
AR Path="/5EA41912/6080BB3C" Ref="C?"  Part="1" 
AR Path="/5FD7CF82/6080BB3C" Ref="C?"  Part="1" 
AR Path="/5EA5E295/6080BB3C" Ref="C180"  Part="1" 
F 0 "C180" H 4450 1400 50  0000 L CNN
F 1 "1uF" H 4500 1475 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0805_2012Metric" H 4425 1475 50  0001 C CNN
F 3 "~" H 4425 1475 50  0001 C CNN
F 4 "08051C105K4T2A -  SMD Multilayer Ceramic Capacitor, 1 µF, 100 V, 0805 [2012 Metric], ± 10%, X7R" H 4425 1075 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/avx/08051c105k4t2a/cap-1-f-100v-10-x7r-0805/dp/2834935" H 4425 975 50  0001 C CNN "Distributor Link"
F 6 "DNI" H 4500 1550 50  0000 C CNN "DNI"
	1    4425 1475
	1    0    0    1   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_VDC #PWR0354
U 1 1 608042CE
P 4825 1000
F 0 "#PWR0354" H 4825 1250 50  0001 C CNN
F 1 "MOT_VDC" H 4826 1173 50  0000 C CNN
F 2 "" H 4825 1000 50  0001 C CNN
F 3 "" H 4825 1000 50  0001 C CNN
	1    4825 1000
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:R_Shunt_US R?
U 1 1 5FE71639
P 2075 1250
AR Path="/5EA41912/5FE71639" Ref="R?"  Part="1" 
AR Path="/5FD7CF82/5FE71639" Ref="R?"  Part="1" 
AR Path="/5EA5E295/5FE71639" Ref="R182"  Part="1" 
F 0 "R182" V 1900 1350 50  0000 R CNN
F 1 "0.005" V 1975 1350 50  0000 R CNN
F 2 "PMSM_Driver_LV_Board:WSL3637R0100FEA" V 2005 1250 50  0001 C CNN
F 3 "~" H 2075 1250 50  0001 C CNN
F 4 "WSL36375L000FEA -  CURRENT SENSE RESISTOR, 0.005 OHM, 3 W,1%" H 2075 750 31  0001 C CNN "Description"
F 5 "https://www.mouser.co.uk/ProductDetail/Vishay-Dale/WSL36375L000FEA?qs=sGAEpiMZZMtlleCFQhR%2FzZ%252BPyWemVGq4tLvR1Gj3Szo%3D" H 2075 650 31  0001 C CNN "Distributor Link"
	1    2075 1250
	0    -1   1    0   
$EndComp
Text Notes 675  800  0    100  ~ 0
Overcurrent and Inrush Current Protection
Wire Notes Line
	600  600  5100 600 
Wire Notes Line
	5100 2250 600  2250
Wire Wire Line
	1475 1825 1475 1950
Text GLabel 1500 2775 0    50   Output ~ 0
MOT_OVRC_SHUNT+
Text GLabel 1500 2875 0    50   Output ~ 0
MOT_OVRC_SHUNT-
$Comp
L PMSM_Driver_LV_Board_Power:MOT_5V0 #PWR?
U 1 1 60894BD4
P 2650 2800
AR Path="/5EFE605D/60894BD4" Ref="#PWR?"  Part="1" 
AR Path="/5EFE7331/60894BD4" Ref="#PWR?"  Part="1" 
AR Path="/5FD7CF82/60894BD4" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E295/60894BD4" Ref="#PWR0341"  Part="1" 
F 0 "#PWR0341" H 2650 3050 50  0001 C CNN
F 1 "MOT_5V0" H 2651 2973 50  0000 C CNN
F 2 "" H 2650 2800 50  0001 C CNN
F 3 "" H 2650 2800 50  0001 C CNN
	1    2650 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	2650 3225 2500 3225
Wire Wire Line
	2600 3025 2500 3025
$Comp
L PMSM_Driver_LV_Board_IC:AD8207WBRZ U42
U 1 1 607F785A
P 2100 3175
F 0 "U42" H 1850 3475 50  0000 C CNN
F 1 "AD8207WBRZ" H 2100 2875 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:SOIC-8_3.9x4.9mm_P1.27mm" H 2150 2725 31  0001 C CNN
F 3 "~" H 2100 3175 50  0001 C CNN
F 4 "AD8207WBRZ -  Differential Amplifier, Bidirectional, 1 Amplifiers, 100 µV, 150 MHz, -40 °C, 125 °C" H 2100 2675 31  0001 C CNN "Description"
F 5 "https://uk.farnell.com/analog-devices/ad8207wbrz/ic-amp-diff-hv-8soic/dp/1864673" H 2100 2625 31  0001 C CNN "Distributor Link"
F 6 "https://www.mouser.co.uk/ProductDetail/Analog-Devices/AD8207WBRZ?qs=sGAEpiMZZMvEn2pkGav3bNYdL9E%2FoSzJcUU3MSHK06o%3D" H 2100 2575 31  0001 C CNN "Distributor Link 2"
	1    2100 3175
	1    0    0    -1  
$EndComp
Wire Wire Line
	1500 2875 1600 2875
Wire Wire Line
	1600 2875 1600 3025
Wire Wire Line
	1600 3025 1700 3025
Wire Wire Line
	1500 2775 2600 2775
Wire Wire Line
	2600 2775 2600 3025
Wire Wire Line
	2650 2800 2650 2900
$Comp
L PMSM_Driver_LV_Board_Power:MOT_PGND #PWR0338
U 1 1 609A300F
P 1600 3625
F 0 "#PWR0338" H 1600 3375 50  0001 C CNN
F 1 "MOT_PGND" H 1605 3452 50  0000 C CNN
F 2 "" H 1600 3625 50  0001 C CNN
F 3 "" H 1600 3625 50  0001 C CNN
	1    1600 3625
	1    0    0    -1  
$EndComp
Wire Wire Line
	1600 3325 1700 3325
Wire Wire Line
	1600 3325 1600 3225
Wire Wire Line
	1600 3225 1700 3225
Connection ~ 1600 3325
Wire Wire Line
	1600 3225 1600 3125
Wire Wire Line
	1600 3125 1700 3125
Connection ~ 1600 3225
$Comp
L PMSM_Driver_LV_Board_Power:MOT_PGND #PWR0340
U 1 1 609D8932
P 2600 3625
F 0 "#PWR0340" H 2600 3375 50  0001 C CNN
F 1 "MOT_PGND" H 2605 3452 50  0000 C CNN
F 2 "" H 2600 3625 50  0001 C CNN
F 3 "" H 2600 3625 50  0001 C CNN
	1    2600 3625
	1    0    0    -1  
$EndComp
Wire Wire Line
	2600 3125 2500 3125
$Comp
L PMSM_Driver_LV_Board_Power:MOT_PGND #PWR0349
U 1 1 60A22041
P 3275 3000
F 0 "#PWR0349" H 3275 2750 50  0001 C CNN
F 1 "MOT_PGND" H 3280 2827 50  0000 C CNN
F 2 "" H 3275 3000 50  0001 C CNN
F 3 "" H 3275 3000 50  0001 C CNN
	1    3275 3000
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 609F7F12
P 2850 2900
AR Path="/5E9C6910/609F7F12" Ref="C?"  Part="1" 
AR Path="/5EBC6654/609F7F12" Ref="C?"  Part="1" 
AR Path="/5EBC681B/609F7F12" Ref="C?"  Part="1" 
AR Path="/5F05E355/609F7F12" Ref="C?"  Part="1" 
AR Path="/5FD7CF82/609F7F12" Ref="C?"  Part="1" 
AR Path="/5EA5E295/609F7F12" Ref="C174"  Part="1" 
F 0 "C174" V 2950 2800 50  0000 L CNN
F 1 "100nF" V 2725 2775 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 2850 2900 50  0001 C CNN
F 3 "~" H 2850 2900 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 2850 2500 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 2850 2400 50  0001 C CNN "Distributor Link"
	1    2850 2900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3275 2900 3275 3000
Wire Wire Line
	2750 2900 2650 2900
Connection ~ 2650 2900
Wire Wire Line
	2650 2900 2650 3225
Text GLabel 3100 3325 2    50   Output ~ 0
MOT_OVRC_AMPL
$Comp
L PMSM_Driver_LV_Board_IC:AD8468WBKSZ U43
U 1 1 60A97B70
P 2725 5100
F 0 "U43" H 2825 5275 50  0000 L CNN
F 1 "AD8468WBKSZ" H 2875 5000 25  0000 L CNN
F 2 "PMSM_Driver_LV_Board:SC70-6" H 2725 4450 31  0001 C CNN
F 3 "~" H 2175 5700 50  0001 C CNN
F 4 "AD8468WBKSZ -  Analogue Comparator, Rail to Rail, Low Power, 1 Comparator, 60 ns, 2.5V to 5.5V, SC-70, 6 Pins" H 2725 4400 31  0001 C CNN "Description"
F 5 "https://uk.farnell.com/analog-devices/ad8468wbksz/ic-comparator-ttl-cmos-sc70-6/dp/1864677" H 2725 4350 31  0001 C CNN "Distributor Link"
F 6 "https://www.mouser.co.uk/ProductDetail/Analog-Devices/AD8468WBKSZ-R7?qs=sGAEpiMZZMuayl%2FEk2kXcXratI4i45FvTyGpHTQUrPU%3D" H 2775 4300 31  0001 C CNN "Distributor Link 2"
	1    2725 5100
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_PGND #PWR0343
U 1 1 60AB7EF6
P 2725 5500
F 0 "#PWR0343" H 2725 5250 50  0001 C CNN
F 1 "MOT_PGND" H 2730 5327 50  0000 C CNN
F 2 "" H 2725 5500 50  0001 C CNN
F 3 "" H 2725 5500 50  0001 C CNN
	1    2725 5500
	1    0    0    -1  
$EndComp
Wire Wire Line
	2725 5500 2725 5400
$Comp
L PMSM_Driver_LV_Board_Power:MOT_5V0 #PWR?
U 1 1 60AC7568
P 3225 5450
AR Path="/5EFE605D/60AC7568" Ref="#PWR?"  Part="1" 
AR Path="/5EFE7331/60AC7568" Ref="#PWR?"  Part="1" 
AR Path="/5FD7CF82/60AC7568" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E295/60AC7568" Ref="#PWR0347"  Part="1" 
F 0 "#PWR0347" H 3225 5700 50  0001 C CNN
F 1 "MOT_5V0" H 3226 5623 50  0000 C CNN
F 2 "" H 3225 5450 50  0001 C CNN
F 3 "" H 3225 5450 50  0001 C CNN
	1    3225 5450
	1    0    0    -1  
$EndComp
Wire Wire Line
	2825 5400 2825 5500
Wire Wire Line
	3225 5500 3225 5450
Wire Wire Line
	2825 5500 3225 5500
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 60B36671
P 925 4850
AR Path="/5F05E355/60B36671" Ref="R?"  Part="1" 
AR Path="/6068445B/60B36671" Ref="R?"  Part="1" 
AR Path="/60E816A3/60B36671" Ref="R?"  Part="1" 
AR Path="/60EB5E32/60B36671" Ref="R?"  Part="1" 
AR Path="/5FD7CF82/60B36671" Ref="R?"  Part="1" 
AR Path="/5EA5E295/60B36671" Ref="R175"  Part="1" 
F 0 "R175" H 775 4850 50  0000 C CNN
F 1 "10k" H 825 4925 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 925 4850 50  0001 C CNN
F 3 "~" H 925 4850 50  0001 C CNN
F 4 "WF06R1002BTL -  SMD Chip Resistor, 0603 [1608 Metric], 10 kohm, WF06R Series, 75 V, Thin Film, 100 mW" H 925 4450 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/walsin/wf06r1002btl/res-10k-0-1-75v-0603-thin-film/dp/2502926" H 925 4350 50  0001 C CNN "Distributor Link"
	1    925  4850
	-1   0    0    1   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 60B57C65
P 2850 3325
AR Path="/5F05E355/60B57C65" Ref="R?"  Part="1" 
AR Path="/5E9C6910/60B57C65" Ref="R?"  Part="1" 
AR Path="/5EFE605D/60B57C65" Ref="R?"  Part="1" 
AR Path="/5FD7CF82/60B57C65" Ref="R?"  Part="1" 
AR Path="/5EA5E295/60B57C65" Ref="R183"  Part="1" 
F 0 "R183" V 2925 3325 50  0000 C CNN
F 1 "22" V 2750 3325 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 2850 3325 50  0001 C CNN
F 3 "~" H 2850 3325 50  0001 C CNN
F 4 "MCWR04X22R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 22 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 2850 2925 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x22r0ftl/res-22r-1-0-0625w-0402-thick-film/dp/2447138" H 2850 2825 50  0001 C CNN "Distributor Link"
	1    2850 3325
	0    -1   -1   0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 60B57C6D
P 3050 3475
AR Path="/5E9C6910/60B57C6D" Ref="C?"  Part="1" 
AR Path="/5EBC6654/60B57C6D" Ref="C?"  Part="1" 
AR Path="/5EBC681B/60B57C6D" Ref="C?"  Part="1" 
AR Path="/5F05E355/60B57C6D" Ref="C?"  Part="1" 
AR Path="/6068445B/60B57C6D" Ref="C?"  Part="1" 
AR Path="/60E816A3/60B57C6D" Ref="C?"  Part="1" 
AR Path="/5EFE605D/60B57C6D" Ref="C?"  Part="1" 
AR Path="/5FD7CF82/60B57C6D" Ref="C?"  Part="1" 
AR Path="/5EA5E295/60B57C6D" Ref="C175"  Part="1" 
F 0 "C175" H 2775 3475 50  0000 L CNN
F 1 "47pF" H 2850 3400 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 3050 3475 50  0001 C CNN
F 3 "~" H 3050 3475 50  0001 C CNN
F 4 "MC0402N470J500CT -  SMD Multilayer Ceramic Capacitor, 47 pF, 50 V, 0402 [1005 Metric], ± 5%, C0G / NP0, MC Series " H 3050 3075 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402n470j500ct/cap-47pf-50v-5-c0g-np0-0402/dp/1758959" H 3050 2975 50  0001 C CNN "Distributor Link"
	1    3050 3475
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2950 3325 3050 3325
Wire Wire Line
	3050 3325 3050 3375
Wire Wire Line
	2500 3325 2750 3325
$Comp
L PMSM_Driver_LV_Board_Power:MOT_PGND #PWR0346
U 1 1 60BB7B69
P 3050 3625
F 0 "#PWR0346" H 3050 3375 50  0001 C CNN
F 1 "MOT_PGND" H 3055 3452 50  0000 C CNN
F 2 "" H 3050 3625 50  0001 C CNN
F 3 "" H 3050 3625 50  0001 C CNN
	1    3050 3625
	1    0    0    -1  
$EndComp
Wire Wire Line
	3050 3625 3050 3575
Wire Wire Line
	1600 3325 1600 3625
Wire Wire Line
	2600 3125 2600 3625
Wire Wire Line
	3100 3325 3050 3325
Connection ~ 3050 3325
Wire Wire Line
	2950 2900 3275 2900
Text GLabel 2075 5200 0    50   Input ~ 0
MOT_OVRC_AMPL
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 60CBB9C2
P 1200 5150
AR Path="/5E9C6910/60CBB9C2" Ref="C?"  Part="1" 
AR Path="/5EBC6654/60CBB9C2" Ref="C?"  Part="1" 
AR Path="/5EBC681B/60CBB9C2" Ref="C?"  Part="1" 
AR Path="/5F05E355/60CBB9C2" Ref="C?"  Part="1" 
AR Path="/5EA41912/60CBB9C2" Ref="C?"  Part="1" 
AR Path="/5FD7CF82/60CBB9C2" Ref="C?"  Part="1" 
AR Path="/5EA5E295/60CBB9C2" Ref="C172"  Part="1" 
F 0 "C172" H 1200 5075 50  0000 L CNN
F 1 "1uF" H 1200 5225 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0805_2012Metric" H 1200 5150 50  0001 C CNN
F 3 "~" H 1200 5150 50  0001 C CNN
F 4 "08051C105K4T2A -  SMD Multilayer Ceramic Capacitor, 1 µF, 100 V, 0805 [2012 Metric], ± 10%, X7R" H 1200 4750 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/avx/08051c105k4t2a/cap-1-f-100v-10-x7r-0805/dp/2834935" H 1200 4650 50  0001 C CNN "Distributor Link"
	1    1200 5150
	1    0    0    1   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 60CD11F8
P 925 5150
AR Path="/5F05E355/60CD11F8" Ref="R?"  Part="1" 
AR Path="/6068445B/60CD11F8" Ref="R?"  Part="1" 
AR Path="/60E816A3/60CD11F8" Ref="R?"  Part="1" 
AR Path="/60EB5E32/60CD11F8" Ref="R?"  Part="1" 
AR Path="/5FD7CF82/60CD11F8" Ref="R?"  Part="1" 
AR Path="/5EA5E295/60CD11F8" Ref="R176"  Part="1" 
F 0 "R176" H 800 5075 50  0000 C CNN
F 1 "200" H 800 5150 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 925 5150 50  0001 C CNN
F 3 "~" H 925 5150 50  0001 C CNN
F 4 "MCWR06X2000FTL -  SMD Chip Resistor, 0603 [1608 Metric], 200 ohm, MCWR Series, 75 V, Thick Film, 100 mW" H 925 4750 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr06x2000ftl/res-200r-1-0-1w-thick-film/dp/2447292" H 925 4650 50  0001 C CNN "Distributor Link"
	1    925  5150
	-1   0    0    1   
$EndComp
Wire Wire Line
	925  5050 925  5000
Wire Wire Line
	925  5000 1200 5000
Wire Wire Line
	1200 5000 1200 5050
Connection ~ 925  5000
Wire Wire Line
	925  5000 925  4950
$Comp
L PMSM_Driver_LV_Board_Power:MOT_PGND #PWR0333
U 1 1 60D34898
P 925 5350
F 0 "#PWR0333" H 925 5100 50  0001 C CNN
F 1 "MOT_PGND" H 930 5177 50  0000 C CNN
F 2 "" H 925 5350 50  0001 C CNN
F 3 "" H 925 5350 50  0001 C CNN
	1    925  5350
	1    0    0    -1  
$EndComp
Wire Wire Line
	925  5250 925  5300
Wire Wire Line
	1200 5250 1200 5300
Wire Wire Line
	1200 5300 925  5300
Connection ~ 925  5300
Wire Wire Line
	925  5300 925  5350
$Comp
L PMSM_Driver_LV_Board_Power:MOT_5V0 #PWR?
U 1 1 60D79C98
P 925 4700
AR Path="/5EFE605D/60D79C98" Ref="#PWR?"  Part="1" 
AR Path="/5EFE7331/60D79C98" Ref="#PWR?"  Part="1" 
AR Path="/5FD7CF82/60D79C98" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E295/60D79C98" Ref="#PWR0332"  Part="1" 
F 0 "#PWR0332" H 925 4950 50  0001 C CNN
F 1 "MOT_5V0" H 926 4873 50  0000 C CNN
F 2 "" H 925 4700 50  0001 C CNN
F 3 "" H 925 4700 50  0001 C CNN
	1    925  4700
	1    0    0    -1  
$EndComp
Wire Wire Line
	925  4700 925  4750
Connection ~ 1200 5000
Text Notes 2350 5000 0    25   ~ 0
0.1V
Text Notes 2225 5200 0    25   ~ 0
0.1V at 1A
$Comp
L PMSM_Driver_LV_Board_Power:MOT_5V0 #PWR?
U 1 1 60DF90E4
P 2725 4475
AR Path="/5EFE605D/60DF90E4" Ref="#PWR?"  Part="1" 
AR Path="/5EFE7331/60DF90E4" Ref="#PWR?"  Part="1" 
AR Path="/5FD7CF82/60DF90E4" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E295/60DF90E4" Ref="#PWR0342"  Part="1" 
F 0 "#PWR0342" H 2725 4725 50  0001 C CNN
F 1 "MOT_5V0" H 2726 4648 50  0000 C CNN
F 2 "" H 2725 4475 50  0001 C CNN
F 3 "" H 2725 4475 50  0001 C CNN
	1    2725 4475
	1    0    0    -1  
$EndComp
Text GLabel 3175 5100 2    50   Output ~ 0
MOT_NOT_INR
Wire Wire Line
	3125 5100 3175 5100
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 60E69E51
P 3075 4525
AR Path="/5E9C6910/60E69E51" Ref="C?"  Part="1" 
AR Path="/5EBC6654/60E69E51" Ref="C?"  Part="1" 
AR Path="/5EBC681B/60E69E51" Ref="C?"  Part="1" 
AR Path="/5F05E355/60E69E51" Ref="C?"  Part="1" 
AR Path="/5EA41912/60E69E51" Ref="C?"  Part="1" 
AR Path="/5FD7CF82/60E69E51" Ref="C?"  Part="1" 
AR Path="/5EA5E295/60E69E51" Ref="C176"  Part="1" 
F 0 "C176" V 3125 4300 50  0000 L CNN
F 1 "1uF" V 3125 4550 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0805_2012Metric" H 3075 4525 50  0001 C CNN
F 3 "~" H 3075 4525 50  0001 C CNN
F 4 "08051C105K4T2A -  SMD Multilayer Ceramic Capacitor, 1 µF, 100 V, 0805 [2012 Metric], ± 10%, X7R" H 3075 4125 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/avx/08051c105k4t2a/cap-1-f-100v-10-x7r-0805/dp/2834935" H 3075 4025 50  0001 C CNN "Distributor Link"
	1    3075 4525
	0    1    -1   0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 60E7D1BB
P 3075 4750
AR Path="/5E9C6910/60E7D1BB" Ref="C?"  Part="1" 
AR Path="/5EBC6654/60E7D1BB" Ref="C?"  Part="1" 
AR Path="/5EBC681B/60E7D1BB" Ref="C?"  Part="1" 
AR Path="/5F05E355/60E7D1BB" Ref="C?"  Part="1" 
AR Path="/5FD7CF82/60E7D1BB" Ref="C?"  Part="1" 
AR Path="/5EA5E295/60E7D1BB" Ref="C177"  Part="1" 
F 0 "C177" V 3125 4775 50  0000 L CNN
F 1 "100nF" V 3125 4475 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 3075 4750 50  0001 C CNN
F 3 "~" H 3075 4750 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 3075 4350 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 3075 4250 50  0001 C CNN "Distributor Link"
	1    3075 4750
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2725 4475 2725 4525
Wire Wire Line
	2975 4525 2725 4525
Connection ~ 2725 4525
Wire Wire Line
	2725 4525 2725 4750
Wire Wire Line
	2975 4750 2725 4750
Connection ~ 2725 4750
Wire Wire Line
	2725 4750 2725 4800
$Comp
L PMSM_Driver_LV_Board_Power:MOT_PGND #PWR0350
U 1 1 60F4DADF
P 3400 4800
F 0 "#PWR0350" H 3400 4550 50  0001 C CNN
F 1 "MOT_PGND" H 3405 4627 50  0000 C CNN
F 2 "" H 3400 4800 50  0001 C CNN
F 3 "" H 3400 4800 50  0001 C CNN
	1    3400 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	3175 4750 3400 4750
Wire Wire Line
	3400 4750 3400 4800
Wire Wire Line
	3175 4525 3400 4525
Wire Wire Line
	3400 4525 3400 4750
Connection ~ 3400 4750
Text Notes 675  4200 0    100  ~ 0
Detect Inrush Current More than 1A
Wire Notes Line
	600  2325 3875 2325
Wire Notes Line
	600  3925 3875 3925
Wire Notes Line
	600  4000 3875 4000
Text Notes 675  2525 0    100  ~ 0
Amplify Voltage Accross Shunt
Wire Wire Line
	2075 5200 2425 5200
Wire Wire Line
	1200 5000 2425 5000
Wire Notes Line
	600  5800 3875 5800
Wire Notes Line
	3875 5800 3875 4000
Wire Notes Line
	600  5800 600  4000
$Comp
L PMSM_Driver_LV_Board_IC:AD8468WBKSZ U44
U 1 1 613C70F2
P 2725 6975
F 0 "U44" H 2825 7150 50  0000 L CNN
F 1 "AD8468WBKSZ" H 2875 6875 25  0000 L CNN
F 2 "PMSM_Driver_LV_Board:SC70-6" H 2725 6325 31  0001 C CNN
F 3 "~" H 2175 7575 50  0001 C CNN
F 4 "AD8468WBKSZ -  Analogue Comparator, Rail to Rail, Low Power, 1 Comparator, 60 ns, 2.5V to 5.5V, SC-70, 6 Pins" H 2725 6275 31  0001 C CNN "Description"
F 5 "https://uk.farnell.com/analog-devices/ad8468wbksz/ic-comparator-ttl-cmos-sc70-6/dp/1864677" H 2725 6225 31  0001 C CNN "Distributor Link"
F 6 "https://www.mouser.co.uk/ProductDetail/Analog-Devices/AD8468WBKSZ-R7?qs=sGAEpiMZZMuayl%2FEk2kXcXratI4i45FvTyGpHTQUrPU%3D" H 2775 6175 31  0001 C CNN "Distributor Link 2"
	1    2725 6975
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_PGND #PWR0345
U 1 1 613C70F8
P 2725 7375
F 0 "#PWR0345" H 2725 7125 50  0001 C CNN
F 1 "MOT_PGND" H 2730 7202 50  0000 C CNN
F 2 "" H 2725 7375 50  0001 C CNN
F 3 "" H 2725 7375 50  0001 C CNN
	1    2725 7375
	1    0    0    -1  
$EndComp
Wire Wire Line
	2725 7375 2725 7275
$Comp
L PMSM_Driver_LV_Board_Power:MOT_5V0 #PWR?
U 1 1 613C70FF
P 3225 7325
AR Path="/5EFE605D/613C70FF" Ref="#PWR?"  Part="1" 
AR Path="/5EFE7331/613C70FF" Ref="#PWR?"  Part="1" 
AR Path="/5FD7CF82/613C70FF" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E295/613C70FF" Ref="#PWR0348"  Part="1" 
F 0 "#PWR0348" H 3225 7575 50  0001 C CNN
F 1 "MOT_5V0" H 3226 7498 50  0000 C CNN
F 2 "" H 3225 7325 50  0001 C CNN
F 3 "" H 3225 7325 50  0001 C CNN
	1    3225 7325
	1    0    0    -1  
$EndComp
Wire Wire Line
	2825 7275 2825 7375
Wire Wire Line
	3225 7375 3225 7325
Wire Wire Line
	2825 7375 3225 7375
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 613C710A
P 925 6875
AR Path="/5F05E355/613C710A" Ref="R?"  Part="1" 
AR Path="/6068445B/613C710A" Ref="R?"  Part="1" 
AR Path="/60E816A3/613C710A" Ref="R?"  Part="1" 
AR Path="/60EB5E32/613C710A" Ref="R?"  Part="1" 
AR Path="/5FD7CF82/613C710A" Ref="R?"  Part="1" 
AR Path="/5EA5E295/613C710A" Ref="R177"  Part="1" 
F 0 "R177" H 775 6875 50  0000 C CNN
F 1 "5k6" H 825 6950 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 925 6875 50  0001 C CNN
F 3 "~" H 925 6875 50  0001 C CNN
F 4 "MCWR06X5601FTL -  SMD Chip Resistor, 0603 [1608 Metric], 5.6 kohm, MCWR Series, 75 V, Thick Film, 100 mW" H 925 6475 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr06x5601ftl/res-5k6-1-0-1w-thick-film/dp/2447405" H 925 6375 50  0001 C CNN "Distributor Link"
	1    925  6875
	-1   0    0    1   
$EndComp
Text GLabel 2075 6875 0    50   Input ~ 0
MOT_OVRC_AMPL
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 613C7113
P 1200 7175
AR Path="/5E9C6910/613C7113" Ref="C?"  Part="1" 
AR Path="/5EBC6654/613C7113" Ref="C?"  Part="1" 
AR Path="/5EBC681B/613C7113" Ref="C?"  Part="1" 
AR Path="/5F05E355/613C7113" Ref="C?"  Part="1" 
AR Path="/5EA41912/613C7113" Ref="C?"  Part="1" 
AR Path="/5FD7CF82/613C7113" Ref="C?"  Part="1" 
AR Path="/5EA5E295/613C7113" Ref="C173"  Part="1" 
F 0 "C173" H 1200 7100 50  0000 L CNN
F 1 "1uF" H 1225 7250 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0805_2012Metric" H 1200 7175 50  0001 C CNN
F 3 "~" H 1200 7175 50  0001 C CNN
F 4 "08051C105K4T2A -  SMD Multilayer Ceramic Capacitor, 1 µF, 100 V, 0805 [2012 Metric], ± 10%, X7R" H 1200 6775 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/avx/08051c105k4t2a/cap-1-f-100v-10-x7r-0805/dp/2834935" H 1200 6675 50  0001 C CNN "Distributor Link"
	1    1200 7175
	1    0    0    1   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 613C711B
P 925 7175
AR Path="/5F05E355/613C711B" Ref="R?"  Part="1" 
AR Path="/6068445B/613C711B" Ref="R?"  Part="1" 
AR Path="/60E816A3/613C711B" Ref="R?"  Part="1" 
AR Path="/60EB5E32/613C711B" Ref="R?"  Part="1" 
AR Path="/5FD7CF82/613C711B" Ref="R?"  Part="1" 
AR Path="/5EA5E295/613C711B" Ref="R178"  Part="1" 
F 0 "R178" H 800 7100 50  0000 C CNN
F 1 "4k3" H 825 7250 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 925 7175 50  0001 C CNN
F 3 "~" H 925 7175 50  0001 C CNN
F 4 "MCWR06X4301FTL -  SMD Chip Resistor, 0603 [1608 Metric], 4.3 kohm, MCWR Series, 75 V, Thick Film, 100 mW" H 925 6775 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr06x4301ftl/res-4k3-1-0-1w-thick-film/dp/2447383" H 925 6675 50  0001 C CNN "Distributor Link"
	1    925  7175
	-1   0    0    1   
$EndComp
Wire Wire Line
	925  7075 925  7025
Wire Wire Line
	925  7025 1200 7025
Wire Wire Line
	1200 7025 1200 7075
Connection ~ 925  7025
Wire Wire Line
	925  7025 925  6975
$Comp
L PMSM_Driver_LV_Board_Power:MOT_PGND #PWR0335
U 1 1 613C7126
P 925 7375
F 0 "#PWR0335" H 925 7125 50  0001 C CNN
F 1 "MOT_PGND" H 930 7202 50  0000 C CNN
F 2 "" H 925 7375 50  0001 C CNN
F 3 "" H 925 7375 50  0001 C CNN
	1    925  7375
	1    0    0    -1  
$EndComp
Wire Wire Line
	925  7275 925  7325
Wire Wire Line
	1200 7275 1200 7325
Wire Wire Line
	1200 7325 925  7325
Connection ~ 925  7325
Wire Wire Line
	925  7325 925  7375
$Comp
L PMSM_Driver_LV_Board_Power:MOT_5V0 #PWR?
U 1 1 613C7131
P 925 6725
AR Path="/5EFE605D/613C7131" Ref="#PWR?"  Part="1" 
AR Path="/5EFE7331/613C7131" Ref="#PWR?"  Part="1" 
AR Path="/5FD7CF82/613C7131" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E295/613C7131" Ref="#PWR0334"  Part="1" 
F 0 "#PWR0334" H 925 6975 50  0001 C CNN
F 1 "MOT_5V0" H 926 6898 50  0000 C CNN
F 2 "" H 925 6725 50  0001 C CNN
F 3 "" H 925 6725 50  0001 C CNN
	1    925  6725
	1    0    0    -1  
$EndComp
Wire Wire Line
	925  6725 925  6775
Text Notes 2300 7075 0    25   ~ 0
2.172V
Text Notes 2200 6875 0    25   ~ 0
2.2V at 22A
$Comp
L PMSM_Driver_LV_Board_Power:MOT_5V0 #PWR?
U 1 1 613C713B
P 2725 6350
AR Path="/5EFE605D/613C713B" Ref="#PWR?"  Part="1" 
AR Path="/5EFE7331/613C713B" Ref="#PWR?"  Part="1" 
AR Path="/5FD7CF82/613C713B" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E295/613C713B" Ref="#PWR0344"  Part="1" 
F 0 "#PWR0344" H 2725 6600 50  0001 C CNN
F 1 "MOT_5V0" H 2726 6523 50  0000 C CNN
F 2 "" H 2725 6350 50  0001 C CNN
F 3 "" H 2725 6350 50  0001 C CNN
	1    2725 6350
	1    0    0    -1  
$EndComp
Text GLabel 3175 6975 2    50   Output ~ 0
MOT_OVRC
Wire Wire Line
	3125 6975 3175 6975
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 613C7145
P 3075 6400
AR Path="/5E9C6910/613C7145" Ref="C?"  Part="1" 
AR Path="/5EBC6654/613C7145" Ref="C?"  Part="1" 
AR Path="/5EBC681B/613C7145" Ref="C?"  Part="1" 
AR Path="/5F05E355/613C7145" Ref="C?"  Part="1" 
AR Path="/5EA41912/613C7145" Ref="C?"  Part="1" 
AR Path="/5FD7CF82/613C7145" Ref="C?"  Part="1" 
AR Path="/5EA5E295/613C7145" Ref="C178"  Part="1" 
F 0 "C178" V 3125 6175 50  0000 L CNN
F 1 "1uF" V 3125 6425 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0805_2012Metric" H 3075 6400 50  0001 C CNN
F 3 "~" H 3075 6400 50  0001 C CNN
F 4 "08051C105K4T2A -  SMD Multilayer Ceramic Capacitor, 1 µF, 100 V, 0805 [2012 Metric], ± 10%, X7R" H 3075 6000 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/avx/08051c105k4t2a/cap-1-f-100v-10-x7r-0805/dp/2834935" H 3075 5900 50  0001 C CNN "Distributor Link"
	1    3075 6400
	0    1    -1   0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 613C714D
P 3075 6625
AR Path="/5E9C6910/613C714D" Ref="C?"  Part="1" 
AR Path="/5EBC6654/613C714D" Ref="C?"  Part="1" 
AR Path="/5EBC681B/613C714D" Ref="C?"  Part="1" 
AR Path="/5F05E355/613C714D" Ref="C?"  Part="1" 
AR Path="/5FD7CF82/613C714D" Ref="C?"  Part="1" 
AR Path="/5EA5E295/613C714D" Ref="C179"  Part="1" 
F 0 "C179" V 3125 6675 50  0000 L CNN
F 1 "100nF" V 3125 6350 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 3075 6625 50  0001 C CNN
F 3 "~" H 3075 6625 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 3075 6225 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 3075 6125 50  0001 C CNN "Distributor Link"
	1    3075 6625
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2725 6350 2725 6400
Wire Wire Line
	2975 6400 2725 6400
Connection ~ 2725 6400
Wire Wire Line
	2725 6400 2725 6625
Wire Wire Line
	2975 6625 2725 6625
Connection ~ 2725 6625
Wire Wire Line
	2725 6625 2725 6675
$Comp
L PMSM_Driver_LV_Board_Power:MOT_PGND #PWR0351
U 1 1 613C715A
P 3400 6675
F 0 "#PWR0351" H 3400 6425 50  0001 C CNN
F 1 "MOT_PGND" H 3405 6502 50  0000 C CNN
F 2 "" H 3400 6675 50  0001 C CNN
F 3 "" H 3400 6675 50  0001 C CNN
	1    3400 6675
	1    0    0    -1  
$EndComp
Wire Wire Line
	3175 6625 3400 6625
Wire Wire Line
	3400 6625 3400 6675
Wire Wire Line
	3175 6400 3400 6400
Wire Wire Line
	3400 6400 3400 6625
Connection ~ 3400 6625
Text Notes 675  6075 0    100  ~ 0
Detect Overcurrent More than 22A
Wire Notes Line
	600  5875 3875 5875
Wire Wire Line
	2075 6875 2425 6875
Wire Notes Line
	600  7675 3875 7675
Wire Notes Line
	3875 7675 3875 5875
Wire Notes Line
	600  7675 600  5875
Wire Notes Line
	5100 600  5100 2250
Wire Notes Line
	600  600  600  2250
Wire Notes Line
	3875 2325 3875 3925
Wire Notes Line
	600  2325 600  3925
Wire Wire Line
	1200 7025 1475 7025
Wire Wire Line
	1475 7025 1475 7075
Wire Wire Line
	1475 7075 2425 7075
Connection ~ 1200 7025
$Comp
L PMSM_Driver_LV_Board_Device:Q_NMOS_GSD Q14
U 1 1 617001B2
P 6550 1875
F 0 "Q14" H 6755 1921 50  0000 L CNN
F 1 "BSS138TA" H 6755 1830 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:SOT-23" H 6750 1975 50  0001 C CNN
F 3 "~" H 6550 1875 50  0001 C CNN
F 4 "BSS138TA -  Power MOSFET, N Channel, 50 V, 200 mA, 3.5 ohm, SOT-23, Surface Mount" H 6550 1375 31  0001 C CNN "Description"
F 5 "https://uk.farnell.com/diodes-inc/bss138ta/mosfet-n-50v-sot-23/dp/1471176" H 6550 1275 31  0001 C CNN "Distributor Link"
	1    6550 1875
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:B3S1000 S2
U 1 1 6173609E
P 6250 1425
F 0 "S2" H 6250 1600 50  0000 C CNN
F 1 "B3S1000" H 6250 1300 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:SW_SPST_B3S-1000" H 6250 1625 31  0001 C CNN
F 3 "~" H 6250 1625 31  0001 C CNN
F 4 "B3S-1000 -  Tactile Switch, B3S Series, Top Actuated, Surface Mount, Round Button, 160 gf, 50mA at 24VDC" H 6250 1225 31  0001 C CNN "Description"
F 5 "https://uk.farnell.com/omron/b3s-1000/switch-spno-0-05a-24v-smd/dp/177807" H 6250 1175 31  0001 C CNN "Distributor Link"
F 6 "https://www.mouser.co.uk/ProductDetail/Omron-Electronics/B3S-1000?qs=sGAEpiMZZMsgGjVA3toVBGvOmVUWX0m0fClzO%252BgxoEQ%3D" H 6250 1125 31  0001 C CNN "Distributor Link 2"
	1    6250 1425
	1    0    0    -1  
$EndComp
Wire Wire Line
	6500 1375 6550 1375
Wire Wire Line
	6500 1475 6550 1475
Wire Wire Line
	6550 1475 6550 1375
Connection ~ 6550 1375
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 617A19F8
P 6650 1225
AR Path="/5F05E355/617A19F8" Ref="R?"  Part="1" 
AR Path="/6068445B/617A19F8" Ref="R?"  Part="1" 
AR Path="/60E816A3/617A19F8" Ref="R?"  Part="1" 
AR Path="/60EB5E32/617A19F8" Ref="R?"  Part="1" 
AR Path="/5FD7CF82/617A19F8" Ref="R?"  Part="1" 
AR Path="/5EA5E295/617A19F8" Ref="R190"  Part="1" 
F 0 "R190" H 6500 1225 50  0000 C CNN
F 1 "10k" H 6550 1300 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 6650 1225 50  0001 C CNN
F 3 "~" H 6650 1225 50  0001 C CNN
F 4 "WF06R1002BTL -  SMD Chip Resistor, 0603 [1608 Metric], 10 kohm, WF06R Series, 75 V, Thin Film, 100 mW" H 6650 825 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/walsin/wf06r1002btl/res-10k-0-1-75v-0603-thin-film/dp/2502926" H 6650 725 50  0001 C CNN "Distributor Link"
	1    6650 1225
	-1   0    0    1   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_5V0 #PWR?
U 1 1 617A19FE
P 6650 1075
AR Path="/5EFE605D/617A19FE" Ref="#PWR?"  Part="1" 
AR Path="/5EFE7331/617A19FE" Ref="#PWR?"  Part="1" 
AR Path="/5FD7CF82/617A19FE" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E295/617A19FE" Ref="#PWR0364"  Part="1" 
F 0 "#PWR0364" H 6650 1325 50  0001 C CNN
F 1 "MOT_5V0" H 6651 1248 50  0000 C CNN
F 2 "" H 6650 1075 50  0001 C CNN
F 3 "" H 6650 1075 50  0001 C CNN
	1    6650 1075
	1    0    0    -1  
$EndComp
Wire Wire Line
	6650 1075 6650 1125
Wire Wire Line
	6650 1375 6650 1325
Wire Wire Line
	6550 1375 6650 1375
Connection ~ 6650 1375
Wire Wire Line
	6650 1525 6750 1525
Text GLabel 6750 1525 2    50   Output ~ 0
MOT_RESET_OVRC
$Comp
L PMSM_Driver_LV_Board_Power:MOT_PGND #PWR0359
U 1 1 61893A5E
P 5950 1525
F 0 "#PWR0359" H 5950 1275 50  0001 C CNN
F 1 "MOT_PGND" H 5955 1352 50  0000 C CNN
F 2 "" H 5950 1525 50  0001 C CNN
F 3 "" H 5950 1525 50  0001 C CNN
	1    5950 1525
	1    0    0    -1  
$EndComp
Wire Wire Line
	5950 1375 6000 1375
Wire Wire Line
	5950 1375 5950 1475
Wire Wire Line
	5950 1475 6000 1475
Connection ~ 5950 1475
Wire Wire Line
	5950 1475 5950 1525
Connection ~ 6650 1525
Wire Wire Line
	6650 1375 6650 1525
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 6193AEBD
P 6150 1875
AR Path="/5F05E355/6193AEBD" Ref="R?"  Part="1" 
AR Path="/5E9C6910/6193AEBD" Ref="R?"  Part="1" 
AR Path="/5EFE605D/6193AEBD" Ref="R?"  Part="1" 
AR Path="/5FD7CF82/6193AEBD" Ref="R?"  Part="1" 
AR Path="/5EA5E295/6193AEBD" Ref="R188"  Part="1" 
F 0 "R188" V 6225 1875 50  0000 C CNN
F 1 "22" V 6050 1875 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 6150 1875 50  0001 C CNN
F 3 "~" H 6150 1875 50  0001 C CNN
F 4 "MCWR04X22R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 22 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 6150 1475 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x22r0ftl/res-22r-1-0-0625w-0402-thick-film/dp/2447138" H 6150 1375 50  0001 C CNN "Distributor Link"
	1    6150 1875
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6650 1525 6650 1675
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 619C7B04
P 6300 2025
AR Path="/5EA41912/619C7B04" Ref="R?"  Part="1" 
AR Path="/5FD7CF82/619C7B04" Ref="R?"  Part="1" 
AR Path="/5EA5E295/619C7B04" Ref="R189"  Part="1" 
F 0 "R189" H 6150 2025 50  0000 C CNN
F 1 "100k" H 6175 2100 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 6300 2025 50  0001 C CNN
F 3 "~" H 6300 2025 50  0001 C CNN
F 4 "MCWR04X1003FTL -  SMD Chip Resistor, 0402 [1005 Metric], 100 kohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 6300 1625 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x1003ftl/res-100k-1-0-0625w-0402-thick/dp/2447094" H 6300 1525 50  0001 C CNN "Distributor Link"
	1    6300 2025
	-1   0    0    1   
$EndComp
Wire Wire Line
	6250 1875 6300 1875
Wire Wire Line
	6300 1925 6300 1875
Connection ~ 6300 1875
Wire Wire Line
	6300 1875 6350 1875
Wire Wire Line
	6300 2125 6300 2175
Wire Wire Line
	6300 2175 6650 2175
Wire Wire Line
	6650 2175 6650 2075
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR?
U 1 1 61A3C66E
P 6650 2225
AR Path="/5EA41912/61A3C66E" Ref="#PWR?"  Part="1" 
AR Path="/5FD7CF82/61A3C66E" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E295/61A3C66E" Ref="#PWR0365"  Part="1" 
F 0 "#PWR0365" H 6650 1975 50  0001 C CNN
F 1 "MOT_DGND" H 6655 2052 50  0000 C CNN
F 2 "" H 6650 2225 50  0001 C CNN
F 3 "" H 6650 2225 50  0001 C CNN
	1    6650 2225
	1    0    0    -1  
$EndComp
Wire Wire Line
	6650 2225 6650 2175
Connection ~ 6650 2175
Text GLabel 6000 1875 0    50   Output ~ 0
ISO_RST_OVR_CUR
Wire Wire Line
	6000 1875 6050 1875
Text Notes 7050 800  2    100  ~ 0
Overcurrent Fault Reset
Text Notes 7100 1475 0    25   ~ 0
Active low signal
$Comp
L PMSM_Driver_LV_Board_Device:PS-2206-L_NS S1
U 1 1 61B2B045
P 6000 3175
F 0 "S1" H 6000 3425 50  0000 C CNN
F 1 "PS-2206-L_NS" H 6000 2975 25  0000 C CNN
F 2 "PMSM_Driver_LV_Board:PS-2206-L_NS" H 6000 2775 31  0001 C CNN
F 3 "~" H 5950 3375 31  0001 C CNN
F 4 "PS-2206-L_NS  - Push Button" H 6000 2725 31  0001 C CNN "Description"
F 5 "https://www.mouser.co.uk/ProductDetail/CK/PS2206LNS?qs=sGAEpiMZZMsqIr59i2oRcn2iBufaGNTPMzcGsvhsvco%3D" H 6000 3175 50  0001 C CNN "Distributor Link"
	1    6000 3175
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR?
U 1 1 5EA77F48
P 5750 3325
AR Path="/5EA41912/5EA77F48" Ref="#PWR?"  Part="1" 
AR Path="/5FD7CF82/5EA77F48" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E295/5EA77F48" Ref="#PWR0358"  Part="1" 
F 0 "#PWR0358" H 5750 3075 50  0001 C CNN
F 1 "MOT_DGND" H 5755 3152 50  0000 C CNN
F 2 "" H 5750 3325 50  0001 C CNN
F 3 "" H 5750 3325 50  0001 C CNN
	1    5750 3325
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 3325 5750 3275
Wire Wire Line
	5800 3275 5750 3275
Wire Wire Line
	5750 3275 5750 3075
Wire Wire Line
	5750 3075 5800 3075
Connection ~ 5750 3275
NoConn ~ 6150 3025
NoConn ~ 6150 3225
Wire Wire Line
	6150 3125 6250 3125
Wire Wire Line
	6250 3125 6250 3325
Wire Wire Line
	6250 3325 6150 3325
Wire Wire Line
	6250 3125 6350 3125
Text GLabel 6350 3125 2    50   Output ~ 0
MOT_EMERGENCY_STOP
Text Notes 6925 3075 0    25   ~ 0
Active low signal
Connection ~ 6250 3125
Text Notes 7000 2775 2    100  ~ 0
Emergency Stop Switch
Text Notes 9475 800  2    100  ~ 0
Overcurrent Fault Latch
$Comp
L PMSM_Driver_LV_Board_IC:ADM1086AKSZ U47
U 1 1 5F1CD10D
P 9200 4425
F 0 "U47" H 8875 4675 50  0000 L CNN
F 1 "ADM1086AKSZ" H 9250 4175 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:SC70-6" H 9300 3725 50  0001 C CNN
F 3 "~" H 8550 5175 50  0001 C CNN
F 4 "ADM1086AKSZ-REEL7 -  Sequencing Circuit, 2.25 V to 3.6 Vsupp., 600 mV threshold/0.5 µs delay, Active-High, SC-70-6" H 9200 3625 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/analog-devices/adm1086aksz-reel7/supervisor-0-6v-sc-70-6/dp/2727558" H 9200 3525 50  0001 C CNN "Distributor Link"
F 6 "https://www.mouser.co.uk/ProductDetail/Analog-Devices/ADM1086AKSZ-REEL7" H 9200 3425 50  0001 C CNN "Distributor Link 2"
	1    9200 4425
	1    0    0    -1  
$EndComp
Wire Wire Line
	9200 3925 9300 3925
Wire Wire Line
	9600 3925 9600 3975
Wire Wire Line
	9500 3925 9600 3925
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5F26910F
P 9400 3925
AR Path="/5E9C6910/5F26910F" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5F26910F" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5F26910F" Ref="C?"  Part="1" 
AR Path="/5F05E355/5F26910F" Ref="C?"  Part="1" 
AR Path="/60EB5E32/5F26910F" Ref="C?"  Part="1" 
AR Path="/5FD7CF82/5F26910F" Ref="C?"  Part="1" 
AR Path="/5EA5E295/5F26910F" Ref="C189"  Part="1" 
F 0 "C189" V 9525 3825 50  0000 L CNN
F 1 "100nF" V 9450 3650 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 9400 3925 50  0001 C CNN
F 3 "~" H 9400 3925 50  0001 C CNN
F 4 "MC0402X104K250CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 25 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 9400 3525 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k250ct/cap-0-1-f-25v-10-x5r-0402/dp/2320760" H 9400 3425 50  0001 C CNN "Distributor Link"
	1    9400 3925
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9200 3825 9200 3925
Wire Wire Line
	9200 4125 9200 3925
Connection ~ 9200 3925
$Comp
L PMSM_Driver_LV_Board_Power:MOT_5V0 #PWR?
U 1 1 5F26911E
P 9200 3825
AR Path="/5EFE605D/5F26911E" Ref="#PWR?"  Part="1" 
AR Path="/5EFE7331/5F26911E" Ref="#PWR?"  Part="1" 
AR Path="/5FD7CF82/5F26911E" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E295/5F26911E" Ref="#PWR0373"  Part="1" 
F 0 "#PWR0373" H 9200 4075 50  0001 C CNN
F 1 "MOT_5V0" H 9201 3998 50  0000 C CNN
F 2 "" H 9200 3825 50  0001 C CNN
F 3 "" H 9200 3825 50  0001 C CNN
	1    9200 3825
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_PGND #PWR0378
U 1 1 5F2B85F3
P 9600 3975
F 0 "#PWR0378" H 9600 3725 50  0001 C CNN
F 1 "MOT_PGND" H 9605 3802 50  0000 C CNN
F 2 "" H 9600 3975 50  0001 C CNN
F 3 "" H 9600 3975 50  0001 C CNN
	1    9600 3975
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5F2E0B1E
P 8700 4675
AR Path="/5E9C6910/5F2E0B1E" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5F2E0B1E" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5F2E0B1E" Ref="C?"  Part="1" 
AR Path="/5F05E355/5F2E0B1E" Ref="C?"  Part="1" 
AR Path="/60EB5E32/5F2E0B1E" Ref="C?"  Part="1" 
AR Path="/5FD7CF82/5F2E0B1E" Ref="C?"  Part="1" 
AR Path="/5EA5E295/5F2E0B1E" Ref="C187"  Part="1" 
F 0 "C187" H 8775 4675 50  0000 L CNN
F 1 "100nF" H 8750 4600 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 8700 4675 50  0001 C CNN
F 3 "~" H 8700 4675 50  0001 C CNN
F 4 "MC0402X104K250CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 25 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 8700 4275 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k250ct/cap-0-1-f-25v-10-x5r-0402/dp/2320760" H 8700 4175 50  0001 C CNN "Distributor Link"
	1    8700 4675
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_PGND #PWR0367
U 1 1 5F37FDF4
P 8700 4825
F 0 "#PWR0367" H 8700 4575 50  0001 C CNN
F 1 "MOT_PGND" H 8705 4652 50  0000 C CNN
F 2 "" H 8700 4825 50  0001 C CNN
F 3 "" H 8700 4825 50  0001 C CNN
	1    8700 4825
	1    0    0    -1  
$EndComp
Wire Wire Line
	8700 4825 8700 4775
Wire Wire Line
	8700 4575 8700 4525
Wire Wire Line
	8700 4525 8750 4525
$Comp
L PMSM_Driver_LV_Board_Power:MOT_5V0 #PWR?
U 1 1 5F3D0DFE
P 8700 3825
AR Path="/5EFE605D/5F3D0DFE" Ref="#PWR?"  Part="1" 
AR Path="/5EFE7331/5F3D0DFE" Ref="#PWR?"  Part="1" 
AR Path="/5FD7CF82/5F3D0DFE" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E295/5F3D0DFE" Ref="#PWR0366"  Part="1" 
F 0 "#PWR0366" H 8700 4075 50  0001 C CNN
F 1 "MOT_5V0" H 8701 3998 50  0000 C CNN
F 2 "" H 8700 3825 50  0001 C CNN
F 3 "" H 8700 3825 50  0001 C CNN
	1    8700 3825
	1    0    0    -1  
$EndComp
Wire Wire Line
	8700 4425 8750 4425
Wire Wire Line
	8700 3825 8700 4425
Text GLabel 8650 4325 0    50   Input ~ 0
MOT_NOT_OVRC_LAT
Wire Wire Line
	8650 4325 8750 4325
Wire Wire Line
	9650 4425 9700 4425
Text Notes 7675 3575 0    100  ~ 0
Delay Overcurrent Fault Recovery
Text Notes 9875 4800 0    31   ~ 0
Rising edge of MOT_NOT_OVRC_LAT\nis delayed by 0.48s. Falling edge\nis without delay. Therefore, the reset\nof overcurrent fault is delayed.\nThe delay is also present during\npower on.
$Comp
L PMSM_Driver_LV_Board_IC:74AHC1G08GV,125 U49
U 1 1 5F644605
P 9800 5875
F 0 "U49" H 9575 6125 50  0000 L CNN
F 1 "74AHC1G08GV,125" H 9925 5650 25  0000 L CNN
F 2 "PMSM_Driver_LV_Board:74AHC1G08GV,125" H 9800 5275 31  0001 C CNN
F 3 "~" H 9900 6125 31  0001 C CNN
F 4 "74AHC1G08GV,125 - AND Gate, 74AHC1G08, 2 Input, 25 mA, 2 V to 5.5 V, SOT-23-5" H 9800 5225 31  0001 C CNN "Description"
F 5 "https://uk.farnell.com/nexperia/74ahc1g08gv-125/and-gate-2-i-p-sc-74a-5/dp/2438540" H 9800 5175 31  0001 C CNN "Distributor Link"
F 6 "https://cz.mouser.com/ProductDetail/Nexperia/74AHC1G08GV125" H 9800 5125 31  0001 C CNN "Distributor Link 2"
	1    9800 5875
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR?
U 1 1 5F645461
P 9800 6225
AR Path="/5EA41912/5F645461" Ref="#PWR?"  Part="1" 
AR Path="/5FD7CF82/5F645461" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E295/5F645461" Ref="#PWR0380"  Part="1" 
F 0 "#PWR0380" H 9800 5975 50  0001 C CNN
F 1 "MOT_DGND" H 9805 6052 50  0000 C CNN
F 2 "" H 9800 6225 50  0001 C CNN
F 3 "" H 9800 6225 50  0001 C CNN
	1    9800 6225
	1    0    0    -1  
$EndComp
Wire Wire Line
	9800 6225 9800 6175
Wire Wire Line
	9800 5475 9900 5475
Wire Wire Line
	10200 5475 10200 5525
Wire Wire Line
	10100 5475 10200 5475
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5F66EFCB
P 10000 5475
AR Path="/5E9C6910/5F66EFCB" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5F66EFCB" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5F66EFCB" Ref="C?"  Part="1" 
AR Path="/5F05E355/5F66EFCB" Ref="C?"  Part="1" 
AR Path="/60EB5E32/5F66EFCB" Ref="C?"  Part="1" 
AR Path="/5FD7CF82/5F66EFCB" Ref="C?"  Part="1" 
AR Path="/5EA5E295/5F66EFCB" Ref="C191"  Part="1" 
F 0 "C191" V 10125 5375 50  0000 L CNN
F 1 "100nF" V 10050 5200 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 10000 5475 50  0001 C CNN
F 3 "~" H 10000 5475 50  0001 C CNN
F 4 "MC0402X104K250CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 25 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 10000 5075 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k250ct/cap-0-1-f-25v-10-x5r-0402/dp/2320760" H 10000 4975 50  0001 C CNN "Distributor Link"
	1    10000 5475
	0    -1   -1   0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR?
U 1 1 5F66EFD4
P 10200 5525
AR Path="/5EA41912/5F66EFD4" Ref="#PWR?"  Part="1" 
AR Path="/5FD7CF82/5F66EFD4" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E295/5F66EFD4" Ref="#PWR0382"  Part="1" 
F 0 "#PWR0382" H 10200 5275 50  0001 C CNN
F 1 "MOT_DGND" H 10205 5352 50  0000 C CNN
F 2 "" H 10200 5525 50  0001 C CNN
F 3 "" H 10200 5525 50  0001 C CNN
	1    10200 5525
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_IC:ADG849YKSZ U46
U 1 1 5F745FB2
P 8450 5775
F 0 "U46" H 8275 6025 50  0000 C CNN
F 1 "ADG849YKSZ" H 8450 5525 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:SC70-6" H 8450 5425 31  0001 C CNN
F 3 "~" H 8600 4775 31  0001 C CNN
F 4 "ADG849YKSZ-REEL7 -  Analogue Switch, 1 Channels, SPDT, 1.1 ohm, 1.8V to 5.5V, SC-70, 6 Pins" H 8450 5375 31  0001 C CNN "Description"
F 5 "https://uk.farnell.com/analog-devices/adg849yksz-reel7/analogue-switch-1ch-spdt-sc-70/dp/2727330" H 8450 5325 31  0001 C CNN "Distributor Link"
F 6 "https://www.mouser.co.uk/ProductDetail/Analog-Devices/ADG849YKSZ-REEL?qs=sGAEpiMZZMv9Q1JI0Mo%2Ftchdbo4TVfN7" H 8450 5275 31  0001 C CNN "Distributor Link 2"
	1    8450 5775
	1    0    0    -1  
$EndComp
Text GLabel 8000 5675 0    50   Input ~ 0
MOT_NOT_OVRC_LAT_DEL
Text GLabel 8000 5775 0    50   Input ~ 0
MOT_NOT_INR
Text GLabel 8000 5875 0    50   Input ~ 0
MOT_NOT_OVRC_LAT
Wire Wire Line
	8900 5475 9000 5475
Wire Wire Line
	9300 5475 9300 5525
Wire Wire Line
	9200 5475 9300 5475
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5F87A289
P 9100 5475
AR Path="/5E9C6910/5F87A289" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5F87A289" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5F87A289" Ref="C?"  Part="1" 
AR Path="/5F05E355/5F87A289" Ref="C?"  Part="1" 
AR Path="/60EB5E32/5F87A289" Ref="C?"  Part="1" 
AR Path="/5FD7CF82/5F87A289" Ref="C?"  Part="1" 
AR Path="/5EA5E295/5F87A289" Ref="C188"  Part="1" 
F 0 "C188" V 9225 5375 50  0000 L CNN
F 1 "100nF" V 9150 5200 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 9100 5475 50  0001 C CNN
F 3 "~" H 9100 5475 50  0001 C CNN
F 4 "MC0402X104K250CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 25 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 9100 5075 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k250ct/cap-0-1-f-25v-10-x5r-0402/dp/2320760" H 9100 4975 50  0001 C CNN "Distributor Link"
	1    9100 5475
	0    -1   -1   0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_5V0 #PWR?
U 1 1 5F87A296
P 8900 5425
AR Path="/5EFE605D/5F87A296" Ref="#PWR?"  Part="1" 
AR Path="/5EFE7331/5F87A296" Ref="#PWR?"  Part="1" 
AR Path="/5FD7CF82/5F87A296" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E295/5F87A296" Ref="#PWR0371"  Part="1" 
F 0 "#PWR0371" H 8900 5675 50  0001 C CNN
F 1 "MOT_5V0" H 8901 5598 50  0000 C CNN
F 2 "" H 8900 5425 50  0001 C CNN
F 3 "" H 8900 5425 50  0001 C CNN
	1    8900 5425
	1    0    0    -1  
$EndComp
Wire Wire Line
	8900 5675 8800 5675
Wire Wire Line
	8800 5875 8850 5875
Wire Wire Line
	8850 5875 8850 6225
$Comp
L PMSM_Driver_LV_Board_Power:MOT_PGND #PWR0370
U 1 1 5FC2BFB9
P 8850 6225
F 0 "#PWR0370" H 8850 5975 50  0001 C CNN
F 1 "MOT_PGND" H 8855 6052 50  0000 C CNN
F 2 "" H 8850 6225 50  0001 C CNN
F 3 "" H 8850 6225 50  0001 C CNN
	1    8850 6225
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_PGND #PWR0375
U 1 1 5FC593B4
P 9300 5525
F 0 "#PWR0375" H 9300 5275 50  0001 C CNN
F 1 "MOT_PGND" H 9305 5352 50  0000 C CNN
F 2 "" H 9300 5525 50  0001 C CNN
F 3 "" H 9300 5525 50  0001 C CNN
	1    9300 5525
	1    0    0    -1  
$EndComp
Text GLabel 10200 5875 2    50   Output ~ 0
NO_FAULT_DETECTED
$Comp
L PMSM_Driver_LV_Board_IC:ADUM5230ARWZ U45
U 1 1 5FDCD75D
P 5550 6675
F 0 "U45" H 5325 7175 50  0000 C CNN
F 1 "ADUM5230ARWZ" H 5550 6175 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:SOIC-16W_7.5x10.3mm_P1.27mm" H 5450 6075 31  0001 C CNN
F 3 "~" H 4750 7325 31  0001 C CNN
F 4 "ADUM5230ARWZ -  MOSFET Driver Half Bridge, 4.5V-5.5V supply, 300mA peak out, 10 Ohm output, WSOIC-16" H 5450 6025 31  0001 C CNN "Description"
F 5 "https://uk.farnell.com/analog-devices/adum5230arwz/driver-mosfet-0-1a-0-3a-wsoic/dp/2102518" H 5450 5975 31  0001 C CNN "Distributor Link"
F 6 "https://www.mouser.co.uk/ProductDetail/Analog-Devices/ADUM5230ARWZ?qs=sGAEpiMZZMssyD0wnx%2FymCCDPdsQyP%2F2kCv%2FMKeptR0%3D" H 5450 5925 31  0001 C CNN "Distributor Link 2"
	1    5550 6675
	1    0    0    -1  
$EndComp
Wire Notes Line
	7525 2500 5175 2500
Wire Notes Line
	5175 600  7525 600 
Wire Notes Line
	7525 600  7525 2500
Wire Notes Line
	5175 600  5175 2500
Wire Notes Line
	7600 600  11100 600 
Wire Notes Line
	7600 3300 11100 3300
Wire Notes Line
	7600 600  7600 3300
Wire Notes Line
	11100 600  11100 3300
Wire Notes Line
	7600 3375 11100 3375
Wire Notes Line
	7600 5075 11100 5075
Wire Notes Line
	7600 3375 7600 5075
Wire Notes Line
	11100 3375 11100 5075
Wire Notes Line
	5175 2575 7525 2575
Wire Notes Line
	7525 2575 7525 3650
Wire Notes Line
	7525 3650 5175 3650
Wire Notes Line
	5175 3650 5175 2575
Wire Wire Line
	10150 5875 10200 5875
Wire Wire Line
	8900 5475 8900 5675
Wire Wire Line
	8900 5475 8900 5425
Connection ~ 8900 5475
Wire Wire Line
	9800 5475 9800 5575
$Comp
L PMSM_Driver_LV_Board_Power:MOT_5V0 #PWR?
U 1 1 5F66EFDA
P 9800 5425
AR Path="/5EFE605D/5F66EFDA" Ref="#PWR?"  Part="1" 
AR Path="/5EFE7331/5F66EFDA" Ref="#PWR?"  Part="1" 
AR Path="/5FD7CF82/5F66EFDA" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E295/5F66EFDA" Ref="#PWR0379"  Part="1" 
F 0 "#PWR0379" H 9800 5675 50  0001 C CNN
F 1 "MOT_5V0" H 9801 5598 50  0000 C CNN
F 2 "" H 9800 5425 50  0001 C CNN
F 3 "" H 9800 5425 50  0001 C CNN
	1    9800 5425
	1    0    0    -1  
$EndComp
Wire Wire Line
	9800 5475 9800 5425
Connection ~ 9800 5475
Wire Wire Line
	9200 4725 9200 4825
Wire Wire Line
	9500 5775 8800 5775
Wire Wire Line
	8000 5675 8100 5675
Wire Wire Line
	8000 5775 8100 5775
Wire Wire Line
	8000 5875 8050 5875
Wire Wire Line
	8050 5875 8050 6100
Wire Wire Line
	8050 6100 9400 6100
Wire Wire Line
	9400 6100 9400 5975
Wire Wire Line
	9400 5975 9500 5975
Connection ~ 8050 5875
Wire Wire Line
	8050 5875 8100 5875
Wire Notes Line
	11100 5150 11100 6475
Wire Notes Line
	11100 6475 6975 6475
Wire Notes Line
	6975 6475 6975 5150
Wire Notes Line
	6975 5150 11100 5150
Text Notes 7050 5350 0    100  ~ 0
Detect Fault
Text Notes 7075 5625 0    31   ~ 0
Inrush current of 1A is detected only \nduring the first 0.48s after power on\nor reset. MOT_VDC is not enabled\nuntil the inrush current is below 1A\nor until 0.48s elapsed.
Text GLabel 4950 6725 0    50   Input ~ 0
NO_FAULT_DETECTED
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR?
U 1 1 60FAFF9A
P 5050 7225
AR Path="/5EA41912/60FAFF9A" Ref="#PWR?"  Part="1" 
AR Path="/5FD7CF82/60FAFF9A" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E295/60FAFF9A" Ref="#PWR0356"  Part="1" 
F 0 "#PWR0356" H 5050 6975 50  0001 C CNN
F 1 "MOT_DGND" H 5055 7052 50  0000 C CNN
F 2 "" H 5050 7225 50  0001 C CNN
F 3 "" H 5050 7225 50  0001 C CNN
	1    5050 7225
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_PGND #PWR0361
U 1 1 60FAFFA0
P 6050 7225
F 0 "#PWR0361" H 6050 6975 50  0001 C CNN
F 1 "MOT_PGND" H 6055 7052 50  0000 C CNN
F 2 "" H 6050 7225 50  0001 C CNN
F 3 "" H 6050 7225 50  0001 C CNN
	1    6050 7225
	1    0    0    -1  
$EndComp
Wire Wire Line
	5050 7225 5050 7025
Wire Wire Line
	5050 7025 5150 7025
Wire Wire Line
	5150 6325 5050 6325
Wire Wire Line
	5050 6325 5050 6625
Connection ~ 5050 7025
Wire Wire Line
	5150 6625 5050 6625
Connection ~ 5050 6625
Wire Wire Line
	5050 6625 5050 6825
Wire Wire Line
	5150 6825 5050 6825
Connection ~ 5050 6825
Wire Wire Line
	5050 6825 5050 7025
Wire Wire Line
	6050 6825 5950 6825
NoConn ~ 5950 7025
NoConn ~ 5950 6725
NoConn ~ 5950 6625
Text GLabel 6100 6325 2    50   Output ~ 0
MOT_VDC_EN
Wire Wire Line
	5950 6325 6100 6325
$Comp
L PMSM_Driver_LV_Board_Power:MOT_5V0 #PWR?
U 1 1 612F1081
P 5000 5425
AR Path="/5EFE605D/612F1081" Ref="#PWR?"  Part="1" 
AR Path="/5EFE7331/612F1081" Ref="#PWR?"  Part="1" 
AR Path="/5FD7CF82/612F1081" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E295/612F1081" Ref="#PWR0355"  Part="1" 
F 0 "#PWR0355" H 5000 5675 50  0001 C CNN
F 1 "MOT_5V0" H 5001 5598 50  0000 C CNN
F 2 "" H 5000 5425 50  0001 C CNN
F 3 "" H 5000 5425 50  0001 C CNN
	1    5000 5425
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 6425 5150 6425
Wire Wire Line
	5000 6425 5000 6925
Wire Wire Line
	5000 6925 5150 6925
Wire Wire Line
	4950 6725 5150 6725
$Comp
L PMSM_Driver_LV_Board_Power:MOT_VDC #PWR0360
U 1 1 61488B92
P 6050 6200
F 0 "#PWR0360" H 6050 6450 50  0001 C CNN
F 1 "MOT_VDC" H 6051 6373 50  0000 C CNN
F 2 "" H 6050 6200 50  0001 C CNN
F 3 "" H 6050 6200 50  0001 C CNN
	1    6050 6200
	1    0    0    -1  
$EndComp
Wire Wire Line
	6050 6525 5950 6525
Wire Wire Line
	6050 6200 6050 6525
Wire Wire Line
	5950 6425 6200 6425
Wire Wire Line
	6050 6525 6050 6725
Wire Wire Line
	6050 6725 6200 6725
Connection ~ 6050 6525
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 615C59E6
P 4200 5925
AR Path="/5F05E355/615C59E6" Ref="R?"  Part="1" 
AR Path="/6068445B/615C59E6" Ref="R?"  Part="1" 
AR Path="/60E816A3/615C59E6" Ref="R?"  Part="1" 
AR Path="/60EB5E32/615C59E6" Ref="R?"  Part="1" 
AR Path="/5FD7CF82/615C59E6" Ref="R?"  Part="1" 
AR Path="/5EA5E295/615C59E6" Ref="R185"  Part="1" 
F 0 "R185" H 4050 5875 50  0000 C CNN
F 1 "3.65k" H 4050 5950 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4200 5925 50  0001 C CNN
F 3 "~" H 4200 5925 50  0001 C CNN
F 4 "MCWR06X3651FTL -  SMD Chip Resistor, 0603 [1608 Metric], 3.65 kohm, MCWR06 Series, 75 V, Thick Film, 100 mW" H 4200 5525 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr06x3651ftl/res-3k65-1-0-1w-0603-thick-film/dp/2694838" H 4200 5425 50  0001 C CNN "Distributor Link"
F 6 "DNI" H 4100 6025 50  0000 C CNN "DNI"
	1    4200 5925
	-1   0    0    1   
$EndComp
Wire Wire Line
	4200 6125 4200 6075
Connection ~ 4200 6075
Wire Wire Line
	4200 6075 4200 6025
$Comp
L PMSM_Driver_LV_Board_Power:MOT_5V0 #PWR?
U 1 1 615C5A02
P 4200 5775
AR Path="/5EFE605D/615C5A02" Ref="#PWR?"  Part="1" 
AR Path="/5EFE7331/615C5A02" Ref="#PWR?"  Part="1" 
AR Path="/5FD7CF82/615C5A02" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E295/615C5A02" Ref="#PWR0352"  Part="1" 
F 0 "#PWR0352" H 4200 6025 50  0001 C CNN
F 1 "MOT_5V0" H 4201 5948 50  0000 C CNN
F 2 "" H 4200 5775 50  0001 C CNN
F 3 "" H 4200 5775 50  0001 C CNN
	1    4200 5775
	1    0    0    -1  
$EndComp
Wire Wire Line
	4200 5775 4200 5825
Wire Wire Line
	4200 6325 4200 6425
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR?
U 1 1 616676AF
P 4200 6425
AR Path="/5EA41912/616676AF" Ref="#PWR?"  Part="1" 
AR Path="/5FD7CF82/616676AF" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E295/616676AF" Ref="#PWR0353"  Part="1" 
F 0 "#PWR0353" H 4200 6175 50  0001 C CNN
F 1 "MOT_DGND" H 4205 6252 50  0000 C CNN
F 2 "" H 4200 6425 50  0001 C CNN
F 3 "" H 4200 6425 50  0001 C CNN
	1    4200 6425
	1    0    0    -1  
$EndComp
Wire Wire Line
	4200 6075 4475 6075
Wire Wire Line
	4475 6075 4475 6525
Wire Wire Line
	4475 6525 5150 6525
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 61743710
P 4200 6225
AR Path="/5F05E355/61743710" Ref="R?"  Part="1" 
AR Path="/6068445B/61743710" Ref="R?"  Part="1" 
AR Path="/60E816A3/61743710" Ref="R?"  Part="1" 
AR Path="/60EB5E32/61743710" Ref="R?"  Part="1" 
AR Path="/5FD7CF82/61743710" Ref="R?"  Part="1" 
AR Path="/5EA5E295/61743710" Ref="R186"  Part="1" 
F 0 "R186" H 4050 6175 50  0000 C CNN
F 1 "3.65k" H 4050 6250 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4200 6225 50  0001 C CNN
F 3 "~" H 4200 6225 50  0001 C CNN
F 4 "MCWR06X3651FTL -  SMD Chip Resistor, 0603 [1608 Metric], 3.65 kohm, MCWR06 Series, 75 V, Thick Film, 100 mW" H 4200 5825 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr06x3651ftl/res-3k65-1-0-1w-0603-thick-film/dp/2694838" H 4200 5725 50  0001 C CNN "Distributor Link"
F 6 "DNI" H 4100 6325 50  0000 C CNN "DNI"
	1    4200 6225
	-1   0    0    1   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 6177EA74
P 5100 5625
AR Path="/5E9C6910/6177EA74" Ref="C?"  Part="1" 
AR Path="/5EBC6654/6177EA74" Ref="C?"  Part="1" 
AR Path="/5EBC681B/6177EA74" Ref="C?"  Part="1" 
AR Path="/5F05E355/6177EA74" Ref="C?"  Part="1" 
AR Path="/6068445B/6177EA74" Ref="C?"  Part="1" 
AR Path="/60E816A3/6177EA74" Ref="C?"  Part="1" 
AR Path="/5EFE605D/6177EA74" Ref="C?"  Part="1" 
AR Path="/5EFE7331/6177EA74" Ref="C?"  Part="1" 
AR Path="/5FD7CF82/6177EA74" Ref="C?"  Part="1" 
AR Path="/5EA5E295/6177EA74" Ref="C181"  Part="1" 
F 0 "C181" H 4900 5700 50  0000 L CNN
F 1 "10uF" H 4900 5550 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 5100 5625 50  0001 C CNN
F 3 "~" H 5100 5625 50  0001 C CNN
F 4 "LMK107BBJ106MALT -  SMD Multilayer Ceramic Capacitor, 10 µF, 10 V, 0603 [1608 Metric], ± 20%, X5R, M Series" H 5100 5225 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/taiyo-yuden/lmk107bbj106malt/cap-10-f-10v-20-x5r-0603/dp/2113070" H 5100 5125 50  0001 C CNN "Distributor Link"
	1    5100 5625
	-1   0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 617B6CB2
P 5450 5625
AR Path="/5E9C6910/617B6CB2" Ref="C?"  Part="1" 
AR Path="/5EBC6654/617B6CB2" Ref="C?"  Part="1" 
AR Path="/5EBC681B/617B6CB2" Ref="C?"  Part="1" 
AR Path="/5F05E355/617B6CB2" Ref="C?"  Part="1" 
AR Path="/60EB5E32/617B6CB2" Ref="C?"  Part="1" 
AR Path="/5FD7CF82/617B6CB2" Ref="C?"  Part="1" 
AR Path="/5EA5E295/617B6CB2" Ref="C182"  Part="1" 
F 0 "C182" H 5450 5700 50  0000 L CNN
F 1 "100nF" H 5450 5550 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 5450 5625 50  0001 C CNN
F 3 "~" H 5450 5625 50  0001 C CNN
F 4 "MC0402X104K250CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 25 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 5450 5225 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k250ct/cap-0-1-f-25v-10-x5r-0402/dp/2320760" H 5450 5125 50  0001 C CNN "Distributor Link"
	1    5450 5625
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 5425 5000 5475
Wire Wire Line
	5100 5525 5100 5475
Wire Wire Line
	5100 5475 5000 5475
Connection ~ 5000 5475
Wire Wire Line
	5450 5525 5450 5475
Wire Wire Line
	5450 5475 5100 5475
Connection ~ 5100 5475
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR?
U 1 1 61A140F0
P 5450 5825
AR Path="/5EA41912/61A140F0" Ref="#PWR?"  Part="1" 
AR Path="/5FD7CF82/61A140F0" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E295/61A140F0" Ref="#PWR0357"  Part="1" 
F 0 "#PWR0357" H 5450 5575 50  0001 C CNN
F 1 "MOT_DGND" H 5455 5652 50  0000 C CNN
F 2 "" H 5450 5825 50  0001 C CNN
F 3 "" H 5450 5825 50  0001 C CNN
	1    5450 5825
	1    0    0    -1  
$EndComp
Wire Wire Line
	5450 5825 5450 5775
Wire Wire Line
	5450 5775 5100 5775
Wire Wire Line
	5100 5775 5100 5725
Connection ~ 5450 5775
Wire Wire Line
	5450 5775 5450 5725
Wire Wire Line
	5000 5475 5000 6425
Connection ~ 5000 6425
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 61BDE6D5
P 6550 6575
AR Path="/5E9C6910/61BDE6D5" Ref="C?"  Part="1" 
AR Path="/5EBC6654/61BDE6D5" Ref="C?"  Part="1" 
AR Path="/5EBC681B/61BDE6D5" Ref="C?"  Part="1" 
AR Path="/5F05E355/61BDE6D5" Ref="C?"  Part="1" 
AR Path="/60EB5E32/61BDE6D5" Ref="C?"  Part="1" 
AR Path="/5FD7CF82/61BDE6D5" Ref="C?"  Part="1" 
AR Path="/5EA5E295/61BDE6D5" Ref="C185"  Part="1" 
F 0 "C185" H 6575 6650 50  0000 L CNN
F 1 "100nF" H 6550 6500 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 6550 6575 50  0001 C CNN
F 3 "~" H 6550 6575 50  0001 C CNN
F 4 "MC0402X104K250CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 25 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 6550 6175 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k250ct/cap-0-1-f-25v-10-x5r-0402/dp/2320760" H 6550 6075 50  0001 C CNN "Distributor Link"
	1    6550 6575
	1    0    0    -1  
$EndComp
Wire Wire Line
	6550 6475 6550 6425
Wire Wire Line
	6550 6725 6550 6675
Wire Wire Line
	6200 6475 6200 6425
Connection ~ 6200 6425
Wire Wire Line
	6200 6425 6550 6425
Wire Wire Line
	6200 6675 6200 6725
Connection ~ 6200 6725
Wire Wire Line
	6200 6725 6550 6725
Text Notes 5300 7350 0    25   ~ 0
Isolated half-bridge gate\ndriver with internal\nhigh-side power supply
$Comp
L PMSM_Driver_LV_Board_Power:MOT_15V0 #PWR0362
U 1 1 61D7D1ED
P 6625 6975
F 0 "#PWR0362" H 6625 7225 50  0001 C CNN
F 1 "MOT_15V0" H 6626 7148 50  0000 C CNN
F 2 "" H 6625 6975 50  0001 C CNN
F 3 "" H 6625 6975 50  0001 C CNN
	1    6625 6975
	1    0    0    -1  
$EndComp
Wire Wire Line
	6625 6975 6625 7025
Wire Wire Line
	6625 7025 6275 7025
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 61DBF7EC
P 6625 7175
AR Path="/5E9C6910/61DBF7EC" Ref="C?"  Part="1" 
AR Path="/5EBC6654/61DBF7EC" Ref="C?"  Part="1" 
AR Path="/5EBC681B/61DBF7EC" Ref="C?"  Part="1" 
AR Path="/5F05E355/61DBF7EC" Ref="C?"  Part="1" 
AR Path="/60EB5E32/61DBF7EC" Ref="C?"  Part="1" 
AR Path="/5FD7CF82/61DBF7EC" Ref="C?"  Part="1" 
AR Path="/5EA5E295/61DBF7EC" Ref="C186"  Part="1" 
F 0 "C186" H 6625 7250 50  0000 L CNN
F 1 "100nF" H 6625 7100 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 6625 7175 50  0001 C CNN
F 3 "~" H 6625 7175 50  0001 C CNN
F 4 "MC0402X104K250CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 25 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 6625 6775 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k250ct/cap-0-1-f-25v-10-x5r-0402/dp/2320760" H 6625 6675 50  0001 C CNN "Distributor Link"
	1    6625 7175
	1    0    0    -1  
$EndComp
Wire Wire Line
	6275 7075 6275 7025
Wire Wire Line
	6625 7075 6625 7025
Wire Wire Line
	6625 7375 6625 7325
Wire Wire Line
	6625 7325 6275 7325
Wire Wire Line
	6275 7325 6275 7275
Connection ~ 6625 7325
Wire Wire Line
	6625 7325 6625 7275
Connection ~ 6625 7025
Wire Wire Line
	6275 7025 6275 6925
Connection ~ 6275 7025
Wire Wire Line
	6050 6825 6050 7225
Wire Wire Line
	6275 6925 5950 6925
$Comp
L PMSM_Driver_LV_Board_Power:MOT_PGND #PWR0363
U 1 1 62023A72
P 6625 7375
F 0 "#PWR0363" H 6625 7125 50  0001 C CNN
F 1 "MOT_PGND" H 6630 7202 50  0000 C CNN
F 2 "" H 6625 7375 50  0001 C CNN
F 3 "" H 6625 7375 50  0001 C CNN
	1    6625 7375
	1    0    0    -1  
$EndComp
Wire Notes Line
	3950 7675 6900 7675
Wire Notes Line
	6900 4975 3950 4975
Wire Notes Line
	6900 4975 6900 7675
Wire Notes Line
	3950 4975 3950 7675
Text Notes 4025 5175 0    100  ~ 0
MOT_VDC_EN Gate Driver
$Comp
L PMSM_Driver_LV_Board_Power:MOT_PGND #PWR0374
U 1 1 5F1CDB45
P 9200 4825
F 0 "#PWR0374" H 9200 4575 50  0001 C CNN
F 1 "MOT_PGND" H 9205 4652 50  0000 C CNN
F 2 "" H 9200 4825 50  0001 C CNN
F 3 "" H 9200 4825 50  0001 C CNN
	1    9200 4825
	1    0    0    -1  
$EndComp
Text GLabel 9700 4425 2    50   Output ~ 0
MOT_NOT_OVRC_LAT_DEL
Wire Wire Line
	9000 1700 9200 1700
Text GLabel 9000 1700 0    50   Output ~ 0
MOT_RESET_OVRC
Wire Wire Line
	10250 1750 10275 1750
Text GLabel 10300 1750 2    50   Output ~ 0
MOT_NOT_OVRC_LAT
Wire Wire Line
	9550 3050 9550 3000
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR?
U 1 1 5EE4CDC2
P 9550 3050
AR Path="/5EA41912/5EE4CDC2" Ref="#PWR?"  Part="1" 
AR Path="/5FD7CF82/5EE4CDC2" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E295/5EE4CDC2" Ref="#PWR0377"  Part="1" 
F 0 "#PWR0377" H 9550 2800 50  0001 C CNN
F 1 "MOT_DGND" H 9555 2877 50  0000 C CNN
F 2 "" H 9550 3050 50  0001 C CNN
F 3 "" H 9550 3050 50  0001 C CNN
	1    9550 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	9100 1950 9100 2050
Connection ~ 9100 1950
Wire Wire Line
	9200 1950 9100 1950
Wire Wire Line
	9100 2050 9100 2200
Connection ~ 9100 2050
Wire Wire Line
	9200 2050 9100 2050
Wire Wire Line
	9100 2200 9100 2300
Connection ~ 9100 2200
Wire Wire Line
	9200 2200 9100 2200
Wire Wire Line
	9100 2300 9100 2450
Connection ~ 9100 2300
Wire Wire Line
	9200 2300 9100 2300
Connection ~ 9100 2550
Wire Wire Line
	9100 2700 9100 2550
Wire Wire Line
	9200 2700 9100 2700
Wire Wire Line
	9100 2450 9100 2550
Connection ~ 9100 2450
Wire Wire Line
	9200 2450 9100 2450
Wire Wire Line
	9100 1050 9100 1950
$Comp
L PMSM_Driver_LV_Board_Power:MOT_5V0 #PWR?
U 1 1 5ED40061
P 9100 1050
AR Path="/5EFE605D/5ED40061" Ref="#PWR?"  Part="1" 
AR Path="/5EFE7331/5ED40061" Ref="#PWR?"  Part="1" 
AR Path="/5FD7CF82/5ED40061" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E295/5ED40061" Ref="#PWR0372"  Part="1" 
F 0 "#PWR0372" H 9100 1300 50  0001 C CNN
F 1 "MOT_5V0" H 9101 1223 50  0000 C CNN
F 2 "" H 9100 1050 50  0001 C CNN
F 3 "" H 9100 1050 50  0001 C CNN
	1    9100 1050
	1    0    0    -1  
$EndComp
Wire Wire Line
	9200 2550 9100 2550
NoConn ~ 9700 3000
NoConn ~ 10250 2500
NoConn ~ 10250 2250
NoConn ~ 10250 2000
$Comp
L PMSM_Driver_LV_Board_Power:MOT_5V0 #PWR?
U 1 1 5EC80AB5
P 9550 1050
AR Path="/5EFE605D/5EC80AB5" Ref="#PWR?"  Part="1" 
AR Path="/5EFE7331/5EC80AB5" Ref="#PWR?"  Part="1" 
AR Path="/5FD7CF82/5EC80AB5" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E295/5EC80AB5" Ref="#PWR0376"  Part="1" 
F 0 "#PWR0376" H 9550 1300 50  0001 C CNN
F 1 "MOT_5V0" H 9551 1223 50  0000 C CNN
F 2 "" H 9550 1050 50  0001 C CNN
F 3 "" H 9550 1050 50  0001 C CNN
	1    9550 1050
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR?
U 1 1 5EC60F54
P 9950 1200
AR Path="/5EA41912/5EC60F54" Ref="#PWR?"  Part="1" 
AR Path="/5FD7CF82/5EC60F54" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E295/5EC60F54" Ref="#PWR0381"  Part="1" 
F 0 "#PWR0381" H 9950 950 50  0001 C CNN
F 1 "MOT_DGND" H 9955 1027 50  0000 C CNN
F 2 "" H 9950 1200 50  0001 C CNN
F 3 "" H 9950 1200 50  0001 C CNN
	1    9950 1200
	1    0    0    -1  
$EndComp
Connection ~ 9550 1150
Wire Wire Line
	9550 1350 9550 1150
Wire Wire Line
	9550 1050 9550 1150
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5EC021A4
P 9750 1150
AR Path="/5E9C6910/5EC021A4" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5EC021A4" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5EC021A4" Ref="C?"  Part="1" 
AR Path="/5F05E355/5EC021A4" Ref="C?"  Part="1" 
AR Path="/60EB5E32/5EC021A4" Ref="C?"  Part="1" 
AR Path="/5FD7CF82/5EC021A4" Ref="C?"  Part="1" 
AR Path="/5EA5E295/5EC021A4" Ref="C190"  Part="1" 
F 0 "C190" V 9875 1050 50  0000 L CNN
F 1 "100nF" V 9800 875 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 9750 1150 50  0001 C CNN
F 3 "~" H 9750 1150 50  0001 C CNN
F 4 "MC0402X104K250CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 25 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 9750 750 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k250ct/cap-0-1-f-25v-10-x5r-0402/dp/2320760" H 9750 650 50  0001 C CNN "Distributor Link"
	1    9750 1150
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9850 1150 9950 1150
Wire Wire Line
	9950 1150 9950 1200
Wire Wire Line
	9550 1150 9650 1150
$Comp
L PMSM_Driver_LV_Board_IC:MC14044BDR2G U48
U 1 1 5EBF54B5
P 9700 2100
F 0 "U48" H 9375 2800 50  0000 C CNN
F 1 "MC14044BDR2G" H 10050 1225 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:SOIC-16_3.9x9.9mm_P1.27mm" H 9700 850 31  0001 C CNN
F 3 "~" H 9350 2800 31  0001 C CNN
F 4 "MC14044BDR2G -  Latch, MC14044, SR, Tri State, 175 ns, SOIC" H 9700 800 31  0001 C CNN "Description"
F 5 "https://uk.farnell.com/on-semiconductor/mc14044bdr2g/sr-latch-quad-tri-state-soic-16/dp/2464496" H 9700 750 31  0001 C CNN "Distributor Link"
F 6 "https://www.mouser.co.uk/ProductDetail/ON-Semiconductor/MC14044BDR2G?qs=sGAEpiMZZMtOwpHsRTksoxa1RO4gLSA950JYh7xIIaY%3D" H 9700 700 31  0001 C CNN "Distributor Link 2"
	1    9700 2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	9000 1800 9000 2400
Wire Wire Line
	8725 2350 8725 2400
Wire Wire Line
	8725 2400 8725 2450
Connection ~ 8725 2400
Text GLabel 8075 2700 0    50   Input ~ 0
MOT_OVRC
Wire Wire Line
	8725 2400 9000 2400
Connection ~ 8725 2450
Wire Wire Line
	8725 2500 8725 2450
Wire Wire Line
	8075 2700 8125 2700
Connection ~ 8725 3000
Wire Wire Line
	8725 3050 8725 3000
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR?
U 1 1 5EFADC83
P 8725 3050
AR Path="/5EA41912/5EFADC83" Ref="#PWR?"  Part="1" 
AR Path="/5FD7CF82/5EFADC83" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E295/5EFADC83" Ref="#PWR0369"  Part="1" 
F 0 "#PWR0369" H 8725 2800 50  0001 C CNN
F 1 "MOT_DGND" H 8730 2877 50  0000 C CNN
F 2 "" H 8725 3050 50  0001 C CNN
F 3 "" H 8725 3050 50  0001 C CNN
	1    8725 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	8725 3000 8725 2900
Wire Wire Line
	8375 3000 8725 3000
Wire Wire Line
	8375 2950 8375 3000
Wire Wire Line
	8375 2700 8425 2700
Connection ~ 8375 2700
Wire Wire Line
	8375 2750 8375 2700
Wire Wire Line
	8325 2700 8375 2700
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 5EFADC76
P 8375 2850
AR Path="/5EA41912/5EFADC76" Ref="R?"  Part="1" 
AR Path="/5FD7CF82/5EFADC76" Ref="R?"  Part="1" 
AR Path="/5EA5E295/5EFADC76" Ref="R192"  Part="1" 
F 0 "R192" H 8225 2850 50  0000 C CNN
F 1 "100k" H 8250 2925 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 8375 2850 50  0001 C CNN
F 3 "~" H 8375 2850 50  0001 C CNN
F 4 "MCWR04X1003FTL -  SMD Chip Resistor, 0402 [1005 Metric], 100 kohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 8375 2450 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x1003ftl/res-100k-1-0-0625w-0402-thick/dp/2447094" H 8375 2350 50  0001 C CNN "Distributor Link"
	1    8375 2850
	-1   0    0    1   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 5EFADC6E
P 8225 2700
AR Path="/5F05E355/5EFADC6E" Ref="R?"  Part="1" 
AR Path="/5E9C6910/5EFADC6E" Ref="R?"  Part="1" 
AR Path="/5EFE605D/5EFADC6E" Ref="R?"  Part="1" 
AR Path="/5FD7CF82/5EFADC6E" Ref="R?"  Part="1" 
AR Path="/5EA5E295/5EFADC6E" Ref="R191"  Part="1" 
F 0 "R191" V 8300 2700 50  0000 C CNN
F 1 "100" V 8125 2700 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 8225 2700 50  0001 C CNN
F 3 "~" H 8225 2700 50  0001 C CNN
F 4 "MCWR04X1000FTL -  SMD Chip Resistor, 0402 [1005 Metric], 100 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 8225 2300 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x1000ftl/res-100r-1-0-0625w-thick-film/dp/2447095" H 8225 2200 50  0001 C CNN "Distributor Link"
	1    8225 2700
	0    -1   -1   0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:Q_NMOS_GSD Q15
U 1 1 5EFADC66
P 8625 2700
F 0 "Q15" H 8830 2746 50  0000 L CNN
F 1 "BSS138TA" H 8830 2655 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:SOT-23" H 8825 2800 50  0001 C CNN
F 3 "~" H 8625 2700 50  0001 C CNN
F 4 "BSS138TA -  Power MOSFET, N Channel, 50 V, 200 mA, 3.5 ohm, SOT-23, Surface Mount" H 8625 2200 31  0001 C CNN "Description"
F 5 "https://uk.farnell.com/diodes-inc/bss138ta/mosfet-n-50v-sot-23/dp/1471176" H 8625 2100 31  0001 C CNN "Distributor Link"
	1    8625 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	8675 2450 8725 2450
Text GLabel 8675 2450 0    50   Output ~ 0
MOT_EMERGENCY_STOP
Wire Wire Line
	8725 2100 8725 2150
$Comp
L PMSM_Driver_LV_Board_Power:MOT_5V0 #PWR?
U 1 1 5EF40EEE
P 8725 2100
AR Path="/5EFE605D/5EF40EEE" Ref="#PWR?"  Part="1" 
AR Path="/5EFE7331/5EF40EEE" Ref="#PWR?"  Part="1" 
AR Path="/5FD7CF82/5EF40EEE" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E295/5EF40EEE" Ref="#PWR0368"  Part="1" 
F 0 "#PWR0368" H 8725 2350 50  0001 C CNN
F 1 "MOT_5V0" H 8726 2273 50  0000 C CNN
F 2 "" H 8725 2100 50  0001 C CNN
F 3 "" H 8725 2100 50  0001 C CNN
	1    8725 2100
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 5EF40EE8
P 8725 2250
AR Path="/5F05E355/5EF40EE8" Ref="R?"  Part="1" 
AR Path="/6068445B/5EF40EE8" Ref="R?"  Part="1" 
AR Path="/60E816A3/5EF40EE8" Ref="R?"  Part="1" 
AR Path="/60EB5E32/5EF40EE8" Ref="R?"  Part="1" 
AR Path="/5FD7CF82/5EF40EE8" Ref="R?"  Part="1" 
AR Path="/5EA5E295/5EF40EE8" Ref="R193"  Part="1" 
F 0 "R193" H 8575 2250 50  0000 C CNN
F 1 "10k" H 8625 2325 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 8725 2250 50  0001 C CNN
F 3 "~" H 8725 2250 50  0001 C CNN
F 4 "WF06R1002BTL -  SMD Chip Resistor, 0603 [1608 Metric], 10 kohm, WF06R Series, 75 V, Thin Film, 100 mW" H 8725 1850 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/walsin/wf06r1002btl/res-10k-0-1-75v-0603-thin-film/dp/2502926" H 8725 1750 50  0001 C CNN "Distributor Link"
	1    8725 2250
	-1   0    0    1   
$EndComp
Wire Wire Line
	9000 1800 9200 1800
Text GLabel 10300 1850 2    50   Output ~ 0
ISO_NOT_OVR_CUR
Wire Wire Line
	10300 1850 10275 1850
Wire Wire Line
	10275 1850 10275 1750
Connection ~ 10275 1750
Wire Wire Line
	10275 1750 10300 1750
$Comp
L power:PWR_FLAG #FLG?
U 1 1 5ED0ADF6
P 4575 1150
AR Path="/5EA5D9E8/5ED0ADF6" Ref="#FLG?"  Part="1" 
AR Path="/5EA5E295/5ED0ADF6" Ref="#FLG017"  Part="1" 
F 0 "#FLG017" H 4575 1225 50  0001 C CNN
F 1 "PWR_FLAG" H 4575 1323 50  0000 C CNN
F 2 "" H 4575 1150 50  0001 C CNN
F 3 "~" H 4575 1150 50  0001 C CNN
	1    4575 1150
	1    0    0    -1  
$EndComp
Wire Wire Line
	4575 1150 4575 1250
Connection ~ 4575 1250
Wire Wire Line
	4575 1250 4425 1250
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 61744008
P 6275 7175
AR Path="/5E9C6910/61744008" Ref="C?"  Part="1" 
AR Path="/5EBC6654/61744008" Ref="C?"  Part="1" 
AR Path="/5EBC681B/61744008" Ref="C?"  Part="1" 
AR Path="/5F05E355/61744008" Ref="C?"  Part="1" 
AR Path="/6068445B/61744008" Ref="C?"  Part="1" 
AR Path="/60E816A3/61744008" Ref="C?"  Part="1" 
AR Path="/5EFE605D/61744008" Ref="C?"  Part="1" 
AR Path="/5EFE7331/61744008" Ref="C?"  Part="1" 
AR Path="/5FD7CF82/61744008" Ref="C?"  Part="1" 
AR Path="/5EA5E295/61744008" Ref="C184"  Part="1" 
AR Path="/5FE92F3B/61744008" Ref="C?"  Part="1" 
F 0 "C184" H 6075 7250 50  0000 L CNN
F 1 "10uF" H 6075 7100 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0805_2012Metric" H 6275 7175 50  0001 C CNN
F 3 "~" H 6275 7175 50  0001 C CNN
F 4 "TMK212BBJ106KG-T - 10uF ceramic cap" H 6275 6775 50  0001 C CNN "Description"
F 5 "https://www.mouser.co.uk/ProductDetail/Taiyo-Yuden/TMK212BBJ106KG-T?qs=sGAEpiMZZMs0AnBnWHyRQAEIN6r3SS%2FOSnPCSGjIcIU%3D" H 6275 6675 50  0001 C CNN "Distributor Link"
	1    6275 7175
	-1   0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 61768E8F
P 6200 6575
AR Path="/5E9C6910/61768E8F" Ref="C?"  Part="1" 
AR Path="/5EBC6654/61768E8F" Ref="C?"  Part="1" 
AR Path="/5EBC681B/61768E8F" Ref="C?"  Part="1" 
AR Path="/5F05E355/61768E8F" Ref="C?"  Part="1" 
AR Path="/6068445B/61768E8F" Ref="C?"  Part="1" 
AR Path="/60E816A3/61768E8F" Ref="C?"  Part="1" 
AR Path="/5EFE605D/61768E8F" Ref="C?"  Part="1" 
AR Path="/5EFE7331/61768E8F" Ref="C?"  Part="1" 
AR Path="/5FD7CF82/61768E8F" Ref="C?"  Part="1" 
AR Path="/5EA5E295/61768E8F" Ref="C183"  Part="1" 
AR Path="/5FE92F3B/61768E8F" Ref="C?"  Part="1" 
F 0 "C183" H 6000 6650 50  0000 L CNN
F 1 "10uF" H 6000 6500 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0805_2012Metric" H 6200 6575 50  0001 C CNN
F 3 "~" H 6200 6575 50  0001 C CNN
F 4 "TMK212BBJ106KG-T - 10uF ceramic cap" H 6200 6175 50  0001 C CNN "Description"
F 5 "https://www.mouser.co.uk/ProductDetail/Taiyo-Yuden/TMK212BBJ106KG-T?qs=sGAEpiMZZMs0AnBnWHyRQAEIN6r3SS%2FOSnPCSGjIcIU%3D" H 6200 6075 50  0001 C CNN "Distributor Link"
	1    6200 6575
	-1   0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:Q_NMOS_GDS Q?
U 1 1 5EE0A6B7
P 4025 1350
AR Path="/5EA5E06A/5EE0A6B7" Ref="Q?"  Part="1" 
AR Path="/5EA5E295/5EE0A6B7" Ref="Q13"  Part="1" 
F 0 "Q13" V 4300 1300 50  0000 L CNN
F 1 "IRFS3806TRLPBF" V 4225 1200 25  0000 L CNN
F 2 "PMSM_Driver_LV_Board:TO-263-2" H 4225 1450 50  0001 C CNN
F 3 "~" H 4025 1350 50  0001 C CNN
F 4 "IRFS3806TRLPBF -  Power MOSFET, N Channel, 60 V, 43 A, 0.0126 ohm, TO-263 (D2PAK), Surface Mount" H 4025 850 31  0001 C CNN "Description"
F 5 "https://uk.farnell.com/infineon/irfs3806trlpbf/mosfet-n-ch-60v-43a-to-263-3/dp/2803423" H 4025 750 31  0001 C CNN "Distributor Link"
	1    4025 1350
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4425 1250 4425 1375
Wire Wire Line
	4425 1575 4425 1700
Wire Wire Line
	4825 1250 4825 1700
Wire Wire Line
	4025 1550 4025 1700
$Comp
L PMSM_Driver_LV_Board_Power:MOT_VIN #PWR?
U 1 1 611ABB16
P 900 1175
AR Path="/5EA5D9E8/611ABB16" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E295/611ABB16" Ref="#PWR081"  Part="1" 
F 0 "#PWR081" H 900 1425 50  0001 C CNN
F 1 "MOT_VIN" H 901 1348 50  0000 C CNN
F 2 "" H 900 1175 50  0001 C CNN
F 3 "" H 900 1175 50  0001 C CNN
	1    900  1175
	1    0    0    -1  
$EndComp
Wire Wire Line
	1500 1250 1875 1250
Wire Wire Line
	1100 1250 900  1250
Wire Wire Line
	900  1250 900  1175
Wire Wire Line
	2275 1250 3825 1250
Text Notes 5275 1825 0    25   ~ 0
Active high signal
$EndSCHEMATC
