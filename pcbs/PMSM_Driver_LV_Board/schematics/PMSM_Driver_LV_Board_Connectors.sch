EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 16
Title "PMSM_Driver_LV_Board"
Date "2020-03-30"
Rev "A"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	2450 6300 2550 6300
Wire Wire Line
	2550 6300 2550 6400
Wire Wire Line
	2450 6200 2550 6200
Wire Wire Line
	2550 6200 2550 6300
Connection ~ 2550 6300
Wire Wire Line
	2450 6100 2550 6100
Wire Wire Line
	2550 6100 2550 6200
Connection ~ 2550 6200
Wire Wire Line
	2450 6000 2550 6000
Wire Wire Line
	2550 6000 2550 6100
Connection ~ 2550 6100
Wire Wire Line
	2450 5900 2550 5900
Wire Wire Line
	2550 5900 2550 6000
Connection ~ 2550 6000
Wire Wire Line
	2450 5800 2550 5800
Wire Wire Line
	2550 5800 2550 5900
Connection ~ 2550 5900
Wire Wire Line
	2450 5700 2550 5700
Wire Wire Line
	2550 5700 2550 5800
Connection ~ 2550 5800
Connection ~ 2550 5700
Wire Wire Line
	2450 5500 2550 5500
Wire Wire Line
	2450 5400 2550 5400
Wire Wire Line
	2550 5400 2550 5500
Connection ~ 2550 5500
Wire Wire Line
	2450 5300 2550 5300
Wire Wire Line
	2550 5300 2550 5400
Connection ~ 2550 5400
Wire Wire Line
	2450 5200 2550 5200
Wire Wire Line
	2550 5200 2550 5300
Connection ~ 2550 5300
Wire Wire Line
	2450 5100 2550 5100
Wire Wire Line
	2550 5100 2550 5200
Connection ~ 2550 5200
Wire Wire Line
	2450 5000 2550 5000
Wire Wire Line
	2550 5000 2550 5100
Connection ~ 2550 5100
Wire Wire Line
	2450 4900 2550 4900
Wire Wire Line
	2550 4900 2550 5000
Connection ~ 2550 5000
Connection ~ 2550 4900
Wire Wire Line
	2450 4700 2550 4700
Wire Wire Line
	2450 4600 2550 4600
Wire Wire Line
	2550 4600 2550 4700
Connection ~ 2550 4700
Wire Wire Line
	2450 4500 2550 4500
Wire Wire Line
	2550 4500 2550 4600
Connection ~ 2550 4600
Wire Wire Line
	2450 4400 2550 4400
Wire Wire Line
	2550 4400 2550 4500
Connection ~ 2550 4500
Wire Wire Line
	2450 4300 2550 4300
Wire Wire Line
	2550 4300 2550 4400
Connection ~ 2550 4400
Wire Wire Line
	2450 4200 2550 4200
Wire Wire Line
	2550 4200 2550 4300
Connection ~ 2550 4300
Wire Wire Line
	2450 4100 2550 4100
Wire Wire Line
	2550 4100 2550 4200
Connection ~ 2550 4200
Wire Wire Line
	2450 4000 2550 4000
Wire Wire Line
	2550 4000 2550 4100
Connection ~ 2550 4100
Wire Wire Line
	2450 3900 2550 3900
Wire Wire Line
	2550 3900 2550 4000
Connection ~ 2550 4000
Wire Wire Line
	2450 2975 2550 2975
Wire Wire Line
	2450 2875 2550 2875
Wire Wire Line
	2550 2875 2550 2975
Connection ~ 2550 2975
Wire Wire Line
	2450 2775 2550 2775
Wire Wire Line
	2550 2775 2550 2875
Connection ~ 2550 2875
Wire Wire Line
	2450 2675 2550 2675
Wire Wire Line
	2550 2675 2550 2775
Connection ~ 2550 2775
Wire Wire Line
	2450 2575 2550 2575
Wire Wire Line
	2550 2575 2550 2675
Connection ~ 2550 2675
Wire Wire Line
	2450 2475 2550 2475
Wire Wire Line
	2550 2475 2550 2575
Connection ~ 2550 2575
Wire Wire Line
	2450 2375 2550 2375
Wire Wire Line
	2550 2375 2550 2475
Connection ~ 2550 2475
Connection ~ 2550 2375
Wire Wire Line
	2450 2175 2550 2175
Wire Wire Line
	2450 2075 2550 2075
Wire Wire Line
	2550 2075 2550 2175
Connection ~ 2550 2175
Wire Wire Line
	2450 1975 2550 1975
Wire Wire Line
	2550 1975 2550 2075
Connection ~ 2550 2075
Wire Wire Line
	2450 1875 2550 1875
Wire Wire Line
	2550 1875 2550 1975
Connection ~ 2550 1975
Wire Wire Line
	2450 1775 2550 1775
Wire Wire Line
	2550 1775 2550 1875
Connection ~ 2550 1875
Wire Wire Line
	2450 1675 2550 1675
Wire Wire Line
	2550 1675 2550 1775
Connection ~ 2550 1775
Wire Wire Line
	2450 1575 2550 1575
Wire Wire Line
	2550 1575 2550 1675
Connection ~ 2550 1675
Connection ~ 2550 1575
Wire Wire Line
	2450 1375 2550 1375
Wire Wire Line
	2450 1275 2550 1275
Wire Wire Line
	2550 1275 2550 1375
Connection ~ 2550 1375
Wire Wire Line
	2450 1175 2550 1175
Wire Wire Line
	2550 1175 2550 1275
Connection ~ 2550 1275
Wire Wire Line
	2450 1075 2550 1075
Wire Wire Line
	2550 1075 2550 1175
Connection ~ 2550 1175
Wire Wire Line
	2550 4700 2550 4800
Wire Wire Line
	2450 4800 2550 4800
Connection ~ 2550 4800
Wire Wire Line
	2550 4800 2550 4900
Wire Wire Line
	2550 5500 2550 5600
Wire Wire Line
	2450 5600 2550 5600
Connection ~ 2550 5600
Wire Wire Line
	2550 5600 2550 5700
Wire Wire Line
	2550 1375 2550 1475
Wire Wire Line
	2450 1475 2550 1475
Connection ~ 2550 1475
Wire Wire Line
	2550 1475 2550 1575
Wire Wire Line
	2550 2175 2550 2275
Wire Wire Line
	2450 2275 2550 2275
Connection ~ 2550 2275
Wire Wire Line
	2550 2275 2550 2375
Wire Wire Line
	1850 3900 1950 3900
Wire Wire Line
	1850 4000 1950 4000
Wire Wire Line
	1850 4100 1950 4100
Wire Wire Line
	1850 4200 1950 4200
Wire Wire Line
	1850 4300 1950 4300
Wire Wire Line
	1850 4400 1950 4400
Wire Wire Line
	1850 4600 1950 4600
Wire Wire Line
	1850 4700 1950 4700
Wire Wire Line
	1850 4800 1950 4800
Wire Wire Line
	1850 4900 1950 4900
Wire Wire Line
	1850 5000 1950 5000
Wire Wire Line
	1850 5100 1950 5100
Wire Wire Line
	1850 5200 1950 5200
Wire Wire Line
	1850 5300 1950 5300
Wire Wire Line
	1850 5400 1950 5400
Wire Wire Line
	1850 5500 1950 5500
Wire Wire Line
	1850 5600 1950 5600
Wire Wire Line
	1850 5700 1950 5700
Wire Wire Line
	1850 4500 1950 4500
Wire Wire Line
	1850 5900 1950 5900
Wire Wire Line
	1850 1075 1950 1075
Wire Wire Line
	1850 1275 1950 1275
Wire Wire Line
	1850 1375 1950 1375
Wire Wire Line
	1850 1475 1950 1475
Wire Wire Line
	1850 1575 1950 1575
Wire Wire Line
	1850 1675 1950 1675
Wire Wire Line
	1850 1775 1950 1775
Wire Wire Line
	1850 1875 1950 1875
Wire Wire Line
	1850 1975 1950 1975
Wire Wire Line
	1850 2075 1950 2075
Wire Wire Line
	1850 2175 1950 2175
Wire Wire Line
	1850 6000 1950 6000
Wire Wire Line
	1850 6100 1950 6100
Wire Wire Line
	1950 2275 1850 2275
Wire Wire Line
	1950 2375 1850 2375
Wire Wire Line
	1850 2375 1850 2275
Wire Wire Line
	1850 2375 1850 2475
Wire Wire Line
	1850 2475 1950 2475
Connection ~ 1850 2375
Wire Wire Line
	1850 2475 1850 2575
Wire Wire Line
	1850 2575 1950 2575
Connection ~ 1850 2475
Wire Wire Line
	1950 2675 1850 2675
Wire Wire Line
	1950 2775 1850 2775
Wire Wire Line
	1850 2775 1850 2675
Wire Wire Line
	1950 2875 1850 2875
Wire Wire Line
	1850 2875 1850 2775
Connection ~ 1850 2775
Wire Wire Line
	1950 2975 1850 2975
Wire Wire Line
	1850 2975 1850 2875
Connection ~ 1850 2875
Wire Wire Line
	1850 5800 1950 5800
Text Notes 675  800  0    100  ~ 0
Control Connector
Wire Notes Line
	600  3475 600  6725
Wire Wire Line
	1850 6200 1950 6200
Wire Wire Line
	1850 6300 1950 6300
$Comp
L PMSM_Driver_LV_Board_Connector:Connector_MC-254-50-00-ST-DIP J?
U 1 1 5E9A6CFF
P 2200 5100
AR Path="/5E9C6910/5E9A6CFF" Ref="J?"  Part="1" 
AR Path="/5EBC6654/5E9A6CFF" Ref="J?"  Part="1" 
AR Path="/5EA5D9E8/5E9A6CFF" Ref="J3"  Part="1" 
F 0 "J3" H 2125 6400 50  0000 C CNN
F 1 "Connector_MC-254-50-00-ST-DIP" H 2125 3800 25  0000 C CNN
F 2 "PMSM_Driver_LV_Board:IDC-Header_2x25_P2.54mm_Vertical" H 2200 3650 50  0001 C CNN
F 3 "~" H 2150 5100 50  0001 C CNN
F 4 "MC-254-50-00-ST-DIP -  Wire-To-Board Connector, 2.54 mm, 50 Contacts, Header, Through Hole, 2 Rows" H 2200 5100 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc-254-50-00-st-dip/connector-header-50pos-2row-2/dp/2843537" H 2200 5100 50  0001 C CNN "Distributor Link"
	1    2200 5100
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Connector:Connector_MC-254-40-00-ST-DIP J?
U 1 1 5E99E10D
P 2200 1975
AR Path="/5E9C6910/5E99E10D" Ref="J?"  Part="1" 
AR Path="/5EBC6654/5E99E10D" Ref="J?"  Part="1" 
AR Path="/5EA5D9E8/5E99E10D" Ref="J2"  Part="1" 
F 0 "J2" H 2125 2975 50  0000 C CNN
F 1 "Connector_MC-254-40-00-ST-DIP" H 2150 875 25  0000 C CNN
F 2 "PMSM_Driver_LV_Board:IDC-Header_2x20_P2.54mm_Vertical" H 2200 775 50  0001 C CNN
F 3 "~" H 2150 1975 50  0001 C CNN
F 4 "MC-254-40-00-ST-DIP -  Wire-To-Board Connector, 2.54 mm, 40 Contacts, Header, Through Hole, 2 Rows" H 2200 1975 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc-254-40-00-st-dip/connector-header-40pos-2row-2/dp/2843536" H 2200 1975 50  0001 C CNN "Distributor Link"
	1    2200 1975
	1    0    0    -1  
$EndComp
Wire Wire Line
	1150 2675 1150 2550
Wire Wire Line
	1850 1175 1950 1175
Text GLabel 1850 4000 0    50   Input ~ 0
CTRL_V_A
Text GLabel 1850 4100 0    50   Input ~ 0
CTRL_V_B
Text GLabel 1850 4200 0    50   Input ~ 0
CTRL_V_C
Text GLabel 1850 4300 0    50   Input ~ 0
CTRL_CUR_A
Text GLabel 1850 4400 0    50   Input ~ 0
CTRL_CUR_B
Text GLabel 1850 4500 0    50   Input ~ 0
CTRL_CUR_T
Text GLabel 1850 4800 0    50   Input ~ 0
CTRL_ENC_I
Text GLabel 1850 4900 0    50   Input ~ 0
CTRL_RES_A
Text GLabel 1850 5000 0    50   Input ~ 0
CTRL_RES_B
Text GLabel 1850 5200 0    50   Input ~ 0
CTRL_HAL_A
Text GLabel 1850 5300 0    50   Input ~ 0
CTRL_HAL_B
Text GLabel 1850 5400 0    50   Input ~ 0
CTRL_HAL_C
Text GLabel 1850 5500 0    50   Input ~ 0
CTRL_TEMP
Text GLabel 1850 5100 0    50   Input ~ 0
CTRL_RES_I
Text GLabel 1850 5700 0    50   Output ~ 0
CTRL_SPI_SEL_RDC
Text GLabel 1850 5600 0    50   Output ~ 0
CTRL_RDC_SAMPLE_N
Text GLabel 1850 5900 0    50   Output ~ 0
CTRL_SPI_SCK
Text GLabel 1850 4700 0    50   Output ~ 0
CTRL_ENC_B
Text GLabel 1850 4600 0    50   Input ~ 0
CTRL_ENC_A
Text GLabel 1850 3900 0    50   Input ~ 0
CTRL_V_SUP
Text GLabel 1850 6100 0    50   Output ~ 0
CTRL_SPI_MOSI
Text GLabel 1850 5800 0    50   Output ~ 0
CTRL_SPI_SEL_AUX
Text GLabel 1850 6300 0    50   BiDi ~ 0
CTRL_SDA
Text GLabel 1850 6200 0    50   Output ~ 0
CTRL_SCL
Text GLabel 1850 1075 0    50   Output ~ 0
CTRL_ADC_CLK
Text GLabel 1850 1275 0    50   Output ~ 0
CTRL_VT_ENABLE
Text GLabel 1850 1475 0    50   Output ~ 0
CTRL_PWM_AH
Text GLabel 1850 1575 0    50   Output ~ 0
CTRL_PWM_AL
Text GLabel 1850 1675 0    50   Output ~ 0
CTRL_PWM_BH
Text GLabel 1850 1775 0    50   Output ~ 0
CTRL_PWM_BL
Text GLabel 1850 1875 0    50   Output ~ 0
CTRL_PWM_CH
Text GLabel 1850 1975 0    50   Output ~ 0
CTRL_PWM_CL
Text GLabel 1850 1375 0    50   Output ~ 0
CTRL_MOT_EN
Text GLabel 1850 2075 0    50   Output ~ 0
CTRL_RST_OVR_CUR
Text GLabel 1850 2175 0    50   Input ~ 0
CTRL_NOT_OVR_CUR
Text GLabel 1850 1175 0    50   Output ~ 0
CTRL_POWER_GOOD
Text Notes 700  925  0    25   ~ 0
CTRL_POWER_GOOD signal is expected to be CTRL_V3V signal,\nall other signals are expected to be CTRL_VADJ signals.
Wire Wire Line
	2550 2975 2550 3075
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_3V3 #PWR?
U 1 1 5EA01EFF
P 825 1825
AR Path="/5E9C6910/5EA01EFF" Ref="#PWR?"  Part="1" 
AR Path="/5EBC6654/5EA01EFF" Ref="#PWR?"  Part="1" 
AR Path="/5EA5D9E8/5EA01EFF" Ref="#PWR01"  Part="1" 
F 0 "#PWR01" H 825 2075 50  0001 C CNN
F 1 "CTRL_3V3" H 826 1998 50  0000 C CNN
F 2 "" H 825 1825 50  0001 C CNN
F 3 "" H 825 1825 50  0001 C CNN
	1    825  1825
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_VADJ #PWR?
U 1 1 5EA024D2
P 1150 2550
AR Path="/5E9C6910/5EA024D2" Ref="#PWR?"  Part="1" 
AR Path="/5EBC6654/5EA024D2" Ref="#PWR?"  Part="1" 
AR Path="/5EA5D9E8/5EA024D2" Ref="#PWR03"  Part="1" 
F 0 "#PWR03" H 1150 2800 50  0001 C CNN
F 1 "CTRL_VADJ" H 1151 2723 50  0000 C CNN
F 2 "" H 1150 2550 50  0001 C CNN
F 3 "" H 1150 2550 50  0001 C CNN
	1    1150 2550
	1    0    0    -1  
$EndComp
Text Notes 700  3800 0    25   ~ 0
CTRL_SCL and CTRL_SDA signals can be either CTRL_VADJ or CTRL_3V3 signals,\nall other signals are expected to be CTRL_VADJ signals.
Text Notes 675  3675 0    100  ~ 0
Sensor Connector
Connection ~ 1850 2275
Wire Notes Line
	600  3400 2825 3400
Wire Notes Line
	2825 600  2825 3400
Wire Notes Line
	600  600  600  3400
Wire Notes Line
	2825 600  600  600 
Wire Notes Line
	600  3475 2825 3475
Wire Notes Line
	2825 3475 2825 6725
Wire Notes Line
	2825 6725 600  6725
Text Notes 700  2600 0    25   ~ 0
CTRL_VADJ is typically\n1.8V, 2.5V or 3.3V.
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_3V3 #PWR?
U 1 1 5E9CA69D
P 3675 1075
AR Path="/5E9C6910/5E9CA69D" Ref="#PWR?"  Part="1" 
AR Path="/5EBC6654/5E9CA69D" Ref="#PWR?"  Part="1" 
AR Path="/5EA5D9E8/5E9CA69D" Ref="#PWR08"  Part="1" 
F 0 "#PWR08" H 3675 1325 50  0001 C CNN
F 1 "CTRL_3V3" H 3676 1248 50  0000 C CNN
F 2 "" H 3675 1075 50  0001 C CNN
F 3 "" H 3675 1075 50  0001 C CNN
	1    3675 1075
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_VADJ #PWR?
U 1 1 5E9D215F
P 5100 1075
AR Path="/5E9C6910/5E9D215F" Ref="#PWR?"  Part="1" 
AR Path="/5EBC6654/5E9D215F" Ref="#PWR?"  Part="1" 
AR Path="/5EA5D9E8/5E9D215F" Ref="#PWR021"  Part="1" 
F 0 "#PWR021" H 5100 1325 50  0001 C CNN
F 1 "CTRL_VADJ" H 5101 1248 50  0000 C CNN
F 2 "" H 5100 1075 50  0001 C CNN
F 3 "" H 5100 1075 50  0001 C CNN
	1    5100 1075
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5E9D9D4F
P 3675 1275
AR Path="/5E9C6910/5E9D9D4F" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5E9D9D4F" Ref="C?"  Part="1" 
AR Path="/5EA5D9E8/5E9D9D4F" Ref="C2"  Part="1" 
F 0 "C2" H 3767 1321 50  0000 L CNN
F 1 "100nF" H 3750 1250 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 3675 1275 50  0001 C CNN
F 3 "~" H 3675 1275 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 3675 875 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 3675 775 50  0001 C CNN "Distributor Link"
	1    3675 1275
	1    0    0    -1  
$EndComp
Wire Wire Line
	3675 1425 3675 1375
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5EAA7E4D
P 3200 1275
AR Path="/5E9C6910/5EAA7E4D" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5EAA7E4D" Ref="C?"  Part="1" 
AR Path="/5EA5D9E8/5EAA7E4D" Ref="C1"  Part="1" 
F 0 "C1" H 3275 1325 50  0000 L CNN
F 1 "47uF" H 3275 1250 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 3200 1275 50  0001 C CNN
F 3 "~" H 3200 1275 50  0001 C CNN
F 4 "GRM31CR60J476ME19L -  SMD Multilayer Ceramic Capacitor, 47 µF, 6.3 V, 1206 [3216 Metric], ± 20%, X5R, GRM Series" H 3200 875 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/murata/grm31cr60j476me19l/cap-47-f-6-3v-20-x5r-1206/dp/1735534" H 3200 775 50  0001 C CNN "Distributor Link"
F 6 "6.3V" H 3225 1175 50  0000 L CNN "Voltage"
	1    3200 1275
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5EAB93DC
P 4150 1275
AR Path="/5E9C6910/5EAB93DC" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5EAB93DC" Ref="C?"  Part="1" 
AR Path="/5EA5D9E8/5EAB93DC" Ref="C3"  Part="1" 
F 0 "C3" H 4242 1321 50  0000 L CNN
F 1 "100nF" H 4225 1250 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 4150 1275 50  0001 C CNN
F 3 "~" H 4150 1275 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 4150 875 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 4150 775 50  0001 C CNN "Distributor Link"
	1    4150 1275
	1    0    0    -1  
$EndComp
Wire Wire Line
	4150 1425 4150 1375
Wire Wire Line
	3200 1175 3200 1125
Wire Wire Line
	4150 1125 4150 1175
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5EB30958
P 5100 1275
AR Path="/5E9C6910/5EB30958" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5EB30958" Ref="C?"  Part="1" 
AR Path="/5EA5D9E8/5EB30958" Ref="C6"  Part="1" 
F 0 "C6" H 5192 1321 50  0000 L CNN
F 1 "100nF" H 5175 1250 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 5100 1275 50  0001 C CNN
F 3 "~" H 5100 1275 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 5100 875 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 5100 775 50  0001 C CNN "Distributor Link"
	1    5100 1275
	1    0    0    -1  
$EndComp
Wire Wire Line
	5100 1425 5100 1375
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5EB30968
P 4625 1275
AR Path="/5E9C6910/5EB30968" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5EB30968" Ref="C?"  Part="1" 
AR Path="/5EA5D9E8/5EB30968" Ref="C5"  Part="1" 
F 0 "C5" H 4700 1325 50  0000 L CNN
F 1 "47uF" H 4700 1250 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 4625 1275 50  0001 C CNN
F 3 "~" H 4625 1275 50  0001 C CNN
F 4 "GRM31CR60J476ME19L -  SMD Multilayer Ceramic Capacitor, 47 µF, 6.3 V, 1206 [3216 Metric], ± 20%, X5R, GRM Series" H 4625 875 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/murata/grm31cr60j476me19l/cap-47-f-6-3v-20-x5r-1206/dp/1735534" H 4625 775 50  0001 C CNN "Distributor Link"
F 6 "6.3V" H 4650 1175 50  0000 L CNN "Voltage"
	1    4625 1275
	1    0    0    -1  
$EndComp
Wire Wire Line
	4625 1425 4625 1375
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5EB30977
P 5575 1275
AR Path="/5E9C6910/5EB30977" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5EB30977" Ref="C?"  Part="1" 
AR Path="/5EA5D9E8/5EB30977" Ref="C7"  Part="1" 
F 0 "C7" H 5667 1321 50  0000 L CNN
F 1 "100nF" H 5650 1250 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 5575 1275 50  0001 C CNN
F 3 "~" H 5575 1275 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 5575 875 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 5575 775 50  0001 C CNN "Distributor Link"
	1    5575 1275
	1    0    0    -1  
$EndComp
Wire Wire Line
	5575 1425 5575 1375
Wire Wire Line
	4625 1175 4625 1125
Connection ~ 5100 1125
Wire Wire Line
	5100 1125 5100 1175
Wire Wire Line
	5575 1125 5575 1175
Wire Wire Line
	5100 1075 5100 1125
Text Notes 2975 800  0    100  ~ 0
Bulk Capacitors
Wire Notes Line
	6000 600  6000 1750
Wire Notes Line
	6000 1750 2900 1750
Wire Notes Line
	2900 600  6000 600 
Wire Notes Line
	2900 600  2900 1750
Text GLabel 1850 6000 0    50   Input ~ 0
CTRL_SPI_MISO
$Comp
L PMSM_Driver_LV_Board_Connector:Connector_MC-254-10-00-ST-DIP J6
U 1 1 606E81DB
P 4225 2625
F 0 "J6" H 4225 2950 50  0000 C CNN
F 1 "Connector_MC-254-10-00-ST-DIP" H 4275 2350 25  0000 C CNN
F 2 "PMSM_Driver_LV_Board:IDC-Header_2x05_P2.54mm_Vertical" H 4225 2175 50  0001 C CNN
F 3 "~" H 4225 2625 50  0001 C CNN
F 4 "MC-254-10-00-ST-DIP -  Wire-To-Board Connector, 2.54 mm, 10 Contacts, Header, Through Hole, 2 Rows" H 4225 2075 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc-254-10-00-st-dip/connector-header-10pos-2row-2/dp/2843527" H 4225 1975 50  0001 C CNN "Distributor Link"
	1    4225 2625
	-1   0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:ISO_SENS_5V0 #PWR017
U 1 1 6070F880
P 4525 2325
F 0 "#PWR017" H 4525 2575 50  0001 C CNN
F 1 "ISO_SENS_5V0" H 4526 2498 50  0000 C CNN
F 2 "" H 4525 2325 50  0001 C CNN
F 3 "" H 4525 2325 50  0001 C CNN
	1    4525 2325
	1    0    0    -1  
$EndComp
Wire Wire Line
	4425 2425 4525 2425
Wire Wire Line
	4525 2425 4525 2325
$Comp
L PMSM_Driver_LV_Board_Power:ISO_DGND #PWR010
U 1 1 607445E9
P 3825 2925
F 0 "#PWR010" H 3825 2675 50  0001 C CNN
F 1 "ISO_DGND" H 3830 2752 50  0000 C CNN
F 2 "" H 3825 2925 50  0001 C CNN
F 3 "" H 3825 2925 50  0001 C CNN
	1    3825 2925
	1    0    0    -1  
$EndComp
Wire Wire Line
	3825 2925 3825 2525
Wire Wire Line
	3825 2525 3925 2525
Text GLabel 4625 2625 2    50   Output ~ 0
ISO_DIF_ENC_A_P
Wire Wire Line
	3725 2625 3925 2625
Wire Wire Line
	3725 2725 3925 2725
Wire Wire Line
	3725 2825 3925 2825
Wire Wire Line
	4425 2625 4625 2625
Wire Wire Line
	4425 2725 4625 2725
Wire Wire Line
	4425 2825 4625 2825
Text GLabel 4625 2725 2    50   Output ~ 0
ISO_DIF_ENC_B_P
Text GLabel 4625 2825 2    50   Output ~ 0
ISO_DIF_ENC_I_P
Text GLabel 3725 2625 0    50   Output ~ 0
ISO_DIF_ENC_A_N
Text GLabel 3725 2725 0    50   Output ~ 0
ISO_DIF_ENC_B_N
Text GLabel 3725 2825 0    50   Output ~ 0
ISO_DIF_ENC_I_N
$Comp
L PMSM_Driver_LV_Board_Power:ISO_DGND #PWR018
U 1 1 607C60C5
P 4525 2925
F 0 "#PWR018" H 4525 2675 50  0001 C CNN
F 1 "ISO_DGND" H 4530 2752 50  0000 C CNN
F 2 "" H 4525 2925 50  0001 C CNN
F 3 "" H 4525 2925 50  0001 C CNN
	1    4525 2925
	1    0    0    -1  
$EndComp
Wire Wire Line
	4525 2925 4525 2525
Wire Wire Line
	4425 2525 4525 2525
Wire Wire Line
	3925 2425 3825 2425
Wire Wire Line
	3825 2425 3825 2525
Connection ~ 3825 2525
$Comp
L PMSM_Driver_LV_Board_Connector:Connector_MC-254-10-00-ST-DIP J4
U 1 1 607E4A3C
P 3575 4350
F 0 "J4" H 3575 4675 50  0000 C CNN
F 1 "Connector_MC-254-10-00-ST-DIP" H 3625 4075 25  0000 C CNN
F 2 "PMSM_Driver_LV_Board:IDC-Header_2x05_P2.54mm_Vertical" H 3575 3900 50  0001 C CNN
F 3 "~" H 3575 4350 50  0001 C CNN
F 4 "MC-254-10-00-ST-DIP -  Wire-To-Board Connector, 2.54 mm, 10 Contacts, Header, Through Hole, 2 Rows" H 3575 3800 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc-254-10-00-st-dip/connector-header-10pos-2row-2/dp/2843527" H 3575 3700 50  0001 C CNN "Distributor Link"
	1    3575 4350
	-1   0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:ISO_SENS_5V0 #PWR011
U 1 1 607E4A42
P 3875 4000
F 0 "#PWR011" H 3875 4250 50  0001 C CNN
F 1 "ISO_SENS_5V0" H 3876 4173 50  0000 C CNN
F 2 "" H 3875 4000 50  0001 C CNN
F 3 "" H 3875 4000 50  0001 C CNN
	1    3875 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	3775 4150 3875 4150
$Comp
L PMSM_Driver_LV_Board_Power:ISO_DGND #PWR06
U 1 1 607E4A4A
P 3175 4650
F 0 "#PWR06" H 3175 4400 50  0001 C CNN
F 1 "ISO_DGND" H 3180 4477 50  0000 C CNN
F 2 "" H 3175 4650 50  0001 C CNN
F 3 "" H 3175 4650 50  0001 C CNN
	1    3175 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	3175 4650 3175 4550
Wire Wire Line
	3175 4250 3275 4250
Text GLabel 5300 4350 2    50   Output ~ 0
ISO_SE_ENC_A
Text GLabel 5300 4450 2    50   Output ~ 0
ISO_SE_ENC_B
Text GLabel 5300 4550 2    50   Output ~ 0
ISO_SE_ENC_I
$Comp
L PMSM_Driver_LV_Board_Power:ISO_DGND #PWR012
U 1 1 607E4A5E
P 3875 4650
F 0 "#PWR012" H 3875 4400 50  0001 C CNN
F 1 "ISO_DGND" H 3880 4477 50  0000 C CNN
F 2 "" H 3875 4650 50  0001 C CNN
F 3 "" H 3875 4650 50  0001 C CNN
	1    3875 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	3875 4650 3875 4250
Wire Wire Line
	3775 4250 3875 4250
Wire Wire Line
	3275 4150 3175 4150
Wire Wire Line
	3175 4150 3175 4250
Connection ~ 3175 4250
Wire Wire Line
	3275 4350 3175 4350
Connection ~ 3175 4350
Wire Wire Line
	3175 4350 3175 4250
Wire Wire Line
	3275 4450 3175 4450
Connection ~ 3175 4450
Wire Wire Line
	3175 4450 3175 4350
Wire Wire Line
	3275 4550 3175 4550
Connection ~ 3175 4550
Wire Wire Line
	3175 4550 3175 4450
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 608AD403
P 4225 4200
AR Path="/5F05E355/608AD403" Ref="R?"  Part="1" 
AR Path="/5E9C6910/608AD403" Ref="R?"  Part="1" 
AR Path="/5EA5D9E8/608AD403" Ref="R1"  Part="1" 
F 0 "R1" H 4325 4225 50  0000 C CNN
F 1 "4k7" H 4325 4150 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4225 4200 50  0001 C CNN
F 3 "~" H 4225 4200 50  0001 C CNN
F 4 "MC0603SAF4701T5E -  RES, THICK FILM, 4K7, 1%, 0.1W, 0603" H 4225 3800 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0603saf4701t5e/res-thick-film-4k7-1-0-1w-0603/dp/1632439" H 4225 3700 50  0001 C CNN "Distributor Link"
	1    4225 4200
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 608AD40B
P 4475 4200
AR Path="/5F05E355/608AD40B" Ref="R?"  Part="1" 
AR Path="/5E9C6910/608AD40B" Ref="R?"  Part="1" 
AR Path="/5EA5D9E8/608AD40B" Ref="R2"  Part="1" 
F 0 "R2" H 4575 4225 50  0000 C CNN
F 1 "4k7" H 4575 4150 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4475 4200 50  0001 C CNN
F 3 "~" H 4475 4200 50  0001 C CNN
F 4 "MC0603SAF4701T5E -  RES, THICK FILM, 4K7, 1%, 0.1W, 0603" H 4475 3800 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0603saf4701t5e/res-thick-film-4k7-1-0-1w-0603/dp/1632439" H 4475 3700 50  0001 C CNN "Distributor Link"
	1    4475 4200
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 608C32AA
P 4725 4200
AR Path="/5F05E355/608C32AA" Ref="R?"  Part="1" 
AR Path="/5E9C6910/608C32AA" Ref="R?"  Part="1" 
AR Path="/5EA5D9E8/608C32AA" Ref="R3"  Part="1" 
F 0 "R3" H 4825 4225 50  0000 C CNN
F 1 "4k7" H 4825 4150 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4725 4200 50  0001 C CNN
F 3 "~" H 4725 4200 50  0001 C CNN
F 4 "MC0603SAF4701T5E -  RES, THICK FILM, 4K7, 1%, 0.1W, 0603" H 4725 3800 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0603saf4701t5e/res-thick-film-4k7-1-0-1w-0603/dp/1632439" H 4725 3700 50  0001 C CNN "Distributor Link"
	1    4725 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	3875 4000 3875 4150
Wire Wire Line
	4225 4100 4225 4050
Wire Wire Line
	4225 4050 4475 4050
Wire Wire Line
	4475 4100 4475 4050
Connection ~ 4475 4050
Wire Wire Line
	4725 4100 4725 4050
Wire Wire Line
	4725 4050 4475 4050
Wire Wire Line
	3775 4350 4725 4350
Wire Wire Line
	3775 4450 4475 4450
Wire Wire Line
	3775 4550 4225 4550
Wire Wire Line
	4225 4300 4225 4550
Wire Wire Line
	4475 4300 4475 4450
Wire Wire Line
	4725 4300 4725 4350
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 609920AA
P 5125 4350
AR Path="/5F05E355/609920AA" Ref="R?"  Part="1" 
AR Path="/5E9C6910/609920AA" Ref="R?"  Part="1" 
AR Path="/5EA5D9E8/609920AA" Ref="R4"  Part="1" 
F 0 "R4" V 5175 4475 50  0000 C CNN
F 1 "10" V 5175 4250 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 5125 4350 50  0001 C CNN
F 3 "~" H 5125 4350 50  0001 C CNN
F 4 "MCWR04X10R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 10 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 5125 3950 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x10r0ftl/res-10r-1-0-0625w-0402-thick-film/dp/2447098" H 5125 3850 50  0001 C CNN "Distributor Link"
	1    5125 4350
	0    -1   -1   0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 609B6386
P 5125 4450
AR Path="/5F05E355/609B6386" Ref="R?"  Part="1" 
AR Path="/5E9C6910/609B6386" Ref="R?"  Part="1" 
AR Path="/5EA5D9E8/609B6386" Ref="R5"  Part="1" 
F 0 "R5" V 5175 4575 50  0000 C CNN
F 1 "10" V 5175 4350 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 5125 4450 50  0001 C CNN
F 3 "~" H 5125 4450 50  0001 C CNN
F 4 "MCWR04X10R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 10 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 5125 4050 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x10r0ftl/res-10r-1-0-0625w-0402-thick-film/dp/2447098" H 5125 3950 50  0001 C CNN "Distributor Link"
	1    5125 4450
	0    -1   -1   0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 609C2305
P 5125 4550
AR Path="/5F05E355/609C2305" Ref="R?"  Part="1" 
AR Path="/5E9C6910/609C2305" Ref="R?"  Part="1" 
AR Path="/5EA5D9E8/609C2305" Ref="R6"  Part="1" 
F 0 "R6" V 5175 4675 50  0000 C CNN
F 1 "10" V 5175 4450 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 5125 4550 50  0001 C CNN
F 3 "~" H 5125 4550 50  0001 C CNN
F 4 "MCWR04X10R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 10 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 5125 4150 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x10r0ftl/res-10r-1-0-0625w-0402-thick-film/dp/2447098" H 5125 4050 50  0001 C CNN "Distributor Link"
	1    5125 4550
	0    -1   -1   0   
$EndComp
Text Notes 2975 2025 0    100  ~ 0
Differential Encoder Connector
Wire Notes Line
	6000 1825 6000 3400
Wire Notes Line
	6000 3400 2900 3400
Wire Notes Line
	2900 3400 2900 1825
Wire Notes Line
	2900 1825 6000 1825
Text Notes 2975 3675 0    100  ~ 0
Single-Ended Encoder Connector
Wire Notes Line
	2900 5050 2900 3475
Wire Notes Line
	2900 3475 6000 3475
Wire Wire Line
	4475 4050 4475 4000
$Comp
L PMSM_Driver_LV_Board_Power:ISO_SENS_5V0 #PWR016
U 1 1 608DB912
P 4475 4000
F 0 "#PWR016" H 4475 4250 50  0001 C CNN
F 1 "ISO_SENS_5V0" H 4476 4173 50  0000 C CNN
F 2 "" H 4475 4000 50  0001 C CNN
F 3 "" H 4475 4000 50  0001 C CNN
	1    4475 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	4725 4350 5025 4350
Connection ~ 4725 4350
Wire Wire Line
	5025 4450 4475 4450
Connection ~ 4475 4450
Wire Wire Line
	4225 4550 5025 4550
Connection ~ 4225 4550
Wire Wire Line
	5225 4350 5300 4350
Wire Wire Line
	5225 4450 5300 4450
Wire Wire Line
	5225 4550 5300 4550
Wire Notes Line
	6000 3475 6000 5050
Wire Notes Line
	6000 5050 2900 5050
$Comp
L PMSM_Driver_LV_Board_Connector:Connector_MC-254-10-00-ST-DIP J9
U 1 1 60E6F900
P 7400 2625
F 0 "J9" H 7400 2950 50  0000 C CNN
F 1 "Connector_MC-254-10-00-ST-DIP" H 7450 2350 25  0000 C CNN
F 2 "PMSM_Driver_LV_Board:IDC-Header_2x05_P2.54mm_Vertical" H 7400 2175 50  0001 C CNN
F 3 "~" H 7400 2625 50  0001 C CNN
F 4 "MC-254-10-00-ST-DIP -  Wire-To-Board Connector, 2.54 mm, 10 Contacts, Header, Through Hole, 2 Rows" H 7400 2075 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc-254-10-00-st-dip/connector-header-10pos-2row-2/dp/2843527" H 7400 1975 50  0001 C CNN "Distributor Link"
	1    7400 2625
	-1   0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:ISO_SENS_5V0 #PWR034
U 1 1 60E6F906
P 7700 2325
F 0 "#PWR034" H 7700 2575 50  0001 C CNN
F 1 "ISO_SENS_5V0" H 7701 2498 50  0000 C CNN
F 2 "" H 7700 2325 50  0001 C CNN
F 3 "" H 7700 2325 50  0001 C CNN
	1    7700 2325
	1    0    0    -1  
$EndComp
Wire Wire Line
	7600 2425 7700 2425
Wire Wire Line
	7700 2425 7700 2325
$Comp
L PMSM_Driver_LV_Board_Power:ISO_DGND #PWR029
U 1 1 60E6F90E
P 7000 2925
F 0 "#PWR029" H 7000 2675 50  0001 C CNN
F 1 "ISO_DGND" H 7005 2752 50  0000 C CNN
F 2 "" H 7000 2925 50  0001 C CNN
F 3 "" H 7000 2925 50  0001 C CNN
	1    7000 2925
	1    0    0    -1  
$EndComp
Wire Wire Line
	7000 2925 7000 2525
Wire Wire Line
	7000 2525 7100 2525
Text GLabel 7800 2625 2    50   Output ~ 0
ISO_DIF_HAL_A_P
Wire Wire Line
	6900 2625 7100 2625
Wire Wire Line
	6900 2725 7100 2725
Wire Wire Line
	6900 2825 7100 2825
Wire Wire Line
	7600 2625 7800 2625
Wire Wire Line
	7600 2725 7800 2725
Wire Wire Line
	7600 2825 7800 2825
Text GLabel 7800 2725 2    50   Output ~ 0
ISO_DIF_HAL_B_P
Text GLabel 7800 2825 2    50   Output ~ 0
ISO_DIF_HAL_C_P
Text GLabel 6900 2625 0    50   Output ~ 0
ISO_DIF_HAL_A_N
Text GLabel 6900 2725 0    50   Output ~ 0
ISO_DIF_HAL_B_N
Text GLabel 6900 2825 0    50   Output ~ 0
ISO_DIF_HAL_C_N
$Comp
L PMSM_Driver_LV_Board_Power:ISO_DGND #PWR035
U 1 1 60E6F922
P 7700 2925
F 0 "#PWR035" H 7700 2675 50  0001 C CNN
F 1 "ISO_DGND" H 7705 2752 50  0000 C CNN
F 2 "" H 7700 2925 50  0001 C CNN
F 3 "" H 7700 2925 50  0001 C CNN
	1    7700 2925
	1    0    0    -1  
$EndComp
Wire Wire Line
	7700 2925 7700 2525
Wire Wire Line
	7600 2525 7700 2525
Wire Wire Line
	7100 2425 7000 2425
Wire Wire Line
	7000 2425 7000 2525
Connection ~ 7000 2525
$Comp
L PMSM_Driver_LV_Board_Connector:Connector_MC-254-10-00-ST-DIP J7
U 1 1 60E6F92F
P 6750 4350
F 0 "J7" H 6750 4675 50  0000 C CNN
F 1 "Connector_MC-254-10-00-ST-DIP" H 6800 4075 25  0000 C CNN
F 2 "PMSM_Driver_LV_Board:IDC-Header_2x05_P2.54mm_Vertical" H 6750 3900 50  0001 C CNN
F 3 "~" H 6750 4350 50  0001 C CNN
F 4 "MC-254-10-00-ST-DIP -  Wire-To-Board Connector, 2.54 mm, 10 Contacts, Header, Through Hole, 2 Rows" H 6750 3800 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc-254-10-00-st-dip/connector-header-10pos-2row-2/dp/2843527" H 6750 3700 50  0001 C CNN "Distributor Link"
	1    6750 4350
	-1   0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:ISO_SENS_5V0 #PWR030
U 1 1 60E6F935
P 7050 4000
F 0 "#PWR030" H 7050 4250 50  0001 C CNN
F 1 "ISO_SENS_5V0" H 7051 4173 50  0000 C CNN
F 2 "" H 7050 4000 50  0001 C CNN
F 3 "" H 7050 4000 50  0001 C CNN
	1    7050 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	6950 4150 7050 4150
$Comp
L PMSM_Driver_LV_Board_Power:ISO_DGND #PWR026
U 1 1 60E6F93C
P 6350 4650
F 0 "#PWR026" H 6350 4400 50  0001 C CNN
F 1 "ISO_DGND" H 6355 4477 50  0000 C CNN
F 2 "" H 6350 4650 50  0001 C CNN
F 3 "" H 6350 4650 50  0001 C CNN
	1    6350 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 4650 6350 4550
Wire Wire Line
	6350 4250 6450 4250
Text GLabel 8475 4350 2    50   Output ~ 0
ISO_SE_HAL_A
Text GLabel 8475 4450 2    50   Output ~ 0
ISO_SE_HAL_B
Text GLabel 8475 4550 2    50   Output ~ 0
ISO_SE_HAL_C
$Comp
L PMSM_Driver_LV_Board_Power:ISO_DGND #PWR031
U 1 1 60E6F947
P 7050 4650
F 0 "#PWR031" H 7050 4400 50  0001 C CNN
F 1 "ISO_DGND" H 7055 4477 50  0000 C CNN
F 2 "" H 7050 4650 50  0001 C CNN
F 3 "" H 7050 4650 50  0001 C CNN
	1    7050 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	7050 4650 7050 4250
Wire Wire Line
	6950 4250 7050 4250
Wire Wire Line
	6450 4150 6350 4150
Wire Wire Line
	6350 4150 6350 4250
Connection ~ 6350 4250
Wire Wire Line
	6450 4350 6350 4350
Connection ~ 6350 4350
Wire Wire Line
	6350 4350 6350 4250
Wire Wire Line
	6450 4450 6350 4450
Connection ~ 6350 4450
Wire Wire Line
	6350 4450 6350 4350
Wire Wire Line
	6450 4550 6350 4550
Connection ~ 6350 4550
Wire Wire Line
	6350 4550 6350 4450
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 60E6F95D
P 7400 4200
AR Path="/5F05E355/60E6F95D" Ref="R?"  Part="1" 
AR Path="/5E9C6910/60E6F95D" Ref="R?"  Part="1" 
AR Path="/5EA5D9E8/60E6F95D" Ref="R7"  Part="1" 
F 0 "R7" H 7500 4225 50  0000 C CNN
F 1 "4k7" H 7500 4150 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 7400 4200 50  0001 C CNN
F 3 "~" H 7400 4200 50  0001 C CNN
F 4 "MC0603SAF4701T5E -  RES, THICK FILM, 4K7, 1%, 0.1W, 0603" H 7400 3800 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0603saf4701t5e/res-thick-film-4k7-1-0-1w-0603/dp/1632439" H 7400 3700 50  0001 C CNN "Distributor Link"
	1    7400 4200
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 60E6F965
P 7650 4200
AR Path="/5F05E355/60E6F965" Ref="R?"  Part="1" 
AR Path="/5E9C6910/60E6F965" Ref="R?"  Part="1" 
AR Path="/5EA5D9E8/60E6F965" Ref="R8"  Part="1" 
F 0 "R8" H 7750 4225 50  0000 C CNN
F 1 "4k7" H 7750 4150 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 7650 4200 50  0001 C CNN
F 3 "~" H 7650 4200 50  0001 C CNN
F 4 "MC0603SAF4701T5E -  RES, THICK FILM, 4K7, 1%, 0.1W, 0603" H 7650 3800 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0603saf4701t5e/res-thick-film-4k7-1-0-1w-0603/dp/1632439" H 7650 3700 50  0001 C CNN "Distributor Link"
	1    7650 4200
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 60E6F96D
P 7900 4200
AR Path="/5F05E355/60E6F96D" Ref="R?"  Part="1" 
AR Path="/5E9C6910/60E6F96D" Ref="R?"  Part="1" 
AR Path="/5EA5D9E8/60E6F96D" Ref="R9"  Part="1" 
F 0 "R9" H 8000 4225 50  0000 C CNN
F 1 "4k7" H 8000 4150 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 7900 4200 50  0001 C CNN
F 3 "~" H 7900 4200 50  0001 C CNN
F 4 "MC0603SAF4701T5E -  RES, THICK FILM, 4K7, 1%, 0.1W, 0603" H 7900 3800 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0603saf4701t5e/res-thick-film-4k7-1-0-1w-0603/dp/1632439" H 7900 3700 50  0001 C CNN "Distributor Link"
	1    7900 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	7050 4000 7050 4150
Wire Wire Line
	7400 4100 7400 4050
Wire Wire Line
	7400 4050 7650 4050
Wire Wire Line
	7650 4100 7650 4050
Connection ~ 7650 4050
Wire Wire Line
	7900 4100 7900 4050
Wire Wire Line
	7900 4050 7650 4050
Wire Wire Line
	6950 4350 7900 4350
Wire Wire Line
	6950 4450 7650 4450
Wire Wire Line
	6950 4550 7400 4550
Wire Wire Line
	7400 4300 7400 4550
Wire Wire Line
	7650 4300 7650 4450
Wire Wire Line
	7900 4300 7900 4350
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 60E6F982
P 8300 4350
AR Path="/5F05E355/60E6F982" Ref="R?"  Part="1" 
AR Path="/5E9C6910/60E6F982" Ref="R?"  Part="1" 
AR Path="/5EA5D9E8/60E6F982" Ref="R10"  Part="1" 
F 0 "R10" V 8350 4475 50  0000 C CNN
F 1 "10" V 8350 4250 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 8300 4350 50  0001 C CNN
F 3 "~" H 8300 4350 50  0001 C CNN
F 4 "MCWR04X10R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 10 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 8300 3950 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x10r0ftl/res-10r-1-0-0625w-0402-thick-film/dp/2447098" H 8300 3850 50  0001 C CNN "Distributor Link"
	1    8300 4350
	0    -1   -1   0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 60E6F98A
P 8300 4450
AR Path="/5F05E355/60E6F98A" Ref="R?"  Part="1" 
AR Path="/5E9C6910/60E6F98A" Ref="R?"  Part="1" 
AR Path="/5EA5D9E8/60E6F98A" Ref="R11"  Part="1" 
F 0 "R11" V 8350 4575 50  0000 C CNN
F 1 "10" V 8350 4350 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 8300 4450 50  0001 C CNN
F 3 "~" H 8300 4450 50  0001 C CNN
F 4 "MCWR04X10R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 10 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 8300 4050 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x10r0ftl/res-10r-1-0-0625w-0402-thick-film/dp/2447098" H 8300 3950 50  0001 C CNN "Distributor Link"
	1    8300 4450
	0    -1   -1   0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 60E6F992
P 8300 4550
AR Path="/5F05E355/60E6F992" Ref="R?"  Part="1" 
AR Path="/5E9C6910/60E6F992" Ref="R?"  Part="1" 
AR Path="/5EA5D9E8/60E6F992" Ref="R12"  Part="1" 
F 0 "R12" V 8350 4675 50  0000 C CNN
F 1 "10" V 8350 4450 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 8300 4550 50  0001 C CNN
F 3 "~" H 8300 4550 50  0001 C CNN
F 4 "MCWR04X10R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 10 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 8300 4150 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x10r0ftl/res-10r-1-0-0625w-0402-thick-film/dp/2447098" H 8300 4050 50  0001 C CNN "Distributor Link"
	1    8300 4550
	0    -1   -1   0   
$EndComp
Text Notes 6150 2025 0    100  ~ 0
Differential Hall Connector
Wire Notes Line
	9175 1825 9175 3400
Wire Notes Line
	9175 3400 6075 3400
Wire Notes Line
	6075 3400 6075 1825
Wire Notes Line
	6075 1825 9175 1825
Text Notes 6150 3675 0    100  ~ 0
Single-Ended Hall Connector
Wire Notes Line
	6075 5050 6075 3475
Wire Notes Line
	6075 3475 9175 3475
Wire Wire Line
	7650 4050 7650 4000
$Comp
L PMSM_Driver_LV_Board_Power:ISO_SENS_5V0 #PWR032
U 1 1 60E6F9A1
P 7650 4000
F 0 "#PWR032" H 7650 4250 50  0001 C CNN
F 1 "ISO_SENS_5V0" H 7651 4173 50  0000 C CNN
F 2 "" H 7650 4000 50  0001 C CNN
F 3 "" H 7650 4000 50  0001 C CNN
	1    7650 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	7900 4350 8200 4350
Connection ~ 7900 4350
Wire Wire Line
	8200 4450 7650 4450
Connection ~ 7650 4450
Wire Wire Line
	7400 4550 8200 4550
Connection ~ 7400 4550
Wire Wire Line
	8400 4350 8475 4350
Wire Wire Line
	8400 4450 8475 4450
Wire Wire Line
	8400 4550 8475 4550
Wire Notes Line
	9175 3475 9175 5050
Wire Notes Line
	9175 5050 6075 5050
Text GLabel 9450 1350 0    50   Input ~ 0
ISO_LOT
Text GLabel 9450 1250 0    50   Input ~ 0
ISO_DOS
Text GLabel 10550 1175 0    50   Output ~ 0
ISO_COS
Text GLabel 10550 1275 0    50   Output ~ 0
ISO_COS_LO
Text GLabel 10550 975  0    50   Output ~ 0
ISO_SIN
Text GLabel 10550 1075 0    50   Output ~ 0
ISO_SIN_LO
Text GLabel 10550 1375 0    50   Input ~ 0
ISO_EXC
Text GLabel 10550 1475 0    50   Input ~ 0
ISO_EXC_LO
$Comp
L PMSM_Driver_LV_Board_Connector:PinHeader_2211S-03G J11
U 1 1 618C3CE7
P 8875 1300
F 0 "J11" H 8825 1525 50  0000 L CNN
F 1 "PinHeader_2211S-03G" H 8500 1100 25  0000 L CNN
F 2 "PMSM_Driver_LV_Board:PinHeader_1x03_P2.54mm_Vertical" H 8875 950 31  0001 C CNN
F 3 "~" H 8875 1300 50  0001 C CNN
F 4 "2211S-03G -  Board-To-Board Connector, 2.54 mm, 3 Contacts, Header, 2211S Series, Through Hole, 1 Rows " H 8875 900 31  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/2211s-03g/header-1-row-vert-3way/dp/1593412" H 8875 850 31  0001 C CNN "Distributor Link"
	1    8875 1300
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Connector:PinHeader_2211S-02G J8
U 1 1 61908631
P 7200 1300
F 0 "J8" H 7150 1450 50  0000 L CNN
F 1 "PinHeader_2211S-02G" H 6950 1150 25  0000 L CNN
F 2 "PMSM_Driver_LV_Board:PinHeader_1x02_P2.54mm_Vertical" H 7200 950 31  0001 C CNN
F 3 "~" H 7200 1300 50  0001 C CNN
F 4 "2211S-02G -  Board-To-Board Connector, 2.54 mm, 2 Contacts, Header, 2211S Series, Through Hole, 1 Rows" H 7200 900 31  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/2211s-02g/header-1-row-vert-2way/dp/1593411" H 7200 850 31  0001 C CNN "Distributor Link"
	1    7200 1300
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:ISO_DGND #PWR028
U 1 1 6196D79E
P 6500 1450
F 0 "#PWR028" H 6500 1200 50  0001 C CNN
F 1 "ISO_DGND" H 6505 1277 50  0000 C CNN
F 2 "" H 6500 1450 50  0001 C CNN
F 3 "" H 6500 1450 50  0001 C CNN
	1    6500 1450
	1    0    0    -1  
$EndComp
Wire Wire Line
	6500 1450 6500 1350
Wire Wire Line
	8675 1200 8575 1200
Wire Wire Line
	8575 1200 8575 1100
$Comp
L PMSM_Driver_LV_Board_Power:ISO_RES_VEXC #PWR036
U 1 1 61ACEEFB
P 7975 1100
F 0 "#PWR036" H 7975 1350 50  0001 C CNN
F 1 "ISO_RES_VEXC" H 7976 1273 50  0000 C CNN
F 2 "" H 7975 1100 50  0001 C CNN
F 3 "" H 7975 1100 50  0001 C CNN
	1    7975 1100
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:ISO_RES_VEXT #PWR027
U 1 1 61AF21FF
P 6500 1100
F 0 "#PWR027" H 6500 1350 50  0001 C CNN
F 1 "ISO_RES_VEXT" H 6501 1273 50  0000 C CNN
F 2 "" H 6500 1100 50  0001 C CNN
F 3 "" H 6500 1100 50  0001 C CNN
	1    6500 1100
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:ISO_RES_VEXT #PWR033
U 1 1 61B3A1C0
P 7675 1300
F 0 "#PWR033" H 7675 1550 50  0001 C CNN
F 1 "ISO_RES_VEXT" H 7676 1473 50  0000 C CNN
F 2 "" H 7675 1300 50  0001 C CNN
F 3 "" H 7675 1300 50  0001 C CNN
	1    7675 1300
	1    0    0    -1  
$EndComp
Wire Wire Line
	7975 1300 8225 1300
Wire Wire Line
	7675 1400 7675 1300
Text Notes 6700 1650 0    25   ~ 0
External power supply for resolver
Text Notes 8125 1675 0    25   ~ 0
Select power supply for resolver - either\nexternal supply or internal 15V supply.
Wire Notes Line
	6075 1750 6075 600 
Wire Notes Line
	6075 600  11100 600 
Wire Notes Line
	11100 600  11100 1750
Wire Notes Line
	11100 1750 6075 1750
Text Notes 6150 800  0    100  ~ 0
Resolver Connectors
$Comp
L PMSM_Driver_LV_Board_Connector:PinHeader_2211S-02G J12
U 1 1 61CDDFBC
P 9750 1300
F 0 "J12" H 9700 1450 50  0000 L CNN
F 1 "PinHeader_2211S-02G" H 9375 1150 25  0000 L CNN
F 2 "PMSM_Driver_LV_Board:PinHeader_1x02_P2.54mm_Vertical" H 9750 950 31  0001 C CNN
F 3 "~" H 9750 1300 50  0001 C CNN
F 4 "2211S-02G -  Board-To-Board Connector, 2.54 mm, 2 Contacts, Header, 2211S Series, Through Hole, 1 Rows" H 9750 900 31  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/2211s-02g/header-1-row-vert-2way/dp/1593411" H 9750 850 31  0001 C CNN "Distributor Link"
F 6 "DNI" H 9925 1225 50  0000 C CNN "DNI"
	1    9750 1300
	1    0    0    -1  
$EndComp
Wire Wire Line
	9550 1250 9450 1250
Wire Wire Line
	9550 1350 9450 1350
Text Notes 9225 1675 0    25   ~ 0
Resolver diagnostics:\n  DOS (degradation of signal)\n  LOT (loss of tracking)
$Comp
L PMSM_Driver_LV_Board_Connector:PinHeader_2211S-06G J15
U 1 1 61EAB81C
P 10850 1225
F 0 "J15" H 10800 1600 50  0000 L CNN
F 1 "PinHeader_2211S-06G" H 10475 875 25  0000 L CNN
F 2 "PMSM_Driver_LV_Board:PinHeader_1x06_P2.54mm_Vertical" H 10850 725 31  0001 C CNN
F 3 "~" H 10450 1075 50  0001 C CNN
F 4 "2211S-06G -  Board-To-Board Connector, 2.54 mm, 6 Contacts, Header, 2211S Series, Through Hole, 1 Rows" H 10850 675 31  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/2211s-06g/header-tht-vertical-2-54mm-6way/dp/1593415" H 10850 625 31  0001 C CNN "Distributor Link"
	1    10850 1225
	1    0    0    -1  
$EndComp
Wire Wire Line
	10650 975  10550 975 
Wire Wire Line
	10650 1075 10550 1075
Wire Wire Line
	10650 1175 10550 1175
Wire Wire Line
	10650 1275 10550 1275
Wire Wire Line
	10650 1375 10550 1375
Wire Wire Line
	10650 1475 10550 1475
Text Notes 10525 1675 0    25   ~ 0
Resolver Connector
Text Notes 675  7000 0    100  ~ 0
Motor Connector
Wire Notes Line
	600  6800 2825 6800
Wire Notes Line
	2825 6800 2825 7675
Wire Notes Line
	2825 7675 600  7675
Wire Notes Line
	600  7675 600  6800
Text GLabel 1750 7400 0    50   Input ~ 0
MOT_PHASE_A
Text GLabel 1750 7300 0    50   Input ~ 0
MOT_PHASE_B
Text GLabel 1750 7200 0    50   Input ~ 0
MOT_PHASE_C
$Comp
L PMSM_Driver_LV_Board_Connector:PinHeader_MSTBVA2,5_3-G-5,08 J1
U 1 1 6200FB9E
P 2050 7300
F 0 "J1" H 2000 7500 50  0000 L CNN
F 1 "PinHeader_MSTBVA2,5_3-G-5,08" H 875 7075 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:PhoenixContact_MSTBVA_2,5_3-G-5,08_1x03_P5.08mm_Vertical" H 2050 6950 31  0001 C CNN
F 3 "~" H 2050 7300 50  0001 C CNN
F 4 "MSTBVA2,5/3-G-5,08 -  Terminal Block, Header, 5.08 mm, 3 Ways, 12 A, 250 V, Through Hole Vertical" H 2050 6900 31  0001 C CNN "Description"
F 5 "https://www.mouser.co.uk/ProductDetail/Amphenol-Anytek/OQ0353510000G?qs=sGAEpiMZZMsDddcp1dBDJB9vmTXOlQqi%252BLMzpiSdHMi9Fy13mU7oxw%3D%3D" H 2050 6850 31  0001 C CNN "Distributor Link"
F 6 "https://uk.farnell.com/weidmuller/1146750000/terminal-block-header-3pos-th/dp/2509469" H 2050 7300 50  0001 C CNN "Alternative Link"
	1    2050 7300
	1    0    0    -1  
$EndComp
Wire Wire Line
	1750 7400 1850 7400
Wire Wire Line
	1750 7300 1850 7300
Wire Wire Line
	1750 7200 1850 7200
Wire Notes Line
	9250 1825 11100 1825
Wire Notes Line
	11100 1825 11100 3400
Wire Notes Line
	11100 3400 9250 3400
Wire Notes Line
	9250 3400 9250 1825
Text Notes 9325 2025 0    100  ~ 0
SPI Interface
Text Notes 9350 2100 0    25   ~ 0
For auxiliary sensor modules.
Wire Notes Line
	9250 3475 11100 3475
Wire Notes Line
	11100 3475 11100 5050
Wire Notes Line
	11100 5050 9250 5050
Wire Notes Line
	9250 5050 9250 3475
Text Notes 9325 3675 0    100  ~ 0
I2C Interface
Text Notes 9350 3750 0    25   ~ 0
For auxiliary sensor modules.
Text GLabel 10350 2825 2    50   Output ~ 0
ISO_SPI_MISO
Text GLabel 10350 2725 2    50   Input ~ 0
ISO_SPI_MOSI
Text GLabel 10350 2625 2    50   Input ~ 0
ISO_SPI_SCK
Text GLabel 10425 4400 2    50   BiDi ~ 0
ISO_SDA
Text GLabel 10425 4300 2    50   Output ~ 0
ISO_SCL
Text GLabel 10350 2925 2    50   Input ~ 0
ISO_SPI_SEL_AUX
$Comp
L PMSM_Driver_LV_Board_Power:ISO_SENS_5V0 #PWR040
U 1 1 6212FFD8
P 10250 2425
F 0 "#PWR040" H 10250 2675 50  0001 C CNN
F 1 "ISO_SENS_5V0" H 10251 2598 50  0000 C CNN
F 2 "" H 10250 2425 50  0001 C CNN
F 3 "" H 10250 2425 50  0001 C CNN
	1    10250 2425
	1    0    0    -1  
$EndComp
Text Notes 3025 5325 0    100  ~ 0
Motor Power Supply Connector
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_DGND #PWR04
U 1 1 5EBF9467
P 2550 3075
F 0 "#PWR04" H 2550 2825 50  0001 C CNN
F 1 "CTRL_DGND" H 2555 2902 50  0000 C CNN
F 2 "" H 2550 3075 50  0001 C CNN
F 3 "" H 2550 3075 50  0001 C CNN
	1    2550 3075
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_DGND #PWR07
U 1 1 5EC33C78
P 3200 1425
F 0 "#PWR07" H 3200 1175 50  0001 C CNN
F 1 "CTRL_DGND" H 3205 1252 50  0000 C CNN
F 2 "" H 3200 1425 50  0001 C CNN
F 3 "" H 3200 1425 50  0001 C CNN
	1    3200 1425
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_DGND #PWR09
U 1 1 5EC47406
P 3675 1425
F 0 "#PWR09" H 3675 1175 50  0001 C CNN
F 1 "CTRL_DGND" H 3680 1252 50  0000 C CNN
F 2 "" H 3675 1425 50  0001 C CNN
F 3 "" H 3675 1425 50  0001 C CNN
	1    3675 1425
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_DGND #PWR014
U 1 1 5EC5AA81
P 4150 1425
F 0 "#PWR014" H 4150 1175 50  0001 C CNN
F 1 "CTRL_DGND" H 4155 1252 50  0000 C CNN
F 2 "" H 4150 1425 50  0001 C CNN
F 3 "" H 4150 1425 50  0001 C CNN
	1    4150 1425
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_DGND #PWR019
U 1 1 5EC6EAB8
P 4625 1425
F 0 "#PWR019" H 4625 1175 50  0001 C CNN
F 1 "CTRL_DGND" H 4630 1252 50  0000 C CNN
F 2 "" H 4625 1425 50  0001 C CNN
F 3 "" H 4625 1425 50  0001 C CNN
	1    4625 1425
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_DGND #PWR022
U 1 1 5EC820B2
P 5100 1425
F 0 "#PWR022" H 5100 1175 50  0001 C CNN
F 1 "CTRL_DGND" H 5105 1252 50  0000 C CNN
F 2 "" H 5100 1425 50  0001 C CNN
F 3 "" H 5100 1425 50  0001 C CNN
	1    5100 1425
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_DGND #PWR024
U 1 1 5EC957B7
P 5575 1425
F 0 "#PWR024" H 5575 1175 50  0001 C CNN
F 1 "CTRL_DGND" H 5580 1252 50  0000 C CNN
F 2 "" H 5575 1425 50  0001 C CNN
F 3 "" H 5575 1425 50  0001 C CNN
	1    5575 1425
	1    0    0    -1  
$EndComp
Wire Wire Line
	3675 1075 3675 1125
Wire Wire Line
	4150 1125 3675 1125
Connection ~ 3675 1125
Wire Wire Line
	3675 1125 3675 1175
Wire Wire Line
	3200 1125 3675 1125
Wire Wire Line
	4625 1125 5100 1125
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_DGND #PWR05
U 1 1 5EDB9D53
P 2550 6400
F 0 "#PWR05" H 2550 6150 50  0001 C CNN
F 1 "CTRL_DGND" H 2555 6227 50  0000 C CNN
F 2 "" H 2550 6400 50  0001 C CNN
F 3 "" H 2550 6400 50  0001 C CNN
	1    2550 6400
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Connector:PinHeader_1755736 J5
U 1 1 5F3E08E3
P 3400 5800
F 0 "J5" H 3400 5650 50  0000 C CNN
F 1 "PinHeader_1755736" H 3475 5950 25  0000 C CNN
F 2 "PMSM_Driver_LV_Board:PhoenixContact_MSTBVA_2,5_2-G-5,08_1x02_P5.08mm_Vertical" H 3400 5450 31  0001 C CNN
F 3 "~" H 3400 5800 50  0001 C CNN
F 4 "1755736 -  Terminal Block, Header, 5.08 mm, 2 Ways, 12 A, 250 V, Through Hole Vertical" H 3400 5400 31  0001 C CNN "Description"
F 5 "https://www.mouser.co.uk/ProductDetail/Amphenol-Anytek/OQ025A000000G?qs=sGAEpiMZZMsDddcp1dBDJB9vmTXOlQqi6qcyXmBk7R17gCcpK6bptQ%3D%3D" H 3400 5350 31  0001 C CNN "Distributor Link"
F 6 "https://uk.farnell.com/weidmuller/1146730000/terminal-block-header-2pos-th/dp/2509467" H 3400 5800 50  0001 C CNN "Alternative Link"
	1    3400 5800
	-1   0    0    1   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:CP1_Small C4
U 1 1 5F40BBB2
P 5275 5900
F 0 "C4" H 5366 5991 50  0000 L CNN
F 1 "470uF" H 5366 5900 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:CP_Radial_D12.5mm_P5.00mm" H 5275 5900 50  0001 C CNN
F 3 "~" H 5275 5900 50  0001 C CNN
F 4 "UVZ1J471MHD1TO - Aluminium Electrolytic Capacitor, 470uF, 63V, 105C" H 5275 5500 50  0001 C CNN "Description"
F 5 "https://www.mouser.co.uk/ProductDetail/Nichicon/UVZ1J471MHD1TO?qs=sGAEpiMZZMvwFf0viD3Y3TLPyGlPD9krZv1vbUyowls%3D" H 5275 5400 50  0001 C CNN "Distributor Link"
F 6 "63V" H 5366 5809 50  0000 L CNN "Voltage"
	1    5275 5900
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_PGND #PWR013
U 1 1 5F40E073
P 3650 6450
F 0 "#PWR013" H 3650 6200 50  0001 C CNN
F 1 "MOT_PGND" H 3655 6277 50  0000 C CNN
F 2 "" H 3650 6450 50  0001 C CNN
F 3 "" H 3650 6450 50  0001 C CNN
	1    3650 6450
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_PGND #PWR015
U 1 1 5F437EA2
P 5275 6450
F 0 "#PWR015" H 5275 6200 50  0001 C CNN
F 1 "MOT_PGND" H 5280 6277 50  0000 C CNN
F 2 "" H 5275 6450 50  0001 C CNN
F 3 "" H 5275 6450 50  0001 C CNN
	1    5275 6450
	1    0    0    -1  
$EndComp
Wire Wire Line
	3600 5850 3650 5850
Wire Wire Line
	5275 5750 5275 5800
$Comp
L PMSM_Driver_LV_Board_Power:MOT_VIN_HDR #PWR023
U 1 1 5F60CFCE
P 3900 5600
F 0 "#PWR023" H 3900 5850 50  0001 C CNN
F 1 "MOT_VIN_HDR" H 3901 5773 50  0000 C CNN
F 2 "" H 3900 5600 50  0001 C CNN
F 3 "" H 3900 5600 50  0001 C CNN
	1    3900 5600
	1    0    0    -1  
$EndComp
Wire Notes Line
	2900 5125 6000 5125
Wire Notes Line
	2900 6725 6000 6725
Wire Notes Line
	2900 5125 2900 6725
Wire Notes Line
	6000 5125 6000 6725
Wire Wire Line
	10250 2525 10150 2525
Wire Wire Line
	10250 2425 10250 2525
Wire Wire Line
	10150 2625 10350 2625
Wire Wire Line
	10150 2725 10350 2725
Wire Wire Line
	10150 2825 10350 2825
Wire Wire Line
	10150 2925 10350 2925
$Comp
L PMSM_Driver_LV_Board_Power:ISO_DGND #PWR038
U 1 1 5F97C353
P 9550 3025
F 0 "#PWR038" H 9550 2775 50  0001 C CNN
F 1 "ISO_DGND" H 9555 2852 50  0000 C CNN
F 2 "" H 9550 3025 50  0001 C CNN
F 3 "" H 9550 3025 50  0001 C CNN
	1    9550 3025
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Connector:Connector_MC-254-10-00-ST-DIP J13
U 1 1 5FA333DA
P 9950 2725
F 0 "J13" H 9950 3025 50  0000 C CNN
F 1 "Connector_MC-254-10-00-ST-DIP" H 9975 2425 25  0000 C CNN
F 2 "PMSM_Driver_LV_Board:IDC-Header_2x05_P2.54mm_Vertical" H 9950 2275 50  0001 C CNN
F 3 "~" H 9950 2725 50  0001 C CNN
F 4 "MC-254-10-00-ST-DIP -  Wire-To-Board Connector, 2.54 mm, 10 Contacts, Header, Through Hole, 2 Rows" H 9950 2175 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc-254-10-00-st-dip/connector-header-10pos-2row-2/dp/2843527" H 9950 2075 50  0001 C CNN "Distributor Link"
	1    9950 2725
	-1   0    0    -1  
$EndComp
Wire Wire Line
	9550 3025 9550 2925
Wire Wire Line
	9550 2925 9650 2925
Wire Wire Line
	9550 2925 9550 2825
Wire Wire Line
	9550 2825 9650 2825
Connection ~ 9550 2925
Wire Wire Line
	9550 2825 9550 2725
Wire Wire Line
	9550 2725 9650 2725
Connection ~ 9550 2825
Wire Wire Line
	9550 2725 9550 2625
Wire Wire Line
	9550 2625 9650 2625
Connection ~ 9550 2725
Wire Wire Line
	9550 2625 9550 2525
Wire Wire Line
	9550 2525 9650 2525
Connection ~ 9550 2625
$Comp
L PMSM_Driver_LV_Board_Power:ISO_SENS_5V0 #PWR041
U 1 1 5FC15C89
P 10325 4100
F 0 "#PWR041" H 10325 4350 50  0001 C CNN
F 1 "ISO_SENS_5V0" H 10326 4273 50  0000 C CNN
F 2 "" H 10325 4100 50  0001 C CNN
F 3 "" H 10325 4100 50  0001 C CNN
	1    10325 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	10325 4200 10225 4200
Wire Wire Line
	10325 4100 10325 4200
Wire Wire Line
	10225 4300 10425 4300
Wire Wire Line
	10225 4400 10425 4400
$Comp
L PMSM_Driver_LV_Board_Power:ISO_DGND #PWR039
U 1 1 5FC15C95
P 9625 4500
F 0 "#PWR039" H 9625 4250 50  0001 C CNN
F 1 "ISO_DGND" H 9630 4327 50  0000 C CNN
F 2 "" H 9625 4500 50  0001 C CNN
F 3 "" H 9625 4500 50  0001 C CNN
	1    9625 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	9625 4500 9625 4400
Wire Wire Line
	9625 4400 9725 4400
Wire Wire Line
	9625 4400 9625 4300
Wire Wire Line
	9625 4300 9725 4300
Connection ~ 9625 4400
Wire Wire Line
	9625 4300 9625 4200
Wire Wire Line
	9625 4200 9725 4200
Connection ~ 9625 4300
$Comp
L PMSM_Driver_LV_Board_Connector:Connector_MC-254-06-00-ST-DIP J14
U 1 1 5FCA0CEB
P 9975 4300
F 0 "J14" H 9925 4500 50  0000 C CNN
F 1 "Connector_MC-254-06-00-ST-DIP" H 9950 4100 25  0000 C CNN
F 2 "PMSM_Driver_LV_Board:IDC-Header_2x03_P2.54mm_Vertical" H 9975 3950 50  0001 C CNN
F 3 "~" H 9925 4300 50  0001 C CNN
F 4 "MC-254-06-00-ST-DIP -  Wire-To-Board Connector, 2.54 mm, 6 Contacts, Header, Through Hole, 2 Rows " H 9975 3900 31  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc-254-06-00-st-dip/connector-header-6pos-2row-2-54mm/dp/2843525" H 9975 3850 31  0001 C CNN "Distributor Link"
	1    9975 4300
	-1   0    0    -1  
$EndComp
Wire Notes Line
	2900 6800 6000 6800
Wire Notes Line
	6000 6800 6000 7675
Wire Notes Line
	6000 7675 2900 7675
Wire Notes Line
	2900 7675 2900 6800
Text Notes 2975 7000 0    100  ~ 0
Mounting Holes
$Comp
L Mechanical:MountingHole H1
U 1 1 5FDC97E3
P 3425 7250
F 0 "H1" H 3375 7375 50  0000 L CNN
F 1 "MountingHoleM3" H 3100 7100 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:MountingHole_3.2mm_M3_DIN965" H 3425 7250 50  0001 C CNN
F 3 "~" H 3425 7250 50  0001 C CNN
	1    3425 7250
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 5FDCA9E3
P 4100 7250
F 0 "H2" H 4050 7375 50  0000 L CNN
F 1 "MountingHoleM3" H 3775 7100 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:MountingHole_3.2mm_M3_DIN965" H 4100 7250 50  0001 C CNN
F 3 "~" H 4100 7250 50  0001 C CNN
	1    4100 7250
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H3
U 1 1 5FDE2BD7
P 4775 7250
F 0 "H3" H 4725 7375 50  0000 L CNN
F 1 "MountingHoleM3" H 4450 7100 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:MountingHole_3.2mm_M3_DIN965" H 4775 7250 50  0001 C CNN
F 3 "~" H 4775 7250 50  0001 C CNN
	1    4775 7250
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H4
U 1 1 5FDFAEDE
P 5450 7250
F 0 "H4" H 5400 7375 50  0000 L CNN
F 1 "MountingHoleM3" H 5125 7100 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:MountingHole_3.2mm_M3_DIN965" H 5450 7250 50  0001 C CNN
F 3 "~" H 5450 7250 50  0001 C CNN
	1    5450 7250
	1    0    0    -1  
$EndComp
Text Notes 3050 5825 0    25   ~ 0
Voltage range:\n12V - 48V
$Comp
L PMSM_Driver_LV_Board_Connector:PinHeader_2211S-02G J10
U 1 1 623826CE
P 8075 5800
F 0 "J10" H 8025 5950 50  0000 L CNN
F 1 "PinHeader_2211S-02G" H 7825 5650 25  0000 L CNN
F 2 "PMSM_Driver_LV_Board:PinHeader_1x02_P2.54mm_Vertical" H 8075 5450 31  0001 C CNN
F 3 "~" H 8075 5800 50  0001 C CNN
F 4 "2211S-02G -  Board-To-Board Connector, 2.54 mm, 2 Contacts, Header, 2211S Series, Through Hole, 1 Rows" H 8075 5400 31  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/2211s-02g/header-1-row-vert-2way/dp/1593411" H 8075 5350 31  0001 C CNN "Distributor Link"
	1    8075 5800
	1    0    0    -1  
$EndComp
Text GLabel 7825 5750 0    50   Output ~ 0
MOT_THERMAL_PROTECTION+
Text GLabel 7825 5850 0    50   Output ~ 0
MOT_THERMAL_PROTECTION-
Wire Wire Line
	7825 5750 7875 5750
Wire Wire Line
	7825 5850 7875 5850
Wire Notes Line
	6075 5125 9175 5125
Wire Notes Line
	9175 5125 9175 6425
Wire Notes Line
	9175 6425 6075 6425
Wire Notes Line
	6075 6425 6075 5125
Text Notes 6150 5325 0    100  ~ 0
Thermal Protection Connector
$Comp
L power:PWR_FLAG #FLG04
U 1 1 5ECD723C
P 3650 5700
F 0 "#FLG04" H 3650 5775 50  0001 C CNN
F 1 "PWR_FLAG" H 3650 5873 50  0000 C CNN
F 2 "" H 3650 5700 50  0001 C CNN
F 3 "~" H 3650 5700 50  0001 C CNN
	1    3650 5700
	1    0    0    -1  
$EndComp
Wire Wire Line
	5100 1125 5575 1125
$Comp
L power:PWR_FLAG #FLG01
U 1 1 5EE80BE8
P 875 3075
F 0 "#FLG01" H 875 3150 50  0001 C CNN
F 1 "PWR_FLAG" H 875 3248 50  0000 C CNN
F 2 "" H 875 3075 50  0001 C CNN
F 3 "~" H 875 3075 50  0001 C CNN
	1    875  3075
	1    0    0    -1  
$EndComp
Wire Wire Line
	3200 1425 3200 1375
Wire Wire Line
	875  3075 875  3125
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_DGND #PWR02
U 1 1 5EEB475F
P 875 3125
F 0 "#PWR02" H 875 2875 50  0001 C CNN
F 1 "CTRL_DGND" H 880 2952 50  0000 C CNN
F 2 "" H 875 3125 50  0001 C CNN
F 3 "" H 875 3125 50  0001 C CNN
	1    875  3125
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG03
U 1 1 5EF38DA8
P 1600 2550
F 0 "#FLG03" H 1600 2625 50  0001 C CNN
F 1 "PWR_FLAG" H 1600 2723 50  0000 C CNN
F 2 "" H 1600 2550 50  0001 C CNN
F 3 "~" H 1600 2550 50  0001 C CNN
	1    1600 2550
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG02
U 1 1 5EF6B472
P 1025 2050
F 0 "#FLG02" H 1025 2125 50  0001 C CNN
F 1 "PWR_FLAG" H 1025 2223 50  0000 C CNN
F 2 "" H 1025 2050 50  0001 C CNN
F 3 "~" H 1025 2050 50  0001 C CNN
	1    1025 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	1150 2675 1600 2675
Wire Wire Line
	1600 2550 1600 2675
Wire Wire Line
	1600 2675 1850 2675
Connection ~ 1600 2675
Connection ~ 1850 2675
Wire Wire Line
	825  2275 825  1825
Wire Wire Line
	825  2275 1025 2275
Wire Wire Line
	1025 2050 1025 2275
Connection ~ 1025 2275
Wire Wire Line
	1025 2275 1850 2275
$Comp
L power:PWR_FLAG #FLG?
U 1 1 606EF732
P 6750 1200
AR Path="/5ED47D19/606EF732" Ref="#FLG?"  Part="1" 
AR Path="/5EA5D9E8/606EF732" Ref="#FLG06"  Part="1" 
F 0 "#FLG06" H 6750 1275 50  0001 C CNN
F 1 "PWR_FLAG" H 6750 1373 50  0000 C CNN
F 2 "" H 6750 1200 50  0001 C CNN
F 3 "~" H 6750 1200 50  0001 C CNN
	1    6750 1200
	-1   0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG?
U 1 1 606EF72C
P 8225 1225
AR Path="/5ED47D19/606EF72C" Ref="#FLG?"  Part="1" 
AR Path="/5EA5D9E8/606EF72C" Ref="#FLG07"  Part="1" 
F 0 "#FLG07" H 8225 1300 50  0001 C CNN
F 1 "PWR_FLAG" H 8225 1398 50  0000 C CNN
F 2 "" H 8225 1225 50  0001 C CNN
F 3 "~" H 8225 1225 50  0001 C CNN
	1    8225 1225
	-1   0    0    -1  
$EndComp
Wire Wire Line
	7975 1100 7975 1300
Wire Wire Line
	8225 1225 8225 1300
Connection ~ 8225 1300
Wire Wire Line
	7675 1400 8675 1400
Wire Wire Line
	6500 1100 6500 1250
Wire Wire Line
	6500 1250 6750 1250
Wire Wire Line
	6750 1200 6750 1250
Wire Wire Line
	6500 1350 7000 1350
Connection ~ 6750 1250
Wire Wire Line
	6750 1250 7000 1250
Wire Wire Line
	8225 1300 8675 1300
$Comp
L PMSM_Driver_LV_Board_Power:MOT_15V0 #PWR?
U 1 1 61619FEC
P 8575 1100
AR Path="/5EA5E06A/61619FEC" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/61619FEC" Ref="#PWR?"  Part="1" 
AR Path="/5FE92F3B/61619FEC" Ref="#PWR?"  Part="1" 
AR Path="/5EA5D9E8/61619FEC" Ref="#PWR037"  Part="1" 
F 0 "#PWR037" H 8575 1350 50  0001 C CNN
F 1 "MOT_15V0" H 8576 1273 50  0000 C CNN
F 2 "" H 8575 1100 50  0001 C CNN
F 3 "" H 8575 1100 50  0001 C CNN
	1    8575 1100
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:D_Zener_Small D?
U 1 1 60C954AB
P 4400 5900
AR Path="/5EA5E295/60C954AB" Ref="D?"  Part="1" 
AR Path="/5EA5D9E8/60C954AB" Ref="D1"  Part="1" 
F 0 "D1" V 4400 5950 50  0000 L CNN
F 1 "1SMB5927BT3G" V 4475 5900 25  0000 L CNN
F 2 "PMSM_Driver_LV_Board:D_SMB" V 4400 5900 50  0001 C CNN
F 3 "~" V 4400 5900 50  0001 C CNN
F 4 "1SMB5927BT3G -  Zener Single Diode, General Purpose, 12 V, 3 W, DO-214AA (SMB), 5 %, 2 Pins, 150 °C" H 4400 5700 31  0001 C CNN "Description"
F 5 "https://uk.farnell.com/on-semiconductor/1smb5927bt3g/diode-zener-12v-3w/dp/1431169" H 4400 5600 31  0001 C CNN "Distributor Link"
	1    4400 5900
	0    1    1    0   
$EndComp
Wire Wire Line
	4400 5800 4400 5750
Wire Wire Line
	4400 5750 4350 5750
Wire Wire Line
	4150 6050 4150 6100
Wire Wire Line
	4150 6100 4400 6100
Wire Wire Line
	4400 6100 4400 6000
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 60C954B8
P 4150 6250
AR Path="/5F05E355/60C954B8" Ref="R?"  Part="1" 
AR Path="/5E9C6910/60C954B8" Ref="R?"  Part="1" 
AR Path="/5EFE605D/60C954B8" Ref="R?"  Part="1" 
AR Path="/5FD7CF82/60C954B8" Ref="R?"  Part="1" 
AR Path="/5EA5E295/60C954B8" Ref="R?"  Part="1" 
AR Path="/5EA5D9E8/60C954B8" Ref="R179"  Part="1" 
F 0 "R179" H 4300 6225 50  0000 C CNN
F 1 "2k" H 4250 6300 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 4150 6250 50  0001 C CNN
F 3 "~" H 4150 6250 50  0001 C CNN
F 4 "MCWR04X2001FTL -  SMD Chip Resistor, 0402 [1005 Metric], 2 kohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 4150 5850 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x2001ftl/res-2k-1-0-0625w-thick-film/dp/2447147" H 4150 5750 50  0001 C CNN "Distributor Link"
	1    4150 6250
	-1   0    0    1   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 60C954C0
P 4400 6250
AR Path="/5F05E355/60C954C0" Ref="R?"  Part="1" 
AR Path="/5E9C6910/60C954C0" Ref="R?"  Part="1" 
AR Path="/5EFE605D/60C954C0" Ref="R?"  Part="1" 
AR Path="/5FD7CF82/60C954C0" Ref="R?"  Part="1" 
AR Path="/5EA5E295/60C954C0" Ref="R?"  Part="1" 
AR Path="/5EA5D9E8/60C954C0" Ref="R180"  Part="1" 
F 0 "R180" H 4250 6225 50  0000 C CNN
F 1 "2k" H 4300 6300 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 4400 6250 50  0001 C CNN
F 3 "~" H 4400 6250 50  0001 C CNN
F 4 "MCWR04X2001FTL -  SMD Chip Resistor, 0402 [1005 Metric], 2 kohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 4400 5850 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x2001ftl/res-2k-1-0-0625w-thick-film/dp/2447147" H 4400 5750 50  0001 C CNN "Distributor Link"
	1    4400 6250
	-1   0    0    1   
$EndComp
Wire Wire Line
	4150 6150 4150 6100
Connection ~ 4150 6100
Wire Wire Line
	4400 6150 4400 6100
Connection ~ 4400 6100
Wire Wire Line
	4150 6350 4150 6400
Wire Wire Line
	4150 6400 4275 6400
Wire Wire Line
	4400 6400 4400 6350
Wire Wire Line
	4275 6400 4275 6450
Connection ~ 4275 6400
Wire Wire Line
	4275 6400 4400 6400
$Comp
L PMSM_Driver_LV_Board_Power:MOT_PGND #PWR?
U 1 1 60C954D0
P 4275 6450
AR Path="/5EA5E295/60C954D0" Ref="#PWR?"  Part="1" 
AR Path="/5EA5D9E8/60C954D0" Ref="#PWR020"  Part="1" 
F 0 "#PWR020" H 4275 6200 50  0001 C CNN
F 1 "MOT_PGND" H 4280 6277 50  0000 C CNN
F 2 "" H 4275 6450 50  0001 C CNN
F 3 "" H 4275 6450 50  0001 C CNN
	1    4275 6450
	1    0    0    -1  
$EndComp
Wire Wire Line
	4400 5750 4775 5750
Connection ~ 4400 5750
Text Notes 3950 5575 0    25   ~ 0
Reverse polarity protection
$Comp
L PMSM_Driver_LV_Board_Device:Q_PMOS_GDS Q?
U 1 1 60C954DB
P 4150 5850
AR Path="/5EA5E295/60C954DB" Ref="Q?"  Part="1" 
AR Path="/5EA5D9E8/60C954DB" Ref="Q12"  Part="1" 
F 0 "Q12" V 4375 5850 50  0000 C CNN
F 1 "SPB80P06PGATMA1" V 4075 6050 25  0000 C CNN
F 2 "PMSM_Driver_LV_Board:TO-263-2" H 4350 5950 50  0001 C CNN
F 3 "~" H 4150 5850 50  0001 C CNN
F 4 "SPB80P06PGATMA1 -  Power MOSFET, P Channel, 60 V, 80 A, 0.021 ohm, TO-263 (D2PAK), Surface Mount" H 4150 5350 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/infineon/spb80p06pgatma1/mosfet-p-ch-60v-80a-to-263/dp/2212850" H 4150 5250 50  0001 C CNN "Distributor Link"
	1    4150 5850
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4775 5750 4775 5800
$Comp
L PMSM_Driver_LV_Board_Power:MOT_PGND #PWR025
U 1 1 60C954E3
P 4775 6450
AR Path="/5EA5D9E8/60C954E3" Ref="#PWR025"  Part="1" 
AR Path="/5EA5E295/60C954E3" Ref="#PWR?"  Part="1" 
F 0 "#PWR025" H 4775 6200 50  0001 C CNN
F 1 "MOT_PGND" H 4780 6277 50  0000 C CNN
F 2 "" H 4775 6450 50  0001 C CNN
F 3 "" H 4775 6450 50  0001 C CNN
	1    4775 6450
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:D_Small D30
U 1 1 60C954EB
P 4775 5900
AR Path="/5EA5D9E8/60C954EB" Ref="D30"  Part="1" 
AR Path="/5EA5E295/60C954EB" Ref="D?"  Part="1" 
F 0 "D30" V 4775 6025 50  0000 C CNN
F 1 "SMCJ48A" V 4850 6075 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:D_SMC" V 4775 5900 50  0001 C CNN
F 3 "~" V 4775 5900 50  0001 C CNN
F 4 "SMCJ48A -  TVS Diode, TRANSZORB SMCJ Series, Unidirectional, 48 V, 77.4 V, DO-214AB (SMC), 2 Pins" H 4775 5900 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/taiwan-semiconductor/smcj48a/diode-tvs-48v-1500w-smc/dp/7277636" H 4775 5900 50  0001 C CNN "Distributor Link"
	1    4775 5900
	0    1    1    0   
$EndComp
Text Notes 4675 5725 0    25   ~ 0
Transient voltage\nsuppression
Connection ~ 4775 5750
Wire Wire Line
	4775 6000 4775 6450
Wire Wire Line
	4775 5750 5275 5750
Wire Wire Line
	5275 6000 5275 6450
Text Notes 5200 5725 0    25   ~ 0
Bulk\ncapacitor
$Comp
L PMSM_Driver_LV_Board_Power:MOT_VIN #PWR079
U 1 1 60F38624
P 5675 5600
F 0 "#PWR079" H 5675 5850 50  0001 C CNN
F 1 "MOT_VIN" H 5676 5773 50  0000 C CNN
F 2 "" H 5675 5600 50  0001 C CNN
F 3 "" H 5675 5600 50  0001 C CNN
	1    5675 5600
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG05
U 1 1 60FA26A0
P 3450 6350
F 0 "#FLG05" H 3450 6425 50  0001 C CNN
F 1 "PWR_FLAG" H 3450 6523 50  0000 C CNN
F 2 "" H 3450 6350 50  0001 C CNN
F 3 "~" H 3450 6350 50  0001 C CNN
	1    3450 6350
	1    0    0    -1  
$EndComp
Wire Wire Line
	3600 5750 3650 5750
Wire Wire Line
	3650 5700 3650 5750
Connection ~ 3650 5750
Wire Wire Line
	3650 5750 3900 5750
Wire Wire Line
	3900 5600 3900 5750
Connection ~ 3900 5750
Wire Wire Line
	3900 5750 3950 5750
Wire Wire Line
	3450 6350 3450 6400
Wire Wire Line
	3450 6400 3650 6400
Connection ~ 3650 6400
Wire Wire Line
	3650 6400 3650 6450
Wire Wire Line
	3650 5850 3650 6400
$Comp
L power:PWR_FLAG #FLG032
U 1 1 610C5F6A
P 5425 5700
F 0 "#FLG032" H 5425 5775 50  0001 C CNN
F 1 "PWR_FLAG" H 5425 5873 50  0000 C CNN
F 2 "" H 5425 5700 50  0001 C CNN
F 3 "~" H 5425 5700 50  0001 C CNN
	1    5425 5700
	1    0    0    -1  
$EndComp
Wire Wire Line
	5425 5700 5425 5750
Wire Wire Line
	5425 5750 5275 5750
Connection ~ 5275 5750
Wire Wire Line
	5675 5600 5675 5750
Wire Wire Line
	5675 5750 5425 5750
Connection ~ 5425 5750
$EndSCHEMATC
