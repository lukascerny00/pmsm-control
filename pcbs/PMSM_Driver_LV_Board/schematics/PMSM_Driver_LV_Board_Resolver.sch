EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 7 16
Title "PMSM_Driver_LV_Board"
Date "2020-03-30"
Rev "A"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L PMSM_Driver_LV_Board_Power:ISO_SENS_5V0 #PWR?
U 1 1 60ED9086
P 2950 1100
AR Path="/6068445B/60ED9086" Ref="#PWR?"  Part="1" 
AR Path="/60EB5E32/60ED9086" Ref="#PWR?"  Part="1" 
AR Path="/5EA5DFF7/60ED9086" Ref="#PWR0167"  Part="1" 
F 0 "#PWR0167" H 2950 1350 50  0001 C CNN
F 1 "ISO_SENS_5V0" H 2951 1273 50  0000 C CNN
F 2 "" H 2950 1100 50  0001 C CNN
F 3 "" H 2950 1100 50  0001 C CNN
	1    2950 1100
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:ISO_RDC_5V0 #PWR0169
U 1 1 60ED9675
P 3600 1100
F 0 "#PWR0169" H 3600 1350 50  0001 C CNN
F 1 "ISO_RDC_5V0" H 3601 1273 50  0000 C CNN
F 2 "" H 3600 1100 50  0001 C CNN
F 3 "" H 3600 1100 50  0001 C CNN
	1    3600 1100
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:ISO_DGND #PWR?
U 1 1 60EDB842
P 4875 1600
AR Path="/5F05E355/60EDB842" Ref="#PWR?"  Part="1" 
AR Path="/6068445B/60EDB842" Ref="#PWR?"  Part="1" 
AR Path="/60EB5E32/60EDB842" Ref="#PWR?"  Part="1" 
AR Path="/5EA5DFF7/60EDB842" Ref="#PWR0173"  Part="1" 
F 0 "#PWR0173" H 4875 1350 50  0001 C CNN
F 1 "ISO_DGND" H 4880 1427 50  0000 C CNN
F 2 "" H 4875 1600 50  0001 C CNN
F 3 "" H 4875 1600 50  0001 C CNN
	1    4875 1600
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 60EDC8C8
P 4200 1400
AR Path="/5E9C6910/60EDC8C8" Ref="C?"  Part="1" 
AR Path="/5EBC6654/60EDC8C8" Ref="C?"  Part="1" 
AR Path="/5EBC681B/60EDC8C8" Ref="C?"  Part="1" 
AR Path="/5F05E355/60EDC8C8" Ref="C?"  Part="1" 
AR Path="/6068445B/60EDC8C8" Ref="C?"  Part="1" 
AR Path="/60EB5E32/60EDC8C8" Ref="C?"  Part="1" 
AR Path="/5EA5DFF7/60EDC8C8" Ref="C59"  Part="1" 
F 0 "C59" H 3975 1400 50  0000 L CNN
F 1 "10nF" H 3925 1475 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 4200 1400 50  0001 C CNN
F 3 "~" H 4200 1400 50  0001 C CNN
F 4 "0402B103K100CT -  SMD Multilayer Ceramic Capacitor, General Purpose, 10000 pF, 10 V, 0402 [1005 Metric], ± 10%, X7R" H 4200 1000 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/walsin/0402b103k100ct/cap-0-01-f-10v-10-x7r-0402-reel/dp/2524673" H 4200 900 50  0001 C CNN "Distributor Link"
	1    4200 1400
	-1   0    0    1   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 60EE21DE
P 3875 1400
AR Path="/5E9C6910/60EE21DE" Ref="C?"  Part="1" 
AR Path="/5EBC6654/60EE21DE" Ref="C?"  Part="1" 
AR Path="/5EBC681B/60EE21DE" Ref="C?"  Part="1" 
AR Path="/5F05E355/60EE21DE" Ref="C?"  Part="1" 
AR Path="/6068445B/60EE21DE" Ref="C?"  Part="1" 
AR Path="/60EB5E32/60EE21DE" Ref="C?"  Part="1" 
AR Path="/5EA5DFF7/60EE21DE" Ref="C58"  Part="1" 
F 0 "C58" H 3650 1400 50  0000 L CNN
F 1 "4.7uF" H 3600 1475 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 3875 1400 50  0001 C CNN
F 3 "~" H 3875 1400 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 3875 1000 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/samsung-electro-mechanics/cl10a475kq8nnnc/cap-4-7uf-6-3v-mlcc-0603/dp/3013399" H 3875 900 50  0001 C CNN "Distributor Link"
	1    3875 1400
	-1   0    0    1   
$EndComp
Wire Wire Line
	4875 1500 4875 1550
$Comp
L PMSM_Driver_LV_Board_IC:AD2S1210DSTZ U23
U 1 1 60ED322F
P 3750 3050
F 0 "U23" H 3250 4350 50  0000 C CNN
F 1 "AD2S1210DSTZ" H 4100 1700 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:LQFP-48_7x7mm_P0.5mm" H 3750 1200 31  0001 C CNN
F 3 "~" H 3350 4350 31  0001 C CNN
F 4 "AD2S1210DSTZ -  Analogue to Digital Converter, 16 bit, Differential, Single Ended, Parallel, Serial, Single, 4.75 V" H 3750 1150 31  0001 C CNN "Description"
F 5 "https://uk.farnell.com/analog-devices/ad2s1210dstz/16-bit-prog-resolver-digital-conerter/dp/2376658" H 3750 1100 31  0001 C CNN "Distributor Link"
F 6 "https://www.mouser.co.uk/ProductDetail/Analog-Devices/AD2S1210DSTZ?qs=%2Fha2pyFadugM5l3x%2FZl8N71dyx2NDFHrehxVU3ugAcfyQZqTjhm1uQ%3D%3D" H 3750 1050 31  0001 C CNN "Distributor Link 2"
	1    3750 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	3875 1500 3875 1550
Wire Wire Line
	3875 1550 4200 1550
Connection ~ 4875 1550
Wire Wire Line
	4875 1550 4875 1600
Wire Wire Line
	4200 1500 4200 1550
Connection ~ 4200 1550
Wire Wire Line
	4200 1550 4550 1550
Wire Wire Line
	4550 1500 4550 1550
Connection ~ 4550 1550
Wire Wire Line
	4550 1550 4875 1550
Wire Wire Line
	3600 1100 3600 1250
Wire Wire Line
	3875 1300 3875 1250
Wire Wire Line
	3875 1250 4200 1250
Wire Wire Line
	4200 1250 4200 1300
Wire Wire Line
	4200 1250 4550 1250
Wire Wire Line
	4550 1250 4550 1300
Connection ~ 4200 1250
Wire Wire Line
	4550 1250 4875 1250
Wire Wire Line
	4875 1250 4875 1300
Connection ~ 4550 1250
Wire Wire Line
	3875 1250 3600 1250
Connection ~ 3875 1250
Connection ~ 3600 1250
Wire Wire Line
	3600 1250 3600 1650
Wire Wire Line
	3750 1700 3750 1650
Wire Wire Line
	3750 1650 3600 1650
Connection ~ 3600 1650
Wire Wire Line
	3600 1650 3600 1700
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 60EEC0FC
P 4550 1400
AR Path="/5E9C6910/60EEC0FC" Ref="C?"  Part="1" 
AR Path="/5EBC6654/60EEC0FC" Ref="C?"  Part="1" 
AR Path="/5EBC681B/60EEC0FC" Ref="C?"  Part="1" 
AR Path="/5F05E355/60EEC0FC" Ref="C?"  Part="1" 
AR Path="/6068445B/60EEC0FC" Ref="C?"  Part="1" 
AR Path="/60EB5E32/60EEC0FC" Ref="C?"  Part="1" 
AR Path="/5EA5DFF7/60EEC0FC" Ref="C60"  Part="1" 
F 0 "C60" H 4325 1400 50  0000 L CNN
F 1 "4.7uF" H 4275 1475 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4550 1400 50  0001 C CNN
F 3 "~" H 4550 1400 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 4550 1000 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/samsung-electro-mechanics/cl10a475kq8nnnc/cap-4-7uf-6-3v-mlcc-0603/dp/3013399" H 4550 900 50  0001 C CNN "Distributor Link"
	1    4550 1400
	-1   0    0    1   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 60EEC4DB
P 4875 1400
AR Path="/5E9C6910/60EEC4DB" Ref="C?"  Part="1" 
AR Path="/5EBC6654/60EEC4DB" Ref="C?"  Part="1" 
AR Path="/5EBC681B/60EEC4DB" Ref="C?"  Part="1" 
AR Path="/5F05E355/60EEC4DB" Ref="C?"  Part="1" 
AR Path="/6068445B/60EEC4DB" Ref="C?"  Part="1" 
AR Path="/60EB5E32/60EEC4DB" Ref="C?"  Part="1" 
AR Path="/5EA5DFF7/60EEC4DB" Ref="C61"  Part="1" 
F 0 "C61" H 4650 1400 50  0000 L CNN
F 1 "10nF" H 4600 1475 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 4875 1400 50  0001 C CNN
F 3 "~" H 4875 1400 50  0001 C CNN
F 4 "0402B103K100CT -  SMD Multilayer Ceramic Capacitor, General Purpose, 10000 pF, 10 V, 0402 [1005 Metric], ± 10%, X7R" H 4875 1000 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/walsin/0402b103k100ct/cap-0-01-f-10v-10-x7r-0402-reel/dp/2524673" H 4875 900 50  0001 C CNN "Distributor Link"
	1    4875 1400
	-1   0    0    1   
$EndComp
Wire Wire Line
	3100 3150 2950 3150
Wire Wire Line
	2950 3150 2950 1250
Wire Wire Line
	3100 3800 2950 3800
Wire Wire Line
	2950 3800 2950 3150
Connection ~ 2950 3150
$Comp
L PMSM_Driver_LV_Board_Power:ISO_DGND #PWR?
U 1 1 60EF19FE
P 2350 1600
AR Path="/5F05E355/60EF19FE" Ref="#PWR?"  Part="1" 
AR Path="/6068445B/60EF19FE" Ref="#PWR?"  Part="1" 
AR Path="/60EB5E32/60EF19FE" Ref="#PWR?"  Part="1" 
AR Path="/5EA5DFF7/60EF19FE" Ref="#PWR0163"  Part="1" 
F 0 "#PWR0163" H 2350 1350 50  0001 C CNN
F 1 "ISO_DGND" H 2355 1427 50  0000 C CNN
F 2 "" H 2350 1600 50  0001 C CNN
F 3 "" H 2350 1600 50  0001 C CNN
	1    2350 1600
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2350 1500 2350 1550
Connection ~ 2350 1550
Wire Wire Line
	2350 1550 2350 1600
Wire Wire Line
	2675 1500 2675 1550
Wire Wire Line
	2675 1550 2350 1550
Wire Wire Line
	2675 1250 2675 1300
Wire Wire Line
	2675 1250 2350 1250
Wire Wire Line
	2350 1250 2350 1300
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 60EF1A12
P 2675 1400
AR Path="/5E9C6910/60EF1A12" Ref="C?"  Part="1" 
AR Path="/5EBC6654/60EF1A12" Ref="C?"  Part="1" 
AR Path="/5EBC681B/60EF1A12" Ref="C?"  Part="1" 
AR Path="/5F05E355/60EF1A12" Ref="C?"  Part="1" 
AR Path="/6068445B/60EF1A12" Ref="C?"  Part="1" 
AR Path="/60EB5E32/60EF1A12" Ref="C?"  Part="1" 
AR Path="/5EA5DFF7/60EF1A12" Ref="C56"  Part="1" 
F 0 "C56" H 2750 1400 50  0000 L CNN
F 1 "4.7uF" H 2700 1475 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 2675 1400 50  0001 C CNN
F 3 "~" H 2675 1400 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 2675 1000 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/samsung-electro-mechanics/cl10a475kq8nnnc/cap-4-7uf-6-3v-mlcc-0603/dp/3013399" H 2675 900 50  0001 C CNN "Distributor Link"
	1    2675 1400
	1    0    0    1   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 60EF1A1A
P 2350 1400
AR Path="/5E9C6910/60EF1A1A" Ref="C?"  Part="1" 
AR Path="/5EBC6654/60EF1A1A" Ref="C?"  Part="1" 
AR Path="/5EBC681B/60EF1A1A" Ref="C?"  Part="1" 
AR Path="/5F05E355/60EF1A1A" Ref="C?"  Part="1" 
AR Path="/6068445B/60EF1A1A" Ref="C?"  Part="1" 
AR Path="/60EB5E32/60EF1A1A" Ref="C?"  Part="1" 
AR Path="/5EA5DFF7/60EF1A1A" Ref="C54"  Part="1" 
F 0 "C54" H 2425 1400 50  0000 L CNN
F 1 "10nF" H 2400 1475 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 2350 1400 50  0001 C CNN
F 3 "~" H 2350 1400 50  0001 C CNN
F 4 "0402B103K100CT -  SMD Multilayer Ceramic Capacitor, General Purpose, 10000 pF, 10 V, 0402 [1005 Metric], ± 10%, X7R" H 2350 1000 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/walsin/0402b103k100ct/cap-0-01-f-10v-10-x7r-0402-reel/dp/2524673" H 2350 900 50  0001 C CNN "Distributor Link"
	1    2350 1400
	1    0    0    1   
$EndComp
Wire Wire Line
	2675 1250 2950 1250
Connection ~ 2675 1250
Connection ~ 2950 1250
Wire Wire Line
	2950 1250 2950 1100
NoConn ~ 4350 2150
NoConn ~ 4350 2250
NoConn ~ 4350 2350
NoConn ~ 4350 2450
NoConn ~ 4350 2550
NoConn ~ 4350 2650
NoConn ~ 4350 2750
NoConn ~ 4350 2850
NoConn ~ 4350 2950
NoConn ~ 4350 3050
NoConn ~ 4350 3150
NoConn ~ 4350 3250
NoConn ~ 4350 3350
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 60F01727
P 4600 1900
AR Path="/5F05E355/60F01727" Ref="R?"  Part="1" 
AR Path="/5E9C6910/60F01727" Ref="R?"  Part="1" 
AR Path="/60EB5E32/60F01727" Ref="R?"  Part="1" 
AR Path="/5EA5DFF7/60F01727" Ref="R101"  Part="1" 
F 0 "R101" V 4650 2025 50  0000 C CNN
F 1 "0" V 4650 1800 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 4600 1900 50  0001 C CNN
F 3 "~" H 4600 1900 50  0001 C CNN
F 4 "..." H 4600 1500 50  0001 C CNN "Description"
F 5 "..." H 4600 1400 50  0001 C CNN "Distributor Link"
	1    4600 1900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4350 1900 4500 1900
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 60F043E3
P 4600 2000
AR Path="/5F05E355/60F043E3" Ref="R?"  Part="1" 
AR Path="/5E9C6910/60F043E3" Ref="R?"  Part="1" 
AR Path="/60EB5E32/60F043E3" Ref="R?"  Part="1" 
AR Path="/5EA5DFF7/60F043E3" Ref="R102"  Part="1" 
F 0 "R102" V 4650 2125 50  0000 C CNN
F 1 "0" V 4650 1900 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 4600 2000 50  0001 C CNN
F 3 "~" H 4600 2000 50  0001 C CNN
F 4 "..." H 4600 1600 50  0001 C CNN "Description"
F 5 "..." H 4600 1500 50  0001 C CNN "Distributor Link"
	1    4600 2000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4350 2000 4500 2000
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 60F04EB4
P 4600 4050
AR Path="/5F05E355/60F04EB4" Ref="R?"  Part="1" 
AR Path="/5E9C6910/60F04EB4" Ref="R?"  Part="1" 
AR Path="/60EB5E32/60F04EB4" Ref="R?"  Part="1" 
AR Path="/5EA5DFF7/60F04EB4" Ref="R103"  Part="1" 
F 0 "R103" V 4650 4175 50  0000 C CNN
F 1 "0" V 4650 3950 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 4600 4050 50  0001 C CNN
F 3 "~" H 4600 4050 50  0001 C CNN
F 4 "..." H 4600 3650 50  0001 C CNN "Description"
F 5 "..." H 4600 3550 50  0001 C CNN "Distributor Link"
	1    4600 4050
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4350 4050 4500 4050
Text GLabel 4850 4050 2    50   Output ~ 0
ISO_RES_I
Text GLabel 4850 2000 2    50   Output ~ 0
ISO_RES_B
Text GLabel 4850 1900 2    50   Output ~ 0
ISO_RES_A
Wire Wire Line
	4700 1900 4850 1900
Wire Wire Line
	4700 2000 4850 2000
Wire Wire Line
	4700 4050 4850 4050
$Comp
L PMSM_Driver_LV_Board_Power:ISO_DGND #PWR?
U 1 1 60F0A757
P 3750 4550
AR Path="/5F05E355/60F0A757" Ref="#PWR?"  Part="1" 
AR Path="/6068445B/60F0A757" Ref="#PWR?"  Part="1" 
AR Path="/60EB5E32/60F0A757" Ref="#PWR?"  Part="1" 
AR Path="/5EA5DFF7/60F0A757" Ref="#PWR0170"  Part="1" 
F 0 "#PWR0170" H 3750 4300 50  0001 C CNN
F 1 "ISO_DGND" H 3755 4377 50  0000 C CNN
F 2 "" H 3750 4550 50  0001 C CNN
F 3 "" H 3750 4550 50  0001 C CNN
	1    3750 4550
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3750 4500 3750 4550
Wire Wire Line
	3750 4500 3650 4500
Wire Wire Line
	3500 4500 3500 4450
Wire Wire Line
	3650 4450 3650 4500
Connection ~ 3650 4500
Wire Wire Line
	3650 4500 3500 4500
Wire Wire Line
	3750 4450 3750 4500
Connection ~ 3750 4500
NoConn ~ 4350 3550
$Comp
L PMSM_Driver_LV_Board_Power:ISO_DGND #PWR?
U 1 1 60F0F095
P 3000 4550
AR Path="/5F05E355/60F0F095" Ref="#PWR?"  Part="1" 
AR Path="/6068445B/60F0F095" Ref="#PWR?"  Part="1" 
AR Path="/60EB5E32/60F0F095" Ref="#PWR?"  Part="1" 
AR Path="/5EA5DFF7/60F0F095" Ref="#PWR0168"  Part="1" 
F 0 "#PWR0168" H 3000 4300 50  0001 C CNN
F 1 "ISO_DGND" H 3005 4377 50  0000 C CNN
F 2 "" H 3000 4550 50  0001 C CNN
F 3 "" H 3000 4550 50  0001 C CNN
	1    3000 4550
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3000 4550 3000 3600
Wire Wire Line
	3000 3600 3100 3600
Wire Wire Line
	3000 3600 3000 2450
Wire Wire Line
	3000 2450 3100 2450
Connection ~ 3000 3600
Text GLabel 4850 3450 2    50   Output ~ 0
ISO_SPI_MISO
Text GLabel 2850 2600 0    50   Input ~ 0
ISO_SPI_MOSI
Text GLabel 2850 2700 0    50   Input ~ 0
ISO_SPI_SCK
Text GLabel 2850 3300 0    50   Output ~ 0
ISO_RDC_SAMPLE_N
Text GLabel 4850 3650 2    50   Output ~ 0
ISO_DOS
Text GLabel 4850 3950 2    50   Output ~ 0
ISO_LOT
Wire Wire Line
	4350 3450 4850 3450
Wire Wire Line
	4350 3650 4850 3650
Wire Wire Line
	4850 3950 4350 3950
NoConn ~ 4350 4150
Text GLabel 4850 4250 2    50   Output ~ 0
ISO_XTALOUT
Wire Wire Line
	4850 4250 4350 4250
Wire Wire Line
	4350 3750 4850 3750
Wire Wire Line
	4350 3850 4850 3850
$Comp
L PMSM_Driver_LV_Board_Device:Crystal_Small Y1
U 1 1 60F5BB0C
P 1600 2150
F 0 "Y1" H 1600 2050 50  0000 C CNN
F 1 "8.192MHZ" H 1600 2275 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:Crystal_SMD_HC49-SD_HandSoldering" H 1600 2150 50  0001 C CNN
F 3 "~" H 1600 2150 50  0001 C CNN
F 4 "9C-8.192MEEJ-T -  Crystal, 8.192 MHz, SMD, 11.4mm x 4.35mm, 10 ppm, 18 pF, 10 ppm, 9C Series" H 1600 1950 31  0001 C CNN "Description"
F 5 "https://uk.farnell.com/txc/9c-8-192meej-t/xtal-8-192mhz-18pf-smd-hc-49s/dp/1842349" H 1600 1900 31  0001 C CNN "Distributor Link"
	1    1600 2150
	-1   0    0    1   
$EndComp
$Comp
L PMSM_Driver_LV_Board_IC:AD8662ARMZ U24
U 1 1 60F60C42
P 9075 3550
F 0 "U24" H 8875 4350 50  0000 C CNN
F 1 "AD8662ARMZ" H 9375 2750 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:SOIC-8_3.9x4.9mm_P1.27mm" H 9075 2250 31  0001 C CNN
F 3 "~" H 9075 3550 31  0001 C CNN
F 4 "AD8662ARMZ -  Operational Amplifier, Dual, 2 Amplifier, 4 MHz, 3.5 V/µs, 5V to 16V, MSOP, 8 Pins" H 9075 2200 31  0001 C CNN "Description"
F 5 "https://uk.farnell.com/analog-devices/ad8662armz/op-amp-dual-4mhz-3-5v-us-msop/dp/2462117" H 9075 2150 31  0001 C CNN "Distributor Link"
F 6 "https://www.mouser.co.uk/ProductDetail/Analog-Devices/AD8662ARMZ?qs=sGAEpiMZZMvtNjJQt4UgLalm6EXZly%2FyeBBy0eNa20E%3D" H 9075 2100 31  0001 C CNN "Distributor Link 2"
	1    9075 3550
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 60F63188
P 1850 2375
AR Path="/5E9C6910/60F63188" Ref="C?"  Part="1" 
AR Path="/5EBC6654/60F63188" Ref="C?"  Part="1" 
AR Path="/5EBC681B/60F63188" Ref="C?"  Part="1" 
AR Path="/5F05E355/60F63188" Ref="C?"  Part="1" 
AR Path="/6068445B/60F63188" Ref="C?"  Part="1" 
AR Path="/60EB5E32/60F63188" Ref="C?"  Part="1" 
AR Path="/5EA5DFF7/60F63188" Ref="C53"  Part="1" 
F 0 "C53" H 1925 2375 50  0000 L CNN
F 1 "20pF" H 1875 2450 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 1850 2375 50  0001 C CNN
F 3 "~" H 1850 2375 50  0001 C CNN
F 4 "GJM1555C1H200FB01D -  SMD Multilayer Ceramic Capacitor, 20 pF, 50 V, 0402 [1005 Metric], ± 1%, C0G / NP0, GJM Series " H 1850 1975 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/murata/gjm1555c1h200fb01d/cap-20pf-50v-1-c0g-np0-0402/dp/2781429" H 1850 1875 50  0001 C CNN "Distributor Link"
	1    1850 2375
	1    0    0    1   
$EndComp
Wire Wire Line
	1700 2150 1850 2150
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 60F82DB8
P 1350 2375
AR Path="/5E9C6910/60F82DB8" Ref="C?"  Part="1" 
AR Path="/5EBC6654/60F82DB8" Ref="C?"  Part="1" 
AR Path="/5EBC681B/60F82DB8" Ref="C?"  Part="1" 
AR Path="/5F05E355/60F82DB8" Ref="C?"  Part="1" 
AR Path="/6068445B/60F82DB8" Ref="C?"  Part="1" 
AR Path="/60EB5E32/60F82DB8" Ref="C?"  Part="1" 
AR Path="/5EA5DFF7/60F82DB8" Ref="C52"  Part="1" 
F 0 "C52" H 1425 2375 50  0000 L CNN
F 1 "20pF" H 1375 2450 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 1350 2375 50  0001 C CNN
F 3 "~" H 1350 2375 50  0001 C CNN
F 4 "GJM1555C1H200FB01D -  SMD Multilayer Ceramic Capacitor, 20 pF, 50 V, 0402 [1005 Metric], ± 1%, C0G / NP0, GJM Series " H 1350 1975 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/murata/gjm1555c1h200fb01d/cap-20pf-50v-1-c0g-np0-0402/dp/2781429" H 1350 1875 50  0001 C CNN "Distributor Link"
	1    1350 2375
	1    0    0    1   
$EndComp
Wire Wire Line
	1500 2150 1350 2150
Wire Wire Line
	1850 2150 1850 2275
Wire Wire Line
	1350 2150 1350 2275
Wire Wire Line
	1850 2575 1850 2475
Wire Wire Line
	1350 2475 1350 2575
$Comp
L PMSM_Driver_LV_Board_Power:ISO_DGND #PWR?
U 1 1 60F8D80E
P 1600 2625
AR Path="/5F05E355/60F8D80E" Ref="#PWR?"  Part="1" 
AR Path="/6068445B/60F8D80E" Ref="#PWR?"  Part="1" 
AR Path="/60EB5E32/60F8D80E" Ref="#PWR?"  Part="1" 
AR Path="/5EA5DFF7/60F8D80E" Ref="#PWR0159"  Part="1" 
F 0 "#PWR0159" H 1600 2375 50  0001 C CNN
F 1 "ISO_DGND" H 1605 2452 50  0000 C CNN
F 2 "" H 1600 2625 50  0001 C CNN
F 3 "" H 1600 2625 50  0001 C CNN
	1    1600 2625
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1350 2575 1600 2575
Wire Wire Line
	1600 2625 1600 2575
Connection ~ 1600 2575
Wire Wire Line
	1600 2575 1850 2575
Text GLabel 1250 2150 0    50   Input ~ 0
ISO_XTALOUT
Wire Wire Line
	1250 2150 1350 2150
Connection ~ 1350 2150
Text GLabel 2850 2250 0    50   Input ~ 0
ISO_COS
Text GLabel 2850 2350 0    50   Input ~ 0
ISO_COS_LO
Wire Wire Line
	3100 2150 1850 2150
Connection ~ 1850 2150
Text GLabel 2850 1900 0    50   Input ~ 0
ISO_A0
Text GLabel 2850 2000 0    50   Input ~ 0
ISO_A1
Text GLabel 2850 2850 0    50   Input ~ 0
ISO_RES0
Text GLabel 2850 2950 0    50   Input ~ 0
ISO_RES1
Text GLabel 2850 3400 0    50   Input ~ 0
ISO_SIN
Text GLabel 2850 3500 0    50   Input ~ 0
ISO_SIN_LO
Text GLabel 2850 3050 0    50   Input ~ 0
ISO_VT_ENABLE
Text GLabel 1925 3900 0    50   Output ~ 0
ISO_SPI_SEL_RDC
$Comp
L PMSM_Driver_LV_Board_Power:ISO_SENS_5V0 #PWR?
U 1 1 610DC503
P 2025 3600
AR Path="/6068445B/610DC503" Ref="#PWR?"  Part="1" 
AR Path="/60EB5E32/610DC503" Ref="#PWR?"  Part="1" 
AR Path="/5EA5DFF7/610DC503" Ref="#PWR0162"  Part="1" 
F 0 "#PWR0162" H 2025 3850 50  0001 C CNN
F 1 "ISO_SENS_5V0" H 2026 3773 50  0000 C CNN
F 2 "" H 2025 3600 50  0001 C CNN
F 3 "" H 2025 3600 50  0001 C CNN
	1    2025 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	2850 1900 3100 1900
Wire Wire Line
	2850 2000 3100 2000
Wire Wire Line
	2850 2250 3100 2250
Wire Wire Line
	2850 2350 3100 2350
Wire Wire Line
	2850 2600 3100 2600
Wire Wire Line
	2850 2700 3100 2700
Wire Wire Line
	2850 2850 3100 2850
Wire Wire Line
	2850 2950 3100 2950
Wire Wire Line
	2850 3050 3100 3050
Wire Wire Line
	2850 3300 3100 3300
Wire Wire Line
	2850 3400 3100 3400
Wire Wire Line
	2850 3500 3100 3500
Wire Wire Line
	2025 3650 2025 3600
Wire Wire Line
	1925 3900 2025 3900
Wire Wire Line
	2025 3900 2025 3850
Wire Wire Line
	2025 3900 3100 3900
Connection ~ 2025 3900
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 6110CC31
P 2025 3750
AR Path="/5F05E355/6110CC31" Ref="R?"  Part="1" 
AR Path="/6068445B/6110CC31" Ref="R?"  Part="1" 
AR Path="/60EB5E32/6110CC31" Ref="R?"  Part="1" 
AR Path="/5EA5DFF7/6110CC31" Ref="R98"  Part="1" 
F 0 "R98" H 2150 3750 50  0000 C CNN
F 1 "10k" H 2125 3675 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 2025 3750 50  0001 C CNN
F 3 "~" H 2025 3750 50  0001 C CNN
F 4 "MCWR04X1002FTL -  SMD Chip Resistor, 0402 [1005 Metric], 10 kohm, MCWR Series, 50 V, Thick Film, 62.5 mW " H 2025 3350 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x1002ftl/res-10k-1-0-0625w-thick-film/dp/2447096" H 2025 3250 50  0001 C CNN "Distributor Link"
	1    2025 3750
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:ISO_DGND #PWR?
U 1 1 61115449
P 2350 4350
AR Path="/5F05E355/61115449" Ref="#PWR?"  Part="1" 
AR Path="/6068445B/61115449" Ref="#PWR?"  Part="1" 
AR Path="/60EB5E32/61115449" Ref="#PWR?"  Part="1" 
AR Path="/5EA5DFF7/61115449" Ref="#PWR0164"  Part="1" 
F 0 "#PWR0164" H 2350 4100 50  0001 C CNN
F 1 "ISO_DGND" H 2355 4177 50  0000 C CNN
F 2 "" H 2350 4350 50  0001 C CNN
F 3 "" H 2350 4350 50  0001 C CNN
	1    2350 4350
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2350 4250 2350 4300
Connection ~ 2350 4300
Wire Wire Line
	2350 4300 2350 4350
Wire Wire Line
	2675 4250 2675 4300
Wire Wire Line
	2675 4300 2350 4300
Wire Wire Line
	2675 4000 2675 4050
Wire Wire Line
	2675 4000 2350 4000
Wire Wire Line
	2350 4000 2350 4050
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 6111545D
P 2675 4150
AR Path="/5E9C6910/6111545D" Ref="C?"  Part="1" 
AR Path="/5EBC6654/6111545D" Ref="C?"  Part="1" 
AR Path="/5EBC681B/6111545D" Ref="C?"  Part="1" 
AR Path="/5F05E355/6111545D" Ref="C?"  Part="1" 
AR Path="/6068445B/6111545D" Ref="C?"  Part="1" 
AR Path="/60EB5E32/6111545D" Ref="C?"  Part="1" 
AR Path="/5EA5DFF7/6111545D" Ref="C57"  Part="1" 
F 0 "C57" H 2750 4150 50  0000 L CNN
F 1 "10uF" H 2725 4225 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 2675 4150 50  0001 C CNN
F 3 "~" H 2675 4150 50  0001 C CNN
F 4 "LMK107BBJ106MALT -  SMD Multilayer Ceramic Capacitor, 10 µF, 10 V, 0603 [1608 Metric], ± 20%, X5R, M Series" H 2675 3750 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/taiyo-yuden/lmk107bbj106malt/cap-10-f-10v-20-x5r-0603/dp/2113070" H 2675 3650 50  0001 C CNN "Distributor Link"
	1    2675 4150
	1    0    0    1   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 61115465
P 2350 4150
AR Path="/5E9C6910/61115465" Ref="C?"  Part="1" 
AR Path="/5EBC6654/61115465" Ref="C?"  Part="1" 
AR Path="/5EBC681B/61115465" Ref="C?"  Part="1" 
AR Path="/5F05E355/61115465" Ref="C?"  Part="1" 
AR Path="/6068445B/61115465" Ref="C?"  Part="1" 
AR Path="/60EB5E32/61115465" Ref="C?"  Part="1" 
AR Path="/5EA5DFF7/61115465" Ref="C55"  Part="1" 
F 0 "C55" H 2425 4150 50  0000 L CNN
F 1 "10nF" H 2375 4225 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 2350 4150 50  0001 C CNN
F 3 "~" H 2350 4150 50  0001 C CNN
F 4 "0402B103K100CT -  SMD Multilayer Ceramic Capacitor, General Purpose, 10000 pF, 10 V, 0402 [1005 Metric], ± 10%, X7R" H 2350 3750 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/walsin/0402b103k100ct/cap-0-01-f-10v-10-x7r-0402-reel/dp/2524673" H 2350 3650 50  0001 C CNN "Distributor Link"
	1    2350 4150
	1    0    0    1   
$EndComp
Wire Wire Line
	3100 4000 2675 4000
Connection ~ 2675 4000
Text GLabel 1900 5825 2    50   Output ~ 0
ISO_A0
Text GLabel 2650 5825 2    50   Output ~ 0
ISO_A1
$Comp
L PMSM_Driver_LV_Board_Connector:Jumper_NC_Small JP?
U 1 1 6112DD67
P 1700 6025
AR Path="/6068445B/6112DD67" Ref="JP?"  Part="1" 
AR Path="/60E816A3/6112DD67" Ref="JP?"  Part="1" 
AR Path="/60EB5E32/6112DD67" Ref="JP?"  Part="1" 
AR Path="/5EA5DFF7/6112DD67" Ref="JP3"  Part="1" 
F 0 "JP3" V 1725 6000 50  0000 R CNN
F 1 "JUMPER" V 1650 6000 50  0000 R CNN
F 2 "PMSM_Driver_LV_Board:PinHeader_1x02_P2.54mm_Vertical" H 1700 5875 50  0001 C CNN
F 3 "~" H 1700 6025 50  0001 C CNN
F 4 "2211S-02G -  Board-To-Board Connector, 2.54 mm, 2 Contacts, Header, 2211S Series, Through Hole, 1 Rows" H 1700 5775 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/2211s-02g/header-1-row-vert-2way/dp/1593411" H 1700 5675 50  0001 C CNN "Distributor Link"
	1    1700 6025
	0    -1   -1   0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:ISO_DGND #PWR?
U 1 1 6112DD6D
P 1700 6225
AR Path="/5F05E355/6112DD6D" Ref="#PWR?"  Part="1" 
AR Path="/6068445B/6112DD6D" Ref="#PWR?"  Part="1" 
AR Path="/60E816A3/6112DD6D" Ref="#PWR?"  Part="1" 
AR Path="/60EB5E32/6112DD6D" Ref="#PWR?"  Part="1" 
AR Path="/5EA5DFF7/6112DD6D" Ref="#PWR0161"  Part="1" 
F 0 "#PWR0161" H 1700 5975 50  0001 C CNN
F 1 "ISO_DGND" H 1705 6052 50  0000 C CNN
F 2 "" H 1700 6225 50  0001 C CNN
F 3 "" H 1700 6225 50  0001 C CNN
	1    1700 6225
	1    0    0    -1  
$EndComp
Wire Wire Line
	1700 6125 1700 6225
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 6112DD76
P 1700 5625
AR Path="/5F05E355/6112DD76" Ref="R?"  Part="1" 
AR Path="/6068445B/6112DD76" Ref="R?"  Part="1" 
AR Path="/60E816A3/6112DD76" Ref="R?"  Part="1" 
AR Path="/60EB5E32/6112DD76" Ref="R?"  Part="1" 
AR Path="/5EA5DFF7/6112DD76" Ref="R97"  Part="1" 
F 0 "R97" H 1825 5625 50  0000 C CNN
F 1 "10k" H 1800 5550 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 1700 5625 50  0001 C CNN
F 3 "~" H 1700 5625 50  0001 C CNN
F 4 "MCWR04X1002FTL -  SMD Chip Resistor, 0402 [1005 Metric], 10 kohm, MCWR Series, 50 V, Thick Film, 62.5 mW " H 1700 5225 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x1002ftl/res-10k-1-0-0625w-thick-film/dp/2447096" H 1700 5125 50  0001 C CNN "Distributor Link"
	1    1700 5625
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:ISO_SENS_5V0 #PWR?
U 1 1 6112DD7C
P 1700 5425
AR Path="/6068445B/6112DD7C" Ref="#PWR?"  Part="1" 
AR Path="/60E816A3/6112DD7C" Ref="#PWR?"  Part="1" 
AR Path="/60EB5E32/6112DD7C" Ref="#PWR?"  Part="1" 
AR Path="/5EA5DFF7/6112DD7C" Ref="#PWR0160"  Part="1" 
F 0 "#PWR0160" H 1700 5675 50  0001 C CNN
F 1 "ISO_SENS_5V0" H 1701 5598 50  0000 C CNN
F 2 "" H 1700 5425 50  0001 C CNN
F 3 "" H 1700 5425 50  0001 C CNN
	1    1700 5425
	1    0    0    -1  
$EndComp
Wire Wire Line
	1700 5525 1700 5425
Wire Wire Line
	1700 5725 1700 5825
Wire Wire Line
	1900 5825 1700 5825
Connection ~ 1700 5825
Wire Wire Line
	1700 5825 1700 5925
$Comp
L PMSM_Driver_LV_Board_Connector:Jumper_NC_Small JP?
U 1 1 611439C3
P 2450 6025
AR Path="/6068445B/611439C3" Ref="JP?"  Part="1" 
AR Path="/60E816A3/611439C3" Ref="JP?"  Part="1" 
AR Path="/60EB5E32/611439C3" Ref="JP?"  Part="1" 
AR Path="/5EA5DFF7/611439C3" Ref="JP4"  Part="1" 
F 0 "JP4" V 2475 6000 50  0000 R CNN
F 1 "JUMPER" V 2400 6000 50  0000 R CNN
F 2 "PMSM_Driver_LV_Board:PinHeader_1x02_P2.54mm_Vertical" H 2450 5875 50  0001 C CNN
F 3 "~" H 2450 6025 50  0001 C CNN
F 4 "2211S-02G -  Board-To-Board Connector, 2.54 mm, 2 Contacts, Header, 2211S Series, Through Hole, 1 Rows" H 2450 5775 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/2211s-02g/header-1-row-vert-2way/dp/1593411" H 2450 5675 50  0001 C CNN "Distributor Link"
	1    2450 6025
	0    -1   -1   0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:ISO_DGND #PWR?
U 1 1 611439C9
P 2450 6225
AR Path="/5F05E355/611439C9" Ref="#PWR?"  Part="1" 
AR Path="/6068445B/611439C9" Ref="#PWR?"  Part="1" 
AR Path="/60E816A3/611439C9" Ref="#PWR?"  Part="1" 
AR Path="/60EB5E32/611439C9" Ref="#PWR?"  Part="1" 
AR Path="/5EA5DFF7/611439C9" Ref="#PWR0166"  Part="1" 
F 0 "#PWR0166" H 2450 5975 50  0001 C CNN
F 1 "ISO_DGND" H 2455 6052 50  0000 C CNN
F 2 "" H 2450 6225 50  0001 C CNN
F 3 "" H 2450 6225 50  0001 C CNN
	1    2450 6225
	1    0    0    -1  
$EndComp
Wire Wire Line
	2450 6125 2450 6225
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 611439D2
P 2450 5625
AR Path="/5F05E355/611439D2" Ref="R?"  Part="1" 
AR Path="/6068445B/611439D2" Ref="R?"  Part="1" 
AR Path="/60E816A3/611439D2" Ref="R?"  Part="1" 
AR Path="/60EB5E32/611439D2" Ref="R?"  Part="1" 
AR Path="/5EA5DFF7/611439D2" Ref="R99"  Part="1" 
F 0 "R99" H 2575 5625 50  0000 C CNN
F 1 "10k" H 2550 5550 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 2450 5625 50  0001 C CNN
F 3 "~" H 2450 5625 50  0001 C CNN
F 4 "MCWR04X1002FTL -  SMD Chip Resistor, 0402 [1005 Metric], 10 kohm, MCWR Series, 50 V, Thick Film, 62.5 mW " H 2450 5225 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x1002ftl/res-10k-1-0-0625w-thick-film/dp/2447096" H 2450 5125 50  0001 C CNN "Distributor Link"
	1    2450 5625
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:ISO_SENS_5V0 #PWR?
U 1 1 611439D8
P 2450 5425
AR Path="/6068445B/611439D8" Ref="#PWR?"  Part="1" 
AR Path="/60E816A3/611439D8" Ref="#PWR?"  Part="1" 
AR Path="/60EB5E32/611439D8" Ref="#PWR?"  Part="1" 
AR Path="/5EA5DFF7/611439D8" Ref="#PWR0165"  Part="1" 
F 0 "#PWR0165" H 2450 5675 50  0001 C CNN
F 1 "ISO_SENS_5V0" H 2451 5598 50  0000 C CNN
F 2 "" H 2450 5425 50  0001 C CNN
F 3 "" H 2450 5425 50  0001 C CNN
	1    2450 5425
	1    0    0    -1  
$EndComp
Wire Wire Line
	2450 5525 2450 5425
Wire Wire Line
	2450 5725 2450 5825
Wire Wire Line
	2650 5825 2450 5825
Connection ~ 2450 5825
Wire Wire Line
	2450 5825 2450 5925
$Comp
L PMSM_Driver_LV_Board_Connector:Jumper_NC_Small JP?
U 1 1 6115590D
P 4125 6025
AR Path="/6068445B/6115590D" Ref="JP?"  Part="1" 
AR Path="/60E816A3/6115590D" Ref="JP?"  Part="1" 
AR Path="/60EB5E32/6115590D" Ref="JP?"  Part="1" 
AR Path="/5EA5DFF7/6115590D" Ref="JP5"  Part="1" 
F 0 "JP5" V 4150 6000 50  0000 R CNN
F 1 "JUMPER" V 4075 6000 50  0000 R CNN
F 2 "PMSM_Driver_LV_Board:PinHeader_1x02_P2.54mm_Vertical" H 4125 5875 50  0001 C CNN
F 3 "~" H 4125 6025 50  0001 C CNN
F 4 "2211S-02G -  Board-To-Board Connector, 2.54 mm, 2 Contacts, Header, 2211S Series, Through Hole, 1 Rows" H 4125 5775 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/2211s-02g/header-1-row-vert-2way/dp/1593411" H 4125 5675 50  0001 C CNN "Distributor Link"
	1    4125 6025
	0    -1   -1   0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:ISO_DGND #PWR?
U 1 1 61155913
P 4125 6225
AR Path="/5F05E355/61155913" Ref="#PWR?"  Part="1" 
AR Path="/6068445B/61155913" Ref="#PWR?"  Part="1" 
AR Path="/60E816A3/61155913" Ref="#PWR?"  Part="1" 
AR Path="/60EB5E32/61155913" Ref="#PWR?"  Part="1" 
AR Path="/5EA5DFF7/61155913" Ref="#PWR0172"  Part="1" 
F 0 "#PWR0172" H 4125 5975 50  0001 C CNN
F 1 "ISO_DGND" H 4130 6052 50  0000 C CNN
F 2 "" H 4125 6225 50  0001 C CNN
F 3 "" H 4125 6225 50  0001 C CNN
	1    4125 6225
	1    0    0    -1  
$EndComp
Wire Wire Line
	4125 6125 4125 6225
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 6115591C
P 4125 5625
AR Path="/5F05E355/6115591C" Ref="R?"  Part="1" 
AR Path="/6068445B/6115591C" Ref="R?"  Part="1" 
AR Path="/60E816A3/6115591C" Ref="R?"  Part="1" 
AR Path="/60EB5E32/6115591C" Ref="R?"  Part="1" 
AR Path="/5EA5DFF7/6115591C" Ref="R100"  Part="1" 
F 0 "R100" H 4275 5625 50  0000 C CNN
F 1 "10k" H 4225 5550 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 4125 5625 50  0001 C CNN
F 3 "~" H 4125 5625 50  0001 C CNN
F 4 "MCWR04X1002FTL -  SMD Chip Resistor, 0402 [1005 Metric], 10 kohm, MCWR Series, 50 V, Thick Film, 62.5 mW " H 4125 5225 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x1002ftl/res-10k-1-0-0625w-thick-film/dp/2447096" H 4125 5125 50  0001 C CNN "Distributor Link"
	1    4125 5625
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:ISO_SENS_5V0 #PWR?
U 1 1 61155922
P 4125 5425
AR Path="/6068445B/61155922" Ref="#PWR?"  Part="1" 
AR Path="/60E816A3/61155922" Ref="#PWR?"  Part="1" 
AR Path="/60EB5E32/61155922" Ref="#PWR?"  Part="1" 
AR Path="/5EA5DFF7/61155922" Ref="#PWR0171"  Part="1" 
F 0 "#PWR0171" H 4125 5675 50  0001 C CNN
F 1 "ISO_SENS_5V0" H 4126 5598 50  0000 C CNN
F 2 "" H 4125 5425 50  0001 C CNN
F 3 "" H 4125 5425 50  0001 C CNN
	1    4125 5425
	1    0    0    -1  
$EndComp
Wire Wire Line
	4125 5525 4125 5425
Wire Wire Line
	4125 5725 4125 5825
Wire Wire Line
	4325 5825 4125 5825
Connection ~ 4125 5825
Wire Wire Line
	4125 5825 4125 5925
$Comp
L PMSM_Driver_LV_Board_Connector:Jumper_NC_Small JP?
U 1 1 6115592F
P 4875 6025
AR Path="/6068445B/6115592F" Ref="JP?"  Part="1" 
AR Path="/60E816A3/6115592F" Ref="JP?"  Part="1" 
AR Path="/60EB5E32/6115592F" Ref="JP?"  Part="1" 
AR Path="/5EA5DFF7/6115592F" Ref="JP6"  Part="1" 
F 0 "JP6" V 4900 6000 50  0000 R CNN
F 1 "JUMPER" V 4825 6000 50  0000 R CNN
F 2 "PMSM_Driver_LV_Board:PinHeader_1x02_P2.54mm_Vertical" H 4875 5875 50  0001 C CNN
F 3 "~" H 4875 6025 50  0001 C CNN
F 4 "2211S-02G -  Board-To-Board Connector, 2.54 mm, 2 Contacts, Header, 2211S Series, Through Hole, 1 Rows" H 4875 5775 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/2211s-02g/header-1-row-vert-2way/dp/1593411" H 4875 5675 50  0001 C CNN "Distributor Link"
	1    4875 6025
	0    -1   -1   0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:ISO_DGND #PWR?
U 1 1 61155935
P 4875 6225
AR Path="/5F05E355/61155935" Ref="#PWR?"  Part="1" 
AR Path="/6068445B/61155935" Ref="#PWR?"  Part="1" 
AR Path="/60E816A3/61155935" Ref="#PWR?"  Part="1" 
AR Path="/60EB5E32/61155935" Ref="#PWR?"  Part="1" 
AR Path="/5EA5DFF7/61155935" Ref="#PWR0175"  Part="1" 
F 0 "#PWR0175" H 4875 5975 50  0001 C CNN
F 1 "ISO_DGND" H 4880 6052 50  0000 C CNN
F 2 "" H 4875 6225 50  0001 C CNN
F 3 "" H 4875 6225 50  0001 C CNN
	1    4875 6225
	1    0    0    -1  
$EndComp
Wire Wire Line
	4875 6125 4875 6225
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 6115593E
P 4875 5625
AR Path="/5F05E355/6115593E" Ref="R?"  Part="1" 
AR Path="/6068445B/6115593E" Ref="R?"  Part="1" 
AR Path="/60E816A3/6115593E" Ref="R?"  Part="1" 
AR Path="/60EB5E32/6115593E" Ref="R?"  Part="1" 
AR Path="/5EA5DFF7/6115593E" Ref="R104"  Part="1" 
F 0 "R104" H 5025 5625 50  0000 C CNN
F 1 "10k" H 4975 5550 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 4875 5625 50  0001 C CNN
F 3 "~" H 4875 5625 50  0001 C CNN
F 4 "MCWR04X1002FTL -  SMD Chip Resistor, 0402 [1005 Metric], 10 kohm, MCWR Series, 50 V, Thick Film, 62.5 mW " H 4875 5225 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x1002ftl/res-10k-1-0-0625w-thick-film/dp/2447096" H 4875 5125 50  0001 C CNN "Distributor Link"
	1    4875 5625
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:ISO_SENS_5V0 #PWR?
U 1 1 61155944
P 4875 5425
AR Path="/6068445B/61155944" Ref="#PWR?"  Part="1" 
AR Path="/60E816A3/61155944" Ref="#PWR?"  Part="1" 
AR Path="/60EB5E32/61155944" Ref="#PWR?"  Part="1" 
AR Path="/5EA5DFF7/61155944" Ref="#PWR0174"  Part="1" 
F 0 "#PWR0174" H 4875 5675 50  0001 C CNN
F 1 "ISO_SENS_5V0" H 4876 5598 50  0000 C CNN
F 2 "" H 4875 5425 50  0001 C CNN
F 3 "" H 4875 5425 50  0001 C CNN
	1    4875 5425
	1    0    0    -1  
$EndComp
Wire Wire Line
	4875 5525 4875 5425
Wire Wire Line
	4875 5725 4875 5825
Wire Wire Line
	5075 5825 4875 5825
Connection ~ 4875 5825
Wire Wire Line
	4875 5825 4875 5925
Text GLabel 4325 5825 2    50   Output ~ 0
ISO_RES0
Text GLabel 5075 5825 2    50   Output ~ 0
ISO_RES1
Text Notes 675  5150 0    100  ~ 0
Select Mode
Text Notes 3225 5150 0    100  ~ 0
Select Resolution
Text GLabel 4850 3750 2    50   Output ~ 0
ISO_RDC_EXC
Text GLabel 4850 3850 2    50   Output ~ 0
ISO_RDC_EXC_N
Text Notes 675  800  0    100  ~ 0
RDC (Resolver-to-Digital Converter)
Wire Notes Line
	5625 600  5625 4875
Wire Notes Line
	5625 4875 600  4875
Wire Notes Line
	600  600  5625 600 
Wire Notes Line
	600  600  600  4875
Wire Notes Line
	600  4950 3075 4950
Wire Notes Line
	3150 4950 5625 4950
Text Notes 725  5925 0    25   ~ 0
A0\n\n0\n0\n1\n1
Text Notes 825  5925 0    25   ~ 0
A1\n\n0\n1\n0\n1
Text Notes 950  5925 0    25   ~ 0
Mode\n\nNormal mode - position input\nNormal mode - velocity input\nReserved\nConfiguration mode
Wire Notes Line
	600  6550 3075 6550
Wire Notes Line
	3075 4950 3075 6550
Wire Notes Line
	600  4950 600  6550
Wire Notes Line
	3150 6550 5625 6550
Wire Notes Line
	3150 4950 3150 6550
Wire Notes Line
	5625 4950 5625 6550
Text Notes 3200 5950 0    25   ~ 0
RES0\n\n0\n0\n1\n1
Text Notes 3325 5950 0    25   ~ 0
RES1\n\n0\n1\n0\n1
Text Notes 3475 5950 0    25   ~ 0
Resoluion\n(bits)\n10\n12\n14\n16
Text Notes 3675 5950 0    25   ~ 0
Pos. LSB\n(arc min)\n21.1\n5.3\n1.3\n0.2
Text Notes 3875 5950 0    25   ~ 0
Vel. LSB\n(RPS)\n4.88\n0.488\n0.06\n0.004
Text GLabel 7275 3150 0    50   Input ~ 0
ISO_RDC_EXC
Text GLabel 7275 3750 0    50   Input ~ 0
ISO_RDC_EXC_N
Text Notes 5775 800  0    100  ~ 0
Excitation Buffer
Text Notes 5800 875  0    25   ~ 0
This is a buffer circuit that drives resolver low-impedance inputs.
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 6129AFE4
P 7475 3150
AR Path="/5F05E355/6129AFE4" Ref="R?"  Part="1" 
AR Path="/6068445B/6129AFE4" Ref="R?"  Part="1" 
AR Path="/60E816A3/6129AFE4" Ref="R?"  Part="1" 
AR Path="/60EB5E32/6129AFE4" Ref="R?"  Part="1" 
AR Path="/5EA5DFF7/6129AFE4" Ref="R109"  Part="1" 
F 0 "R109" V 7575 3150 50  0000 C CNN
F 1 "10k" V 7375 3150 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 7475 3150 50  0001 C CNN
F 3 "~" H 7475 3150 50  0001 C CNN
F 4 "MCWR04X1002FTL -  SMD Chip Resistor, 0402 [1005 Metric], 10 kohm, MCWR Series, 50 V, Thick Film, 62.5 mW " H 7475 2750 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x1002ftl/res-10k-1-0-0625w-thick-film/dp/2447096" H 7475 2650 50  0001 C CNN "Distributor Link"
	1    7475 3150
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7275 3150 7375 3150
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 612B2E19
P 6500 3200
AR Path="/5F05E355/612B2E19" Ref="R?"  Part="1" 
AR Path="/6068445B/612B2E19" Ref="R?"  Part="1" 
AR Path="/60E816A3/612B2E19" Ref="R?"  Part="1" 
AR Path="/60EB5E32/612B2E19" Ref="R?"  Part="1" 
AR Path="/5EA5DFF7/612B2E19" Ref="R107"  Part="1" 
F 0 "R107" H 6375 3125 50  0000 C CNN
F 1 "22k" H 6375 3200 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6500 3200 50  0001 C CNN
F 3 "~" H 6500 3200 50  0001 C CNN
F 4 "ERJ6RBD2202V -  SMD Chip Resistor, 0805 [2012 Metric], 22 kohm, ERJ6R Series, 150 V, Thick Film, 100 mW" H 6500 2800 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/panasonic/erj6rbd2202v/res-22k-0-5-0-1w-0805-thick-film/dp/2380345" H 6500 2700 50  0001 C CNN "Distributor Link"
	1    6500 3200
	-1   0    0    1   
$EndComp
Wire Wire Line
	6500 3000 6500 3100
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 612BF700
P 6500 3500
AR Path="/5F05E355/612BF700" Ref="R?"  Part="1" 
AR Path="/6068445B/612BF700" Ref="R?"  Part="1" 
AR Path="/60E816A3/612BF700" Ref="R?"  Part="1" 
AR Path="/60EB5E32/612BF700" Ref="R?"  Part="1" 
AR Path="/5EA5DFF7/612BF700" Ref="R108"  Part="1" 
F 0 "R108" H 6350 3500 50  0000 C CNN
F 1 "10k" H 6400 3575 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 6500 3500 50  0001 C CNN
F 3 "~" H 6500 3500 50  0001 C CNN
F 4 "WF06R1002BTL -  SMD Chip Resistor, 0603 [1608 Metric], 10 kohm, WF06R Series, 75 V, Thin Film, 100 mW" H 6500 3100 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/walsin/wf06r1002btl/res-10k-0-1-75v-0603-thin-film/dp/2502926" H 6500 3000 50  0001 C CNN "Distributor Link"
	1    6500 3500
	-1   0    0    1   
$EndComp
Wire Wire Line
	6500 3300 6500 3350
Wire Wire Line
	6500 3600 6500 3700
$Comp
L PMSM_Driver_LV_Board_Power:ISO_DGND #PWR?
U 1 1 612CAF90
P 6500 3700
AR Path="/5F05E355/612CAF90" Ref="#PWR?"  Part="1" 
AR Path="/6068445B/612CAF90" Ref="#PWR?"  Part="1" 
AR Path="/60EB5E32/612CAF90" Ref="#PWR?"  Part="1" 
AR Path="/5EA5DFF7/612CAF90" Ref="#PWR0179"  Part="1" 
F 0 "#PWR0179" H 6500 3450 50  0001 C CNN
F 1 "ISO_DGND" H 6505 3527 50  0000 C CNN
F 2 "" H 6500 3700 50  0001 C CNN
F 3 "" H 6500 3700 50  0001 C CNN
	1    6500 3700
	-1   0    0    -1  
$EndComp
Connection ~ 6500 3350
Wire Wire Line
	6500 3350 6500 3400
Wire Wire Line
	6050 3600 6050 3700
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 6131E45D
P 6050 4100
AR Path="/5F05E355/6131E45D" Ref="R?"  Part="1" 
AR Path="/6068445B/6131E45D" Ref="R?"  Part="1" 
AR Path="/60E816A3/6131E45D" Ref="R?"  Part="1" 
AR Path="/60EB5E32/6131E45D" Ref="R?"  Part="1" 
AR Path="/5EA5DFF7/6131E45D" Ref="R106"  Part="1" 
F 0 "R106" H 5900 4100 50  0000 C CNN
F 1 "10k" H 5950 4175 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 6050 4100 50  0001 C CNN
F 3 "~" H 6050 4100 50  0001 C CNN
F 4 "WF06R1002BTL -  SMD Chip Resistor, 0603 [1608 Metric], 10 kohm, WF06R Series, 75 V, Thin Film, 100 mW" H 6050 3700 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/walsin/wf06r1002btl/res-10k-0-1-75v-0603-thin-film/dp/2502926" H 6050 3600 50  0001 C CNN "Distributor Link"
	1    6050 4100
	-1   0    0    1   
$EndComp
Wire Wire Line
	6050 3900 6050 3950
Wire Wire Line
	6050 4200 6050 4300
$Comp
L PMSM_Driver_LV_Board_Power:ISO_DGND #PWR?
U 1 1 6131E465
P 6050 4300
AR Path="/5F05E355/6131E465" Ref="#PWR?"  Part="1" 
AR Path="/6068445B/6131E465" Ref="#PWR?"  Part="1" 
AR Path="/60EB5E32/6131E465" Ref="#PWR?"  Part="1" 
AR Path="/5EA5DFF7/6131E465" Ref="#PWR0177"  Part="1" 
F 0 "#PWR0177" H 6050 4050 50  0001 C CNN
F 1 "ISO_DGND" H 6055 4127 50  0000 C CNN
F 2 "" H 6050 4300 50  0001 C CNN
F 3 "" H 6050 4300 50  0001 C CNN
	1    6050 4300
	-1   0    0    -1  
$EndComp
Connection ~ 6050 3950
Wire Wire Line
	6050 3950 6050 4000
Wire Wire Line
	7625 3150 7625 3050
Wire Wire Line
	7575 3150 7625 3150
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 6135AD78
P 7875 3050
AR Path="/5F05E355/6135AD78" Ref="R?"  Part="1" 
AR Path="/6068445B/6135AD78" Ref="R?"  Part="1" 
AR Path="/60E816A3/6135AD78" Ref="R?"  Part="1" 
AR Path="/60EB5E32/6135AD78" Ref="R?"  Part="1" 
AR Path="/5EA5DFF7/6135AD78" Ref="R111"  Part="1" 
F 0 "R111" V 7925 3200 50  0000 C CNN
F 1 "15k4" V 7925 2925 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 7875 3050 50  0001 C CNN
F 3 "~" H 7875 3050 50  0001 C CNN
F 4 "MCWF06R1542BTL -  SMD Chip Resistor, 0603 [1608 Metric], 15.4 kohm, MCWF06R Series, 75 V, Thin Film, 100 mW" H 7875 2650 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwf06r1542btl/res-15k4-0-1-0-1w-0603-thin-film/dp/2336991" H 7875 2550 50  0001 C CNN "Distributor Link"
	1    7875 3050
	0    -1   -1   0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 61380521
P 7875 2900
AR Path="/5E9C6910/61380521" Ref="C?"  Part="1" 
AR Path="/5EBC6654/61380521" Ref="C?"  Part="1" 
AR Path="/5EBC681B/61380521" Ref="C?"  Part="1" 
AR Path="/5F05E355/61380521" Ref="C?"  Part="1" 
AR Path="/6068445B/61380521" Ref="C?"  Part="1" 
AR Path="/60EB5E32/61380521" Ref="C?"  Part="1" 
AR Path="/5EA5DFF7/61380521" Ref="C62"  Part="1" 
F 0 "C62" V 7825 2725 50  0000 L CNN
F 1 "120pF" V 7825 2950 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 7875 2900 50  0001 C CNN
F 3 "~" H 7875 2900 50  0001 C CNN
F 4 "0402B121K500CT -  SMD Multilayer Ceramic Capacitor, General Purpose, 120 pF, 50 V, 0402 [1005 Metric], ± 10%, X7R" H 7875 2500 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/walsin/0402b121k500ct/cap-120pf-50v-10-x7r-0402-reel/dp/2524680" H 7875 2400 50  0001 C CNN "Distributor Link"
	1    7875 2900
	0    1    1    0   
$EndComp
Wire Wire Line
	7625 3050 7625 2900
Connection ~ 7625 3050
Wire Wire Line
	8100 2900 8100 3050
Connection ~ 8100 3050
Text GLabel 8200 3050 2    50   Input ~ 0
ISO_EXC
Wire Wire Line
	8100 3050 8200 3050
Wire Wire Line
	9075 2500 9175 2500
Wire Wire Line
	9475 2500 9475 2550
Wire Wire Line
	9375 2500 9475 2500
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 613F4296
P 9275 2500
AR Path="/5E9C6910/613F4296" Ref="C?"  Part="1" 
AR Path="/5EBC6654/613F4296" Ref="C?"  Part="1" 
AR Path="/5EBC681B/613F4296" Ref="C?"  Part="1" 
AR Path="/5F05E355/613F4296" Ref="C?"  Part="1" 
AR Path="/60EB5E32/613F4296" Ref="C?"  Part="1" 
AR Path="/5EA5DFF7/613F4296" Ref="C64"  Part="1" 
F 0 "C64" V 9400 2450 50  0000 L CNN
F 1 "100nF" V 9325 2225 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 9275 2500 50  0001 C CNN
F 3 "~" H 9275 2500 50  0001 C CNN
F 4 "MC0402X104K250CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 25 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 9275 2100 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k250ct/cap-0-1-f-25v-10-x5r-0402/dp/2320760" H 9275 2000 50  0001 C CNN "Distributor Link"
	1    9275 2500
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9075 2400 9075 2500
$Comp
L PMSM_Driver_LV_Board_Power:ISO_DGND #PWR?
U 1 1 613F42A5
P 9475 2550
AR Path="/5F05E355/613F42A5" Ref="#PWR?"  Part="1" 
AR Path="/60EB5E32/613F42A5" Ref="#PWR?"  Part="1" 
AR Path="/5EA5DFF7/613F42A5" Ref="#PWR0182"  Part="1" 
F 0 "#PWR0182" H 9475 2300 50  0001 C CNN
F 1 "ISO_DGND" H 9480 2377 50  0000 C CNN
F 2 "" H 9475 2550 50  0001 C CNN
F 3 "" H 9475 2550 50  0001 C CNN
	1    9475 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	9075 2700 9075 2500
Connection ~ 9075 2500
$Comp
L PMSM_Driver_LV_Board_Power:ISO_DGND #PWR?
U 1 1 61422724
P 9075 4500
AR Path="/5F05E355/61422724" Ref="#PWR?"  Part="1" 
AR Path="/60EB5E32/61422724" Ref="#PWR?"  Part="1" 
AR Path="/5EA5DFF7/61422724" Ref="#PWR0181"  Part="1" 
F 0 "#PWR0181" H 9075 4250 50  0001 C CNN
F 1 "ISO_DGND" H 9080 4327 50  0000 C CNN
F 2 "" H 9075 4500 50  0001 C CNN
F 3 "" H 9075 4500 50  0001 C CNN
	1    9075 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	9075 4500 9075 4400
Wire Wire Line
	6500 3350 8725 3350
Wire Wire Line
	6050 3950 8725 3950
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 6144A171
P 7475 3750
AR Path="/5F05E355/6144A171" Ref="R?"  Part="1" 
AR Path="/6068445B/6144A171" Ref="R?"  Part="1" 
AR Path="/60E816A3/6144A171" Ref="R?"  Part="1" 
AR Path="/60EB5E32/6144A171" Ref="R?"  Part="1" 
AR Path="/5EA5DFF7/6144A171" Ref="R110"  Part="1" 
F 0 "R110" V 7575 3750 50  0000 C CNN
F 1 "10k" V 7375 3750 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 7475 3750 50  0001 C CNN
F 3 "~" H 7475 3750 50  0001 C CNN
F 4 "MCWR04X1002FTL -  SMD Chip Resistor, 0402 [1005 Metric], 10 kohm, MCWR Series, 50 V, Thick Film, 62.5 mW " H 7475 3350 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x1002ftl/res-10k-1-0-0625w-thick-film/dp/2447096" H 7475 3250 50  0001 C CNN "Distributor Link"
	1    7475 3750
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7275 3750 7375 3750
Wire Wire Line
	7625 3750 7625 3650
Wire Wire Line
	7575 3750 7625 3750
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 6144A186
P 7875 3500
AR Path="/5E9C6910/6144A186" Ref="C?"  Part="1" 
AR Path="/5EBC6654/6144A186" Ref="C?"  Part="1" 
AR Path="/5EBC681B/6144A186" Ref="C?"  Part="1" 
AR Path="/5F05E355/6144A186" Ref="C?"  Part="1" 
AR Path="/6068445B/6144A186" Ref="C?"  Part="1" 
AR Path="/60EB5E32/6144A186" Ref="C?"  Part="1" 
AR Path="/5EA5DFF7/6144A186" Ref="C63"  Part="1" 
F 0 "C63" V 7825 3325 50  0000 L CNN
F 1 "120pF" V 7825 3550 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 7875 3500 50  0001 C CNN
F 3 "~" H 7875 3500 50  0001 C CNN
F 4 "0402B121K500CT -  SMD Multilayer Ceramic Capacitor, General Purpose, 120 pF, 50 V, 0402 [1005 Metric], ± 10%, X7R" H 7875 3100 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/walsin/0402b121k500ct/cap-120pf-50v-10-x7r-0402-reel/dp/2524680" H 7875 3000 50  0001 C CNN "Distributor Link"
	1    7875 3500
	0    1    1    0   
$EndComp
Wire Wire Line
	7625 3650 7625 3500
Connection ~ 7625 3650
Wire Wire Line
	8100 3500 8100 3650
Connection ~ 8100 3650
Text GLabel 8200 3650 2    50   Input ~ 0
ISO_EXC_LO
Wire Wire Line
	8100 3650 8200 3650
Wire Notes Line
	5700 600  11100 600 
$Comp
L PMSM_Driver_LV_Board_Device:Q_PNP_BEC Q2
U 1 1 614A7FAE
P 10275 2750
F 0 "Q2" H 10475 2750 50  0000 L CNN
F 1 "BC856BW" H 10475 2825 25  0000 L CNN
F 2 "PMSM_Driver_LV_Board:SOT-323_SC-70" H 10475 2850 50  0001 C CNN
F 3 "~" H 10275 2750 50  0001 C CNN
F 4 "BC856BW -  Bipolar (BJT) Single Transistor, PNP, -65 V, 200 MHz, 200 mW, -100 mA, 290 hFE" H 10275 2750 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/diodes-inc/bc856bw/transistor-pnp-65v-0-1a-sot323/dp/1902486" H 10275 2750 50  0001 C CNN "Distributor Link"
	1    10275 2750
	1    0    0    1   
$EndComp
Wire Wire Line
	10175 900  10175 1000
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 614AFA2F
P 9975 1200
AR Path="/5F05E355/614AFA2F" Ref="R?"  Part="1" 
AR Path="/6068445B/614AFA2F" Ref="R?"  Part="1" 
AR Path="/60E816A3/614AFA2F" Ref="R?"  Part="1" 
AR Path="/60EB5E32/614AFA2F" Ref="R?"  Part="1" 
AR Path="/5EA5DFF7/614AFA2F" Ref="R113"  Part="1" 
F 0 "R113" H 10125 1225 50  0000 C CNN
F 1 "2k2" H 10075 1125 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 9975 1200 50  0001 C CNN
F 3 "~" H 9975 1200 50  0001 C CNN
F 4 "MCWF08R2201BTL -  SMD Chip Resistor, 0805 [2012 Metric], 2.2 kohm, MCWF08R Series, 150 V, Thin Film, 125 mW" H 9975 800 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwf08r2201btl/res-2k2-0-1-0-125w-0805-thin-film/dp/2337697" H 9975 700 50  0001 C CNN "Distributor Link"
F 6 "0.1%" H 10075 1050 50  0000 C CNN "Tolerance"
	1    9975 1200
	1    0    0    -1  
$EndComp
Wire Wire Line
	9975 1300 9975 1450
Wire Wire Line
	10075 1450 9975 1450
Wire Wire Line
	10375 1250 10375 1000
Wire Wire Line
	10375 1000 10175 1000
Wire Wire Line
	9975 1000 9975 1100
Connection ~ 10175 1000
Wire Wire Line
	10175 1000 9975 1000
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 614E7A00
P 9975 1650
AR Path="/5F05E355/614E7A00" Ref="R?"  Part="1" 
AR Path="/6068445B/614E7A00" Ref="R?"  Part="1" 
AR Path="/60E816A3/614E7A00" Ref="R?"  Part="1" 
AR Path="/60EB5E32/614E7A00" Ref="R?"  Part="1" 
AR Path="/5EA5DFF7/614E7A00" Ref="R114"  Part="1" 
F 0 "R114" H 10125 1675 50  0000 C CNN
F 1 "3.3" H 10075 1575 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 9975 1650 50  0001 C CNN
F 3 "~" H 9975 1650 50  0001 C CNN
F 4 "WR06X3R3 JTL -  SMD Chip Resistor, 0603 [1608 Metric], 3.3 ohm, WR06 Series, 75 V, Thick Film, 100 mW" H 9975 1250 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/walsin/wr06x3r3-jtl/res-3r3-5-0-1w-0603-thick-film/dp/2670793" H 9975 1150 50  0001 C CNN "Distributor Link"
	1    9975 1650
	1    0    0    -1  
$EndComp
Wire Wire Line
	9975 1450 9975 1550
Wire Wire Line
	9975 1750 9975 1850
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 614F9EE7
P 10375 1850
AR Path="/5F05E355/614F9EE7" Ref="R?"  Part="1" 
AR Path="/6068445B/614F9EE7" Ref="R?"  Part="1" 
AR Path="/60E816A3/614F9EE7" Ref="R?"  Part="1" 
AR Path="/60EB5E32/614F9EE7" Ref="R?"  Part="1" 
AR Path="/5EA5DFF7/614F9EE7" Ref="R121"  Part="1" 
F 0 "R121" H 10525 1875 50  0000 C CNN
F 1 "4.7" H 10475 1775 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 10375 1850 50  0001 C CNN
F 3 "~" H 10375 1850 50  0001 C CNN
F 4 "MCWR06W4R70FTL -  SMD Chip Resistor, 0603 [1608 Metric], 4.7 ohm, MCWR Series, 75 V, Thick Film, 100 mW" H 10375 1450 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr06w4r70ftl/res-4r7-1-0-1w-thick-film/dp/2447390" H 10375 1350 50  0001 C CNN "Distributor Link"
	1    10375 1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	10375 1650 10375 1750
$Comp
L PMSM_Driver_LV_Board_Device:D_Small D2
U 1 1 61514713
P 9975 1950
F 0 "D2" V 9975 1875 50  0000 R CNN
F 1 "1N4148W-7-F" V 9900 1925 25  0000 R CNN
F 2 "PMSM_Driver_LV_Board:D_SOD-123" V 9975 1950 50  0001 C CNN
F 3 "~" V 9975 1950 50  0001 C CNN
F 4 "1N4148W-7-F -  Small Signal Diode, Single, 100 V, 300 mA, 1.25 V, 4 ns, 2 A" V 9975 1950 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/diodes-inc/1n4148w-7-f/diode-switch-300ma-100v-sod123/dp/1776392" V 9975 1950 50  0001 C CNN "Distributor Link"
	1    9975 1950
	0    -1   -1   0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:Q_NPN_BEC Q1
U 1 1 61494CC9
P 10275 1450
F 0 "Q1" H 10475 1450 50  0000 L CNN
F 1 "BC846BLT1G" H 10450 1375 25  0000 L CNN
F 2 "PMSM_Driver_LV_Board:SOT-23" H 10475 1550 50  0001 C CNN
F 3 "~" H 10275 1450 50  0001 C CNN
F 4 "BC846BLT1G. -  Bipolar (BJT) Single Transistor, General Purpose, NPN, 65 V, 100 MHz, 300 mW, 100 mA, 290 hFE" H 10275 1450 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/on-semiconductor/bc846blt1g/transistor-npn-65v-0-1a-sot23/dp/1653605" H 10275 1450 50  0001 C CNN "Distributor Link"
	1    10275 1450
	1    0    0    -1  
$EndComp
Wire Wire Line
	9975 2050 9975 2100
$Comp
L PMSM_Driver_LV_Board_Device:D_Small D3
U 1 1 6152070F
P 9975 2250
F 0 "D3" V 9975 2175 50  0000 R CNN
F 1 "1N4148W-7-F" V 9900 2225 25  0000 R CNN
F 2 "PMSM_Driver_LV_Board:D_SOD-123" V 9975 2250 50  0001 C CNN
F 3 "~" V 9975 2250 50  0001 C CNN
F 4 "1N4148W-7-F -  Small Signal Diode, Single, 100 V, 300 mA, 1.25 V, 4 ns, 2 A" V 9975 2250 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/diodes-inc/1n4148w-7-f/diode-switch-300ma-100v-sod123/dp/1776392" V 9975 2250 50  0001 C CNN "Distributor Link"
	1    9975 2250
	0    -1   -1   0   
$EndComp
Connection ~ 9975 2100
Wire Wire Line
	9975 2100 9975 2150
Wire Wire Line
	10375 1950 10375 2100
Wire Wire Line
	10375 2100 10450 2100
Wire Wire Line
	10375 2250 10375 2100
Connection ~ 10375 2100
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 61547D78
P 10375 2350
AR Path="/5F05E355/61547D78" Ref="R?"  Part="1" 
AR Path="/6068445B/61547D78" Ref="R?"  Part="1" 
AR Path="/60E816A3/61547D78" Ref="R?"  Part="1" 
AR Path="/60EB5E32/61547D78" Ref="R?"  Part="1" 
AR Path="/5EA5DFF7/61547D78" Ref="R122"  Part="1" 
F 0 "R122" H 10525 2375 50  0000 C CNN
F 1 "4.7" H 10475 2275 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 10375 2350 50  0001 C CNN
F 3 "~" H 10375 2350 50  0001 C CNN
F 4 "MCWR06W4R70FTL -  SMD Chip Resistor, 0603 [1608 Metric], 4.7 ohm, MCWR Series, 75 V, Thick Film, 100 mW" H 10375 1950 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr06w4r70ftl/res-4r7-1-0-1w-thick-film/dp/2447390" H 10375 1850 50  0001 C CNN "Distributor Link"
	1    10375 2350
	1    0    0    -1  
$EndComp
Wire Wire Line
	10375 2450 10375 2550
Wire Wire Line
	9975 2350 9975 2450
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 61569451
P 9975 2550
AR Path="/5F05E355/61569451" Ref="R?"  Part="1" 
AR Path="/6068445B/61569451" Ref="R?"  Part="1" 
AR Path="/60E816A3/61569451" Ref="R?"  Part="1" 
AR Path="/60EB5E32/61569451" Ref="R?"  Part="1" 
AR Path="/5EA5DFF7/61569451" Ref="R115"  Part="1" 
F 0 "R115" H 10125 2575 50  0000 C CNN
F 1 "3.3" H 10075 2475 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 9975 2550 50  0001 C CNN
F 3 "~" H 9975 2550 50  0001 C CNN
F 4 "WR06X3R3 JTL -  SMD Chip Resistor, 0603 [1608 Metric], 3.3 ohm, WR06 Series, 75 V, Thick Film, 100 mW" H 9975 2150 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/walsin/wr06x3r3-jtl/res-3r3-5-0-1w-0603-thick-film/dp/2670793" H 9975 2050 50  0001 C CNN "Distributor Link"
	1    9975 2550
	1    0    0    -1  
$EndComp
Connection ~ 9975 1450
Wire Wire Line
	10075 2750 9975 2750
Wire Wire Line
	9975 2750 9975 2650
Wire Wire Line
	9975 2900 9975 2750
Connection ~ 9975 2750
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 6158B226
P 9975 3000
AR Path="/5F05E355/6158B226" Ref="R?"  Part="1" 
AR Path="/6068445B/6158B226" Ref="R?"  Part="1" 
AR Path="/60E816A3/6158B226" Ref="R?"  Part="1" 
AR Path="/60EB5E32/6158B226" Ref="R?"  Part="1" 
AR Path="/5EA5DFF7/6158B226" Ref="R116"  Part="1" 
F 0 "R116" H 10125 3025 50  0000 C CNN
F 1 "2k2" H 10075 2925 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 9975 3000 50  0001 C CNN
F 3 "~" H 9975 3000 50  0001 C CNN
F 4 "MCWF08R2201BTL -  SMD Chip Resistor, 0805 [2012 Metric], 2.2 kohm, MCWF08R Series, 150 V, Thin Film, 125 mW" H 9975 2600 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwf08r2201btl/res-2k2-0-1-0-125w-0805-thin-film/dp/2337697" H 9975 2500 50  0001 C CNN "Distributor Link"
F 6 "0.1%" H 10075 2850 50  0000 C CNN "Tolerance"
	1    9975 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	10175 3300 10175 3200
Wire Wire Line
	10375 2950 10375 3200
Wire Wire Line
	10375 3200 10175 3200
Wire Wire Line
	9975 3200 9975 3100
Connection ~ 10175 3200
Wire Wire Line
	10175 3200 9975 3200
$Comp
L PMSM_Driver_LV_Board_Power:ISO_DGND #PWR?
U 1 1 61599730
P 10175 3300
AR Path="/5F05E355/61599730" Ref="#PWR?"  Part="1" 
AR Path="/60EB5E32/61599730" Ref="#PWR?"  Part="1" 
AR Path="/5EA5DFF7/61599730" Ref="#PWR0184"  Part="1" 
F 0 "#PWR0184" H 10175 3050 50  0001 C CNN
F 1 "ISO_DGND" H 10180 3127 50  0000 C CNN
F 2 "" H 10175 3300 50  0001 C CNN
F 3 "" H 10175 3300 50  0001 C CNN
	1    10175 3300
	1    0    0    -1  
$EndComp
Text GLabel 10450 2100 2    50   Output ~ 0
ISO_EXC
$Comp
L PMSM_Driver_LV_Board_Device:Q_PNP_BEC Q4
U 1 1 615CD335
P 10275 5625
F 0 "Q4" H 10475 5625 50  0000 L CNN
F 1 "BC856BW" H 10475 5700 25  0000 L CNN
F 2 "PMSM_Driver_LV_Board:SOT-323_SC-70" H 10475 5725 50  0001 C CNN
F 3 "~" H 10275 5625 50  0001 C CNN
F 4 "BC856BW -  Bipolar (BJT) Single Transistor, PNP, -65 V, 200 MHz, 200 mW, -100 mA, 290 hFE" H 10275 5625 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/diodes-inc/bc856bw/transistor-pnp-65v-0-1a-sot323/dp/1902486" H 10275 5625 50  0001 C CNN "Distributor Link"
	1    10275 5625
	1    0    0    1   
$EndComp
Wire Wire Line
	10175 3775 10175 3875
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 615CD345
P 9975 4075
AR Path="/5F05E355/615CD345" Ref="R?"  Part="1" 
AR Path="/6068445B/615CD345" Ref="R?"  Part="1" 
AR Path="/60E816A3/615CD345" Ref="R?"  Part="1" 
AR Path="/60EB5E32/615CD345" Ref="R?"  Part="1" 
AR Path="/5EA5DFF7/615CD345" Ref="R117"  Part="1" 
F 0 "R117" H 10125 4100 50  0000 C CNN
F 1 "2k2" H 10075 4000 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 9975 4075 50  0001 C CNN
F 3 "~" H 9975 4075 50  0001 C CNN
F 4 "MCWF08R2201BTL -  SMD Chip Resistor, 0805 [2012 Metric], 2.2 kohm, MCWF08R Series, 150 V, Thin Film, 125 mW" H 9975 3675 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwf08r2201btl/res-2k2-0-1-0-125w-0805-thin-film/dp/2337697" H 9975 3575 50  0001 C CNN "Distributor Link"
F 6 "0.1%" H 10075 3925 50  0000 C CNN "Tolerance"
	1    9975 4075
	1    0    0    -1  
$EndComp
Wire Wire Line
	9975 4175 9975 4325
Wire Wire Line
	10075 4325 9975 4325
Wire Wire Line
	10375 4125 10375 3875
Wire Wire Line
	10375 3875 10175 3875
Wire Wire Line
	9975 3875 9975 3975
Connection ~ 10175 3875
Wire Wire Line
	10175 3875 9975 3875
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 615CD354
P 9975 4525
AR Path="/5F05E355/615CD354" Ref="R?"  Part="1" 
AR Path="/6068445B/615CD354" Ref="R?"  Part="1" 
AR Path="/60E816A3/615CD354" Ref="R?"  Part="1" 
AR Path="/60EB5E32/615CD354" Ref="R?"  Part="1" 
AR Path="/5EA5DFF7/615CD354" Ref="R118"  Part="1" 
F 0 "R118" H 10125 4550 50  0000 C CNN
F 1 "3.3" H 10075 4450 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 9975 4525 50  0001 C CNN
F 3 "~" H 9975 4525 50  0001 C CNN
F 4 "WR06X3R3 JTL -  SMD Chip Resistor, 0603 [1608 Metric], 3.3 ohm, WR06 Series, 75 V, Thick Film, 100 mW" H 9975 4125 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/walsin/wr06x3r3-jtl/res-3r3-5-0-1w-0603-thick-film/dp/2670793" H 9975 4025 50  0001 C CNN "Distributor Link"
	1    9975 4525
	1    0    0    -1  
$EndComp
Wire Wire Line
	9975 4325 9975 4425
Wire Wire Line
	9975 4625 9975 4725
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 615CD35E
P 10375 4725
AR Path="/5F05E355/615CD35E" Ref="R?"  Part="1" 
AR Path="/6068445B/615CD35E" Ref="R?"  Part="1" 
AR Path="/60E816A3/615CD35E" Ref="R?"  Part="1" 
AR Path="/60EB5E32/615CD35E" Ref="R?"  Part="1" 
AR Path="/5EA5DFF7/615CD35E" Ref="R123"  Part="1" 
F 0 "R123" H 10525 4750 50  0000 C CNN
F 1 "4.7" H 10475 4650 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 10375 4725 50  0001 C CNN
F 3 "~" H 10375 4725 50  0001 C CNN
F 4 "MCWR06W4R70FTL -  SMD Chip Resistor, 0603 [1608 Metric], 4.7 ohm, MCWR Series, 75 V, Thick Film, 100 mW" H 10375 4325 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr06w4r70ftl/res-4r7-1-0-1w-thick-film/dp/2447390" H 10375 4225 50  0001 C CNN "Distributor Link"
	1    10375 4725
	1    0    0    -1  
$EndComp
Wire Wire Line
	10375 4525 10375 4625
$Comp
L PMSM_Driver_LV_Board_Device:D_Small D4
U 1 1 615CD367
P 9975 4825
F 0 "D4" V 9975 4750 50  0000 R CNN
F 1 "1N4148W-7-F" V 9900 4800 25  0000 R CNN
F 2 "PMSM_Driver_LV_Board:D_SOD-123" V 9975 4825 50  0001 C CNN
F 3 "~" V 9975 4825 50  0001 C CNN
F 4 "1N4148W-7-F -  Small Signal Diode, Single, 100 V, 300 mA, 1.25 V, 4 ns, 2 A" V 9975 4825 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/diodes-inc/1n4148w-7-f/diode-switch-300ma-100v-sod123/dp/1776392" V 9975 4825 50  0001 C CNN "Distributor Link"
	1    9975 4825
	0    -1   -1   0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:Q_NPN_BEC Q3
U 1 1 615CD36F
P 10275 4325
F 0 "Q3" H 10475 4325 50  0000 L CNN
F 1 "BC846BLT1G" H 10450 4250 25  0000 L CNN
F 2 "PMSM_Driver_LV_Board:SOT-23" H 10475 4425 50  0001 C CNN
F 3 "~" H 10275 4325 50  0001 C CNN
F 4 "BC846BLT1G. -  Bipolar (BJT) Single Transistor, General Purpose, NPN, 65 V, 100 MHz, 300 mW, 100 mA, 290 hFE" H 10275 4325 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/on-semiconductor/bc846blt1g/transistor-npn-65v-0-1a-sot23/dp/1653605" H 10275 4325 50  0001 C CNN "Distributor Link"
	1    10275 4325
	1    0    0    -1  
$EndComp
Wire Wire Line
	9975 4925 9975 4975
$Comp
L PMSM_Driver_LV_Board_Device:D_Small D5
U 1 1 615CD378
P 9975 5125
F 0 "D5" V 9975 5050 50  0000 R CNN
F 1 "1N4148W-7-F" V 9900 5100 25  0000 R CNN
F 2 "PMSM_Driver_LV_Board:D_SOD-123" V 9975 5125 50  0001 C CNN
F 3 "~" V 9975 5125 50  0001 C CNN
F 4 "1N4148W-7-F -  Small Signal Diode, Single, 100 V, 300 mA, 1.25 V, 4 ns, 2 A" V 9975 5125 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/diodes-inc/1n4148w-7-f/diode-switch-300ma-100v-sod123/dp/1776392" V 9975 5125 50  0001 C CNN "Distributor Link"
	1    9975 5125
	0    -1   -1   0   
$EndComp
Connection ~ 9975 4975
Wire Wire Line
	9975 4975 9975 5025
Wire Wire Line
	10375 4825 10375 4975
Wire Wire Line
	10375 4975 10450 4975
Wire Wire Line
	10375 5125 10375 4975
Connection ~ 10375 4975
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 615CD387
P 10375 5225
AR Path="/5F05E355/615CD387" Ref="R?"  Part="1" 
AR Path="/6068445B/615CD387" Ref="R?"  Part="1" 
AR Path="/60E816A3/615CD387" Ref="R?"  Part="1" 
AR Path="/60EB5E32/615CD387" Ref="R?"  Part="1" 
AR Path="/5EA5DFF7/615CD387" Ref="R124"  Part="1" 
F 0 "R124" H 10525 5250 50  0000 C CNN
F 1 "4.7" H 10475 5150 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 10375 5225 50  0001 C CNN
F 3 "~" H 10375 5225 50  0001 C CNN
F 4 "MCWR06W4R70FTL -  SMD Chip Resistor, 0603 [1608 Metric], 4.7 ohm, MCWR Series, 75 V, Thick Film, 100 mW" H 10375 4825 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr06w4r70ftl/res-4r7-1-0-1w-thick-film/dp/2447390" H 10375 4725 50  0001 C CNN "Distributor Link"
	1    10375 5225
	1    0    0    -1  
$EndComp
Wire Wire Line
	10375 5325 10375 5425
Wire Wire Line
	9975 5225 9975 5325
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 615CD391
P 9975 5425
AR Path="/5F05E355/615CD391" Ref="R?"  Part="1" 
AR Path="/6068445B/615CD391" Ref="R?"  Part="1" 
AR Path="/60E816A3/615CD391" Ref="R?"  Part="1" 
AR Path="/60EB5E32/615CD391" Ref="R?"  Part="1" 
AR Path="/5EA5DFF7/615CD391" Ref="R119"  Part="1" 
F 0 "R119" H 10125 5450 50  0000 C CNN
F 1 "3.3" H 10075 5350 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 9975 5425 50  0001 C CNN
F 3 "~" H 9975 5425 50  0001 C CNN
F 4 "WR06X3R3 JTL -  SMD Chip Resistor, 0603 [1608 Metric], 3.3 ohm, WR06 Series, 75 V, Thick Film, 100 mW" H 9975 5025 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/walsin/wr06x3r3-jtl/res-3r3-5-0-1w-0603-thick-film/dp/2670793" H 9975 4925 50  0001 C CNN "Distributor Link"
	1    9975 5425
	1    0    0    -1  
$EndComp
Connection ~ 9975 4325
Wire Wire Line
	10075 5625 9975 5625
Wire Wire Line
	9975 5625 9975 5525
Wire Wire Line
	9975 5775 9975 5625
Connection ~ 9975 5625
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 615CD39F
P 9975 5875
AR Path="/5F05E355/615CD39F" Ref="R?"  Part="1" 
AR Path="/6068445B/615CD39F" Ref="R?"  Part="1" 
AR Path="/60E816A3/615CD39F" Ref="R?"  Part="1" 
AR Path="/60EB5E32/615CD39F" Ref="R?"  Part="1" 
AR Path="/5EA5DFF7/615CD39F" Ref="R120"  Part="1" 
F 0 "R120" H 10125 5900 50  0000 C CNN
F 1 "2k2" H 10075 5800 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 9975 5875 50  0001 C CNN
F 3 "~" H 9975 5875 50  0001 C CNN
F 4 "MCWF08R2201BTL -  SMD Chip Resistor, 0805 [2012 Metric], 2.2 kohm, MCWF08R Series, 150 V, Thin Film, 125 mW" H 9975 5475 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwf08r2201btl/res-2k2-0-1-0-125w-0805-thin-film/dp/2337697" H 9975 5375 50  0001 C CNN "Distributor Link"
F 6 "0.1%" H 10075 5725 50  0000 C CNN "Tolerance"
	1    9975 5875
	1    0    0    -1  
$EndComp
Wire Wire Line
	10175 6175 10175 6075
Wire Wire Line
	10375 5825 10375 6075
Wire Wire Line
	10375 6075 10175 6075
Wire Wire Line
	9975 6075 9975 5975
Connection ~ 10175 6075
Wire Wire Line
	10175 6075 9975 6075
$Comp
L PMSM_Driver_LV_Board_Power:ISO_DGND #PWR?
U 1 1 615CD3AB
P 10175 6175
AR Path="/5F05E355/615CD3AB" Ref="#PWR?"  Part="1" 
AR Path="/60EB5E32/615CD3AB" Ref="#PWR?"  Part="1" 
AR Path="/5EA5DFF7/615CD3AB" Ref="#PWR0186"  Part="1" 
F 0 "#PWR0186" H 10175 5925 50  0001 C CNN
F 1 "ISO_DGND" H 10180 6002 50  0000 C CNN
F 2 "" H 10175 6175 50  0001 C CNN
F 3 "" H 10175 6175 50  0001 C CNN
	1    10175 6175
	1    0    0    -1  
$EndComp
Wire Notes Line
	11100 6475 5700 6475
Wire Notes Line
	5700 600  5700 6475
Wire Notes Line
	11100 600  11100 6475
Text GLabel 10450 4975 2    50   Output ~ 0
ISO_EXC_LO
Wire Wire Line
	9475 3250 9725 3250
Wire Wire Line
	9725 3250 9725 2100
Wire Wire Line
	9725 2100 9975 2100
Wire Wire Line
	9475 3850 9725 3850
Wire Wire Line
	9725 3850 9725 4975
Wire Wire Line
	9725 4975 9975 4975
Text Notes 7100 3100 0    25   ~ 0
3.6Vp-p
Text Notes 8275 3200 0    25   ~ 0
Feedback gain is 1.54.
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 617F8019
P 6050 3800
AR Path="/5F05E355/617F8019" Ref="R?"  Part="1" 
AR Path="/6068445B/617F8019" Ref="R?"  Part="1" 
AR Path="/60E816A3/617F8019" Ref="R?"  Part="1" 
AR Path="/60EB5E32/617F8019" Ref="R?"  Part="1" 
AR Path="/5EA5DFF7/617F8019" Ref="R105"  Part="1" 
F 0 "R105" H 5900 3800 50  0000 C CNN
F 1 "22k" H 5950 3875 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6050 3800 50  0001 C CNN
F 3 "~" H 6050 3800 50  0001 C CNN
F 4 "ERJ6RBD2202V -  SMD Chip Resistor, 0805 [2012 Metric], 22 kohm, ERJ6R Series, 150 V, Thick Film, 100 mW" H 6050 3400 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/panasonic/erj6rbd2202v/res-22k-0-5-0-1w-0805-thick-film/dp/2380345" H 6050 3300 50  0001 C CNN "Distributor Link"
	1    6050 3800
	-1   0    0    1   
$EndComp
Text Notes 7625 3350 0    25   ~ 0
Common-mode voltage is 3.75V for ISO_RDC_VEXC=12V.
Wire Wire Line
	7975 3050 8100 3050
Wire Wire Line
	7975 2900 8100 2900
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 6184E6BB
P 7875 3650
AR Path="/5F05E355/6184E6BB" Ref="R?"  Part="1" 
AR Path="/6068445B/6184E6BB" Ref="R?"  Part="1" 
AR Path="/60E816A3/6184E6BB" Ref="R?"  Part="1" 
AR Path="/60EB5E32/6184E6BB" Ref="R?"  Part="1" 
AR Path="/5EA5DFF7/6184E6BB" Ref="R112"  Part="1" 
F 0 "R112" V 7925 3800 50  0000 C CNN
F 1 "15k4" V 7925 3525 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 7875 3650 50  0001 C CNN
F 3 "~" H 7875 3650 50  0001 C CNN
F 4 "MCWF06R1542BTL -  SMD Chip Resistor, 0603 [1608 Metric], 15.4 kohm, MCWF06R Series, 75 V, Thin Film, 100 mW" H 7875 3250 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwf06r1542btl/res-15k4-0-1-0-1w-0603-thin-film/dp/2336991" H 7875 3150 50  0001 C CNN "Distributor Link"
	1    7875 3650
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7975 3500 8100 3500
Wire Wire Line
	7975 3650 8100 3650
Text Notes 10400 2050 0    25   ~ 0
5.544Vp-p for ISO_RDC_VEXC>=7V
$Comp
L PMSM_Driver_LV_Board_Power:ISO_RES_VEXC #PWR0183
U 1 1 61A5BD10
P 10175 900
F 0 "#PWR0183" H 10175 1150 50  0001 C CNN
F 1 "ISO_RES_VEXC" H 10176 1073 50  0000 C CNN
F 2 "" H 10175 900 50  0001 C CNN
F 3 "" H 10175 900 50  0001 C CNN
	1    10175 900 
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:ISO_RES_VEXC #PWR0180
U 1 1 61A65F14
P 9075 2400
F 0 "#PWR0180" H 9075 2650 50  0001 C CNN
F 1 "ISO_RES_VEXC" H 9076 2573 50  0000 C CNN
F 2 "" H 9075 2400 50  0001 C CNN
F 3 "" H 9075 2400 50  0001 C CNN
	1    9075 2400
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:ISO_RES_VEXC #PWR0178
U 1 1 61A78F1B
P 6500 3000
F 0 "#PWR0178" H 6500 3250 50  0001 C CNN
F 1 "ISO_RES_VEXC" H 6501 3173 50  0000 C CNN
F 2 "" H 6500 3000 50  0001 C CNN
F 3 "" H 6500 3000 50  0001 C CNN
	1    6500 3000
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:ISO_RES_VEXC #PWR0176
U 1 1 61A9EE45
P 6050 3600
F 0 "#PWR0176" H 6050 3850 50  0001 C CNN
F 1 "ISO_RES_VEXC" H 6051 3773 50  0000 C CNN
F 2 "" H 6050 3600 50  0001 C CNN
F 3 "" H 6050 3600 50  0001 C CNN
	1    6050 3600
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:ISO_RES_VEXC #PWR0185
U 1 1 61AB1E64
P 10175 3775
F 0 "#PWR0185" H 10175 4025 50  0001 C CNN
F 1 "ISO_RES_VEXC" H 10176 3948 50  0000 C CNN
F 2 "" H 10175 3775 50  0001 C CNN
F 3 "" H 10175 3775 50  0001 C CNN
	1    10175 3775
	1    0    0    -1  
$EndComp
Text Notes 700  875  0    25   ~ 0
With SPI output and encoder emulation output.
Connection ~ 7625 3750
Wire Wire Line
	7625 3750 8725 3750
Wire Wire Line
	7625 3500 7775 3500
Wire Wire Line
	7625 3650 7775 3650
Connection ~ 7625 3150
Wire Wire Line
	7625 3150 8725 3150
Wire Wire Line
	7625 3050 7775 3050
Wire Wire Line
	7625 2900 7775 2900
$EndSCHEMATC
