EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 8 16
Title "PMSM_Driver_LV_Board"
Date "2020-03-30"
Rev "A"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L PMSM_Driver_LV_Board_IC:ADUM7223ACCZ U25
U 1 1 5EA45BCF
P 2350 1800
F 0 "U25" H 2050 2250 50  0000 C CNN
F 1 "ADUM7223ACCZ" H 2400 1350 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:ADUM7223ACCZ" H 2350 1250 50  0001 C CNN
F 3 "~" H 2050 2250 50  0001 C CNN
F 4 "ADUM7223ACCZ -  Digital Isolator, Dual, 2 Channel, 40 ns, 3 V, 5.5 V, LGA, 13 Pins" H 2350 1150 31  0001 C CNN "Description"
F 5 "https://uk.farnell.com/analog-devices/adum7223accz/digital-isolator-2ch-46ns-lga/dp/2379771" H 2350 1100 31  0001 C CNN "Distributor Link"
F 6 "https://www.mouser.co.uk/ProductDetail/Analog-Devices/ADUM7223ACCZ?qs=sGAEpiMZZMvQcoNRkxSQknfgB19UTwPcTuWmjdDMJWo%3D" H 2350 1050 31  0001 C CNN "Distributor Link 2"
	1    2350 1800
	1    0    0    -1  
$EndComp
Text GLabel 3625 1275 2    50   Input ~ 0
MOT_CHARGE_PUMP_A
Text GLabel 3550 1700 2    50   Input ~ 0
MOT_OUT_A
Text GLabel 3550 6350 2    50   Input ~ 0
MOT_OUT_C
Text GLabel 3625 3600 2    50   Input ~ 0
MOT_CHARGE_PUMP_B
Text GLabel 3550 4025 2    50   Input ~ 0
MOT_OUT_B
Text GLabel 1600 1600 0    50   Input ~ 0
ISO_PWM_AH
Text GLabel 1600 1700 0    50   Input ~ 0
ISO_PWM_AL
Text GLabel 1600 3925 0    50   Input ~ 0
ISO_PWM_BH
Text GLabel 1600 4025 0    50   Input ~ 0
ISO_PWM_BL
Text GLabel 1600 6250 0    50   Input ~ 0
ISO_PWM_CH
Text GLabel 1600 6350 0    50   Input ~ 0
ISO_PWM_CL
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR0196
U 1 1 5EBCA352
P 1800 2400
F 0 "#PWR0196" H 1800 2150 50  0001 C CNN
F 1 "MOT_DGND" H 1805 2227 50  0000 C CNN
F 2 "" H 1800 2400 50  0001 C CNN
F 3 "" H 1800 2400 50  0001 C CNN
	1    1800 2400
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_PGND #PWR0200
U 1 1 5EBCAF6A
P 2900 2400
F 0 "#PWR0200" H 2900 2150 50  0001 C CNN
F 1 "MOT_PGND" H 2905 2227 50  0000 C CNN
F 2 "" H 2900 2400 50  0001 C CNN
F 3 "" H 2900 2400 50  0001 C CNN
	1    2900 2400
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_15V0 #PWR0199
U 1 1 5EBCB482
P 2900 925
F 0 "#PWR0199" H 2900 1175 50  0001 C CNN
F 1 "MOT_15V0" H 2901 1098 50  0000 C CNN
F 2 "" H 2900 925 50  0001 C CNN
F 3 "" H 2900 925 50  0001 C CNN
	1    2900 925 
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_5V0 #PWR0193
U 1 1 5EBCB857
P 1700 1400
F 0 "#PWR0193" H 1700 1650 50  0001 C CNN
F 1 "MOT_5V0" H 1701 1573 50  0000 C CNN
F 2 "" H 1700 1400 50  0001 C CNN
F 3 "" H 1700 1400 50  0001 C CNN
	1    1700 1400
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:D_Small D?
U 1 1 5F4EA355
P 3425 1275
AR Path="/5EA41912/5F4EA355" Ref="D?"  Part="1" 
AR Path="/5EA90258/5F4EA355" Ref="D?"  Part="1" 
AR Path="/5EA5E06A/5F4EA355" Ref="D9"  Part="1" 
F 0 "D9" H 3425 1375 50  0000 C CNN
F 1 "ES1C" H 3425 1200 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:D_SMA" V 3425 1275 50  0001 C CNN
F 3 "~" V 3425 1275 50  0001 C CNN
F 4 "ES1C -  Fast / Ultrafast Diode, 150 V, 1 A, Single, 920 mV, 15 ns, 30 A" H 3425 1275 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/on-semiconductor/es1c/rect-ultrafast-1a-150v-do-214ac/dp/2453261" H 3425 1275 50  0001 C CNN "Distributor Link"
	1    3425 1275
	1    0    0    -1  
$EndComp
Wire Wire Line
	3525 1275 3625 1275
$Comp
L PMSM_Driver_LV_Board_Device:D_Small D?
U 1 1 5F4EDA14
P 2900 1075
AR Path="/5EA41912/5F4EDA14" Ref="D?"  Part="1" 
AR Path="/5EA90258/5F4EDA14" Ref="D?"  Part="1" 
AR Path="/5EA5E06A/5F4EDA14" Ref="D6"  Part="1" 
F 0 "D6" V 2900 975 50  0000 C CNN
F 1 "1N4148W-7-F" V 2825 900 25  0000 C CNN
F 2 "PMSM_Driver_LV_Board:D_SOD-123" V 2900 1075 50  0001 C CNN
F 3 "~" V 2900 1075 50  0001 C CNN
F 4 "1N4148W-7-F -  Small Signal Diode, Single, 100 V, 300 mA, 1.25 V, 4 ns, 2 A" H 2900 1075 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/diodes-inc/1n4148w-7-f/diode-switch-300ma-100v-sod123/dp/1776392" H 2900 1075 50  0001 C CNN "Distributor Link"
	1    2900 1075
	0    -1   -1   0   
$EndComp
NoConn ~ 1900 2000
Wire Wire Line
	2800 2100 2900 2100
Wire Wire Line
	1800 1500 1900 1500
Wire Wire Line
	1700 1400 1700 1800
Wire Wire Line
	1700 2100 1900 2100
Wire Wire Line
	1900 1800 1700 1800
Connection ~ 1700 1800
Wire Wire Line
	1700 1800 1700 2100
Wire Wire Line
	1600 1600 1900 1600
Wire Wire Line
	1600 1700 1900 1700
Text GLabel 1600 1900 0    50   Input ~ 0
ISO_MOT_DISABLE
Wire Wire Line
	1600 1900 1900 1900
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5F4FD4E0
P 1350 2250
AR Path="/5E9C6910/5F4FD4E0" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5F4FD4E0" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5F4FD4E0" Ref="C?"  Part="1" 
AR Path="/5F05E355/5F4FD4E0" Ref="C?"  Part="1" 
AR Path="/5EA41912/5F4FD4E0" Ref="C?"  Part="1" 
AR Path="/5EA5E06A/5F4FD4E0" Ref="C68"  Part="1" 
F 0 "C68" H 1125 2250 50  0000 L CNN
F 1 "100nF" H 1075 2175 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 1350 2250 50  0001 C CNN
F 3 "~" H 1350 2250 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 1350 1850 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 1350 1750 50  0001 C CNN "Distributor Link"
	1    1350 2250
	-1   0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR0190
U 1 1 5F4FE412
P 1350 2400
F 0 "#PWR0190" H 1350 2150 50  0001 C CNN
F 1 "MOT_DGND" H 1355 2227 50  0000 C CNN
F 2 "" H 1350 2400 50  0001 C CNN
F 3 "" H 1350 2400 50  0001 C CNN
	1    1350 2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	1350 2100 1350 2150
Wire Wire Line
	1350 2400 1350 2350
Wire Wire Line
	1800 1500 1800 2400
Wire Wire Line
	2900 2100 2900 2400
Wire Wire Line
	1350 2100 1700 2100
Connection ~ 1700 2100
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5F5059F4
P 925 2250
AR Path="/5E9C6910/5F5059F4" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5F5059F4" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5F5059F4" Ref="C?"  Part="1" 
AR Path="/5F05E355/5F5059F4" Ref="C?"  Part="1" 
AR Path="/5EA41912/5F5059F4" Ref="C?"  Part="1" 
AR Path="/5EA5E06A/5F5059F4" Ref="C65"  Part="1" 
F 0 "C65" H 700 2250 50  0000 L CNN
F 1 "1uF" H 725 2175 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 925 2250 50  0001 C CNN
F 3 "~" H 925 2250 50  0001 C CNN
F 4 "LMK107B7105KA-T -  SMD Multilayer Ceramic Capacitor, 1 µF, 10 V, 0603 [1608 Metric], ± 10%, X7R, M Series" H 925 1850 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/taiyo-yuden/lmk107b7105ka-t/cap-1-f-10v-10-x7r-0603/dp/2112849" H 925 1750 50  0001 C CNN "Distributor Link"
	1    925  2250
	-1   0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR0187
U 1 1 5F5059FA
P 925 2400
F 0 "#PWR0187" H 925 2150 50  0001 C CNN
F 1 "MOT_DGND" H 930 2227 50  0000 C CNN
F 2 "" H 925 2400 50  0001 C CNN
F 3 "" H 925 2400 50  0001 C CNN
	1    925  2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	925  2100 925  2150
Wire Wire Line
	925  2400 925  2350
Wire Wire Line
	925  2100 1350 2100
Connection ~ 1350 2100
Text GLabel 2900 1600 2    50   Output ~ 0
MOT_GATE_AH
Wire Wire Line
	2800 1600 2900 1600
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5F5122F3
P 3075 1450
AR Path="/5E9C6910/5F5122F3" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5F5122F3" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5F5122F3" Ref="C?"  Part="1" 
AR Path="/5F05E355/5F5122F3" Ref="C?"  Part="1" 
AR Path="/5EA41912/5F5122F3" Ref="C?"  Part="1" 
AR Path="/5EA5E06A/5F5122F3" Ref="C71"  Part="1" 
F 0 "C71" V 2975 1400 50  0000 L CNN
F 1 "1uF" V 3025 1250 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0805_2012Metric" H 3075 1450 50  0001 C CNN
F 3 "~" H 3075 1450 50  0001 C CNN
F 4 "08051C105K4T2A -  SMD Multilayer Ceramic Capacitor, 1 µF, 100 V, 0805 [2012 Metric], ± 10%, X7R" H 3075 1050 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/avx/08051c105k4t2a/cap-1-f-100v-10-x7r-0805/dp/2834935" H 3075 950 50  0001 C CNN "Distributor Link"
	1    3075 1450
	0    -1   1    0   
$EndComp
Wire Wire Line
	2800 1500 2900 1500
Wire Wire Line
	2900 1500 2900 1450
Wire Wire Line
	2975 1450 2900 1450
Connection ~ 2900 1450
Wire Wire Line
	2800 1700 3500 1700
Wire Wire Line
	3175 1450 3500 1450
Connection ~ 3500 1700
Wire Wire Line
	3500 1700 3550 1700
Text GLabel 2900 2000 2    50   Output ~ 0
MOT_GATE_AL
Wire Wire Line
	2800 2000 2900 2000
$Comp
L PMSM_Driver_LV_Board_Power:MOT_15V0 #PWR0208
U 1 1 5F524FF4
P 4100 1750
F 0 "#PWR0208" H 4100 2000 50  0001 C CNN
F 1 "MOT_15V0" H 4101 1923 50  0000 C CNN
F 2 "" H 4100 1750 50  0001 C CNN
F 3 "" H 4100 1750 50  0001 C CNN
	1    4100 1750
	1    0    0    -1  
$EndComp
Wire Wire Line
	2800 1900 3525 1900
Wire Wire Line
	4100 1900 4100 1750
$Comp
L PMSM_Driver_LV_Board_Power:MOT_PGND #PWR0205
U 1 1 5F5261C4
P 3525 2400
F 0 "#PWR0205" H 3525 2150 50  0001 C CNN
F 1 "MOT_PGND" H 3530 2227 50  0000 C CNN
F 2 "" H 3525 2400 50  0001 C CNN
F 3 "" H 3525 2400 50  0001 C CNN
	1    3525 2400
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5F5273A6
P 3525 2250
AR Path="/5E9C6910/5F5273A6" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5F5273A6" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5F5273A6" Ref="C?"  Part="1" 
AR Path="/5F05E355/5F5273A6" Ref="C?"  Part="1" 
AR Path="/5EA41912/5F5273A6" Ref="C?"  Part="1" 
AR Path="/5EA5E06A/5F5273A6" Ref="C74"  Part="1" 
F 0 "C74" H 3300 2250 50  0000 L CNN
F 1 "4.7uF" H 3250 2175 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0805_2012Metric" H 3525 2250 50  0001 C CNN
F 3 "~" H 3525 2250 50  0001 C CNN
F 4 "EMK212B7475KG-T -  SMD Multilayer Ceramic Capacitor, 4.7 µF, 16 V, 0805 [2012 Metric], ± 10%, X7R, M Series" H 3525 1850 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/taiyo-yuden/emk212b7475kg-t/cap-4-7-f-16v-10-x7r-0805/dp/2112841" H 3525 1750 50  0001 C CNN "Distributor Link"
	1    3525 2250
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3525 2400 3525 2350
Wire Wire Line
	3525 2150 3525 1900
Connection ~ 3525 1900
Wire Wire Line
	3525 1900 4100 1900
Text GLabel 7650 4600 2    50   Output ~ 0
MOT_PHASE_B
Text GLabel 6250 6425 2    50   Output ~ 0
MOT_PHASE_C
Text Notes 675  800  0    100  ~ 0
Phase A Gate Driver
Text Notes 4850 800  0    100  ~ 0
Phase A Half Bridge
$Comp
L PMSM_Driver_LV_Board_Power:MOT_VDC #PWR0213
U 1 1 5F52AA89
P 6750 900
F 0 "#PWR0213" H 6750 1150 50  0001 C CNN
F 1 "MOT_VDC" H 6751 1073 50  0000 C CNN
F 2 "" H 6750 900 50  0001 C CNN
F 3 "" H 6750 900 50  0001 C CNN
	1    6750 900 
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_PGND #PWR0214
U 1 1 5F52B218
P 6750 2525
F 0 "#PWR0214" H 6750 2275 50  0001 C CNN
F 1 "MOT_PGND" H 6755 2352 50  0000 C CNN
F 2 "" H 6750 2525 50  0001 C CNN
F 3 "" H 6750 2525 50  0001 C CNN
	1    6750 2525
	1    0    0    -1  
$EndComp
Wire Wire Line
	7475 950  7475 1000
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5F5334B8
P 7925 1100
AR Path="/5E9C6910/5F5334B8" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5F5334B8" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5F5334B8" Ref="C?"  Part="1" 
AR Path="/5F05E355/5F5334B8" Ref="C?"  Part="1" 
AR Path="/5EA41912/5F5334B8" Ref="C?"  Part="1" 
AR Path="/5EA5E06A/5F5334B8" Ref="C81"  Part="1" 
F 0 "C81" H 8025 1075 50  0000 L CNN
F 1 "1uF" H 8025 1150 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0805_2012Metric" H 7925 1100 50  0001 C CNN
F 3 "~" H 7925 1100 50  0001 C CNN
F 4 "08051C105K4T2A -  SMD Multilayer Ceramic Capacitor, 1 µF, 100 V, 0805 [2012 Metric], ± 10%, X7R" H 7925 700 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/avx/08051c105k4t2a/cap-1-f-100v-10-x7r-0805/dp/2834935" H 7925 600 50  0001 C CNN "Distributor Link"
	1    7925 1100
	1    0    0    1   
$EndComp
Wire Wire Line
	7475 950  7925 950 
Wire Wire Line
	7925 950  7925 1000
$Comp
L PMSM_Driver_LV_Board_Power:MOT_PGND #PWR0219
U 1 1 5F5348BC
P 7475 1250
F 0 "#PWR0219" H 7475 1000 50  0001 C CNN
F 1 "MOT_PGND" H 7480 1077 50  0000 C CNN
F 2 "" H 7475 1250 50  0001 C CNN
F 3 "" H 7475 1250 50  0001 C CNN
	1    7475 1250
	1    0    0    -1  
$EndComp
Wire Wire Line
	7475 1250 7475 1200
$Comp
L PMSM_Driver_LV_Board_Power:MOT_PGND #PWR0221
U 1 1 5F53512F
P 7925 1250
F 0 "#PWR0221" H 7925 1000 50  0001 C CNN
F 1 "MOT_PGND" H 7930 1077 50  0000 C CNN
F 2 "" H 7925 1250 50  0001 C CNN
F 3 "" H 7925 1250 50  0001 C CNN
	1    7925 1250
	1    0    0    -1  
$EndComp
Wire Wire Line
	7925 1250 7925 1200
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R135
U 1 1 5EA24354
P 6250 1375
F 0 "R135" V 6175 1375 50  0000 C CNN
F 1 "22" V 6325 1375 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 6250 1375 50  0001 C CNN
F 3 "~" H 6250 1375 50  0001 C CNN
F 4 "MCWR04X22R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 22 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 6250 975 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x22r0ftl/res-22r-1-0-0625w-0402-thick-film/dp/2447138" H 6250 875 50  0001 C CNN "Distributor Link"
	1    6250 1375
	0    1    1    0   
$EndComp
Wire Wire Line
	6350 1375 6400 1375
Wire Wire Line
	6400 1375 6400 1425
Connection ~ 6400 1375
Wire Wire Line
	6400 1375 6450 1375
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R139
U 1 1 5EA27394
P 6400 1525
F 0 "R139" H 6550 1525 50  0000 C CNN
F 1 "10k" H 6500 1450 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 6400 1525 50  0001 C CNN
F 3 "~" H 6400 1525 50  0001 C CNN
F 4 "MCWR04X1002FTL -  SMD Chip Resistor, 0402 [1005 Metric], 10 kohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 6400 1125 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x1002ftl/res-10k-1-0-0625w-thick-film/dp/2447096" H 6400 1025 50  0001 C CNN "Distributor Link"
	1    6400 1525
	1    0    0    -1  
$EndComp
Wire Wire Line
	6400 1725 6750 1725
Wire Wire Line
	6750 1675 6750 1575
Wire Wire Line
	6400 1375 6400 1175
Wire Wire Line
	6400 1175 6350 1175
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R131
U 1 1 5EA33B79
P 6000 1175
F 0 "R131" V 5925 1175 50  0000 C CNN
F 1 "3.3" V 6075 1175 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 6000 1175 50  0001 C CNN
F 3 "~" H 6000 1175 50  0001 C CNN
F 4 "WR06X3R3 JTL -  SMD Chip Resistor, 0603 [1608 Metric], 3.3 ohm, WR06 Series, 75 V, Thick Film, 100 mW" H 6000 775 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/walsin/wr06x3r3-jtl/res-3r3-5-0-1w-0603-thick-film/dp/2670793" H 6000 675 50  0001 C CNN "Distributor Link"
	1    6000 1175
	0    1    1    0   
$EndComp
Wire Wire Line
	6100 1175 6150 1175
Wire Wire Line
	5900 1175 5850 1175
Wire Wire Line
	5850 1175 5850 1375
Wire Wire Line
	5850 1375 6150 1375
Connection ~ 6750 1675
Wire Wire Line
	6750 2325 6750 2425
Text GLabel 5800 1375 0    50   Input ~ 0
MOT_GATE_AH
Wire Wire Line
	5850 1375 5800 1375
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R136
U 1 1 5EA5897D
P 6250 2125
F 0 "R136" V 6175 2125 50  0000 C CNN
F 1 "22" V 6325 2125 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 6250 2125 50  0001 C CNN
F 3 "~" H 6250 2125 50  0001 C CNN
F 4 "MCWR04X22R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 22 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 6250 1725 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x22r0ftl/res-22r-1-0-0625w-0402-thick-film/dp/2447138" H 6250 1625 50  0001 C CNN "Distributor Link"
	1    6250 2125
	0    1    1    0   
$EndComp
Wire Wire Line
	6350 2125 6400 2125
Wire Wire Line
	6400 2125 6400 2175
Connection ~ 6400 2125
Wire Wire Line
	6400 2125 6450 2125
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R140
U 1 1 5EA58989
P 6400 2275
F 0 "R140" H 6550 2275 50  0000 C CNN
F 1 "10k" H 6500 2200 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 6400 2275 50  0001 C CNN
F 3 "~" H 6400 2275 50  0001 C CNN
F 4 "MCWR04X1002FTL -  SMD Chip Resistor, 0402 [1005 Metric], 10 kohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 6400 1875 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x1002ftl/res-10k-1-0-0625w-thick-film/dp/2447096" H 6400 1775 50  0001 C CNN "Distributor Link"
	1    6400 2275
	1    0    0    -1  
$EndComp
Wire Wire Line
	6400 2375 6400 2425
Wire Wire Line
	6750 2425 6400 2425
Wire Wire Line
	6400 2125 6400 1925
Wire Wire Line
	6400 1925 6350 1925
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R132
U 1 1 5EA58995
P 6000 1925
F 0 "R132" V 5925 1925 50  0000 C CNN
F 1 "3.3" V 6075 1925 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 6000 1925 50  0001 C CNN
F 3 "~" H 6000 1925 50  0001 C CNN
F 4 "WR06X3R3 JTL -  SMD Chip Resistor, 0603 [1608 Metric], 3.3 ohm, WR06 Series, 75 V, Thick Film, 100 mW" H 6000 1525 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/walsin/wr06x3r3-jtl/res-3r3-5-0-1w-0603-thick-film/dp/2670793" H 6000 1425 50  0001 C CNN "Distributor Link"
	1    6000 1925
	0    1    1    0   
$EndComp
Wire Wire Line
	6100 1925 6150 1925
Wire Wire Line
	5900 1925 5850 1925
Wire Wire Line
	5850 1925 5850 2125
Wire Wire Line
	5850 2125 6150 2125
$Comp
L PMSM_Driver_LV_Board_Device:D_Small D?
U 1 1 5EA589A1
P 6250 1925
AR Path="/5EA41912/5EA589A1" Ref="D?"  Part="1" 
AR Path="/5EA90258/5EA589A1" Ref="D?"  Part="1" 
AR Path="/5EA5E06A/5EA589A1" Ref="D15"  Part="1" 
F 0 "D15" H 6250 2025 50  0000 C CNN
F 1 "1N4148W-7-F" H 6450 1975 25  0000 C CNN
F 2 "PMSM_Driver_LV_Board:D_SOD-123" V 6250 1925 50  0001 C CNN
F 3 "~" V 6250 1925 50  0001 C CNN
F 4 "1N4148W-7-F -  Small Signal Diode, Single, 100 V, 300 mA, 1.25 V, 4 ns, 2 A" H 6250 1925 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/diodes-inc/1n4148w-7-f/diode-switch-300ma-100v-sod123/dp/1776392" H 6250 1925 50  0001 C CNN "Distributor Link"
	1    6250 1925
	1    0    0    -1  
$EndComp
Wire Wire Line
	5850 2125 5800 2125
Connection ~ 6750 2425
Wire Wire Line
	6750 2425 6750 2525
Connection ~ 5850 2125
Wire Wire Line
	6750 1675 6750 1725
Text GLabel 5800 2125 0    50   Input ~ 0
MOT_GATE_AL
Text GLabel 7650 1675 2    50   Output ~ 0
MOT_OUT_A
Connection ~ 5850 1375
Wire Wire Line
	7550 1775 7550 1675
Wire Wire Line
	7550 1675 7650 1675
Connection ~ 7550 1675
Wire Wire Line
	7550 2275 7550 2175
Wire Wire Line
	7550 2275 7650 2275
Text GLabel 7650 2275 2    50   Output ~ 0
MOT_PHASE_A
Wire Wire Line
	7700 1875 7800 1875
Text GLabel 7800 1875 2    50   Output ~ 0
MOT_SHUNT_A
$Comp
L PMSM_Driver_LV_Board_Power:MOT_SENS_A_AGND #PWR0223
U 1 1 5EAB9533
P 8300 2250
F 0 "#PWR0223" H 8300 2000 50  0001 C CNN
F 1 "MOT_SENS_A_AGND" H 8305 2077 50  0000 C CNN
F 2 "" H 8300 2250 50  0001 C CNN
F 3 "" H 8300 2250 50  0001 C CNN
	1    8300 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	7700 2075 8300 2075
Wire Wire Line
	8300 2075 8300 2250
$Comp
L PMSM_Driver_LV_Board_Device:R_Shunt_US R143
U 1 1 5EAD7DA9
P 7550 1975
F 0 "R143" H 7463 2021 50  0000 R CNN
F 1 "0.01" H 7463 1930 50  0000 R CNN
F 2 "PMSM_Driver_LV_Board:WSL3637R0100FEA" V 7480 1975 50  0001 C CNN
F 3 "~" H 7550 1975 50  0001 C CNN
F 4 "WSL3637R0100FEA -  SMD Current Sense Resistor, 0.01 ohm, WSL3637 Series, 3637 [9194 Metric], 3 W, ± 1%, 4-Terminal" H 7550 1475 31  0001 C CNN "Description"
F 5 "https://uk.farnell.com/vishay/wsl3637r0100fea/current-sense-res-0r01-1-3-w-3637/dp/2395994" H 7550 1375 31  0001 C CNN "Distributor Link"
	1    7550 1975
	1    0    0    -1  
$EndComp
Text Notes 7750 2050 0    25   ~ 0
Measured voltage is +/- 0.25V at +/- 25A.\nFull scale is +/-32A.\n(See AD7403BRIZ datasheet.)
Wire Wire Line
	6750 1175 6750 950 
Connection ~ 6750 950 
Wire Wire Line
	6750 950  6750 900 
Wire Wire Line
	2900 975  2900 925 
Wire Wire Line
	2900 1275 3325 1275
Wire Wire Line
	2900 1275 2900 1450
Connection ~ 2900 1275
$Comp
L PMSM_Driver_LV_Board_Device:D_Small D?
U 1 1 5EA3668B
P 6250 1175
AR Path="/5EA41912/5EA3668B" Ref="D?"  Part="1" 
AR Path="/5EA90258/5EA3668B" Ref="D?"  Part="1" 
AR Path="/5EA5E06A/5EA3668B" Ref="D14"  Part="1" 
F 0 "D14" H 6250 1275 50  0000 C CNN
F 1 "1N4148W-7-F" H 6450 1225 25  0000 C CNN
F 2 "PMSM_Driver_LV_Board:D_SOD-123" V 6250 1175 50  0001 C CNN
F 3 "~" V 6250 1175 50  0001 C CNN
F 4 "1N4148W-7-F -  Small Signal Diode, Single, 100 V, 300 mA, 1.25 V, 4 ns, 2 A" H 6250 1175 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/diodes-inc/1n4148w-7-f/diode-switch-300ma-100v-sod123/dp/1776392" H 6250 1175 50  0001 C CNN "Distributor Link"
	1    6250 1175
	1    0    0    -1  
$EndComp
Wire Notes Line
	600  2850 8825 2850
Wire Notes Line
	600  600  8825 600 
Wire Notes Line
	600  600  600  2850
Wire Notes Line
	8825 600  8825 2850
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR0197
U 1 1 5ECA9254
P 1800 4725
F 0 "#PWR0197" H 1800 4475 50  0001 C CNN
F 1 "MOT_DGND" H 1805 4552 50  0000 C CNN
F 2 "" H 1800 4725 50  0001 C CNN
F 3 "" H 1800 4725 50  0001 C CNN
	1    1800 4725
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_PGND #PWR0202
U 1 1 5ECA925A
P 2900 4725
F 0 "#PWR0202" H 2900 4475 50  0001 C CNN
F 1 "MOT_PGND" H 2905 4552 50  0000 C CNN
F 2 "" H 2900 4725 50  0001 C CNN
F 3 "" H 2900 4725 50  0001 C CNN
	1    2900 4725
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_15V0 #PWR0201
U 1 1 5ECA9260
P 2900 3250
F 0 "#PWR0201" H 2900 3500 50  0001 C CNN
F 1 "MOT_15V0" H 2901 3423 50  0000 C CNN
F 2 "" H 2900 3250 50  0001 C CNN
F 3 "" H 2900 3250 50  0001 C CNN
	1    2900 3250
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_5V0 #PWR0194
U 1 1 5ECA9266
P 1700 3725
F 0 "#PWR0194" H 1700 3975 50  0001 C CNN
F 1 "MOT_5V0" H 1701 3898 50  0000 C CNN
F 2 "" H 1700 3725 50  0001 C CNN
F 3 "" H 1700 3725 50  0001 C CNN
	1    1700 3725
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:D_Small D?
U 1 1 5ECA926E
P 3425 3600
AR Path="/5EA41912/5ECA926E" Ref="D?"  Part="1" 
AR Path="/5EA90258/5ECA926E" Ref="D?"  Part="1" 
AR Path="/5EA5E06A/5ECA926E" Ref="D10"  Part="1" 
F 0 "D10" H 3425 3700 50  0000 C CNN
F 1 "ES1C" H 3425 3525 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:D_SMA" V 3425 3600 50  0001 C CNN
F 3 "~" V 3425 3600 50  0001 C CNN
F 4 "ES1C -  Fast / Ultrafast Diode, 150 V, 1 A, Single, 920 mV, 15 ns, 30 A" H 3425 3600 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/on-semiconductor/es1c/rect-ultrafast-1a-150v-do-214ac/dp/2453261" H 3425 3600 50  0001 C CNN "Distributor Link"
	1    3425 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	3525 3600 3625 3600
$Comp
L PMSM_Driver_LV_Board_Device:D_Small D?
U 1 1 5ECA9277
P 2900 3400
AR Path="/5EA41912/5ECA9277" Ref="D?"  Part="1" 
AR Path="/5EA90258/5ECA9277" Ref="D?"  Part="1" 
AR Path="/5EA5E06A/5ECA9277" Ref="D7"  Part="1" 
F 0 "D7" V 2900 3300 50  0000 C CNN
F 1 "1N4148W-7-F" V 2825 3225 25  0000 C CNN
F 2 "PMSM_Driver_LV_Board:D_SOD-123" V 2900 3400 50  0001 C CNN
F 3 "~" V 2900 3400 50  0001 C CNN
F 4 "1N4148W-7-F -  Small Signal Diode, Single, 100 V, 300 mA, 1.25 V, 4 ns, 2 A" H 2900 3400 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/diodes-inc/1n4148w-7-f/diode-switch-300ma-100v-sod123/dp/1776392" H 2900 3400 50  0001 C CNN "Distributor Link"
	1    2900 3400
	0    -1   -1   0   
$EndComp
NoConn ~ 1900 4325
Wire Wire Line
	2800 4425 2900 4425
Wire Wire Line
	1800 3825 1900 3825
Wire Wire Line
	1700 3725 1700 4125
Wire Wire Line
	1700 4425 1900 4425
Wire Wire Line
	1900 4125 1700 4125
Connection ~ 1700 4125
Wire Wire Line
	1700 4125 1700 4425
Wire Wire Line
	1600 3925 1900 3925
Wire Wire Line
	1600 4025 1900 4025
Text GLabel 1600 4225 0    50   Input ~ 0
ISO_MOT_DISABLE
Wire Wire Line
	1600 4225 1900 4225
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5ECA928B
P 1350 4575
AR Path="/5E9C6910/5ECA928B" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5ECA928B" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5ECA928B" Ref="C?"  Part="1" 
AR Path="/5F05E355/5ECA928B" Ref="C?"  Part="1" 
AR Path="/5EA41912/5ECA928B" Ref="C?"  Part="1" 
AR Path="/5EA5E06A/5ECA928B" Ref="C69"  Part="1" 
F 0 "C69" H 1125 4575 50  0000 L CNN
F 1 "100nF" H 1075 4500 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 1350 4575 50  0001 C CNN
F 3 "~" H 1350 4575 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 1350 4175 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 1350 4075 50  0001 C CNN "Distributor Link"
	1    1350 4575
	-1   0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR0191
U 1 1 5ECA9291
P 1350 4725
F 0 "#PWR0191" H 1350 4475 50  0001 C CNN
F 1 "MOT_DGND" H 1355 4552 50  0000 C CNN
F 2 "" H 1350 4725 50  0001 C CNN
F 3 "" H 1350 4725 50  0001 C CNN
	1    1350 4725
	1    0    0    -1  
$EndComp
Wire Wire Line
	1350 4425 1350 4475
Wire Wire Line
	1350 4725 1350 4675
Wire Wire Line
	1800 3825 1800 4725
Wire Wire Line
	2900 4425 2900 4725
Wire Wire Line
	1350 4425 1700 4425
Connection ~ 1700 4425
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5ECA929F
P 925 4575
AR Path="/5E9C6910/5ECA929F" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5ECA929F" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5ECA929F" Ref="C?"  Part="1" 
AR Path="/5F05E355/5ECA929F" Ref="C?"  Part="1" 
AR Path="/5EA41912/5ECA929F" Ref="C?"  Part="1" 
AR Path="/5EA5E06A/5ECA929F" Ref="C66"  Part="1" 
F 0 "C66" H 700 4575 50  0000 L CNN
F 1 "1uF" H 725 4500 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 925 4575 50  0001 C CNN
F 3 "~" H 925 4575 50  0001 C CNN
F 4 "LMK107B7105KA-T -  SMD Multilayer Ceramic Capacitor, 1 µF, 10 V, 0603 [1608 Metric], ± 10%, X7R, M Series" H 925 4175 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/taiyo-yuden/lmk107b7105ka-t/cap-1-f-10v-10-x7r-0603/dp/2112849" H 925 4075 50  0001 C CNN "Distributor Link"
	1    925  4575
	-1   0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR0188
U 1 1 5ECA92A5
P 925 4725
F 0 "#PWR0188" H 925 4475 50  0001 C CNN
F 1 "MOT_DGND" H 930 4552 50  0000 C CNN
F 2 "" H 925 4725 50  0001 C CNN
F 3 "" H 925 4725 50  0001 C CNN
	1    925  4725
	1    0    0    -1  
$EndComp
Wire Wire Line
	925  4425 925  4475
Wire Wire Line
	925  4725 925  4675
Wire Wire Line
	925  4425 1350 4425
Connection ~ 1350 4425
Wire Wire Line
	2800 3925 2900 3925
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5ECA92B3
P 3075 3775
AR Path="/5E9C6910/5ECA92B3" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5ECA92B3" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5ECA92B3" Ref="C?"  Part="1" 
AR Path="/5F05E355/5ECA92B3" Ref="C?"  Part="1" 
AR Path="/5EA41912/5ECA92B3" Ref="C?"  Part="1" 
AR Path="/5EA5E06A/5ECA92B3" Ref="C72"  Part="1" 
F 0 "C72" V 2975 3725 50  0000 L CNN
F 1 "1uF" V 3025 3575 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0805_2012Metric" H 3075 3775 50  0001 C CNN
F 3 "~" H 3075 3775 50  0001 C CNN
F 4 "08051C105K4T2A -  SMD Multilayer Ceramic Capacitor, 1 µF, 100 V, 0805 [2012 Metric], ± 10%, X7R" H 3075 3375 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/avx/08051c105k4t2a/cap-1-f-100v-10-x7r-0805/dp/2834935" H 3075 3275 50  0001 C CNN "Distributor Link"
	1    3075 3775
	0    -1   1    0   
$EndComp
Wire Wire Line
	2800 3825 2900 3825
Wire Wire Line
	2900 3825 2900 3775
Wire Wire Line
	2975 3775 2900 3775
Connection ~ 2900 3775
Wire Wire Line
	2800 4025 3500 4025
Wire Wire Line
	3175 3775 3500 3775
Connection ~ 3500 4025
Wire Wire Line
	3500 4025 3550 4025
Text GLabel 2900 4325 2    50   Output ~ 0
MOT_GATE_BL
Wire Wire Line
	2800 4325 2900 4325
$Comp
L PMSM_Driver_LV_Board_Power:MOT_15V0 #PWR0209
U 1 1 5ECA92C4
P 4100 4075
F 0 "#PWR0209" H 4100 4325 50  0001 C CNN
F 1 "MOT_15V0" H 4101 4248 50  0000 C CNN
F 2 "" H 4100 4075 50  0001 C CNN
F 3 "" H 4100 4075 50  0001 C CNN
	1    4100 4075
	1    0    0    -1  
$EndComp
Wire Wire Line
	2800 4225 3525 4225
Wire Wire Line
	4100 4225 4100 4075
$Comp
L PMSM_Driver_LV_Board_Power:MOT_PGND #PWR0206
U 1 1 5ECA92CC
P 3525 4725
F 0 "#PWR0206" H 3525 4475 50  0001 C CNN
F 1 "MOT_PGND" H 3530 4552 50  0000 C CNN
F 2 "" H 3525 4725 50  0001 C CNN
F 3 "" H 3525 4725 50  0001 C CNN
	1    3525 4725
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5ECA92D4
P 3525 4575
AR Path="/5E9C6910/5ECA92D4" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5ECA92D4" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5ECA92D4" Ref="C?"  Part="1" 
AR Path="/5F05E355/5ECA92D4" Ref="C?"  Part="1" 
AR Path="/5EA41912/5ECA92D4" Ref="C?"  Part="1" 
AR Path="/5EA5E06A/5ECA92D4" Ref="C75"  Part="1" 
F 0 "C75" H 3300 4575 50  0000 L CNN
F 1 "4.7uF" H 3250 4500 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0805_2012Metric" H 3525 4575 50  0001 C CNN
F 3 "~" H 3525 4575 50  0001 C CNN
F 4 "EMK212B7475KG-T -  SMD Multilayer Ceramic Capacitor, 4.7 µF, 16 V, 0805 [2012 Metric], ± 10%, X7R, M Series" H 3525 4175 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/taiyo-yuden/emk212b7475kg-t/cap-4-7-f-16v-10-x7r-0805/dp/2112841" H 3525 4075 50  0001 C CNN "Distributor Link"
	1    3525 4575
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3525 4725 3525 4675
Wire Wire Line
	3525 4475 3525 4225
Connection ~ 3525 4225
Wire Wire Line
	3525 4225 4100 4225
Text Notes 675  3125 0    100  ~ 0
Phase B Gate Driver
Text Notes 4850 3125 0    100  ~ 0
Phase B Half Bridge
$Comp
L PMSM_Driver_LV_Board_Power:MOT_VDC #PWR0215
U 1 1 5ECA92E0
P 6750 3225
F 0 "#PWR0215" H 6750 3475 50  0001 C CNN
F 1 "MOT_VDC" H 6751 3398 50  0000 C CNN
F 2 "" H 6750 3225 50  0001 C CNN
F 3 "" H 6750 3225 50  0001 C CNN
	1    6750 3225
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_PGND #PWR0216
U 1 1 5ECA92E6
P 6750 4850
F 0 "#PWR0216" H 6750 4600 50  0001 C CNN
F 1 "MOT_PGND" H 6755 4677 50  0000 C CNN
F 2 "" H 6750 4850 50  0001 C CNN
F 3 "" H 6750 4850 50  0001 C CNN
	1    6750 4850
	1    0    0    -1  
$EndComp
Wire Wire Line
	7475 3275 7475 3325
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5ECA92F8
P 7925 3425
AR Path="/5E9C6910/5ECA92F8" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5ECA92F8" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5ECA92F8" Ref="C?"  Part="1" 
AR Path="/5F05E355/5ECA92F8" Ref="C?"  Part="1" 
AR Path="/5EA41912/5ECA92F8" Ref="C?"  Part="1" 
AR Path="/5EA5E06A/5ECA92F8" Ref="C82"  Part="1" 
F 0 "C82" H 8025 3400 50  0000 L CNN
F 1 "1uF" H 8025 3475 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0805_2012Metric" H 7925 3425 50  0001 C CNN
F 3 "~" H 7925 3425 50  0001 C CNN
F 4 "08051C105K4T2A -  SMD Multilayer Ceramic Capacitor, 1 µF, 100 V, 0805 [2012 Metric], ± 10%, X7R" H 7925 3025 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/avx/08051c105k4t2a/cap-1-f-100v-10-x7r-0805/dp/2834935" H 7925 2925 50  0001 C CNN "Distributor Link"
	1    7925 3425
	1    0    0    1   
$EndComp
Wire Wire Line
	7475 3275 7925 3275
Wire Wire Line
	7925 3275 7925 3325
$Comp
L PMSM_Driver_LV_Board_Power:MOT_PGND #PWR0220
U 1 1 5ECA9300
P 7475 3575
F 0 "#PWR0220" H 7475 3325 50  0001 C CNN
F 1 "MOT_PGND" H 7480 3402 50  0000 C CNN
F 2 "" H 7475 3575 50  0001 C CNN
F 3 "" H 7475 3575 50  0001 C CNN
	1    7475 3575
	1    0    0    -1  
$EndComp
Wire Wire Line
	7475 3575 7475 3525
$Comp
L PMSM_Driver_LV_Board_Power:MOT_PGND #PWR0222
U 1 1 5ECA9307
P 7925 3575
F 0 "#PWR0222" H 7925 3325 50  0001 C CNN
F 1 "MOT_PGND" H 7930 3402 50  0000 C CNN
F 2 "" H 7925 3575 50  0001 C CNN
F 3 "" H 7925 3575 50  0001 C CNN
	1    7925 3575
	1    0    0    -1  
$EndComp
Wire Wire Line
	7925 3575 7925 3525
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R137
U 1 1 5ECA931A
P 6250 3700
F 0 "R137" V 6175 3700 50  0000 C CNN
F 1 "22" V 6325 3700 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 6250 3700 50  0001 C CNN
F 3 "~" H 6250 3700 50  0001 C CNN
F 4 "MCWR04X22R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 22 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 6250 3300 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x22r0ftl/res-22r-1-0-0625w-0402-thick-film/dp/2447138" H 6250 3200 50  0001 C CNN "Distributor Link"
	1    6250 3700
	0    1    1    0   
$EndComp
Wire Wire Line
	6350 3700 6400 3700
Wire Wire Line
	6400 3700 6400 3750
Connection ~ 6400 3700
Wire Wire Line
	6400 3700 6450 3700
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R141
U 1 1 5ECA9326
P 6400 3850
F 0 "R141" H 6550 3850 50  0000 C CNN
F 1 "10k" H 6500 3775 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 6400 3850 50  0001 C CNN
F 3 "~" H 6400 3850 50  0001 C CNN
F 4 "MCWR04X1002FTL -  SMD Chip Resistor, 0402 [1005 Metric], 10 kohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 6400 3450 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x1002ftl/res-10k-1-0-0625w-thick-film/dp/2447096" H 6400 3350 50  0001 C CNN "Distributor Link"
	1    6400 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	6400 4050 6750 4050
Wire Wire Line
	6750 4000 6750 3900
Wire Wire Line
	6400 3700 6400 3500
Wire Wire Line
	6400 3500 6350 3500
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R133
U 1 1 5ECA9333
P 6000 3500
F 0 "R133" V 5925 3500 50  0000 C CNN
F 1 "3.3" V 6075 3500 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 6000 3500 50  0001 C CNN
F 3 "~" H 6000 3500 50  0001 C CNN
F 4 "WR06X3R3 JTL -  SMD Chip Resistor, 0603 [1608 Metric], 3.3 ohm, WR06 Series, 75 V, Thick Film, 100 mW" H 6000 3100 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/walsin/wr06x3r3-jtl/res-3r3-5-0-1w-0603-thick-film/dp/2670793" H 6000 3000 50  0001 C CNN "Distributor Link"
	1    6000 3500
	0    1    1    0   
$EndComp
Wire Wire Line
	6100 3500 6150 3500
Wire Wire Line
	5900 3500 5850 3500
Wire Wire Line
	5850 3500 5850 3700
Wire Wire Line
	5850 3700 6150 3700
Connection ~ 6750 4000
Wire Wire Line
	6750 4650 6750 4750
Text GLabel 5800 3700 0    50   Input ~ 0
MOT_GATE_BH
Wire Wire Line
	5850 3700 5800 3700
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R138
U 1 1 5ECA934D
P 6250 4450
F 0 "R138" V 6175 4450 50  0000 C CNN
F 1 "22" V 6325 4450 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 6250 4450 50  0001 C CNN
F 3 "~" H 6250 4450 50  0001 C CNN
F 4 "MCWR04X22R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 22 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 6250 4050 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x22r0ftl/res-22r-1-0-0625w-0402-thick-film/dp/2447138" H 6250 3950 50  0001 C CNN "Distributor Link"
	1    6250 4450
	0    1    1    0   
$EndComp
Wire Wire Line
	6350 4450 6400 4450
Wire Wire Line
	6400 4450 6400 4500
Connection ~ 6400 4450
Wire Wire Line
	6400 4450 6450 4450
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R142
U 1 1 5ECA9359
P 6400 4600
F 0 "R142" H 6550 4600 50  0000 C CNN
F 1 "10k" H 6500 4525 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 6400 4600 50  0001 C CNN
F 3 "~" H 6400 4600 50  0001 C CNN
F 4 "MCWR04X1002FTL -  SMD Chip Resistor, 0402 [1005 Metric], 10 kohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 6400 4200 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x1002ftl/res-10k-1-0-0625w-thick-film/dp/2447096" H 6400 4100 50  0001 C CNN "Distributor Link"
	1    6400 4600
	1    0    0    -1  
$EndComp
Wire Wire Line
	6400 4700 6400 4750
Wire Wire Line
	6750 4750 6400 4750
Wire Wire Line
	6400 4450 6400 4250
Wire Wire Line
	6400 4250 6350 4250
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R134
U 1 1 5ECA9365
P 6000 4250
F 0 "R134" V 5925 4250 50  0000 C CNN
F 1 "3.3" V 6075 4250 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 6000 4250 50  0001 C CNN
F 3 "~" H 6000 4250 50  0001 C CNN
F 4 "WR06X3R3 JTL -  SMD Chip Resistor, 0603 [1608 Metric], 3.3 ohm, WR06 Series, 75 V, Thick Film, 100 mW" H 6000 3850 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/walsin/wr06x3r3-jtl/res-3r3-5-0-1w-0603-thick-film/dp/2670793" H 6000 3750 50  0001 C CNN "Distributor Link"
	1    6000 4250
	0    1    1    0   
$EndComp
Wire Wire Line
	6100 4250 6150 4250
Wire Wire Line
	5900 4250 5850 4250
Wire Wire Line
	5850 4250 5850 4450
Wire Wire Line
	5850 4450 6150 4450
$Comp
L PMSM_Driver_LV_Board_Device:D_Small D?
U 1 1 5ECA9371
P 6250 4250
AR Path="/5EA41912/5ECA9371" Ref="D?"  Part="1" 
AR Path="/5EA90258/5ECA9371" Ref="D?"  Part="1" 
AR Path="/5EA5E06A/5ECA9371" Ref="D17"  Part="1" 
F 0 "D17" H 6250 4350 50  0000 C CNN
F 1 "1N4148W-7-F" H 6450 4300 25  0000 C CNN
F 2 "PMSM_Driver_LV_Board:D_SOD-123" V 6250 4250 50  0001 C CNN
F 3 "~" V 6250 4250 50  0001 C CNN
F 4 "1N4148W-7-F -  Small Signal Diode, Single, 100 V, 300 mA, 1.25 V, 4 ns, 2 A" H 6250 4250 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/diodes-inc/1n4148w-7-f/diode-switch-300ma-100v-sod123/dp/1776392" H 6250 4250 50  0001 C CNN "Distributor Link"
	1    6250 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	5850 4450 5800 4450
Connection ~ 6750 4750
Wire Wire Line
	6750 4750 6750 4850
Connection ~ 5850 4450
Wire Wire Line
	6750 4000 6750 4050
Text GLabel 5800 4450 0    50   Input ~ 0
MOT_GATE_BL
Text GLabel 7650 4000 2    50   Output ~ 0
MOT_OUT_B
Connection ~ 5850 3700
Wire Wire Line
	7550 4100 7550 4000
Wire Wire Line
	7550 4000 7650 4000
Connection ~ 7550 4000
Wire Wire Line
	7550 4600 7550 4500
Wire Wire Line
	7550 4600 7650 4600
Wire Wire Line
	7700 4200 7800 4200
Text GLabel 7800 4200 2    50   Output ~ 0
MOT_SHUNT_B
Wire Wire Line
	7700 4400 8300 4400
Wire Wire Line
	8300 4400 8300 4575
$Comp
L PMSM_Driver_LV_Board_Device:R_Shunt_US R144
U 1 1 5ECA9393
P 7550 4300
F 0 "R144" H 7463 4346 50  0000 R CNN
F 1 "0.01" H 7463 4255 50  0000 R CNN
F 2 "PMSM_Driver_LV_Board:WSL3637R0100FEA" V 7480 4300 50  0001 C CNN
F 3 "~" H 7550 4300 50  0001 C CNN
F 4 "WSL3637R0100FEA -  SMD Current Sense Resistor, 0.01 ohm, WSL3637 Series, 3637 [9194 Metric], 3 W, ± 1%, 4-Terminal" H 7550 3800 31  0001 C CNN "Description"
F 5 "https://uk.farnell.com/vishay/wsl3637r0100fea/current-sense-res-0r01-1-3-w-3637/dp/2395994" H 7550 3700 31  0001 C CNN "Distributor Link"
	1    7550 4300
	1    0    0    -1  
$EndComp
Wire Wire Line
	6750 3500 6750 3275
Connection ~ 6750 3275
Wire Wire Line
	6750 3275 6750 3225
Wire Wire Line
	2900 3300 2900 3250
Wire Wire Line
	2900 3600 3325 3600
Wire Wire Line
	2900 3600 2900 3775
Connection ~ 2900 3600
$Comp
L PMSM_Driver_LV_Board_Device:D_Small D?
U 1 1 5ECA93A5
P 6250 3500
AR Path="/5EA41912/5ECA93A5" Ref="D?"  Part="1" 
AR Path="/5EA90258/5ECA93A5" Ref="D?"  Part="1" 
AR Path="/5EA5E06A/5ECA93A5" Ref="D16"  Part="1" 
F 0 "D16" H 6250 3600 50  0000 C CNN
F 1 "1N4148W-7-F" H 6450 3550 25  0000 C CNN
F 2 "PMSM_Driver_LV_Board:D_SOD-123" V 6250 3500 50  0001 C CNN
F 3 "~" V 6250 3500 50  0001 C CNN
F 4 "1N4148W-7-F -  Small Signal Diode, Single, 100 V, 300 mA, 1.25 V, 4 ns, 2 A" H 6250 3500 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/diodes-inc/1n4148w-7-f/diode-switch-300ma-100v-sod123/dp/1776392" H 6250 3500 50  0001 C CNN "Distributor Link"
	1    6250 3500
	1    0    0    -1  
$EndComp
Wire Notes Line
	600  5175 8825 5175
Wire Notes Line
	600  2925 8825 2925
Wire Notes Line
	600  2925 600  5175
Wire Notes Line
	8825 2925 8825 5175
Text GLabel 3625 5925 2    50   Input ~ 0
MOT_CHARGE_PUMP_C
Text GLabel 2900 3925 2    50   Output ~ 0
MOT_GATE_BH
$Comp
L PMSM_Driver_LV_Board_IC:ADUM7223ACCZ U27
U 1 1 5ED84BAF
P 2350 6450
F 0 "U27" H 2050 6900 50  0000 C CNN
F 1 "ADUM7223ACCZ" H 2400 6000 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:ADUM7223ACCZ" H 2350 5900 50  0001 C CNN
F 3 "~" H 2050 6900 50  0001 C CNN
F 4 "ADUM7223ACCZ -  Digital Isolator, Dual, 2 Channel, 40 ns, 3 V, 5.5 V, LGA, 13 Pins" H 2350 5800 31  0001 C CNN "Description"
F 5 "https://uk.farnell.com/analog-devices/adum7223accz/digital-isolator-2ch-46ns-lga/dp/2379771" H 2350 5750 31  0001 C CNN "Distributor Link"
F 6 "https://www.mouser.co.uk/ProductDetail/Analog-Devices/ADUM7223ACCZ?qs=sGAEpiMZZMvQcoNRkxSQknfgB19UTwPcTuWmjdDMJWo%3D" H 2350 5700 31  0001 C CNN "Distributor Link 2"
	1    2350 6450
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR0198
U 1 1 5ED84BB5
P 1800 7050
F 0 "#PWR0198" H 1800 6800 50  0001 C CNN
F 1 "MOT_DGND" H 1805 6877 50  0000 C CNN
F 2 "" H 1800 7050 50  0001 C CNN
F 3 "" H 1800 7050 50  0001 C CNN
	1    1800 7050
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_PGND #PWR0204
U 1 1 5ED84BBB
P 2900 7050
F 0 "#PWR0204" H 2900 6800 50  0001 C CNN
F 1 "MOT_PGND" H 2905 6877 50  0000 C CNN
F 2 "" H 2900 7050 50  0001 C CNN
F 3 "" H 2900 7050 50  0001 C CNN
	1    2900 7050
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_15V0 #PWR0203
U 1 1 5ED84BC1
P 2900 5575
F 0 "#PWR0203" H 2900 5825 50  0001 C CNN
F 1 "MOT_15V0" H 2901 5748 50  0000 C CNN
F 2 "" H 2900 5575 50  0001 C CNN
F 3 "" H 2900 5575 50  0001 C CNN
	1    2900 5575
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_5V0 #PWR0195
U 1 1 5ED84BC7
P 1700 6050
F 0 "#PWR0195" H 1700 6300 50  0001 C CNN
F 1 "MOT_5V0" H 1701 6223 50  0000 C CNN
F 2 "" H 1700 6050 50  0001 C CNN
F 3 "" H 1700 6050 50  0001 C CNN
	1    1700 6050
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:D_Small D?
U 1 1 5ED84BCF
P 3425 5925
AR Path="/5EA41912/5ED84BCF" Ref="D?"  Part="1" 
AR Path="/5EA90258/5ED84BCF" Ref="D?"  Part="1" 
AR Path="/5EA5E06A/5ED84BCF" Ref="D11"  Part="1" 
F 0 "D11" H 3425 6025 50  0000 C CNN
F 1 "ES1C" H 3425 5850 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:D_SMA" V 3425 5925 50  0001 C CNN
F 3 "~" V 3425 5925 50  0001 C CNN
F 4 "ES1C -  Fast / Ultrafast Diode, 150 V, 1 A, Single, 920 mV, 15 ns, 30 A" H 3425 5925 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/on-semiconductor/es1c/rect-ultrafast-1a-150v-do-214ac/dp/2453261" H 3425 5925 50  0001 C CNN "Distributor Link"
	1    3425 5925
	1    0    0    -1  
$EndComp
Wire Wire Line
	3525 5925 3625 5925
$Comp
L PMSM_Driver_LV_Board_Device:D_Small D?
U 1 1 5ED84BD8
P 2900 5725
AR Path="/5EA41912/5ED84BD8" Ref="D?"  Part="1" 
AR Path="/5EA90258/5ED84BD8" Ref="D?"  Part="1" 
AR Path="/5EA5E06A/5ED84BD8" Ref="D8"  Part="1" 
F 0 "D8" V 2900 5625 50  0000 C CNN
F 1 "1N4148W-7-F" V 2825 5550 25  0000 C CNN
F 2 "PMSM_Driver_LV_Board:D_SOD-123" V 2900 5725 50  0001 C CNN
F 3 "~" V 2900 5725 50  0001 C CNN
F 4 "1N4148W-7-F -  Small Signal Diode, Single, 100 V, 300 mA, 1.25 V, 4 ns, 2 A" H 2900 5725 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/diodes-inc/1n4148w-7-f/diode-switch-300ma-100v-sod123/dp/1776392" H 2900 5725 50  0001 C CNN "Distributor Link"
	1    2900 5725
	0    -1   -1   0   
$EndComp
NoConn ~ 1900 6650
Wire Wire Line
	2800 6750 2900 6750
Wire Wire Line
	1800 6150 1900 6150
Wire Wire Line
	1700 6050 1700 6450
Wire Wire Line
	1700 6750 1900 6750
Wire Wire Line
	1900 6450 1700 6450
Connection ~ 1700 6450
Wire Wire Line
	1700 6450 1700 6750
Wire Wire Line
	1600 6250 1900 6250
Wire Wire Line
	1600 6350 1900 6350
Text GLabel 1600 6550 0    50   Input ~ 0
ISO_MOT_DISABLE
Wire Wire Line
	1600 6550 1900 6550
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5ED84BEC
P 1350 6900
AR Path="/5E9C6910/5ED84BEC" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5ED84BEC" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5ED84BEC" Ref="C?"  Part="1" 
AR Path="/5F05E355/5ED84BEC" Ref="C?"  Part="1" 
AR Path="/5EA41912/5ED84BEC" Ref="C?"  Part="1" 
AR Path="/5EA5E06A/5ED84BEC" Ref="C70"  Part="1" 
F 0 "C70" H 1125 6900 50  0000 L CNN
F 1 "100nF" H 1075 6825 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 1350 6900 50  0001 C CNN
F 3 "~" H 1350 6900 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 1350 6500 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 1350 6400 50  0001 C CNN "Distributor Link"
	1    1350 6900
	-1   0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR0192
U 1 1 5ED84BF2
P 1350 7050
F 0 "#PWR0192" H 1350 6800 50  0001 C CNN
F 1 "MOT_DGND" H 1355 6877 50  0000 C CNN
F 2 "" H 1350 7050 50  0001 C CNN
F 3 "" H 1350 7050 50  0001 C CNN
	1    1350 7050
	1    0    0    -1  
$EndComp
Wire Wire Line
	1350 6750 1350 6800
Wire Wire Line
	1350 7050 1350 7000
Wire Wire Line
	1800 6150 1800 7050
Wire Wire Line
	2900 6750 2900 7050
Wire Wire Line
	1350 6750 1700 6750
Connection ~ 1700 6750
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5ED84C00
P 925 6900
AR Path="/5E9C6910/5ED84C00" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5ED84C00" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5ED84C00" Ref="C?"  Part="1" 
AR Path="/5F05E355/5ED84C00" Ref="C?"  Part="1" 
AR Path="/5EA41912/5ED84C00" Ref="C?"  Part="1" 
AR Path="/5EA5E06A/5ED84C00" Ref="C67"  Part="1" 
F 0 "C67" H 700 6900 50  0000 L CNN
F 1 "1uF" H 725 6825 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 925 6900 50  0001 C CNN
F 3 "~" H 925 6900 50  0001 C CNN
F 4 "LMK107B7105KA-T -  SMD Multilayer Ceramic Capacitor, 1 µF, 10 V, 0603 [1608 Metric], ± 10%, X7R, M Series" H 925 6500 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/taiyo-yuden/lmk107b7105ka-t/cap-1-f-10v-10-x7r-0603/dp/2112849" H 925 6400 50  0001 C CNN "Distributor Link"
	1    925  6900
	-1   0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR0189
U 1 1 5ED84C06
P 925 7050
F 0 "#PWR0189" H 925 6800 50  0001 C CNN
F 1 "MOT_DGND" H 930 6877 50  0000 C CNN
F 2 "" H 925 7050 50  0001 C CNN
F 3 "" H 925 7050 50  0001 C CNN
	1    925  7050
	1    0    0    -1  
$EndComp
Wire Wire Line
	925  6750 925  6800
Wire Wire Line
	925  7050 925  7000
Wire Wire Line
	925  6750 1350 6750
Connection ~ 1350 6750
Wire Wire Line
	2800 6250 2900 6250
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5ED84C13
P 3075 6100
AR Path="/5E9C6910/5ED84C13" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5ED84C13" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5ED84C13" Ref="C?"  Part="1" 
AR Path="/5F05E355/5ED84C13" Ref="C?"  Part="1" 
AR Path="/5EA41912/5ED84C13" Ref="C?"  Part="1" 
AR Path="/5EA5E06A/5ED84C13" Ref="C73"  Part="1" 
F 0 "C73" V 2975 6050 50  0000 L CNN
F 1 "1uF" V 3025 5900 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0805_2012Metric" H 3075 6100 50  0001 C CNN
F 3 "~" H 3075 6100 50  0001 C CNN
F 4 "08051C105K4T2A -  SMD Multilayer Ceramic Capacitor, 1 µF, 100 V, 0805 [2012 Metric], ± 10%, X7R" H 3075 5700 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/avx/08051c105k4t2a/cap-1-f-100v-10-x7r-0805/dp/2834935" H 3075 5600 50  0001 C CNN "Distributor Link"
	1    3075 6100
	0    -1   1    0   
$EndComp
Wire Wire Line
	2800 6150 2900 6150
Wire Wire Line
	2900 6150 2900 6100
Wire Wire Line
	2975 6100 2900 6100
Connection ~ 2900 6100
Wire Wire Line
	2800 6350 3500 6350
Wire Wire Line
	3175 6100 3500 6100
Connection ~ 3500 6350
Wire Wire Line
	3500 6350 3550 6350
Text GLabel 2900 6650 2    50   Output ~ 0
MOT_GATE_CL
Wire Wire Line
	2800 6650 2900 6650
$Comp
L PMSM_Driver_LV_Board_Power:MOT_15V0 #PWR0210
U 1 1 5ED84C24
P 4100 6400
F 0 "#PWR0210" H 4100 6650 50  0001 C CNN
F 1 "MOT_15V0" H 4101 6573 50  0000 C CNN
F 2 "" H 4100 6400 50  0001 C CNN
F 3 "" H 4100 6400 50  0001 C CNN
	1    4100 6400
	1    0    0    -1  
$EndComp
Wire Wire Line
	2800 6550 3525 6550
Wire Wire Line
	4100 6550 4100 6400
$Comp
L PMSM_Driver_LV_Board_Power:MOT_PGND #PWR0207
U 1 1 5ED84C2C
P 3525 7050
F 0 "#PWR0207" H 3525 6800 50  0001 C CNN
F 1 "MOT_PGND" H 3530 6877 50  0000 C CNN
F 2 "" H 3525 7050 50  0001 C CNN
F 3 "" H 3525 7050 50  0001 C CNN
	1    3525 7050
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5ED84C34
P 3525 6900
AR Path="/5E9C6910/5ED84C34" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5ED84C34" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5ED84C34" Ref="C?"  Part="1" 
AR Path="/5F05E355/5ED84C34" Ref="C?"  Part="1" 
AR Path="/5EA41912/5ED84C34" Ref="C?"  Part="1" 
AR Path="/5EA5E06A/5ED84C34" Ref="C76"  Part="1" 
F 0 "C76" H 3300 6900 50  0000 L CNN
F 1 "4.7uF" H 3250 6825 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0805_2012Metric" H 3525 6900 50  0001 C CNN
F 3 "~" H 3525 6900 50  0001 C CNN
F 4 "EMK212B7475KG-T -  SMD Multilayer Ceramic Capacitor, 4.7 µF, 16 V, 0805 [2012 Metric], ± 10%, X7R, M Series" H 3525 6500 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/taiyo-yuden/emk212b7475kg-t/cap-4-7-f-16v-10-x7r-0805/dp/2112841" H 3525 6400 50  0001 C CNN "Distributor Link"
	1    3525 6900
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3525 7050 3525 7000
Wire Wire Line
	3525 6800 3525 6550
Connection ~ 3525 6550
Wire Wire Line
	3525 6550 4100 6550
Text Notes 675  5450 0    100  ~ 0
Phase C Gate Driver
Text Notes 4300 5450 0    100  ~ 0
Phase C Half Bridge
$Comp
L PMSM_Driver_LV_Board_Power:MOT_VDC #PWR0211
U 1 1 5ED84C40
P 6050 5550
F 0 "#PWR0211" H 6050 5800 50  0001 C CNN
F 1 "MOT_VDC" H 6051 5723 50  0000 C CNN
F 2 "" H 6050 5550 50  0001 C CNN
F 3 "" H 6050 5550 50  0001 C CNN
	1    6050 5550
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_PGND #PWR0212
U 1 1 5ED84C46
P 6050 7175
F 0 "#PWR0212" H 6050 6925 50  0001 C CNN
F 1 "MOT_PGND" H 6055 7002 50  0000 C CNN
F 2 "" H 6050 7175 50  0001 C CNN
F 3 "" H 6050 7175 50  0001 C CNN
	1    6050 7175
	1    0    0    -1  
$EndComp
Wire Wire Line
	6775 5600 6775 5650
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5ED84C58
P 7225 5750
AR Path="/5E9C6910/5ED84C58" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5ED84C58" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5ED84C58" Ref="C?"  Part="1" 
AR Path="/5F05E355/5ED84C58" Ref="C?"  Part="1" 
AR Path="/5EA41912/5ED84C58" Ref="C?"  Part="1" 
AR Path="/5EA5E06A/5ED84C58" Ref="C78"  Part="1" 
F 0 "C78" H 7325 5725 50  0000 L CNN
F 1 "1uF" H 7325 5800 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0805_2012Metric" H 7225 5750 50  0001 C CNN
F 3 "~" H 7225 5750 50  0001 C CNN
F 4 "08051C105K4T2A -  SMD Multilayer Ceramic Capacitor, 1 µF, 100 V, 0805 [2012 Metric], ± 10%, X7R" H 7225 5350 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/avx/08051c105k4t2a/cap-1-f-100v-10-x7r-0805/dp/2834935" H 7225 5250 50  0001 C CNN "Distributor Link"
	1    7225 5750
	1    0    0    1   
$EndComp
Wire Wire Line
	6775 5600 7225 5600
Wire Wire Line
	7225 5600 7225 5650
$Comp
L PMSM_Driver_LV_Board_Power:MOT_PGND #PWR0217
U 1 1 5ED84C60
P 6775 5900
F 0 "#PWR0217" H 6775 5650 50  0001 C CNN
F 1 "MOT_PGND" H 6780 5727 50  0000 C CNN
F 2 "" H 6775 5900 50  0001 C CNN
F 3 "" H 6775 5900 50  0001 C CNN
	1    6775 5900
	1    0    0    -1  
$EndComp
Wire Wire Line
	6775 5900 6775 5850
$Comp
L PMSM_Driver_LV_Board_Power:MOT_PGND #PWR0218
U 1 1 5ED84C67
P 7225 5900
F 0 "#PWR0218" H 7225 5650 50  0001 C CNN
F 1 "MOT_PGND" H 7230 5727 50  0000 C CNN
F 2 "" H 7225 5900 50  0001 C CNN
F 3 "" H 7225 5900 50  0001 C CNN
	1    7225 5900
	1    0    0    -1  
$EndComp
Wire Wire Line
	7225 5900 7225 5850
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R127
U 1 1 5ED84C7A
P 5550 6025
F 0 "R127" V 5475 6025 50  0000 C CNN
F 1 "22" V 5625 6025 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 5550 6025 50  0001 C CNN
F 3 "~" H 5550 6025 50  0001 C CNN
F 4 "MCWR04X22R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 22 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 5550 5625 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x22r0ftl/res-22r-1-0-0625w-0402-thick-film/dp/2447138" H 5550 5525 50  0001 C CNN "Distributor Link"
	1    5550 6025
	0    1    1    0   
$EndComp
Wire Wire Line
	5650 6025 5700 6025
Wire Wire Line
	5700 6025 5700 6075
Connection ~ 5700 6025
Wire Wire Line
	5700 6025 5750 6025
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R129
U 1 1 5ED84C86
P 5700 6175
F 0 "R129" H 5850 6175 50  0000 C CNN
F 1 "10k" H 5800 6100 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 5700 6175 50  0001 C CNN
F 3 "~" H 5700 6175 50  0001 C CNN
F 4 "MCWR04X1002FTL -  SMD Chip Resistor, 0402 [1005 Metric], 10 kohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 5700 5775 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x1002ftl/res-10k-1-0-0625w-thick-film/dp/2447096" H 5700 5675 50  0001 C CNN "Distributor Link"
	1    5700 6175
	1    0    0    -1  
$EndComp
Wire Wire Line
	5700 6375 6050 6375
Wire Wire Line
	6050 6325 6050 6225
Wire Wire Line
	5700 6025 5700 5825
Wire Wire Line
	5700 5825 5650 5825
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R125
U 1 1 5ED84C93
P 5300 5825
F 0 "R125" V 5225 5825 50  0000 C CNN
F 1 "3.3" V 5375 5825 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 5300 5825 50  0001 C CNN
F 3 "~" H 5300 5825 50  0001 C CNN
F 4 "WR06X3R3 JTL -  SMD Chip Resistor, 0603 [1608 Metric], 3.3 ohm, WR06 Series, 75 V, Thick Film, 100 mW" H 5300 5425 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/walsin/wr06x3r3-jtl/res-3r3-5-0-1w-0603-thick-film/dp/2670793" H 5300 5325 50  0001 C CNN "Distributor Link"
	1    5300 5825
	0    1    1    0   
$EndComp
Wire Wire Line
	5400 5825 5450 5825
Wire Wire Line
	5200 5825 5150 5825
Wire Wire Line
	5150 5825 5150 6025
Wire Wire Line
	5150 6025 5450 6025
Connection ~ 6050 6325
Wire Wire Line
	6050 6975 6050 7075
Text GLabel 5100 6025 0    50   Input ~ 0
MOT_GATE_CH
Wire Wire Line
	5150 6025 5100 6025
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R128
U 1 1 5ED84CAD
P 5550 6775
F 0 "R128" V 5475 6775 50  0000 C CNN
F 1 "22" V 5625 6775 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 5550 6775 50  0001 C CNN
F 3 "~" H 5550 6775 50  0001 C CNN
F 4 "MCWR04X22R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 22 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 5550 6375 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x22r0ftl/res-22r-1-0-0625w-0402-thick-film/dp/2447138" H 5550 6275 50  0001 C CNN "Distributor Link"
	1    5550 6775
	0    1    1    0   
$EndComp
Wire Wire Line
	5650 6775 5700 6775
Wire Wire Line
	5700 6775 5700 6825
Connection ~ 5700 6775
Wire Wire Line
	5700 6775 5750 6775
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R130
U 1 1 5ED84CB9
P 5700 6925
F 0 "R130" H 5850 6925 50  0000 C CNN
F 1 "10k" H 5800 6850 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 5700 6925 50  0001 C CNN
F 3 "~" H 5700 6925 50  0001 C CNN
F 4 "MCWR04X1002FTL -  SMD Chip Resistor, 0402 [1005 Metric], 10 kohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 5700 6525 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x1002ftl/res-10k-1-0-0625w-thick-film/dp/2447096" H 5700 6425 50  0001 C CNN "Distributor Link"
	1    5700 6925
	1    0    0    -1  
$EndComp
Wire Wire Line
	5700 7025 5700 7075
Wire Wire Line
	6050 7075 5700 7075
Wire Wire Line
	5700 6775 5700 6575
Wire Wire Line
	5700 6575 5650 6575
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R126
U 1 1 5ED84CC5
P 5300 6575
F 0 "R126" V 5225 6575 50  0000 C CNN
F 1 "3.3" V 5375 6575 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 5300 6575 50  0001 C CNN
F 3 "~" H 5300 6575 50  0001 C CNN
F 4 "WR06X3R3 JTL -  SMD Chip Resistor, 0603 [1608 Metric], 3.3 ohm, WR06 Series, 75 V, Thick Film, 100 mW" H 5300 6175 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/walsin/wr06x3r3-jtl/res-3r3-5-0-1w-0603-thick-film/dp/2670793" H 5300 6075 50  0001 C CNN "Distributor Link"
	1    5300 6575
	0    1    1    0   
$EndComp
Wire Wire Line
	5400 6575 5450 6575
Wire Wire Line
	5200 6575 5150 6575
Wire Wire Line
	5150 6575 5150 6775
Wire Wire Line
	5150 6775 5450 6775
$Comp
L PMSM_Driver_LV_Board_Device:D_Small D?
U 1 1 5ED84CD1
P 5550 6575
AR Path="/5EA41912/5ED84CD1" Ref="D?"  Part="1" 
AR Path="/5EA90258/5ED84CD1" Ref="D?"  Part="1" 
AR Path="/5EA5E06A/5ED84CD1" Ref="D13"  Part="1" 
F 0 "D13" H 5550 6675 50  0000 C CNN
F 1 "1N4148W-7-F" H 5750 6625 25  0000 C CNN
F 2 "PMSM_Driver_LV_Board:D_SOD-123" V 5550 6575 50  0001 C CNN
F 3 "~" V 5550 6575 50  0001 C CNN
F 4 "1N4148W-7-F -  Small Signal Diode, Single, 100 V, 300 mA, 1.25 V, 4 ns, 2 A" H 5550 6575 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/diodes-inc/1n4148w-7-f/diode-switch-300ma-100v-sod123/dp/1776392" H 5550 6575 50  0001 C CNN "Distributor Link"
	1    5550 6575
	1    0    0    -1  
$EndComp
Wire Wire Line
	5150 6775 5100 6775
Connection ~ 6050 7075
Wire Wire Line
	6050 7075 6050 7175
Connection ~ 5150 6775
Text GLabel 5100 6775 0    50   Input ~ 0
MOT_GATE_CL
Connection ~ 5150 6025
Wire Wire Line
	6050 5825 6050 5600
Connection ~ 6050 5600
Wire Wire Line
	6050 5600 6050 5550
Wire Wire Line
	2900 5625 2900 5575
Wire Wire Line
	2900 5925 3325 5925
Wire Wire Line
	2900 5925 2900 6100
Connection ~ 2900 5925
$Comp
L PMSM_Driver_LV_Board_Device:D_Small D?
U 1 1 5ED84D03
P 5550 5825
AR Path="/5EA41912/5ED84D03" Ref="D?"  Part="1" 
AR Path="/5EA90258/5ED84D03" Ref="D?"  Part="1" 
AR Path="/5EA5E06A/5ED84D03" Ref="D12"  Part="1" 
F 0 "D12" H 5550 5925 50  0000 C CNN
F 1 "1N4148W-7-F" H 5750 5875 25  0000 C CNN
F 2 "PMSM_Driver_LV_Board:D_SOD-123" V 5550 5825 50  0001 C CNN
F 3 "~" V 5550 5825 50  0001 C CNN
F 4 "1N4148W-7-F -  Small Signal Diode, Single, 100 V, 300 mA, 1.25 V, 4 ns, 2 A" H 5550 5825 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/diodes-inc/1n4148w-7-f/diode-switch-300ma-100v-sod123/dp/1776392" H 5550 5825 50  0001 C CNN "Distributor Link"
	1    5550 5825
	1    0    0    -1  
$EndComp
Wire Notes Line
	600  5250 8825 5250
Text GLabel 2900 6250 2    50   Output ~ 0
MOT_GATE_CH
Wire Notes Line
	600  7675 6900 7675
Wire Notes Line
	6900 7675 6900 6450
Wire Notes Line
	6900 6450 8825 6450
Wire Notes Line
	8825 6450 8825 5250
Wire Notes Line
	600  5250 600  7675
Text GLabel 6250 6325 2    50   Output ~ 0
MOT_OUT_C
$Comp
L PMSM_Driver_LV_Board_Power:MOT_SENS_B_AGND #PWR0224
U 1 1 5EFB5026
P 8300 4575
F 0 "#PWR0224" H 8300 4325 50  0001 C CNN
F 1 "MOT_SENS_B_AGND" H 8305 4402 50  0000 C CNN
F 2 "" H 8300 4575 50  0001 C CNN
F 3 "" H 8300 4575 50  0001 C CNN
	1    8300 4575
	1    0    0    -1  
$EndComp
Text Notes 700  925  0    25   ~ 0
Isolated half-bridge gate driver with bootstrapped high side\nwith the bootstrap capacitor being also charged by a charge pump.
Text Notes 7750 4375 0    25   ~ 0
Measured voltage is +/- 0.25V at +/- 25A.\nFull scale is +/-32A.\n(See AD7403BRIZ datasheet.)
$Comp
L PMSM_Driver_LV_Board_IC:ADUM7223ACCZ U26
U 1 1 5ECA924A
P 2350 4125
F 0 "U26" H 2050 4575 50  0000 C CNN
F 1 "ADUM7223ACCZ" H 2400 3675 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:ADUM7223ACCZ" H 2350 3575 50  0001 C CNN
F 3 "~" H 2050 4575 50  0001 C CNN
F 4 "ADUM7223ACCZ -  Digital Isolator, Dual, 2 Channel, 40 ns, 3 V, 5.5 V, LGA, 13 Pins" H 2350 3475 31  0001 C CNN "Description"
F 5 "https://uk.farnell.com/analog-devices/adum7223accz/digital-isolator-2ch-46ns-lga/dp/2379771" H 2350 3425 31  0001 C CNN "Distributor Link"
F 6 "https://www.mouser.co.uk/ProductDetail/Analog-Devices/ADUM7223ACCZ?qs=sGAEpiMZZMvQcoNRkxSQknfgB19UTwPcTuWmjdDMJWo%3D" H 2350 3375 31  0001 C CNN "Distributor Link 2"
	1    2350 4125
	1    0    0    -1  
$EndComp
Text GLabel 9700 1450 0    50   Input ~ 0
ISO_MOT_EN
Text GLabel 10100 2875 2    50   Output ~ 0
ISO_MOT_DISABLE
$Comp
L PMSM_Driver_LV_Board_Power:MOT_5V0 #PWR?
U 1 1 5F9C38DD
P 10050 1100
AR Path="/5EFE605D/5F9C38DD" Ref="#PWR?"  Part="1" 
AR Path="/5EFE7331/5F9C38DD" Ref="#PWR?"  Part="1" 
AR Path="/5FD7CF82/5F9C38DD" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E295/5F9C38DD" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E06A/5F9C38DD" Ref="#PWR0225"  Part="1" 
F 0 "#PWR0225" H 10050 1350 50  0001 C CNN
F 1 "MOT_5V0" H 10051 1273 50  0000 C CNN
F 2 "" H 10050 1100 50  0001 C CNN
F 3 "" H 10050 1100 50  0001 C CNN
	1    10050 1100
	1    0    0    -1  
$EndComp
Wire Wire Line
	10050 2875 10100 2875
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR?
U 1 1 5F9C38E7
P 10450 1200
AR Path="/5EA41912/5F9C38E7" Ref="#PWR?"  Part="1" 
AR Path="/5FD7CF82/5F9C38E7" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E295/5F9C38E7" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E06A/5F9C38E7" Ref="#PWR0229"  Part="1" 
F 0 "#PWR0229" H 10450 950 50  0001 C CNN
F 1 "MOT_DGND" H 10455 1027 50  0000 C CNN
F 2 "" H 10450 1200 50  0001 C CNN
F 3 "" H 10450 1200 50  0001 C CNN
	1    10450 1200
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5F9C38EF
P 10250 1150
AR Path="/5E9C6910/5F9C38EF" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5F9C38EF" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5F9C38EF" Ref="C?"  Part="1" 
AR Path="/5F05E355/5F9C38EF" Ref="C?"  Part="1" 
AR Path="/60EB5E32/5F9C38EF" Ref="C?"  Part="1" 
AR Path="/5FD7CF82/5F9C38EF" Ref="C?"  Part="1" 
AR Path="/5EA5E295/5F9C38EF" Ref="C?"  Part="1" 
AR Path="/5EA5E06A/5F9C38EF" Ref="C83"  Part="1" 
F 0 "C83" V 10375 1050 50  0000 L CNN
F 1 "100nF" V 10300 875 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 10250 1150 50  0001 C CNN
F 3 "~" H 10250 1150 50  0001 C CNN
F 4 "MC0402X104K250CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 25 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 10250 750 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k250ct/cap-0-1-f-25v-10-x5r-0402/dp/2320760" H 10250 650 50  0001 C CNN "Distributor Link"
	1    10250 1150
	0    -1   -1   0   
$EndComp
Wire Wire Line
	10350 1150 10450 1150
Wire Wire Line
	10450 1150 10450 1200
Wire Wire Line
	10050 1150 10050 1100
Wire Wire Line
	10050 1150 10050 1250
Connection ~ 10050 1150
Wire Wire Line
	10050 1150 10150 1150
Wire Wire Line
	10050 1900 10050 1850
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR?
U 1 1 5F9C38FC
P 10050 1900
AR Path="/5EA41912/5F9C38FC" Ref="#PWR?"  Part="1" 
AR Path="/5FD7CF82/5F9C38FC" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E295/5F9C38FC" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E06A/5F9C38FC" Ref="#PWR0226"  Part="1" 
F 0 "#PWR0226" H 10050 1650 50  0001 C CNN
F 1 "MOT_DGND" H 10055 1727 50  0000 C CNN
F 2 "" H 10050 1900 50  0001 C CNN
F 3 "" H 10050 1900 50  0001 C CNN
	1    10050 1900
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_IC:74AHC1G08GV,125 U?
U 1 1 5F9C3905
P 10050 1550
AR Path="/5EA5E295/5F9C3905" Ref="U?"  Part="1" 
AR Path="/5EA5E06A/5F9C3905" Ref="U28"  Part="1" 
F 0 "U28" H 9825 1800 50  0000 L CNN
F 1 "74AHC1G08GV,125" H 10175 1325 25  0000 L CNN
F 2 "PMSM_Driver_LV_Board:74AHC1G08GV,125" H 10050 950 31  0001 C CNN
F 3 "~" H 10150 1800 31  0001 C CNN
F 4 "74AHC1G08GV,125 - AND Gate, 74AHC1G08, 2 Input, 25 mA, 2 V to 5.5 V, SOT-23-5" H 10050 900 31  0001 C CNN "Description"
F 5 "https://uk.farnell.com/nexperia/74ahc1g08gv-125/and-gate-2-i-p-sc-74a-5/dp/2438540" H 10050 850 31  0001 C CNN "Distributor Link"
F 6 "https://cz.mouser.com/ProductDetail/Nexperia/74AHC1G08GV125" H 10050 800 31  0001 C CNN "Distributor Link 2"
	1    10050 1550
	1    0    0    -1  
$EndComp
Text GLabel 9700 1650 0    50   Input ~ 0
ISO_NOT_OVR_CUR
Wire Wire Line
	9700 1450 9750 1450
Wire Wire Line
	9700 1650 9750 1650
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 5FAE21B9
P 10050 2725
AR Path="/5F05E355/5FAE21B9" Ref="R?"  Part="1" 
AR Path="/6068445B/5FAE21B9" Ref="R?"  Part="1" 
AR Path="/60E816A3/5FAE21B9" Ref="R?"  Part="1" 
AR Path="/60EB5E32/5FAE21B9" Ref="R?"  Part="1" 
AR Path="/5FD7CF82/5FAE21B9" Ref="R?"  Part="1" 
AR Path="/5EA5E295/5FAE21B9" Ref="R?"  Part="1" 
AR Path="/5EA5E06A/5FAE21B9" Ref="R146"  Part="1" 
F 0 "R146" H 9900 2725 50  0000 C CNN
F 1 "10k" H 9950 2800 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 10050 2725 50  0001 C CNN
F 3 "~" H 10050 2725 50  0001 C CNN
F 4 "WF06R1002BTL -  SMD Chip Resistor, 0603 [1608 Metric], 10 kohm, WF06R Series, 75 V, Thin Film, 100 mW" H 10050 2325 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/walsin/wf06r1002btl/res-10k-0-1-75v-0603-thin-film/dp/2502926" H 10050 2225 50  0001 C CNN "Distributor Link"
	1    10050 2725
	-1   0    0    1   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_5V0 #PWR?
U 1 1 5FAE21BF
P 10050 2575
AR Path="/5EFE605D/5FAE21BF" Ref="#PWR?"  Part="1" 
AR Path="/5EFE7331/5FAE21BF" Ref="#PWR?"  Part="1" 
AR Path="/5FD7CF82/5FAE21BF" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E295/5FAE21BF" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E06A/5FAE21BF" Ref="#PWR0227"  Part="1" 
F 0 "#PWR0227" H 10050 2825 50  0001 C CNN
F 1 "MOT_5V0" H 10051 2748 50  0000 C CNN
F 2 "" H 10050 2575 50  0001 C CNN
F 3 "" H 10050 2575 50  0001 C CNN
	1    10050 2575
	1    0    0    -1  
$EndComp
Wire Wire Line
	10050 2575 10050 2625
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 5FAE21D7
P 9600 3125
AR Path="/5F05E355/5FAE21D7" Ref="R?"  Part="1" 
AR Path="/5E9C6910/5FAE21D7" Ref="R?"  Part="1" 
AR Path="/5EFE605D/5FAE21D7" Ref="R?"  Part="1" 
AR Path="/5FD7CF82/5FAE21D7" Ref="R?"  Part="1" 
AR Path="/5EA5E295/5FAE21D7" Ref="R?"  Part="1" 
AR Path="/5EA5E06A/5FAE21D7" Ref="R145"  Part="1" 
F 0 "R145" V 9675 3125 50  0000 C CNN
F 1 "22" V 9500 3125 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 9600 3125 50  0001 C CNN
F 3 "~" H 9600 3125 50  0001 C CNN
F 4 "MCWR04X22R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 22 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 9600 2725 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x22r0ftl/res-22r-1-0-0625w-0402-thick-film/dp/2447138" H 9600 2625 50  0001 C CNN "Distributor Link"
	1    9600 3125
	0    -1   -1   0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:Q_NMOS_GSD Q?
U 1 1 5FAE21A4
P 9950 3125
AR Path="/5EA5E295/5FAE21A4" Ref="Q?"  Part="1" 
AR Path="/5EA5E06A/5FAE21A4" Ref="Q11"  Part="1" 
F 0 "Q11" H 10155 3171 50  0000 L CNN
F 1 "BSS138TA" H 10155 3080 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:SOT-23" H 10150 3225 50  0001 C CNN
F 3 "~" H 9950 3125 50  0001 C CNN
F 4 "BSS138TA -  Power MOSFET, N Channel, 50 V, 200 mA, 3.5 ohm, SOT-23, Surface Mount" H 9950 2625 31  0001 C CNN "Description"
F 5 "https://uk.farnell.com/diodes-inc/bss138ta/mosfet-n-50v-sot-23/dp/1471176" H 9950 2525 31  0001 C CNN "Distributor Link"
	1    9950 3125
	1    0    0    -1  
$EndComp
Wire Wire Line
	10050 3375 10050 3325
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR?
U 1 1 5FB58DEB
P 10050 3375
AR Path="/5EA41912/5FB58DEB" Ref="#PWR?"  Part="1" 
AR Path="/5FD7CF82/5FB58DEB" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E295/5FB58DEB" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E06A/5FB58DEB" Ref="#PWR0228"  Part="1" 
F 0 "#PWR0228" H 10050 3125 50  0001 C CNN
F 1 "MOT_DGND" H 10055 3202 50  0000 C CNN
F 2 "" H 10050 3375 50  0001 C CNN
F 3 "" H 10050 3375 50  0001 C CNN
	1    10050 3375
	1    0    0    -1  
$EndComp
Wire Wire Line
	9700 3125 9750 3125
Wire Wire Line
	10050 2825 10050 2875
Connection ~ 10050 2875
Wire Wire Line
	10050 2875 10050 2925
Wire Wire Line
	9300 3125 9500 3125
Wire Wire Line
	10400 1550 10600 1550
Wire Wire Line
	10600 1550 10600 2250
Wire Wire Line
	9300 2250 10600 2250
Wire Notes Line
	8900 600  11100 600 
Text Notes 8975 800  0    100  ~ 0
Disable Motor
Wire Notes Line
	8900 3700 11100 3700
Wire Notes Line
	8900 600  8900 3700
Wire Notes Line
	11100 600  11100 3700
Wire Wire Line
	9300 2250 9300 3125
Text Notes 9000 900  0    25   ~ 0
Disable motor when ISO_MOT_EN is low\nor when overcurrent fault detected.
Wire Wire Line
	2900 5825 2900 5925
Wire Wire Line
	2900 3500 2900 3600
Wire Wire Line
	2900 1175 2900 1275
Wire Wire Line
	3500 1450 3500 1700
Wire Wire Line
	3500 3775 3500 4025
Wire Wire Line
	3500 6100 3500 6350
$Comp
L PMSM_Driver_LV_Board_Device:Q_NMOS_GDS Q7
U 1 1 5EB856E2
P 6650 1375
F 0 "Q7" H 6855 1401 50  0000 L CNN
F 1 "IRFS3806TRLPBF" H 6855 1329 25  0000 L CNN
F 2 "PMSM_Driver_LV_Board:TO-263-2" H 6850 1475 50  0001 C CNN
F 3 "~" H 6650 1375 50  0001 C CNN
F 4 "IRFS3806TRLPBF -  Power MOSFET, N Channel, 60 V, 43 A, 0.0126 ohm, TO-263 (D2PAK), Surface Mount" H 6650 875 31  0001 C CNN "Description"
F 5 "https://uk.farnell.com/infineon/irfs3806trlpbf/mosfet-n-ch-60v-43a-to-263-3/dp/2803423" H 6650 775 31  0001 C CNN "Distributor Link"
	1    6650 1375
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:Q_NMOS_GDS Q8
U 1 1 5EBDDE17
P 6650 2125
F 0 "Q8" H 6855 2151 50  0000 L CNN
F 1 "IRFS3806TRLPBF" H 6855 2079 25  0000 L CNN
F 2 "PMSM_Driver_LV_Board:TO-263-2" H 6850 2225 50  0001 C CNN
F 3 "~" H 6650 2125 50  0001 C CNN
F 4 "IRFS3806TRLPBF -  Power MOSFET, N Channel, 60 V, 43 A, 0.0126 ohm, TO-263 (D2PAK), Surface Mount" H 6650 1625 31  0001 C CNN "Description"
F 5 "https://uk.farnell.com/infineon/irfs3806trlpbf/mosfet-n-ch-60v-43a-to-263-3/dp/2803423" H 6650 1525 31  0001 C CNN "Distributor Link"
	1    6650 2125
	1    0    0    -1  
$EndComp
Wire Wire Line
	6750 950  7475 950 
Connection ~ 7475 950 
Wire Wire Line
	6750 1675 7550 1675
Connection ~ 6750 1725
Wire Wire Line
	6750 1725 6750 1925
Wire Wire Line
	6400 1625 6400 1725
$Comp
L PMSM_Driver_LV_Board_Device:Q_NMOS_GDS Q9
U 1 1 5EC79D9A
P 6650 3700
F 0 "Q9" H 6855 3726 50  0000 L CNN
F 1 "IRFS3806TRLPBF" H 6855 3654 25  0000 L CNN
F 2 "PMSM_Driver_LV_Board:TO-263-2" H 6850 3800 50  0001 C CNN
F 3 "~" H 6650 3700 50  0001 C CNN
F 4 "IRFS3806TRLPBF -  Power MOSFET, N Channel, 60 V, 43 A, 0.0126 ohm, TO-263 (D2PAK), Surface Mount" H 6650 3200 31  0001 C CNN "Description"
F 5 "https://uk.farnell.com/infineon/irfs3806trlpbf/mosfet-n-ch-60v-43a-to-263-3/dp/2803423" H 6650 3100 31  0001 C CNN "Distributor Link"
	1    6650 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	6750 3275 7475 3275
Connection ~ 7475 3275
$Comp
L PMSM_Driver_LV_Board_Device:Q_NMOS_GDS Q10
U 1 1 5ECE189D
P 6650 4450
F 0 "Q10" H 6855 4476 50  0000 L CNN
F 1 "IRFS3806TRLPBF" H 6855 4404 25  0000 L CNN
F 2 "PMSM_Driver_LV_Board:TO-263-2" H 6850 4550 50  0001 C CNN
F 3 "~" H 6650 4450 50  0001 C CNN
F 4 "IRFS3806TRLPBF -  Power MOSFET, N Channel, 60 V, 43 A, 0.0126 ohm, TO-263 (D2PAK), Surface Mount" H 6650 3950 31  0001 C CNN "Description"
F 5 "https://uk.farnell.com/infineon/irfs3806trlpbf/mosfet-n-ch-60v-43a-to-263-3/dp/2803423" H 6650 3850 31  0001 C CNN "Distributor Link"
	1    6650 4450
	1    0    0    -1  
$EndComp
Wire Wire Line
	6750 4000 7550 4000
Connection ~ 6750 4050
Wire Wire Line
	6750 4050 6750 4250
Wire Wire Line
	6400 3950 6400 4050
$Comp
L PMSM_Driver_LV_Board_Device:Q_NMOS_GDS Q5
U 1 1 5ED36C12
P 5950 6025
F 0 "Q5" H 6155 6051 50  0000 L CNN
F 1 "IRFS3806TRLPBF" H 6155 5979 25  0000 L CNN
F 2 "PMSM_Driver_LV_Board:TO-263-2" H 6150 6125 50  0001 C CNN
F 3 "~" H 5950 6025 50  0001 C CNN
F 4 "IRFS3806TRLPBF -  Power MOSFET, N Channel, 60 V, 43 A, 0.0126 ohm, TO-263 (D2PAK), Surface Mount" H 5950 5525 31  0001 C CNN "Description"
F 5 "https://uk.farnell.com/infineon/irfs3806trlpbf/mosfet-n-ch-60v-43a-to-263-3/dp/2803423" H 5950 5425 31  0001 C CNN "Distributor Link"
	1    5950 6025
	1    0    0    -1  
$EndComp
Wire Wire Line
	6050 5600 6775 5600
Connection ~ 6775 5600
$Comp
L PMSM_Driver_LV_Board_Device:Q_NMOS_GDS Q6
U 1 1 5ED7B2EA
P 5950 6775
F 0 "Q6" H 6155 6801 50  0000 L CNN
F 1 "IRFS3806TRLPBF" H 6155 6729 25  0000 L CNN
F 2 "PMSM_Driver_LV_Board:TO-263-2" H 6150 6875 50  0001 C CNN
F 3 "~" H 5950 6775 50  0001 C CNN
F 4 "IRFS3806TRLPBF -  Power MOSFET, N Channel, 60 V, 43 A, 0.0126 ohm, TO-263 (D2PAK), Surface Mount" H 5950 6275 31  0001 C CNN "Description"
F 5 "https://uk.farnell.com/infineon/irfs3806trlpbf/mosfet-n-ch-60v-43a-to-263-3/dp/2803423" H 5950 6175 31  0001 C CNN "Distributor Link"
	1    5950 6775
	1    0    0    -1  
$EndComp
Wire Wire Line
	6050 6325 6050 6375
Connection ~ 6050 6375
Wire Wire Line
	6050 6375 6050 6425
Wire Wire Line
	5700 6275 5700 6375
Wire Wire Line
	6050 6325 6250 6325
Wire Wire Line
	6050 6425 6250 6425
Connection ~ 6050 6425
Wire Wire Line
	6050 6425 6050 6575
$Comp
L PMSM_Driver_LV_Board_Device:CP1_Small C?
U 1 1 60AA4261
P 7475 3425
AR Path="/5EA5D9E8/60AA4261" Ref="C?"  Part="1" 
AR Path="/5EA5E06A/60AA4261" Ref="C80"  Part="1" 
F 0 "C80" H 7566 3516 50  0000 L CNN
F 1 "470uF" H 7566 3425 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:CP_Radial_D12.5mm_P5.00mm" H 7475 3425 50  0001 C CNN
F 3 "~" H 7475 3425 50  0001 C CNN
F 4 "UVZ1J471MHD1TO - Aluminium Electrolytic Capacitor, 470uF, 63V, 105C" H 7475 3025 50  0001 C CNN "Description"
F 5 "https://www.mouser.co.uk/ProductDetail/Nichicon/UVZ1J471MHD1TO?qs=sGAEpiMZZMvwFf0viD3Y3TLPyGlPD9krZv1vbUyowls%3D" H 7475 2925 50  0001 C CNN "Distributor Link"
F 6 "63V" H 7566 3334 50  0000 L CNN "Voltage"
	1    7475 3425
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:CP1_Small C?
U 1 1 60AC623F
P 7475 1100
AR Path="/5EA5D9E8/60AC623F" Ref="C?"  Part="1" 
AR Path="/5EA5E06A/60AC623F" Ref="C79"  Part="1" 
F 0 "C79" H 7566 1191 50  0000 L CNN
F 1 "470uF" H 7566 1100 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:CP_Radial_D12.5mm_P5.00mm" H 7475 1100 50  0001 C CNN
F 3 "~" H 7475 1100 50  0001 C CNN
F 4 "UVZ1J471MHD1TO - Aluminium Electrolytic Capacitor, 470uF, 63V, 105C" H 7475 700 50  0001 C CNN "Description"
F 5 "https://www.mouser.co.uk/ProductDetail/Nichicon/UVZ1J471MHD1TO?qs=sGAEpiMZZMvwFf0viD3Y3TLPyGlPD9krZv1vbUyowls%3D" H 7475 600 50  0001 C CNN "Distributor Link"
F 6 "63V" H 7566 1009 50  0000 L CNN "Voltage"
	1    7475 1100
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:CP1_Small C?
U 1 1 60AE822D
P 6775 5750
AR Path="/5EA5D9E8/60AE822D" Ref="C?"  Part="1" 
AR Path="/5EA5E06A/60AE822D" Ref="C77"  Part="1" 
F 0 "C77" H 6866 5841 50  0000 L CNN
F 1 "470uF" H 6866 5750 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:CP_Radial_D12.5mm_P5.00mm" H 6775 5750 50  0001 C CNN
F 3 "~" H 6775 5750 50  0001 C CNN
F 4 "UVZ1J471MHD1TO - Aluminium Electrolytic Capacitor, 470uF, 63V, 105C" H 6775 5350 50  0001 C CNN "Description"
F 5 "https://www.mouser.co.uk/ProductDetail/Nichicon/UVZ1J471MHD1TO?qs=sGAEpiMZZMvwFf0viD3Y3TLPyGlPD9krZv1vbUyowls%3D" H 6775 5250 50  0001 C CNN "Distributor Link"
F 6 "63V" H 6866 5659 50  0000 L CNN "Voltage"
	1    6775 5750
	1    0    0    -1  
$EndComp
Text Notes 5550 1625 0    25   ~ 0
Pull-down 10k resistor to keep Vgs zero\nwhen gate input is open-circuited.
Text Notes 4950 1300 0    25   ~ 0
Turn-on gate resistor: 22 Ohm.\nTurn-off gate resistors:\n3.3 Ohm parallel with 22 Ohm, i.e. 2.9 Ohm.
$EndSCHEMATC
