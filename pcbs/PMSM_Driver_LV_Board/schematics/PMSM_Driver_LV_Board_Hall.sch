EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 6 16
Title "PMSM_Driver_LV_Board"
Date "2020-03-30"
Rev "A"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text GLabel 2575 3750 2    50   Output ~ 0
ISO_HAL_A
Text GLabel 2575 5150 2    50   Output ~ 0
ISO_HAL_B
Text GLabel 2600 6550 2    50   Output ~ 0
ISO_HAL_C
Text GLabel 7250 2275 2    50   Output ~ 0
ISO_DIF_HAL_C_N
Text GLabel 7250 1975 2    50   Output ~ 0
ISO_DIF_HAL_C_P
Text GLabel 1550 3850 0    50   Input ~ 0
ISO_SE_HAL_A
Text GLabel 1550 5250 0    50   Input ~ 0
ISO_SE_HAL_B
Text GLabel 1575 6650 0    50   Input ~ 0
ISO_SE_HAL_C
Wire Wire Line
	2450 3400 2550 3400
Wire Wire Line
	2850 3400 2850 3450
Wire Wire Line
	2750 3400 2850 3400
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 60E94BC4
P 2650 3400
AR Path="/5E9C6910/60E94BC4" Ref="C?"  Part="1" 
AR Path="/5EBC6654/60E94BC4" Ref="C?"  Part="1" 
AR Path="/5EBC681B/60E94BC4" Ref="C?"  Part="1" 
AR Path="/5F05E355/60E94BC4" Ref="C?"  Part="1" 
AR Path="/6068445B/60E94BC4" Ref="C?"  Part="1" 
AR Path="/60E816A3/60E94BC4" Ref="C?"  Part="1" 
AR Path="/5EA5DF8B/60E94BC4" Ref="C48"  Part="1" 
F 0 "C48" V 2775 3350 50  0000 L CNN
F 1 "100nF" V 2700 3125 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 2650 3400 50  0001 C CNN
F 3 "~" H 2650 3400 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 2650 3000 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 2650 2900 50  0001 C CNN "Distributor Link"
	1    2650 3400
	0    -1   -1   0   
$EndComp
Connection ~ 2450 3400
Wire Wire Line
	2450 3400 2450 3650
Wire Wire Line
	2450 3300 2450 3400
$Comp
L PMSM_Driver_LV_Board_Power:ISO_DGND #PWR?
U 1 1 60E94BCD
P 2850 3450
AR Path="/5F05E355/60E94BCD" Ref="#PWR?"  Part="1" 
AR Path="/6068445B/60E94BCD" Ref="#PWR?"  Part="1" 
AR Path="/60E816A3/60E94BCD" Ref="#PWR?"  Part="1" 
AR Path="/5EA5DF8B/60E94BCD" Ref="#PWR0151"  Part="1" 
F 0 "#PWR0151" H 2850 3200 50  0001 C CNN
F 1 "ISO_DGND" H 2855 3277 50  0000 C CNN
F 2 "" H 2850 3450 50  0001 C CNN
F 3 "" H 2850 3450 50  0001 C CNN
	1    2850 3450
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:ISO_SENS_5V0 #PWR?
U 1 1 60E94BD3
P 2450 3300
AR Path="/6068445B/60E94BD3" Ref="#PWR?"  Part="1" 
AR Path="/60E816A3/60E94BD3" Ref="#PWR?"  Part="1" 
AR Path="/5EA5DF8B/60E94BD3" Ref="#PWR0145"  Part="1" 
F 0 "#PWR0145" H 2450 3550 50  0001 C CNN
F 1 "ISO_SENS_5V0" H 2451 3473 50  0000 C CNN
F 2 "" H 2450 3300 50  0001 C CNN
F 3 "" H 2450 3300 50  0001 C CNN
	1    2450 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	2350 3650 2450 3650
$Comp
L PMSM_Driver_LV_Board_Power:ISO_DGND #PWR?
U 1 1 60E94BDA
P 2450 4050
AR Path="/5F05E355/60E94BDA" Ref="#PWR?"  Part="1" 
AR Path="/6068445B/60E94BDA" Ref="#PWR?"  Part="1" 
AR Path="/60E816A3/60E94BDA" Ref="#PWR?"  Part="1" 
AR Path="/5EA5DF8B/60E94BDA" Ref="#PWR0146"  Part="1" 
F 0 "#PWR0146" H 2450 3800 50  0001 C CNN
F 1 "ISO_DGND" H 2455 3877 50  0000 C CNN
F 2 "" H 2450 4050 50  0001 C CNN
F 3 "" H 2450 4050 50  0001 C CNN
	1    2450 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	2350 3850 2450 3850
Wire Wire Line
	2350 3750 2575 3750
$Comp
L PMSM_Driver_LV_Board_IC:ADG849YKSZ U?
U 1 1 60E94BE5
P 2000 3750
AR Path="/6068445B/60E94BE5" Ref="U?"  Part="1" 
AR Path="/60E816A3/60E94BE5" Ref="U?"  Part="1" 
AR Path="/5EA5DF8B/60E94BE5" Ref="U19"  Part="1" 
F 0 "U19" H 1825 4000 50  0000 C CNN
F 1 "ADG849YKSZ" H 2000 3500 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:SC70-6" H 2150 2750 31  0001 C CNN
F 3 "~" H 2150 2750 31  0001 C CNN
F 4 "ADG849YKSZ-REEL7 -  Analogue Switch, 1 Channels, SPDT, 1.1 ohm, 1.8V to 5.5V, SC-70, 6 Pins" H 2000 3400 31  0001 C CNN "Description"
F 5 "https://uk.farnell.com/analog-devices/adg849yksz-reel7/analogue-switch-1ch-spdt-sc-70/dp/2727330" H 2000 3350 31  0001 C CNN "Distributor Link"
F 6 "https://www.mouser.co.uk/ProductDetail/Analog-Devices/ADG849YKSZ-REEL?qs=sGAEpiMZZMv9Q1JI0Mo%2Ftchdbo4TVfN7" H 2000 3300 31  0001 C CNN "Distributor Link 2"
	1    2000 3750
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Connector:Jumper_NC_Small JP?
U 1 1 60E94BED
P 1525 2050
AR Path="/6068445B/60E94BED" Ref="JP?"  Part="1" 
AR Path="/60E816A3/60E94BED" Ref="JP?"  Part="1" 
AR Path="/5EA5DF8B/60E94BED" Ref="JP2"  Part="1" 
F 0 "JP2" V 1550 2025 50  0000 R CNN
F 1 "JUMPER" V 1475 2025 50  0000 R CNN
F 2 "PMSM_Driver_LV_Board:PinHeader_1x02_P2.54mm_Vertical" H 1525 1900 50  0001 C CNN
F 3 "~" H 1525 2050 50  0001 C CNN
F 4 "2211S-02G -  Board-To-Board Connector, 2.54 mm, 2 Contacts, Header, 2211S Series, Through Hole, 1 Rows" H 1525 1800 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/2211s-02g/header-1-row-vert-2way/dp/1593411" H 1525 1700 50  0001 C CNN "Distributor Link"
	1    1525 2050
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1650 3650 1550 3650
$Comp
L PMSM_Driver_LV_Board_Power:ISO_DGND #PWR?
U 1 1 60E94BF4
P 1525 2250
AR Path="/5F05E355/60E94BF4" Ref="#PWR?"  Part="1" 
AR Path="/6068445B/60E94BF4" Ref="#PWR?"  Part="1" 
AR Path="/60E816A3/60E94BF4" Ref="#PWR?"  Part="1" 
AR Path="/5EA5DF8B/60E94BF4" Ref="#PWR0143"  Part="1" 
F 0 "#PWR0143" H 1525 2000 50  0001 C CNN
F 1 "ISO_DGND" H 1530 2077 50  0000 C CNN
F 2 "" H 1525 2250 50  0001 C CNN
F 3 "" H 1525 2250 50  0001 C CNN
	1    1525 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	1525 2150 1525 2250
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 60E94BFD
P 1525 1650
AR Path="/5F05E355/60E94BFD" Ref="R?"  Part="1" 
AR Path="/6068445B/60E94BFD" Ref="R?"  Part="1" 
AR Path="/60E816A3/60E94BFD" Ref="R?"  Part="1" 
AR Path="/5EA5DF8B/60E94BFD" Ref="R93"  Part="1" 
F 0 "R93" H 1650 1650 50  0000 C CNN
F 1 "10k" H 1625 1575 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 1525 1650 50  0001 C CNN
F 3 "~" H 1525 1650 50  0001 C CNN
F 4 "MCWR04X1002FTL -  SMD Chip Resistor, 0402 [1005 Metric], 10 kohm, MCWR Series, 50 V, Thick Film, 62.5 mW " H 1525 1250 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x1002ftl/res-10k-1-0-0625w-thick-film/dp/2447096" H 1525 1150 50  0001 C CNN "Distributor Link"
	1    1525 1650
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:ISO_SENS_5V0 #PWR?
U 1 1 60E94C03
P 1525 1450
AR Path="/6068445B/60E94C03" Ref="#PWR?"  Part="1" 
AR Path="/60E816A3/60E94C03" Ref="#PWR?"  Part="1" 
AR Path="/5EA5DF8B/60E94C03" Ref="#PWR0142"  Part="1" 
F 0 "#PWR0142" H 1525 1700 50  0001 C CNN
F 1 "ISO_SENS_5V0" H 1526 1623 50  0000 C CNN
F 2 "" H 1525 1450 50  0001 C CNN
F 3 "" H 1525 1450 50  0001 C CNN
	1    1525 1450
	1    0    0    -1  
$EndComp
Wire Wire Line
	1525 1550 1525 1450
Text GLabel 2075 1850 2    50   Output ~ 0
ISO_SELECT_SE_HAL
Text GLabel 1550 3750 0    50   Input ~ 0
ISO_DIF_HAL_A
Wire Wire Line
	1525 1750 1525 1850
Wire Wire Line
	1550 3750 1650 3750
Wire Wire Line
	1550 3850 1650 3850
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 60E94C11
P 1975 2050
AR Path="/5E9C6910/60E94C11" Ref="C?"  Part="1" 
AR Path="/5EBC6654/60E94C11" Ref="C?"  Part="1" 
AR Path="/5EBC681B/60E94C11" Ref="C?"  Part="1" 
AR Path="/5F05E355/60E94C11" Ref="C?"  Part="1" 
AR Path="/6068445B/60E94C11" Ref="C?"  Part="1" 
AR Path="/60E816A3/60E94C11" Ref="C?"  Part="1" 
AR Path="/5EA5DF8B/60E94C11" Ref="C47"  Part="1" 
F 0 "C47" H 2075 2050 50  0000 L CNN
F 1 "1uF" H 2050 1975 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 1975 2050 50  0001 C CNN
F 3 "~" H 1975 2050 50  0001 C CNN
F 4 "LMK107B7105KA-T -  SMD Multilayer Ceramic Capacitor, 1 µF, 10 V, 0603 [1608 Metric], ± 10%, X7R, M Series" H 1975 1650 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/taiyo-yuden/lmk107b7105ka-t/cap-1-f-10v-10-x7r-0603/dp/2112849" H 1975 1550 50  0001 C CNN "Distributor Link"
	1    1975 2050
	1    0    0    -1  
$EndComp
Connection ~ 1525 1850
Wire Wire Line
	1525 1850 1525 1950
Wire Wire Line
	1525 1850 1975 1850
Wire Wire Line
	1975 1950 1975 1850
$Comp
L PMSM_Driver_LV_Board_Power:ISO_DGND #PWR?
U 1 1 60E94C1B
P 1975 2250
AR Path="/5F05E355/60E94C1B" Ref="#PWR?"  Part="1" 
AR Path="/6068445B/60E94C1B" Ref="#PWR?"  Part="1" 
AR Path="/60E816A3/60E94C1B" Ref="#PWR?"  Part="1" 
AR Path="/5EA5DF8B/60E94C1B" Ref="#PWR0144"  Part="1" 
F 0 "#PWR0144" H 1975 2000 50  0001 C CNN
F 1 "ISO_DGND" H 1980 2077 50  0000 C CNN
F 2 "" H 1975 2250 50  0001 C CNN
F 3 "" H 1975 2250 50  0001 C CNN
	1    1975 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	1975 2250 1975 2150
Wire Wire Line
	1975 1850 2075 1850
Connection ~ 1975 1850
Wire Wire Line
	2450 4050 2450 3850
Wire Wire Line
	2450 4800 2550 4800
Wire Wire Line
	2850 4800 2850 4850
Wire Wire Line
	2750 4800 2850 4800
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 60E94C2A
P 2650 4800
AR Path="/5E9C6910/60E94C2A" Ref="C?"  Part="1" 
AR Path="/5EBC6654/60E94C2A" Ref="C?"  Part="1" 
AR Path="/5EBC681B/60E94C2A" Ref="C?"  Part="1" 
AR Path="/5F05E355/60E94C2A" Ref="C?"  Part="1" 
AR Path="/6068445B/60E94C2A" Ref="C?"  Part="1" 
AR Path="/60E816A3/60E94C2A" Ref="C?"  Part="1" 
AR Path="/5EA5DF8B/60E94C2A" Ref="C49"  Part="1" 
F 0 "C49" V 2775 4750 50  0000 L CNN
F 1 "100nF" V 2700 4525 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 2650 4800 50  0001 C CNN
F 3 "~" H 2650 4800 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 2650 4400 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 2650 4300 50  0001 C CNN "Distributor Link"
	1    2650 4800
	0    -1   -1   0   
$EndComp
Connection ~ 2450 4800
Wire Wire Line
	2450 4800 2450 5050
Wire Wire Line
	2450 4700 2450 4800
$Comp
L PMSM_Driver_LV_Board_Power:ISO_DGND #PWR?
U 1 1 60E94C33
P 2850 4850
AR Path="/5F05E355/60E94C33" Ref="#PWR?"  Part="1" 
AR Path="/6068445B/60E94C33" Ref="#PWR?"  Part="1" 
AR Path="/60E816A3/60E94C33" Ref="#PWR?"  Part="1" 
AR Path="/5EA5DF8B/60E94C33" Ref="#PWR0152"  Part="1" 
F 0 "#PWR0152" H 2850 4600 50  0001 C CNN
F 1 "ISO_DGND" H 2855 4677 50  0000 C CNN
F 2 "" H 2850 4850 50  0001 C CNN
F 3 "" H 2850 4850 50  0001 C CNN
	1    2850 4850
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:ISO_SENS_5V0 #PWR?
U 1 1 60E94C39
P 2450 4700
AR Path="/6068445B/60E94C39" Ref="#PWR?"  Part="1" 
AR Path="/60E816A3/60E94C39" Ref="#PWR?"  Part="1" 
AR Path="/5EA5DF8B/60E94C39" Ref="#PWR0147"  Part="1" 
F 0 "#PWR0147" H 2450 4950 50  0001 C CNN
F 1 "ISO_SENS_5V0" H 2451 4873 50  0000 C CNN
F 2 "" H 2450 4700 50  0001 C CNN
F 3 "" H 2450 4700 50  0001 C CNN
	1    2450 4700
	1    0    0    -1  
$EndComp
Wire Wire Line
	2350 5050 2450 5050
$Comp
L PMSM_Driver_LV_Board_Power:ISO_DGND #PWR?
U 1 1 60E94C40
P 2450 5450
AR Path="/5F05E355/60E94C40" Ref="#PWR?"  Part="1" 
AR Path="/6068445B/60E94C40" Ref="#PWR?"  Part="1" 
AR Path="/60E816A3/60E94C40" Ref="#PWR?"  Part="1" 
AR Path="/5EA5DF8B/60E94C40" Ref="#PWR0148"  Part="1" 
F 0 "#PWR0148" H 2450 5200 50  0001 C CNN
F 1 "ISO_DGND" H 2455 5277 50  0000 C CNN
F 2 "" H 2450 5450 50  0001 C CNN
F 3 "" H 2450 5450 50  0001 C CNN
	1    2450 5450
	1    0    0    -1  
$EndComp
Wire Wire Line
	2350 5250 2450 5250
Wire Wire Line
	2350 5150 2575 5150
$Comp
L PMSM_Driver_LV_Board_IC:ADG849YKSZ U?
U 1 1 60E94C4B
P 2000 5150
AR Path="/6068445B/60E94C4B" Ref="U?"  Part="1" 
AR Path="/60E816A3/60E94C4B" Ref="U?"  Part="1" 
AR Path="/5EA5DF8B/60E94C4B" Ref="U20"  Part="1" 
F 0 "U20" H 1825 5400 50  0000 C CNN
F 1 "ADG849YKSZ" H 2000 4900 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:SC70-6" H 2150 4150 31  0001 C CNN
F 3 "~" H 2150 4150 31  0001 C CNN
F 4 "ADG849YKSZ-REEL7 -  Analogue Switch, 1 Channels, SPDT, 1.1 ohm, 1.8V to 5.5V, SC-70, 6 Pins" H 2000 4800 31  0001 C CNN "Description"
F 5 "https://uk.farnell.com/analog-devices/adg849yksz-reel7/analogue-switch-1ch-spdt-sc-70/dp/2727330" H 2000 4750 31  0001 C CNN "Distributor Link"
F 6 "https://www.mouser.co.uk/ProductDetail/Analog-Devices/ADG849YKSZ-REEL?qs=sGAEpiMZZMv9Q1JI0Mo%2Ftchdbo4TVfN7" H 2000 4700 31  0001 C CNN "Distributor Link 2"
	1    2000 5150
	1    0    0    -1  
$EndComp
Wire Wire Line
	1650 5050 1550 5050
Wire Wire Line
	1550 5150 1650 5150
Wire Wire Line
	1550 5250 1650 5250
Wire Wire Line
	2450 5450 2450 5250
Wire Wire Line
	2475 6200 2575 6200
Wire Wire Line
	2875 6200 2875 6250
Wire Wire Line
	2775 6200 2875 6200
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 60E94C5A
P 2675 6200
AR Path="/5E9C6910/60E94C5A" Ref="C?"  Part="1" 
AR Path="/5EBC6654/60E94C5A" Ref="C?"  Part="1" 
AR Path="/5EBC681B/60E94C5A" Ref="C?"  Part="1" 
AR Path="/5F05E355/60E94C5A" Ref="C?"  Part="1" 
AR Path="/6068445B/60E94C5A" Ref="C?"  Part="1" 
AR Path="/60E816A3/60E94C5A" Ref="C?"  Part="1" 
AR Path="/5EA5DF8B/60E94C5A" Ref="C50"  Part="1" 
F 0 "C50" V 2800 6150 50  0000 L CNN
F 1 "100nF" V 2725 5925 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 2675 6200 50  0001 C CNN
F 3 "~" H 2675 6200 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 2675 5800 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 2675 5700 50  0001 C CNN "Distributor Link"
	1    2675 6200
	0    -1   -1   0   
$EndComp
Connection ~ 2475 6200
Wire Wire Line
	2475 6200 2475 6450
Wire Wire Line
	2475 6100 2475 6200
$Comp
L PMSM_Driver_LV_Board_Power:ISO_DGND #PWR?
U 1 1 60E94C63
P 2875 6250
AR Path="/5F05E355/60E94C63" Ref="#PWR?"  Part="1" 
AR Path="/6068445B/60E94C63" Ref="#PWR?"  Part="1" 
AR Path="/60E816A3/60E94C63" Ref="#PWR?"  Part="1" 
AR Path="/5EA5DF8B/60E94C63" Ref="#PWR0153"  Part="1" 
F 0 "#PWR0153" H 2875 6000 50  0001 C CNN
F 1 "ISO_DGND" H 2880 6077 50  0000 C CNN
F 2 "" H 2875 6250 50  0001 C CNN
F 3 "" H 2875 6250 50  0001 C CNN
	1    2875 6250
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:ISO_SENS_5V0 #PWR?
U 1 1 60E94C69
P 2475 6100
AR Path="/6068445B/60E94C69" Ref="#PWR?"  Part="1" 
AR Path="/60E816A3/60E94C69" Ref="#PWR?"  Part="1" 
AR Path="/5EA5DF8B/60E94C69" Ref="#PWR0149"  Part="1" 
F 0 "#PWR0149" H 2475 6350 50  0001 C CNN
F 1 "ISO_SENS_5V0" H 2476 6273 50  0000 C CNN
F 2 "" H 2475 6100 50  0001 C CNN
F 3 "" H 2475 6100 50  0001 C CNN
	1    2475 6100
	1    0    0    -1  
$EndComp
Wire Wire Line
	2375 6450 2475 6450
$Comp
L PMSM_Driver_LV_Board_Power:ISO_DGND #PWR?
U 1 1 60E94C70
P 2475 6850
AR Path="/5F05E355/60E94C70" Ref="#PWR?"  Part="1" 
AR Path="/6068445B/60E94C70" Ref="#PWR?"  Part="1" 
AR Path="/60E816A3/60E94C70" Ref="#PWR?"  Part="1" 
AR Path="/5EA5DF8B/60E94C70" Ref="#PWR0150"  Part="1" 
F 0 "#PWR0150" H 2475 6600 50  0001 C CNN
F 1 "ISO_DGND" H 2480 6677 50  0000 C CNN
F 2 "" H 2475 6850 50  0001 C CNN
F 3 "" H 2475 6850 50  0001 C CNN
	1    2475 6850
	1    0    0    -1  
$EndComp
Wire Wire Line
	2375 6650 2475 6650
Wire Wire Line
	2375 6550 2600 6550
$Comp
L PMSM_Driver_LV_Board_IC:ADG849YKSZ U?
U 1 1 60E94C7B
P 2025 6550
AR Path="/6068445B/60E94C7B" Ref="U?"  Part="1" 
AR Path="/60E816A3/60E94C7B" Ref="U?"  Part="1" 
AR Path="/5EA5DF8B/60E94C7B" Ref="U21"  Part="1" 
F 0 "U21" H 1850 6800 50  0000 C CNN
F 1 "ADG849YKSZ" H 2025 6300 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:SC70-6" H 2175 5550 31  0001 C CNN
F 3 "~" H 2175 5550 31  0001 C CNN
F 4 "ADG849YKSZ-REEL7 -  Analogue Switch, 1 Channels, SPDT, 1.1 ohm, 1.8V to 5.5V, SC-70, 6 Pins" H 2025 6200 31  0001 C CNN "Description"
F 5 "https://uk.farnell.com/analog-devices/adg849yksz-reel7/analogue-switch-1ch-spdt-sc-70/dp/2727330" H 2025 6150 31  0001 C CNN "Distributor Link"
F 6 "https://www.mouser.co.uk/ProductDetail/Analog-Devices/ADG849YKSZ-REEL?qs=sGAEpiMZZMv9Q1JI0Mo%2Ftchdbo4TVfN7" H 2025 6100 31  0001 C CNN "Distributor Link 2"
	1    2025 6550
	1    0    0    -1  
$EndComp
Wire Wire Line
	1675 6450 1575 6450
Wire Wire Line
	1575 6550 1675 6550
Wire Wire Line
	1575 6650 1675 6650
Wire Wire Line
	2475 6850 2475 6650
Text GLabel 1550 5150 0    50   Input ~ 0
ISO_DIF_HAL_B
Text GLabel 1575 6550 0    50   Input ~ 0
ISO_DIF_HAL_C
$Comp
L PMSM_Driver_LV_Board_IC:MAX3095 U?
U 1 1 60E94C8A
P 5700 1825
AR Path="/6068445B/60E94C8A" Ref="U?"  Part="1" 
AR Path="/60E816A3/60E94C8A" Ref="U?"  Part="1" 
AR Path="/5EA5DF8B/60E94C8A" Ref="U22"  Part="1" 
F 0 "U22" H 5525 2325 50  0000 C CNN
F 1 "MAX3095" H 5700 1325 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:QSOP-16_3.9x4.9mm_P0.635mm" H 5700 1225 31  0001 C CNN
F 3 "~" H 5150 2325 31  0001 C CNN
F 4 "MAX3095EEE+ -  Line Receiver RS422, RS485, 4.75V-5.25V supply, QSOP-16 " H 5700 1175 31  0001 C CNN "Description"
F 5 "https://uk.farnell.com/maxim-integrated-products/max3095eee/rs422-rs485-rx-10mbps-5-25v-qsop/dp/2511435" H 5700 1125 31  0001 C CNN "Distributor Link"
F 6 "https://www.mouser.co.uk/ProductDetail/Maxim-Integrated/MAX3095EEE%2b?qs=sGAEpiMZZMuXae9YOZoWd2EMEyiB%2FMauqy%2FIQB84zec%3D" H 5700 1825 50  0001 C CNN "Distributor Link 2"
	1    5700 1825
	1    0    0    -1  
$EndComp
Wire Wire Line
	6150 1225 6250 1225
Wire Wire Line
	6550 1225 6550 1275
Wire Wire Line
	6450 1225 6550 1225
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 60E94C95
P 6350 1225
AR Path="/5E9C6910/60E94C95" Ref="C?"  Part="1" 
AR Path="/5EBC6654/60E94C95" Ref="C?"  Part="1" 
AR Path="/5EBC681B/60E94C95" Ref="C?"  Part="1" 
AR Path="/5F05E355/60E94C95" Ref="C?"  Part="1" 
AR Path="/6068445B/60E94C95" Ref="C?"  Part="1" 
AR Path="/60E816A3/60E94C95" Ref="C?"  Part="1" 
AR Path="/5EA5DF8B/60E94C95" Ref="C51"  Part="1" 
F 0 "C51" V 6475 1175 50  0000 L CNN
F 1 "100nF" V 6400 950 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 6350 1225 50  0001 C CNN
F 3 "~" H 6350 1225 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 6350 825 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 6350 725 50  0001 C CNN "Distributor Link"
	1    6350 1225
	0    -1   -1   0   
$EndComp
Connection ~ 6150 1225
Wire Wire Line
	6150 1225 6150 1475
Wire Wire Line
	6150 1125 6150 1225
$Comp
L PMSM_Driver_LV_Board_Power:ISO_DGND #PWR?
U 1 1 60E94C9E
P 6550 1275
AR Path="/5F05E355/60E94C9E" Ref="#PWR?"  Part="1" 
AR Path="/6068445B/60E94C9E" Ref="#PWR?"  Part="1" 
AR Path="/60E816A3/60E94C9E" Ref="#PWR?"  Part="1" 
AR Path="/5EA5DF8B/60E94C9E" Ref="#PWR0158"  Part="1" 
F 0 "#PWR0158" H 6550 1025 50  0001 C CNN
F 1 "ISO_DGND" H 6555 1102 50  0000 C CNN
F 2 "" H 6550 1275 50  0001 C CNN
F 3 "" H 6550 1275 50  0001 C CNN
	1    6550 1275
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:ISO_SENS_5V0 #PWR?
U 1 1 60E94CA4
P 6150 1125
AR Path="/6068445B/60E94CA4" Ref="#PWR?"  Part="1" 
AR Path="/60E816A3/60E94CA4" Ref="#PWR?"  Part="1" 
AR Path="/5EA5DF8B/60E94CA4" Ref="#PWR0156"  Part="1" 
F 0 "#PWR0156" H 6150 1375 50  0001 C CNN
F 1 "ISO_SENS_5V0" H 6151 1298 50  0000 C CNN
F 2 "" H 6150 1125 50  0001 C CNN
F 3 "" H 6150 1125 50  0001 C CNN
	1    6150 1125
	1    0    0    -1  
$EndComp
Wire Wire Line
	6050 1475 6150 1475
$Comp
L PMSM_Driver_LV_Board_Power:ISO_DGND #PWR?
U 1 1 60E94CAB
P 5250 2375
AR Path="/5F05E355/60E94CAB" Ref="#PWR?"  Part="1" 
AR Path="/6068445B/60E94CAB" Ref="#PWR?"  Part="1" 
AR Path="/60E816A3/60E94CAB" Ref="#PWR?"  Part="1" 
AR Path="/5EA5DF8B/60E94CAB" Ref="#PWR0155"  Part="1" 
F 0 "#PWR0155" H 5250 2125 50  0001 C CNN
F 1 "ISO_DGND" H 5255 2202 50  0000 C CNN
F 2 "" H 5250 2375 50  0001 C CNN
F 3 "" H 5250 2375 50  0001 C CNN
	1    5250 2375
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5350 2175 5250 2175
Wire Wire Line
	5250 2375 5250 2175
$Comp
L PMSM_Driver_LV_Board_Power:ISO_DGND #PWR?
U 1 1 60E94CB3
P 6150 2375
AR Path="/5F05E355/60E94CB3" Ref="#PWR?"  Part="1" 
AR Path="/6068445B/60E94CB3" Ref="#PWR?"  Part="1" 
AR Path="/60E816A3/60E94CB3" Ref="#PWR?"  Part="1" 
AR Path="/5EA5DF8B/60E94CB3" Ref="#PWR0157"  Part="1" 
F 0 "#PWR0157" H 6150 2125 50  0001 C CNN
F 1 "ISO_DGND" H 6155 2202 50  0000 C CNN
F 2 "" H 6150 2375 50  0001 C CNN
F 3 "" H 6150 2375 50  0001 C CNN
	1    6150 2375
	1    0    0    -1  
$EndComp
Wire Wire Line
	6150 1875 6050 1875
Wire Wire Line
	6150 1875 6150 2375
Wire Wire Line
	5350 1775 5250 1775
Wire Wire Line
	5250 1125 5250 1775
$Comp
L PMSM_Driver_LV_Board_Power:ISO_SENS_5V0 #PWR?
U 1 1 60E94CBD
P 5250 1125
AR Path="/6068445B/60E94CBD" Ref="#PWR?"  Part="1" 
AR Path="/60E816A3/60E94CBD" Ref="#PWR?"  Part="1" 
AR Path="/5EA5DF8B/60E94CBD" Ref="#PWR0154"  Part="1" 
F 0 "#PWR0154" H 5250 1375 50  0001 C CNN
F 1 "ISO_SENS_5V0" H 5251 1298 50  0000 C CNN
F 2 "" H 5250 1125 50  0001 C CNN
F 3 "" H 5250 1125 50  0001 C CNN
	1    5250 1125
	1    0    0    -1  
$EndComp
NoConn ~ 6050 1575
NoConn ~ 6050 1675
NoConn ~ 6050 1775
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 60E94CC8
P 4250 1525
AR Path="/5F05E355/60E94CC8" Ref="R?"  Part="1" 
AR Path="/6068445B/60E94CC8" Ref="R?"  Part="1" 
AR Path="/60E816A3/60E94CC8" Ref="R?"  Part="1" 
AR Path="/5EA5DF8B/60E94CC8" Ref="R94"  Part="1" 
F 0 "R94" H 4375 1550 50  0000 C CNN
F 1 "100" H 4375 1475 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 4250 1525 50  0001 C CNN
F 3 "~" H 4250 1525 50  0001 C CNN
F 4 "MCWR04X1000FTL -  SMD Chip Resistor, 0402 [1005 Metric], 100 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 4250 1125 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x1000ftl/res-100r-1-0-0625w-thick-film/dp/2447095" H 4250 1025 50  0001 C CNN "Distributor Link"
	1    4250 1525
	1    0    0    -1  
$EndComp
Wire Wire Line
	4150 1375 4250 1375
Wire Wire Line
	4250 1375 4250 1425
Wire Wire Line
	4150 1675 4250 1675
Wire Wire Line
	4250 1675 4250 1625
Wire Wire Line
	4250 1375 4450 1375
Connection ~ 4250 1375
Wire Wire Line
	4250 1675 4450 1675
Connection ~ 4250 1675
Wire Wire Line
	4450 1575 4450 1675
Wire Wire Line
	4450 1375 4450 1475
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 60E94CDA
P 4250 2025
AR Path="/5F05E355/60E94CDA" Ref="R?"  Part="1" 
AR Path="/6068445B/60E94CDA" Ref="R?"  Part="1" 
AR Path="/60E816A3/60E94CDA" Ref="R?"  Part="1" 
AR Path="/5EA5DF8B/60E94CDA" Ref="R95"  Part="1" 
F 0 "R95" H 4375 2050 50  0000 C CNN
F 1 "100" H 4375 1975 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 4250 2025 50  0001 C CNN
F 3 "~" H 4250 2025 50  0001 C CNN
F 4 "MCWR04X1000FTL -  SMD Chip Resistor, 0402 [1005 Metric], 100 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 4250 1625 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x1000ftl/res-100r-1-0-0625w-thick-film/dp/2447095" H 4250 1525 50  0001 C CNN "Distributor Link"
	1    4250 2025
	1    0    0    -1  
$EndComp
Wire Wire Line
	4150 1875 4250 1875
Wire Wire Line
	4250 1875 4250 1925
Wire Wire Line
	4150 2175 4250 2175
Wire Wire Line
	4250 2175 4250 2125
Wire Wire Line
	4250 1875 4450 1875
Connection ~ 4250 1875
Wire Wire Line
	4250 2175 4450 2175
Connection ~ 4250 2175
Wire Wire Line
	4450 2075 4450 2175
Wire Wire Line
	4450 1875 4450 1975
Wire Wire Line
	5150 1675 5350 1675
Wire Wire Line
	4450 1475 5350 1475
Wire Wire Line
	4450 1575 5350 1575
Wire Wire Line
	4450 1975 5350 1975
Wire Wire Line
	4450 2075 5350 2075
Wire Wire Line
	5150 1875 5350 1875
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 60E94CF4
P 7150 2125
AR Path="/5F05E355/60E94CF4" Ref="R?"  Part="1" 
AR Path="/6068445B/60E94CF4" Ref="R?"  Part="1" 
AR Path="/60E816A3/60E94CF4" Ref="R?"  Part="1" 
AR Path="/5EA5DF8B/60E94CF4" Ref="R96"  Part="1" 
F 0 "R96" H 7025 2150 50  0000 C CNN
F 1 "100" H 7025 2075 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 7150 2125 50  0001 C CNN
F 3 "~" H 7150 2125 50  0001 C CNN
F 4 "MCWR04X1000FTL -  SMD Chip Resistor, 0402 [1005 Metric], 100 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 7150 1725 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x1000ftl/res-100r-1-0-0625w-thick-film/dp/2447095" H 7150 1625 50  0001 C CNN "Distributor Link"
	1    7150 2125
	-1   0    0    -1  
$EndComp
Wire Wire Line
	7250 1975 7150 1975
Wire Wire Line
	7150 1975 7150 2025
Wire Wire Line
	7250 2275 7150 2275
Wire Wire Line
	7150 2275 7150 2225
Wire Wire Line
	7150 1975 6950 1975
Connection ~ 7150 1975
Wire Wire Line
	7150 2275 6950 2275
Connection ~ 7150 2275
Wire Wire Line
	6950 2175 6950 2275
Wire Wire Line
	6950 1975 6950 2075
Wire Wire Line
	6950 2075 6050 2075
Wire Wire Line
	6950 2175 6050 2175
Text GLabel 6250 1975 2    50   Output ~ 0
ISO_DIF_HAL_C
Wire Wire Line
	6250 1975 6050 1975
Text Notes 675  975  0    100  ~ 0
Select Differential or \nSingle-Ended Hall Sensor
Text Notes 1200 2125 0    31   ~ 0
Select\ndifferential\nHall sensor
Text GLabel 1550 3650 0    50   Input ~ 0
ISO_SELECT_SE_HAL
Text GLabel 1550 5050 0    50   Input ~ 0
ISO_SELECT_SE_HAL
Text GLabel 1575 6450 0    50   Input ~ 0
ISO_SELECT_SE_HAL
Text Notes 3375 800  0    100  ~ 0
Differential Signals to Single-Ended Signals
Wire Notes Line
	3300 600  8025 600 
Wire Notes Line
	8025 600  8025 2675
Wire Notes Line
	8025 2675 3300 2675
Wire Notes Line
	3300 600  3300 2675
Text Notes 675  2950 0    100  ~ 0
Select Signals to Controller
Wire Notes Line
	600  2750 3225 2750
Wire Notes Line
	3225 2750 3225 7150
Wire Notes Line
	3225 7150 600  7150
Wire Notes Line
	600  2750 600  7150
Wire Notes Line
	3225 600  3225 2675
Wire Notes Line
	600  600  600  2675
Wire Notes Line
	600  2675 3225 2675
Wire Notes Line
	600  600  3225 600 
Text GLabel 5150 1875 0    50   Output ~ 0
ISO_DIF_HAL_B
Text GLabel 5150 1675 0    50   Output ~ 0
ISO_DIF_HAL_A
Text GLabel 4150 1875 0    50   Output ~ 0
ISO_DIF_HAL_B_P
Text GLabel 4150 2175 0    50   Output ~ 0
ISO_DIF_HAL_B_N
Text GLabel 4150 1675 0    50   Output ~ 0
ISO_DIF_HAL_A_P
Text GLabel 4150 1375 0    50   Output ~ 0
ISO_DIF_HAL_A_N
$EndSCHEMATC
