EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 11 16
Title "PMSM_Driver_LV_Board"
Date "2020-03-30"
Rev "A"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text GLabel 3750 5200 2    50   Output ~ 0
ISO_V_B
Text GLabel 7625 2475 2    50   Output ~ 0
ISO_V_C
Text GLabel 7625 5200 2    50   Output ~ 0
ISO_V_SUP
Text GLabel 1250 4400 0    50   Input ~ 0
MOT_PHASE_B
Text GLabel 5125 1675 0    50   Input ~ 0
MOT_PHASE_C
Text GLabel 1250 1675 0    50   Input ~ 0
MOT_PHASE_A
Wire Wire Line
	1350 1775 1350 1675
Wire Wire Line
	1350 1675 1250 1675
$Comp
L PMSM_Driver_LV_Board_IC:AD7403BRIZ U?
U 1 1 602190D9
P 3100 2325
AR Path="/5EFE605D/602190D9" Ref="U?"  Part="1" 
AR Path="/5EFE7331/602190D9" Ref="U?"  Part="1" 
AR Path="/5EA5E1C2/602190D9" Ref="U38"  Part="1" 
F 0 "U38" H 2825 2825 50  0000 C CNN
F 1 "AD7403BRIZ" H 3100 1825 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:SOIC-16W_7.5x12.8mm_P1.27mm" H 3100 1675 50  0001 C CNN
F 3 "~" H 2800 2825 50  0001 C CNN
F 4 "AD7403BRIZ -  Analogue to Digital Converter, 16 bit, Differential, Serial, Single, 4.5 V" H 3100 1625 31  0001 C CNN "Description"
F 5 "https://uk.farnell.com/analog-devices/ad7403briz/adc-16bit-soic-16/dp/2431665" H 3100 1575 31  0001 C CNN "Distributor Link"
F 6 "https://www.mouser.co.uk/ProductDetail/Analog-Devices/AD7403BRIZ?qs=sGAEpiMZZMt%2Foy9k%2F5obbVdPNrLrhFiKvGld2xGDVNA%3D" H 3100 1525 31  0001 C CNN "Distributor Link 2"
	1    3100 2325
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_5V0 #PWR?
U 1 1 602190DF
P 3750 1100
AR Path="/5EFE605D/602190DF" Ref="#PWR?"  Part="1" 
AR Path="/5EFE7331/602190DF" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E1C2/602190DF" Ref="#PWR0313"  Part="1" 
F 0 "#PWR0313" H 3750 1350 50  0001 C CNN
F 1 "MOT_5V0" H 3751 1273 50  0000 C CNN
F 2 "" H 3750 1100 50  0001 C CNN
F 3 "" H 3750 1100 50  0001 C CNN
	1    3750 1100
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR?
U 1 1 602190E5
P 3650 2775
AR Path="/5EFE605D/602190E5" Ref="#PWR?"  Part="1" 
AR Path="/5EFE7331/602190E5" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E1C2/602190E5" Ref="#PWR0311"  Part="1" 
F 0 "#PWR0311" H 3650 2525 50  0001 C CNN
F 1 "MOT_DGND" H 3655 2602 50  0000 C CNN
F 2 "" H 3650 2775 50  0001 C CNN
F 3 "" H 3650 2775 50  0001 C CNN
	1    3650 2775
	1    0    0    -1  
$EndComp
Wire Wire Line
	3650 2775 3650 2675
Wire Wire Line
	3650 1975 3550 1975
Wire Wire Line
	3550 2675 3650 2675
Connection ~ 3650 2675
Wire Wire Line
	3650 2675 3650 1975
Wire Wire Line
	3750 2175 3550 2175
NoConn ~ 3550 2075
NoConn ~ 3550 2375
NoConn ~ 3550 2575
Text GLabel 3750 2275 2    50   Input ~ 0
ISO_ADC_CLK
Wire Wire Line
	3750 2275 3550 2275
Wire Wire Line
	3750 2475 3550 2475
Wire Wire Line
	1800 1450 1800 1500
Wire Wire Line
	2450 2575 2650 2575
Wire Wire Line
	2650 1975 2450 1975
Wire Wire Line
	2450 1975 2450 2575
Wire Wire Line
	2550 2800 2550 2675
Wire Wire Line
	2550 2675 2650 2675
Wire Wire Line
	2550 2675 2550 2275
Wire Wire Line
	2550 2275 2650 2275
Connection ~ 2550 2675
NoConn ~ 2650 2375
NoConn ~ 2650 2475
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 6021910A
P 1925 2075
AR Path="/5F05E355/6021910A" Ref="R?"  Part="1" 
AR Path="/5E9C6910/6021910A" Ref="R?"  Part="1" 
AR Path="/5EFE605D/6021910A" Ref="R?"  Part="1" 
AR Path="/5EFE7331/6021910A" Ref="R?"  Part="1" 
AR Path="/5EA5E1C2/6021910A" Ref="R162"  Part="1" 
F 0 "R162" V 1975 2225 50  0000 C CNN
F 1 "22" V 1975 1975 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 1925 2075 50  0001 C CNN
F 3 "~" H 1925 2075 50  0001 C CNN
F 4 "MCWR04X22R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 22 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 1925 1675 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x22r0ftl/res-22r-1-0-0625w-0402-thick-film/dp/2447138" H 1925 1575 50  0001 C CNN "Distributor Link"
	1    1925 2075
	0    -1   -1   0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 60219112
P 1925 2375
AR Path="/5F05E355/60219112" Ref="R?"  Part="1" 
AR Path="/5E9C6910/60219112" Ref="R?"  Part="1" 
AR Path="/5EFE605D/60219112" Ref="R?"  Part="1" 
AR Path="/5EFE7331/60219112" Ref="R?"  Part="1" 
AR Path="/5EA5E1C2/60219112" Ref="R163"  Part="1" 
F 0 "R163" V 1975 2525 50  0000 C CNN
F 1 "22" V 1975 2275 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 1925 2375 50  0001 C CNN
F 3 "~" H 1925 2375 50  0001 C CNN
F 4 "MCWR04X22R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 22 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 1925 1975 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x22r0ftl/res-22r-1-0-0625w-0402-thick-film/dp/2447138" H 1925 1875 50  0001 C CNN "Distributor Link"
	1    1925 2375
	0    -1   -1   0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 6021911C
P 2125 2225
AR Path="/5E9C6910/6021911C" Ref="C?"  Part="1" 
AR Path="/5EBC6654/6021911C" Ref="C?"  Part="1" 
AR Path="/5EBC681B/6021911C" Ref="C?"  Part="1" 
AR Path="/5F05E355/6021911C" Ref="C?"  Part="1" 
AR Path="/6068445B/6021911C" Ref="C?"  Part="1" 
AR Path="/60E816A3/6021911C" Ref="C?"  Part="1" 
AR Path="/5EFE605D/6021911C" Ref="C?"  Part="1" 
AR Path="/5EFE7331/6021911C" Ref="C?"  Part="1" 
AR Path="/5EA5E1C2/6021911C" Ref="C151"  Part="1" 
F 0 "C151" H 1925 2300 50  0000 L CNN
F 1 "47pF" H 1925 2150 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 2125 2225 50  0001 C CNN
F 3 "~" H 2125 2225 50  0001 C CNN
F 4 "MC0402N470J500CT -  SMD Multilayer Ceramic Capacitor, 47 pF, 50 V, 0402 [1005 Metric], ± 5%, C0G / NP0, MC Series " H 2125 1825 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402n470j500ct/cap-47pf-50v-5-c0g-np0-0402/dp/1758959" H 2125 1725 50  0001 C CNN "Distributor Link"
	1    2125 2225
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2025 2075 2125 2075
Wire Wire Line
	2125 2075 2125 2125
Wire Wire Line
	2025 2375 2125 2375
Wire Wire Line
	2125 2375 2125 2325
Wire Wire Line
	2125 2375 2375 2375
Wire Wire Line
	2375 2375 2375 2175
Wire Wire Line
	2375 2175 2650 2175
Connection ~ 2125 2375
Wire Wire Line
	2650 2075 2125 2075
Connection ~ 2125 2075
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 60219134
P 2125 1300
AR Path="/5E9C6910/60219134" Ref="C?"  Part="1" 
AR Path="/5EBC6654/60219134" Ref="C?"  Part="1" 
AR Path="/5EBC681B/60219134" Ref="C?"  Part="1" 
AR Path="/5F05E355/60219134" Ref="C?"  Part="1" 
AR Path="/6068445B/60219134" Ref="C?"  Part="1" 
AR Path="/60E816A3/60219134" Ref="C?"  Part="1" 
AR Path="/5EFE605D/60219134" Ref="C?"  Part="1" 
AR Path="/5EFE7331/60219134" Ref="C?"  Part="1" 
AR Path="/5EA5E1C2/60219134" Ref="C150"  Part="1" 
F 0 "C150" H 1925 1375 50  0000 L CNN
F 1 "1nF" H 1975 1225 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 2125 1300 50  0001 C CNN
F 3 "~" H 2125 1300 50  0001 C CNN
F 4 "MC0402B102K160CT -  SMD Multilayer Ceramic Capacitor, 1000 pF, 16 V, 0402 [1005 Metric], ± 10%, X7R, MC Series" H 2125 900 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402b102k160ct/cap-1000pf-16v-10-x7r-0402/dp/2320774" H 2125 800 50  0001 C CNN "Distributor Link"
	1    2125 1300
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2125 1400 2125 1450
Wire Wire Line
	2125 1150 2125 1200
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 6021913E
P 1800 1300
AR Path="/5E9C6910/6021913E" Ref="C?"  Part="1" 
AR Path="/5EBC6654/6021913E" Ref="C?"  Part="1" 
AR Path="/5EBC681B/6021913E" Ref="C?"  Part="1" 
AR Path="/5F05E355/6021913E" Ref="C?"  Part="1" 
AR Path="/6068445B/6021913E" Ref="C?"  Part="1" 
AR Path="/60E816A3/6021913E" Ref="C?"  Part="1" 
AR Path="/5EFE605D/6021913E" Ref="C?"  Part="1" 
AR Path="/5EFE7331/6021913E" Ref="C?"  Part="1" 
AR Path="/5EA5E1C2/6021913E" Ref="C146"  Part="1" 
F 0 "C146" H 1600 1375 50  0000 L CNN
F 1 "10uF" H 1600 1225 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 1800 1300 50  0001 C CNN
F 3 "~" H 1800 1300 50  0001 C CNN
F 4 "LMK107BBJ106MALT -  SMD Multilayer Ceramic Capacitor, 10 µF, 10 V, 0603 [1608 Metric], ± 20%, X5R, M Series" H 1800 900 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/taiyo-yuden/lmk107bbj106malt/cap-10-f-10v-20-x5r-0603/dp/2113070" H 1800 800 50  0001 C CNN "Distributor Link"
	1    1800 1300
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1800 1400 1800 1450
Wire Wire Line
	1800 1150 1800 1200
Connection ~ 1800 1450
Wire Wire Line
	2450 1100 2450 1150
Wire Wire Line
	1800 1450 2125 1450
Wire Wire Line
	1800 1150 2125 1150
Connection ~ 2125 1150
Wire Wire Line
	2125 1150 2450 1150
Connection ~ 2450 1150
Wire Wire Line
	3100 1450 3100 1500
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR?
U 1 1 6021914E
P 3100 1500
AR Path="/5EFE605D/6021914E" Ref="#PWR?"  Part="1" 
AR Path="/5EFE7331/6021914E" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E1C2/6021914E" Ref="#PWR0309"  Part="1" 
F 0 "#PWR0309" H 3100 1250 50  0001 C CNN
F 1 "MOT_DGND" H 3105 1327 50  0000 C CNN
F 2 "" H 3100 1500 50  0001 C CNN
F 3 "" H 3100 1500 50  0001 C CNN
	1    3100 1500
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3425 1400 3425 1450
Wire Wire Line
	3425 1150 3425 1200
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 60219158
P 3100 1300
AR Path="/5E9C6910/60219158" Ref="C?"  Part="1" 
AR Path="/5EBC6654/60219158" Ref="C?"  Part="1" 
AR Path="/5EBC681B/60219158" Ref="C?"  Part="1" 
AR Path="/5F05E355/60219158" Ref="C?"  Part="1" 
AR Path="/6068445B/60219158" Ref="C?"  Part="1" 
AR Path="/60E816A3/60219158" Ref="C?"  Part="1" 
AR Path="/5EFE605D/60219158" Ref="C?"  Part="1" 
AR Path="/5EFE7331/60219158" Ref="C?"  Part="1" 
AR Path="/5EA5E1C2/60219158" Ref="C154"  Part="1" 
F 0 "C154" H 2900 1375 50  0000 L CNN
F 1 "10uF" H 2900 1225 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 3100 1300 50  0001 C CNN
F 3 "~" H 3100 1300 50  0001 C CNN
F 4 "LMK107BBJ106MALT -  SMD Multilayer Ceramic Capacitor, 10 µF, 10 V, 0603 [1608 Metric], ± 20%, X5R, M Series" H 3100 900 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/taiyo-yuden/lmk107bbj106malt/cap-10-f-10v-20-x5r-0603/dp/2113070" H 3100 800 50  0001 C CNN "Distributor Link"
	1    3100 1300
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3100 1400 3100 1450
Wire Wire Line
	3100 1150 3100 1200
Connection ~ 3100 1450
Wire Wire Line
	3750 1100 3750 1150
Wire Wire Line
	3100 1450 3425 1450
Wire Wire Line
	3100 1150 3425 1150
Connection ~ 3425 1150
Wire Wire Line
	3425 1150 3750 1150
Connection ~ 3750 1150
Connection ~ 2450 1975
Wire Wire Line
	3750 1150 3750 2175
Wire Wire Line
	2450 1150 2450 1975
Wire Wire Line
	1750 2875 1750 2925
Wire Wire Line
	2075 2825 2075 2875
Wire Wire Line
	2075 2575 2075 2625
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 6021916F
P 1750 2725
AR Path="/5E9C6910/6021916F" Ref="C?"  Part="1" 
AR Path="/5EBC6654/6021916F" Ref="C?"  Part="1" 
AR Path="/5EBC681B/6021916F" Ref="C?"  Part="1" 
AR Path="/5F05E355/6021916F" Ref="C?"  Part="1" 
AR Path="/6068445B/6021916F" Ref="C?"  Part="1" 
AR Path="/60E816A3/6021916F" Ref="C?"  Part="1" 
AR Path="/5EFE605D/6021916F" Ref="C?"  Part="1" 
AR Path="/5EFE7331/6021916F" Ref="C?"  Part="1" 
AR Path="/5EA5E1C2/6021916F" Ref="C144"  Part="1" 
F 0 "C144" H 1550 2800 50  0000 L CNN
F 1 "10uF" H 1550 2650 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 1750 2725 50  0001 C CNN
F 3 "~" H 1750 2725 50  0001 C CNN
F 4 "LMK107BBJ106MALT -  SMD Multilayer Ceramic Capacitor, 10 µF, 10 V, 0603 [1608 Metric], ± 20%, X5R, M Series" H 1750 2325 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/taiyo-yuden/lmk107bbj106malt/cap-10-f-10v-20-x5r-0603/dp/2113070" H 1750 2225 50  0001 C CNN "Distributor Link"
	1    1750 2725
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1750 2825 1750 2875
Wire Wire Line
	1750 2575 1750 2625
Connection ~ 1750 2875
Wire Wire Line
	1750 2875 2075 2875
Wire Wire Line
	1750 2575 2075 2575
Wire Wire Line
	1350 2375 1825 2375
Connection ~ 2450 2575
Wire Wire Line
	2450 2575 2075 2575
Connection ~ 2075 2575
Text Notes 2800 2900 0    25   ~ 0
Isolated sigma-delta modulator
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 6021918E
P 3425 1300
AR Path="/5E9C6910/6021918E" Ref="C?"  Part="1" 
AR Path="/5EBC6654/6021918E" Ref="C?"  Part="1" 
AR Path="/5EBC681B/6021918E" Ref="C?"  Part="1" 
AR Path="/5F05E355/6021918E" Ref="C?"  Part="1" 
AR Path="/6068445B/6021918E" Ref="C?"  Part="1" 
AR Path="/60E816A3/6021918E" Ref="C?"  Part="1" 
AR Path="/5EFE605D/6021918E" Ref="C?"  Part="1" 
AR Path="/5EFE7331/6021918E" Ref="C?"  Part="1" 
AR Path="/5EA5E1C2/6021918E" Ref="C156"  Part="1" 
F 0 "C156" H 3225 1375 50  0000 L CNN
F 1 "1nF" H 3275 1225 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 3425 1300 50  0001 C CNN
F 3 "~" H 3425 1300 50  0001 C CNN
F 4 "MC0402B102K160CT -  SMD Multilayer Ceramic Capacitor, 1000 pF, 16 V, 0402 [1005 Metric], ± 10%, X7R, MC Series" H 3425 900 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402b102k160ct/cap-1000pf-16v-10-x7r-0402/dp/2320774" H 3425 800 50  0001 C CNN "Distributor Link"
	1    3425 1300
	-1   0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 60219196
P 2075 2725
AR Path="/5E9C6910/60219196" Ref="C?"  Part="1" 
AR Path="/5EBC6654/60219196" Ref="C?"  Part="1" 
AR Path="/5EBC681B/60219196" Ref="C?"  Part="1" 
AR Path="/5F05E355/60219196" Ref="C?"  Part="1" 
AR Path="/6068445B/60219196" Ref="C?"  Part="1" 
AR Path="/60E816A3/60219196" Ref="C?"  Part="1" 
AR Path="/5EFE605D/60219196" Ref="C?"  Part="1" 
AR Path="/5EFE7331/60219196" Ref="C?"  Part="1" 
AR Path="/5EA5E1C2/60219196" Ref="C148"  Part="1" 
F 0 "C148" H 1875 2800 50  0000 L CNN
F 1 "1nF" H 1925 2650 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 2075 2725 50  0001 C CNN
F 3 "~" H 2075 2725 50  0001 C CNN
F 4 "MC0402B102K160CT -  SMD Multilayer Ceramic Capacitor, 1000 pF, 16 V, 0402 [1005 Metric], ± 10%, X7R, MC Series" H 2075 2325 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402b102k160ct/cap-1000pf-16v-10-x7r-0402/dp/2320774" H 2075 2225 50  0001 C CNN "Distributor Link"
	1    2075 2725
	-1   0    0    -1  
$EndComp
Text GLabel 3750 2475 2    50   Output ~ 0
ISO_V_A
$Comp
L PMSM_Driver_LV_Board_Power:MOT_PGND #PWR0299
U 1 1 6022F369
P 1350 2675
F 0 "#PWR0299" H 1350 2425 50  0001 C CNN
F 1 "MOT_PGND" H 1355 2502 50  0000 C CNN
F 2 "" H 1350 2675 50  0001 C CNN
F 3 "" H 1350 2675 50  0001 C CNN
	1    1350 2675
	1    0    0    -1  
$EndComp
Wire Wire Line
	1350 2375 1350 2325
Wire Wire Line
	1350 2125 1350 2075
Wire Wire Line
	1350 2075 1825 2075
$Comp
L PMSM_Driver_LV_Board_Power:MOT_5V0 #PWR?
U 1 1 602D33A6
P 2450 1100
AR Path="/5EFE605D/602D33A6" Ref="#PWR?"  Part="1" 
AR Path="/5EFE7331/602D33A6" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E1C2/602D33A6" Ref="#PWR0305"  Part="1" 
F 0 "#PWR0305" H 2450 1350 50  0001 C CNN
F 1 "MOT_5V0" H 2451 1273 50  0000 C CNN
F 2 "" H 2450 1100 50  0001 C CNN
F 3 "" H 2450 1100 50  0001 C CNN
	1    2450 1100
	1    0    0    -1  
$EndComp
Wire Notes Line
	4400 3250 600  3250
Wire Notes Line
	600  3250 600  600 
Wire Notes Line
	600  600  4400 600 
Wire Notes Line
	4400 600  4400 3250
Text Notes 675  800  0    100  ~ 0
Phase A Voltage Measurement
Text Notes 700  875  0    25   ~ 0
For measuring back EMF during identification.
Text Notes 700  1600 0    25   ~ 0
This voltage divider creates 0.25V\nfrom 20V phase voltage. Full-scale\nvoltage is 0.32V (25.5V phase \nvoltage). You can place the jumper\nto add parallel resistor of your choice\nto adjust the range for normal\noperation.\n
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 601E9FAB
P 1350 2225
AR Path="/5F05E355/601E9FAB" Ref="R?"  Part="1" 
AR Path="/5E9C6910/601E9FAB" Ref="R?"  Part="1" 
AR Path="/5EFE605D/601E9FAB" Ref="R?"  Part="1" 
AR Path="/5EFE7331/601E9FAB" Ref="R?"  Part="1" 
AR Path="/5EA5E1C2/601E9FAB" Ref="R159"  Part="1" 
F 0 "R159" H 1500 2275 50  0000 C CNN
F 1 "1k27" H 1500 2200 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 1350 2225 50  0001 C CNN
F 3 "~" H 1350 2225 50  0001 C CNN
F 4 "WF08U1271BTL -  SMD Chip Resistor, 0805 [2012 Metric], 1.27 kohm, WF08U Series, 100 V, Thin Film, 100 mW" H 1350 1825 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/walsin/wf08u1271btl/res-1k27-0-1-0-1w-0805-thin-film/dp/2670133" H 1350 1725 50  0001 C CNN "Distributor Link"
F 6 "0.1%" H 1475 2125 50  0000 C CNN "Tolerance"
	1    1350 2225
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 6038DA8E
P 1350 1875
AR Path="/5F05E355/6038DA8E" Ref="R?"  Part="1" 
AR Path="/5E9C6910/6038DA8E" Ref="R?"  Part="1" 
AR Path="/5EFE605D/6038DA8E" Ref="R?"  Part="1" 
AR Path="/5EFE7331/6038DA8E" Ref="R?"  Part="1" 
AR Path="/5EA5E1C2/6038DA8E" Ref="R158"  Part="1" 
F 0 "R158" H 1500 1925 50  0000 C CNN
F 1 "100k" H 1500 1850 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 1350 1875 50  0001 C CNN
F 3 "~" H 1350 1875 50  0001 C CNN
F 4 "MCWF08U1003BTL -  SMD Chip Resistor, 0805 [2012 Metric], 100 kohm, MCWF08U Series, 100 V, Thin Film, 100 mW" H 1350 1475 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwf08u1003btl/res-100k-0-1-0-1w-0805-thin-film/dp/2694161" H 1350 1375 50  0001 C CNN "Distributor Link"
F 6 "0.1%" H 1500 1775 50  0000 C CNN "Tolerance"
	1    1350 1875
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Connector:Jumper_NC_Small JP?
U 1 1 603BF2B7
P 950 2225
AR Path="/6068445B/603BF2B7" Ref="JP?"  Part="1" 
AR Path="/60E816A3/603BF2B7" Ref="JP?"  Part="1" 
AR Path="/60EB5E32/603BF2B7" Ref="JP?"  Part="1" 
AR Path="/5EFE7331/603BF2B7" Ref="JP?"  Part="1" 
AR Path="/5EA5E1C2/603BF2B7" Ref="JP7"  Part="1" 
F 0 "JP7" V 975 2200 50  0000 R CNN
F 1 "JUMPER" V 900 2200 50  0000 R CNN
F 2 "PMSM_Driver_LV_Board:PinHeader_1x02_P2.54mm_Vertical" H 950 2075 50  0001 C CNN
F 3 "~" H 950 2225 50  0001 C CNN
F 4 "2211S-02G -  Board-To-Board Connector, 2.54 mm, 2 Contacts, Header, 2211S Series, Through Hole, 1 Rows" H 950 1975 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/2211s-02g/header-1-row-vert-2way/dp/1593411" H 950 1875 50  0001 C CNN "Distributor Link"
F 6 "DNI" V 850 2325 50  0000 C CNN "DNI"
	1    950  2225
	0    -1   -1   0   
$EndComp
Connection ~ 1350 2375
Wire Wire Line
	1350 1975 1350 2025
Connection ~ 1350 2075
Wire Wire Line
	950  2125 950  2025
Wire Wire Line
	950  2025 1350 2025
Connection ~ 1350 2025
Wire Wire Line
	1350 2025 1350 2075
Wire Wire Line
	1350 2625 1350 2675
Wire Wire Line
	1350 4400 1250 4400
$Comp
L PMSM_Driver_LV_Board_IC:AD7403BRIZ U?
U 1 1 603F1D09
P 3100 5050
AR Path="/5EFE605D/603F1D09" Ref="U?"  Part="1" 
AR Path="/5EFE7331/603F1D09" Ref="U?"  Part="1" 
AR Path="/5EA5E1C2/603F1D09" Ref="U39"  Part="1" 
F 0 "U39" H 2825 5550 50  0000 C CNN
F 1 "AD7403BRIZ" H 3100 4550 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:SOIC-16W_7.5x12.8mm_P1.27mm" H 3100 4400 50  0001 C CNN
F 3 "~" H 2800 5550 50  0001 C CNN
F 4 "AD7403BRIZ -  Analogue to Digital Converter, 16 bit, Differential, Serial, Single, 4.5 V" H 3100 4350 31  0001 C CNN "Description"
F 5 "https://uk.farnell.com/analog-devices/ad7403briz/adc-16bit-soic-16/dp/2431665" H 3100 4300 31  0001 C CNN "Distributor Link"
F 6 "https://www.mouser.co.uk/ProductDetail/Analog-Devices/AD7403BRIZ?qs=sGAEpiMZZMt%2Foy9k%2F5obbVdPNrLrhFiKvGld2xGDVNA%3D" H 3100 4250 31  0001 C CNN "Distributor Link 2"
	1    3100 5050
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_5V0 #PWR?
U 1 1 603F1D0F
P 3750 3825
AR Path="/5EFE605D/603F1D0F" Ref="#PWR?"  Part="1" 
AR Path="/5EFE7331/603F1D0F" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E1C2/603F1D0F" Ref="#PWR0314"  Part="1" 
F 0 "#PWR0314" H 3750 4075 50  0001 C CNN
F 1 "MOT_5V0" H 3751 3998 50  0000 C CNN
F 2 "" H 3750 3825 50  0001 C CNN
F 3 "" H 3750 3825 50  0001 C CNN
	1    3750 3825
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR?
U 1 1 603F1D15
P 3650 5500
AR Path="/5EFE605D/603F1D15" Ref="#PWR?"  Part="1" 
AR Path="/5EFE7331/603F1D15" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E1C2/603F1D15" Ref="#PWR0312"  Part="1" 
F 0 "#PWR0312" H 3650 5250 50  0001 C CNN
F 1 "MOT_DGND" H 3655 5327 50  0000 C CNN
F 2 "" H 3650 5500 50  0001 C CNN
F 3 "" H 3650 5500 50  0001 C CNN
	1    3650 5500
	1    0    0    -1  
$EndComp
Wire Wire Line
	3650 5500 3650 5400
Wire Wire Line
	3650 4700 3550 4700
Wire Wire Line
	3550 5400 3650 5400
Connection ~ 3650 5400
Wire Wire Line
	3650 5400 3650 4700
Wire Wire Line
	3750 4900 3550 4900
NoConn ~ 3550 4800
NoConn ~ 3550 5100
NoConn ~ 3550 5300
Text GLabel 3750 5000 2    50   Input ~ 0
ISO_ADC_CLK
Wire Wire Line
	3750 5000 3550 5000
Wire Wire Line
	3750 5200 3550 5200
Wire Wire Line
	1800 4175 1800 4225
Wire Wire Line
	2450 5300 2650 5300
Wire Wire Line
	2650 4700 2450 4700
Wire Wire Line
	2450 4700 2450 5300
Wire Wire Line
	2550 5525 2550 5400
Wire Wire Line
	2550 5400 2650 5400
Wire Wire Line
	2550 5400 2550 5000
Wire Wire Line
	2550 5000 2650 5000
Connection ~ 2550 5400
NoConn ~ 2650 5100
NoConn ~ 2650 5200
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 603F1D34
P 1925 4800
AR Path="/5F05E355/603F1D34" Ref="R?"  Part="1" 
AR Path="/5E9C6910/603F1D34" Ref="R?"  Part="1" 
AR Path="/5EFE605D/603F1D34" Ref="R?"  Part="1" 
AR Path="/5EFE7331/603F1D34" Ref="R?"  Part="1" 
AR Path="/5EA5E1C2/603F1D34" Ref="R164"  Part="1" 
F 0 "R164" V 1975 4925 50  0000 C CNN
F 1 "22" V 1975 4700 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 1925 4800 50  0001 C CNN
F 3 "~" H 1925 4800 50  0001 C CNN
F 4 "MCWR04X22R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 22 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 1925 4400 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x22r0ftl/res-22r-1-0-0625w-0402-thick-film/dp/2447138" H 1925 4300 50  0001 C CNN "Distributor Link"
	1    1925 4800
	0    -1   -1   0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 603F1D3C
P 1925 5100
AR Path="/5F05E355/603F1D3C" Ref="R?"  Part="1" 
AR Path="/5E9C6910/603F1D3C" Ref="R?"  Part="1" 
AR Path="/5EFE605D/603F1D3C" Ref="R?"  Part="1" 
AR Path="/5EFE7331/603F1D3C" Ref="R?"  Part="1" 
AR Path="/5EA5E1C2/603F1D3C" Ref="R165"  Part="1" 
F 0 "R165" V 1975 5250 50  0000 C CNN
F 1 "22" V 1975 5000 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 1925 5100 50  0001 C CNN
F 3 "~" H 1925 5100 50  0001 C CNN
F 4 "MCWR04X22R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 22 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 1925 4700 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x22r0ftl/res-22r-1-0-0625w-0402-thick-film/dp/2447138" H 1925 4600 50  0001 C CNN "Distributor Link"
	1    1925 5100
	0    -1   -1   0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 603F1D44
P 2125 4950
AR Path="/5E9C6910/603F1D44" Ref="C?"  Part="1" 
AR Path="/5EBC6654/603F1D44" Ref="C?"  Part="1" 
AR Path="/5EBC681B/603F1D44" Ref="C?"  Part="1" 
AR Path="/5F05E355/603F1D44" Ref="C?"  Part="1" 
AR Path="/6068445B/603F1D44" Ref="C?"  Part="1" 
AR Path="/60E816A3/603F1D44" Ref="C?"  Part="1" 
AR Path="/5EFE605D/603F1D44" Ref="C?"  Part="1" 
AR Path="/5EFE7331/603F1D44" Ref="C?"  Part="1" 
AR Path="/5EA5E1C2/603F1D44" Ref="C153"  Part="1" 
F 0 "C153" H 1925 5025 50  0000 L CNN
F 1 "47pF" H 1925 4875 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 2125 4950 50  0001 C CNN
F 3 "~" H 2125 4950 50  0001 C CNN
F 4 "MC0402N470J500CT -  SMD Multilayer Ceramic Capacitor, 47 pF, 50 V, 0402 [1005 Metric], ± 5%, C0G / NP0, MC Series " H 2125 4550 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402n470j500ct/cap-47pf-50v-5-c0g-np0-0402/dp/1758959" H 2125 4450 50  0001 C CNN "Distributor Link"
	1    2125 4950
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2025 4800 2125 4800
Wire Wire Line
	2125 4800 2125 4850
Wire Wire Line
	2025 5100 2125 5100
Wire Wire Line
	2125 5100 2125 5050
Wire Wire Line
	2125 5100 2375 5100
Wire Wire Line
	2375 5100 2375 4900
Wire Wire Line
	2375 4900 2650 4900
Connection ~ 2125 5100
Wire Wire Line
	2650 4800 2125 4800
Connection ~ 2125 4800
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 603F1D56
P 2125 4025
AR Path="/5E9C6910/603F1D56" Ref="C?"  Part="1" 
AR Path="/5EBC6654/603F1D56" Ref="C?"  Part="1" 
AR Path="/5EBC681B/603F1D56" Ref="C?"  Part="1" 
AR Path="/5F05E355/603F1D56" Ref="C?"  Part="1" 
AR Path="/6068445B/603F1D56" Ref="C?"  Part="1" 
AR Path="/60E816A3/603F1D56" Ref="C?"  Part="1" 
AR Path="/5EFE605D/603F1D56" Ref="C?"  Part="1" 
AR Path="/5EFE7331/603F1D56" Ref="C?"  Part="1" 
AR Path="/5EA5E1C2/603F1D56" Ref="C152"  Part="1" 
F 0 "C152" H 1925 4100 50  0000 L CNN
F 1 "1nF" H 1975 3950 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 2125 4025 50  0001 C CNN
F 3 "~" H 2125 4025 50  0001 C CNN
F 4 "MC0402B102K160CT -  SMD Multilayer Ceramic Capacitor, 1000 pF, 16 V, 0402 [1005 Metric], ± 10%, X7R, MC Series" H 2125 3625 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402b102k160ct/cap-1000pf-16v-10-x7r-0402/dp/2320774" H 2125 3525 50  0001 C CNN "Distributor Link"
	1    2125 4025
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2125 4125 2125 4175
Wire Wire Line
	2125 3875 2125 3925
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 603F1D60
P 1800 4025
AR Path="/5E9C6910/603F1D60" Ref="C?"  Part="1" 
AR Path="/5EBC6654/603F1D60" Ref="C?"  Part="1" 
AR Path="/5EBC681B/603F1D60" Ref="C?"  Part="1" 
AR Path="/5F05E355/603F1D60" Ref="C?"  Part="1" 
AR Path="/6068445B/603F1D60" Ref="C?"  Part="1" 
AR Path="/60E816A3/603F1D60" Ref="C?"  Part="1" 
AR Path="/5EFE605D/603F1D60" Ref="C?"  Part="1" 
AR Path="/5EFE7331/603F1D60" Ref="C?"  Part="1" 
AR Path="/5EA5E1C2/603F1D60" Ref="C147"  Part="1" 
F 0 "C147" H 1600 4100 50  0000 L CNN
F 1 "10uF" H 1600 3950 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 1800 4025 50  0001 C CNN
F 3 "~" H 1800 4025 50  0001 C CNN
F 4 "LMK107BBJ106MALT -  SMD Multilayer Ceramic Capacitor, 10 µF, 10 V, 0603 [1608 Metric], ± 20%, X5R, M Series" H 1800 3625 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/taiyo-yuden/lmk107bbj106malt/cap-10-f-10v-20-x5r-0603/dp/2113070" H 1800 3525 50  0001 C CNN "Distributor Link"
	1    1800 4025
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1800 4125 1800 4175
Wire Wire Line
	1800 3875 1800 3925
Connection ~ 1800 4175
Wire Wire Line
	2450 3825 2450 3875
Wire Wire Line
	1800 4175 2125 4175
Wire Wire Line
	1800 3875 2125 3875
Connection ~ 2125 3875
Wire Wire Line
	2125 3875 2450 3875
Connection ~ 2450 3875
Wire Wire Line
	3100 4175 3100 4225
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR?
U 1 1 603F1D70
P 3100 4225
AR Path="/5EFE605D/603F1D70" Ref="#PWR?"  Part="1" 
AR Path="/5EFE7331/603F1D70" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E1C2/603F1D70" Ref="#PWR0310"  Part="1" 
F 0 "#PWR0310" H 3100 3975 50  0001 C CNN
F 1 "MOT_DGND" H 3105 4052 50  0000 C CNN
F 2 "" H 3100 4225 50  0001 C CNN
F 3 "" H 3100 4225 50  0001 C CNN
	1    3100 4225
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3425 4125 3425 4175
Wire Wire Line
	3425 3875 3425 3925
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 603F1D7A
P 3100 4025
AR Path="/5E9C6910/603F1D7A" Ref="C?"  Part="1" 
AR Path="/5EBC6654/603F1D7A" Ref="C?"  Part="1" 
AR Path="/5EBC681B/603F1D7A" Ref="C?"  Part="1" 
AR Path="/5F05E355/603F1D7A" Ref="C?"  Part="1" 
AR Path="/6068445B/603F1D7A" Ref="C?"  Part="1" 
AR Path="/60E816A3/603F1D7A" Ref="C?"  Part="1" 
AR Path="/5EFE605D/603F1D7A" Ref="C?"  Part="1" 
AR Path="/5EFE7331/603F1D7A" Ref="C?"  Part="1" 
AR Path="/5EA5E1C2/603F1D7A" Ref="C155"  Part="1" 
F 0 "C155" H 2900 4100 50  0000 L CNN
F 1 "10uF" H 2900 3950 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 3100 4025 50  0001 C CNN
F 3 "~" H 3100 4025 50  0001 C CNN
F 4 "LMK107BBJ106MALT -  SMD Multilayer Ceramic Capacitor, 10 µF, 10 V, 0603 [1608 Metric], ± 20%, X5R, M Series" H 3100 3625 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/taiyo-yuden/lmk107bbj106malt/cap-10-f-10v-20-x5r-0603/dp/2113070" H 3100 3525 50  0001 C CNN "Distributor Link"
	1    3100 4025
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3100 4125 3100 4175
Wire Wire Line
	3100 3875 3100 3925
Connection ~ 3100 4175
Wire Wire Line
	3750 3825 3750 3875
Wire Wire Line
	3100 4175 3425 4175
Wire Wire Line
	3100 3875 3425 3875
Connection ~ 3425 3875
Wire Wire Line
	3425 3875 3750 3875
Connection ~ 3750 3875
Connection ~ 2450 4700
Wire Wire Line
	3750 3875 3750 4900
Wire Wire Line
	2450 3875 2450 4700
Wire Wire Line
	1750 5600 1750 5650
Wire Wire Line
	2075 5550 2075 5600
Wire Wire Line
	2075 5300 2075 5350
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 603F1D91
P 1750 5450
AR Path="/5E9C6910/603F1D91" Ref="C?"  Part="1" 
AR Path="/5EBC6654/603F1D91" Ref="C?"  Part="1" 
AR Path="/5EBC681B/603F1D91" Ref="C?"  Part="1" 
AR Path="/5F05E355/603F1D91" Ref="C?"  Part="1" 
AR Path="/6068445B/603F1D91" Ref="C?"  Part="1" 
AR Path="/60E816A3/603F1D91" Ref="C?"  Part="1" 
AR Path="/5EFE605D/603F1D91" Ref="C?"  Part="1" 
AR Path="/5EFE7331/603F1D91" Ref="C?"  Part="1" 
AR Path="/5EA5E1C2/603F1D91" Ref="C145"  Part="1" 
F 0 "C145" H 1550 5525 50  0000 L CNN
F 1 "10uF" H 1550 5375 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 1750 5450 50  0001 C CNN
F 3 "~" H 1750 5450 50  0001 C CNN
F 4 "LMK107BBJ106MALT -  SMD Multilayer Ceramic Capacitor, 10 µF, 10 V, 0603 [1608 Metric], ± 20%, X5R, M Series" H 1750 5050 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/taiyo-yuden/lmk107bbj106malt/cap-10-f-10v-20-x5r-0603/dp/2113070" H 1750 4950 50  0001 C CNN "Distributor Link"
	1    1750 5450
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1750 5550 1750 5600
Wire Wire Line
	1750 5300 1750 5350
Connection ~ 1750 5600
Wire Wire Line
	1750 5600 2075 5600
Wire Wire Line
	1750 5300 2075 5300
Connection ~ 2450 5300
Wire Wire Line
	2450 5300 2075 5300
Connection ~ 2075 5300
Text Notes 2800 5625 0    25   ~ 0
Isolated sigma-delta modulator
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 603F1DA3
P 3425 4025
AR Path="/5E9C6910/603F1DA3" Ref="C?"  Part="1" 
AR Path="/5EBC6654/603F1DA3" Ref="C?"  Part="1" 
AR Path="/5EBC681B/603F1DA3" Ref="C?"  Part="1" 
AR Path="/5F05E355/603F1DA3" Ref="C?"  Part="1" 
AR Path="/6068445B/603F1DA3" Ref="C?"  Part="1" 
AR Path="/60E816A3/603F1DA3" Ref="C?"  Part="1" 
AR Path="/5EFE605D/603F1DA3" Ref="C?"  Part="1" 
AR Path="/5EFE7331/603F1DA3" Ref="C?"  Part="1" 
AR Path="/5EA5E1C2/603F1DA3" Ref="C157"  Part="1" 
F 0 "C157" H 3225 4100 50  0000 L CNN
F 1 "1nF" H 3275 3950 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 3425 4025 50  0001 C CNN
F 3 "~" H 3425 4025 50  0001 C CNN
F 4 "MC0402B102K160CT -  SMD Multilayer Ceramic Capacitor, 1000 pF, 16 V, 0402 [1005 Metric], ± 10%, X7R, MC Series" H 3425 3625 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402b102k160ct/cap-1000pf-16v-10-x7r-0402/dp/2320774" H 3425 3525 50  0001 C CNN "Distributor Link"
	1    3425 4025
	-1   0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 603F1DAB
P 2075 5450
AR Path="/5E9C6910/603F1DAB" Ref="C?"  Part="1" 
AR Path="/5EBC6654/603F1DAB" Ref="C?"  Part="1" 
AR Path="/5EBC681B/603F1DAB" Ref="C?"  Part="1" 
AR Path="/5F05E355/603F1DAB" Ref="C?"  Part="1" 
AR Path="/6068445B/603F1DAB" Ref="C?"  Part="1" 
AR Path="/60E816A3/603F1DAB" Ref="C?"  Part="1" 
AR Path="/5EFE605D/603F1DAB" Ref="C?"  Part="1" 
AR Path="/5EFE7331/603F1DAB" Ref="C?"  Part="1" 
AR Path="/5EA5E1C2/603F1DAB" Ref="C149"  Part="1" 
F 0 "C149" H 1875 5525 50  0000 L CNN
F 1 "1nF" H 1925 5375 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 2075 5450 50  0001 C CNN
F 3 "~" H 2075 5450 50  0001 C CNN
F 4 "MC0402B102K160CT -  SMD Multilayer Ceramic Capacitor, 1000 pF, 16 V, 0402 [1005 Metric], ± 10%, X7R, MC Series" H 2075 5050 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402b102k160ct/cap-1000pf-16v-10-x7r-0402/dp/2320774" H 2075 4950 50  0001 C CNN "Distributor Link"
	1    2075 5450
	-1   0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_5V0 #PWR?
U 1 1 603F1DCD
P 2450 3825
AR Path="/5EFE605D/603F1DCD" Ref="#PWR?"  Part="1" 
AR Path="/5EFE7331/603F1DCD" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E1C2/603F1DCD" Ref="#PWR0306"  Part="1" 
F 0 "#PWR0306" H 2450 4075 50  0001 C CNN
F 1 "MOT_5V0" H 2451 3998 50  0000 C CNN
F 2 "" H 2450 3825 50  0001 C CNN
F 3 "" H 2450 3825 50  0001 C CNN
	1    2450 3825
	1    0    0    -1  
$EndComp
Wire Notes Line
	4400 5975 600  5975
Wire Notes Line
	600  5975 600  3325
Wire Notes Line
	600  3325 4400 3325
Wire Notes Line
	4400 3325 4400 5975
Text Notes 675  3525 0    100  ~ 0
Phase B Voltage Measurement
Text Notes 700  3600 0    25   ~ 0
For measuring back EMF during identification.
Wire Wire Line
	5225 1675 5125 1675
$Comp
L PMSM_Driver_LV_Board_IC:AD7403BRIZ U?
U 1 1 6042A033
P 6975 2325
AR Path="/5EFE605D/6042A033" Ref="U?"  Part="1" 
AR Path="/5EFE7331/6042A033" Ref="U?"  Part="1" 
AR Path="/5EA5E1C2/6042A033" Ref="U40"  Part="1" 
F 0 "U40" H 6700 2825 50  0000 C CNN
F 1 "AD7403BRIZ" H 6975 1825 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:SOIC-16W_7.5x12.8mm_P1.27mm" H 6975 1675 50  0001 C CNN
F 3 "~" H 6675 2825 50  0001 C CNN
F 4 "AD7403BRIZ -  Analogue to Digital Converter, 16 bit, Differential, Serial, Single, 4.5 V" H 6975 1625 31  0001 C CNN "Description"
F 5 "https://uk.farnell.com/analog-devices/ad7403briz/adc-16bit-soic-16/dp/2431665" H 6975 1575 31  0001 C CNN "Distributor Link"
F 6 "https://www.mouser.co.uk/ProductDetail/Analog-Devices/AD7403BRIZ?qs=sGAEpiMZZMt%2Foy9k%2F5obbVdPNrLrhFiKvGld2xGDVNA%3D" H 6975 1525 31  0001 C CNN "Distributor Link 2"
	1    6975 2325
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_5V0 #PWR?
U 1 1 6042A039
P 7625 1100
AR Path="/5EFE605D/6042A039" Ref="#PWR?"  Part="1" 
AR Path="/5EFE7331/6042A039" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E1C2/6042A039" Ref="#PWR0330"  Part="1" 
F 0 "#PWR0330" H 7625 1350 50  0001 C CNN
F 1 "MOT_5V0" H 7626 1273 50  0000 C CNN
F 2 "" H 7625 1100 50  0001 C CNN
F 3 "" H 7625 1100 50  0001 C CNN
	1    7625 1100
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR?
U 1 1 6042A03F
P 7525 2775
AR Path="/5EFE605D/6042A03F" Ref="#PWR?"  Part="1" 
AR Path="/5EFE7331/6042A03F" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E1C2/6042A03F" Ref="#PWR0328"  Part="1" 
F 0 "#PWR0328" H 7525 2525 50  0001 C CNN
F 1 "MOT_DGND" H 7530 2602 50  0000 C CNN
F 2 "" H 7525 2775 50  0001 C CNN
F 3 "" H 7525 2775 50  0001 C CNN
	1    7525 2775
	1    0    0    -1  
$EndComp
Wire Wire Line
	7525 2775 7525 2675
Wire Wire Line
	7525 1975 7425 1975
Wire Wire Line
	7425 2675 7525 2675
Connection ~ 7525 2675
Wire Wire Line
	7525 2675 7525 1975
Wire Wire Line
	7625 2175 7425 2175
NoConn ~ 7425 2075
NoConn ~ 7425 2375
NoConn ~ 7425 2575
Text GLabel 7625 2275 2    50   Input ~ 0
ISO_ADC_CLK
Wire Wire Line
	7625 2275 7425 2275
Wire Wire Line
	7625 2475 7425 2475
Wire Wire Line
	5675 1450 5675 1500
Wire Wire Line
	6325 2575 6525 2575
Wire Wire Line
	6525 1975 6325 1975
Wire Wire Line
	6325 1975 6325 2575
Wire Wire Line
	6425 2800 6425 2675
Wire Wire Line
	6425 2675 6525 2675
Wire Wire Line
	6425 2675 6425 2275
Wire Wire Line
	6425 2275 6525 2275
Connection ~ 6425 2675
NoConn ~ 6525 2375
NoConn ~ 6525 2475
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 6042A05E
P 5800 2075
AR Path="/5F05E355/6042A05E" Ref="R?"  Part="1" 
AR Path="/5E9C6910/6042A05E" Ref="R?"  Part="1" 
AR Path="/5EFE605D/6042A05E" Ref="R?"  Part="1" 
AR Path="/5EFE7331/6042A05E" Ref="R?"  Part="1" 
AR Path="/5EA5E1C2/6042A05E" Ref="R171"  Part="1" 
F 0 "R171" V 5850 2225 50  0000 C CNN
F 1 "22" V 5850 1975 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 5800 2075 50  0001 C CNN
F 3 "~" H 5800 2075 50  0001 C CNN
F 4 "MCWR04X22R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 22 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 5800 1675 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x22r0ftl/res-22r-1-0-0625w-0402-thick-film/dp/2447138" H 5800 1575 50  0001 C CNN "Distributor Link"
	1    5800 2075
	0    -1   -1   0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 6042A066
P 5800 2375
AR Path="/5F05E355/6042A066" Ref="R?"  Part="1" 
AR Path="/5E9C6910/6042A066" Ref="R?"  Part="1" 
AR Path="/5EFE605D/6042A066" Ref="R?"  Part="1" 
AR Path="/5EFE7331/6042A066" Ref="R?"  Part="1" 
AR Path="/5EA5E1C2/6042A066" Ref="R172"  Part="1" 
F 0 "R172" V 5850 2525 50  0000 C CNN
F 1 "22" V 5850 2275 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 5800 2375 50  0001 C CNN
F 3 "~" H 5800 2375 50  0001 C CNN
F 4 "MCWR04X22R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 22 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 5800 1975 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x22r0ftl/res-22r-1-0-0625w-0402-thick-film/dp/2447138" H 5800 1875 50  0001 C CNN "Distributor Link"
	1    5800 2375
	0    -1   -1   0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 6042A06E
P 6000 2225
AR Path="/5E9C6910/6042A06E" Ref="C?"  Part="1" 
AR Path="/5EBC6654/6042A06E" Ref="C?"  Part="1" 
AR Path="/5EBC681B/6042A06E" Ref="C?"  Part="1" 
AR Path="/5F05E355/6042A06E" Ref="C?"  Part="1" 
AR Path="/6068445B/6042A06E" Ref="C?"  Part="1" 
AR Path="/60E816A3/6042A06E" Ref="C?"  Part="1" 
AR Path="/5EFE605D/6042A06E" Ref="C?"  Part="1" 
AR Path="/5EFE7331/6042A06E" Ref="C?"  Part="1" 
AR Path="/5EA5E1C2/6042A06E" Ref="C165"  Part="1" 
F 0 "C165" H 5800 2300 50  0000 L CNN
F 1 "47pF" H 5800 2150 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 6000 2225 50  0001 C CNN
F 3 "~" H 6000 2225 50  0001 C CNN
F 4 "MC0402N470J500CT -  SMD Multilayer Ceramic Capacitor, 47 pF, 50 V, 0402 [1005 Metric], ± 5%, C0G / NP0, MC Series " H 6000 1825 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402n470j500ct/cap-47pf-50v-5-c0g-np0-0402/dp/1758959" H 6000 1725 50  0001 C CNN "Distributor Link"
	1    6000 2225
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5900 2075 6000 2075
Wire Wire Line
	6000 2075 6000 2125
Wire Wire Line
	5900 2375 6000 2375
Wire Wire Line
	6000 2375 6000 2325
Wire Wire Line
	6000 2375 6250 2375
Wire Wire Line
	6250 2375 6250 2175
Wire Wire Line
	6250 2175 6525 2175
Connection ~ 6000 2375
Wire Wire Line
	6525 2075 6000 2075
Connection ~ 6000 2075
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 6042A080
P 6000 1300
AR Path="/5E9C6910/6042A080" Ref="C?"  Part="1" 
AR Path="/5EBC6654/6042A080" Ref="C?"  Part="1" 
AR Path="/5EBC681B/6042A080" Ref="C?"  Part="1" 
AR Path="/5F05E355/6042A080" Ref="C?"  Part="1" 
AR Path="/6068445B/6042A080" Ref="C?"  Part="1" 
AR Path="/60E816A3/6042A080" Ref="C?"  Part="1" 
AR Path="/5EFE605D/6042A080" Ref="C?"  Part="1" 
AR Path="/5EFE7331/6042A080" Ref="C?"  Part="1" 
AR Path="/5EA5E1C2/6042A080" Ref="C164"  Part="1" 
F 0 "C164" H 5800 1375 50  0000 L CNN
F 1 "1nF" H 5850 1225 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 6000 1300 50  0001 C CNN
F 3 "~" H 6000 1300 50  0001 C CNN
F 4 "MC0402B102K160CT -  SMD Multilayer Ceramic Capacitor, 1000 pF, 16 V, 0402 [1005 Metric], ± 10%, X7R, MC Series" H 6000 900 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402b102k160ct/cap-1000pf-16v-10-x7r-0402/dp/2320774" H 6000 800 50  0001 C CNN "Distributor Link"
	1    6000 1300
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6000 1400 6000 1450
Wire Wire Line
	6000 1150 6000 1200
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 6042A08A
P 5675 1300
AR Path="/5E9C6910/6042A08A" Ref="C?"  Part="1" 
AR Path="/5EBC6654/6042A08A" Ref="C?"  Part="1" 
AR Path="/5EBC681B/6042A08A" Ref="C?"  Part="1" 
AR Path="/5F05E355/6042A08A" Ref="C?"  Part="1" 
AR Path="/6068445B/6042A08A" Ref="C?"  Part="1" 
AR Path="/60E816A3/6042A08A" Ref="C?"  Part="1" 
AR Path="/5EFE605D/6042A08A" Ref="C?"  Part="1" 
AR Path="/5EFE7331/6042A08A" Ref="C?"  Part="1" 
AR Path="/5EA5E1C2/6042A08A" Ref="C160"  Part="1" 
F 0 "C160" H 5475 1375 50  0000 L CNN
F 1 "10uF" H 5475 1225 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 5675 1300 50  0001 C CNN
F 3 "~" H 5675 1300 50  0001 C CNN
F 4 "LMK107BBJ106MALT -  SMD Multilayer Ceramic Capacitor, 10 µF, 10 V, 0603 [1608 Metric], ± 20%, X5R, M Series" H 5675 900 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/taiyo-yuden/lmk107bbj106malt/cap-10-f-10v-20-x5r-0603/dp/2113070" H 5675 800 50  0001 C CNN "Distributor Link"
	1    5675 1300
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5675 1400 5675 1450
Wire Wire Line
	5675 1150 5675 1200
Connection ~ 5675 1450
Wire Wire Line
	6325 1100 6325 1150
Wire Wire Line
	5675 1450 6000 1450
Wire Wire Line
	5675 1150 6000 1150
Connection ~ 6000 1150
Wire Wire Line
	6000 1150 6325 1150
Connection ~ 6325 1150
Wire Wire Line
	6975 1450 6975 1500
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR?
U 1 1 6042A09A
P 6975 1500
AR Path="/5EFE605D/6042A09A" Ref="#PWR?"  Part="1" 
AR Path="/5EFE7331/6042A09A" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E1C2/6042A09A" Ref="#PWR0326"  Part="1" 
F 0 "#PWR0326" H 6975 1250 50  0001 C CNN
F 1 "MOT_DGND" H 6980 1327 50  0000 C CNN
F 2 "" H 6975 1500 50  0001 C CNN
F 3 "" H 6975 1500 50  0001 C CNN
	1    6975 1500
	-1   0    0    -1  
$EndComp
Wire Wire Line
	7300 1400 7300 1450
Wire Wire Line
	7300 1150 7300 1200
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 6042A0A4
P 6975 1300
AR Path="/5E9C6910/6042A0A4" Ref="C?"  Part="1" 
AR Path="/5EBC6654/6042A0A4" Ref="C?"  Part="1" 
AR Path="/5EBC681B/6042A0A4" Ref="C?"  Part="1" 
AR Path="/5F05E355/6042A0A4" Ref="C?"  Part="1" 
AR Path="/6068445B/6042A0A4" Ref="C?"  Part="1" 
AR Path="/60E816A3/6042A0A4" Ref="C?"  Part="1" 
AR Path="/5EFE605D/6042A0A4" Ref="C?"  Part="1" 
AR Path="/5EFE7331/6042A0A4" Ref="C?"  Part="1" 
AR Path="/5EA5E1C2/6042A0A4" Ref="C168"  Part="1" 
F 0 "C168" H 6775 1375 50  0000 L CNN
F 1 "10uF" H 6775 1225 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 6975 1300 50  0001 C CNN
F 3 "~" H 6975 1300 50  0001 C CNN
F 4 "LMK107BBJ106MALT -  SMD Multilayer Ceramic Capacitor, 10 µF, 10 V, 0603 [1608 Metric], ± 20%, X5R, M Series" H 6975 900 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/taiyo-yuden/lmk107bbj106malt/cap-10-f-10v-20-x5r-0603/dp/2113070" H 6975 800 50  0001 C CNN "Distributor Link"
	1    6975 1300
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6975 1400 6975 1450
Wire Wire Line
	6975 1150 6975 1200
Connection ~ 6975 1450
Wire Wire Line
	7625 1100 7625 1150
Wire Wire Line
	6975 1450 7300 1450
Wire Wire Line
	6975 1150 7300 1150
Connection ~ 7300 1150
Wire Wire Line
	7300 1150 7625 1150
Connection ~ 7625 1150
Connection ~ 6325 1975
Wire Wire Line
	7625 1150 7625 2175
Wire Wire Line
	6325 1150 6325 1975
Wire Wire Line
	5625 2875 5625 2925
Wire Wire Line
	5950 2825 5950 2875
Wire Wire Line
	5950 2575 5950 2625
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 6042A0BB
P 5625 2725
AR Path="/5E9C6910/6042A0BB" Ref="C?"  Part="1" 
AR Path="/5EBC6654/6042A0BB" Ref="C?"  Part="1" 
AR Path="/5EBC681B/6042A0BB" Ref="C?"  Part="1" 
AR Path="/5F05E355/6042A0BB" Ref="C?"  Part="1" 
AR Path="/6068445B/6042A0BB" Ref="C?"  Part="1" 
AR Path="/60E816A3/6042A0BB" Ref="C?"  Part="1" 
AR Path="/5EFE605D/6042A0BB" Ref="C?"  Part="1" 
AR Path="/5EFE7331/6042A0BB" Ref="C?"  Part="1" 
AR Path="/5EA5E1C2/6042A0BB" Ref="C158"  Part="1" 
F 0 "C158" H 5425 2800 50  0000 L CNN
F 1 "10uF" H 5425 2650 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 5625 2725 50  0001 C CNN
F 3 "~" H 5625 2725 50  0001 C CNN
F 4 "LMK107BBJ106MALT -  SMD Multilayer Ceramic Capacitor, 10 µF, 10 V, 0603 [1608 Metric], ± 20%, X5R, M Series" H 5625 2325 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/taiyo-yuden/lmk107bbj106malt/cap-10-f-10v-20-x5r-0603/dp/2113070" H 5625 2225 50  0001 C CNN "Distributor Link"
	1    5625 2725
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5625 2825 5625 2875
Wire Wire Line
	5625 2575 5625 2625
Connection ~ 5625 2875
Wire Wire Line
	5625 2875 5950 2875
Wire Wire Line
	5625 2575 5950 2575
Connection ~ 6325 2575
Wire Wire Line
	6325 2575 5950 2575
Connection ~ 5950 2575
Text Notes 6675 2900 0    25   ~ 0
Isolated sigma-delta modulator
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 6042A0CD
P 7300 1300
AR Path="/5E9C6910/6042A0CD" Ref="C?"  Part="1" 
AR Path="/5EBC6654/6042A0CD" Ref="C?"  Part="1" 
AR Path="/5EBC681B/6042A0CD" Ref="C?"  Part="1" 
AR Path="/5F05E355/6042A0CD" Ref="C?"  Part="1" 
AR Path="/6068445B/6042A0CD" Ref="C?"  Part="1" 
AR Path="/60E816A3/6042A0CD" Ref="C?"  Part="1" 
AR Path="/5EFE605D/6042A0CD" Ref="C?"  Part="1" 
AR Path="/5EFE7331/6042A0CD" Ref="C?"  Part="1" 
AR Path="/5EA5E1C2/6042A0CD" Ref="C170"  Part="1" 
F 0 "C170" H 7100 1375 50  0000 L CNN
F 1 "1nF" H 7150 1225 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 7300 1300 50  0001 C CNN
F 3 "~" H 7300 1300 50  0001 C CNN
F 4 "MC0402B102K160CT -  SMD Multilayer Ceramic Capacitor, 1000 pF, 16 V, 0402 [1005 Metric], ± 10%, X7R, MC Series" H 7300 900 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402b102k160ct/cap-1000pf-16v-10-x7r-0402/dp/2320774" H 7300 800 50  0001 C CNN "Distributor Link"
	1    7300 1300
	-1   0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 6042A0D5
P 5950 2725
AR Path="/5E9C6910/6042A0D5" Ref="C?"  Part="1" 
AR Path="/5EBC6654/6042A0D5" Ref="C?"  Part="1" 
AR Path="/5EBC681B/6042A0D5" Ref="C?"  Part="1" 
AR Path="/5F05E355/6042A0D5" Ref="C?"  Part="1" 
AR Path="/6068445B/6042A0D5" Ref="C?"  Part="1" 
AR Path="/60E816A3/6042A0D5" Ref="C?"  Part="1" 
AR Path="/5EFE605D/6042A0D5" Ref="C?"  Part="1" 
AR Path="/5EFE7331/6042A0D5" Ref="C?"  Part="1" 
AR Path="/5EA5E1C2/6042A0D5" Ref="C162"  Part="1" 
F 0 "C162" H 5750 2800 50  0000 L CNN
F 1 "1nF" H 5800 2650 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 5950 2725 50  0001 C CNN
F 3 "~" H 5950 2725 50  0001 C CNN
F 4 "MC0402B102K160CT -  SMD Multilayer Ceramic Capacitor, 1000 pF, 16 V, 0402 [1005 Metric], ± 10%, X7R, MC Series" H 5950 2325 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402b102k160ct/cap-1000pf-16v-10-x7r-0402/dp/2320774" H 5950 2225 50  0001 C CNN "Distributor Link"
	1    5950 2725
	-1   0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_5V0 #PWR?
U 1 1 6042A0F6
P 6325 1100
AR Path="/5EFE605D/6042A0F6" Ref="#PWR?"  Part="1" 
AR Path="/5EFE7331/6042A0F6" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E1C2/6042A0F6" Ref="#PWR0322"  Part="1" 
F 0 "#PWR0322" H 6325 1350 50  0001 C CNN
F 1 "MOT_5V0" H 6326 1273 50  0000 C CNN
F 2 "" H 6325 1100 50  0001 C CNN
F 3 "" H 6325 1100 50  0001 C CNN
	1    6325 1100
	1    0    0    -1  
$EndComp
Wire Notes Line
	8275 3250 4475 3250
Wire Notes Line
	4475 3250 4475 600 
Wire Notes Line
	4475 600  8275 600 
Wire Notes Line
	8275 600  8275 3250
Text Notes 4550 800  0    100  ~ 0
Phase C Voltage Measurement
Text Notes 4575 875  0    25   ~ 0
For measuring back EMF during identification.
Wire Wire Line
	5225 4500 5225 4400
$Comp
L PMSM_Driver_LV_Board_IC:AD7403BRIZ U?
U 1 1 604D1F7D
P 6975 5050
AR Path="/5EFE605D/604D1F7D" Ref="U?"  Part="1" 
AR Path="/5EFE7331/604D1F7D" Ref="U?"  Part="1" 
AR Path="/5EA5E1C2/604D1F7D" Ref="U41"  Part="1" 
F 0 "U41" H 6700 5550 50  0000 C CNN
F 1 "AD7403BRIZ" H 6975 4550 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:SOIC-16W_7.5x12.8mm_P1.27mm" H 6975 4400 50  0001 C CNN
F 3 "~" H 6675 5550 50  0001 C CNN
F 4 "AD7403BRIZ -  Analogue to Digital Converter, 16 bit, Differential, Serial, Single, 4.5 V" H 6975 4350 31  0001 C CNN "Description"
F 5 "https://uk.farnell.com/analog-devices/ad7403briz/adc-16bit-soic-16/dp/2431665" H 6975 4300 31  0001 C CNN "Distributor Link"
F 6 "https://www.mouser.co.uk/ProductDetail/Analog-Devices/AD7403BRIZ?qs=sGAEpiMZZMt%2Foy9k%2F5obbVdPNrLrhFiKvGld2xGDVNA%3D" H 6975 4250 31  0001 C CNN "Distributor Link 2"
	1    6975 5050
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_5V0 #PWR?
U 1 1 604D1F83
P 7625 3825
AR Path="/5EFE605D/604D1F83" Ref="#PWR?"  Part="1" 
AR Path="/5EFE7331/604D1F83" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E1C2/604D1F83" Ref="#PWR0331"  Part="1" 
F 0 "#PWR0331" H 7625 4075 50  0001 C CNN
F 1 "MOT_5V0" H 7626 3998 50  0000 C CNN
F 2 "" H 7625 3825 50  0001 C CNN
F 3 "" H 7625 3825 50  0001 C CNN
	1    7625 3825
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR?
U 1 1 604D1F89
P 7525 5500
AR Path="/5EFE605D/604D1F89" Ref="#PWR?"  Part="1" 
AR Path="/5EFE7331/604D1F89" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E1C2/604D1F89" Ref="#PWR0329"  Part="1" 
F 0 "#PWR0329" H 7525 5250 50  0001 C CNN
F 1 "MOT_DGND" H 7530 5327 50  0000 C CNN
F 2 "" H 7525 5500 50  0001 C CNN
F 3 "" H 7525 5500 50  0001 C CNN
	1    7525 5500
	1    0    0    -1  
$EndComp
Wire Wire Line
	7525 5500 7525 5400
Wire Wire Line
	7525 4700 7425 4700
Wire Wire Line
	7425 5400 7525 5400
Connection ~ 7525 5400
Wire Wire Line
	7525 5400 7525 4700
Wire Wire Line
	7625 4900 7425 4900
NoConn ~ 7425 4800
NoConn ~ 7425 5100
NoConn ~ 7425 5300
Text GLabel 7625 5000 2    50   Input ~ 0
ISO_ADC_CLK
Wire Wire Line
	7625 5000 7425 5000
Wire Wire Line
	7625 5200 7425 5200
Wire Wire Line
	5675 4175 5675 4225
Wire Wire Line
	6325 5300 6525 5300
Wire Wire Line
	6525 4700 6325 4700
Wire Wire Line
	6325 4700 6325 5300
Wire Wire Line
	6425 5525 6425 5400
Wire Wire Line
	6425 5400 6525 5400
Wire Wire Line
	6425 5400 6425 5000
Wire Wire Line
	6425 5000 6525 5000
Connection ~ 6425 5400
NoConn ~ 6525 5100
NoConn ~ 6525 5200
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 604D1FA8
P 5800 4800
AR Path="/5F05E355/604D1FA8" Ref="R?"  Part="1" 
AR Path="/5E9C6910/604D1FA8" Ref="R?"  Part="1" 
AR Path="/5EFE605D/604D1FA8" Ref="R?"  Part="1" 
AR Path="/5EFE7331/604D1FA8" Ref="R?"  Part="1" 
AR Path="/5EA5E1C2/604D1FA8" Ref="R173"  Part="1" 
F 0 "R173" V 5850 4950 50  0000 C CNN
F 1 "22" V 5850 4700 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 5800 4800 50  0001 C CNN
F 3 "~" H 5800 4800 50  0001 C CNN
F 4 "MCWR04X22R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 22 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 5800 4400 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x22r0ftl/res-22r-1-0-0625w-0402-thick-film/dp/2447138" H 5800 4300 50  0001 C CNN "Distributor Link"
	1    5800 4800
	0    -1   -1   0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 604D1FB0
P 5800 5100
AR Path="/5F05E355/604D1FB0" Ref="R?"  Part="1" 
AR Path="/5E9C6910/604D1FB0" Ref="R?"  Part="1" 
AR Path="/5EFE605D/604D1FB0" Ref="R?"  Part="1" 
AR Path="/5EFE7331/604D1FB0" Ref="R?"  Part="1" 
AR Path="/5EA5E1C2/604D1FB0" Ref="R174"  Part="1" 
F 0 "R174" V 5850 5250 50  0000 C CNN
F 1 "22" V 5850 5000 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 5800 5100 50  0001 C CNN
F 3 "~" H 5800 5100 50  0001 C CNN
F 4 "MCWR04X22R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 22 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 5800 4700 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x22r0ftl/res-22r-1-0-0625w-0402-thick-film/dp/2447138" H 5800 4600 50  0001 C CNN "Distributor Link"
	1    5800 5100
	0    -1   -1   0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 604D1FB8
P 6000 4950
AR Path="/5E9C6910/604D1FB8" Ref="C?"  Part="1" 
AR Path="/5EBC6654/604D1FB8" Ref="C?"  Part="1" 
AR Path="/5EBC681B/604D1FB8" Ref="C?"  Part="1" 
AR Path="/5F05E355/604D1FB8" Ref="C?"  Part="1" 
AR Path="/6068445B/604D1FB8" Ref="C?"  Part="1" 
AR Path="/60E816A3/604D1FB8" Ref="C?"  Part="1" 
AR Path="/5EFE605D/604D1FB8" Ref="C?"  Part="1" 
AR Path="/5EFE7331/604D1FB8" Ref="C?"  Part="1" 
AR Path="/5EA5E1C2/604D1FB8" Ref="C167"  Part="1" 
F 0 "C167" H 5800 5025 50  0000 L CNN
F 1 "47pF" H 5800 4875 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 6000 4950 50  0001 C CNN
F 3 "~" H 6000 4950 50  0001 C CNN
F 4 "MC0402N470J500CT -  SMD Multilayer Ceramic Capacitor, 47 pF, 50 V, 0402 [1005 Metric], ± 5%, C0G / NP0, MC Series " H 6000 4550 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402n470j500ct/cap-47pf-50v-5-c0g-np0-0402/dp/1758959" H 6000 4450 50  0001 C CNN "Distributor Link"
	1    6000 4950
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5900 4800 6000 4800
Wire Wire Line
	6000 4800 6000 4850
Wire Wire Line
	5900 5100 6000 5100
Wire Wire Line
	6000 5100 6000 5050
Wire Wire Line
	6000 5100 6250 5100
Wire Wire Line
	6250 5100 6250 4900
Wire Wire Line
	6250 4900 6525 4900
Connection ~ 6000 5100
Wire Wire Line
	6525 4800 6000 4800
Connection ~ 6000 4800
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 604D1FCA
P 6000 4025
AR Path="/5E9C6910/604D1FCA" Ref="C?"  Part="1" 
AR Path="/5EBC6654/604D1FCA" Ref="C?"  Part="1" 
AR Path="/5EBC681B/604D1FCA" Ref="C?"  Part="1" 
AR Path="/5F05E355/604D1FCA" Ref="C?"  Part="1" 
AR Path="/6068445B/604D1FCA" Ref="C?"  Part="1" 
AR Path="/60E816A3/604D1FCA" Ref="C?"  Part="1" 
AR Path="/5EFE605D/604D1FCA" Ref="C?"  Part="1" 
AR Path="/5EFE7331/604D1FCA" Ref="C?"  Part="1" 
AR Path="/5EA5E1C2/604D1FCA" Ref="C166"  Part="1" 
F 0 "C166" H 5800 4100 50  0000 L CNN
F 1 "1nF" H 5850 3950 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 6000 4025 50  0001 C CNN
F 3 "~" H 6000 4025 50  0001 C CNN
F 4 "MC0402B102K160CT -  SMD Multilayer Ceramic Capacitor, 1000 pF, 16 V, 0402 [1005 Metric], ± 10%, X7R, MC Series" H 6000 3625 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402b102k160ct/cap-1000pf-16v-10-x7r-0402/dp/2320774" H 6000 3525 50  0001 C CNN "Distributor Link"
	1    6000 4025
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6000 4125 6000 4175
Wire Wire Line
	6000 3875 6000 3925
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 604D1FD4
P 5675 4025
AR Path="/5E9C6910/604D1FD4" Ref="C?"  Part="1" 
AR Path="/5EBC6654/604D1FD4" Ref="C?"  Part="1" 
AR Path="/5EBC681B/604D1FD4" Ref="C?"  Part="1" 
AR Path="/5F05E355/604D1FD4" Ref="C?"  Part="1" 
AR Path="/6068445B/604D1FD4" Ref="C?"  Part="1" 
AR Path="/60E816A3/604D1FD4" Ref="C?"  Part="1" 
AR Path="/5EFE605D/604D1FD4" Ref="C?"  Part="1" 
AR Path="/5EFE7331/604D1FD4" Ref="C?"  Part="1" 
AR Path="/5EA5E1C2/604D1FD4" Ref="C161"  Part="1" 
F 0 "C161" H 5475 4100 50  0000 L CNN
F 1 "10uF" H 5475 3950 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 5675 4025 50  0001 C CNN
F 3 "~" H 5675 4025 50  0001 C CNN
F 4 "LMK107BBJ106MALT -  SMD Multilayer Ceramic Capacitor, 10 µF, 10 V, 0603 [1608 Metric], ± 20%, X5R, M Series" H 5675 3625 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/taiyo-yuden/lmk107bbj106malt/cap-10-f-10v-20-x5r-0603/dp/2113070" H 5675 3525 50  0001 C CNN "Distributor Link"
	1    5675 4025
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5675 4125 5675 4175
Wire Wire Line
	5675 3875 5675 3925
Connection ~ 5675 4175
Wire Wire Line
	6325 3825 6325 3875
Wire Wire Line
	5675 4175 6000 4175
Wire Wire Line
	5675 3875 6000 3875
Connection ~ 6000 3875
Wire Wire Line
	6000 3875 6325 3875
Connection ~ 6325 3875
Wire Wire Line
	6975 4175 6975 4225
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR?
U 1 1 604D1FE4
P 6975 4225
AR Path="/5EFE605D/604D1FE4" Ref="#PWR?"  Part="1" 
AR Path="/5EFE7331/604D1FE4" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E1C2/604D1FE4" Ref="#PWR0327"  Part="1" 
F 0 "#PWR0327" H 6975 3975 50  0001 C CNN
F 1 "MOT_DGND" H 6980 4052 50  0000 C CNN
F 2 "" H 6975 4225 50  0001 C CNN
F 3 "" H 6975 4225 50  0001 C CNN
	1    6975 4225
	-1   0    0    -1  
$EndComp
Wire Wire Line
	7300 4125 7300 4175
Wire Wire Line
	7300 3875 7300 3925
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 604D1FEE
P 6975 4025
AR Path="/5E9C6910/604D1FEE" Ref="C?"  Part="1" 
AR Path="/5EBC6654/604D1FEE" Ref="C?"  Part="1" 
AR Path="/5EBC681B/604D1FEE" Ref="C?"  Part="1" 
AR Path="/5F05E355/604D1FEE" Ref="C?"  Part="1" 
AR Path="/6068445B/604D1FEE" Ref="C?"  Part="1" 
AR Path="/60E816A3/604D1FEE" Ref="C?"  Part="1" 
AR Path="/5EFE605D/604D1FEE" Ref="C?"  Part="1" 
AR Path="/5EFE7331/604D1FEE" Ref="C?"  Part="1" 
AR Path="/5EA5E1C2/604D1FEE" Ref="C169"  Part="1" 
F 0 "C169" H 6775 4100 50  0000 L CNN
F 1 "10uF" H 6775 3950 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 6975 4025 50  0001 C CNN
F 3 "~" H 6975 4025 50  0001 C CNN
F 4 "LMK107BBJ106MALT -  SMD Multilayer Ceramic Capacitor, 10 µF, 10 V, 0603 [1608 Metric], ± 20%, X5R, M Series" H 6975 3625 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/taiyo-yuden/lmk107bbj106malt/cap-10-f-10v-20-x5r-0603/dp/2113070" H 6975 3525 50  0001 C CNN "Distributor Link"
	1    6975 4025
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6975 4125 6975 4175
Wire Wire Line
	6975 3875 6975 3925
Connection ~ 6975 4175
Wire Wire Line
	7625 3825 7625 3875
Wire Wire Line
	6975 4175 7300 4175
Wire Wire Line
	6975 3875 7300 3875
Connection ~ 7300 3875
Wire Wire Line
	7300 3875 7625 3875
Connection ~ 7625 3875
Connection ~ 6325 4700
Wire Wire Line
	7625 3875 7625 4900
Wire Wire Line
	6325 3875 6325 4700
Wire Wire Line
	5625 5600 5625 5650
Wire Wire Line
	5950 5550 5950 5600
Wire Wire Line
	5950 5300 5950 5350
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 604D2005
P 5625 5450
AR Path="/5E9C6910/604D2005" Ref="C?"  Part="1" 
AR Path="/5EBC6654/604D2005" Ref="C?"  Part="1" 
AR Path="/5EBC681B/604D2005" Ref="C?"  Part="1" 
AR Path="/5F05E355/604D2005" Ref="C?"  Part="1" 
AR Path="/6068445B/604D2005" Ref="C?"  Part="1" 
AR Path="/60E816A3/604D2005" Ref="C?"  Part="1" 
AR Path="/5EFE605D/604D2005" Ref="C?"  Part="1" 
AR Path="/5EFE7331/604D2005" Ref="C?"  Part="1" 
AR Path="/5EA5E1C2/604D2005" Ref="C159"  Part="1" 
F 0 "C159" H 5425 5525 50  0000 L CNN
F 1 "10uF" H 5425 5375 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 5625 5450 50  0001 C CNN
F 3 "~" H 5625 5450 50  0001 C CNN
F 4 "LMK107BBJ106MALT -  SMD Multilayer Ceramic Capacitor, 10 µF, 10 V, 0603 [1608 Metric], ± 20%, X5R, M Series" H 5625 5050 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/taiyo-yuden/lmk107bbj106malt/cap-10-f-10v-20-x5r-0603/dp/2113070" H 5625 4950 50  0001 C CNN "Distributor Link"
	1    5625 5450
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5625 5550 5625 5600
Wire Wire Line
	5625 5300 5625 5350
Connection ~ 5625 5600
Wire Wire Line
	5625 5600 5950 5600
Wire Wire Line
	5625 5300 5950 5300
Wire Wire Line
	5225 5100 5700 5100
Connection ~ 6325 5300
Wire Wire Line
	6325 5300 5950 5300
Connection ~ 5950 5300
Text Notes 6675 5625 0    25   ~ 0
Isolated sigma-delta modulator
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 604D2017
P 7300 4025
AR Path="/5E9C6910/604D2017" Ref="C?"  Part="1" 
AR Path="/5EBC6654/604D2017" Ref="C?"  Part="1" 
AR Path="/5EBC681B/604D2017" Ref="C?"  Part="1" 
AR Path="/5F05E355/604D2017" Ref="C?"  Part="1" 
AR Path="/6068445B/604D2017" Ref="C?"  Part="1" 
AR Path="/60E816A3/604D2017" Ref="C?"  Part="1" 
AR Path="/5EFE605D/604D2017" Ref="C?"  Part="1" 
AR Path="/5EFE7331/604D2017" Ref="C?"  Part="1" 
AR Path="/5EA5E1C2/604D2017" Ref="C171"  Part="1" 
F 0 "C171" H 7100 4100 50  0000 L CNN
F 1 "1nF" H 7150 3950 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 7300 4025 50  0001 C CNN
F 3 "~" H 7300 4025 50  0001 C CNN
F 4 "MC0402B102K160CT -  SMD Multilayer Ceramic Capacitor, 1000 pF, 16 V, 0402 [1005 Metric], ± 10%, X7R, MC Series" H 7300 3625 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402b102k160ct/cap-1000pf-16v-10-x7r-0402/dp/2320774" H 7300 3525 50  0001 C CNN "Distributor Link"
	1    7300 4025
	-1   0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 604D201F
P 5950 5450
AR Path="/5E9C6910/604D201F" Ref="C?"  Part="1" 
AR Path="/5EBC6654/604D201F" Ref="C?"  Part="1" 
AR Path="/5EBC681B/604D201F" Ref="C?"  Part="1" 
AR Path="/5F05E355/604D201F" Ref="C?"  Part="1" 
AR Path="/6068445B/604D201F" Ref="C?"  Part="1" 
AR Path="/60E816A3/604D201F" Ref="C?"  Part="1" 
AR Path="/5EFE605D/604D201F" Ref="C?"  Part="1" 
AR Path="/5EFE7331/604D201F" Ref="C?"  Part="1" 
AR Path="/5EA5E1C2/604D201F" Ref="C163"  Part="1" 
F 0 "C163" H 5750 5525 50  0000 L CNN
F 1 "1nF" H 5800 5375 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 5950 5450 50  0001 C CNN
F 3 "~" H 5950 5450 50  0001 C CNN
F 4 "MC0402B102K160CT -  SMD Multilayer Ceramic Capacitor, 1000 pF, 16 V, 0402 [1005 Metric], ± 10%, X7R, MC Series" H 5950 5050 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402b102k160ct/cap-1000pf-16v-10-x7r-0402/dp/2320774" H 5950 4950 50  0001 C CNN "Distributor Link"
	1    5950 5450
	-1   0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_PGND #PWR0317
U 1 1 604D2025
P 5225 5200
F 0 "#PWR0317" H 5225 4950 50  0001 C CNN
F 1 "MOT_PGND" H 5230 5027 50  0000 C CNN
F 2 "" H 5225 5200 50  0001 C CNN
F 3 "" H 5225 5200 50  0001 C CNN
	1    5225 5200
	1    0    0    -1  
$EndComp
Wire Wire Line
	5225 5100 5225 5050
Wire Wire Line
	5225 4850 5225 4800
Wire Wire Line
	5225 4800 5700 4800
$Comp
L PMSM_Driver_LV_Board_Power:MOT_5V0 #PWR?
U 1 1 604D2040
P 6325 3825
AR Path="/5EFE605D/604D2040" Ref="#PWR?"  Part="1" 
AR Path="/5EFE7331/604D2040" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E1C2/604D2040" Ref="#PWR0323"  Part="1" 
F 0 "#PWR0323" H 6325 4075 50  0001 C CNN
F 1 "MOT_5V0" H 6326 3998 50  0000 C CNN
F 2 "" H 6325 3825 50  0001 C CNN
F 3 "" H 6325 3825 50  0001 C CNN
	1    6325 3825
	1    0    0    -1  
$EndComp
Wire Notes Line
	8275 5975 4475 5975
Wire Notes Line
	4475 5975 4475 3325
Wire Notes Line
	4475 3325 8275 3325
Wire Notes Line
	8275 3325 8275 5975
Text Notes 4550 3525 0    100  ~ 0
Power Supply Voltage Measurement
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 604D2050
P 5225 4950
AR Path="/5F05E355/604D2050" Ref="R?"  Part="1" 
AR Path="/5E9C6910/604D2050" Ref="R?"  Part="1" 
AR Path="/5EFE605D/604D2050" Ref="R?"  Part="1" 
AR Path="/5EFE7331/604D2050" Ref="R?"  Part="1" 
AR Path="/5EA5E1C2/604D2050" Ref="R170"  Part="1" 
F 0 "R170" H 5375 5000 50  0000 C CNN
F 1 "1k27" H 5375 4925 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5225 4950 50  0001 C CNN
F 3 "~" H 5225 4950 50  0001 C CNN
F 4 "WF08U1271BTL -  SMD Chip Resistor, 0805 [2012 Metric], 1.27 kohm, WF08U Series, 100 V, Thin Film, 100 mW" H 5225 4550 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/walsin/wf08u1271btl/res-1k27-0-1-0-1w-0805-thin-film/dp/2670133" H 5225 4450 50  0001 C CNN "Distributor Link"
F 6 "0.1%" H 5350 4850 50  0000 C CNN "Tolerance"
	1    5225 4950
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 604D2059
P 5225 4600
AR Path="/5F05E355/604D2059" Ref="R?"  Part="1" 
AR Path="/5E9C6910/604D2059" Ref="R?"  Part="1" 
AR Path="/5EFE605D/604D2059" Ref="R?"  Part="1" 
AR Path="/5EFE7331/604D2059" Ref="R?"  Part="1" 
AR Path="/5EA5E1C2/604D2059" Ref="R169"  Part="1" 
F 0 "R169" H 5375 4650 50  0000 C CNN
F 1 "249k" H 5375 4575 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5225 4600 50  0001 C CNN
F 3 "~" H 5225 4600 50  0001 C CNN
F 4 "WF08U2493BTL -  SMD Chip Resistor, 0805 [2012 Metric], 249 kohm, WF08U Series, 100 V, Thin Film, 100 mW" H 5225 4200 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/walsin/wf08u2493btl/res-249k-0-1-0-1w-0805-thin-film/dp/2670208" H 5225 4100 50  0001 C CNN "Distributor Link"
F 6 "0.1%" H 5375 4500 50  0000 C CNN "Tolerance"
	1    5225 4600
	1    0    0    -1  
$EndComp
Connection ~ 5225 5100
Connection ~ 5225 4800
Wire Wire Line
	5225 4700 5225 4800
Wire Wire Line
	5225 5100 5225 5200
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 6054CA59
P 950 2475
AR Path="/5F05E355/6054CA59" Ref="R?"  Part="1" 
AR Path="/5E9C6910/6054CA59" Ref="R?"  Part="1" 
AR Path="/5EFE605D/6054CA59" Ref="R?"  Part="1" 
AR Path="/5EFE7331/6054CA59" Ref="R?"  Part="1" 
AR Path="/5EA5E1C2/6054CA59" Ref="R156"  Part="1" 
F 0 "R156" H 1100 2525 50  0000 C CNN
F 1 "0" H 1025 2450 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 950 2475 50  0001 C CNN
F 3 "~" H 950 2475 50  0001 C CNN
F 4 "..." H 950 2075 50  0001 C CNN "Description"
F 5 "..." H 950 1975 50  0001 C CNN "Distributor Link"
F 6 "DNI" H 850 2375 50  0000 C CNN "DNI"
	1    950  2475
	1    0    0    -1  
$EndComp
Wire Wire Line
	950  2375 950  2325
Wire Wire Line
	950  2575 950  2625
Wire Wire Line
	950  2625 1350 2625
Wire Wire Line
	1350 2375 1350 2625
Connection ~ 1350 2625
Wire Wire Line
	1350 4500 1350 4400
Wire Wire Line
	1350 5100 1825 5100
$Comp
L PMSM_Driver_LV_Board_Power:MOT_PGND #PWR0300
U 1 1 605F0EC4
P 1350 5400
F 0 "#PWR0300" H 1350 5150 50  0001 C CNN
F 1 "MOT_PGND" H 1355 5227 50  0000 C CNN
F 2 "" H 1350 5400 50  0001 C CNN
F 3 "" H 1350 5400 50  0001 C CNN
	1    1350 5400
	1    0    0    -1  
$EndComp
Wire Wire Line
	1350 5100 1350 5050
Wire Wire Line
	1350 4850 1350 4800
Wire Wire Line
	1350 4800 1825 4800
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 605F0ED0
P 1350 4950
AR Path="/5F05E355/605F0ED0" Ref="R?"  Part="1" 
AR Path="/5E9C6910/605F0ED0" Ref="R?"  Part="1" 
AR Path="/5EFE605D/605F0ED0" Ref="R?"  Part="1" 
AR Path="/5EFE7331/605F0ED0" Ref="R?"  Part="1" 
AR Path="/5EA5E1C2/605F0ED0" Ref="R161"  Part="1" 
F 0 "R161" H 1500 5000 50  0000 C CNN
F 1 "1k27" H 1500 4925 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 1350 4950 50  0001 C CNN
F 3 "~" H 1350 4950 50  0001 C CNN
F 4 "WF08U1271BTL -  SMD Chip Resistor, 0805 [2012 Metric], 1.27 kohm, WF08U Series, 100 V, Thin Film, 100 mW" H 1350 4550 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/walsin/wf08u1271btl/res-1k27-0-1-0-1w-0805-thin-film/dp/2670133" H 1350 4450 50  0001 C CNN "Distributor Link"
F 6 "0.1%" H 1475 4850 50  0000 C CNN "Tolerance"
	1    1350 4950
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 605F0ED9
P 1350 4600
AR Path="/5F05E355/605F0ED9" Ref="R?"  Part="1" 
AR Path="/5E9C6910/605F0ED9" Ref="R?"  Part="1" 
AR Path="/5EFE605D/605F0ED9" Ref="R?"  Part="1" 
AR Path="/5EFE7331/605F0ED9" Ref="R?"  Part="1" 
AR Path="/5EA5E1C2/605F0ED9" Ref="R160"  Part="1" 
F 0 "R160" H 1500 4650 50  0000 C CNN
F 1 "100k" H 1500 4575 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 1350 4600 50  0001 C CNN
F 3 "~" H 1350 4600 50  0001 C CNN
F 4 "MCWF08U1003BTL -  SMD Chip Resistor, 0805 [2012 Metric], 100 kohm, MCWF08U Series, 100 V, Thin Film, 100 mW" H 1350 4200 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwf08u1003btl/res-100k-0-1-0-1w-0805-thin-film/dp/2694161" H 1350 4100 50  0001 C CNN "Distributor Link"
F 6 "0.1%" H 1500 4500 50  0000 C CNN "Tolerance"
	1    1350 4600
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Connector:Jumper_NC_Small JP?
U 1 1 605F0EE1
P 950 4950
AR Path="/6068445B/605F0EE1" Ref="JP?"  Part="1" 
AR Path="/60E816A3/605F0EE1" Ref="JP?"  Part="1" 
AR Path="/60EB5E32/605F0EE1" Ref="JP?"  Part="1" 
AR Path="/5EFE7331/605F0EE1" Ref="JP?"  Part="1" 
AR Path="/5EA5E1C2/605F0EE1" Ref="JP8"  Part="1" 
F 0 "JP8" V 975 4925 50  0000 R CNN
F 1 "JUMPER" V 900 4925 50  0000 R CNN
F 2 "PMSM_Driver_LV_Board:PinHeader_1x02_P2.54mm_Vertical" H 950 4800 50  0001 C CNN
F 3 "~" H 950 4950 50  0001 C CNN
F 4 "2211S-02G -  Board-To-Board Connector, 2.54 mm, 2 Contacts, Header, 2211S Series, Through Hole, 1 Rows" H 950 4700 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/2211s-02g/header-1-row-vert-2way/dp/1593411" H 950 4600 50  0001 C CNN "Distributor Link"
F 6 "DNI" V 875 5050 50  0000 C CNN "DNI"
	1    950  4950
	0    -1   -1   0   
$EndComp
Connection ~ 1350 5100
Wire Wire Line
	1350 4700 1350 4750
Connection ~ 1350 4800
Wire Wire Line
	950  4850 950  4750
Wire Wire Line
	950  4750 1350 4750
Connection ~ 1350 4750
Wire Wire Line
	1350 4750 1350 4800
Wire Wire Line
	1350 5350 1350 5400
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 605F0EF1
P 950 5200
AR Path="/5F05E355/605F0EF1" Ref="R?"  Part="1" 
AR Path="/5E9C6910/605F0EF1" Ref="R?"  Part="1" 
AR Path="/5EFE605D/605F0EF1" Ref="R?"  Part="1" 
AR Path="/5EFE7331/605F0EF1" Ref="R?"  Part="1" 
AR Path="/5EA5E1C2/605F0EF1" Ref="R157"  Part="1" 
F 0 "R157" H 1100 5250 50  0000 C CNN
F 1 "0" H 1025 5175 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 950 5200 50  0001 C CNN
F 3 "~" H 950 5200 50  0001 C CNN
F 4 "..." H 950 4800 50  0001 C CNN "Description"
F 5 "..." H 950 4700 50  0001 C CNN "Distributor Link"
F 6 "DNI" H 850 5100 50  0000 C CNN "DNI"
	1    950  5200
	1    0    0    -1  
$EndComp
Wire Wire Line
	950  5100 950  5050
Wire Wire Line
	950  5300 950  5350
Wire Wire Line
	950  5350 1350 5350
Wire Wire Line
	1350 5100 1350 5350
Connection ~ 1350 5350
Wire Wire Line
	5225 1775 5225 1675
Wire Wire Line
	5225 2375 5700 2375
$Comp
L PMSM_Driver_LV_Board_Power:MOT_PGND #PWR0315
U 1 1 6061FEA1
P 5225 2675
F 0 "#PWR0315" H 5225 2425 50  0001 C CNN
F 1 "MOT_PGND" H 5230 2502 50  0000 C CNN
F 2 "" H 5225 2675 50  0001 C CNN
F 3 "" H 5225 2675 50  0001 C CNN
	1    5225 2675
	1    0    0    -1  
$EndComp
Wire Wire Line
	5225 2375 5225 2325
Wire Wire Line
	5225 2125 5225 2075
Wire Wire Line
	5225 2075 5700 2075
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 6061FEAD
P 5225 2225
AR Path="/5F05E355/6061FEAD" Ref="R?"  Part="1" 
AR Path="/5E9C6910/6061FEAD" Ref="R?"  Part="1" 
AR Path="/5EFE605D/6061FEAD" Ref="R?"  Part="1" 
AR Path="/5EFE7331/6061FEAD" Ref="R?"  Part="1" 
AR Path="/5EA5E1C2/6061FEAD" Ref="R168"  Part="1" 
F 0 "R168" H 5375 2275 50  0000 C CNN
F 1 "1k27" H 5375 2200 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5225 2225 50  0001 C CNN
F 3 "~" H 5225 2225 50  0001 C CNN
F 4 "WF08U1271BTL -  SMD Chip Resistor, 0805 [2012 Metric], 1.27 kohm, WF08U Series, 100 V, Thin Film, 100 mW" H 5225 1825 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/walsin/wf08u1271btl/res-1k27-0-1-0-1w-0805-thin-film/dp/2670133" H 5225 1725 50  0001 C CNN "Distributor Link"
F 6 "0.1%" H 5350 2125 50  0000 C CNN "Tolerance"
	1    5225 2225
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 6061FEB6
P 5225 1875
AR Path="/5F05E355/6061FEB6" Ref="R?"  Part="1" 
AR Path="/5E9C6910/6061FEB6" Ref="R?"  Part="1" 
AR Path="/5EFE605D/6061FEB6" Ref="R?"  Part="1" 
AR Path="/5EFE7331/6061FEB6" Ref="R?"  Part="1" 
AR Path="/5EA5E1C2/6061FEB6" Ref="R167"  Part="1" 
F 0 "R167" H 5375 1925 50  0000 C CNN
F 1 "100k" H 5375 1850 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5225 1875 50  0001 C CNN
F 3 "~" H 5225 1875 50  0001 C CNN
F 4 "MCWF08U1003BTL -  SMD Chip Resistor, 0805 [2012 Metric], 100 kohm, MCWF08U Series, 100 V, Thin Film, 100 mW" H 5225 1475 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwf08u1003btl/res-100k-0-1-0-1w-0805-thin-film/dp/2694161" H 5225 1375 50  0001 C CNN "Distributor Link"
F 6 "0.1%" H 5375 1775 50  0000 C CNN "Tolerance"
	1    5225 1875
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Connector:Jumper_NC_Small JP?
U 1 1 6061FEBE
P 4825 2225
AR Path="/6068445B/6061FEBE" Ref="JP?"  Part="1" 
AR Path="/60E816A3/6061FEBE" Ref="JP?"  Part="1" 
AR Path="/60EB5E32/6061FEBE" Ref="JP?"  Part="1" 
AR Path="/5EFE7331/6061FEBE" Ref="JP?"  Part="1" 
AR Path="/5EA5E1C2/6061FEBE" Ref="JP9"  Part="1" 
F 0 "JP9" V 4850 2200 50  0000 R CNN
F 1 "JUMPER" V 4775 2200 50  0000 R CNN
F 2 "PMSM_Driver_LV_Board:PinHeader_1x02_P2.54mm_Vertical" H 4825 2075 50  0001 C CNN
F 3 "~" H 4825 2225 50  0001 C CNN
F 4 "2211S-02G -  Board-To-Board Connector, 2.54 mm, 2 Contacts, Header, 2211S Series, Through Hole, 1 Rows" H 4825 1975 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/2211s-02g/header-1-row-vert-2way/dp/1593411" H 4825 1875 50  0001 C CNN "Distributor Link"
F 6 "DNI" V 4725 2325 50  0000 C CNN "DNI"
	1    4825 2225
	0    -1   -1   0   
$EndComp
Connection ~ 5225 2375
Wire Wire Line
	5225 1975 5225 2025
Connection ~ 5225 2075
Wire Wire Line
	4825 2125 4825 2025
Wire Wire Line
	4825 2025 5225 2025
Connection ~ 5225 2025
Wire Wire Line
	5225 2025 5225 2075
Wire Wire Line
	5225 2625 5225 2675
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 6061FECE
P 4825 2475
AR Path="/5F05E355/6061FECE" Ref="R?"  Part="1" 
AR Path="/5E9C6910/6061FECE" Ref="R?"  Part="1" 
AR Path="/5EFE605D/6061FECE" Ref="R?"  Part="1" 
AR Path="/5EFE7331/6061FECE" Ref="R?"  Part="1" 
AR Path="/5EA5E1C2/6061FECE" Ref="R166"  Part="1" 
F 0 "R166" H 4975 2525 50  0000 C CNN
F 1 "0" H 4900 2450 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4825 2475 50  0001 C CNN
F 3 "~" H 4825 2475 50  0001 C CNN
F 4 "..." H 4825 2075 50  0001 C CNN "Description"
F 5 "..." H 4825 1975 50  0001 C CNN "Distributor Link"
F 6 "DNI" H 4725 2375 50  0000 C CNN "DNI"
	1    4825 2475
	1    0    0    -1  
$EndComp
Wire Wire Line
	4825 2375 4825 2325
Wire Wire Line
	4825 2575 4825 2625
Wire Wire Line
	4825 2625 5225 2625
Wire Wire Line
	5225 2375 5225 2625
Connection ~ 5225 2625
$Comp
L PMSM_Driver_LV_Board_Power:MOT_VIN_HDR #PWR?
U 1 1 60632544
P 5225 4400
AR Path="/5E9C6910/60632544" Ref="#PWR?"  Part="1" 
AR Path="/5EFE7331/60632544" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E1C2/60632544" Ref="#PWR0316"  Part="1" 
F 0 "#PWR0316" H 5225 4650 50  0001 C CNN
F 1 "MOT_VIN_HDR" H 5226 4573 50  0000 C CNN
F 2 "" H 5225 4400 50  0001 C CNN
F 3 "" H 5225 4400 50  0001 C CNN
	1    5225 4400
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR?
U 1 1 60695740
P 2550 5525
AR Path="/5EFE605D/60695740" Ref="#PWR?"  Part="1" 
AR Path="/5EFE7331/60695740" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E1C2/60695740" Ref="#PWR0308"  Part="1" 
F 0 "#PWR0308" H 2550 5275 50  0001 C CNN
F 1 "MOT_DGND" H 2555 5352 50  0000 C CNN
F 2 "" H 2550 5525 50  0001 C CNN
F 3 "" H 2550 5525 50  0001 C CNN
	1    2550 5525
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR?
U 1 1 606A5E36
P 1750 5650
AR Path="/5EFE605D/606A5E36" Ref="#PWR?"  Part="1" 
AR Path="/5EFE7331/606A5E36" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E1C2/606A5E36" Ref="#PWR0302"  Part="1" 
F 0 "#PWR0302" H 1750 5400 50  0001 C CNN
F 1 "MOT_DGND" H 1755 5477 50  0000 C CNN
F 2 "" H 1750 5650 50  0001 C CNN
F 3 "" H 1750 5650 50  0001 C CNN
	1    1750 5650
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR?
U 1 1 606B6658
P 1800 4225
AR Path="/5EFE605D/606B6658" Ref="#PWR?"  Part="1" 
AR Path="/5EFE7331/606B6658" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E1C2/606B6658" Ref="#PWR0304"  Part="1" 
F 0 "#PWR0304" H 1800 3975 50  0001 C CNN
F 1 "MOT_DGND" H 1805 4052 50  0000 C CNN
F 2 "" H 1800 4225 50  0001 C CNN
F 3 "" H 1800 4225 50  0001 C CNN
	1    1800 4225
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR?
U 1 1 606F7A77
P 1800 1500
AR Path="/5EFE605D/606F7A77" Ref="#PWR?"  Part="1" 
AR Path="/5EFE7331/606F7A77" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E1C2/606F7A77" Ref="#PWR0303"  Part="1" 
F 0 "#PWR0303" H 1800 1250 50  0001 C CNN
F 1 "MOT_DGND" H 1805 1327 50  0000 C CNN
F 2 "" H 1800 1500 50  0001 C CNN
F 3 "" H 1800 1500 50  0001 C CNN
	1    1800 1500
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR?
U 1 1 607081C2
P 1750 2925
AR Path="/5EFE605D/607081C2" Ref="#PWR?"  Part="1" 
AR Path="/5EFE7331/607081C2" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E1C2/607081C2" Ref="#PWR0301"  Part="1" 
F 0 "#PWR0301" H 1750 2675 50  0001 C CNN
F 1 "MOT_DGND" H 1755 2752 50  0000 C CNN
F 2 "" H 1750 2925 50  0001 C CNN
F 3 "" H 1750 2925 50  0001 C CNN
	1    1750 2925
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR?
U 1 1 607189E1
P 2550 2800
AR Path="/5EFE605D/607189E1" Ref="#PWR?"  Part="1" 
AR Path="/5EFE7331/607189E1" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E1C2/607189E1" Ref="#PWR0307"  Part="1" 
F 0 "#PWR0307" H 2550 2550 50  0001 C CNN
F 1 "MOT_DGND" H 2555 2627 50  0000 C CNN
F 2 "" H 2550 2800 50  0001 C CNN
F 3 "" H 2550 2800 50  0001 C CNN
	1    2550 2800
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR?
U 1 1 60759E36
P 5625 2925
AR Path="/5EFE605D/60759E36" Ref="#PWR?"  Part="1" 
AR Path="/5EFE7331/60759E36" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E1C2/60759E36" Ref="#PWR0318"  Part="1" 
F 0 "#PWR0318" H 5625 2675 50  0001 C CNN
F 1 "MOT_DGND" H 5630 2752 50  0000 C CNN
F 2 "" H 5625 2925 50  0001 C CNN
F 3 "" H 5625 2925 50  0001 C CNN
	1    5625 2925
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR?
U 1 1 6076A39B
P 6425 2800
AR Path="/5EFE605D/6076A39B" Ref="#PWR?"  Part="1" 
AR Path="/5EFE7331/6076A39B" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E1C2/6076A39B" Ref="#PWR0324"  Part="1" 
F 0 "#PWR0324" H 6425 2550 50  0001 C CNN
F 1 "MOT_DGND" H 6430 2627 50  0000 C CNN
F 2 "" H 6425 2800 50  0001 C CNN
F 3 "" H 6425 2800 50  0001 C CNN
	1    6425 2800
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR?
U 1 1 6077AA8A
P 5675 1500
AR Path="/5EFE605D/6077AA8A" Ref="#PWR?"  Part="1" 
AR Path="/5EFE7331/6077AA8A" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E1C2/6077AA8A" Ref="#PWR0320"  Part="1" 
F 0 "#PWR0320" H 5675 1250 50  0001 C CNN
F 1 "MOT_DGND" H 5680 1327 50  0000 C CNN
F 2 "" H 5675 1500 50  0001 C CNN
F 3 "" H 5675 1500 50  0001 C CNN
	1    5675 1500
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR?
U 1 1 607BBE84
P 5675 4225
AR Path="/5EFE605D/607BBE84" Ref="#PWR?"  Part="1" 
AR Path="/5EFE7331/607BBE84" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E1C2/607BBE84" Ref="#PWR0321"  Part="1" 
F 0 "#PWR0321" H 5675 3975 50  0001 C CNN
F 1 "MOT_DGND" H 5680 4052 50  0000 C CNN
F 2 "" H 5675 4225 50  0001 C CNN
F 3 "" H 5675 4225 50  0001 C CNN
	1    5675 4225
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR?
U 1 1 607CC5EA
P 5625 5650
AR Path="/5EFE605D/607CC5EA" Ref="#PWR?"  Part="1" 
AR Path="/5EFE7331/607CC5EA" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E1C2/607CC5EA" Ref="#PWR0319"  Part="1" 
F 0 "#PWR0319" H 5625 5400 50  0001 C CNN
F 1 "MOT_DGND" H 5630 5477 50  0000 C CNN
F 2 "" H 5625 5650 50  0001 C CNN
F 3 "" H 5625 5650 50  0001 C CNN
	1    5625 5650
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR?
U 1 1 607DCD42
P 6425 5525
AR Path="/5EFE605D/607DCD42" Ref="#PWR?"  Part="1" 
AR Path="/5EFE7331/607DCD42" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E1C2/607DCD42" Ref="#PWR0325"  Part="1" 
F 0 "#PWR0325" H 6425 5275 50  0001 C CNN
F 1 "MOT_DGND" H 6430 5352 50  0000 C CNN
F 2 "" H 6425 5525 50  0001 C CNN
F 3 "" H 6425 5525 50  0001 C CNN
	1    6425 5525
	1    0    0    -1  
$EndComp
Text Notes 4575 1600 0    25   ~ 0
This voltage divider creates 0.25V\nfrom 20V phase voltage. Full-scale\nvoltage is 0.32V (25.5V phase \nvoltage). You can place the jumper\nto add parallel resistor of your choice\nto adjust the range for normal\noperation.\n
Text Notes 700  4325 0    25   ~ 0
This voltage divider creates 0.25V\nfrom 20V phase voltage. Full-scale\nvoltage is 0.32V (25.5V phase \nvoltage). You can place the jumper\nto add parallel resistor of your choice\nto adjust the range for normal\noperation.\n
$EndSCHEMATC
