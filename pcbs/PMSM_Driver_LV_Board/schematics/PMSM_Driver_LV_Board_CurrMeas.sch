EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 10 16
Title "PMSM_Driver_LV_Board"
Date "2020-03-30"
Rev "A"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text GLabel 1325 2075 0    50   Input ~ 0
MOT_SHUNT_A
$Comp
L PMSM_Driver_LV_Board_Power:MOT_SENS_A_AGND #PWR?
U 1 1 5EFE6CF8
P 1000 2425
AR Path="/5EA41912/5EFE6CF8" Ref="#PWR?"  Part="1" 
AR Path="/5EFE6CF8" Ref="#PWR?"  Part="1" 
AR Path="/5EFE605D/5EFE6CF8" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E155/5EFE6CF8" Ref="#PWR0248"  Part="1" 
F 0 "#PWR0248" H 1000 2175 50  0001 C CNN
F 1 "MOT_SENS_A_AGND" H 1005 2252 50  0000 C CNN
F 2 "" H 1000 2425 50  0001 C CNN
F 3 "" H 1000 2425 50  0001 C CNN
	1    1000 2425
	1    0    0    -1  
$EndComp
Text GLabel 4825 2075 0    50   Input ~ 0
MOT_SHUNT_B
$Comp
L PMSM_Driver_LV_Board_Power:MOT_SENS_B_AGND #PWR?
U 1 1 5EFE6CFF
P 6700 4750
AR Path="/5EA41912/5EFE6CFF" Ref="#PWR?"  Part="1" 
AR Path="/5EFE6CFF" Ref="#PWR?"  Part="1" 
AR Path="/5EFE605D/5EFE6CFF" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E155/5EFE6CFF" Ref="#PWR0277"  Part="1" 
F 0 "#PWR0277" H 6700 4500 50  0001 C CNN
F 1 "MOT_SENS_B_AGND" H 6705 4577 50  0000 C CNN
F 2 "" H 6700 4750 50  0001 C CNN
F 3 "" H 6700 4750 50  0001 C CNN
	1    6700 4750
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_IC:AD7403BRIZ U33
U 1 1 5EFF40F5
P 2750 2325
F 0 "U33" H 2475 2825 50  0000 C CNN
F 1 "AD7403BRIZ" H 2750 1825 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:SOIC-16W_7.5x12.8mm_P1.27mm" H 2750 1675 50  0001 C CNN
F 3 "~" H 2450 2825 50  0001 C CNN
F 4 "AD7403BRIZ -  Analogue to Digital Converter, 16 bit, Differential, Serial, Single, 4.5 V" H 2750 1625 31  0001 C CNN "Description"
F 5 "https://uk.farnell.com/analog-devices/ad7403briz/adc-16bit-soic-16/dp/2431665" H 2750 1575 31  0001 C CNN "Distributor Link"
F 6 "https://www.mouser.co.uk/ProductDetail/Analog-Devices/AD7403BRIZ?qs=sGAEpiMZZMt%2Foy9k%2F5obbVdPNrLrhFiKvGld2xGDVNA%3D" H 2750 1525 31  0001 C CNN "Distributor Link 2"
	1    2750 2325
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_IC:ADUM5000ARWZ U32
U 1 1 5EFF7ED4
P 2275 4300
F 0 "U32" H 1975 4800 50  0000 C CNN
F 1 "ADUM5000ARWZ" H 2275 3800 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:SOIC-16W_7.5x10.3mm_P1.27mm" H 1375 4850 50  0001 C CNN
F 3 "~" H 1375 4850 50  0001 C CNN
F 4 "ADUM5000ARWZ -  DC-DC Switching Regulator, Adjustable, 2.7V to 5.5V in, 3.3V to 5.5V/100 mA out, 180 MHz, WSOIC-16" H 2275 3700 31  0001 C CNN "Description"
F 5 "https://uk.farnell.com/analog-devices/adum5000arwz/ic-isolated-dc-dc-conv-180mhz/dp/2102522" H 2275 3650 31  0001 C CNN "Distributor Link"
F 6 "https://www.mouser.co.uk/ProductDetail/Analog-Devices/ADUM5000ARWZ?qs=sGAEpiMZZMtitjHzVIkrqYThiPHWFX9aYDreduI%2FIuI%3D" H 2275 3600 31  0001 C CNN "Distributor Link 2"
	1    2275 4300
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_SENS_B_5V0 #PWR0272
U 1 1 5EFFA95D
P 5600 1100
F 0 "#PWR0272" H 5600 1350 50  0001 C CNN
F 1 "MOT_SENS_B_5V0" H 5601 1273 50  0000 C CNN
F 2 "" H 5600 1100 50  0001 C CNN
F 3 "" H 5600 1100 50  0001 C CNN
	1    5600 1100
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_5V0 #PWR0262
U 1 1 5EFFD503
P 3400 1100
F 0 "#PWR0262" H 3400 1350 50  0001 C CNN
F 1 "MOT_5V0" H 3401 1273 50  0000 C CNN
F 2 "" H 3400 1100 50  0001 C CNN
F 3 "" H 3400 1100 50  0001 C CNN
	1    3400 1100
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR0261
U 1 1 5F001CE9
P 3300 2775
F 0 "#PWR0261" H 3300 2525 50  0001 C CNN
F 1 "MOT_DGND" H 3305 2602 50  0000 C CNN
F 2 "" H 3300 2775 50  0001 C CNN
F 3 "" H 3300 2775 50  0001 C CNN
	1    3300 2775
	1    0    0    -1  
$EndComp
Wire Wire Line
	3300 2775 3300 2675
Wire Wire Line
	3300 1975 3200 1975
Wire Wire Line
	3200 2675 3300 2675
Connection ~ 3300 2675
Wire Wire Line
	3300 2675 3300 1975
Wire Wire Line
	3400 2175 3200 2175
NoConn ~ 3200 2075
NoConn ~ 3200 2375
NoConn ~ 3200 2575
Text GLabel 3400 2275 2    50   Input ~ 0
ISO_ADC_CLK
Wire Wire Line
	3400 2275 3200 2275
Text GLabel 3400 2475 2    50   Output ~ 0
ISO_CUR_A
Wire Wire Line
	3400 2475 3200 2475
Wire Wire Line
	1450 1450 1450 1500
Wire Wire Line
	2100 2575 2300 2575
Wire Wire Line
	2300 1975 2100 1975
Wire Wire Line
	2100 1975 2100 2575
$Comp
L PMSM_Driver_LV_Board_Power:MOT_SENS_A_AGND #PWR?
U 1 1 5F031AC2
P 2200 2800
AR Path="/5EA41912/5F031AC2" Ref="#PWR?"  Part="1" 
AR Path="/5F031AC2" Ref="#PWR?"  Part="1" 
AR Path="/5EFE605D/5F031AC2" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E155/5F031AC2" Ref="#PWR0256"  Part="1" 
F 0 "#PWR0256" H 2200 2550 50  0001 C CNN
F 1 "MOT_SENS_A_AGND" H 2205 2627 50  0000 C CNN
F 2 "" H 2200 2800 50  0001 C CNN
F 3 "" H 2200 2800 50  0001 C CNN
	1    2200 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	2200 2800 2200 2675
Wire Wire Line
	2200 2675 2300 2675
Wire Wire Line
	2200 2675 2200 2275
Wire Wire Line
	2200 2275 2300 2275
Connection ~ 2200 2675
NoConn ~ 2300 2375
NoConn ~ 2300 2475
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 5F034E56
P 1575 2075
AR Path="/5F05E355/5F034E56" Ref="R?"  Part="1" 
AR Path="/5E9C6910/5F034E56" Ref="R?"  Part="1" 
AR Path="/5EFE605D/5F034E56" Ref="R?"  Part="1" 
AR Path="/5EA5E155/5F034E56" Ref="R150"  Part="1" 
F 0 "R150" V 1625 2225 50  0000 C CNN
F 1 "22" V 1625 1975 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 1575 2075 50  0001 C CNN
F 3 "~" H 1575 2075 50  0001 C CNN
F 4 "MCWR04X22R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 22 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 1575 1675 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x22r0ftl/res-22r-1-0-0625w-0402-thick-film/dp/2447138" H 1575 1575 50  0001 C CNN "Distributor Link"
	1    1575 2075
	0    -1   -1   0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 5F03980E
P 1575 2375
AR Path="/5F05E355/5F03980E" Ref="R?"  Part="1" 
AR Path="/5E9C6910/5F03980E" Ref="R?"  Part="1" 
AR Path="/5EFE605D/5F03980E" Ref="R?"  Part="1" 
AR Path="/5EA5E155/5F03980E" Ref="R151"  Part="1" 
F 0 "R151" V 1625 2550 50  0000 C CNN
F 1 "22" V 1625 2275 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 1575 2375 50  0001 C CNN
F 3 "~" H 1575 2375 50  0001 C CNN
F 4 "MCWR04X22R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 22 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 1575 1975 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x22r0ftl/res-22r-1-0-0625w-0402-thick-film/dp/2447138" H 1575 1875 50  0001 C CNN "Distributor Link"
	1    1575 2375
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1325 2075 1475 2075
Wire Wire Line
	1000 2425 1000 2375
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5F03BE21
P 1775 2225
AR Path="/5E9C6910/5F03BE21" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5F03BE21" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5F03BE21" Ref="C?"  Part="1" 
AR Path="/5F05E355/5F03BE21" Ref="C?"  Part="1" 
AR Path="/6068445B/5F03BE21" Ref="C?"  Part="1" 
AR Path="/60E816A3/5F03BE21" Ref="C?"  Part="1" 
AR Path="/5EFE605D/5F03BE21" Ref="C?"  Part="1" 
AR Path="/5EA5E155/5F03BE21" Ref="C105"  Part="1" 
F 0 "C105" H 1550 2300 50  0000 L CNN
F 1 "47pF" H 1550 2150 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 1775 2225 50  0001 C CNN
F 3 "~" H 1775 2225 50  0001 C CNN
F 4 "MC0402N470J500CT -  SMD Multilayer Ceramic Capacitor, 47 pF, 50 V, 0402 [1005 Metric], ± 5%, C0G / NP0, MC Series " H 1775 1825 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402n470j500ct/cap-47pf-50v-5-c0g-np0-0402/dp/1758959" H 1775 1725 50  0001 C CNN "Distributor Link"
	1    1775 2225
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1675 2075 1775 2075
Wire Wire Line
	1775 2075 1775 2125
Wire Wire Line
	1675 2375 1775 2375
Wire Wire Line
	1775 2375 1775 2325
Wire Wire Line
	1775 2375 2025 2375
Wire Wire Line
	2025 2375 2025 2175
Wire Wire Line
	2025 2175 2300 2175
Connection ~ 1775 2375
Wire Wire Line
	2300 2075 1775 2075
Connection ~ 1775 2075
Text Notes 675  800  0    100  ~ 0
Phase A Current Measurement
Wire Notes Line
	600  600  4025 600 
$Comp
L PMSM_Driver_LV_Board_Power:MOT_SENS_A_5V0 #PWR0255
U 1 1 5EFFA36F
P 2100 1100
F 0 "#PWR0255" H 2100 1350 50  0001 C CNN
F 1 "MOT_SENS_A_5V0" H 2101 1273 50  0000 C CNN
F 2 "" H 2100 1100 50  0001 C CNN
F 3 "" H 2100 1100 50  0001 C CNN
	1    2100 1100
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5F02EFC9
P 1775 1300
AR Path="/5E9C6910/5F02EFC9" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5F02EFC9" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5F02EFC9" Ref="C?"  Part="1" 
AR Path="/5F05E355/5F02EFC9" Ref="C?"  Part="1" 
AR Path="/6068445B/5F02EFC9" Ref="C?"  Part="1" 
AR Path="/60E816A3/5F02EFC9" Ref="C?"  Part="1" 
AR Path="/5EFE605D/5F02EFC9" Ref="C?"  Part="1" 
AR Path="/5EA5E155/5F02EFC9" Ref="C104"  Part="1" 
F 0 "C104" H 1500 1300 50  0000 L CNN
F 1 "1nF" H 1600 1225 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 1775 1300 50  0001 C CNN
F 3 "~" H 1775 1300 50  0001 C CNN
F 4 "MC0402B102K160CT -  SMD Multilayer Ceramic Capacitor, 1000 pF, 16 V, 0402 [1005 Metric], ± 10%, X7R, MC Series" H 1775 900 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402b102k160ct/cap-1000pf-16v-10-x7r-0402/dp/2320774" H 1775 800 50  0001 C CNN "Distributor Link"
	1    1775 1300
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1775 1400 1775 1450
Wire Wire Line
	1775 1150 1775 1200
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5F0C30BA
P 1450 1300
AR Path="/5E9C6910/5F0C30BA" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5F0C30BA" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5F0C30BA" Ref="C?"  Part="1" 
AR Path="/5F05E355/5F0C30BA" Ref="C?"  Part="1" 
AR Path="/6068445B/5F0C30BA" Ref="C?"  Part="1" 
AR Path="/60E816A3/5F0C30BA" Ref="C?"  Part="1" 
AR Path="/5EFE605D/5F0C30BA" Ref="C?"  Part="1" 
AR Path="/5EA5E155/5F0C30BA" Ref="C101"  Part="1" 
F 0 "C101" H 1525 1300 50  0000 L CNN
F 1 "10uF" H 1475 1225 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 1450 1300 50  0001 C CNN
F 3 "~" H 1450 1300 50  0001 C CNN
F 4 "LMK107BBJ106MALT -  SMD Multilayer Ceramic Capacitor, 10 µF, 10 V, 0603 [1608 Metric], ± 20%, X5R, M Series" H 1450 900 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/taiyo-yuden/lmk107bbj106malt/cap-10-f-10v-20-x5r-0603/dp/2113070" H 1450 800 50  0001 C CNN "Distributor Link"
	1    1450 1300
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1450 1400 1450 1450
Wire Wire Line
	1450 1150 1450 1200
Connection ~ 1450 1450
Wire Wire Line
	2100 1100 2100 1150
Wire Wire Line
	1450 1450 1775 1450
Wire Wire Line
	1450 1150 1775 1150
Connection ~ 1775 1150
Wire Wire Line
	1775 1150 2100 1150
Connection ~ 2100 1150
Wire Wire Line
	2750 1450 2750 1500
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR0258
U 1 1 5F0CE6CE
P 2750 1500
F 0 "#PWR0258" H 2750 1250 50  0001 C CNN
F 1 "MOT_DGND" H 2755 1327 50  0000 C CNN
F 2 "" H 2750 1500 50  0001 C CNN
F 3 "" H 2750 1500 50  0001 C CNN
	1    2750 1500
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3075 1400 3075 1450
Wire Wire Line
	3075 1150 3075 1200
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5F0CE6E0
P 2750 1300
AR Path="/5E9C6910/5F0CE6E0" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5F0CE6E0" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5F0CE6E0" Ref="C?"  Part="1" 
AR Path="/5F05E355/5F0CE6E0" Ref="C?"  Part="1" 
AR Path="/6068445B/5F0CE6E0" Ref="C?"  Part="1" 
AR Path="/60E816A3/5F0CE6E0" Ref="C?"  Part="1" 
AR Path="/5EFE605D/5F0CE6E0" Ref="C?"  Part="1" 
AR Path="/5EA5E155/5F0CE6E0" Ref="C109"  Part="1" 
F 0 "C109" H 2825 1300 50  0000 L CNN
F 1 "10uF" H 2775 1225 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 2750 1300 50  0001 C CNN
F 3 "~" H 2750 1300 50  0001 C CNN
F 4 "LMK107BBJ106MALT -  SMD Multilayer Ceramic Capacitor, 10 µF, 10 V, 0603 [1608 Metric], ± 20%, X5R, M Series" H 2750 900 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/taiyo-yuden/lmk107bbj106malt/cap-10-f-10v-20-x5r-0603/dp/2113070" H 2750 800 50  0001 C CNN "Distributor Link"
	1    2750 1300
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2750 1400 2750 1450
Wire Wire Line
	2750 1150 2750 1200
Connection ~ 2750 1450
Wire Wire Line
	3400 1100 3400 1150
Wire Wire Line
	2750 1450 3075 1450
Wire Wire Line
	2750 1150 3075 1150
Connection ~ 3075 1150
Wire Wire Line
	3075 1150 3400 1150
Connection ~ 3400 1150
Connection ~ 2100 1975
Wire Wire Line
	3400 1150 3400 2175
Wire Wire Line
	2100 1150 2100 1975
Wire Wire Line
	1400 2875 1400 2925
Wire Wire Line
	1725 2825 1725 2875
Wire Wire Line
	1725 2575 1725 2625
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5F0DB97C
P 1400 2725
AR Path="/5E9C6910/5F0DB97C" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5F0DB97C" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5F0DB97C" Ref="C?"  Part="1" 
AR Path="/5F05E355/5F0DB97C" Ref="C?"  Part="1" 
AR Path="/6068445B/5F0DB97C" Ref="C?"  Part="1" 
AR Path="/60E816A3/5F0DB97C" Ref="C?"  Part="1" 
AR Path="/5EFE605D/5F0DB97C" Ref="C?"  Part="1" 
AR Path="/5EA5E155/5F0DB97C" Ref="C100"  Part="1" 
F 0 "C100" H 1175 2725 50  0000 L CNN
F 1 "10uF" H 1125 2650 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 1400 2725 50  0001 C CNN
F 3 "~" H 1400 2725 50  0001 C CNN
F 4 "LMK107BBJ106MALT -  SMD Multilayer Ceramic Capacitor, 10 µF, 10 V, 0603 [1608 Metric], ± 20%, X5R, M Series" H 1400 2325 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/taiyo-yuden/lmk107bbj106malt/cap-10-f-10v-20-x5r-0603/dp/2113070" H 1400 2225 50  0001 C CNN "Distributor Link"
	1    1400 2725
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1400 2825 1400 2875
Wire Wire Line
	1400 2575 1400 2625
Connection ~ 1400 2875
Wire Wire Line
	1400 2875 1725 2875
Wire Wire Line
	1400 2575 1725 2575
Wire Wire Line
	1000 2375 1475 2375
Connection ~ 2100 2575
Wire Wire Line
	2100 2575 1725 2575
Connection ~ 1725 2575
Text Notes 800  2175 0    25   ~ 0
The cut-off frequency is 76 MHz.
Text Notes 2450 2900 0    25   ~ 0
Isolated sigma-delta modulator
Text Notes 1975 4875 0    25   ~ 0
Isolated floating power supply
$Comp
L PMSM_Driver_LV_Board_Power:MOT_SENS_A_AGND #PWR?
U 1 1 5F0F98AA
P 1400 2925
AR Path="/5EA41912/5F0F98AA" Ref="#PWR?"  Part="1" 
AR Path="/5F0F98AA" Ref="#PWR?"  Part="1" 
AR Path="/5EFE605D/5F0F98AA" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E155/5F0F98AA" Ref="#PWR0251"  Part="1" 
F 0 "#PWR0251" H 1400 2675 50  0001 C CNN
F 1 "MOT_SENS_A_AGND" H 1405 2752 50  0000 C CNN
F 2 "" H 1400 2925 50  0001 C CNN
F 3 "" H 1400 2925 50  0001 C CNN
	1    1400 2925
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_SENS_A_AGND #PWR?
U 1 1 5F0FB454
P 1450 1500
AR Path="/5EA41912/5F0FB454" Ref="#PWR?"  Part="1" 
AR Path="/5F0FB454" Ref="#PWR?"  Part="1" 
AR Path="/5EFE605D/5F0FB454" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E155/5F0FB454" Ref="#PWR0252"  Part="1" 
F 0 "#PWR0252" H 1450 1250 50  0001 C CNN
F 1 "MOT_SENS_A_AGND" H 1455 1327 50  0000 C CNN
F 2 "" H 1450 1500 50  0001 C CNN
F 3 "" H 1450 1500 50  0001 C CNN
	1    1450 1500
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5F0FD405
P 3075 1300
AR Path="/5E9C6910/5F0FD405" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5F0FD405" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5F0FD405" Ref="C?"  Part="1" 
AR Path="/5F05E355/5F0FD405" Ref="C?"  Part="1" 
AR Path="/6068445B/5F0FD405" Ref="C?"  Part="1" 
AR Path="/60E816A3/5F0FD405" Ref="C?"  Part="1" 
AR Path="/5EFE605D/5F0FD405" Ref="C?"  Part="1" 
AR Path="/5EA5E155/5F0FD405" Ref="C111"  Part="1" 
F 0 "C111" H 2800 1300 50  0000 L CNN
F 1 "1nF" H 2900 1225 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 3075 1300 50  0001 C CNN
F 3 "~" H 3075 1300 50  0001 C CNN
F 4 "MC0402B102K160CT -  SMD Multilayer Ceramic Capacitor, 1000 pF, 16 V, 0402 [1005 Metric], ± 10%, X7R, MC Series" H 3075 900 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402b102k160ct/cap-1000pf-16v-10-x7r-0402/dp/2320774" H 3075 800 50  0001 C CNN "Distributor Link"
	1    3075 1300
	-1   0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5F0FF0D4
P 1725 2725
AR Path="/5E9C6910/5F0FF0D4" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5F0FF0D4" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5F0FF0D4" Ref="C?"  Part="1" 
AR Path="/5F05E355/5F0FF0D4" Ref="C?"  Part="1" 
AR Path="/6068445B/5F0FF0D4" Ref="C?"  Part="1" 
AR Path="/60E816A3/5F0FF0D4" Ref="C?"  Part="1" 
AR Path="/5EFE605D/5F0FF0D4" Ref="C?"  Part="1" 
AR Path="/5EA5E155/5F0FF0D4" Ref="C103"  Part="1" 
F 0 "C103" H 1450 2725 50  0000 L CNN
F 1 "1nF" H 1550 2650 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 1725 2725 50  0001 C CNN
F 3 "~" H 1725 2725 50  0001 C CNN
F 4 "MC0402B102K160CT -  SMD Multilayer Ceramic Capacitor, 1000 pF, 16 V, 0402 [1005 Metric], ± 10%, X7R, MC Series" H 1725 2325 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402b102k160ct/cap-1000pf-16v-10-x7r-0402/dp/2320774" H 1725 2225 50  0001 C CNN "Distributor Link"
	1    1725 2725
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2600 5375 2600 5425
Wire Wire Line
	2925 5325 2925 5375
Wire Wire Line
	2925 5075 2925 5125
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5F100AB1
P 2925 5225
AR Path="/5E9C6910/5F100AB1" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5F100AB1" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5F100AB1" Ref="C?"  Part="1" 
AR Path="/5F05E355/5F100AB1" Ref="C?"  Part="1" 
AR Path="/6068445B/5F100AB1" Ref="C?"  Part="1" 
AR Path="/60E816A3/5F100AB1" Ref="C?"  Part="1" 
AR Path="/5EFE605D/5F100AB1" Ref="C?"  Part="1" 
AR Path="/5EA5E155/5F100AB1" Ref="C110"  Part="1" 
F 0 "C110" H 2725 5300 50  0000 L CNN
F 1 "10uF" H 2725 5150 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 2925 5225 50  0001 C CNN
F 3 "~" H 2925 5225 50  0001 C CNN
F 4 "LMK107BBJ106MALT -  SMD Multilayer Ceramic Capacitor, 10 µF, 10 V, 0603 [1608 Metric], ± 20%, X5R, M Series" H 2925 4825 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/taiyo-yuden/lmk107bbj106malt/cap-10-f-10v-20-x5r-0603/dp/2113070" H 2925 4725 50  0001 C CNN "Distributor Link"
	1    2925 5225
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2600 5325 2600 5375
Wire Wire Line
	2600 5075 2600 5125
Connection ~ 2600 5375
Wire Wire Line
	2600 5375 2925 5375
Wire Wire Line
	2600 5075 2825 5075
$Comp
L PMSM_Driver_LV_Board_Power:MOT_SENS_A_AGND #PWR?
U 1 1 5F100AC1
P 2600 5425
AR Path="/5EA41912/5F100AC1" Ref="#PWR?"  Part="1" 
AR Path="/5F100AC1" Ref="#PWR?"  Part="1" 
AR Path="/5EFE605D/5F100AC1" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E155/5F100AC1" Ref="#PWR0257"  Part="1" 
F 0 "#PWR0257" H 2600 5175 50  0001 C CNN
F 1 "MOT_SENS_A_AGND" H 2605 5252 50  0000 C CNN
F 2 "" H 2600 5425 50  0001 C CNN
F 3 "" H 2600 5425 50  0001 C CNN
	1    2600 5425
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5F104338
P 2600 5225
AR Path="/5E9C6910/5F104338" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5F104338" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5F104338" Ref="C?"  Part="1" 
AR Path="/5F05E355/5F104338" Ref="C?"  Part="1" 
AR Path="/5EA41912/5F104338" Ref="C?"  Part="1" 
AR Path="/5EFE605D/5F104338" Ref="C?"  Part="1" 
AR Path="/5EA5E155/5F104338" Ref="C108"  Part="1" 
F 0 "C108" H 2400 5300 50  0000 L CNN
F 1 "100nF" H 2350 5150 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 2600 5225 50  0001 C CNN
F 3 "~" H 2600 5225 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 2600 4825 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 2600 4725 50  0001 C CNN "Distributor Link"
	1    2600 5225
	-1   0    0    -1  
$EndComp
NoConn ~ 1775 4150
NoConn ~ 1775 4250
NoConn ~ 1775 4350
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR0254
U 1 1 5F113897
P 1675 4750
F 0 "#PWR0254" H 1675 4500 50  0001 C CNN
F 1 "MOT_DGND" H 1680 4577 50  0000 C CNN
F 2 "" H 1675 4750 50  0001 C CNN
F 3 "" H 1675 4750 50  0001 C CNN
	1    1675 4750
	1    0    0    -1  
$EndComp
Wire Wire Line
	1675 4750 1675 4650
Wire Wire Line
	1675 4650 1775 4650
Wire Wire Line
	3200 4750 3200 4650
$Comp
L PMSM_Driver_LV_Board_Power:MOT_SENS_A_AGND #PWR?
U 1 1 5F11961A
P 3200 4750
AR Path="/5EA41912/5F11961A" Ref="#PWR?"  Part="1" 
AR Path="/5F11961A" Ref="#PWR?"  Part="1" 
AR Path="/5EFE605D/5F11961A" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E155/5F11961A" Ref="#PWR0260"  Part="1" 
F 0 "#PWR0260" H 3200 4500 50  0001 C CNN
F 1 "MOT_SENS_A_AGND" H 3205 4577 50  0000 C CNN
F 2 "" H 3200 4750 50  0001 C CNN
F 3 "" H 3200 4750 50  0001 C CNN
	1    3200 4750
	1    0    0    -1  
$EndComp
Connection ~ 3200 4650
Wire Wire Line
	1675 4650 1675 4600
Wire Wire Line
	1675 4050 1775 4050
Connection ~ 1675 4650
$Comp
L PMSM_Driver_LV_Board_Power:MOT_5V0 #PWR0253
U 1 1 5F1208CB
P 1575 3850
F 0 "#PWR0253" H 1575 4100 50  0001 C CNN
F 1 "MOT_5V0" H 1576 4023 50  0000 C CNN
F 2 "" H 1575 3850 50  0001 C CNN
F 3 "" H 1575 3850 50  0001 C CNN
	1    1575 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	1575 3850 1575 3950
Wire Wire Line
	1575 3950 1775 3950
Wire Wire Line
	1775 4550 1575 4550
Wire Wire Line
	1575 4550 1575 4450
Connection ~ 1575 3950
Wire Wire Line
	1775 4450 1575 4450
Connection ~ 1575 4450
Wire Wire Line
	1575 4450 1575 3950
$Comp
L PMSM_Driver_LV_Board_Device:Ferrite_Bead_Small FB1
U 1 1 5F132B2D
P 3400 3950
F 0 "FB1" V 3525 3875 50  0000 L CNN
F 1 "Z0805C420APWST" V 3275 3775 25  0000 L CNN
F 2 "PMSM_Driver_LV_Board:L_0805_2012Metric" V 3330 3950 50  0001 C CNN
F 3 "~" H 3400 3950 50  0001 C CNN
F 4 "Z0805C420APWST -  Ferrite Bead, 0805 [2012 Metric], 42 ohm, 4 A, Z-PWS Series, 0.008 ohm, ± 25%" H 3400 3950 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/kemet/z0805c420apwst/ferrite-bead-42-ohm-4a-0805/dp/2846741" H 3400 3950 50  0001 C CNN "Distributor Link"
F 6 "42 Ohms at 100 MHz" V 3225 3725 25  0000 L CNN "Impedance"
	1    3400 3950
	0    -1   -1   0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_SENS_A_5V0 #PWR0263
U 1 1 5F1459E1
P 3600 3850
F 0 "#PWR0263" H 3600 4100 50  0001 C CNN
F 1 "MOT_SENS_A_5V0" H 3601 4023 50  0000 C CNN
F 2 "" H 3600 3850 50  0001 C CNN
F 3 "" H 3600 3850 50  0001 C CNN
	1    3600 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	3500 3950 3600 3950
Wire Wire Line
	3600 3950 3600 3850
Wire Wire Line
	2925 5375 3250 5375
Wire Wire Line
	3250 5325 3250 5375
Wire Wire Line
	3250 5075 3250 5125
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5F156303
P 3250 5225
AR Path="/5E9C6910/5F156303" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5F156303" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5F156303" Ref="C?"  Part="1" 
AR Path="/5F05E355/5F156303" Ref="C?"  Part="1" 
AR Path="/5EA41912/5F156303" Ref="C?"  Part="1" 
AR Path="/5EFE605D/5F156303" Ref="C?"  Part="1" 
AR Path="/5EA5E155/5F156303" Ref="C112"  Part="1" 
F 0 "C112" H 3050 5300 50  0000 L CNN
F 1 "100nF" H 3000 5150 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 3250 5225 50  0001 C CNN
F 3 "~" H 3250 5225 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 3250 4825 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 3250 4725 50  0001 C CNN "Distributor Link"
	1    3250 5225
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3575 5325 3575 5375
Wire Wire Line
	3575 5075 3575 5125
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5F158858
P 3575 5225
AR Path="/5E9C6910/5F158858" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5F158858" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5F158858" Ref="C?"  Part="1" 
AR Path="/5F05E355/5F158858" Ref="C?"  Part="1" 
AR Path="/6068445B/5F158858" Ref="C?"  Part="1" 
AR Path="/60E816A3/5F158858" Ref="C?"  Part="1" 
AR Path="/5EFE605D/5F158858" Ref="C?"  Part="1" 
AR Path="/5EA5E155/5F158858" Ref="C113"  Part="1" 
F 0 "C113" H 3375 5300 50  0000 L CNN
F 1 "10uF" H 3375 5150 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 3575 5225 50  0001 C CNN
F 3 "~" H 3575 5225 50  0001 C CNN
F 4 "LMK107BBJ106MALT -  SMD Multilayer Ceramic Capacitor, 10 µF, 10 V, 0603 [1608 Metric], ± 20%, X5R, M Series" H 3575 4825 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/taiyo-yuden/lmk107bbj106malt/cap-10-f-10v-20-x5r-0603/dp/2113070" H 3575 4725 50  0001 C CNN "Distributor Link"
	1    3575 5225
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3250 5375 3575 5375
Wire Wire Line
	3250 5075 3575 5075
Wire Wire Line
	2725 3950 2825 3950
Connection ~ 2925 5075
Connection ~ 2925 5375
Connection ~ 3250 5075
Connection ~ 3250 5375
Wire Wire Line
	1275 5375 1275 5425
Wire Wire Line
	1600 5325 1600 5375
Wire Wire Line
	1600 5075 1600 5125
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5F1728D3
P 1600 5225
AR Path="/5E9C6910/5F1728D3" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5F1728D3" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5F1728D3" Ref="C?"  Part="1" 
AR Path="/5F05E355/5F1728D3" Ref="C?"  Part="1" 
AR Path="/6068445B/5F1728D3" Ref="C?"  Part="1" 
AR Path="/60E816A3/5F1728D3" Ref="C?"  Part="1" 
AR Path="/5EFE605D/5F1728D3" Ref="C?"  Part="1" 
AR Path="/5EA5E155/5F1728D3" Ref="C102"  Part="1" 
F 0 "C102" H 1400 5300 50  0000 L CNN
F 1 "10uF" H 1400 5150 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 1600 5225 50  0001 C CNN
F 3 "~" H 1600 5225 50  0001 C CNN
F 4 "LMK107BBJ106MALT -  SMD Multilayer Ceramic Capacitor, 10 µF, 10 V, 0603 [1608 Metric], ± 20%, X5R, M Series" H 1600 4825 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/taiyo-yuden/lmk107bbj106malt/cap-10-f-10v-20-x5r-0603/dp/2113070" H 1600 4725 50  0001 C CNN "Distributor Link"
	1    1600 5225
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1275 5325 1275 5375
Wire Wire Line
	1275 5075 1275 5125
Connection ~ 1275 5375
Wire Wire Line
	1275 5375 1600 5375
Wire Wire Line
	1275 5075 1600 5075
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5F1728E6
P 1275 5225
AR Path="/5E9C6910/5F1728E6" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5F1728E6" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5F1728E6" Ref="C?"  Part="1" 
AR Path="/5F05E355/5F1728E6" Ref="C?"  Part="1" 
AR Path="/5EA41912/5F1728E6" Ref="C?"  Part="1" 
AR Path="/5EFE605D/5F1728E6" Ref="C?"  Part="1" 
AR Path="/5EA5E155/5F1728E6" Ref="C99"  Part="1" 
F 0 "C99" H 1125 5300 50  0000 L CNN
F 1 "100nF" H 1025 5150 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 1275 5225 50  0001 C CNN
F 3 "~" H 1275 5225 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 1275 4825 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 1275 4725 50  0001 C CNN "Distributor Link"
	1    1275 5225
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1600 5375 1925 5375
Wire Wire Line
	1925 5325 1925 5375
Wire Wire Line
	1925 5075 1925 5125
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5F1728F1
P 1925 5225
AR Path="/5E9C6910/5F1728F1" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5F1728F1" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5F1728F1" Ref="C?"  Part="1" 
AR Path="/5F05E355/5F1728F1" Ref="C?"  Part="1" 
AR Path="/5EA41912/5F1728F1" Ref="C?"  Part="1" 
AR Path="/5EFE605D/5F1728F1" Ref="C?"  Part="1" 
AR Path="/5EA5E155/5F1728F1" Ref="C106"  Part="1" 
F 0 "C106" H 1725 5300 50  0000 L CNN
F 1 "100nF" H 1675 5150 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 1925 5225 50  0001 C CNN
F 3 "~" H 1925 5225 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 1925 4825 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 1925 4725 50  0001 C CNN "Distributor Link"
	1    1925 5225
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2250 5325 2250 5375
Wire Wire Line
	2250 5075 2250 5125
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5F1728FB
P 2250 5225
AR Path="/5E9C6910/5F1728FB" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5F1728FB" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5F1728FB" Ref="C?"  Part="1" 
AR Path="/5F05E355/5F1728FB" Ref="C?"  Part="1" 
AR Path="/6068445B/5F1728FB" Ref="C?"  Part="1" 
AR Path="/60E816A3/5F1728FB" Ref="C?"  Part="1" 
AR Path="/5EFE605D/5F1728FB" Ref="C?"  Part="1" 
AR Path="/5EA5E155/5F1728FB" Ref="C107"  Part="1" 
F 0 "C107" H 2050 5300 50  0000 L CNN
F 1 "10uF" H 2050 5150 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 2250 5225 50  0001 C CNN
F 3 "~" H 2250 5225 50  0001 C CNN
F 4 "LMK107BBJ106MALT -  SMD Multilayer Ceramic Capacitor, 10 µF, 10 V, 0603 [1608 Metric], ± 20%, X5R, M Series" H 2250 4825 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/taiyo-yuden/lmk107bbj106malt/cap-10-f-10v-20-x5r-0603/dp/2113070" H 2250 4725 50  0001 C CNN "Distributor Link"
	1    2250 5225
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1925 5375 2250 5375
Wire Wire Line
	1925 5075 2250 5075
Connection ~ 1600 5075
Connection ~ 1600 5375
Connection ~ 1925 5075
Connection ~ 1925 5375
Wire Wire Line
	1600 5075 1925 5075
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR0250
U 1 1 5F18130D
P 1275 5425
F 0 "#PWR0250" H 1275 5175 50  0001 C CNN
F 1 "MOT_DGND" H 1280 5252 50  0000 C CNN
F 2 "" H 1275 5425 50  0001 C CNN
F 3 "" H 1275 5425 50  0001 C CNN
	1    1275 5425
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_5V0 #PWR0249
U 1 1 5F184909
P 1275 4975
F 0 "#PWR0249" H 1275 5225 50  0001 C CNN
F 1 "MOT_5V0" H 1276 5148 50  0000 C CNN
F 2 "" H 1275 4975 50  0001 C CNN
F 3 "" H 1275 4975 50  0001 C CNN
	1    1275 4975
	1    0    0    -1  
$EndComp
Wire Wire Line
	1275 5075 1275 4975
Connection ~ 1275 5075
$Comp
L PMSM_Driver_LV_Board_Device:TestPoint TP1
U 1 1 5F195276
P 1275 4550
F 0 "TP1" H 1225 4750 50  0000 L CNN
F 1 "TestPoint" H 1300 4600 25  0000 L CNN
F 2 "PMSM_Driver_LV_Board:TestPoint_Pad_1.0x1.0mm" H 1475 4550 50  0001 C CNN
F 3 "~" H 1475 4550 50  0001 C CNN
	1    1275 4550
	1    0    0    -1  
$EndComp
Wire Wire Line
	1675 4600 1275 4600
Wire Wire Line
	1275 4600 1275 4550
Connection ~ 1675 4600
Wire Wire Line
	1675 4600 1675 4050
$Comp
L PMSM_Driver_LV_Board_Device:TestPoint TP2
U 1 1 5F1A3415
P 3450 4550
F 0 "TP2" H 3400 4750 50  0000 L CNN
F 1 "TestPoint" H 3475 4600 25  0000 L CNN
F 2 "PMSM_Driver_LV_Board:TestPoint_Pad_1.0x1.0mm" H 3650 4550 50  0001 C CNN
F 3 "~" H 3650 4550 50  0001 C CNN
	1    3450 4550
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3450 4600 3450 4550
Wire Wire Line
	3450 4600 3200 4600
NoConn ~ 2725 4150
NoConn ~ 2725 4350
NoConn ~ 2725 4450
Wire Wire Line
	2825 3950 2825 4250
Connection ~ 2825 3950
Connection ~ 2825 5075
Wire Wire Line
	2825 5075 2925 5075
Wire Wire Line
	2925 5075 3250 5075
Wire Wire Line
	2725 4650 3200 4650
Wire Wire Line
	2725 4050 3200 4050
Wire Wire Line
	2725 4550 2825 4550
Connection ~ 2825 4550
Wire Wire Line
	2825 4550 2825 5075
Wire Wire Line
	2725 4250 2825 4250
Connection ~ 2825 4250
Wire Wire Line
	2825 4250 2825 4550
Connection ~ 3200 4600
Wire Wire Line
	3200 4600 3200 4650
Wire Wire Line
	3200 4050 3200 4600
Wire Notes Line
	600  6425 4025 6425
$Comp
L PMSM_Driver_LV_Board_IC:AD7403BRIZ U35
U 1 1 5F24E518
P 6250 2325
F 0 "U35" H 5975 2825 50  0000 C CNN
F 1 "AD7403BRIZ" H 6250 1825 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:SOIC-16W_7.5x12.8mm_P1.27mm" H 6250 1675 50  0001 C CNN
F 3 "~" H 5950 2825 50  0001 C CNN
F 4 "AD7403BRIZ -  Analogue to Digital Converter, 16 bit, Differential, Serial, Single, 4.5 V" H 6250 1625 31  0001 C CNN "Description"
F 5 "https://uk.farnell.com/analog-devices/ad7403briz/adc-16bit-soic-16/dp/2431665" H 6250 1575 31  0001 C CNN "Distributor Link"
F 6 "https://www.mouser.co.uk/ProductDetail/Analog-Devices/AD7403BRIZ?qs=sGAEpiMZZMt%2Foy9k%2F5obbVdPNrLrhFiKvGld2xGDVNA%3D" H 6250 1525 31  0001 C CNN "Distributor Link 2"
	1    6250 2325
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_IC:ADUM5000ARWZ U34
U 1 1 5F24E521
P 5775 4300
F 0 "U34" H 5475 4800 50  0000 C CNN
F 1 "ADUM5000ARWZ" H 5775 3800 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:SOIC-16W_7.5x10.3mm_P1.27mm" H 4875 4850 50  0001 C CNN
F 3 "~" H 4875 4850 50  0001 C CNN
F 4 "ADUM5000ARWZ -  DC-DC Switching Regulator, Adjustable, 2.7V to 5.5V in, 3.3V to 5.5V/100 mA out, 180 MHz, WSOIC-16" H 5775 3700 31  0001 C CNN "Description"
F 5 "https://uk.farnell.com/analog-devices/adum5000arwz/ic-isolated-dc-dc-conv-180mhz/dp/2102522" H 5775 3650 31  0001 C CNN "Distributor Link"
F 6 "https://www.mouser.co.uk/ProductDetail/Analog-Devices/ADUM5000ARWZ?qs=sGAEpiMZZMtitjHzVIkrqYThiPHWFX9aYDreduI%2FIuI%3D" H 5775 3600 31  0001 C CNN "Distributor Link 2"
	1    5775 4300
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_5V0 #PWR0279
U 1 1 5F24E527
P 6900 1100
F 0 "#PWR0279" H 6900 1350 50  0001 C CNN
F 1 "MOT_5V0" H 6901 1273 50  0000 C CNN
F 2 "" H 6900 1100 50  0001 C CNN
F 3 "" H 6900 1100 50  0001 C CNN
	1    6900 1100
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR0278
U 1 1 5F24E52D
P 6800 2775
F 0 "#PWR0278" H 6800 2525 50  0001 C CNN
F 1 "MOT_DGND" H 6805 2602 50  0000 C CNN
F 2 "" H 6800 2775 50  0001 C CNN
F 3 "" H 6800 2775 50  0001 C CNN
	1    6800 2775
	1    0    0    -1  
$EndComp
Wire Wire Line
	6800 2775 6800 2675
Wire Wire Line
	6800 1975 6700 1975
Wire Wire Line
	6700 2675 6800 2675
Connection ~ 6800 2675
Wire Wire Line
	6800 2675 6800 1975
Wire Wire Line
	6900 2175 6700 2175
NoConn ~ 6700 2075
NoConn ~ 6700 2375
NoConn ~ 6700 2575
Text GLabel 6900 2275 2    50   Input ~ 0
ISO_ADC_CLK
Wire Wire Line
	6900 2275 6700 2275
Text GLabel 6900 2475 2    50   Output ~ 0
ISO_CUR_B
Wire Wire Line
	6900 2475 6700 2475
Wire Wire Line
	4950 1450 4950 1500
Wire Wire Line
	5600 2575 5800 2575
Wire Wire Line
	5800 1975 5600 1975
Wire Wire Line
	5600 1975 5600 2575
Wire Wire Line
	5700 2800 5700 2675
Wire Wire Line
	5700 2675 5800 2675
Wire Wire Line
	5700 2675 5700 2275
Wire Wire Line
	5700 2275 5800 2275
Connection ~ 5700 2675
NoConn ~ 5800 2375
NoConn ~ 5800 2475
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 5F24E553
P 5075 2075
AR Path="/5F05E355/5F24E553" Ref="R?"  Part="1" 
AR Path="/5E9C6910/5F24E553" Ref="R?"  Part="1" 
AR Path="/5EFE605D/5F24E553" Ref="R?"  Part="1" 
AR Path="/5EA5E155/5F24E553" Ref="R152"  Part="1" 
F 0 "R152" V 5125 2225 50  0000 C CNN
F 1 "22" V 5125 1975 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 5075 2075 50  0001 C CNN
F 3 "~" H 5075 2075 50  0001 C CNN
F 4 "MCWR04X22R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 22 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 5075 1675 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x22r0ftl/res-22r-1-0-0625w-0402-thick-film/dp/2447138" H 5075 1575 50  0001 C CNN "Distributor Link"
	1    5075 2075
	0    -1   -1   0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 5F24E55B
P 5075 2375
AR Path="/5F05E355/5F24E55B" Ref="R?"  Part="1" 
AR Path="/5E9C6910/5F24E55B" Ref="R?"  Part="1" 
AR Path="/5EFE605D/5F24E55B" Ref="R?"  Part="1" 
AR Path="/5EA5E155/5F24E55B" Ref="R153"  Part="1" 
F 0 "R153" V 5125 2525 50  0000 C CNN
F 1 "22" V 5125 2275 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 5075 2375 50  0001 C CNN
F 3 "~" H 5075 2375 50  0001 C CNN
F 4 "MCWR04X22R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 22 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 5075 1975 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x22r0ftl/res-22r-1-0-0625w-0402-thick-film/dp/2447138" H 5075 1875 50  0001 C CNN "Distributor Link"
	1    5075 2375
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4825 2075 4975 2075
Wire Wire Line
	4500 2425 4500 2375
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5F24E565
P 5275 2225
AR Path="/5E9C6910/5F24E565" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5F24E565" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5F24E565" Ref="C?"  Part="1" 
AR Path="/5F05E355/5F24E565" Ref="C?"  Part="1" 
AR Path="/6068445B/5F24E565" Ref="C?"  Part="1" 
AR Path="/60E816A3/5F24E565" Ref="C?"  Part="1" 
AR Path="/5EFE605D/5F24E565" Ref="C?"  Part="1" 
AR Path="/5EA5E155/5F24E565" Ref="C120"  Part="1" 
F 0 "C120" H 5050 2300 50  0000 L CNN
F 1 "47pF" H 5050 2125 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 5275 2225 50  0001 C CNN
F 3 "~" H 5275 2225 50  0001 C CNN
F 4 "MC0402N470J500CT -  SMD Multilayer Ceramic Capacitor, 47 pF, 50 V, 0402 [1005 Metric], ± 5%, C0G / NP0, MC Series " H 5275 1825 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402n470j500ct/cap-47pf-50v-5-c0g-np0-0402/dp/1758959" H 5275 1725 50  0001 C CNN "Distributor Link"
	1    5275 2225
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5175 2075 5275 2075
Wire Wire Line
	5275 2075 5275 2125
Wire Wire Line
	5175 2375 5275 2375
Wire Wire Line
	5275 2375 5275 2325
Wire Wire Line
	5275 2375 5525 2375
Wire Wire Line
	5525 2375 5525 2175
Wire Wire Line
	5525 2175 5800 2175
Connection ~ 5275 2375
Wire Wire Line
	5800 2075 5275 2075
Connection ~ 5275 2075
Text Notes 4175 800  0    100  ~ 0
Phase B Current Measurement
Wire Notes Line
	4100 600  7525 600 
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5F24E57F
P 5275 1300
AR Path="/5E9C6910/5F24E57F" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5F24E57F" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5F24E57F" Ref="C?"  Part="1" 
AR Path="/5F05E355/5F24E57F" Ref="C?"  Part="1" 
AR Path="/6068445B/5F24E57F" Ref="C?"  Part="1" 
AR Path="/60E816A3/5F24E57F" Ref="C?"  Part="1" 
AR Path="/5EFE605D/5F24E57F" Ref="C?"  Part="1" 
AR Path="/5EA5E155/5F24E57F" Ref="C119"  Part="1" 
F 0 "C119" H 5000 1300 50  0000 L CNN
F 1 "1nF" H 5100 1225 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 5275 1300 50  0001 C CNN
F 3 "~" H 5275 1300 50  0001 C CNN
F 4 "MC0402B102K160CT -  SMD Multilayer Ceramic Capacitor, 1000 pF, 16 V, 0402 [1005 Metric], ± 10%, X7R, MC Series" H 5275 900 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402b102k160ct/cap-1000pf-16v-10-x7r-0402/dp/2320774" H 5275 800 50  0001 C CNN "Distributor Link"
	1    5275 1300
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5275 1400 5275 1450
Wire Wire Line
	5275 1150 5275 1200
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5F24E589
P 4950 1300
AR Path="/5E9C6910/5F24E589" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5F24E589" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5F24E589" Ref="C?"  Part="1" 
AR Path="/5F05E355/5F24E589" Ref="C?"  Part="1" 
AR Path="/6068445B/5F24E589" Ref="C?"  Part="1" 
AR Path="/60E816A3/5F24E589" Ref="C?"  Part="1" 
AR Path="/5EFE605D/5F24E589" Ref="C?"  Part="1" 
AR Path="/5EA5E155/5F24E589" Ref="C116"  Part="1" 
F 0 "C116" H 5025 1300 50  0000 L CNN
F 1 "10uF" H 4975 1225 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4950 1300 50  0001 C CNN
F 3 "~" H 4950 1300 50  0001 C CNN
F 4 "LMK107BBJ106MALT -  SMD Multilayer Ceramic Capacitor, 10 µF, 10 V, 0603 [1608 Metric], ± 20%, X5R, M Series" H 4950 900 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/taiyo-yuden/lmk107bbj106malt/cap-10-f-10v-20-x5r-0603/dp/2113070" H 4950 800 50  0001 C CNN "Distributor Link"
	1    4950 1300
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4950 1400 4950 1450
Wire Wire Line
	4950 1150 4950 1200
Connection ~ 4950 1450
Wire Wire Line
	5600 1100 5600 1150
Wire Wire Line
	4950 1450 5275 1450
Wire Wire Line
	4950 1150 5275 1150
Connection ~ 5275 1150
Wire Wire Line
	5275 1150 5600 1150
Connection ~ 5600 1150
Wire Wire Line
	6250 1450 6250 1500
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR0275
U 1 1 5F24E599
P 6250 1500
F 0 "#PWR0275" H 6250 1250 50  0001 C CNN
F 1 "MOT_DGND" H 6255 1327 50  0000 C CNN
F 2 "" H 6250 1500 50  0001 C CNN
F 3 "" H 6250 1500 50  0001 C CNN
	1    6250 1500
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6575 1400 6575 1450
Wire Wire Line
	6575 1150 6575 1200
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5F24E5A3
P 6250 1300
AR Path="/5E9C6910/5F24E5A3" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5F24E5A3" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5F24E5A3" Ref="C?"  Part="1" 
AR Path="/5F05E355/5F24E5A3" Ref="C?"  Part="1" 
AR Path="/6068445B/5F24E5A3" Ref="C?"  Part="1" 
AR Path="/60E816A3/5F24E5A3" Ref="C?"  Part="1" 
AR Path="/5EFE605D/5F24E5A3" Ref="C?"  Part="1" 
AR Path="/5EA5E155/5F24E5A3" Ref="C124"  Part="1" 
F 0 "C124" H 6325 1300 50  0000 L CNN
F 1 "10uF" H 6275 1225 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 6250 1300 50  0001 C CNN
F 3 "~" H 6250 1300 50  0001 C CNN
F 4 "LMK107BBJ106MALT -  SMD Multilayer Ceramic Capacitor, 10 µF, 10 V, 0603 [1608 Metric], ± 20%, X5R, M Series" H 6250 900 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/taiyo-yuden/lmk107bbj106malt/cap-10-f-10v-20-x5r-0603/dp/2113070" H 6250 800 50  0001 C CNN "Distributor Link"
	1    6250 1300
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6250 1400 6250 1450
Wire Wire Line
	6250 1150 6250 1200
Connection ~ 6250 1450
Wire Wire Line
	6900 1100 6900 1150
Wire Wire Line
	6250 1450 6575 1450
Wire Wire Line
	6250 1150 6575 1150
Connection ~ 6575 1150
Wire Wire Line
	6575 1150 6900 1150
Connection ~ 6900 1150
Connection ~ 5600 1975
Wire Wire Line
	6900 1150 6900 2175
Wire Wire Line
	5600 1150 5600 1975
Wire Wire Line
	4900 2875 4900 2925
Wire Wire Line
	5225 2825 5225 2875
Wire Wire Line
	5225 2575 5225 2625
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5F24E5BA
P 4900 2725
AR Path="/5E9C6910/5F24E5BA" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5F24E5BA" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5F24E5BA" Ref="C?"  Part="1" 
AR Path="/5F05E355/5F24E5BA" Ref="C?"  Part="1" 
AR Path="/6068445B/5F24E5BA" Ref="C?"  Part="1" 
AR Path="/60E816A3/5F24E5BA" Ref="C?"  Part="1" 
AR Path="/5EFE605D/5F24E5BA" Ref="C?"  Part="1" 
AR Path="/5EA5E155/5F24E5BA" Ref="C115"  Part="1" 
F 0 "C115" H 4675 2800 50  0000 L CNN
F 1 "10uF" H 4700 2650 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4900 2725 50  0001 C CNN
F 3 "~" H 4900 2725 50  0001 C CNN
F 4 "LMK107BBJ106MALT -  SMD Multilayer Ceramic Capacitor, 10 µF, 10 V, 0603 [1608 Metric], ± 20%, X5R, M Series" H 4900 2325 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/taiyo-yuden/lmk107bbj106malt/cap-10-f-10v-20-x5r-0603/dp/2113070" H 4900 2225 50  0001 C CNN "Distributor Link"
	1    4900 2725
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4900 2825 4900 2875
Wire Wire Line
	4900 2575 4900 2625
Connection ~ 4900 2875
Wire Wire Line
	4900 2875 5225 2875
Wire Wire Line
	4900 2575 5225 2575
Wire Wire Line
	4500 2375 4975 2375
Connection ~ 5600 2575
Wire Wire Line
	5600 2575 5225 2575
Connection ~ 5225 2575
Text Notes 5950 2900 0    25   ~ 0
Isolated sigma-delta modulator
Text Notes 5475 4875 0    25   ~ 0
Isolated floating power supply
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5F24E5DA
P 6575 1300
AR Path="/5E9C6910/5F24E5DA" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5F24E5DA" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5F24E5DA" Ref="C?"  Part="1" 
AR Path="/5F05E355/5F24E5DA" Ref="C?"  Part="1" 
AR Path="/6068445B/5F24E5DA" Ref="C?"  Part="1" 
AR Path="/60E816A3/5F24E5DA" Ref="C?"  Part="1" 
AR Path="/5EFE605D/5F24E5DA" Ref="C?"  Part="1" 
AR Path="/5EA5E155/5F24E5DA" Ref="C126"  Part="1" 
F 0 "C126" H 6300 1300 50  0000 L CNN
F 1 "1nF" H 6400 1225 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 6575 1300 50  0001 C CNN
F 3 "~" H 6575 1300 50  0001 C CNN
F 4 "MC0402B102K160CT -  SMD Multilayer Ceramic Capacitor, 1000 pF, 16 V, 0402 [1005 Metric], ± 10%, X7R, MC Series" H 6575 900 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402b102k160ct/cap-1000pf-16v-10-x7r-0402/dp/2320774" H 6575 800 50  0001 C CNN "Distributor Link"
	1    6575 1300
	-1   0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5F24E5E2
P 5225 2725
AR Path="/5E9C6910/5F24E5E2" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5F24E5E2" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5F24E5E2" Ref="C?"  Part="1" 
AR Path="/5F05E355/5F24E5E2" Ref="C?"  Part="1" 
AR Path="/6068445B/5F24E5E2" Ref="C?"  Part="1" 
AR Path="/60E816A3/5F24E5E2" Ref="C?"  Part="1" 
AR Path="/5EFE605D/5F24E5E2" Ref="C?"  Part="1" 
AR Path="/5EA5E155/5F24E5E2" Ref="C118"  Part="1" 
F 0 "C118" H 4950 2725 50  0000 L CNN
F 1 "1nF" H 5050 2650 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 5225 2725 50  0001 C CNN
F 3 "~" H 5225 2725 50  0001 C CNN
F 4 "MC0402B102K160CT -  SMD Multilayer Ceramic Capacitor, 1000 pF, 16 V, 0402 [1005 Metric], ± 10%, X7R, MC Series" H 5225 2325 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402b102k160ct/cap-1000pf-16v-10-x7r-0402/dp/2320774" H 5225 2225 50  0001 C CNN "Distributor Link"
	1    5225 2725
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6100 5375 6100 5425
Wire Wire Line
	6425 5325 6425 5375
Wire Wire Line
	6425 5075 6425 5125
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5F24E5ED
P 6425 5225
AR Path="/5E9C6910/5F24E5ED" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5F24E5ED" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5F24E5ED" Ref="C?"  Part="1" 
AR Path="/5F05E355/5F24E5ED" Ref="C?"  Part="1" 
AR Path="/6068445B/5F24E5ED" Ref="C?"  Part="1" 
AR Path="/60E816A3/5F24E5ED" Ref="C?"  Part="1" 
AR Path="/5EFE605D/5F24E5ED" Ref="C?"  Part="1" 
AR Path="/5EA5E155/5F24E5ED" Ref="C125"  Part="1" 
F 0 "C125" H 6225 5300 50  0000 L CNN
F 1 "10uF" H 6225 5150 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 6425 5225 50  0001 C CNN
F 3 "~" H 6425 5225 50  0001 C CNN
F 4 "LMK107BBJ106MALT -  SMD Multilayer Ceramic Capacitor, 10 µF, 10 V, 0603 [1608 Metric], ± 20%, X5R, M Series" H 6425 4825 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/taiyo-yuden/lmk107bbj106malt/cap-10-f-10v-20-x5r-0603/dp/2113070" H 6425 4725 50  0001 C CNN "Distributor Link"
	1    6425 5225
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6100 5325 6100 5375
Wire Wire Line
	6100 5075 6100 5125
Connection ~ 6100 5375
Wire Wire Line
	6100 5375 6425 5375
Wire Wire Line
	6100 5075 6325 5075
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5F24E600
P 6100 5225
AR Path="/5E9C6910/5F24E600" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5F24E600" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5F24E600" Ref="C?"  Part="1" 
AR Path="/5F05E355/5F24E600" Ref="C?"  Part="1" 
AR Path="/5EA41912/5F24E600" Ref="C?"  Part="1" 
AR Path="/5EFE605D/5F24E600" Ref="C?"  Part="1" 
AR Path="/5EA5E155/5F24E600" Ref="C123"  Part="1" 
F 0 "C123" H 5900 5300 50  0000 L CNN
F 1 "100nF" H 5850 5150 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 6100 5225 50  0001 C CNN
F 3 "~" H 6100 5225 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 6100 4825 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 6100 4725 50  0001 C CNN "Distributor Link"
	1    6100 5225
	-1   0    0    -1  
$EndComp
NoConn ~ 5275 4150
NoConn ~ 5275 4250
NoConn ~ 5275 4350
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR0271
U 1 1 5F24E609
P 5175 4750
F 0 "#PWR0271" H 5175 4500 50  0001 C CNN
F 1 "MOT_DGND" H 5180 4577 50  0000 C CNN
F 2 "" H 5175 4750 50  0001 C CNN
F 3 "" H 5175 4750 50  0001 C CNN
	1    5175 4750
	1    0    0    -1  
$EndComp
Wire Wire Line
	5175 4750 5175 4650
Wire Wire Line
	5175 4650 5275 4650
Wire Wire Line
	6700 4750 6700 4650
Connection ~ 6700 4650
Wire Wire Line
	5175 4650 5175 4600
Wire Wire Line
	5175 4050 5275 4050
Connection ~ 5175 4650
$Comp
L PMSM_Driver_LV_Board_Power:MOT_5V0 #PWR0270
U 1 1 5F24E61C
P 5075 3850
F 0 "#PWR0270" H 5075 4100 50  0001 C CNN
F 1 "MOT_5V0" H 5076 4023 50  0000 C CNN
F 2 "" H 5075 3850 50  0001 C CNN
F 3 "" H 5075 3850 50  0001 C CNN
	1    5075 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	5075 3850 5075 3950
Wire Wire Line
	5075 3950 5275 3950
Wire Wire Line
	5275 4550 5075 4550
Wire Wire Line
	5075 4550 5075 4450
Connection ~ 5075 3950
Wire Wire Line
	5275 4450 5075 4450
Connection ~ 5075 4450
Wire Wire Line
	5075 4450 5075 3950
$Comp
L PMSM_Driver_LV_Board_Device:Ferrite_Bead_Small FB2
U 1 1 5F24E62D
P 6900 3950
F 0 "FB2" V 7025 3875 50  0000 L CNN
F 1 "Z0805C420APWST" V 6775 3775 25  0000 L CNN
F 2 "PMSM_Driver_LV_Board:L_0805_2012Metric" V 6830 3950 50  0001 C CNN
F 3 "~" H 6900 3950 50  0001 C CNN
F 4 "Z0805C420APWST -  Ferrite Bead, 0805 [2012 Metric], 42 ohm, 4 A, Z-PWS Series, 0.008 ohm, ± 25%" H 6900 3950 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/kemet/z0805c420apwst/ferrite-bead-42-ohm-4a-0805/dp/2846741" H 6900 3950 50  0001 C CNN "Distributor Link"
F 6 "42 Ohms at 100 MHz" V 6725 3725 25  0000 L CNN "Impedance"
	1    6900 3950
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7000 3950 7100 3950
Wire Wire Line
	7100 3950 7100 3850
Wire Wire Line
	6425 5375 6750 5375
Wire Wire Line
	6750 5325 6750 5375
Wire Wire Line
	6750 5075 6750 5125
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5F24E640
P 6750 5225
AR Path="/5E9C6910/5F24E640" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5F24E640" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5F24E640" Ref="C?"  Part="1" 
AR Path="/5F05E355/5F24E640" Ref="C?"  Part="1" 
AR Path="/5EA41912/5F24E640" Ref="C?"  Part="1" 
AR Path="/5EFE605D/5F24E640" Ref="C?"  Part="1" 
AR Path="/5EA5E155/5F24E640" Ref="C127"  Part="1" 
F 0 "C127" H 6550 5300 50  0000 L CNN
F 1 "100nF" H 6500 5150 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 6750 5225 50  0001 C CNN
F 3 "~" H 6750 5225 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 6750 4825 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 6750 4725 50  0001 C CNN "Distributor Link"
	1    6750 5225
	-1   0    0    -1  
$EndComp
Wire Wire Line
	7075 5325 7075 5375
Wire Wire Line
	7075 5075 7075 5125
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5F24E64A
P 7075 5225
AR Path="/5E9C6910/5F24E64A" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5F24E64A" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5F24E64A" Ref="C?"  Part="1" 
AR Path="/5F05E355/5F24E64A" Ref="C?"  Part="1" 
AR Path="/6068445B/5F24E64A" Ref="C?"  Part="1" 
AR Path="/60E816A3/5F24E64A" Ref="C?"  Part="1" 
AR Path="/5EFE605D/5F24E64A" Ref="C?"  Part="1" 
AR Path="/5EA5E155/5F24E64A" Ref="C128"  Part="1" 
F 0 "C128" H 6875 5300 50  0000 L CNN
F 1 "10uF" H 6875 5150 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 7075 5225 50  0001 C CNN
F 3 "~" H 7075 5225 50  0001 C CNN
F 4 "LMK107BBJ106MALT -  SMD Multilayer Ceramic Capacitor, 10 µF, 10 V, 0603 [1608 Metric], ± 20%, X5R, M Series" H 7075 4825 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/taiyo-yuden/lmk107bbj106malt/cap-10-f-10v-20-x5r-0603/dp/2113070" H 7075 4725 50  0001 C CNN "Distributor Link"
	1    7075 5225
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6750 5375 7075 5375
Wire Wire Line
	6750 5075 7075 5075
Wire Wire Line
	6225 3950 6325 3950
Connection ~ 6425 5075
Connection ~ 6425 5375
Connection ~ 6750 5075
Connection ~ 6750 5375
Wire Wire Line
	4775 5375 4775 5425
Wire Wire Line
	5100 5325 5100 5375
Wire Wire Line
	5100 5075 5100 5125
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5F24E65C
P 5100 5225
AR Path="/5E9C6910/5F24E65C" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5F24E65C" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5F24E65C" Ref="C?"  Part="1" 
AR Path="/5F05E355/5F24E65C" Ref="C?"  Part="1" 
AR Path="/6068445B/5F24E65C" Ref="C?"  Part="1" 
AR Path="/60E816A3/5F24E65C" Ref="C?"  Part="1" 
AR Path="/5EFE605D/5F24E65C" Ref="C?"  Part="1" 
AR Path="/5EA5E155/5F24E65C" Ref="C117"  Part="1" 
F 0 "C117" H 4900 5300 50  0000 L CNN
F 1 "10uF" H 4900 5150 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 5100 5225 50  0001 C CNN
F 3 "~" H 5100 5225 50  0001 C CNN
F 4 "LMK107BBJ106MALT -  SMD Multilayer Ceramic Capacitor, 10 µF, 10 V, 0603 [1608 Metric], ± 20%, X5R, M Series" H 5100 4825 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/taiyo-yuden/lmk107bbj106malt/cap-10-f-10v-20-x5r-0603/dp/2113070" H 5100 4725 50  0001 C CNN "Distributor Link"
	1    5100 5225
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4775 5325 4775 5375
Wire Wire Line
	4775 5075 4775 5125
Connection ~ 4775 5375
Wire Wire Line
	4775 5375 5100 5375
Wire Wire Line
	4775 5075 5100 5075
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5F24E669
P 4775 5225
AR Path="/5E9C6910/5F24E669" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5F24E669" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5F24E669" Ref="C?"  Part="1" 
AR Path="/5F05E355/5F24E669" Ref="C?"  Part="1" 
AR Path="/5EA41912/5F24E669" Ref="C?"  Part="1" 
AR Path="/5EFE605D/5F24E669" Ref="C?"  Part="1" 
AR Path="/5EA5E155/5F24E669" Ref="C114"  Part="1" 
F 0 "C114" H 4575 5300 50  0000 L CNN
F 1 "100nF" H 4550 5150 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 4775 5225 50  0001 C CNN
F 3 "~" H 4775 5225 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 4775 4825 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 4775 4725 50  0001 C CNN "Distributor Link"
	1    4775 5225
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5100 5375 5425 5375
Wire Wire Line
	5425 5325 5425 5375
Wire Wire Line
	5425 5075 5425 5125
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5F24E674
P 5425 5225
AR Path="/5E9C6910/5F24E674" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5F24E674" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5F24E674" Ref="C?"  Part="1" 
AR Path="/5F05E355/5F24E674" Ref="C?"  Part="1" 
AR Path="/5EA41912/5F24E674" Ref="C?"  Part="1" 
AR Path="/5EFE605D/5F24E674" Ref="C?"  Part="1" 
AR Path="/5EA5E155/5F24E674" Ref="C121"  Part="1" 
F 0 "C121" H 5225 5300 50  0000 L CNN
F 1 "100nF" H 5175 5150 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 5425 5225 50  0001 C CNN
F 3 "~" H 5425 5225 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 5425 4825 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 5425 4725 50  0001 C CNN "Distributor Link"
	1    5425 5225
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5750 5325 5750 5375
Wire Wire Line
	5750 5075 5750 5125
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5F24E67E
P 5750 5225
AR Path="/5E9C6910/5F24E67E" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5F24E67E" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5F24E67E" Ref="C?"  Part="1" 
AR Path="/5F05E355/5F24E67E" Ref="C?"  Part="1" 
AR Path="/6068445B/5F24E67E" Ref="C?"  Part="1" 
AR Path="/60E816A3/5F24E67E" Ref="C?"  Part="1" 
AR Path="/5EFE605D/5F24E67E" Ref="C?"  Part="1" 
AR Path="/5EA5E155/5F24E67E" Ref="C122"  Part="1" 
F 0 "C122" H 5550 5300 50  0000 L CNN
F 1 "10uF" H 5550 5150 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 5750 5225 50  0001 C CNN
F 3 "~" H 5750 5225 50  0001 C CNN
F 4 "LMK107BBJ106MALT -  SMD Multilayer Ceramic Capacitor, 10 µF, 10 V, 0603 [1608 Metric], ± 20%, X5R, M Series" H 5750 4825 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/taiyo-yuden/lmk107bbj106malt/cap-10-f-10v-20-x5r-0603/dp/2113070" H 5750 4725 50  0001 C CNN "Distributor Link"
	1    5750 5225
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5425 5375 5750 5375
Wire Wire Line
	5425 5075 5750 5075
Connection ~ 5100 5075
Connection ~ 5100 5375
Connection ~ 5425 5075
Connection ~ 5425 5375
Wire Wire Line
	5100 5075 5425 5075
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR0267
U 1 1 5F24E68B
P 4775 5425
F 0 "#PWR0267" H 4775 5175 50  0001 C CNN
F 1 "MOT_DGND" H 4780 5252 50  0000 C CNN
F 2 "" H 4775 5425 50  0001 C CNN
F 3 "" H 4775 5425 50  0001 C CNN
	1    4775 5425
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_5V0 #PWR0266
U 1 1 5F24E691
P 4775 4975
F 0 "#PWR0266" H 4775 5225 50  0001 C CNN
F 1 "MOT_5V0" H 4776 5148 50  0000 C CNN
F 2 "" H 4775 4975 50  0001 C CNN
F 3 "" H 4775 4975 50  0001 C CNN
	1    4775 4975
	1    0    0    -1  
$EndComp
Wire Wire Line
	4775 5075 4775 4975
Connection ~ 4775 5075
$Comp
L PMSM_Driver_LV_Board_Device:TestPoint TP3
U 1 1 5F24E699
P 4775 4550
F 0 "TP3" H 4725 4750 50  0000 L CNN
F 1 "TestPoint" H 4800 4600 25  0000 L CNN
F 2 "PMSM_Driver_LV_Board:TestPoint_Pad_1.0x1.0mm" H 4975 4550 50  0001 C CNN
F 3 "~" H 4975 4550 50  0001 C CNN
	1    4775 4550
	1    0    0    -1  
$EndComp
Wire Wire Line
	5175 4600 4775 4600
Wire Wire Line
	4775 4600 4775 4550
Connection ~ 5175 4600
Wire Wire Line
	5175 4600 5175 4050
$Comp
L PMSM_Driver_LV_Board_Device:TestPoint TP4
U 1 1 5F24E6A3
P 6950 4550
F 0 "TP4" H 6900 4750 50  0000 L CNN
F 1 "TestPoint" H 6975 4600 25  0000 L CNN
F 2 "PMSM_Driver_LV_Board:TestPoint_Pad_1.0x1.0mm" H 7150 4550 50  0001 C CNN
F 3 "~" H 7150 4550 50  0001 C CNN
	1    6950 4550
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6950 4600 6950 4550
Wire Wire Line
	6950 4600 6700 4600
NoConn ~ 6225 4150
NoConn ~ 6225 4350
NoConn ~ 6225 4450
Wire Wire Line
	6325 3950 6325 4250
Connection ~ 6325 3950
Connection ~ 6325 5075
Wire Wire Line
	6325 5075 6425 5075
Wire Wire Line
	6425 5075 6750 5075
Wire Wire Line
	6325 3950 6425 3950
Wire Wire Line
	6225 4650 6700 4650
Wire Wire Line
	6225 4050 6700 4050
Wire Wire Line
	6225 4550 6325 4550
Connection ~ 6325 4550
Wire Wire Line
	6325 4550 6325 5075
Wire Wire Line
	6225 4250 6325 4250
Connection ~ 6325 4250
Wire Wire Line
	6325 4250 6325 4550
Connection ~ 6700 4600
Wire Wire Line
	6700 4600 6700 4650
Wire Wire Line
	6700 4050 6700 4600
Wire Notes Line
	4100 6425 7525 6425
$Comp
L PMSM_Driver_LV_Board_Power:MOT_SENS_B_5V0 #PWR0280
U 1 1 5F2AE62E
P 7100 3850
F 0 "#PWR0280" H 7100 4100 50  0001 C CNN
F 1 "MOT_SENS_B_5V0" H 7101 4023 50  0000 C CNN
F 2 "" H 7100 3850 50  0001 C CNN
F 3 "" H 7100 3850 50  0001 C CNN
	1    7100 3850
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_SENS_B_AGND #PWR?
U 1 1 5F2F6745
P 6100 5425
AR Path="/5EA41912/5F2F6745" Ref="#PWR?"  Part="1" 
AR Path="/5F2F6745" Ref="#PWR?"  Part="1" 
AR Path="/5EFE605D/5F2F6745" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E155/5F2F6745" Ref="#PWR0274"  Part="1" 
F 0 "#PWR0274" H 6100 5175 50  0001 C CNN
F 1 "MOT_SENS_B_AGND" H 6105 5252 50  0000 C CNN
F 2 "" H 6100 5425 50  0001 C CNN
F 3 "" H 6100 5425 50  0001 C CNN
	1    6100 5425
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_SENS_B_AGND #PWR?
U 1 1 5F31A3F3
P 4900 2925
AR Path="/5EA41912/5F31A3F3" Ref="#PWR?"  Part="1" 
AR Path="/5F31A3F3" Ref="#PWR?"  Part="1" 
AR Path="/5EFE605D/5F31A3F3" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E155/5F31A3F3" Ref="#PWR0268"  Part="1" 
F 0 "#PWR0268" H 4900 2675 50  0001 C CNN
F 1 "MOT_SENS_B_AGND" H 4905 2752 50  0000 C CNN
F 2 "" H 4900 2925 50  0001 C CNN
F 3 "" H 4900 2925 50  0001 C CNN
	1    4900 2925
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_SENS_B_AGND #PWR?
U 1 1 5F326611
P 4500 2425
AR Path="/5EA41912/5F326611" Ref="#PWR?"  Part="1" 
AR Path="/5F326611" Ref="#PWR?"  Part="1" 
AR Path="/5EFE605D/5F326611" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E155/5F326611" Ref="#PWR0265"  Part="1" 
F 0 "#PWR0265" H 4500 2175 50  0001 C CNN
F 1 "MOT_SENS_B_AGND" H 4505 2252 50  0000 C CNN
F 2 "" H 4500 2425 50  0001 C CNN
F 3 "" H 4500 2425 50  0001 C CNN
	1    4500 2425
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_SENS_B_AGND #PWR?
U 1 1 5F33E3F4
P 5700 2800
AR Path="/5EA41912/5F33E3F4" Ref="#PWR?"  Part="1" 
AR Path="/5F33E3F4" Ref="#PWR?"  Part="1" 
AR Path="/5EFE605D/5F33E3F4" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E155/5F33E3F4" Ref="#PWR0273"  Part="1" 
F 0 "#PWR0273" H 5700 2550 50  0001 C CNN
F 1 "MOT_SENS_B_AGND" H 5705 2627 50  0000 C CNN
F 2 "" H 5700 2800 50  0001 C CNN
F 3 "" H 5700 2800 50  0001 C CNN
	1    5700 2800
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_SENS_T_AGND #PWR?
U 1 1 5FE8A386
P 9600 5425
AR Path="/5FD7CF82/5FE8A386" Ref="#PWR?"  Part="1" 
AR Path="/5EFE605D/5FE8A386" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E155/5FE8A386" Ref="#PWR0291"  Part="1" 
F 0 "#PWR0291" H 9600 5175 50  0001 C CNN
F 1 "MOT_SENS_T_AGND" H 9605 5252 50  0000 C CNN
F 2 "" H 9600 5425 50  0001 C CNN
F 3 "" H 9600 5425 50  0001 C CNN
	1    9600 5425
	1    0    0    -1  
$EndComp
Text GLabel 8325 2075 0    50   Input ~ 0
MOT_SHUNT_T
$Comp
L PMSM_Driver_LV_Board_Power:MOT_SENS_T_5V0 #PWR0297
U 1 1 5FE96756
P 10600 3850
F 0 "#PWR0297" H 10600 4100 50  0001 C CNN
F 1 "MOT_SENS_T_5V0" H 10601 4023 50  0000 C CNN
F 2 "" H 10600 3850 50  0001 C CNN
F 3 "" H 10600 3850 50  0001 C CNN
	1    10600 3850
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_IC:AD7403BRIZ U37
U 1 1 5FEAC5BA
P 9750 2325
F 0 "U37" H 9475 2825 50  0000 C CNN
F 1 "AD7403BRIZ" H 9750 1825 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:SOIC-16W_7.5x12.8mm_P1.27mm" H 9750 1675 50  0001 C CNN
F 3 "~" H 9450 2825 50  0001 C CNN
F 4 "AD7403BRIZ -  Analogue to Digital Converter, 16 bit, Differential, Serial, Single, 4.5 V" H 9750 1625 31  0001 C CNN "Description"
F 5 "https://uk.farnell.com/analog-devices/ad7403briz/adc-16bit-soic-16/dp/2431665" H 9750 1575 31  0001 C CNN "Distributor Link"
F 6 "https://www.mouser.co.uk/ProductDetail/Analog-Devices/AD7403BRIZ?qs=sGAEpiMZZMt%2Foy9k%2F5obbVdPNrLrhFiKvGld2xGDVNA%3D" H 9750 1525 31  0001 C CNN "Distributor Link 2"
	1    9750 2325
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_IC:ADUM5000ARWZ U36
U 1 1 5FEAC5C3
P 9275 4300
F 0 "U36" H 8975 4800 50  0000 C CNN
F 1 "ADUM5000ARWZ" H 9275 3800 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:SOIC-16W_7.5x10.3mm_P1.27mm" H 8375 4850 50  0001 C CNN
F 3 "~" H 8375 4850 50  0001 C CNN
F 4 "ADUM5000ARWZ -  DC-DC Switching Regulator, Adjustable, 2.7V to 5.5V in, 3.3V to 5.5V/100 mA out, 180 MHz, WSOIC-16" H 9275 3700 31  0001 C CNN "Description"
F 5 "https://uk.farnell.com/analog-devices/adum5000arwz/ic-isolated-dc-dc-conv-180mhz/dp/2102522" H 9275 3650 31  0001 C CNN "Distributor Link"
F 6 "https://www.mouser.co.uk/ProductDetail/Analog-Devices/ADUM5000ARWZ?qs=sGAEpiMZZMtitjHzVIkrqYThiPHWFX9aYDreduI%2FIuI%3D" H 9275 3600 31  0001 C CNN "Distributor Link 2"
	1    9275 4300
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_5V0 #PWR0296
U 1 1 5FEAC5C9
P 10400 1100
F 0 "#PWR0296" H 10400 1350 50  0001 C CNN
F 1 "MOT_5V0" H 10401 1273 50  0000 C CNN
F 2 "" H 10400 1100 50  0001 C CNN
F 3 "" H 10400 1100 50  0001 C CNN
	1    10400 1100
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR0295
U 1 1 5FEAC5CF
P 10300 2775
F 0 "#PWR0295" H 10300 2525 50  0001 C CNN
F 1 "MOT_DGND" H 10305 2602 50  0000 C CNN
F 2 "" H 10300 2775 50  0001 C CNN
F 3 "" H 10300 2775 50  0001 C CNN
	1    10300 2775
	1    0    0    -1  
$EndComp
Wire Wire Line
	10300 2775 10300 2675
Wire Wire Line
	10300 1975 10200 1975
Wire Wire Line
	10200 2675 10300 2675
Connection ~ 10300 2675
Wire Wire Line
	10300 2675 10300 1975
Wire Wire Line
	10400 2175 10200 2175
NoConn ~ 10200 2075
NoConn ~ 10200 2375
NoConn ~ 10200 2575
Text GLabel 10400 2275 2    50   Input ~ 0
ISO_ADC_CLK
Wire Wire Line
	10400 2275 10200 2275
Wire Wire Line
	10400 2475 10200 2475
Wire Wire Line
	8450 1450 8450 1500
Wire Wire Line
	9100 2575 9300 2575
Wire Wire Line
	9300 1975 9100 1975
Wire Wire Line
	9100 1975 9100 2575
Wire Wire Line
	9200 2800 9200 2675
Wire Wire Line
	9200 2675 9300 2675
Wire Wire Line
	9200 2675 9200 2275
Wire Wire Line
	9200 2275 9300 2275
Connection ~ 9200 2675
NoConn ~ 9300 2375
NoConn ~ 9300 2475
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 5FEAC5EF
P 8575 2075
AR Path="/5F05E355/5FEAC5EF" Ref="R?"  Part="1" 
AR Path="/5E9C6910/5FEAC5EF" Ref="R?"  Part="1" 
AR Path="/5EFE605D/5FEAC5EF" Ref="R?"  Part="1" 
AR Path="/5EA5E155/5FEAC5EF" Ref="R154"  Part="1" 
F 0 "R154" V 8625 2225 50  0000 C CNN
F 1 "22" V 8625 1975 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 8575 2075 50  0001 C CNN
F 3 "~" H 8575 2075 50  0001 C CNN
F 4 "MCWR04X22R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 22 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 8575 1675 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x22r0ftl/res-22r-1-0-0625w-0402-thick-film/dp/2447138" H 8575 1575 50  0001 C CNN "Distributor Link"
	1    8575 2075
	0    -1   -1   0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 5FEAC5F7
P 8575 2375
AR Path="/5F05E355/5FEAC5F7" Ref="R?"  Part="1" 
AR Path="/5E9C6910/5FEAC5F7" Ref="R?"  Part="1" 
AR Path="/5EFE605D/5FEAC5F7" Ref="R?"  Part="1" 
AR Path="/5EA5E155/5FEAC5F7" Ref="R155"  Part="1" 
F 0 "R155" V 8625 2525 50  0000 C CNN
F 1 "22" V 8625 2275 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 8575 2375 50  0001 C CNN
F 3 "~" H 8575 2375 50  0001 C CNN
F 4 "MCWR04X22R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 22 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 8575 1975 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x22r0ftl/res-22r-1-0-0625w-0402-thick-film/dp/2447138" H 8575 1875 50  0001 C CNN "Distributor Link"
	1    8575 2375
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8325 2075 8475 2075
Wire Wire Line
	8000 2425 8000 2375
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5FEAC601
P 8775 2225
AR Path="/5E9C6910/5FEAC601" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5FEAC601" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5FEAC601" Ref="C?"  Part="1" 
AR Path="/5F05E355/5FEAC601" Ref="C?"  Part="1" 
AR Path="/6068445B/5FEAC601" Ref="C?"  Part="1" 
AR Path="/60E816A3/5FEAC601" Ref="C?"  Part="1" 
AR Path="/5EFE605D/5FEAC601" Ref="C?"  Part="1" 
AR Path="/5EA5E155/5FEAC601" Ref="C135"  Part="1" 
F 0 "C135" H 8575 2300 50  0000 L CNN
F 1 "47pF" H 8575 2150 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 8775 2225 50  0001 C CNN
F 3 "~" H 8775 2225 50  0001 C CNN
F 4 "MC0402N470J500CT -  SMD Multilayer Ceramic Capacitor, 47 pF, 50 V, 0402 [1005 Metric], ± 5%, C0G / NP0, MC Series " H 8775 1825 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402n470j500ct/cap-47pf-50v-5-c0g-np0-0402/dp/1758959" H 8775 1725 50  0001 C CNN "Distributor Link"
	1    8775 2225
	-1   0    0    -1  
$EndComp
Wire Wire Line
	8675 2075 8775 2075
Wire Wire Line
	8775 2075 8775 2125
Wire Wire Line
	8675 2375 8775 2375
Wire Wire Line
	8775 2375 8775 2325
Wire Wire Line
	8775 2375 9025 2375
Wire Wire Line
	9025 2375 9025 2175
Wire Wire Line
	9025 2175 9300 2175
Connection ~ 8775 2375
Wire Wire Line
	9300 2075 8775 2075
Connection ~ 8775 2075
Text Notes 7675 800  0    100  ~ 0
Total Current Measurement
Wire Notes Line
	7600 600  11025 600 
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5FEAC615
P 8775 1300
AR Path="/5E9C6910/5FEAC615" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5FEAC615" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5FEAC615" Ref="C?"  Part="1" 
AR Path="/5F05E355/5FEAC615" Ref="C?"  Part="1" 
AR Path="/6068445B/5FEAC615" Ref="C?"  Part="1" 
AR Path="/60E816A3/5FEAC615" Ref="C?"  Part="1" 
AR Path="/5EFE605D/5FEAC615" Ref="C?"  Part="1" 
AR Path="/5EA5E155/5FEAC615" Ref="C134"  Part="1" 
F 0 "C134" H 8500 1300 50  0000 L CNN
F 1 "1nF" H 8600 1225 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 8775 1300 50  0001 C CNN
F 3 "~" H 8775 1300 50  0001 C CNN
F 4 "MC0402B102K160CT -  SMD Multilayer Ceramic Capacitor, 1000 pF, 16 V, 0402 [1005 Metric], ± 10%, X7R, MC Series" H 8775 900 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402b102k160ct/cap-1000pf-16v-10-x7r-0402/dp/2320774" H 8775 800 50  0001 C CNN "Distributor Link"
	1    8775 1300
	-1   0    0    -1  
$EndComp
Wire Wire Line
	8775 1400 8775 1450
Wire Wire Line
	8775 1150 8775 1200
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5FEAC61F
P 8450 1300
AR Path="/5E9C6910/5FEAC61F" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5FEAC61F" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5FEAC61F" Ref="C?"  Part="1" 
AR Path="/5F05E355/5FEAC61F" Ref="C?"  Part="1" 
AR Path="/6068445B/5FEAC61F" Ref="C?"  Part="1" 
AR Path="/60E816A3/5FEAC61F" Ref="C?"  Part="1" 
AR Path="/5EFE605D/5FEAC61F" Ref="C?"  Part="1" 
AR Path="/5EA5E155/5FEAC61F" Ref="C131"  Part="1" 
F 0 "C131" H 8525 1300 50  0000 L CNN
F 1 "10uF" H 8475 1225 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 8450 1300 50  0001 C CNN
F 3 "~" H 8450 1300 50  0001 C CNN
F 4 "LMK107BBJ106MALT -  SMD Multilayer Ceramic Capacitor, 10 µF, 10 V, 0603 [1608 Metric], ± 20%, X5R, M Series" H 8450 900 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/taiyo-yuden/lmk107bbj106malt/cap-10-f-10v-20-x5r-0603/dp/2113070" H 8450 800 50  0001 C CNN "Distributor Link"
	1    8450 1300
	-1   0    0    -1  
$EndComp
Wire Wire Line
	8450 1400 8450 1450
Wire Wire Line
	8450 1150 8450 1200
Connection ~ 8450 1450
Wire Wire Line
	9100 1100 9100 1150
Wire Wire Line
	8450 1450 8775 1450
Wire Wire Line
	8450 1150 8775 1150
Connection ~ 8775 1150
Wire Wire Line
	8775 1150 9100 1150
Connection ~ 9100 1150
Wire Wire Line
	9750 1450 9750 1500
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR0292
U 1 1 5FEAC62F
P 9750 1500
F 0 "#PWR0292" H 9750 1250 50  0001 C CNN
F 1 "MOT_DGND" H 9755 1327 50  0000 C CNN
F 2 "" H 9750 1500 50  0001 C CNN
F 3 "" H 9750 1500 50  0001 C CNN
	1    9750 1500
	-1   0    0    -1  
$EndComp
Wire Wire Line
	10075 1400 10075 1450
Wire Wire Line
	10075 1150 10075 1200
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5FEAC639
P 9750 1300
AR Path="/5E9C6910/5FEAC639" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5FEAC639" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5FEAC639" Ref="C?"  Part="1" 
AR Path="/5F05E355/5FEAC639" Ref="C?"  Part="1" 
AR Path="/6068445B/5FEAC639" Ref="C?"  Part="1" 
AR Path="/60E816A3/5FEAC639" Ref="C?"  Part="1" 
AR Path="/5EFE605D/5FEAC639" Ref="C?"  Part="1" 
AR Path="/5EA5E155/5FEAC639" Ref="C139"  Part="1" 
F 0 "C139" H 9825 1300 50  0000 L CNN
F 1 "10uF" H 9775 1225 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 9750 1300 50  0001 C CNN
F 3 "~" H 9750 1300 50  0001 C CNN
F 4 "LMK107BBJ106MALT -  SMD Multilayer Ceramic Capacitor, 10 µF, 10 V, 0603 [1608 Metric], ± 20%, X5R, M Series" H 9750 900 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/taiyo-yuden/lmk107bbj106malt/cap-10-f-10v-20-x5r-0603/dp/2113070" H 9750 800 50  0001 C CNN "Distributor Link"
	1    9750 1300
	-1   0    0    -1  
$EndComp
Wire Wire Line
	9750 1400 9750 1450
Wire Wire Line
	9750 1150 9750 1200
Connection ~ 9750 1450
Wire Wire Line
	10400 1100 10400 1150
Wire Wire Line
	9750 1450 10075 1450
Wire Wire Line
	9750 1150 10075 1150
Connection ~ 10075 1150
Wire Wire Line
	10075 1150 10400 1150
Connection ~ 10400 1150
Connection ~ 9100 1975
Wire Wire Line
	10400 1150 10400 2175
Wire Wire Line
	9100 1150 9100 1975
Wire Wire Line
	8400 2875 8400 2925
Wire Wire Line
	8725 2825 8725 2875
Wire Wire Line
	8725 2575 8725 2625
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5FEAC650
P 8400 2725
AR Path="/5E9C6910/5FEAC650" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5FEAC650" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5FEAC650" Ref="C?"  Part="1" 
AR Path="/5F05E355/5FEAC650" Ref="C?"  Part="1" 
AR Path="/6068445B/5FEAC650" Ref="C?"  Part="1" 
AR Path="/60E816A3/5FEAC650" Ref="C?"  Part="1" 
AR Path="/5EFE605D/5FEAC650" Ref="C?"  Part="1" 
AR Path="/5EA5E155/5FEAC650" Ref="C130"  Part="1" 
F 0 "C130" H 8200 2800 50  0000 L CNN
F 1 "10uF" H 8200 2650 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 8400 2725 50  0001 C CNN
F 3 "~" H 8400 2725 50  0001 C CNN
F 4 "LMK107BBJ106MALT -  SMD Multilayer Ceramic Capacitor, 10 µF, 10 V, 0603 [1608 Metric], ± 20%, X5R, M Series" H 8400 2325 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/taiyo-yuden/lmk107bbj106malt/cap-10-f-10v-20-x5r-0603/dp/2113070" H 8400 2225 50  0001 C CNN "Distributor Link"
	1    8400 2725
	-1   0    0    -1  
$EndComp
Wire Wire Line
	8400 2825 8400 2875
Wire Wire Line
	8400 2575 8400 2625
Connection ~ 8400 2875
Wire Wire Line
	8400 2875 8725 2875
Wire Wire Line
	8400 2575 8725 2575
Wire Wire Line
	8000 2375 8475 2375
Connection ~ 9100 2575
Wire Wire Line
	9100 2575 8725 2575
Connection ~ 8725 2575
Text Notes 9450 2900 0    25   ~ 0
Isolated sigma-delta modulator
Text Notes 8975 4875 0    25   ~ 0
Isolated floating power supply
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5FEAC669
P 10075 1300
AR Path="/5E9C6910/5FEAC669" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5FEAC669" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5FEAC669" Ref="C?"  Part="1" 
AR Path="/5F05E355/5FEAC669" Ref="C?"  Part="1" 
AR Path="/6068445B/5FEAC669" Ref="C?"  Part="1" 
AR Path="/60E816A3/5FEAC669" Ref="C?"  Part="1" 
AR Path="/5EFE605D/5FEAC669" Ref="C?"  Part="1" 
AR Path="/5EA5E155/5FEAC669" Ref="C141"  Part="1" 
F 0 "C141" H 9800 1300 50  0000 L CNN
F 1 "1nF" H 9900 1225 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 10075 1300 50  0001 C CNN
F 3 "~" H 10075 1300 50  0001 C CNN
F 4 "MC0402B102K160CT -  SMD Multilayer Ceramic Capacitor, 1000 pF, 16 V, 0402 [1005 Metric], ± 10%, X7R, MC Series" H 10075 900 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402b102k160ct/cap-1000pf-16v-10-x7r-0402/dp/2320774" H 10075 800 50  0001 C CNN "Distributor Link"
	1    10075 1300
	-1   0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5FEAC671
P 8725 2725
AR Path="/5E9C6910/5FEAC671" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5FEAC671" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5FEAC671" Ref="C?"  Part="1" 
AR Path="/5F05E355/5FEAC671" Ref="C?"  Part="1" 
AR Path="/6068445B/5FEAC671" Ref="C?"  Part="1" 
AR Path="/60E816A3/5FEAC671" Ref="C?"  Part="1" 
AR Path="/5EFE605D/5FEAC671" Ref="C?"  Part="1" 
AR Path="/5EA5E155/5FEAC671" Ref="C133"  Part="1" 
F 0 "C133" H 8525 2800 50  0000 L CNN
F 1 "1nF" H 8575 2650 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 8725 2725 50  0001 C CNN
F 3 "~" H 8725 2725 50  0001 C CNN
F 4 "MC0402B102K160CT -  SMD Multilayer Ceramic Capacitor, 1000 pF, 16 V, 0402 [1005 Metric], ± 10%, X7R, MC Series" H 8725 2325 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402b102k160ct/cap-1000pf-16v-10-x7r-0402/dp/2320774" H 8725 2225 50  0001 C CNN "Distributor Link"
	1    8725 2725
	-1   0    0    -1  
$EndComp
Wire Wire Line
	9600 5375 9600 5425
Wire Wire Line
	9925 5325 9925 5375
Wire Wire Line
	9925 5075 9925 5125
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5FEAC67C
P 9925 5225
AR Path="/5E9C6910/5FEAC67C" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5FEAC67C" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5FEAC67C" Ref="C?"  Part="1" 
AR Path="/5F05E355/5FEAC67C" Ref="C?"  Part="1" 
AR Path="/6068445B/5FEAC67C" Ref="C?"  Part="1" 
AR Path="/60E816A3/5FEAC67C" Ref="C?"  Part="1" 
AR Path="/5EFE605D/5FEAC67C" Ref="C?"  Part="1" 
AR Path="/5EA5E155/5FEAC67C" Ref="C140"  Part="1" 
F 0 "C140" H 9725 5300 50  0000 L CNN
F 1 "10uF" H 9725 5150 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 9925 5225 50  0001 C CNN
F 3 "~" H 9925 5225 50  0001 C CNN
F 4 "LMK107BBJ106MALT -  SMD Multilayer Ceramic Capacitor, 10 µF, 10 V, 0603 [1608 Metric], ± 20%, X5R, M Series" H 9925 4825 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/taiyo-yuden/lmk107bbj106malt/cap-10-f-10v-20-x5r-0603/dp/2113070" H 9925 4725 50  0001 C CNN "Distributor Link"
	1    9925 5225
	-1   0    0    -1  
$EndComp
Wire Wire Line
	9600 5325 9600 5375
Wire Wire Line
	9600 5075 9600 5125
Connection ~ 9600 5375
Wire Wire Line
	9600 5375 9925 5375
Wire Wire Line
	9600 5075 9825 5075
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5FEAC689
P 9600 5225
AR Path="/5E9C6910/5FEAC689" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5FEAC689" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5FEAC689" Ref="C?"  Part="1" 
AR Path="/5F05E355/5FEAC689" Ref="C?"  Part="1" 
AR Path="/5EA41912/5FEAC689" Ref="C?"  Part="1" 
AR Path="/5EFE605D/5FEAC689" Ref="C?"  Part="1" 
AR Path="/5EA5E155/5FEAC689" Ref="C138"  Part="1" 
F 0 "C138" H 9400 5300 50  0000 L CNN
F 1 "100nF" H 9350 5150 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 9600 5225 50  0001 C CNN
F 3 "~" H 9600 5225 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 9600 4825 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 9600 4725 50  0001 C CNN "Distributor Link"
	1    9600 5225
	-1   0    0    -1  
$EndComp
NoConn ~ 8775 4150
NoConn ~ 8775 4250
NoConn ~ 8775 4350
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR0288
U 1 1 5FEAC692
P 8675 4750
F 0 "#PWR0288" H 8675 4500 50  0001 C CNN
F 1 "MOT_DGND" H 8680 4577 50  0000 C CNN
F 2 "" H 8675 4750 50  0001 C CNN
F 3 "" H 8675 4750 50  0001 C CNN
	1    8675 4750
	1    0    0    -1  
$EndComp
Wire Wire Line
	8675 4750 8675 4650
Wire Wire Line
	8675 4650 8775 4650
Wire Wire Line
	10200 4750 10200 4650
Connection ~ 10200 4650
Wire Wire Line
	8675 4650 8675 4600
Wire Wire Line
	8675 4050 8775 4050
Connection ~ 8675 4650
$Comp
L PMSM_Driver_LV_Board_Power:MOT_5V0 #PWR0287
U 1 1 5FEAC69F
P 8575 3850
F 0 "#PWR0287" H 8575 4100 50  0001 C CNN
F 1 "MOT_5V0" H 8576 4023 50  0000 C CNN
F 2 "" H 8575 3850 50  0001 C CNN
F 3 "" H 8575 3850 50  0001 C CNN
	1    8575 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	8575 3850 8575 3950
Wire Wire Line
	8575 3950 8775 3950
Wire Wire Line
	8775 4550 8575 4550
Wire Wire Line
	8575 4550 8575 4450
Connection ~ 8575 3950
Wire Wire Line
	8775 4450 8575 4450
Connection ~ 8575 4450
Wire Wire Line
	8575 4450 8575 3950
$Comp
L PMSM_Driver_LV_Board_Device:Ferrite_Bead_Small FB3
U 1 1 5FEAC6B0
P 10400 3950
F 0 "FB3" V 10525 3875 50  0000 L CNN
F 1 "Z0805C420APWST" V 10275 3775 25  0000 L CNN
F 2 "PMSM_Driver_LV_Board:L_0805_2012Metric" V 10330 3950 50  0001 C CNN
F 3 "~" H 10400 3950 50  0001 C CNN
F 4 "Z0805C420APWST -  Ferrite Bead, 0805 [2012 Metric], 42 ohm, 4 A, Z-PWS Series, 0.008 ohm, ± 25%" H 10400 3950 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/kemet/z0805c420apwst/ferrite-bead-42-ohm-4a-0805/dp/2846741" H 10400 3950 50  0001 C CNN "Distributor Link"
F 6 "42 Ohms at 100 MHz" V 10225 3725 25  0000 L CNN "Impedance"
	1    10400 3950
	0    -1   -1   0   
$EndComp
Wire Wire Line
	10500 3950 10600 3950
Wire Wire Line
	10600 3950 10600 3850
Wire Wire Line
	9925 5375 10250 5375
Wire Wire Line
	10250 5325 10250 5375
Wire Wire Line
	10250 5075 10250 5125
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5FEAC6BD
P 10250 5225
AR Path="/5E9C6910/5FEAC6BD" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5FEAC6BD" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5FEAC6BD" Ref="C?"  Part="1" 
AR Path="/5F05E355/5FEAC6BD" Ref="C?"  Part="1" 
AR Path="/5EA41912/5FEAC6BD" Ref="C?"  Part="1" 
AR Path="/5EFE605D/5FEAC6BD" Ref="C?"  Part="1" 
AR Path="/5EA5E155/5FEAC6BD" Ref="C142"  Part="1" 
F 0 "C142" H 10050 5300 50  0000 L CNN
F 1 "100nF" H 10000 5150 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 10250 5225 50  0001 C CNN
F 3 "~" H 10250 5225 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 10250 4825 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 10250 4725 50  0001 C CNN "Distributor Link"
	1    10250 5225
	-1   0    0    -1  
$EndComp
Wire Wire Line
	10575 5325 10575 5375
Wire Wire Line
	10575 5075 10575 5125
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5FEAC6C7
P 10575 5225
AR Path="/5E9C6910/5FEAC6C7" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5FEAC6C7" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5FEAC6C7" Ref="C?"  Part="1" 
AR Path="/5F05E355/5FEAC6C7" Ref="C?"  Part="1" 
AR Path="/6068445B/5FEAC6C7" Ref="C?"  Part="1" 
AR Path="/60E816A3/5FEAC6C7" Ref="C?"  Part="1" 
AR Path="/5EFE605D/5FEAC6C7" Ref="C?"  Part="1" 
AR Path="/5EA5E155/5FEAC6C7" Ref="C143"  Part="1" 
F 0 "C143" H 10375 5300 50  0000 L CNN
F 1 "10uF" H 10375 5150 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 10575 5225 50  0001 C CNN
F 3 "~" H 10575 5225 50  0001 C CNN
F 4 "LMK107BBJ106MALT -  SMD Multilayer Ceramic Capacitor, 10 µF, 10 V, 0603 [1608 Metric], ± 20%, X5R, M Series" H 10575 4825 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/taiyo-yuden/lmk107bbj106malt/cap-10-f-10v-20-x5r-0603/dp/2113070" H 10575 4725 50  0001 C CNN "Distributor Link"
	1    10575 5225
	-1   0    0    -1  
$EndComp
Wire Wire Line
	10250 5375 10575 5375
Wire Wire Line
	10250 5075 10575 5075
Wire Wire Line
	9725 3950 9825 3950
Connection ~ 9925 5075
Connection ~ 9925 5375
Connection ~ 10250 5075
Connection ~ 10250 5375
Wire Wire Line
	8275 5375 8275 5425
Wire Wire Line
	8600 5325 8600 5375
Wire Wire Line
	8600 5075 8600 5125
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5FEAC6D9
P 8600 5225
AR Path="/5E9C6910/5FEAC6D9" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5FEAC6D9" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5FEAC6D9" Ref="C?"  Part="1" 
AR Path="/5F05E355/5FEAC6D9" Ref="C?"  Part="1" 
AR Path="/6068445B/5FEAC6D9" Ref="C?"  Part="1" 
AR Path="/60E816A3/5FEAC6D9" Ref="C?"  Part="1" 
AR Path="/5EFE605D/5FEAC6D9" Ref="C?"  Part="1" 
AR Path="/5EA5E155/5FEAC6D9" Ref="C132"  Part="1" 
F 0 "C132" H 8400 5300 50  0000 L CNN
F 1 "10uF" H 8400 5150 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 8600 5225 50  0001 C CNN
F 3 "~" H 8600 5225 50  0001 C CNN
F 4 "LMK107BBJ106MALT -  SMD Multilayer Ceramic Capacitor, 10 µF, 10 V, 0603 [1608 Metric], ± 20%, X5R, M Series" H 8600 4825 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/taiyo-yuden/lmk107bbj106malt/cap-10-f-10v-20-x5r-0603/dp/2113070" H 8600 4725 50  0001 C CNN "Distributor Link"
	1    8600 5225
	-1   0    0    -1  
$EndComp
Wire Wire Line
	8275 5325 8275 5375
Wire Wire Line
	8275 5075 8275 5125
Connection ~ 8275 5375
Wire Wire Line
	8275 5375 8600 5375
Wire Wire Line
	8275 5075 8600 5075
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5FEAC6E6
P 8275 5225
AR Path="/5E9C6910/5FEAC6E6" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5FEAC6E6" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5FEAC6E6" Ref="C?"  Part="1" 
AR Path="/5F05E355/5FEAC6E6" Ref="C?"  Part="1" 
AR Path="/5EA41912/5FEAC6E6" Ref="C?"  Part="1" 
AR Path="/5EFE605D/5FEAC6E6" Ref="C?"  Part="1" 
AR Path="/5EA5E155/5FEAC6E6" Ref="C129"  Part="1" 
F 0 "C129" H 8075 5300 50  0000 L CNN
F 1 "100nF" H 8025 5150 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 8275 5225 50  0001 C CNN
F 3 "~" H 8275 5225 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 8275 4825 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 8275 4725 50  0001 C CNN "Distributor Link"
	1    8275 5225
	-1   0    0    -1  
$EndComp
Wire Wire Line
	8600 5375 8925 5375
Wire Wire Line
	8925 5325 8925 5375
Wire Wire Line
	8925 5075 8925 5125
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5FEAC6F1
P 8925 5225
AR Path="/5E9C6910/5FEAC6F1" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5FEAC6F1" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5FEAC6F1" Ref="C?"  Part="1" 
AR Path="/5F05E355/5FEAC6F1" Ref="C?"  Part="1" 
AR Path="/5EA41912/5FEAC6F1" Ref="C?"  Part="1" 
AR Path="/5EFE605D/5FEAC6F1" Ref="C?"  Part="1" 
AR Path="/5EA5E155/5FEAC6F1" Ref="C136"  Part="1" 
F 0 "C136" H 8725 5300 50  0000 L CNN
F 1 "100nF" H 8675 5150 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 8925 5225 50  0001 C CNN
F 3 "~" H 8925 5225 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 8925 4825 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 8925 4725 50  0001 C CNN "Distributor Link"
	1    8925 5225
	-1   0    0    -1  
$EndComp
Wire Wire Line
	9250 5325 9250 5375
Wire Wire Line
	9250 5075 9250 5125
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5FEAC6FB
P 9250 5225
AR Path="/5E9C6910/5FEAC6FB" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5FEAC6FB" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5FEAC6FB" Ref="C?"  Part="1" 
AR Path="/5F05E355/5FEAC6FB" Ref="C?"  Part="1" 
AR Path="/6068445B/5FEAC6FB" Ref="C?"  Part="1" 
AR Path="/60E816A3/5FEAC6FB" Ref="C?"  Part="1" 
AR Path="/5EFE605D/5FEAC6FB" Ref="C?"  Part="1" 
AR Path="/5EA5E155/5FEAC6FB" Ref="C137"  Part="1" 
F 0 "C137" H 9050 5300 50  0000 L CNN
F 1 "10uF" H 9050 5150 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 9250 5225 50  0001 C CNN
F 3 "~" H 9250 5225 50  0001 C CNN
F 4 "LMK107BBJ106MALT -  SMD Multilayer Ceramic Capacitor, 10 µF, 10 V, 0603 [1608 Metric], ± 20%, X5R, M Series" H 9250 4825 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/taiyo-yuden/lmk107bbj106malt/cap-10-f-10v-20-x5r-0603/dp/2113070" H 9250 4725 50  0001 C CNN "Distributor Link"
	1    9250 5225
	-1   0    0    -1  
$EndComp
Wire Wire Line
	8925 5375 9250 5375
Wire Wire Line
	8925 5075 9250 5075
Connection ~ 8600 5075
Connection ~ 8600 5375
Connection ~ 8925 5075
Connection ~ 8925 5375
Wire Wire Line
	8600 5075 8925 5075
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR0284
U 1 1 5FEAC708
P 8275 5425
F 0 "#PWR0284" H 8275 5175 50  0001 C CNN
F 1 "MOT_DGND" H 8280 5252 50  0000 C CNN
F 2 "" H 8275 5425 50  0001 C CNN
F 3 "" H 8275 5425 50  0001 C CNN
	1    8275 5425
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_5V0 #PWR0283
U 1 1 5FEAC70E
P 8275 4975
F 0 "#PWR0283" H 8275 5225 50  0001 C CNN
F 1 "MOT_5V0" H 8276 5148 50  0000 C CNN
F 2 "" H 8275 4975 50  0001 C CNN
F 3 "" H 8275 4975 50  0001 C CNN
	1    8275 4975
	1    0    0    -1  
$EndComp
Wire Wire Line
	8275 5075 8275 4975
Connection ~ 8275 5075
$Comp
L PMSM_Driver_LV_Board_Device:TestPoint TP5
U 1 1 5FEAC716
P 8275 4550
F 0 "TP5" H 8225 4750 50  0000 L CNN
F 1 "TestPoint" H 8300 4600 25  0000 L CNN
F 2 "PMSM_Driver_LV_Board:TestPoint_Pad_1.0x1.0mm" H 8475 4550 50  0001 C CNN
F 3 "~" H 8475 4550 50  0001 C CNN
	1    8275 4550
	1    0    0    -1  
$EndComp
Wire Wire Line
	8675 4600 8275 4600
Wire Wire Line
	8275 4600 8275 4550
Connection ~ 8675 4600
Wire Wire Line
	8675 4600 8675 4050
$Comp
L PMSM_Driver_LV_Board_Device:TestPoint TP6
U 1 1 5FEAC720
P 10450 4550
F 0 "TP6" H 10400 4750 50  0000 L CNN
F 1 "TestPoint" H 10475 4600 25  0000 L CNN
F 2 "PMSM_Driver_LV_Board:TestPoint_Pad_1.0x1.0mm" H 10650 4550 50  0001 C CNN
F 3 "~" H 10650 4550 50  0001 C CNN
	1    10450 4550
	-1   0    0    -1  
$EndComp
Wire Wire Line
	10450 4600 10450 4550
Wire Wire Line
	10450 4600 10200 4600
NoConn ~ 9725 4150
NoConn ~ 9725 4350
NoConn ~ 9725 4450
Wire Wire Line
	9825 3950 9825 4250
Connection ~ 9825 3950
Connection ~ 9825 5075
Wire Wire Line
	9825 5075 9925 5075
Wire Wire Line
	9925 5075 10250 5075
Wire Wire Line
	9825 3950 9925 3950
Wire Wire Line
	9725 4650 10200 4650
Wire Wire Line
	9725 4050 10200 4050
Wire Wire Line
	9725 4550 9825 4550
Connection ~ 9825 4550
Wire Wire Line
	9825 4550 9825 5075
Wire Wire Line
	9725 4250 9825 4250
Connection ~ 9825 4250
Wire Wire Line
	9825 4250 9825 4550
Connection ~ 10200 4600
Wire Wire Line
	10200 4600 10200 4650
Wire Wire Line
	10200 4050 10200 4600
Wire Notes Line
	7600 6425 11025 6425
$Comp
L PMSM_Driver_LV_Board_Power:MOT_SENS_T_AGND #PWR?
U 1 1 5FF2A6E3
P 10200 4750
AR Path="/5FD7CF82/5FF2A6E3" Ref="#PWR?"  Part="1" 
AR Path="/5EFE605D/5FF2A6E3" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E155/5FF2A6E3" Ref="#PWR0294"  Part="1" 
F 0 "#PWR0294" H 10200 4500 50  0001 C CNN
F 1 "MOT_SENS_T_AGND" H 10205 4577 50  0000 C CNN
F 2 "" H 10200 4750 50  0001 C CNN
F 3 "" H 10200 4750 50  0001 C CNN
	1    10200 4750
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_SENS_T_AGND #PWR?
U 1 1 5FF5C7FE
P 8400 2925
AR Path="/5FD7CF82/5FF5C7FE" Ref="#PWR?"  Part="1" 
AR Path="/5EFE605D/5FF5C7FE" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E155/5FF5C7FE" Ref="#PWR0285"  Part="1" 
F 0 "#PWR0285" H 8400 2675 50  0001 C CNN
F 1 "MOT_SENS_T_AGND" H 8405 2752 50  0000 C CNN
F 2 "" H 8400 2925 50  0001 C CNN
F 3 "" H 8400 2925 50  0001 C CNN
	1    8400 2925
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_SENS_T_AGND #PWR?
U 1 1 5FF8E9CE
P 8000 2425
AR Path="/5FD7CF82/5FF8E9CE" Ref="#PWR?"  Part="1" 
AR Path="/5EFE605D/5FF8E9CE" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E155/5FF8E9CE" Ref="#PWR0282"  Part="1" 
F 0 "#PWR0282" H 8000 2175 50  0001 C CNN
F 1 "MOT_SENS_T_AGND" H 8005 2252 50  0000 C CNN
F 2 "" H 8000 2425 50  0001 C CNN
F 3 "" H 8000 2425 50  0001 C CNN
	1    8000 2425
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_SENS_T_AGND #PWR?
U 1 1 5FFC0BF8
P 8450 1500
AR Path="/5FD7CF82/5FFC0BF8" Ref="#PWR?"  Part="1" 
AR Path="/5EFE605D/5FFC0BF8" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E155/5FFC0BF8" Ref="#PWR0286"  Part="1" 
F 0 "#PWR0286" H 8450 1250 50  0001 C CNN
F 1 "MOT_SENS_T_AGND" H 8455 1327 50  0000 C CNN
F 2 "" H 8450 1500 50  0001 C CNN
F 3 "" H 8450 1500 50  0001 C CNN
	1    8450 1500
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_SENS_B_AGND #PWR?
U 1 1 5FFF2ED9
P 4950 1500
AR Path="/5EA41912/5FFF2ED9" Ref="#PWR?"  Part="1" 
AR Path="/5FFF2ED9" Ref="#PWR?"  Part="1" 
AR Path="/5EFE605D/5FFF2ED9" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E155/5FFF2ED9" Ref="#PWR0269"  Part="1" 
F 0 "#PWR0269" H 4950 1250 50  0001 C CNN
F 1 "MOT_SENS_B_AGND" H 4955 1327 50  0000 C CNN
F 2 "" H 4950 1500 50  0001 C CNN
F 3 "" H 4950 1500 50  0001 C CNN
	1    4950 1500
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_SENS_T_AGND #PWR?
U 1 1 600251D3
P 9200 2800
AR Path="/5FD7CF82/600251D3" Ref="#PWR?"  Part="1" 
AR Path="/5EFE605D/600251D3" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E155/600251D3" Ref="#PWR0290"  Part="1" 
F 0 "#PWR0290" H 9200 2550 50  0001 C CNN
F 1 "MOT_SENS_T_AGND" H 9205 2627 50  0000 C CNN
F 2 "" H 9200 2800 50  0001 C CNN
F 3 "" H 9200 2800 50  0001 C CNN
	1    9200 2800
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_SENS_T_5V0 #PWR0289
U 1 1 600A2AA1
P 9100 1100
F 0 "#PWR0289" H 9100 1350 50  0001 C CNN
F 1 "MOT_SENS_T_5V0" H 9101 1273 50  0000 C CNN
F 2 "" H 9100 1100 50  0001 C CNN
F 3 "" H 9100 1100 50  0001 C CNN
	1    9100 1100
	1    0    0    -1  
$EndComp
Text GLabel 10400 2475 2    50   Output ~ 0
ISO_CUR_T
$Comp
L power:PWR_FLAG #FLG?
U 1 1 5F4507A4
P 2875 6050
AR Path="/5ED47D19/5F4507A4" Ref="#FLG?"  Part="1" 
AR Path="/5EA5E155/5F4507A4" Ref="#FLG08"  Part="1" 
F 0 "#FLG08" H 2875 6125 50  0001 C CNN
F 1 "PWR_FLAG" H 2875 6223 50  0000 C CNN
F 2 "" H 2875 6050 50  0001 C CNN
F 3 "~" H 2875 6050 50  0001 C CNN
	1    2875 6050
	-1   0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_SENS_A_AGND #PWR?
U 1 1 5F49BD4E
P 2875 6100
AR Path="/5EA41912/5F49BD4E" Ref="#PWR?"  Part="1" 
AR Path="/5F49BD4E" Ref="#PWR?"  Part="1" 
AR Path="/5EFE605D/5F49BD4E" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E155/5F49BD4E" Ref="#PWR0259"  Part="1" 
F 0 "#PWR0259" H 2875 5850 50  0001 C CNN
F 1 "MOT_SENS_A_AGND" H 2880 5927 50  0000 C CNN
F 2 "" H 2875 6100 50  0001 C CNN
F 3 "" H 2875 6100 50  0001 C CNN
	1    2875 6100
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_SENS_A_5V0 #PWR0264
U 1 1 5F4CECAB
P 3600 6050
F 0 "#PWR0264" H 3600 6300 50  0001 C CNN
F 1 "MOT_SENS_A_5V0" H 3601 6223 50  0000 C CNN
F 2 "" H 3600 6050 50  0001 C CNN
F 3 "" H 3600 6050 50  0001 C CNN
	1    3600 6050
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG?
U 1 1 5F4E84B1
P 3600 6100
AR Path="/5ED47D19/5F4E84B1" Ref="#FLG?"  Part="1" 
AR Path="/5EA5E155/5F4E84B1" Ref="#FLG010"  Part="1" 
F 0 "#FLG010" H 3600 6175 50  0001 C CNN
F 1 "PWR_FLAG" H 3600 6273 50  0000 C CNN
F 2 "" H 3600 6100 50  0001 C CNN
F 3 "~" H 3600 6100 50  0001 C CNN
	1    3600 6100
	-1   0    0    1   
$EndComp
Wire Wire Line
	3600 6100 3600 6050
Wire Wire Line
	2875 6100 2875 6050
Wire Notes Line
	4025 600  4025 6425
Wire Notes Line
	600  600  600  6425
Wire Notes Line
	4100 600  4100 6425
Wire Notes Line
	7525 600  7525 6425
Wire Notes Line
	7600 600  7600 6425
Wire Notes Line
	11025 600  11025 6425
$Comp
L power:PWR_FLAG #FLG?
U 1 1 5F6B8349
P 6375 6050
AR Path="/5ED47D19/5F6B8349" Ref="#FLG?"  Part="1" 
AR Path="/5EA5E155/5F6B8349" Ref="#FLG011"  Part="1" 
F 0 "#FLG011" H 6375 6125 50  0001 C CNN
F 1 "PWR_FLAG" H 6375 6223 50  0000 C CNN
F 2 "" H 6375 6050 50  0001 C CNN
F 3 "~" H 6375 6050 50  0001 C CNN
	1    6375 6050
	-1   0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG?
U 1 1 5F6B835B
P 7100 6100
AR Path="/5ED47D19/5F6B835B" Ref="#FLG?"  Part="1" 
AR Path="/5EA5E155/5F6B835B" Ref="#FLG013"  Part="1" 
F 0 "#FLG013" H 7100 6175 50  0001 C CNN
F 1 "PWR_FLAG" H 7100 6273 50  0000 C CNN
F 2 "" H 7100 6100 50  0001 C CNN
F 3 "~" H 7100 6100 50  0001 C CNN
	1    7100 6100
	-1   0    0    1   
$EndComp
Wire Wire Line
	7100 6100 7100 6050
Wire Wire Line
	6375 6100 6375 6050
$Comp
L PMSM_Driver_LV_Board_Power:MOT_SENS_B_AGND #PWR?
U 1 1 5F72034B
P 6375 6100
AR Path="/5EA41912/5F72034B" Ref="#PWR?"  Part="1" 
AR Path="/5F72034B" Ref="#PWR?"  Part="1" 
AR Path="/5EFE605D/5F72034B" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E155/5F72034B" Ref="#PWR0276"  Part="1" 
F 0 "#PWR0276" H 6375 5850 50  0001 C CNN
F 1 "MOT_SENS_B_AGND" H 6380 5927 50  0000 C CNN
F 2 "" H 6375 6100 50  0001 C CNN
F 3 "" H 6375 6100 50  0001 C CNN
	1    6375 6100
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_SENS_B_5V0 #PWR0281
U 1 1 5F753FD4
P 7100 6050
F 0 "#PWR0281" H 7100 6300 50  0001 C CNN
F 1 "MOT_SENS_B_5V0" H 7101 6223 50  0000 C CNN
F 2 "" H 7100 6050 50  0001 C CNN
F 3 "" H 7100 6050 50  0001 C CNN
	1    7100 6050
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG?
U 1 1 5F76F08F
P 9875 6050
AR Path="/5ED47D19/5F76F08F" Ref="#FLG?"  Part="1" 
AR Path="/5EA5E155/5F76F08F" Ref="#FLG014"  Part="1" 
F 0 "#FLG014" H 9875 6125 50  0001 C CNN
F 1 "PWR_FLAG" H 9875 6223 50  0000 C CNN
F 2 "" H 9875 6050 50  0001 C CNN
F 3 "~" H 9875 6050 50  0001 C CNN
	1    9875 6050
	-1   0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG?
U 1 1 5F76F095
P 10600 6100
AR Path="/5ED47D19/5F76F095" Ref="#FLG?"  Part="1" 
AR Path="/5EA5E155/5F76F095" Ref="#FLG016"  Part="1" 
F 0 "#FLG016" H 10600 6175 50  0001 C CNN
F 1 "PWR_FLAG" H 10600 6273 50  0000 C CNN
F 2 "" H 10600 6100 50  0001 C CNN
F 3 "~" H 10600 6100 50  0001 C CNN
	1    10600 6100
	-1   0    0    1   
$EndComp
Wire Wire Line
	10600 6100 10600 6050
Wire Wire Line
	9875 6100 9875 6050
$Comp
L PMSM_Driver_LV_Board_Power:MOT_SENS_T_AGND #PWR?
U 1 1 5F7BD9F0
P 9875 6100
AR Path="/5FD7CF82/5F7BD9F0" Ref="#PWR?"  Part="1" 
AR Path="/5EFE605D/5F7BD9F0" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E155/5F7BD9F0" Ref="#PWR0293"  Part="1" 
F 0 "#PWR0293" H 9875 5850 50  0001 C CNN
F 1 "MOT_SENS_T_AGND" H 9880 5927 50  0000 C CNN
F 2 "" H 9875 6100 50  0001 C CNN
F 3 "" H 9875 6100 50  0001 C CNN
	1    9875 6100
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_SENS_T_5V0 #PWR0298
U 1 1 5F7D7E5E
P 10600 6050
F 0 "#PWR0298" H 10600 6300 50  0001 C CNN
F 1 "MOT_SENS_T_5V0" H 10601 6223 50  0000 C CNN
F 2 "" H 10600 6050 50  0001 C CNN
F 3 "" H 10600 6050 50  0001 C CNN
	1    10600 6050
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG?
U 1 1 5FD2B6BE
P 2925 3925
AR Path="/5EA5E06A/5FD2B6BE" Ref="#FLG?"  Part="1" 
AR Path="/5EA5E155/5FD2B6BE" Ref="#FLG09"  Part="1" 
F 0 "#FLG09" H 2925 4000 50  0001 C CNN
F 1 "PWR_FLAG" H 2925 4079 25  0000 C CNN
F 2 "" H 2925 3925 50  0001 C CNN
F 3 "~" H 2925 3925 50  0001 C CNN
	1    2925 3925
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2925 3950 2925 3925
$Comp
L power:PWR_FLAG #FLG?
U 1 1 5FD7C26C
P 6425 3925
AR Path="/5EA5E06A/5FD7C26C" Ref="#FLG?"  Part="1" 
AR Path="/5EA5E155/5FD7C26C" Ref="#FLG012"  Part="1" 
F 0 "#FLG012" H 6425 4098 50  0001 C CNN
F 1 "PWR_FLAG" H 6425 4079 25  0000 C CNN
F 2 "" H 6425 3925 50  0001 C CNN
F 3 "~" H 6425 3925 50  0001 C CNN
	1    6425 3925
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6425 3950 6425 3925
Connection ~ 6425 3950
Wire Wire Line
	6425 3950 6800 3950
Connection ~ 2925 3950
Wire Wire Line
	2925 3950 3300 3950
Wire Wire Line
	2825 3950 2925 3950
$Comp
L power:PWR_FLAG #FLG?
U 1 1 5FDCD84B
P 9925 3925
AR Path="/5EA5E06A/5FDCD84B" Ref="#FLG?"  Part="1" 
AR Path="/5EA5E155/5FDCD84B" Ref="#FLG015"  Part="1" 
F 0 "#FLG015" H 9925 4000 50  0001 C CNN
F 1 "PWR_FLAG" H 9925 4079 25  0000 C CNN
F 2 "" H 9925 3925 50  0001 C CNN
F 3 "~" H 9925 3925 50  0001 C CNN
	1    9925 3925
	-1   0    0    -1  
$EndComp
Wire Wire Line
	9925 3950 9925 3925
Connection ~ 9925 3950
Wire Wire Line
	9925 3950 10300 3950
$EndSCHEMATC
