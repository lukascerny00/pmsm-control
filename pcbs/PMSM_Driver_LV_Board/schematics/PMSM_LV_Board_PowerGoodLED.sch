EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 16 16
Title "PMSM_Driver_LV_Board"
Date "2020-03-30"
Rev "A"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_DGND #PWR?
U 1 1 61A9DC6E
P 1475 2100
AR Path="/5EA5D9E8/61A9DC6E" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/61A9DC6E" Ref="#PWR?"  Part="1" 
AR Path="/61A61EC8/61A9DC6E" Ref="#PWR0473"  Part="1" 
F 0 "#PWR0473" H 1475 1850 50  0001 C CNN
F 1 "CTRL_DGND" H 1480 1927 50  0000 C CNN
F 2 "" H 1475 2100 50  0001 C CNN
F 3 "" H 1475 2100 50  0001 C CNN
	1    1475 2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	1475 2050 1475 2100
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 61A9DC77
P 1700 1250
AR Path="/5E9C6910/61A9DC77" Ref="C?"  Part="1" 
AR Path="/5EBC6654/61A9DC77" Ref="C?"  Part="1" 
AR Path="/5EBC681B/61A9DC77" Ref="C?"  Part="1" 
AR Path="/5EA5DDD6/61A9DC77" Ref="C?"  Part="1" 
AR Path="/5ED47D19/61A9DC77" Ref="C?"  Part="1" 
AR Path="/61A61EC8/61A9DC77" Ref="C258"  Part="1" 
F 0 "C258" V 1825 1150 50  0000 L CNN
F 1 "100nF" V 1750 975 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 1700 1250 50  0001 C CNN
F 3 "~" H 1700 1250 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 1700 850 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 1700 750 50  0001 C CNN "Distributor Link"
	1    1700 1250
	0    -1   -1   0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_3V3 #PWR?
U 1 1 61A9DC7D
P 1475 1200
AR Path="/5EA5DDD6/61A9DC7D" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/61A9DC7D" Ref="#PWR?"  Part="1" 
AR Path="/61A61EC8/61A9DC7D" Ref="#PWR0472"  Part="1" 
F 0 "#PWR0472" H 1475 1450 50  0001 C CNN
F 1 "CTRL_3V3" H 1476 1373 50  0000 C CNN
F 2 "" H 1475 1200 50  0001 C CNN
F 3 "" H 1475 1200 50  0001 C CNN
	1    1475 1200
	1    0    0    -1  
$EndComp
Wire Wire Line
	1800 1250 1900 1250
Wire Wire Line
	1900 1250 1900 1300
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_DGND #PWR?
U 1 1 61A9DC85
P 1900 1300
AR Path="/5EA5DDD6/61A9DC85" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/61A9DC85" Ref="#PWR?"  Part="1" 
AR Path="/61A61EC8/61A9DC85" Ref="#PWR0476"  Part="1" 
F 0 "#PWR0476" H 1900 1050 50  0001 C CNN
F 1 "CTRL_DGND" H 1905 1127 50  0000 C CNN
F 2 "" H 1900 1300 50  0001 C CNN
F 3 "" H 1900 1300 50  0001 C CNN
	1    1900 1300
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_3V3 #PWR?
U 1 1 61A9DC8B
P 850 1200
AR Path="/5EA5DDD6/61A9DC8B" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/61A9DC8B" Ref="#PWR?"  Part="1" 
AR Path="/61A61EC8/61A9DC8B" Ref="#PWR0468"  Part="1" 
F 0 "#PWR0468" H 850 1450 50  0001 C CNN
F 1 "CTRL_3V3" H 851 1373 50  0000 C CNN
F 2 "" H 850 1200 50  0001 C CNN
F 3 "" H 850 1200 50  0001 C CNN
	1    850  1200
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 61A9DC93
P 850 1650
AR Path="/5F05E355/61A9DC93" Ref="R?"  Part="1" 
AR Path="/6068445B/61A9DC93" Ref="R?"  Part="1" 
AR Path="/60E816A3/61A9DC93" Ref="R?"  Part="1" 
AR Path="/60EB5E32/61A9DC93" Ref="R?"  Part="1" 
AR Path="/5EA5DFF7/61A9DC93" Ref="R?"  Part="1" 
AR Path="/5ED47D19/61A9DC93" Ref="R?"  Part="1" 
AR Path="/61A61EC8/61A9DC93" Ref="R215"  Part="1" 
F 0 "R215" H 1000 1675 50  0000 C CNN
F 1 "82k" H 950 1575 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 850 1650 50  0001 C CNN
F 3 "~" H 850 1650 50  0001 C CNN
F 4 "MCWR04X8202FTL -  SMD Chip Resistor, 0402 [1005 Metric], 82 kohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 850 1250 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x8202ftl/res-82k-1-0-0625w-thick-film/dp/2447219" H 850 1150 50  0001 C CNN "Distributor Link"
	1    850  1650
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 61A9DC9B
P 850 1950
AR Path="/5F05E355/61A9DC9B" Ref="R?"  Part="1" 
AR Path="/6068445B/61A9DC9B" Ref="R?"  Part="1" 
AR Path="/60E816A3/61A9DC9B" Ref="R?"  Part="1" 
AR Path="/60EB5E32/61A9DC9B" Ref="R?"  Part="1" 
AR Path="/5EA5DFF7/61A9DC9B" Ref="R?"  Part="1" 
AR Path="/5ED47D19/61A9DC9B" Ref="R?"  Part="1" 
AR Path="/61A61EC8/61A9DC9B" Ref="R216"  Part="1" 
F 0 "R216" H 1000 1975 50  0000 C CNN
F 1 "20k" H 950 1875 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 850 1950 50  0001 C CNN
F 3 "~" H 850 1950 50  0001 C CNN
F 4 "MCWR04X2002FTL -  SMD Chip Resistor, 0402 [1005 Metric], 20 kohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 850 1550 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x2002ftl/res-20k-1-0-0625w-thick-film/dp/2447133" H 850 1450 50  0001 C CNN "Distributor Link"
	1    850  1950
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_DGND #PWR?
U 1 1 61A9DCA1
P 850 2100
AR Path="/5EA5D9E8/61A9DCA1" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/61A9DCA1" Ref="#PWR?"  Part="1" 
AR Path="/61A61EC8/61A9DCA1" Ref="#PWR0469"  Part="1" 
F 0 "#PWR0469" H 850 1850 50  0001 C CNN
F 1 "CTRL_DGND" H 855 1927 50  0000 C CNN
F 2 "" H 850 2100 50  0001 C CNN
F 3 "" H 850 2100 50  0001 C CNN
	1    850  2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	850  2100 850  2050
Wire Wire Line
	1075 1800 850  1800
Wire Wire Line
	850  1800 850  1850
Wire Wire Line
	850  1800 850  1750
Connection ~ 850  1800
$Comp
L PMSM_Driver_LV_Board_IC:ADCMP350YKSZ U?
U 1 1 61A9DCAF
P 1425 1750
AR Path="/5ED47D19/61A9DCAF" Ref="U?"  Part="1" 
AR Path="/61A61EC8/61A9DCAF" Ref="U59"  Part="1" 
F 0 "U59" H 1200 2000 50  0000 L CNN
F 1 "ADCMP350YKSZ" H 1500 1500 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:Analog_KS-4" H 1425 900 50  0001 C CNN
F 3 "" H 575 2150 50  0001 C CNN
F 4 "ADCMP350YKSZ-REEL7 -  Analogue Comparator, Single, General Purpose, 1 Comparator, 5 µs, 2.25V to 5.5V, SC-70, 4 Pins" H 1425 800 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/analog-devices/adcmp350yksz-reel7/comparator-single-5us-sc-70-4/dp/2376802RL" H 1425 700 50  0001 C CNN "Distributor Link"
F 6 "https://www.mouser.co.uk/ProductDetail/Analog-Devices/ADCMP350YKSZ-REEL7?qs=sGAEpiMZZMv9Q1JI0Mo%2FtaNnJ1AHFVFg" H 1425 600 50  0001 C CNN "Distributor Link 2"
	1    1425 1750
	1    0    0    -1  
$EndComp
Wire Wire Line
	1475 1200 1475 1250
Wire Wire Line
	1600 1250 1475 1250
Connection ~ 1475 1250
$Comp
L PMSM_Driver_LV_Board_Device:LED_Small D?
U 1 1 61A9DCBB
P 2250 1600
AR Path="/5ED47D19/61A9DCBB" Ref="D?"  Part="1" 
AR Path="/61A61EC8/61A9DCBB" Ref="D38"  Part="1" 
F 0 "D38" V 2325 1550 50  0000 R CNN
F 1 "KPT-2012SGC" V 2250 1550 25  0000 R CNN
F 2 "PMSM_Driver_LV_Board:LED_0805_2012Metric" V 2250 1600 50  0001 C CNN
F 3 "~" V 2250 1600 50  0001 C CNN
F 4 "KPT-2012SGC -  LED, Low Power, Green, SMD, 0805, 20 mA, 2.2 V, 568 nm" H 2250 1400 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/kingbright/kpt-2012sgc/led-0805-green-12mcd-568nm/dp/2099239" H 2250 1300 50  0001 C CNN "Distributor Link"
F 6 "2.2V" V 2175 1550 50  0000 R CNN "Voltage"
	1    2250 1600
	0    -1   -1   0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 61A9DCC3
P 2250 1350
AR Path="/5F05E355/61A9DCC3" Ref="R?"  Part="1" 
AR Path="/6068445B/61A9DCC3" Ref="R?"  Part="1" 
AR Path="/60E816A3/61A9DCC3" Ref="R?"  Part="1" 
AR Path="/60EB5E32/61A9DCC3" Ref="R?"  Part="1" 
AR Path="/5EA5DFF7/61A9DCC3" Ref="R?"  Part="1" 
AR Path="/5ED47D19/61A9DCC3" Ref="R?"  Part="1" 
AR Path="/61A61EC8/61A9DCC3" Ref="R221"  Part="1" 
F 0 "R221" H 2400 1400 50  0000 C CNN
F 1 "120" H 2375 1325 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 2250 1350 50  0001 C CNN
F 3 "~" H 2250 1350 50  0001 C CNN
F 4 "MCWR04X1200FTL -  SMD Chip Resistor, 0402 [1005 Metric], 120 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 2250 950 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x1200ftl/res-120r-1-0-0625w-thick-film/dp/2447103" H 2250 850 50  0001 C CNN "Distributor Link"
	1    2250 1350
	1    0    0    -1  
$EndComp
Wire Wire Line
	2250 1500 2250 1450
Wire Wire Line
	2250 1700 2250 1750
Wire Wire Line
	2250 1750 1775 1750
Wire Wire Line
	2250 1250 2250 1200
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_3V3 #PWR?
U 1 1 61A9DCCD
P 2250 1200
AR Path="/5EA5DDD6/61A9DCCD" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/61A9DCCD" Ref="#PWR?"  Part="1" 
AR Path="/61A61EC8/61A9DCCD" Ref="#PWR0480"  Part="1" 
F 0 "#PWR0480" H 2250 1450 50  0001 C CNN
F 1 "CTRL_3V3" H 2251 1373 50  0000 C CNN
F 2 "" H 2250 1200 50  0001 C CNN
F 3 "" H 2250 1200 50  0001 C CNN
	1    2250 1200
	1    0    0    -1  
$EndComp
Wire Wire Line
	850  1200 850  1550
Wire Wire Line
	1475 1250 1475 1450
Text Notes 675  800  0    100  ~ 0
CTRL_3V3 Power Good
Text Notes 700  875  0    25   ~ 0
Power good LED is on at 3.06V.
Wire Notes Line
	600  600  2625 600 
Wire Notes Line
	600  2475 2625 2475
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_DGND #PWR?
U 1 1 61A9DCDB
P 3575 2100
AR Path="/5EA5D9E8/61A9DCDB" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/61A9DCDB" Ref="#PWR?"  Part="1" 
AR Path="/61A61EC8/61A9DCDB" Ref="#PWR0487"  Part="1" 
F 0 "#PWR0487" H 3575 1850 50  0001 C CNN
F 1 "CTRL_DGND" H 3580 1927 50  0000 C CNN
F 2 "" H 3575 2100 50  0001 C CNN
F 3 "" H 3575 2100 50  0001 C CNN
	1    3575 2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	3575 2050 3575 2100
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 61A9DCE4
P 3800 1250
AR Path="/5E9C6910/61A9DCE4" Ref="C?"  Part="1" 
AR Path="/5EBC6654/61A9DCE4" Ref="C?"  Part="1" 
AR Path="/5EBC681B/61A9DCE4" Ref="C?"  Part="1" 
AR Path="/5EA5DDD6/61A9DCE4" Ref="C?"  Part="1" 
AR Path="/5ED47D19/61A9DCE4" Ref="C?"  Part="1" 
AR Path="/61A61EC8/61A9DCE4" Ref="C260"  Part="1" 
F 0 "C260" V 3925 1150 50  0000 L CNN
F 1 "100nF" V 3850 975 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 3800 1250 50  0001 C CNN
F 3 "~" H 3800 1250 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 3800 850 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 3800 750 50  0001 C CNN "Distributor Link"
	1    3800 1250
	0    -1   -1   0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_3V3 #PWR?
U 1 1 61A9DCEA
P 3575 1200
AR Path="/5EA5DDD6/61A9DCEA" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/61A9DCEA" Ref="#PWR?"  Part="1" 
AR Path="/61A61EC8/61A9DCEA" Ref="#PWR0486"  Part="1" 
F 0 "#PWR0486" H 3575 1450 50  0001 C CNN
F 1 "CTRL_3V3" H 3576 1373 50  0000 C CNN
F 2 "" H 3575 1200 50  0001 C CNN
F 3 "" H 3575 1200 50  0001 C CNN
	1    3575 1200
	1    0    0    -1  
$EndComp
Wire Wire Line
	3900 1250 4000 1250
Wire Wire Line
	4000 1250 4000 1300
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_DGND #PWR?
U 1 1 61A9DCF2
P 4000 1300
AR Path="/5EA5DDD6/61A9DCF2" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/61A9DCF2" Ref="#PWR?"  Part="1" 
AR Path="/61A61EC8/61A9DCF2" Ref="#PWR0490"  Part="1" 
F 0 "#PWR0490" H 4000 1050 50  0001 C CNN
F 1 "CTRL_DGND" H 4005 1127 50  0000 C CNN
F 2 "" H 4000 1300 50  0001 C CNN
F 3 "" H 4000 1300 50  0001 C CNN
	1    4000 1300
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 61A9DCFA
P 2950 1650
AR Path="/5F05E355/61A9DCFA" Ref="R?"  Part="1" 
AR Path="/6068445B/61A9DCFA" Ref="R?"  Part="1" 
AR Path="/60E816A3/61A9DCFA" Ref="R?"  Part="1" 
AR Path="/60EB5E32/61A9DCFA" Ref="R?"  Part="1" 
AR Path="/5EA5DFF7/61A9DCFA" Ref="R?"  Part="1" 
AR Path="/5ED47D19/61A9DCFA" Ref="R?"  Part="1" 
AR Path="/61A61EC8/61A9DCFA" Ref="R223"  Part="1" 
F 0 "R223" H 3100 1675 50  0000 C CNN
F 1 "62k" H 3050 1575 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 2950 1650 50  0001 C CNN
F 3 "~" H 2950 1650 50  0001 C CNN
F 4 "MCWR04X6202FTL -  SMD Chip Resistor, 0402 [1005 Metric], 62 kohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 2950 1250 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x6202ftl/res-62k-1-0-0625w-thick-film/dp/2447204" H 2950 1150 50  0001 C CNN "Distributor Link"
	1    2950 1650
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 61A9DD02
P 2950 1950
AR Path="/5F05E355/61A9DD02" Ref="R?"  Part="1" 
AR Path="/6068445B/61A9DD02" Ref="R?"  Part="1" 
AR Path="/60E816A3/61A9DD02" Ref="R?"  Part="1" 
AR Path="/60EB5E32/61A9DD02" Ref="R?"  Part="1" 
AR Path="/5EA5DFF7/61A9DD02" Ref="R?"  Part="1" 
AR Path="/5ED47D19/61A9DD02" Ref="R?"  Part="1" 
AR Path="/61A61EC8/61A9DD02" Ref="R224"  Part="1" 
F 0 "R224" H 3100 1975 50  0000 C CNN
F 1 "36k" H 3050 1875 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 2950 1950 50  0001 C CNN
F 3 "~" H 2950 1950 50  0001 C CNN
F 4 "MCWR04X3602FTL -  SMD Chip Resistor, 0402 [1005 Metric], 36 kohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 2950 1550 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x3602ftl/res-36k-1-0-0625w-thick-film/dp/2447164" H 2950 1450 50  0001 C CNN "Distributor Link"
	1    2950 1950
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_DGND #PWR?
U 1 1 61A9DD08
P 2950 2100
AR Path="/5EA5D9E8/61A9DD08" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/61A9DD08" Ref="#PWR?"  Part="1" 
AR Path="/61A61EC8/61A9DD08" Ref="#PWR0483"  Part="1" 
F 0 "#PWR0483" H 2950 1850 50  0001 C CNN
F 1 "CTRL_DGND" H 2955 1927 50  0000 C CNN
F 2 "" H 2950 2100 50  0001 C CNN
F 3 "" H 2950 2100 50  0001 C CNN
	1    2950 2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	2950 2100 2950 2050
Wire Wire Line
	3175 1800 2950 1800
Wire Wire Line
	2950 1800 2950 1850
Wire Wire Line
	2950 1800 2950 1750
Connection ~ 2950 1800
$Comp
L PMSM_Driver_LV_Board_IC:ADCMP350YKSZ U?
U 1 1 61A9DD16
P 3525 1750
AR Path="/5ED47D19/61A9DD16" Ref="U?"  Part="1" 
AR Path="/61A61EC8/61A9DD16" Ref="U61"  Part="1" 
F 0 "U61" H 3300 2000 50  0000 L CNN
F 1 "ADCMP350YKSZ" H 3600 1500 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:Analog_KS-4" H 3525 900 50  0001 C CNN
F 3 "" H 2675 2150 50  0001 C CNN
F 4 "ADCMP350YKSZ-REEL7 -  Analogue Comparator, Single, General Purpose, 1 Comparator, 5 µs, 2.25V to 5.5V, SC-70, 4 Pins" H 3525 800 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/analog-devices/adcmp350yksz-reel7/comparator-single-5us-sc-70-4/dp/2376802RL" H 3525 700 50  0001 C CNN "Distributor Link"
F 6 "https://www.mouser.co.uk/ProductDetail/Analog-Devices/ADCMP350YKSZ-REEL7?qs=sGAEpiMZZMv9Q1JI0Mo%2FtaNnJ1AHFVFg" H 3525 600 50  0001 C CNN "Distributor Link 2"
	1    3525 1750
	1    0    0    -1  
$EndComp
Wire Wire Line
	3575 1200 3575 1250
Wire Wire Line
	3700 1250 3575 1250
Connection ~ 3575 1250
$Comp
L PMSM_Driver_LV_Board_Device:LED_Small D?
U 1 1 61A9DD22
P 4350 1600
AR Path="/5ED47D19/61A9DD22" Ref="D?"  Part="1" 
AR Path="/61A61EC8/61A9DD22" Ref="D41"  Part="1" 
F 0 "D41" V 4425 1550 50  0000 R CNN
F 1 "KPT-2012SGC" V 4350 1550 25  0000 R CNN
F 2 "PMSM_Driver_LV_Board:LED_0805_2012Metric" V 4350 1600 50  0001 C CNN
F 3 "~" V 4350 1600 50  0001 C CNN
F 4 "KPT-2012SGC -  LED, Low Power, Green, SMD, 0805, 20 mA, 2.2 V, 568 nm" H 4350 1400 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/kingbright/kpt-2012sgc/led-0805-green-12mcd-568nm/dp/2099239" H 4350 1300 50  0001 C CNN "Distributor Link"
F 6 "2.2V" V 4275 1550 50  0000 R CNN "Voltage"
	1    4350 1600
	0    -1   -1   0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 61A9DD2A
P 4350 1350
AR Path="/5F05E355/61A9DD2A" Ref="R?"  Part="1" 
AR Path="/6068445B/61A9DD2A" Ref="R?"  Part="1" 
AR Path="/60E816A3/61A9DD2A" Ref="R?"  Part="1" 
AR Path="/60EB5E32/61A9DD2A" Ref="R?"  Part="1" 
AR Path="/5EA5DFF7/61A9DD2A" Ref="R?"  Part="1" 
AR Path="/5ED47D19/61A9DD2A" Ref="R?"  Part="1" 
AR Path="/61A61EC8/61A9DD2A" Ref="R228"  Part="1" 
F 0 "R228" H 4500 1400 50  0000 C CNN
F 1 "120" H 4475 1325 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 4350 1350 50  0001 C CNN
F 3 "~" H 4350 1350 50  0001 C CNN
F 4 "MCWR04X1200FTL -  SMD Chip Resistor, 0402 [1005 Metric], 120 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 4350 950 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x1200ftl/res-120r-1-0-0625w-thick-film/dp/2447103" H 4350 850 50  0001 C CNN "Distributor Link"
	1    4350 1350
	1    0    0    -1  
$EndComp
Wire Wire Line
	4350 1500 4350 1450
Wire Wire Line
	4350 1700 4350 1750
Wire Wire Line
	4350 1750 3875 1750
Wire Wire Line
	4350 1250 4350 1200
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_3V3 #PWR?
U 1 1 61A9DD34
P 4350 1200
AR Path="/5EA5DDD6/61A9DD34" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/61A9DD34" Ref="#PWR?"  Part="1" 
AR Path="/61A61EC8/61A9DD34" Ref="#PWR0493"  Part="1" 
F 0 "#PWR0493" H 4350 1450 50  0001 C CNN
F 1 "CTRL_3V3" H 4351 1373 50  0000 C CNN
F 2 "" H 4350 1200 50  0001 C CNN
F 3 "" H 4350 1200 50  0001 C CNN
	1    4350 1200
	1    0    0    -1  
$EndComp
Wire Wire Line
	2950 1200 2950 1550
Wire Wire Line
	3575 1250 3575 1450
Text Notes 2775 800  0    100  ~ 0
CTRL_VADJ Power Good
Text Notes 2800 875  0    25   ~ 0
Power good LED is on at 1.63V.
Wire Notes Line
	2700 600  4725 600 
Wire Notes Line
	2700 2475 4725 2475
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_DGND #PWR?
U 1 1 61A9DD42
P 5675 2100
AR Path="/5EA5D9E8/61A9DD42" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/61A9DD42" Ref="#PWR?"  Part="1" 
AR Path="/61A61EC8/61A9DD42" Ref="#PWR0500"  Part="1" 
F 0 "#PWR0500" H 5675 1850 50  0001 C CNN
F 1 "CTRL_DGND" H 5680 1927 50  0000 C CNN
F 2 "" H 5675 2100 50  0001 C CNN
F 3 "" H 5675 2100 50  0001 C CNN
	1    5675 2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	5675 2050 5675 2100
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 61A9DD4B
P 5900 1250
AR Path="/5E9C6910/61A9DD4B" Ref="C?"  Part="1" 
AR Path="/5EBC6654/61A9DD4B" Ref="C?"  Part="1" 
AR Path="/5EBC681B/61A9DD4B" Ref="C?"  Part="1" 
AR Path="/5EA5DDD6/61A9DD4B" Ref="C?"  Part="1" 
AR Path="/5ED47D19/61A9DD4B" Ref="C?"  Part="1" 
AR Path="/61A61EC8/61A9DD4B" Ref="C262"  Part="1" 
F 0 "C262" V 6025 1150 50  0000 L CNN
F 1 "100nF" V 5950 975 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 5900 1250 50  0001 C CNN
F 3 "~" H 5900 1250 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 5900 850 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 5900 750 50  0001 C CNN "Distributor Link"
	1    5900 1250
	0    -1   -1   0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_3V3 #PWR?
U 1 1 61A9DD51
P 5675 1200
AR Path="/5EA5DDD6/61A9DD51" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/61A9DD51" Ref="#PWR?"  Part="1" 
AR Path="/61A61EC8/61A9DD51" Ref="#PWR0499"  Part="1" 
F 0 "#PWR0499" H 5675 1450 50  0001 C CNN
F 1 "CTRL_3V3" H 5676 1373 50  0000 C CNN
F 2 "" H 5675 1200 50  0001 C CNN
F 3 "" H 5675 1200 50  0001 C CNN
	1    5675 1200
	1    0    0    -1  
$EndComp
Wire Wire Line
	6000 1250 6100 1250
Wire Wire Line
	6100 1250 6100 1300
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_DGND #PWR?
U 1 1 61A9DD59
P 6100 1300
AR Path="/5EA5DDD6/61A9DD59" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/61A9DD59" Ref="#PWR?"  Part="1" 
AR Path="/61A61EC8/61A9DD59" Ref="#PWR0503"  Part="1" 
F 0 "#PWR0503" H 6100 1050 50  0001 C CNN
F 1 "CTRL_DGND" H 6105 1127 50  0000 C CNN
F 2 "" H 6100 1300 50  0001 C CNN
F 3 "" H 6100 1300 50  0001 C CNN
	1    6100 1300
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 61A9DD61
P 5050 1650
AR Path="/5F05E355/61A9DD61" Ref="R?"  Part="1" 
AR Path="/6068445B/61A9DD61" Ref="R?"  Part="1" 
AR Path="/60E816A3/61A9DD61" Ref="R?"  Part="1" 
AR Path="/60EB5E32/61A9DD61" Ref="R?"  Part="1" 
AR Path="/5EA5DFF7/61A9DD61" Ref="R?"  Part="1" 
AR Path="/5ED47D19/61A9DD61" Ref="R?"  Part="1" 
AR Path="/61A61EC8/61A9DD61" Ref="R230"  Part="1" 
F 0 "R230" H 5200 1675 50  0000 C CNN
F 1 "68k1" H 5175 1575 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 5050 1650 50  0001 C CNN
F 3 "~" H 5050 1650 50  0001 C CNN
F 4 "MCMR04X6812FTL -  SMD Chip Resistor, Ceramic, 0402 [1005 Metric], 68.1 kohm, MCMR Series, 50 V, Thick Film, 62.5 mW" H 5050 1250 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcmr04x6812ftl/res-68k1-1-0-0625w-0402-ceramic/dp/2073208" H 5050 1150 50  0001 C CNN "Distributor Link"
	1    5050 1650
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 61A9DD69
P 5050 1950
AR Path="/5F05E355/61A9DD69" Ref="R?"  Part="1" 
AR Path="/6068445B/61A9DD69" Ref="R?"  Part="1" 
AR Path="/60E816A3/61A9DD69" Ref="R?"  Part="1" 
AR Path="/60EB5E32/61A9DD69" Ref="R?"  Part="1" 
AR Path="/5EA5DFF7/61A9DD69" Ref="R?"  Part="1" 
AR Path="/5ED47D19/61A9DD69" Ref="R?"  Part="1" 
AR Path="/61A61EC8/61A9DD69" Ref="R231"  Part="1" 
F 0 "R231" H 5200 1975 50  0000 C CNN
F 1 "10k" H 5150 1875 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 5050 1950 50  0001 C CNN
F 3 "~" H 5050 1950 50  0001 C CNN
F 4 "MCWR04X1002FTL -  SMD Chip Resistor, 0402 [1005 Metric], 10 kohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 5050 1550 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x1002ftl/res-10k-1-0-0625w-thick-film/dp/2447096" H 5050 1450 50  0001 C CNN "Distributor Link"
	1    5050 1950
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_DGND #PWR?
U 1 1 61A9DD6F
P 5050 2100
AR Path="/5EA5D9E8/61A9DD6F" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/61A9DD6F" Ref="#PWR?"  Part="1" 
AR Path="/61A61EC8/61A9DD6F" Ref="#PWR0496"  Part="1" 
F 0 "#PWR0496" H 5050 1850 50  0001 C CNN
F 1 "CTRL_DGND" H 5055 1927 50  0000 C CNN
F 2 "" H 5050 2100 50  0001 C CNN
F 3 "" H 5050 2100 50  0001 C CNN
	1    5050 2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	5050 2100 5050 2050
Wire Wire Line
	5275 1800 5050 1800
Wire Wire Line
	5050 1800 5050 1850
Wire Wire Line
	5050 1800 5050 1750
Connection ~ 5050 1800
$Comp
L PMSM_Driver_LV_Board_IC:ADCMP350YKSZ U?
U 1 1 61A9DD7D
P 5625 1750
AR Path="/5ED47D19/61A9DD7D" Ref="U?"  Part="1" 
AR Path="/61A61EC8/61A9DD7D" Ref="U63"  Part="1" 
F 0 "U63" H 5400 2000 50  0000 L CNN
F 1 "ADCMP350YKSZ" H 5700 1500 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:Analog_KS-4" H 5625 900 50  0001 C CNN
F 3 "" H 4775 2150 50  0001 C CNN
F 4 "ADCMP350YKSZ-REEL7 -  Analogue Comparator, Single, General Purpose, 1 Comparator, 5 µs, 2.25V to 5.5V, SC-70, 4 Pins" H 5625 800 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/analog-devices/adcmp350yksz-reel7/comparator-single-5us-sc-70-4/dp/2376802RL" H 5625 700 50  0001 C CNN "Distributor Link"
F 6 "https://www.mouser.co.uk/ProductDetail/Analog-Devices/ADCMP350YKSZ-REEL7?qs=sGAEpiMZZMv9Q1JI0Mo%2FtaNnJ1AHFVFg" H 5625 600 50  0001 C CNN "Distributor Link 2"
	1    5625 1750
	1    0    0    -1  
$EndComp
Wire Wire Line
	5675 1200 5675 1250
Wire Wire Line
	5800 1250 5675 1250
Connection ~ 5675 1250
$Comp
L PMSM_Driver_LV_Board_Device:LED_Small D?
U 1 1 61A9DD89
P 6450 1600
AR Path="/5ED47D19/61A9DD89" Ref="D?"  Part="1" 
AR Path="/61A61EC8/61A9DD89" Ref="D43"  Part="1" 
F 0 "D43" V 6525 1550 50  0000 R CNN
F 1 "KPT-2012SGC" V 6450 1550 25  0000 R CNN
F 2 "PMSM_Driver_LV_Board:LED_0805_2012Metric" V 6450 1600 50  0001 C CNN
F 3 "~" V 6450 1600 50  0001 C CNN
F 4 "KPT-2012SGC -  LED, Low Power, Green, SMD, 0805, 20 mA, 2.2 V, 568 nm" H 6450 1400 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/kingbright/kpt-2012sgc/led-0805-green-12mcd-568nm/dp/2099239" H 6450 1300 50  0001 C CNN "Distributor Link"
F 6 "2.2V" V 6375 1550 50  0000 R CNN "Voltage"
	1    6450 1600
	0    -1   -1   0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 61A9DD91
P 6450 1350
AR Path="/5F05E355/61A9DD91" Ref="R?"  Part="1" 
AR Path="/6068445B/61A9DD91" Ref="R?"  Part="1" 
AR Path="/60E816A3/61A9DD91" Ref="R?"  Part="1" 
AR Path="/60EB5E32/61A9DD91" Ref="R?"  Part="1" 
AR Path="/5EA5DFF7/61A9DD91" Ref="R?"  Part="1" 
AR Path="/5ED47D19/61A9DD91" Ref="R?"  Part="1" 
AR Path="/61A61EC8/61A9DD91" Ref="R234"  Part="1" 
F 0 "R234" H 6600 1400 50  0000 C CNN
F 1 "120" H 6575 1325 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 6450 1350 50  0001 C CNN
F 3 "~" H 6450 1350 50  0001 C CNN
F 4 "MCWR04X1200FTL -  SMD Chip Resistor, 0402 [1005 Metric], 120 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 6450 950 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x1200ftl/res-120r-1-0-0625w-thick-film/dp/2447103" H 6450 850 50  0001 C CNN "Distributor Link"
	1    6450 1350
	1    0    0    -1  
$EndComp
Wire Wire Line
	6450 1500 6450 1450
Wire Wire Line
	6450 1700 6450 1750
Wire Wire Line
	6450 1750 5975 1750
Wire Wire Line
	6450 1250 6450 1200
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_3V3 #PWR?
U 1 1 61A9DD9B
P 6450 1200
AR Path="/5EA5DDD6/61A9DD9B" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/61A9DD9B" Ref="#PWR?"  Part="1" 
AR Path="/61A61EC8/61A9DD9B" Ref="#PWR0505"  Part="1" 
F 0 "#PWR0505" H 6450 1450 50  0001 C CNN
F 1 "CTRL_3V3" H 6451 1373 50  0000 C CNN
F 2 "" H 6450 1200 50  0001 C CNN
F 3 "" H 6450 1200 50  0001 C CNN
	1    6450 1200
	1    0    0    -1  
$EndComp
Wire Wire Line
	5050 1200 5050 1550
Wire Wire Line
	5675 1250 5675 1450
Text Notes 4875 800  0    100  ~ 0
CTRL_5V0 Power Good
Text Notes 4900 875  0    25   ~ 0
Power good LED is on at 4.69V.
Wire Notes Line
	4800 600  6825 600 
Wire Notes Line
	4800 2475 6825 2475
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_VADJ #PWR?
U 1 1 61A9DDA9
P 2950 1200
AR Path="/5E9C6910/61A9DDA9" Ref="#PWR?"  Part="1" 
AR Path="/5EBC6654/61A9DDA9" Ref="#PWR?"  Part="1" 
AR Path="/5EA5D9E8/61A9DDA9" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/61A9DDA9" Ref="#PWR?"  Part="1" 
AR Path="/61A61EC8/61A9DDA9" Ref="#PWR0482"  Part="1" 
F 0 "#PWR0482" H 2950 1450 50  0001 C CNN
F 1 "CTRL_VADJ" H 2951 1373 50  0000 C CNN
F 2 "" H 2950 1200 50  0001 C CNN
F 3 "" H 2950 1200 50  0001 C CNN
	1    2950 1200
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_5V0 #PWR?
U 1 1 61A9DDAF
P 5050 1200
AR Path="/5ED47D19/61A9DDAF" Ref="#PWR?"  Part="1" 
AR Path="/61A61EC8/61A9DDAF" Ref="#PWR0495"  Part="1" 
F 0 "#PWR0495" H 5050 1450 50  0001 C CNN
F 1 "CTRL_5V0" H 5051 1373 50  0000 C CNN
F 2 "" H 5050 1200 50  0001 C CNN
F 3 "" H 5050 1200 50  0001 C CNN
	1    5050 1200
	1    0    0    -1  
$EndComp
Wire Wire Line
	1475 4000 1475 4050
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 61ABE2E8
P 1700 3200
AR Path="/5E9C6910/61ABE2E8" Ref="C?"  Part="1" 
AR Path="/5EBC6654/61ABE2E8" Ref="C?"  Part="1" 
AR Path="/5EBC681B/61ABE2E8" Ref="C?"  Part="1" 
AR Path="/5EA5DDD6/61ABE2E8" Ref="C?"  Part="1" 
AR Path="/5ED47D19/61ABE2E8" Ref="C?"  Part="1" 
AR Path="/5FE92F3B/61ABE2E8" Ref="C?"  Part="1" 
AR Path="/61A61EC8/61ABE2E8" Ref="C259"  Part="1" 
F 0 "C259" V 1825 3100 50  0000 L CNN
F 1 "100nF" V 1750 2925 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 1700 3200 50  0001 C CNN
F 3 "~" H 1700 3200 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 1700 2800 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 1700 2700 50  0001 C CNN "Distributor Link"
	1    1700 3200
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1800 3200 1900 3200
Wire Wire Line
	1900 3200 1900 3250
Wire Wire Line
	850  4050 850  4000
$Comp
L PMSM_Driver_LV_Board_IC:ADCMP350YKSZ U?
U 1 1 61ABE320
P 1425 3700
AR Path="/5ED47D19/61ABE320" Ref="U?"  Part="1" 
AR Path="/5FE92F3B/61ABE320" Ref="U?"  Part="1" 
AR Path="/61A61EC8/61ABE320" Ref="U60"  Part="1" 
F 0 "U60" H 1200 3950 50  0000 L CNN
F 1 "ADCMP350YKSZ" H 1500 3450 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:Analog_KS-4" H 1425 2850 50  0001 C CNN
F 3 "" H 575 4100 50  0001 C CNN
F 4 "ADCMP350YKSZ-REEL7 -  Analogue Comparator, Single, General Purpose, 1 Comparator, 5 µs, 2.25V to 5.5V, SC-70, 4 Pins" H 1425 2750 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/analog-devices/adcmp350yksz-reel7/comparator-single-5us-sc-70-4/dp/2376802RL" H 1425 2650 50  0001 C CNN "Distributor Link"
F 6 "https://www.mouser.co.uk/ProductDetail/Analog-Devices/ADCMP350YKSZ-REEL7?qs=sGAEpiMZZMv9Q1JI0Mo%2FtaNnJ1AHFVFg" H 1425 2550 50  0001 C CNN "Distributor Link 2"
	1    1425 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	1475 3150 1475 3200
Wire Wire Line
	1600 3200 1475 3200
Connection ~ 1475 3200
$Comp
L PMSM_Driver_LV_Board_Device:LED_Small D?
U 1 1 61ABE32C
P 2250 3550
AR Path="/5ED47D19/61ABE32C" Ref="D?"  Part="1" 
AR Path="/5FE92F3B/61ABE32C" Ref="D?"  Part="1" 
AR Path="/61A61EC8/61ABE32C" Ref="D39"  Part="1" 
F 0 "D39" V 2325 3500 50  0000 R CNN
F 1 "KPT-2012SGC" V 2250 3500 25  0000 R CNN
F 2 "PMSM_Driver_LV_Board:LED_0805_2012Metric" V 2250 3550 50  0001 C CNN
F 3 "~" V 2250 3550 50  0001 C CNN
F 4 "KPT-2012SGC -  LED, Low Power, Green, SMD, 0805, 20 mA, 2.2 V, 568 nm" H 2250 3350 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/kingbright/kpt-2012sgc/led-0805-green-12mcd-568nm/dp/2099239" H 2250 3250 50  0001 C CNN "Distributor Link"
F 6 "2.2V" V 2175 3500 50  0000 R CNN "Voltage"
	1    2250 3550
	0    -1   -1   0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 61ABE334
P 2250 3300
AR Path="/5F05E355/61ABE334" Ref="R?"  Part="1" 
AR Path="/6068445B/61ABE334" Ref="R?"  Part="1" 
AR Path="/60E816A3/61ABE334" Ref="R?"  Part="1" 
AR Path="/60EB5E32/61ABE334" Ref="R?"  Part="1" 
AR Path="/5EA5DFF7/61ABE334" Ref="R?"  Part="1" 
AR Path="/5ED47D19/61ABE334" Ref="R?"  Part="1" 
AR Path="/5FE92F3B/61ABE334" Ref="R?"  Part="1" 
AR Path="/61A61EC8/61ABE334" Ref="R222"  Part="1" 
F 0 "R222" H 2400 3350 50  0000 C CNN
F 1 "220" H 2375 3275 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 2250 3300 50  0001 C CNN
F 3 "~" H 2250 3300 50  0001 C CNN
F 4 "MCWR04X2200FTL -  SMD Chip Resistor, 0402 [1005 Metric], 220 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 2250 2900 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x2200ftl/res-220r-1-0-0625w-thick-film/dp/2447136" H 2250 2800 50  0001 C CNN "Distributor Link"
	1    2250 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	2250 3450 2250 3400
Wire Wire Line
	2250 3650 2250 3700
Wire Wire Line
	2250 3700 1775 3700
Wire Wire Line
	2250 3200 2250 3150
Wire Wire Line
	850  3150 850  3500
Wire Wire Line
	1475 3200 1475 3400
Text Notes 675  2750 0    100  ~ 0
MOT_5V0 Power Good
Text Notes 700  2825 0    25   ~ 0
Power good LED is on at 4.69V.
Wire Notes Line
	600  2550 2625 2550
Wire Notes Line
	600  4425 2625 4425
Wire Wire Line
	3575 4000 3575 4050
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 61ABE355
P 3800 3200
AR Path="/5E9C6910/61ABE355" Ref="C?"  Part="1" 
AR Path="/5EBC6654/61ABE355" Ref="C?"  Part="1" 
AR Path="/5EBC681B/61ABE355" Ref="C?"  Part="1" 
AR Path="/5EA5DDD6/61ABE355" Ref="C?"  Part="1" 
AR Path="/5ED47D19/61ABE355" Ref="C?"  Part="1" 
AR Path="/5FE92F3B/61ABE355" Ref="C?"  Part="1" 
AR Path="/61A61EC8/61ABE355" Ref="C261"  Part="1" 
F 0 "C261" V 3925 3100 50  0000 L CNN
F 1 "100nF" V 3850 2925 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 3800 3200 50  0001 C CNN
F 3 "~" H 3800 3200 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 3800 2800 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 3800 2700 50  0001 C CNN "Distributor Link"
	1    3800 3200
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3900 3200 4000 3200
Wire Wire Line
	4000 3200 4000 3250
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 61ABE36B
P 2950 3600
AR Path="/5F05E355/61ABE36B" Ref="R?"  Part="1" 
AR Path="/6068445B/61ABE36B" Ref="R?"  Part="1" 
AR Path="/60E816A3/61ABE36B" Ref="R?"  Part="1" 
AR Path="/60EB5E32/61ABE36B" Ref="R?"  Part="1" 
AR Path="/5EA5DFF7/61ABE36B" Ref="R?"  Part="1" 
AR Path="/5ED47D19/61ABE36B" Ref="R?"  Part="1" 
AR Path="/5FE92F3B/61ABE36B" Ref="R?"  Part="1" 
AR Path="/61A61EC8/61ABE36B" Ref="R225"  Part="1" 
F 0 "R225" H 3100 3625 50  0000 C CNN
F 1 "180k" H 3075 3525 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 2950 3600 50  0001 C CNN
F 3 "~" H 2950 3600 50  0001 C CNN
F 4 "WR04X1803FTL -  SMD Chip Resistor, 0402 [1005 Metric], 180 kohm, WR04 Series, 50 V, Thick Film, 62.5 mW" H 2950 3200 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/walsin/wr04x1803ftl/res-180k-1-50v-0402-thick-film/dp/2502519" H 2950 3100 50  0001 C CNN "Distributor Link"
	1    2950 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	2950 4050 2950 4000
Wire Wire Line
	3175 3750 2950 3750
Wire Wire Line
	2950 3750 2950 3800
Wire Wire Line
	2950 3750 2950 3700
Connection ~ 2950 3750
$Comp
L PMSM_Driver_LV_Board_IC:ADCMP350YKSZ U?
U 1 1 61ABE387
P 3525 3700
AR Path="/5ED47D19/61ABE387" Ref="U?"  Part="1" 
AR Path="/5FE92F3B/61ABE387" Ref="U?"  Part="1" 
AR Path="/61A61EC8/61ABE387" Ref="U62"  Part="1" 
F 0 "U62" H 3300 3950 50  0000 L CNN
F 1 "ADCMP350YKSZ" H 3600 3450 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:Analog_KS-4" H 3525 2850 50  0001 C CNN
F 3 "" H 2675 4100 50  0001 C CNN
F 4 "ADCMP350YKSZ-REEL7 -  Analogue Comparator, Single, General Purpose, 1 Comparator, 5 µs, 2.25V to 5.5V, SC-70, 4 Pins" H 3525 2750 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/analog-devices/adcmp350yksz-reel7/comparator-single-5us-sc-70-4/dp/2376802RL" H 3525 2650 50  0001 C CNN "Distributor Link"
F 6 "https://www.mouser.co.uk/ProductDetail/Analog-Devices/ADCMP350YKSZ-REEL7?qs=sGAEpiMZZMv9Q1JI0Mo%2FtaNnJ1AHFVFg" H 3525 2550 50  0001 C CNN "Distributor Link 2"
	1    3525 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	3575 3150 3575 3200
Wire Wire Line
	3700 3200 3575 3200
Connection ~ 3575 3200
$Comp
L PMSM_Driver_LV_Board_Device:LED_Small D?
U 1 1 61ABE393
P 4350 3550
AR Path="/5ED47D19/61ABE393" Ref="D?"  Part="1" 
AR Path="/5FE92F3B/61ABE393" Ref="D?"  Part="1" 
AR Path="/61A61EC8/61ABE393" Ref="D42"  Part="1" 
F 0 "D42" V 4425 3500 50  0000 R CNN
F 1 "KPT-2012SGC" V 4350 3500 25  0000 R CNN
F 2 "PMSM_Driver_LV_Board:LED_0805_2012Metric" V 4350 3550 50  0001 C CNN
F 3 "~" V 4350 3550 50  0001 C CNN
F 4 "KPT-2012SGC -  LED, Low Power, Green, SMD, 0805, 20 mA, 2.2 V, 568 nm" H 4350 3350 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/kingbright/kpt-2012sgc/led-0805-green-12mcd-568nm/dp/2099239" H 4350 3250 50  0001 C CNN "Distributor Link"
F 6 "2.2V" V 4275 3500 50  0000 R CNN "Voltage"
	1    4350 3550
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4350 3450 4350 3400
Wire Wire Line
	4350 3650 4350 3700
Wire Wire Line
	4350 3700 3875 3700
Wire Wire Line
	4350 3200 4350 3150
Wire Wire Line
	2950 3150 2950 3500
Wire Wire Line
	3575 3200 3575 3400
Text Notes 2775 2750 0    100  ~ 0
MOT_15V0 Power Good
Text Notes 2800 2825 0    25   ~ 0
Power good LED is on at 11.4V (this supply can be configured as 12V supply).
Wire Notes Line
	2700 2550 4725 2550
Wire Notes Line
	2700 4425 4725 4425
Wire Wire Line
	5675 4000 5675 4050
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 61ABE3BC
P 5900 3200
AR Path="/5E9C6910/61ABE3BC" Ref="C?"  Part="1" 
AR Path="/5EBC6654/61ABE3BC" Ref="C?"  Part="1" 
AR Path="/5EBC681B/61ABE3BC" Ref="C?"  Part="1" 
AR Path="/5EA5DDD6/61ABE3BC" Ref="C?"  Part="1" 
AR Path="/5ED47D19/61ABE3BC" Ref="C?"  Part="1" 
AR Path="/5FE92F3B/61ABE3BC" Ref="C?"  Part="1" 
AR Path="/61A61EC8/61ABE3BC" Ref="C263"  Part="1" 
F 0 "C263" V 6025 3100 50  0000 L CNN
F 1 "100nF" V 5950 2925 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 5900 3200 50  0001 C CNN
F 3 "~" H 5900 3200 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 5900 2800 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 5900 2700 50  0001 C CNN "Distributor Link"
	1    5900 3200
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6000 3200 6100 3200
Wire Wire Line
	6100 3200 6100 3250
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 61ABE3DA
P 5050 3900
AR Path="/5F05E355/61ABE3DA" Ref="R?"  Part="1" 
AR Path="/6068445B/61ABE3DA" Ref="R?"  Part="1" 
AR Path="/60E816A3/61ABE3DA" Ref="R?"  Part="1" 
AR Path="/60EB5E32/61ABE3DA" Ref="R?"  Part="1" 
AR Path="/5EA5DFF7/61ABE3DA" Ref="R?"  Part="1" 
AR Path="/5ED47D19/61ABE3DA" Ref="R?"  Part="1" 
AR Path="/5FE92F3B/61ABE3DA" Ref="R?"  Part="1" 
AR Path="/61A61EC8/61ABE3DA" Ref="R233"  Part="1" 
F 0 "R233" H 5200 3925 50  0000 C CNN
F 1 "10k" H 5150 3825 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 5050 3900 50  0001 C CNN
F 3 "~" H 5050 3900 50  0001 C CNN
F 4 "MCWR04X1002FTL -  SMD Chip Resistor, 0402 [1005 Metric], 10 kohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 5050 3500 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x1002ftl/res-10k-1-0-0625w-thick-film/dp/2447096" H 5050 3400 50  0001 C CNN "Distributor Link"
	1    5050 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	5050 4050 5050 4000
Wire Wire Line
	5275 3750 5050 3750
Wire Wire Line
	5050 3750 5050 3800
Wire Wire Line
	5050 3750 5050 3700
Connection ~ 5050 3750
$Comp
L PMSM_Driver_LV_Board_IC:ADCMP350YKSZ U?
U 1 1 61ABE3EE
P 5625 3700
AR Path="/5ED47D19/61ABE3EE" Ref="U?"  Part="1" 
AR Path="/5FE92F3B/61ABE3EE" Ref="U?"  Part="1" 
AR Path="/61A61EC8/61ABE3EE" Ref="U64"  Part="1" 
F 0 "U64" H 5400 3950 50  0000 L CNN
F 1 "ADCMP350YKSZ" H 5700 3450 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:Analog_KS-4" H 5625 2850 50  0001 C CNN
F 3 "" H 4775 4100 50  0001 C CNN
F 4 "ADCMP350YKSZ-REEL7 -  Analogue Comparator, Single, General Purpose, 1 Comparator, 5 µs, 2.25V to 5.5V, SC-70, 4 Pins" H 5625 2750 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/analog-devices/adcmp350yksz-reel7/comparator-single-5us-sc-70-4/dp/2376802RL" H 5625 2650 50  0001 C CNN "Distributor Link"
F 6 "https://www.mouser.co.uk/ProductDetail/Analog-Devices/ADCMP350YKSZ-REEL7?qs=sGAEpiMZZMv9Q1JI0Mo%2FtaNnJ1AHFVFg" H 5625 2550 50  0001 C CNN "Distributor Link 2"
	1    5625 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	5675 3150 5675 3200
Wire Wire Line
	5800 3200 5675 3200
Connection ~ 5675 3200
$Comp
L PMSM_Driver_LV_Board_Device:LED_Small D?
U 1 1 61ABE3FA
P 6450 3550
AR Path="/5ED47D19/61ABE3FA" Ref="D?"  Part="1" 
AR Path="/5FE92F3B/61ABE3FA" Ref="D?"  Part="1" 
AR Path="/61A61EC8/61ABE3FA" Ref="D44"  Part="1" 
F 0 "D44" V 6525 3500 50  0000 R CNN
F 1 "KPT-2012SGC" V 6450 3500 25  0000 R CNN
F 2 "PMSM_Driver_LV_Board:LED_0805_2012Metric" V 6450 3550 50  0001 C CNN
F 3 "~" V 6450 3550 50  0001 C CNN
F 4 "KPT-2012SGC -  LED, Low Power, Green, SMD, 0805, 20 mA, 2.2 V, 568 nm" H 6450 3350 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/kingbright/kpt-2012sgc/led-0805-green-12mcd-568nm/dp/2099239" H 6450 3250 50  0001 C CNN "Distributor Link"
F 6 "2.2V" V 6375 3500 50  0000 R CNN "Voltage"
	1    6450 3550
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6450 3450 6450 3400
Wire Wire Line
	6450 3650 6450 3700
Wire Wire Line
	6450 3700 5975 3700
Wire Wire Line
	6450 3200 6450 3150
Wire Wire Line
	5050 3150 5050 3500
Wire Wire Line
	5675 3200 5675 3400
Text Notes 4875 2750 0    100  ~ 0
MOT_VDC Power Good
Text Notes 4900 2825 0    25   ~ 0
Power good LED is on at 11.4V.
Wire Notes Line
	4800 2550 6825 2550
Wire Notes Line
	4800 4425 6825 4425
$Comp
L PMSM_Driver_LV_Board_Power:MOT_15V0 #PWR?
U 1 1 61AE28DA
P 2950 3150
AR Path="/5EA5E06A/61AE28DA" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/61AE28DA" Ref="#PWR?"  Part="1" 
AR Path="/5FE92F3B/61AE28DA" Ref="#PWR?"  Part="1" 
AR Path="/61A61EC8/61AE28DA" Ref="#PWR0484"  Part="1" 
F 0 "#PWR0484" H 2950 3400 50  0001 C CNN
F 1 "MOT_15V0" H 2951 3323 50  0000 C CNN
F 2 "" H 2950 3150 50  0001 C CNN
F 3 "" H 2950 3150 50  0001 C CNN
	1    2950 3150
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_5V0 #PWR?
U 1 1 61AE28E0
P 850 3150
AR Path="/5EFE605D/61AE28E0" Ref="#PWR?"  Part="1" 
AR Path="/5EFE7331/61AE28E0" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E1C2/61AE28E0" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/61AE28E0" Ref="#PWR?"  Part="1" 
AR Path="/5FE92F3B/61AE28E0" Ref="#PWR?"  Part="1" 
AR Path="/61A61EC8/61AE28E0" Ref="#PWR0470"  Part="1" 
F 0 "#PWR0470" H 850 3400 50  0001 C CNN
F 1 "MOT_5V0" H 851 3323 50  0000 C CNN
F 2 "" H 850 3150 50  0001 C CNN
F 3 "" H 850 3150 50  0001 C CNN
	1    850  3150
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR?
U 1 1 61AE609C
P 850 4050
AR Path="/5EA5E06A/61AE609C" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/61AE609C" Ref="#PWR?"  Part="1" 
AR Path="/5FE92F3B/61AE609C" Ref="#PWR?"  Part="1" 
AR Path="/61A61EC8/61AE609C" Ref="#PWR0471"  Part="1" 
F 0 "#PWR0471" H 850 3800 50  0001 C CNN
F 1 "MOT_DGND" H 855 3877 50  0000 C CNN
F 2 "" H 850 4050 50  0001 C CNN
F 3 "" H 850 4050 50  0001 C CNN
	1    850  4050
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_VDC #PWR?
U 1 1 61AE99E3
P 5050 3150
AR Path="/5EA5E295/61AE99E3" Ref="#PWR?"  Part="1" 
AR Path="/61A61EC8/61AE99E3" Ref="#PWR0497"  Part="1" 
F 0 "#PWR0497" H 5050 3400 50  0001 C CNN
F 1 "MOT_VDC" H 5051 3323 50  0000 C CNN
F 2 "" H 5050 3150 50  0001 C CNN
F 3 "" H 5050 3150 50  0001 C CNN
	1    5050 3150
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR?
U 1 1 61B1B5AB
P 1475 4050
AR Path="/5EA5E06A/61B1B5AB" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/61B1B5AB" Ref="#PWR?"  Part="1" 
AR Path="/5FE92F3B/61B1B5AB" Ref="#PWR?"  Part="1" 
AR Path="/61A61EC8/61B1B5AB" Ref="#PWR0475"  Part="1" 
F 0 "#PWR0475" H 1475 3800 50  0001 C CNN
F 1 "MOT_DGND" H 1480 3877 50  0000 C CNN
F 2 "" H 1475 4050 50  0001 C CNN
F 3 "" H 1475 4050 50  0001 C CNN
	1    1475 4050
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR?
U 1 1 61B1DE60
P 1900 3250
AR Path="/5EA5E06A/61B1DE60" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/61B1DE60" Ref="#PWR?"  Part="1" 
AR Path="/5FE92F3B/61B1DE60" Ref="#PWR?"  Part="1" 
AR Path="/61A61EC8/61B1DE60" Ref="#PWR0477"  Part="1" 
F 0 "#PWR0477" H 1900 3000 50  0001 C CNN
F 1 "MOT_DGND" H 1905 3077 50  0000 C CNN
F 2 "" H 1900 3250 50  0001 C CNN
F 3 "" H 1900 3250 50  0001 C CNN
	1    1900 3250
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR?
U 1 1 61B207AA
P 2950 4050
AR Path="/5EA5E06A/61B207AA" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/61B207AA" Ref="#PWR?"  Part="1" 
AR Path="/5FE92F3B/61B207AA" Ref="#PWR?"  Part="1" 
AR Path="/61A61EC8/61B207AA" Ref="#PWR0485"  Part="1" 
F 0 "#PWR0485" H 2950 3800 50  0001 C CNN
F 1 "MOT_DGND" H 2955 3877 50  0000 C CNN
F 2 "" H 2950 4050 50  0001 C CNN
F 3 "" H 2950 4050 50  0001 C CNN
	1    2950 4050
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR?
U 1 1 61B231BD
P 3575 4050
AR Path="/5EA5E06A/61B231BD" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/61B231BD" Ref="#PWR?"  Part="1" 
AR Path="/5FE92F3B/61B231BD" Ref="#PWR?"  Part="1" 
AR Path="/61A61EC8/61B231BD" Ref="#PWR0489"  Part="1" 
F 0 "#PWR0489" H 3575 3800 50  0001 C CNN
F 1 "MOT_DGND" H 3580 3877 50  0000 C CNN
F 2 "" H 3575 4050 50  0001 C CNN
F 3 "" H 3575 4050 50  0001 C CNN
	1    3575 4050
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR?
U 1 1 61B25B9B
P 4000 3250
AR Path="/5EA5E06A/61B25B9B" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/61B25B9B" Ref="#PWR?"  Part="1" 
AR Path="/5FE92F3B/61B25B9B" Ref="#PWR?"  Part="1" 
AR Path="/61A61EC8/61B25B9B" Ref="#PWR0491"  Part="1" 
F 0 "#PWR0491" H 4000 3000 50  0001 C CNN
F 1 "MOT_DGND" H 4005 3077 50  0000 C CNN
F 2 "" H 4000 3250 50  0001 C CNN
F 3 "" H 4000 3250 50  0001 C CNN
	1    4000 3250
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR?
U 1 1 61B28391
P 5050 4050
AR Path="/5EA5E06A/61B28391" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/61B28391" Ref="#PWR?"  Part="1" 
AR Path="/5FE92F3B/61B28391" Ref="#PWR?"  Part="1" 
AR Path="/61A61EC8/61B28391" Ref="#PWR0498"  Part="1" 
F 0 "#PWR0498" H 5050 3800 50  0001 C CNN
F 1 "MOT_DGND" H 5055 3877 50  0000 C CNN
F 2 "" H 5050 4050 50  0001 C CNN
F 3 "" H 5050 4050 50  0001 C CNN
	1    5050 4050
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR?
U 1 1 61B2ADFA
P 5675 4050
AR Path="/5EA5E06A/61B2ADFA" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/61B2ADFA" Ref="#PWR?"  Part="1" 
AR Path="/5FE92F3B/61B2ADFA" Ref="#PWR?"  Part="1" 
AR Path="/61A61EC8/61B2ADFA" Ref="#PWR0502"  Part="1" 
F 0 "#PWR0502" H 5675 3800 50  0001 C CNN
F 1 "MOT_DGND" H 5680 3877 50  0000 C CNN
F 2 "" H 5675 4050 50  0001 C CNN
F 3 "" H 5675 4050 50  0001 C CNN
	1    5675 4050
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR?
U 1 1 61B2D760
P 6100 3250
AR Path="/5EA5E06A/61B2D760" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/61B2D760" Ref="#PWR?"  Part="1" 
AR Path="/5FE92F3B/61B2D760" Ref="#PWR?"  Part="1" 
AR Path="/61A61EC8/61B2D760" Ref="#PWR0504"  Part="1" 
F 0 "#PWR0504" H 6100 3000 50  0001 C CNN
F 1 "MOT_DGND" H 6105 3077 50  0000 C CNN
F 2 "" H 6100 3250 50  0001 C CNN
F 3 "" H 6100 3250 50  0001 C CNN
	1    6100 3250
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_5V0 #PWR?
U 1 1 61B3E927
P 6450 3150
AR Path="/5EFE605D/61B3E927" Ref="#PWR?"  Part="1" 
AR Path="/5EFE7331/61B3E927" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E1C2/61B3E927" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/61B3E927" Ref="#PWR?"  Part="1" 
AR Path="/5FE92F3B/61B3E927" Ref="#PWR?"  Part="1" 
AR Path="/61A61EC8/61B3E927" Ref="#PWR0506"  Part="1" 
F 0 "#PWR0506" H 6450 3400 50  0001 C CNN
F 1 "MOT_5V0" H 6451 3323 50  0000 C CNN
F 2 "" H 6450 3150 50  0001 C CNN
F 3 "" H 6450 3150 50  0001 C CNN
	1    6450 3150
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_5V0 #PWR?
U 1 1 61B41248
P 5675 3150
AR Path="/5EFE605D/61B41248" Ref="#PWR?"  Part="1" 
AR Path="/5EFE7331/61B41248" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E1C2/61B41248" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/61B41248" Ref="#PWR?"  Part="1" 
AR Path="/5FE92F3B/61B41248" Ref="#PWR?"  Part="1" 
AR Path="/61A61EC8/61B41248" Ref="#PWR0501"  Part="1" 
F 0 "#PWR0501" H 5675 3400 50  0001 C CNN
F 1 "MOT_5V0" H 5676 3323 50  0000 C CNN
F 2 "" H 5675 3150 50  0001 C CNN
F 3 "" H 5675 3150 50  0001 C CNN
	1    5675 3150
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_5V0 #PWR?
U 1 1 61B43BB4
P 4350 3150
AR Path="/5EFE605D/61B43BB4" Ref="#PWR?"  Part="1" 
AR Path="/5EFE7331/61B43BB4" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E1C2/61B43BB4" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/61B43BB4" Ref="#PWR?"  Part="1" 
AR Path="/5FE92F3B/61B43BB4" Ref="#PWR?"  Part="1" 
AR Path="/61A61EC8/61B43BB4" Ref="#PWR0494"  Part="1" 
F 0 "#PWR0494" H 4350 3400 50  0001 C CNN
F 1 "MOT_5V0" H 4351 3323 50  0000 C CNN
F 2 "" H 4350 3150 50  0001 C CNN
F 3 "" H 4350 3150 50  0001 C CNN
	1    4350 3150
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_5V0 #PWR?
U 1 1 61B46412
P 3575 3150
AR Path="/5EFE605D/61B46412" Ref="#PWR?"  Part="1" 
AR Path="/5EFE7331/61B46412" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E1C2/61B46412" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/61B46412" Ref="#PWR?"  Part="1" 
AR Path="/5FE92F3B/61B46412" Ref="#PWR?"  Part="1" 
AR Path="/61A61EC8/61B46412" Ref="#PWR0488"  Part="1" 
F 0 "#PWR0488" H 3575 3400 50  0001 C CNN
F 1 "MOT_5V0" H 3576 3323 50  0000 C CNN
F 2 "" H 3575 3150 50  0001 C CNN
F 3 "" H 3575 3150 50  0001 C CNN
	1    3575 3150
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_5V0 #PWR?
U 1 1 61B48CAD
P 2250 3150
AR Path="/5EFE605D/61B48CAD" Ref="#PWR?"  Part="1" 
AR Path="/5EFE7331/61B48CAD" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E1C2/61B48CAD" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/61B48CAD" Ref="#PWR?"  Part="1" 
AR Path="/5FE92F3B/61B48CAD" Ref="#PWR?"  Part="1" 
AR Path="/61A61EC8/61B48CAD" Ref="#PWR0481"  Part="1" 
F 0 "#PWR0481" H 2250 3400 50  0001 C CNN
F 1 "MOT_5V0" H 2251 3323 50  0000 C CNN
F 2 "" H 2250 3150 50  0001 C CNN
F 3 "" H 2250 3150 50  0001 C CNN
	1    2250 3150
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_5V0 #PWR?
U 1 1 61B4B4AE
P 1475 3150
AR Path="/5EFE605D/61B4B4AE" Ref="#PWR?"  Part="1" 
AR Path="/5EFE7331/61B4B4AE" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E1C2/61B4B4AE" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/61B4B4AE" Ref="#PWR?"  Part="1" 
AR Path="/5FE92F3B/61B4B4AE" Ref="#PWR?"  Part="1" 
AR Path="/61A61EC8/61B4B4AE" Ref="#PWR0474"  Part="1" 
F 0 "#PWR0474" H 1475 3400 50  0001 C CNN
F 1 "MOT_5V0" H 1476 3323 50  0000 C CNN
F 2 "" H 1475 3150 50  0001 C CNN
F 3 "" H 1475 3150 50  0001 C CNN
	1    1475 3150
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 61B535A7
P 4350 3300
AR Path="/5F05E355/61B535A7" Ref="R?"  Part="1" 
AR Path="/6068445B/61B535A7" Ref="R?"  Part="1" 
AR Path="/60E816A3/61B535A7" Ref="R?"  Part="1" 
AR Path="/60EB5E32/61B535A7" Ref="R?"  Part="1" 
AR Path="/5EA5DFF7/61B535A7" Ref="R?"  Part="1" 
AR Path="/5ED47D19/61B535A7" Ref="R?"  Part="1" 
AR Path="/5FE92F3B/61B535A7" Ref="R?"  Part="1" 
AR Path="/61A61EC8/61B535A7" Ref="R229"  Part="1" 
F 0 "R229" H 4500 3350 50  0000 C CNN
F 1 "220" H 4475 3275 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 4350 3300 50  0001 C CNN
F 3 "~" H 4350 3300 50  0001 C CNN
F 4 "MCWR04X2200FTL -  SMD Chip Resistor, 0402 [1005 Metric], 220 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 4350 2900 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x2200ftl/res-220r-1-0-0625w-thick-film/dp/2447136" H 4350 2800 50  0001 C CNN "Distributor Link"
	1    4350 3300
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 61B586CB
P 6450 3300
AR Path="/5F05E355/61B586CB" Ref="R?"  Part="1" 
AR Path="/6068445B/61B586CB" Ref="R?"  Part="1" 
AR Path="/60E816A3/61B586CB" Ref="R?"  Part="1" 
AR Path="/60EB5E32/61B586CB" Ref="R?"  Part="1" 
AR Path="/5EA5DFF7/61B586CB" Ref="R?"  Part="1" 
AR Path="/5ED47D19/61B586CB" Ref="R?"  Part="1" 
AR Path="/5FE92F3B/61B586CB" Ref="R?"  Part="1" 
AR Path="/61A61EC8/61B586CB" Ref="R235"  Part="1" 
F 0 "R235" H 6600 3350 50  0000 C CNN
F 1 "220" H 6575 3275 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 6450 3300 50  0001 C CNN
F 3 "~" H 6450 3300 50  0001 C CNN
F 4 "MCWR04X2200FTL -  SMD Chip Resistor, 0402 [1005 Metric], 220 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 6450 2900 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x2200ftl/res-220r-1-0-0625w-thick-film/dp/2447136" H 6450 2800 50  0001 C CNN "Distributor Link"
	1    6450 3300
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 61B5FBFD
P 850 3600
AR Path="/5F05E355/61B5FBFD" Ref="R?"  Part="1" 
AR Path="/6068445B/61B5FBFD" Ref="R?"  Part="1" 
AR Path="/60E816A3/61B5FBFD" Ref="R?"  Part="1" 
AR Path="/60EB5E32/61B5FBFD" Ref="R?"  Part="1" 
AR Path="/5EA5DFF7/61B5FBFD" Ref="R?"  Part="1" 
AR Path="/5ED47D19/61B5FBFD" Ref="R?"  Part="1" 
AR Path="/61A61EC8/61B5FBFD" Ref="R217"  Part="1" 
F 0 "R217" H 1000 3625 50  0000 C CNN
F 1 "68k1" H 975 3525 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 850 3600 50  0001 C CNN
F 3 "~" H 850 3600 50  0001 C CNN
F 4 "MCMR04X6812FTL -  SMD Chip Resistor, Ceramic, 0402 [1005 Metric], 68.1 kohm, MCMR Series, 50 V, Thick Film, 62.5 mW" H 850 3200 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcmr04x6812ftl/res-68k1-1-0-0625w-0402-ceramic/dp/2073208" H 850 3100 50  0001 C CNN "Distributor Link"
	1    850  3600
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 61B5FC05
P 850 3900
AR Path="/5F05E355/61B5FC05" Ref="R?"  Part="1" 
AR Path="/6068445B/61B5FC05" Ref="R?"  Part="1" 
AR Path="/60E816A3/61B5FC05" Ref="R?"  Part="1" 
AR Path="/60EB5E32/61B5FC05" Ref="R?"  Part="1" 
AR Path="/5EA5DFF7/61B5FC05" Ref="R?"  Part="1" 
AR Path="/5ED47D19/61B5FC05" Ref="R?"  Part="1" 
AR Path="/61A61EC8/61B5FC05" Ref="R218"  Part="1" 
F 0 "R218" H 1000 3925 50  0000 C CNN
F 1 "10k" H 950 3825 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 850 3900 50  0001 C CNN
F 3 "~" H 850 3900 50  0001 C CNN
F 4 "MCWR04X1002FTL -  SMD Chip Resistor, 0402 [1005 Metric], 10 kohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 850 3500 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x1002ftl/res-10k-1-0-0625w-thick-film/dp/2447096" H 850 3400 50  0001 C CNN "Distributor Link"
	1    850  3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	1075 3750 850  3750
Wire Wire Line
	850  3750 850  3800
Wire Wire Line
	850  3750 850  3700
Connection ~ 850  3750
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 61B8A623
P 2950 3900
AR Path="/5F05E355/61B8A623" Ref="R?"  Part="1" 
AR Path="/6068445B/61B8A623" Ref="R?"  Part="1" 
AR Path="/60E816A3/61B8A623" Ref="R?"  Part="1" 
AR Path="/60EB5E32/61B8A623" Ref="R?"  Part="1" 
AR Path="/5EA5DFF7/61B8A623" Ref="R?"  Part="1" 
AR Path="/5ED47D19/61B8A623" Ref="R?"  Part="1" 
AR Path="/61A61EC8/61B8A623" Ref="R226"  Part="1" 
F 0 "R226" H 3100 3925 50  0000 C CNN
F 1 "10k" H 3050 3825 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 2950 3900 50  0001 C CNN
F 3 "~" H 2950 3900 50  0001 C CNN
F 4 "MCWR04X1002FTL -  SMD Chip Resistor, 0402 [1005 Metric], 10 kohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 2950 3500 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x1002ftl/res-10k-1-0-0625w-thick-film/dp/2447096" H 2950 3400 50  0001 C CNN "Distributor Link"
	1    2950 3900
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 61B8FB18
P 5050 3600
AR Path="/5F05E355/61B8FB18" Ref="R?"  Part="1" 
AR Path="/6068445B/61B8FB18" Ref="R?"  Part="1" 
AR Path="/60E816A3/61B8FB18" Ref="R?"  Part="1" 
AR Path="/60EB5E32/61B8FB18" Ref="R?"  Part="1" 
AR Path="/5EA5DFF7/61B8FB18" Ref="R?"  Part="1" 
AR Path="/5ED47D19/61B8FB18" Ref="R?"  Part="1" 
AR Path="/5FE92F3B/61B8FB18" Ref="R?"  Part="1" 
AR Path="/61A61EC8/61B8FB18" Ref="R232"  Part="1" 
F 0 "R232" H 5200 3625 50  0000 C CNN
F 1 "180k" H 5175 3525 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 5050 3600 50  0001 C CNN
F 3 "~" H 5050 3600 50  0001 C CNN
F 4 "WR04X1803FTL -  SMD Chip Resistor, 0402 [1005 Metric], 180 kohm, WR04 Series, 50 V, Thick Film, 62.5 mW" H 5050 3200 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/walsin/wr04x1803ftl/res-180k-1-50v-0402-thick-film/dp/2502519" H 5050 3100 50  0001 C CNN "Distributor Link"
	1    5050 3600
	1    0    0    -1  
$EndComp
Wire Notes Line
	600  600  600  2475
Wire Notes Line
	2625 600  2625 2475
Wire Notes Line
	6825 600  6825 2475
Wire Notes Line
	4800 600  4800 2475
Wire Notes Line
	4725 600  4725 2475
Wire Notes Line
	2700 600  2700 2475
Wire Notes Line
	600  2550 600  4425
Wire Notes Line
	2625 2550 2625 4425
Wire Notes Line
	2700 2550 2700 4425
Wire Notes Line
	4725 2550 4725 4425
Wire Notes Line
	4800 2550 4800 4425
Wire Notes Line
	6825 2550 6825 4425
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 61C0B1B2
P 1575 5750
AR Path="/5F05E355/61C0B1B2" Ref="R?"  Part="1" 
AR Path="/5E9C6910/61C0B1B2" Ref="R?"  Part="1" 
AR Path="/5EFE605D/61C0B1B2" Ref="R?"  Part="1" 
AR Path="/5FD7CF82/61C0B1B2" Ref="R?"  Part="1" 
AR Path="/5EA5E295/61C0B1B2" Ref="R?"  Part="1" 
AR Path="/61A61EC8/61C0B1B2" Ref="R219"  Part="1" 
F 0 "R219" V 1650 5750 50  0000 C CNN
F 1 "22" V 1475 5750 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 1575 5750 50  0001 C CNN
F 3 "~" H 1575 5750 50  0001 C CNN
F 4 "MCWR04X22R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 22 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 1575 5350 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x22r0ftl/res-22r-1-0-0625w-0402-thick-film/dp/2447138" H 1575 5250 50  0001 C CNN "Distributor Link"
	1    1575 5750
	0    -1   -1   0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR?
U 1 1 61C0B1C7
P 2025 6000
AR Path="/5EA41912/61C0B1C7" Ref="#PWR?"  Part="1" 
AR Path="/5FD7CF82/61C0B1C7" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E295/61C0B1C7" Ref="#PWR?"  Part="1" 
AR Path="/61A61EC8/61C0B1C7" Ref="#PWR0479"  Part="1" 
F 0 "#PWR0479" H 2025 5750 50  0001 C CNN
F 1 "MOT_DGND" H 2030 5827 50  0000 C CNN
F 2 "" H 2025 6000 50  0001 C CNN
F 3 "" H 2025 6000 50  0001 C CNN
	1    2025 6000
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:LED_Small D?
U 1 1 61C0EE99
P 2025 5400
AR Path="/5ED47D19/61C0EE99" Ref="D?"  Part="1" 
AR Path="/5FE92F3B/61C0EE99" Ref="D?"  Part="1" 
AR Path="/61A61EC8/61C0EE99" Ref="D37"  Part="1" 
F 0 "D37" V 2100 5350 50  0000 R CNN
F 1 "KPT-2012SGC" V 2025 5350 25  0000 R CNN
F 2 "PMSM_Driver_LV_Board:LED_0805_2012Metric" V 2025 5400 50  0001 C CNN
F 3 "~" V 2025 5400 50  0001 C CNN
F 4 "KPT-2012SGC -  LED, Low Power, Green, SMD, 0805, 20 mA, 2.2 V, 568 nm" H 2025 5200 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/kingbright/kpt-2012sgc/led-0805-green-12mcd-568nm/dp/2099239" H 2025 5100 50  0001 C CNN "Distributor Link"
F 6 "2.2V" V 1950 5350 50  0000 R CNN "Voltage"
	1    2025 5400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2025 5300 2025 5250
Wire Wire Line
	2025 5050 2025 5000
$Comp
L PMSM_Driver_LV_Board_Power:MOT_5V0 #PWR?
U 1 1 61C0EEA1
P 2025 5000
AR Path="/5EFE605D/61C0EEA1" Ref="#PWR?"  Part="1" 
AR Path="/5EFE7331/61C0EEA1" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E1C2/61C0EEA1" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/61C0EEA1" Ref="#PWR?"  Part="1" 
AR Path="/5FE92F3B/61C0EEA1" Ref="#PWR?"  Part="1" 
AR Path="/61A61EC8/61C0EEA1" Ref="#PWR0478"  Part="1" 
F 0 "#PWR0478" H 2025 5250 50  0001 C CNN
F 1 "MOT_5V0" H 2026 5173 50  0000 C CNN
F 2 "" H 2025 5000 50  0001 C CNN
F 3 "" H 2025 5000 50  0001 C CNN
	1    2025 5000
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 61C0EEA9
P 2025 5150
AR Path="/5F05E355/61C0EEA9" Ref="R?"  Part="1" 
AR Path="/6068445B/61C0EEA9" Ref="R?"  Part="1" 
AR Path="/60E816A3/61C0EEA9" Ref="R?"  Part="1" 
AR Path="/60EB5E32/61C0EEA9" Ref="R?"  Part="1" 
AR Path="/5EA5DFF7/61C0EEA9" Ref="R?"  Part="1" 
AR Path="/5ED47D19/61C0EEA9" Ref="R?"  Part="1" 
AR Path="/5FE92F3B/61C0EEA9" Ref="R?"  Part="1" 
AR Path="/61A61EC8/61C0EEA9" Ref="R220"  Part="1" 
F 0 "R220" H 2175 5200 50  0000 C CNN
F 1 "220" H 2150 5125 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 2025 5150 50  0001 C CNN
F 3 "~" H 2025 5150 50  0001 C CNN
F 4 "MCWR04X2200FTL -  SMD Chip Resistor, 0402 [1005 Metric], 220 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 2025 4750 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x2200ftl/res-220r-1-0-0625w-thick-film/dp/2447136" H 2025 4650 50  0001 C CNN "Distributor Link"
	1    2025 5150
	1    0    0    -1  
$EndComp
Wire Wire Line
	2025 5500 2025 5550
$Comp
L PMSM_Driver_LV_Board_Device:Q_NMOS_GSD Q?
U 1 1 61C0B1AA
P 1925 5750
AR Path="/5EA5E295/61C0B1AA" Ref="Q?"  Part="1" 
AR Path="/61A61EC8/61C0B1AA" Ref="Q17"  Part="1" 
F 0 "Q17" H 2130 5796 50  0000 L CNN
F 1 "BSS138TA" H 2130 5705 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:SOT-23" H 2125 5850 50  0001 C CNN
F 3 "~" H 1925 5750 50  0001 C CNN
F 4 "BSS138TA -  Power MOSFET, N Channel, 50 V, 200 mA, 3.5 ohm, SOT-23, Surface Mount" H 1925 5250 31  0001 C CNN "Description"
F 5 "https://uk.farnell.com/diodes-inc/bss138ta/mosfet-n-50v-sot-23/dp/1471176" H 1925 5150 31  0001 C CNN "Distributor Link"
	1    1925 5750
	1    0    0    -1  
$EndComp
Wire Wire Line
	2025 6000 2025 5950
Wire Wire Line
	1675 5750 1725 5750
Text GLabel 1425 5750 0    50   Input ~ 0
ISO_NOT_OVR_CUR
Wire Wire Line
	1425 5750 1475 5750
Text Notes 675  4700 0    100  ~ 0
Overcurrent Indication
Wire Notes Line
	600  6325 2625 6325
Wire Notes Line
	2625 6325 2625 4500
Wire Notes Line
	600  4500 2625 4500
Wire Notes Line
	600  4500 600  6325
Text Notes 2775 4700 0    100  ~ 0
Motor Enabled Indication
Wire Notes Line
	2700 4500 4725 4500
Wire Notes Line
	2700 4500 2700 6325
Wire Notes Line
	2700 6325 4725 6325
Wire Notes Line
	4725 6325 4725 4500
Text Notes 700  4775 0    25   ~ 0
LED is on when overcurrent not detected (current OK LED).
Text Notes 2800 4775 0    25   ~ 0
LED is on when motor is enabled.
Text GLabel 3525 5750 0    50   Input ~ 0
ISO_MOT_DISABLE
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 61C3CCAD
P 4100 5150
AR Path="/5F05E355/61C3CCAD" Ref="R?"  Part="1" 
AR Path="/6068445B/61C3CCAD" Ref="R?"  Part="1" 
AR Path="/60E816A3/61C3CCAD" Ref="R?"  Part="1" 
AR Path="/60EB5E32/61C3CCAD" Ref="R?"  Part="1" 
AR Path="/5EA5DFF7/61C3CCAD" Ref="R?"  Part="1" 
AR Path="/5ED47D19/61C3CCAD" Ref="R?"  Part="1" 
AR Path="/5FE92F3B/61C3CCAD" Ref="R?"  Part="1" 
AR Path="/61A61EC8/61C3CCAD" Ref="R227"  Part="1" 
F 0 "R227" H 4250 5200 50  0000 C CNN
F 1 "220" H 4225 5125 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 4100 5150 50  0001 C CNN
F 3 "~" H 4100 5150 50  0001 C CNN
F 4 "MCWR04X2200FTL -  SMD Chip Resistor, 0402 [1005 Metric], 220 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 4100 4750 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x2200ftl/res-220r-1-0-0625w-thick-film/dp/2447136" H 4100 4650 50  0001 C CNN "Distributor Link"
	1    4100 5150
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_5V0 #PWR?
U 1 1 61C3CCA5
P 4100 5000
AR Path="/5EFE605D/61C3CCA5" Ref="#PWR?"  Part="1" 
AR Path="/5EFE7331/61C3CCA5" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E1C2/61C3CCA5" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/61C3CCA5" Ref="#PWR?"  Part="1" 
AR Path="/5FE92F3B/61C3CCA5" Ref="#PWR?"  Part="1" 
AR Path="/61A61EC8/61C3CCA5" Ref="#PWR0492"  Part="1" 
F 0 "#PWR0492" H 4100 5250 50  0001 C CNN
F 1 "MOT_5V0" H 4101 5173 50  0000 C CNN
F 2 "" H 4100 5000 50  0001 C CNN
F 3 "" H 4100 5000 50  0001 C CNN
	1    4100 5000
	1    0    0    -1  
$EndComp
Wire Wire Line
	4100 5050 4100 5000
Wire Wire Line
	4100 5300 4100 5250
$Comp
L PMSM_Driver_LV_Board_Device:LED_Small D?
U 1 1 61C3CC9D
P 4100 5400
AR Path="/5ED47D19/61C3CC9D" Ref="D?"  Part="1" 
AR Path="/5FE92F3B/61C3CC9D" Ref="D?"  Part="1" 
AR Path="/61A61EC8/61C3CC9D" Ref="D40"  Part="1" 
F 0 "D40" V 4175 5350 50  0000 R CNN
F 1 "KPT-2012SGC" V 4100 5350 25  0000 R CNN
F 2 "PMSM_Driver_LV_Board:LED_0805_2012Metric" V 4100 5400 50  0001 C CNN
F 3 "~" V 4100 5400 50  0001 C CNN
F 4 "KPT-2012SGC -  LED, Low Power, Green, SMD, 0805, 20 mA, 2.2 V, 568 nm" H 4100 5200 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/kingbright/kpt-2012sgc/led-0805-green-12mcd-568nm/dp/2099239" H 4100 5100 50  0001 C CNN "Distributor Link"
F 6 "2.2V" V 4025 5350 50  0000 R CNN "Voltage"
	1    4100 5400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4100 5500 4100 5750
Wire Wire Line
	3525 5750 4100 5750
$EndSCHEMATC
