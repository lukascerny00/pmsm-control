EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 14 16
Title "PMSM_Driver_LV_Board"
Date "2020-03-30"
Rev "A"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L PMSM_Driver_LV_Board_Power:ISO_RDC_5V0 #PWR?
U 1 1 5F43853A
P 10125 3950
AR Path="/5EA5DFF7/5F43853A" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/5F43853A" Ref="#PWR0429"  Part="1" 
F 0 "#PWR0429" H 10125 4200 50  0001 C CNN
F 1 "ISO_RDC_5V0" H 10126 4123 50  0000 C CNN
F 2 "" H 10125 3950 50  0001 C CNN
F 3 "" H 10125 3950 50  0001 C CNN
	1    10125 3950
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_IC:ADP1614ACPZ-1.3 U52
U 1 1 5FED72F4
P 2700 2050
F 0 "U52" H 2450 2450 50  0000 C CNN
F 1 "ADP1614ACPZ-1.3" H 3200 1600 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:LFCSP-WD-10-1EP_3x3mm_P0.5mm_EP1.64x2.38mm" H 2700 1100 50  0001 C CNN
F 3 "~" H 2700 2050 50  0001 C CNN
F 4 "ADP1614ACPZ-1.3-R7 -  DC-DC Switching Boost (Step Up) Regulator, Adjustable, 2.5V-5.5Vin, 3.6V-20Vout, 4Aout, LFCSP-10" H 2700 1000 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/analog-devices/adp1614acpz-1-3-r7/boost-5-5vin-650k-1-3mhz-4a-10lfcsp/dp/2191713" H 2700 900 50  0001 C CNN "Distributor Link"
F 6 "https://www.mouser.co.uk/ProductDetail/Analog-Devices/ADP1614ACPZ-13-R7?qs=%2Fha2pyFadugAR4r7XJ4GsFu0fy3ajS6raELtJv%252BJO2OY1ArYKxsARg%3D%3D" H 2700 800 50  0001 C CNN "Distributor Link 2"
	1    2700 2050
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_DGND #PWR?
U 1 1 5FED8FD8
P 2600 2900
AR Path="/5EA5D9E8/5FED8FD8" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/5FED8FD8" Ref="#PWR0401"  Part="1" 
F 0 "#PWR0401" H 2600 2650 50  0001 C CNN
F 1 "CTRL_DGND" H 2605 2727 50  0000 C CNN
F 2 "" H 2600 2900 50  0001 C CNN
F 3 "" H 2600 2900 50  0001 C CNN
	1    2600 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	2700 2550 2700 2600
Wire Wire Line
	2700 2600 2600 2600
Connection ~ 2600 2600
Wire Wire Line
	2600 2600 2600 2550
Wire Wire Line
	2800 2550 2800 2600
Wire Wire Line
	2800 2600 2700 2600
Connection ~ 2700 2600
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C201
U 1 1 5FED9A99
P 1925 1350
F 0 "C201" H 2017 1441 50  0000 L CNN
F 1 "2.2uF" H 2017 1350 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 1925 1350 50  0001 C CNN
F 3 "~" H 1925 1350 50  0001 C CNN
F 4 "GRM188R61E225KA12D -  SMD Multilayer Ceramic Capacitor, 2.2 µF, 25 V, 0603 [1608 Metric], ± 10%, X5R, GRM Series" H 1925 950 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/murata/grm188r61e225ka12d/cap-2-2-f-25v-10-x5r-0603/dp/1845734" H 1925 850 50  0001 C CNN "Distributor Link"
F 6 "25V" H 2017 1259 50  0000 L CNN "Voltage"
	1    1925 1350
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R199
U 1 1 5FEDAA56
P 2700 1350
F 0 "R199" H 2768 1441 50  0000 L CNN
F 1 "1" H 2775 1375 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 2700 1350 50  0001 C CNN
F 3 "~" H 2700 1350 50  0001 C CNN
F 4 "MCMR04X1R0 JTL -  SMD Chip Resistor, Ceramic, 0402 [1005 Metric], 1 ohm, MCMR Series, 50 V, Thick Film, 62.5 mW" H 2700 950 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcmr04x1r0-jtl/res-1r-5-0-0625w-0402-ceramic/dp/2072741" H 2700 850 50  0001 C CNN "Distributor Link"
F 6 "5%" H 2775 1300 50  0000 L CNN "Tolerance"
	1    2700 1350
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_3V3 #PWR?
U 1 1 5FF2AEF3
P 1875 1150
AR Path="/5E9C6910/5FF2AEF3" Ref="#PWR?"  Part="1" 
AR Path="/5EBC6654/5FF2AEF3" Ref="#PWR?"  Part="1" 
AR Path="/5EA5D9E8/5FF2AEF3" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/5FF2AEF3" Ref="#PWR0397"  Part="1" 
F 0 "#PWR0397" H 1875 1400 50  0001 C CNN
F 1 "CTRL_3V3" H 1876 1323 50  0000 C CNN
F 2 "" H 1875 1150 50  0001 C CNN
F 3 "" H 1875 1150 50  0001 C CNN
	1    1875 1150
	1    0    0    -1  
$EndComp
Wire Wire Line
	2700 1450 2700 1500
Wire Wire Line
	1875 1150 1875 1200
Wire Wire Line
	1925 1200 1925 1250
Wire Wire Line
	2700 1200 2700 1250
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_DGND #PWR?
U 1 1 5FF2EF68
P 1925 1500
AR Path="/5EA5D9E8/5FF2EF68" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/5FF2EF68" Ref="#PWR0398"  Part="1" 
F 0 "#PWR0398" H 1925 1250 50  0001 C CNN
F 1 "CTRL_DGND" H 1930 1327 50  0000 C CNN
F 2 "" H 1925 1500 50  0001 C CNN
F 3 "" H 1925 1500 50  0001 C CNN
	1    1925 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	1925 1500 1925 1450
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C206
U 1 1 5FF2FA5F
P 3100 1500
F 0 "C206" V 3225 1450 50  0000 L CNN
F 1 "1uF" V 3150 1325 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 3100 1500 50  0001 C CNN
F 3 "~" H 3100 1500 50  0001 C CNN
F 4 "LMK107B7105KA-T -  SMD Multilayer Ceramic Capacitor, 1 µF, 10 V, 0603 [1608 Metric], ± 10%, X7R, M Series" H 3100 1100 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/taiyo-yuden/lmk107b7105ka-t/cap-1-f-10v-10-x7r-0603/dp/2112849" H 3100 1000 50  0001 C CNN "Distributor Link"
	1    3100 1500
	0    -1   -1   0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_DGND #PWR?
U 1 1 5FF315F8
P 3250 1550
AR Path="/5EA5D9E8/5FF315F8" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/5FF315F8" Ref="#PWR0403"  Part="1" 
F 0 "#PWR0403" H 3250 1300 50  0001 C CNN
F 1 "CTRL_DGND" H 3255 1377 50  0000 C CNN
F 2 "" H 3250 1550 50  0001 C CNN
F 3 "" H 3250 1550 50  0001 C CNN
	1    3250 1550
	1    0    0    -1  
$EndComp
Wire Wire Line
	3200 1500 3250 1500
Wire Wire Line
	3250 1500 3250 1550
Wire Wire Line
	3000 1500 2700 1500
Connection ~ 2700 1500
Wire Wire Line
	2700 1500 2700 1550
Wire Wire Line
	1875 1200 1925 1200
Wire Wire Line
	1925 1200 2700 1200
Connection ~ 1925 1200
Wire Wire Line
	2250 1550 2250 1900
Wire Wire Line
	2250 1900 2300 1900
Connection ~ 2700 1550
Wire Wire Line
	2700 1550 2700 1600
Wire Wire Line
	3100 1900 3150 1900
Wire Wire Line
	3500 1900 3500 1200
Wire Wire Line
	2700 1200 3000 1200
Connection ~ 2700 1200
Wire Wire Line
	3100 2000 3150 2000
Wire Wire Line
	3150 2000 3150 1900
Connection ~ 3150 1900
Wire Wire Line
	3150 1900 3500 1900
$Comp
L PMSM_Driver_LV_Board_Device:L_Core_Ferrite_Small L1
U 1 1 5FF34FFF
P 3100 1200
F 0 "L1" V 3200 1150 50  0000 L CNN
F 1 "1.5uH" V 3150 875 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:L_ME3220-152MLC" H 3100 1200 50  0001 C CNN
F 3 "~" H 3100 1200 50  0001 C CNN
F 4 "ME3220-152MLC - Power Inductor (SMD), 1.5 µH, 2.2 A, Unshielded, 2.2 A, ME3220 Series, 3.2mm x 2.8mm x 2mm" H 3100 800 50  0001 C CNN "Description"
F 5 "https://www.mouser.co.uk/ProductDetail/Coilcraft/ME3220-152MLC?qs=zCSbvcPd3pb%252BAsFLd0gUow==" H 3100 700 50  0001 C CNN "Distributor Link"
	1    3100 1200
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3200 1200 3500 1200
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_DGND #PWR?
U 1 1 5FF39BF3
P 2100 2900
AR Path="/5EA5D9E8/5FF39BF3" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/5FF39BF3" Ref="#PWR0399"  Part="1" 
F 0 "#PWR0399" H 2100 2650 50  0001 C CNN
F 1 "CTRL_DGND" H 2105 2727 50  0000 C CNN
F 2 "" H 2100 2900 50  0001 C CNN
F 3 "" H 2100 2900 50  0001 C CNN
	1    2100 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	2100 2200 2300 2200
Wire Wire Line
	2100 2200 2100 2400
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C199
U 1 1 5FF3B351
P 1625 2500
F 0 "C199" H 1717 2591 50  0000 L CNN
F 1 "10nF" H 1717 2500 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 1625 2500 50  0001 C CNN
F 3 "~" H 1625 2500 50  0001 C CNN
F 4 "MC0402B103J160CT -  SMD Multilayer Ceramic Capacitor, 10000 pF, 16 V, 0402 [1005 Metric], ± 5%, X7R, MC Series" H 1625 2100 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402b103j160ct/cap-0-01-f-16v-5-x7r-0402/dp/2320775" H 1625 2000 50  0001 C CNN "Distributor Link"
F 6 "5%" H 1717 2409 50  0000 L CNN "Tolerance"
	1    1625 2500
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_DGND #PWR?
U 1 1 5FF3B357
P 1625 2900
AR Path="/5EA5D9E8/5FF3B357" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/5FF3B357" Ref="#PWR0396"  Part="1" 
F 0 "#PWR0396" H 1625 2650 50  0001 C CNN
F 1 "CTRL_DGND" H 1630 2727 50  0000 C CNN
F 2 "" H 1625 2900 50  0001 C CNN
F 3 "" H 1625 2900 50  0001 C CNN
	1    1625 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	1625 2650 1625 2600
Wire Wire Line
	1625 2200 1625 2400
Wire Wire Line
	1625 2200 2100 2200
Connection ~ 2100 2200
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R198
U 1 1 5FF3C2E5
P 1625 2750
F 0 "R198" H 1700 2800 50  0000 L CNN
F 1 "2k74" H 1700 2725 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 1625 2750 50  0001 C CNN
F 3 "~" H 1625 2750 50  0001 C CNN
F 4 "MCMR04X2741FTL -  SMD Chip Resistor, Ceramic, 0402 [1005 Metric], 2.74 kohm, MCMR Series, 50 V, Thick Film, 62.5 mW" H 1625 2350 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcmr04x2741ftl/res-2k74-1-0-0625w-0402-ceramic/dp/2072863" H 1625 2250 50  0001 C CNN "Distributor Link"
F 6 "1%" H 1700 2650 50  0000 L CNN "Tolerance"
	1    1625 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	1625 2900 1625 2850
Wire Wire Line
	2600 2600 2600 2900
Wire Wire Line
	2100 2600 2100 2900
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C196
U 1 1 5FF416D5
P 1150 2500
F 0 "C196" H 1242 2591 50  0000 L CNN
F 1 "10nF" H 1242 2500 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 1150 2500 50  0001 C CNN
F 3 "~" H 1150 2500 50  0001 C CNN
F 4 "MC0402B103J160CT -  SMD Multilayer Ceramic Capacitor, 10000 pF, 16 V, 0402 [1005 Metric], ± 5%, X7R, MC Series" H 1150 2100 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402b103j160ct/cap-0-01-f-16v-5-x7r-0402/dp/2320775" H 1150 2000 50  0001 C CNN "Distributor Link"
F 6 "5%" H 1242 2409 50  0000 L CNN "Tolerance"
	1    1150 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	1150 2100 1150 2400
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_DGND #PWR?
U 1 1 5FF42BC3
P 1150 2900
AR Path="/5EA5D9E8/5FF42BC3" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/5FF42BC3" Ref="#PWR0393"  Part="1" 
F 0 "#PWR0393" H 1150 2650 50  0001 C CNN
F 1 "CTRL_DGND" H 1155 2727 50  0000 C CNN
F 2 "" H 1150 2900 50  0001 C CNN
F 3 "" H 1150 2900 50  0001 C CNN
	1    1150 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	1150 2600 1150 2700
Wire Wire Line
	1150 2100 2300 2100
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R197
U 1 1 5FF44A53
P 775 2500
F 0 "R197" H 850 2550 50  0000 L CNN
F 1 "69k8" H 850 2475 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 775 2500 50  0001 C CNN
F 3 "~" H 775 2500 50  0001 C CNN
F 4 "MCWR04X6982FTL -  SMD Chip Resistor, 0402 [1005 Metric], 69.8 kohm, MCWR04 Series, 50 V, Thick Film, 62.5 mW" H 775 2100 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x6982ftl/res-69k8-1-0-0625w-0402-thick/dp/2694651" H 775 2000 50  0001 C CNN "Distributor Link"
F 6 "1%" H 850 2400 50  0000 L CNN "Tolerance"
	1    775  2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	2300 2000 775  2000
Wire Wire Line
	775  2000 775  2400
Wire Wire Line
	775  2600 775  2700
Wire Wire Line
	775  2700 1150 2700
Connection ~ 1150 2700
Wire Wire Line
	1150 2700 1150 2900
$Comp
L PMSM_Driver_LV_Board_Device:D_Schottky_Small D32
U 1 1 5FF46D9D
P 3700 1200
F 0 "D32" H 3700 1300 50  0000 C CNN
F 1 "B240A-E3/61T" H 3700 1125 25  0000 C CNN
F 2 "PMSM_Driver_LV_Board:D_SMA" V 3700 1200 50  0001 C CNN
F 3 "~" V 3700 1200 50  0001 C CNN
F 4 "B240A-E3/61T -  Schottky Rectifier, 40 V, 2 A, Single, DO-214AC (SMA), 2 Pins, 550 mV" H 3700 1000 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/vishay/b240a-e3-61t/schottky-rect-single-40v-do-214ac/dp/2889114" H 3700 900 50  0001 C CNN "Distributor Link"
	1    3700 1200
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3600 1200 3500 1200
Connection ~ 3500 1200
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C208
U 1 1 5FF49E53
P 3900 1400
F 0 "C208" H 3992 1446 50  0000 L CNN
F 1 "470nF" H 3992 1355 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_1210_3225Metric" H 3900 1400 50  0001 C CNN
F 3 "~" H 3900 1400 50  0001 C CNN
F 4 "1210B474K500CT -  SMD Multilayer Ceramic Capacitor, 0.47 µF, 50 V, 1210 [3225 Metric], ± 10%, X7R" H 3900 1000 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/walsin/1210b474k500ct/cap-0-47-f-50v-10-x7r-1210/dp/2497175" H 3900 900 50  0001 C CNN "Distributor Link"
	1    3900 1400
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_DGND #PWR?
U 1 1 5FF4AAD7
P 3900 1600
AR Path="/5EA5D9E8/5FF4AAD7" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/5FF4AAD7" Ref="#PWR0405"  Part="1" 
F 0 "#PWR0405" H 3900 1350 50  0001 C CNN
F 1 "CTRL_DGND" H 3905 1427 50  0000 C CNN
F 2 "" H 3900 1600 50  0001 C CNN
F 3 "" H 3900 1600 50  0001 C CNN
	1    3900 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	3900 1600 3900 1500
Wire Wire Line
	3900 1300 3900 1200
Wire Wire Line
	3900 1200 3800 1200
$Comp
L PMSM_Driver_LV_Board_Device:L_Core_Ferrite_Small L2
U 1 1 5FF4D7C9
P 4150 1200
F 0 "L2" V 4250 1150 50  0000 L CNN
F 1 "110nH" V 4200 875 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:L_0805LS-111XJLB" H 4150 1200 50  0001 C CNN
F 3 "~" H 4150 1200 50  0001 C CNN
F 4 "0805LS-111XJLB -  High Frequency Inductor, 110 nH, 0805LS Series, 2 A, 0805 [2012 Metric], Wirewound, 0.05 ohm" H 4150 800 50  0001 C CNN "Description"
F 5 "https://www.mouser.co.uk/ProductDetail/Coilcraft/0805LS-111XJLB?qs=sGAEpiMZZMsg%252By3WlYCkU2kWFds1hA9DSxP0mWlLwqk%3D" H 4150 700 50  0001 C CNN "Distributor Link"
	1    4150 1200
	0    -1   -1   0   
$EndComp
Connection ~ 3900 1200
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5FF53A9B
P 4400 1400
AR Path="/5E9C6910/5FF53A9B" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5FF53A9B" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5FF53A9B" Ref="C?"  Part="1" 
AR Path="/5F05E355/5FF53A9B" Ref="C?"  Part="1" 
AR Path="/6068445B/5FF53A9B" Ref="C?"  Part="1" 
AR Path="/60E816A3/5FF53A9B" Ref="C?"  Part="1" 
AR Path="/5EFE605D/5FF53A9B" Ref="C?"  Part="1" 
AR Path="/5EFE7331/5FF53A9B" Ref="C?"  Part="1" 
AR Path="/5FD7CF82/5FF53A9B" Ref="C?"  Part="1" 
AR Path="/5EA5E295/5FF53A9B" Ref="C?"  Part="1" 
AR Path="/5ED47D19/5FF53A9B" Ref="C210"  Part="1" 
F 0 "C210" H 4200 1475 50  0000 L CNN
F 1 "10uF" H 4200 1325 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4400 1400 50  0001 C CNN
F 3 "~" H 4400 1400 50  0001 C CNN
F 4 "LMK107BBJ106MALT -  SMD Multilayer Ceramic Capacitor, 10 µF, 10 V, 0603 [1608 Metric], ± 20%, X5R, M Series" H 4400 1000 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/taiyo-yuden/lmk107bbj106malt/cap-10-f-10v-20-x5r-0603/dp/2113070" H 4400 900 50  0001 C CNN "Distributor Link"
	1    4400 1400
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3900 1200 4050 1200
Wire Wire Line
	4250 1200 4400 1200
Wire Wire Line
	4400 1200 4400 1300
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_DGND #PWR?
U 1 1 5FF5717E
P 4400 1600
AR Path="/5EA5D9E8/5FF5717E" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/5FF5717E" Ref="#PWR0409"  Part="1" 
F 0 "#PWR0409" H 4400 1350 50  0001 C CNN
F 1 "CTRL_DGND" H 4405 1427 50  0000 C CNN
F 2 "" H 4400 1600 50  0001 C CNN
F 3 "" H 4400 1600 50  0001 C CNN
	1    4400 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	4400 1600 4400 1500
$Comp
L PMSM_Driver_LV_Board_Device:Ferrite_Bead_Small FB?
U 1 1 5FF596CD
P 4900 1200
AR Path="/5EA5E155/5FF596CD" Ref="FB?"  Part="1" 
AR Path="/5ED47D19/5FF596CD" Ref="FB5"  Part="1" 
F 0 "FB5" V 5025 1125 50  0000 L CNN
F 1 "Z0805C420APWST" V 4775 1025 25  0000 L CNN
F 2 "PMSM_Driver_LV_Board:L_0805_2012Metric" V 4830 1200 50  0001 C CNN
F 3 "~" H 4900 1200 50  0001 C CNN
F 4 "Z0805C420APWST -  Ferrite Bead, 0805 [2012 Metric], 42 ohm, 4 A, Z-PWS Series, 0.008 ohm, ± 25%" H 4900 1200 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/kemet/z0805c420apwst/ferrite-bead-42-ohm-4a-0805/dp/2846741" H 4900 1200 50  0001 C CNN "Distributor Link"
F 6 "42 Ohms at 100 MHz" V 4725 975 25  0000 L CNN "Impedance"
	1    4900 1200
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4400 1200 4675 1200
Connection ~ 4400 1200
Wire Wire Line
	4675 1200 4675 2100
Connection ~ 4675 1200
Wire Wire Line
	4675 1200 4800 1200
Wire Wire Line
	5000 1200 5100 1200
Wire Wire Line
	5100 1200 5100 1100
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R201
U 1 1 5FF61B7B
P 4375 2100
F 0 "R201" V 4450 2050 50  0000 L CNN
F 1 "95k3" V 4275 2000 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4375 2100 50  0001 C CNN
F 3 "~" H 4375 2100 50  0001 C CNN
F 4 "WR06X9532FTL -  SMD Chip Resistor, 0603 [1608 Metric], 95.3 kohm, WR06 Series, 75 V, Thick Film, 100 mW" H 4375 1700 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/walsin/wr06x9532ftl/res-95k3-1-0-1w-0603-thick-film/dp/2670922" H 4375 1600 50  0001 C CNN "Distributor Link"
F 6 "1%" V 4200 2050 50  0000 L CNN "Tolerance"
	1    4375 2100
	0    -1   -1   0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R200
U 1 1 5FF636FF
P 3975 2300
F 0 "R200" H 4050 2350 50  0000 L CNN
F 1 "31k6" H 4050 2275 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 3975 2300 50  0001 C CNN
F 3 "~" H 3975 2300 50  0001 C CNN
F 4 "MCWR04X3162FTL -  SMD Chip Resistor, 0402 [1005 Metric], 31.6 kohm, MCWR04 Series, 50 V, Thick Film, 62.5 mW" H 3975 1900 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x3162ftl/res-31k6-1-0-0625w-0402-thick/dp/2694591" H 3975 1800 50  0001 C CNN "Distributor Link"
F 6 "1%" H 4050 2200 50  0000 L CNN "Tolerance"
	1    3975 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	4275 2100 3975 2100
Wire Wire Line
	3975 2100 3975 2200
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_DGND #PWR?
U 1 1 5FF66AA8
P 3975 2900
AR Path="/5EA5D9E8/5FF66AA8" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/5FF66AA8" Ref="#PWR0406"  Part="1" 
F 0 "#PWR0406" H 3975 2650 50  0001 C CNN
F 1 "CTRL_DGND" H 3980 2727 50  0000 C CNN
F 2 "" H 3975 2900 50  0001 C CNN
F 3 "" H 3975 2900 50  0001 C CNN
	1    3975 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	3975 2400 3975 2900
Wire Wire Line
	4475 2100 4675 2100
Wire Wire Line
	3975 2100 3100 2100
Connection ~ 3975 2100
$Comp
L power:PWR_FLAG #FLG021
U 1 1 5FF6CB8F
P 5100 1100
F 0 "#FLG021" H 5100 1175 50  0001 C CNN
F 1 "PWR_FLAG" H 5100 1273 50  0000 C CNN
F 2 "" H 5100 1100 50  0001 C CNN
F 3 "~" H 5100 1100 50  0001 C CNN
	1    5100 1100
	1    0    0    -1  
$EndComp
Wire Wire Line
	5525 1100 5525 1200
Connection ~ 5100 1200
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_5V0 #PWR0412
U 1 1 5FF72B26
P 5525 1100
F 0 "#PWR0412" H 5525 1350 50  0001 C CNN
F 1 "CTRL_5V0" H 5526 1273 50  0000 C CNN
F 2 "" H 5525 1100 50  0001 C CNN
F 3 "" H 5525 1100 50  0001 C CNN
	1    5525 1100
	1    0    0    -1  
$EndComp
Wire Wire Line
	5100 1200 5525 1200
Text Notes 675  800  0    100  ~ 0
Main 5V Supply on the Controller Side
Wire Notes Line
	5825 600  5825 3225
Wire Notes Line
	5825 3225 600  3225
Wire Notes Line
	600  600  5825 600 
Wire Notes Line
	600  600  600  3225
Text Notes 5975 825  0    100  ~ 0
Isolated 5V Supply for Sensors
Text Notes 6000 900  0    25   ~ 0
Supplies encoder, hall sensor and logic side of resolver.
Wire Wire Line
	8150 1450 8150 2050
Wire Wire Line
	8150 2100 8150 2050
Wire Wire Line
	8575 2100 8150 2100
Wire Wire Line
	8150 2150 8150 2100
Wire Wire Line
	8775 1350 8775 1250
$Comp
L PMSM_Driver_LV_Board_Power:ISO_DGND #PWR?
U 1 1 5FFE54AA
P 7550 2825
AR Path="/5EA5D9E8/5FFE54AA" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/5FFE54AA" Ref="#PWR0420"  Part="1" 
F 0 "#PWR0420" H 7550 2575 50  0001 C CNN
F 1 "ISO_DGND" H 7555 2652 50  0000 C CNN
F 2 "" H 7550 2825 50  0001 C CNN
F 3 "" H 7550 2825 50  0001 C CNN
	1    7550 2825
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_DGND #PWR?
U 1 1 5FFD05C1
P 6225 2825
AR Path="/5EA5D9E8/5FFD05C1" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/5FFD05C1" Ref="#PWR0416"  Part="1" 
F 0 "#PWR0416" H 6225 2575 50  0001 C CNN
F 1 "CTRL_DGND" H 6230 2652 50  0000 C CNN
F 2 "" H 6225 2825 50  0001 C CNN
F 3 "" H 6225 2825 50  0001 C CNN
	1    6225 2825
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_DGND #PWR?
U 1 1 5FFCBAD5
P 6625 2150
AR Path="/5EA5D9E8/5FFCBAD5" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/5FFCBAD5" Ref="#PWR0418"  Part="1" 
F 0 "#PWR0418" H 6625 1900 50  0001 C CNN
F 1 "CTRL_DGND" H 6630 1977 50  0000 C CNN
F 2 "" H 6625 2150 50  0001 C CNN
F 3 "" H 6625 2150 50  0001 C CNN
	1    6625 2150
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_5V0 #PWR0415
U 1 1 5FFBDC62
P 6225 2375
F 0 "#PWR0415" H 6225 2625 50  0001 C CNN
F 1 "CTRL_5V0" H 6226 2548 50  0000 C CNN
F 2 "" H 6225 2375 50  0001 C CNN
F 3 "" H 6225 2375 50  0001 C CNN
	1    6225 2375
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:TestPoint TP?
U 1 1 5FFA0FAC
P 6225 1950
AR Path="/5EA5E155/5FFA0FAC" Ref="TP?"  Part="1" 
AR Path="/5ED47D19/5FFA0FAC" Ref="TP7"  Part="1" 
F 0 "TP7" H 6175 2150 50  0000 L CNN
F 1 "TestPoint" H 6250 2000 25  0000 L CNN
F 2 "PMSM_Driver_LV_Board:TestPoint_Pad_1.0x1.0mm" H 6425 1950 50  0001 C CNN
F 3 "~" H 6425 1950 50  0001 C CNN
	1    6225 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	6625 2000 6225 2000
Connection ~ 6625 2000
Wire Wire Line
	6625 2050 6625 2000
Wire Wire Line
	6625 2000 6625 1450
Wire Wire Line
	6225 2000 6225 1950
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_5V0 #PWR0417
U 1 1 5FFAA361
P 6525 1250
F 0 "#PWR0417" H 6525 1500 50  0001 C CNN
F 1 "CTRL_5V0" H 6526 1423 50  0000 C CNN
F 2 "" H 6525 1250 50  0001 C CNN
F 3 "" H 6525 1250 50  0001 C CNN
	1    6525 1250
	1    0    0    -1  
$EndComp
Wire Wire Line
	7775 1350 7875 1350
Wire Wire Line
	7875 1350 8250 1350
Connection ~ 7875 1350
Wire Wire Line
	7875 1350 7875 1325
$Comp
L power:PWR_FLAG #FLG?
U 1 1 5FFA0FD1
P 7875 1325
AR Path="/5EA5E06A/5FFA0FD1" Ref="#FLG?"  Part="1" 
AR Path="/5EA5E155/5FFA0FD1" Ref="#FLG?"  Part="1" 
AR Path="/5ED47D19/5FFA0FD1" Ref="#FLG024"  Part="1" 
F 0 "#FLG024" H 7875 1400 50  0001 C CNN
F 1 "PWR_FLAG" H 7875 1479 25  0000 C CNN
F 2 "" H 7875 1325 50  0001 C CNN
F 3 "~" H 7875 1325 50  0001 C CNN
	1    7875 1325
	-1   0    0    -1  
$EndComp
Wire Wire Line
	7775 1650 7775 1950
Connection ~ 7775 1650
Wire Wire Line
	7675 1650 7775 1650
Wire Wire Line
	7775 1950 7775 2475
Connection ~ 7775 1950
Wire Wire Line
	7675 1950 7775 1950
Wire Wire Line
	7675 1450 8150 1450
Wire Wire Line
	7675 2050 8150 2050
Wire Wire Line
	7875 2475 8200 2475
Wire Wire Line
	7775 2475 7875 2475
Connection ~ 7775 2475
Connection ~ 7775 1350
Wire Wire Line
	7775 1350 7775 1650
NoConn ~ 7675 1850
NoConn ~ 7675 1750
NoConn ~ 7675 1550
Wire Wire Line
	8575 2100 8575 2050
$Comp
L PMSM_Driver_LV_Board_Device:TestPoint TP?
U 1 1 5FFA0FB6
P 8575 2050
AR Path="/5EA5E155/5FFA0FB6" Ref="TP?"  Part="1" 
AR Path="/5ED47D19/5FFA0FB6" Ref="TP8"  Part="1" 
F 0 "TP8" H 8525 2250 50  0000 L CNN
F 1 "TestPoint" H 8600 2100 25  0000 L CNN
F 2 "PMSM_Driver_LV_Board:TestPoint_Pad_1.0x1.0mm" H 8775 2050 50  0001 C CNN
F 3 "~" H 8775 2050 50  0001 C CNN
	1    8575 2050
	-1   0    0    -1  
$EndComp
Connection ~ 6225 2475
Wire Wire Line
	6225 2475 6225 2375
Wire Wire Line
	6550 2475 6875 2475
Connection ~ 6875 2775
Connection ~ 6875 2475
Connection ~ 6550 2775
Connection ~ 6550 2475
Wire Wire Line
	6875 2475 7200 2475
Wire Wire Line
	6875 2775 7200 2775
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5FFA0F91
P 7200 2625
AR Path="/5E9C6910/5FFA0F91" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5FFA0F91" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5FFA0F91" Ref="C?"  Part="1" 
AR Path="/5F05E355/5FFA0F91" Ref="C?"  Part="1" 
AR Path="/6068445B/5FFA0F91" Ref="C?"  Part="1" 
AR Path="/60E816A3/5FFA0F91" Ref="C?"  Part="1" 
AR Path="/5EFE605D/5FFA0F91" Ref="C?"  Part="1" 
AR Path="/5EA5E155/5FFA0F91" Ref="C?"  Part="1" 
AR Path="/5ED47D19/5FFA0F91" Ref="C221"  Part="1" 
F 0 "C221" H 7000 2700 50  0000 L CNN
F 1 "10uF" H 7000 2550 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 7200 2625 50  0001 C CNN
F 3 "~" H 7200 2625 50  0001 C CNN
F 4 "LMK107BBJ106MALT -  SMD Multilayer Ceramic Capacitor, 10 µF, 10 V, 0603 [1608 Metric], ± 20%, X5R, M Series" H 7200 2225 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/taiyo-yuden/lmk107bbj106malt/cap-10-f-10v-20-x5r-0603/dp/2113070" H 7200 2125 50  0001 C CNN "Distributor Link"
	1    7200 2625
	-1   0    0    -1  
$EndComp
Wire Wire Line
	7200 2475 7200 2525
Wire Wire Line
	7200 2725 7200 2775
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5FFA0F87
P 6875 2625
AR Path="/5E9C6910/5FFA0F87" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5FFA0F87" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5FFA0F87" Ref="C?"  Part="1" 
AR Path="/5F05E355/5FFA0F87" Ref="C?"  Part="1" 
AR Path="/5EA41912/5FFA0F87" Ref="C?"  Part="1" 
AR Path="/5EFE605D/5FFA0F87" Ref="C?"  Part="1" 
AR Path="/5EA5E155/5FFA0F87" Ref="C?"  Part="1" 
AR Path="/5ED47D19/5FFA0F87" Ref="C220"  Part="1" 
F 0 "C220" H 6675 2700 50  0000 L CNN
F 1 "100nF" H 6625 2550 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 6875 2625 50  0001 C CNN
F 3 "~" H 6875 2625 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 6875 2225 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 6875 2125 50  0001 C CNN "Distributor Link"
	1    6875 2625
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6875 2475 6875 2525
Wire Wire Line
	6875 2725 6875 2775
Wire Wire Line
	6550 2775 6875 2775
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5FFA0F7C
P 6225 2625
AR Path="/5E9C6910/5FFA0F7C" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5FFA0F7C" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5FFA0F7C" Ref="C?"  Part="1" 
AR Path="/5F05E355/5FFA0F7C" Ref="C?"  Part="1" 
AR Path="/5EA41912/5FFA0F7C" Ref="C?"  Part="1" 
AR Path="/5EFE605D/5FFA0F7C" Ref="C?"  Part="1" 
AR Path="/5EA5E155/5FFA0F7C" Ref="C?"  Part="1" 
AR Path="/5ED47D19/5FFA0F7C" Ref="C216"  Part="1" 
F 0 "C216" H 6075 2700 50  0000 L CNN
F 1 "100nF" H 5975 2550 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 6225 2625 50  0001 C CNN
F 3 "~" H 6225 2625 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 6225 2225 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 6225 2125 50  0001 C CNN "Distributor Link"
	1    6225 2625
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6225 2475 6550 2475
Wire Wire Line
	6225 2775 6550 2775
Connection ~ 6225 2775
Wire Wire Line
	6225 2475 6225 2525
Wire Wire Line
	6225 2725 6225 2775
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5FFA0F6F
P 6550 2625
AR Path="/5E9C6910/5FFA0F6F" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5FFA0F6F" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5FFA0F6F" Ref="C?"  Part="1" 
AR Path="/5F05E355/5FFA0F6F" Ref="C?"  Part="1" 
AR Path="/6068445B/5FFA0F6F" Ref="C?"  Part="1" 
AR Path="/60E816A3/5FFA0F6F" Ref="C?"  Part="1" 
AR Path="/5EFE605D/5FFA0F6F" Ref="C?"  Part="1" 
AR Path="/5EA5E155/5FFA0F6F" Ref="C?"  Part="1" 
AR Path="/5ED47D19/5FFA0F6F" Ref="C218"  Part="1" 
F 0 "C218" H 6350 2700 50  0000 L CNN
F 1 "10uF" H 6350 2550 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 6550 2625 50  0001 C CNN
F 3 "~" H 6550 2625 50  0001 C CNN
F 4 "LMK107BBJ106MALT -  SMD Multilayer Ceramic Capacitor, 10 µF, 10 V, 0603 [1608 Metric], ± 20%, X5R, M Series" H 6550 2225 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/taiyo-yuden/lmk107bbj106malt/cap-10-f-10v-20-x5r-0603/dp/2113070" H 6550 2125 50  0001 C CNN "Distributor Link"
	1    6550 2625
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6550 2475 6550 2525
Wire Wire Line
	6550 2725 6550 2775
Wire Wire Line
	6225 2775 6225 2825
Connection ~ 8200 2775
Connection ~ 8200 2475
Connection ~ 7875 2775
Connection ~ 7875 2475
Wire Wire Line
	7675 1350 7775 1350
Wire Wire Line
	8200 2475 8525 2475
Wire Wire Line
	8200 2775 8525 2775
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5FFA0F5D
P 8525 2625
AR Path="/5E9C6910/5FFA0F5D" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5FFA0F5D" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5FFA0F5D" Ref="C?"  Part="1" 
AR Path="/5F05E355/5FFA0F5D" Ref="C?"  Part="1" 
AR Path="/6068445B/5FFA0F5D" Ref="C?"  Part="1" 
AR Path="/60E816A3/5FFA0F5D" Ref="C?"  Part="1" 
AR Path="/5EFE605D/5FFA0F5D" Ref="C?"  Part="1" 
AR Path="/5EA5E155/5FFA0F5D" Ref="C?"  Part="1" 
AR Path="/5ED47D19/5FFA0F5D" Ref="C228"  Part="1" 
F 0 "C228" H 8325 2700 50  0000 L CNN
F 1 "10uF" H 8325 2550 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 8525 2625 50  0001 C CNN
F 3 "~" H 8525 2625 50  0001 C CNN
F 4 "LMK107BBJ106MALT -  SMD Multilayer Ceramic Capacitor, 10 µF, 10 V, 0603 [1608 Metric], ± 20%, X5R, M Series" H 8525 2225 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/taiyo-yuden/lmk107bbj106malt/cap-10-f-10v-20-x5r-0603/dp/2113070" H 8525 2125 50  0001 C CNN "Distributor Link"
	1    8525 2625
	-1   0    0    -1  
$EndComp
Wire Wire Line
	8525 2475 8525 2525
Wire Wire Line
	8525 2725 8525 2775
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5FFA0F53
P 8200 2625
AR Path="/5E9C6910/5FFA0F53" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5FFA0F53" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5FFA0F53" Ref="C?"  Part="1" 
AR Path="/5F05E355/5FFA0F53" Ref="C?"  Part="1" 
AR Path="/5EA41912/5FFA0F53" Ref="C?"  Part="1" 
AR Path="/5EFE605D/5FFA0F53" Ref="C?"  Part="1" 
AR Path="/5EA5E155/5FFA0F53" Ref="C?"  Part="1" 
AR Path="/5ED47D19/5FFA0F53" Ref="C226"  Part="1" 
F 0 "C226" H 8000 2700 50  0000 L CNN
F 1 "100nF" H 7950 2550 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 8200 2625 50  0001 C CNN
F 3 "~" H 8200 2625 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 8200 2225 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 8200 2125 50  0001 C CNN "Distributor Link"
	1    8200 2625
	-1   0    0    -1  
$EndComp
Wire Wire Line
	8200 2475 8200 2525
Wire Wire Line
	8200 2725 8200 2775
Wire Wire Line
	7875 2775 8200 2775
Wire Wire Line
	8775 1350 8775 1450
$Comp
L PMSM_Driver_LV_Board_Device:Ferrite_Bead_Small FB?
U 1 1 5FFA0F40
P 8350 1350
AR Path="/5EA5E155/5FFA0F40" Ref="FB?"  Part="1" 
AR Path="/5ED47D19/5FFA0F40" Ref="FB7"  Part="1" 
F 0 "FB7" V 8475 1275 50  0000 L CNN
F 1 "Z0805C420APWST" V 8225 1175 25  0000 L CNN
F 2 "PMSM_Driver_LV_Board:L_0805_2012Metric" V 8280 1350 50  0001 C CNN
F 3 "~" H 8350 1350 50  0001 C CNN
F 4 "Z0805C420APWST -  Ferrite Bead, 0805 [2012 Metric], 42 ohm, 4 A, Z-PWS Series, 0.008 ohm, ± 25%" H 8350 1350 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/kemet/z0805c420apwst/ferrite-bead-42-ohm-4a-0805/dp/2846741" H 8350 1350 50  0001 C CNN "Distributor Link"
F 6 "42 Ohms at 100 MHz" V 8175 1125 25  0000 L CNN "Impedance"
	1    8350 1350
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6525 1850 6525 1350
Connection ~ 6525 1850
Wire Wire Line
	6725 1850 6525 1850
Connection ~ 6525 1350
Wire Wire Line
	6525 1950 6525 1850
Wire Wire Line
	6725 1950 6525 1950
Wire Wire Line
	6525 1350 6725 1350
Wire Wire Line
	6525 1250 6525 1350
Connection ~ 6625 2050
Wire Wire Line
	6625 1450 6725 1450
Connection ~ 8150 2050
Wire Wire Line
	6625 2050 6725 2050
Wire Wire Line
	6625 2150 6625 2050
NoConn ~ 6725 1750
NoConn ~ 6725 1650
NoConn ~ 6725 1550
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5FFA0F13
P 7550 2625
AR Path="/5E9C6910/5FFA0F13" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5FFA0F13" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5FFA0F13" Ref="C?"  Part="1" 
AR Path="/5F05E355/5FFA0F13" Ref="C?"  Part="1" 
AR Path="/5EA41912/5FFA0F13" Ref="C?"  Part="1" 
AR Path="/5EFE605D/5FFA0F13" Ref="C?"  Part="1" 
AR Path="/5EA5E155/5FFA0F13" Ref="C?"  Part="1" 
AR Path="/5ED47D19/5FFA0F13" Ref="C222"  Part="1" 
F 0 "C222" H 7350 2700 50  0000 L CNN
F 1 "100nF" H 7300 2550 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 7550 2625 50  0001 C CNN
F 3 "~" H 7550 2625 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 7550 2225 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 7550 2125 50  0001 C CNN "Distributor Link"
	1    7550 2625
	-1   0    0    -1  
$EndComp
Wire Wire Line
	7550 2475 7775 2475
Wire Wire Line
	7550 2775 7875 2775
Connection ~ 7550 2775
Wire Wire Line
	7550 2475 7550 2525
Wire Wire Line
	7550 2725 7550 2775
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5FFA0F00
P 7875 2625
AR Path="/5E9C6910/5FFA0F00" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5FFA0F00" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5FFA0F00" Ref="C?"  Part="1" 
AR Path="/5F05E355/5FFA0F00" Ref="C?"  Part="1" 
AR Path="/6068445B/5FFA0F00" Ref="C?"  Part="1" 
AR Path="/60E816A3/5FFA0F00" Ref="C?"  Part="1" 
AR Path="/5EFE605D/5FFA0F00" Ref="C?"  Part="1" 
AR Path="/5EA5E155/5FFA0F00" Ref="C?"  Part="1" 
AR Path="/5ED47D19/5FFA0F00" Ref="C224"  Part="1" 
F 0 "C224" H 7675 2700 50  0000 L CNN
F 1 "10uF" H 7675 2550 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 7875 2625 50  0001 C CNN
F 3 "~" H 7875 2625 50  0001 C CNN
F 4 "LMK107BBJ106MALT -  SMD Multilayer Ceramic Capacitor, 10 µF, 10 V, 0603 [1608 Metric], ± 20%, X5R, M Series" H 7875 2225 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/taiyo-yuden/lmk107bbj106malt/cap-10-f-10v-20-x5r-0603/dp/2113070" H 7875 2125 50  0001 C CNN "Distributor Link"
	1    7875 2625
	-1   0    0    -1  
$EndComp
Wire Wire Line
	7875 2475 7875 2525
Wire Wire Line
	7875 2725 7875 2775
Wire Wire Line
	7550 2775 7550 2825
Text Notes 6925 2275 0    25   ~ 0
Isolated floating power supply
$Comp
L PMSM_Driver_LV_Board_IC:ADUM5000ARWZ U?
U 1 1 5FFA0EF4
P 7225 1700
AR Path="/5EA5E155/5FFA0EF4" Ref="U?"  Part="1" 
AR Path="/5ED47D19/5FFA0EF4" Ref="U54"  Part="1" 
F 0 "U54" H 6925 2200 50  0000 C CNN
F 1 "ADUM5000ARWZ" H 7225 1200 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:SOIC-16W_7.5x10.3mm_P1.27mm" H 6325 2250 50  0001 C CNN
F 3 "~" H 6325 2250 50  0001 C CNN
F 4 "ADUM5000ARWZ -  DC-DC Switching Regulator, Adjustable, 2.7V to 5.5V in, 3.3V to 5.5V/100 mA out, 180 MHz, WSOIC-16" H 7225 1100 31  0001 C CNN "Description"
F 5 "https://uk.farnell.com/analog-devices/adum5000arwz/ic-isolated-dc-dc-conv-180mhz/dp/2102522" H 7225 1050 31  0001 C CNN "Distributor Link"
F 6 "https://www.mouser.co.uk/ProductDetail/Analog-Devices/ADUM5000ARWZ?qs=sGAEpiMZZMtitjHzVIkrqYThiPHWFX9aYDreduI%2FIuI%3D" H 7225 1000 31  0001 C CNN "Distributor Link 2"
	1    7225 1700
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG026
U 1 1 5F4324AF
P 8775 1450
F 0 "#FLG026" H 8775 1525 50  0001 C CNN
F 1 "PWR_FLAG" H 8775 1623 50  0000 C CNN
F 2 "" H 8775 1450 50  0001 C CNN
F 3 "~" H 8775 1450 50  0001 C CNN
	1    8775 1450
	-1   0    0    1   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:ISO_DGND #PWR?
U 1 1 5F295A87
P 8150 2150
AR Path="/5EA5D9E8/5F295A87" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/5F295A87" Ref="#PWR0425"  Part="1" 
F 0 "#PWR0425" H 8150 1900 50  0001 C CNN
F 1 "ISO_DGND" H 8155 1977 50  0000 C CNN
F 2 "" H 8150 2150 50  0001 C CNN
F 3 "" H 8150 2150 50  0001 C CNN
	1    8150 2150
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:ISO_SENS_5V0 #PWR?
U 1 1 5F295A81
P 8775 1250
AR Path="/5EA5D9E8/5F295A81" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/5F295A81" Ref="#PWR0426"  Part="1" 
F 0 "#PWR0426" H 8775 1500 50  0001 C CNN
F 1 "ISO_SENS_5V0" H 8776 1423 50  0000 C CNN
F 2 "" H 8775 1250 50  0001 C CNN
F 3 "" H 8775 1250 50  0001 C CNN
	1    8775 1250
	1    0    0    -1  
$EndComp
Wire Notes Line
	5900 600  5900 3225
Wire Notes Line
	9150 600  9150 3225
Wire Wire Line
	8450 1350 8775 1350
Connection ~ 8775 1350
Wire Notes Line
	5900 600  9150 600 
Wire Notes Line
	5900 3225 9150 3225
Text Notes 675  3525 0    100  ~ 0
Isolated 5V Supply for Digital Isolators
Wire Wire Line
	2850 4150 2850 4750
Wire Wire Line
	3475 4050 3475 3950
$Comp
L PMSM_Driver_LV_Board_Power:ISO_DGND #PWR?
U 1 1 60352CB2
P 2250 5525
AR Path="/5EA5D9E8/60352CB2" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/60352CB2" Ref="#PWR0400"  Part="1" 
F 0 "#PWR0400" H 2250 5275 50  0001 C CNN
F 1 "ISO_DGND" H 2255 5352 50  0000 C CNN
F 2 "" H 2250 5525 50  0001 C CNN
F 3 "" H 2250 5525 50  0001 C CNN
	1    2250 5525
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_DGND #PWR?
U 1 1 60352CBE
P 1325 4850
AR Path="/5EA5D9E8/60352CBE" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/60352CBE" Ref="#PWR0395"  Part="1" 
F 0 "#PWR0395" H 1325 4600 50  0001 C CNN
F 1 "CTRL_DGND" H 1330 4677 50  0000 C CNN
F 2 "" H 1325 4850 50  0001 C CNN
F 3 "" H 1325 4850 50  0001 C CNN
	1    1325 4850
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_5V0 #PWR0391
U 1 1 60352CC4
P 925 5075
F 0 "#PWR0391" H 925 5325 50  0001 C CNN
F 1 "CTRL_5V0" H 926 5248 50  0000 C CNN
F 2 "" H 925 5075 50  0001 C CNN
F 3 "" H 925 5075 50  0001 C CNN
	1    925  5075
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_5V0 #PWR0394
U 1 1 60352CD5
P 1225 3950
F 0 "#PWR0394" H 1225 4200 50  0001 C CNN
F 1 "CTRL_5V0" H 1226 4123 50  0000 C CNN
F 2 "" H 1225 3950 50  0001 C CNN
F 3 "" H 1225 3950 50  0001 C CNN
	1    1225 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	2475 4050 2575 4050
Wire Wire Line
	2575 4050 2950 4050
Connection ~ 2575 4050
Wire Wire Line
	2575 4050 2575 4025
$Comp
L power:PWR_FLAG #FLG?
U 1 1 60352CDF
P 2575 4025
AR Path="/5EA5E06A/60352CDF" Ref="#FLG?"  Part="1" 
AR Path="/5EA5E155/60352CDF" Ref="#FLG?"  Part="1" 
AR Path="/5ED47D19/60352CDF" Ref="#FLG019"  Part="1" 
F 0 "#FLG019" H 2575 4100 50  0001 C CNN
F 1 "PWR_FLAG" H 2575 4179 25  0000 C CNN
F 2 "" H 2575 4025 50  0001 C CNN
F 3 "~" H 2575 4025 50  0001 C CNN
	1    2575 4025
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2475 4350 2475 4650
Connection ~ 2475 4350
Wire Wire Line
	2375 4350 2475 4350
Wire Wire Line
	2475 4650 2475 5175
Connection ~ 2475 4650
Wire Wire Line
	2375 4650 2475 4650
Wire Wire Line
	2375 4150 2850 4150
Wire Wire Line
	2375 4750 2850 4750
Wire Wire Line
	2575 5175 2900 5175
Wire Wire Line
	2475 5175 2575 5175
Connection ~ 2475 5175
Connection ~ 2475 4050
Wire Wire Line
	2475 4050 2475 4350
NoConn ~ 2375 4550
NoConn ~ 2375 4450
NoConn ~ 2375 4250
Connection ~ 925  5175
Wire Wire Line
	925  5175 925  5075
Wire Wire Line
	1250 5175 1575 5175
Connection ~ 1575 5475
Connection ~ 1575 5175
Connection ~ 1250 5475
Connection ~ 1250 5175
Wire Wire Line
	1575 5175 1900 5175
Wire Wire Line
	1575 5475 1900 5475
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 60352D07
P 1900 5325
AR Path="/5E9C6910/60352D07" Ref="C?"  Part="1" 
AR Path="/5EBC6654/60352D07" Ref="C?"  Part="1" 
AR Path="/5EBC681B/60352D07" Ref="C?"  Part="1" 
AR Path="/5F05E355/60352D07" Ref="C?"  Part="1" 
AR Path="/6068445B/60352D07" Ref="C?"  Part="1" 
AR Path="/60E816A3/60352D07" Ref="C?"  Part="1" 
AR Path="/5EFE605D/60352D07" Ref="C?"  Part="1" 
AR Path="/5EA5E155/60352D07" Ref="C?"  Part="1" 
AR Path="/5ED47D19/60352D07" Ref="C200"  Part="1" 
F 0 "C200" H 1700 5400 50  0000 L CNN
F 1 "10uF" H 1700 5250 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 1900 5325 50  0001 C CNN
F 3 "~" H 1900 5325 50  0001 C CNN
F 4 "LMK107BBJ106MALT -  SMD Multilayer Ceramic Capacitor, 10 µF, 10 V, 0603 [1608 Metric], ± 20%, X5R, M Series" H 1900 4925 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/taiyo-yuden/lmk107bbj106malt/cap-10-f-10v-20-x5r-0603/dp/2113070" H 1900 4825 50  0001 C CNN "Distributor Link"
	1    1900 5325
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1900 5175 1900 5225
Wire Wire Line
	1900 5425 1900 5475
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 60352D11
P 1575 5325
AR Path="/5E9C6910/60352D11" Ref="C?"  Part="1" 
AR Path="/5EBC6654/60352D11" Ref="C?"  Part="1" 
AR Path="/5EBC681B/60352D11" Ref="C?"  Part="1" 
AR Path="/5F05E355/60352D11" Ref="C?"  Part="1" 
AR Path="/5EA41912/60352D11" Ref="C?"  Part="1" 
AR Path="/5EFE605D/60352D11" Ref="C?"  Part="1" 
AR Path="/5EA5E155/60352D11" Ref="C?"  Part="1" 
AR Path="/5ED47D19/60352D11" Ref="C198"  Part="1" 
F 0 "C198" H 1375 5400 50  0000 L CNN
F 1 "100nF" H 1325 5250 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 1575 5325 50  0001 C CNN
F 3 "~" H 1575 5325 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 1575 4925 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 1575 4825 50  0001 C CNN "Distributor Link"
	1    1575 5325
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1575 5175 1575 5225
Wire Wire Line
	1575 5425 1575 5475
Wire Wire Line
	1250 5475 1575 5475
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 60352D1C
P 925 5325
AR Path="/5E9C6910/60352D1C" Ref="C?"  Part="1" 
AR Path="/5EBC6654/60352D1C" Ref="C?"  Part="1" 
AR Path="/5EBC681B/60352D1C" Ref="C?"  Part="1" 
AR Path="/5F05E355/60352D1C" Ref="C?"  Part="1" 
AR Path="/5EA41912/60352D1C" Ref="C?"  Part="1" 
AR Path="/5EFE605D/60352D1C" Ref="C?"  Part="1" 
AR Path="/5EA5E155/60352D1C" Ref="C?"  Part="1" 
AR Path="/5ED47D19/60352D1C" Ref="C195"  Part="1" 
F 0 "C195" H 725 5400 50  0000 L CNN
F 1 "100nF" H 675 5250 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 925 5325 50  0001 C CNN
F 3 "~" H 925 5325 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 925 4925 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 925 4825 50  0001 C CNN "Distributor Link"
	1    925  5325
	-1   0    0    -1  
$EndComp
Wire Wire Line
	925  5175 1250 5175
Wire Wire Line
	925  5475 1250 5475
Wire Wire Line
	925  5175 925  5225
Wire Wire Line
	925  5425 925  5475
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 60352D29
P 1250 5325
AR Path="/5E9C6910/60352D29" Ref="C?"  Part="1" 
AR Path="/5EBC6654/60352D29" Ref="C?"  Part="1" 
AR Path="/5EBC681B/60352D29" Ref="C?"  Part="1" 
AR Path="/5F05E355/60352D29" Ref="C?"  Part="1" 
AR Path="/6068445B/60352D29" Ref="C?"  Part="1" 
AR Path="/60E816A3/60352D29" Ref="C?"  Part="1" 
AR Path="/5EFE605D/60352D29" Ref="C?"  Part="1" 
AR Path="/5EA5E155/60352D29" Ref="C?"  Part="1" 
AR Path="/5ED47D19/60352D29" Ref="C197"  Part="1" 
F 0 "C197" H 1050 5400 50  0000 L CNN
F 1 "10uF" H 1050 5250 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 1250 5325 50  0001 C CNN
F 3 "~" H 1250 5325 50  0001 C CNN
F 4 "LMK107BBJ106MALT -  SMD Multilayer Ceramic Capacitor, 10 µF, 10 V, 0603 [1608 Metric], ± 20%, X5R, M Series" H 1250 4925 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/taiyo-yuden/lmk107bbj106malt/cap-10-f-10v-20-x5r-0603/dp/2113070" H 1250 4825 50  0001 C CNN "Distributor Link"
	1    1250 5325
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1250 5175 1250 5225
Wire Wire Line
	1250 5425 1250 5475
Connection ~ 2900 5475
Connection ~ 2900 5175
Connection ~ 2575 5475
Connection ~ 2575 5175
Wire Wire Line
	2375 4050 2475 4050
Wire Wire Line
	2900 5175 3225 5175
Wire Wire Line
	2900 5475 3225 5475
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 60352D3B
P 3225 5325
AR Path="/5E9C6910/60352D3B" Ref="C?"  Part="1" 
AR Path="/5EBC6654/60352D3B" Ref="C?"  Part="1" 
AR Path="/5EBC681B/60352D3B" Ref="C?"  Part="1" 
AR Path="/5F05E355/60352D3B" Ref="C?"  Part="1" 
AR Path="/6068445B/60352D3B" Ref="C?"  Part="1" 
AR Path="/60E816A3/60352D3B" Ref="C?"  Part="1" 
AR Path="/5EFE605D/60352D3B" Ref="C?"  Part="1" 
AR Path="/5EA5E155/60352D3B" Ref="C?"  Part="1" 
AR Path="/5ED47D19/60352D3B" Ref="C207"  Part="1" 
F 0 "C207" H 3025 5400 50  0000 L CNN
F 1 "10uF" H 3025 5250 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 3225 5325 50  0001 C CNN
F 3 "~" H 3225 5325 50  0001 C CNN
F 4 "LMK107BBJ106MALT -  SMD Multilayer Ceramic Capacitor, 10 µF, 10 V, 0603 [1608 Metric], ± 20%, X5R, M Series" H 3225 4925 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/taiyo-yuden/lmk107bbj106malt/cap-10-f-10v-20-x5r-0603/dp/2113070" H 3225 4825 50  0001 C CNN "Distributor Link"
	1    3225 5325
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3225 5175 3225 5225
Wire Wire Line
	3225 5425 3225 5475
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 60352D45
P 2900 5325
AR Path="/5E9C6910/60352D45" Ref="C?"  Part="1" 
AR Path="/5EBC6654/60352D45" Ref="C?"  Part="1" 
AR Path="/5EBC681B/60352D45" Ref="C?"  Part="1" 
AR Path="/5F05E355/60352D45" Ref="C?"  Part="1" 
AR Path="/5EA41912/60352D45" Ref="C?"  Part="1" 
AR Path="/5EFE605D/60352D45" Ref="C?"  Part="1" 
AR Path="/5EA5E155/60352D45" Ref="C?"  Part="1" 
AR Path="/5ED47D19/60352D45" Ref="C205"  Part="1" 
F 0 "C205" H 2700 5400 50  0000 L CNN
F 1 "100nF" H 2650 5250 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 2900 5325 50  0001 C CNN
F 3 "~" H 2900 5325 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 2900 4925 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 2900 4825 50  0001 C CNN "Distributor Link"
	1    2900 5325
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2900 5175 2900 5225
Wire Wire Line
	2900 5425 2900 5475
Wire Wire Line
	2575 5475 2900 5475
Wire Wire Line
	3475 4050 3475 4150
$Comp
L PMSM_Driver_LV_Board_Device:Ferrite_Bead_Small FB?
U 1 1 60352D52
P 3050 4050
AR Path="/5EA5E155/60352D52" Ref="FB?"  Part="1" 
AR Path="/5ED47D19/60352D52" Ref="FB4"  Part="1" 
F 0 "FB4" V 3175 3975 50  0000 L CNN
F 1 "Z0805C420APWST" V 2925 3875 25  0000 L CNN
F 2 "PMSM_Driver_LV_Board:L_0805_2012Metric" V 2980 4050 50  0001 C CNN
F 3 "~" H 3050 4050 50  0001 C CNN
F 4 "Z0805C420APWST -  Ferrite Bead, 0805 [2012 Metric], 42 ohm, 4 A, Z-PWS Series, 0.008 ohm, ± 25%" H 3050 4050 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/kemet/z0805c420apwst/ferrite-bead-42-ohm-4a-0805/dp/2846741" H 3050 4050 50  0001 C CNN "Distributor Link"
F 6 "42 Ohms at 100 MHz" V 2875 3825 25  0000 L CNN "Impedance"
	1    3050 4050
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1225 4550 1225 4050
Connection ~ 1225 4550
Wire Wire Line
	1425 4550 1225 4550
Connection ~ 1225 4050
Wire Wire Line
	1225 4650 1225 4550
Wire Wire Line
	1425 4650 1225 4650
Wire Wire Line
	1225 4050 1425 4050
Wire Wire Line
	1225 3950 1225 4050
Connection ~ 1325 4750
Wire Wire Line
	1325 4150 1425 4150
Connection ~ 2850 4750
Wire Wire Line
	1325 4750 1425 4750
Wire Wire Line
	1325 4850 1325 4750
NoConn ~ 1425 4450
NoConn ~ 1425 4350
NoConn ~ 1425 4250
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 60352D6B
P 2250 5325
AR Path="/5E9C6910/60352D6B" Ref="C?"  Part="1" 
AR Path="/5EBC6654/60352D6B" Ref="C?"  Part="1" 
AR Path="/5EBC681B/60352D6B" Ref="C?"  Part="1" 
AR Path="/5F05E355/60352D6B" Ref="C?"  Part="1" 
AR Path="/5EA41912/60352D6B" Ref="C?"  Part="1" 
AR Path="/5EFE605D/60352D6B" Ref="C?"  Part="1" 
AR Path="/5EA5E155/60352D6B" Ref="C?"  Part="1" 
AR Path="/5ED47D19/60352D6B" Ref="C203"  Part="1" 
F 0 "C203" H 2050 5400 50  0000 L CNN
F 1 "100nF" H 2000 5250 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 2250 5325 50  0001 C CNN
F 3 "~" H 2250 5325 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 2250 4925 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 2250 4825 50  0001 C CNN "Distributor Link"
	1    2250 5325
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2250 5175 2475 5175
Wire Wire Line
	2250 5475 2575 5475
Connection ~ 2250 5475
Wire Wire Line
	2250 5175 2250 5225
Wire Wire Line
	2250 5425 2250 5475
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 60352D78
P 2575 5325
AR Path="/5E9C6910/60352D78" Ref="C?"  Part="1" 
AR Path="/5EBC6654/60352D78" Ref="C?"  Part="1" 
AR Path="/5EBC681B/60352D78" Ref="C?"  Part="1" 
AR Path="/5F05E355/60352D78" Ref="C?"  Part="1" 
AR Path="/6068445B/60352D78" Ref="C?"  Part="1" 
AR Path="/60E816A3/60352D78" Ref="C?"  Part="1" 
AR Path="/5EFE605D/60352D78" Ref="C?"  Part="1" 
AR Path="/5EA5E155/60352D78" Ref="C?"  Part="1" 
AR Path="/5ED47D19/60352D78" Ref="C204"  Part="1" 
F 0 "C204" H 2375 5400 50  0000 L CNN
F 1 "10uF" H 2375 5250 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 2575 5325 50  0001 C CNN
F 3 "~" H 2575 5325 50  0001 C CNN
F 4 "LMK107BBJ106MALT -  SMD Multilayer Ceramic Capacitor, 10 µF, 10 V, 0603 [1608 Metric], ± 20%, X5R, M Series" H 2575 4925 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/taiyo-yuden/lmk107bbj106malt/cap-10-f-10v-20-x5r-0603/dp/2113070" H 2575 4825 50  0001 C CNN "Distributor Link"
	1    2575 5325
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2575 5175 2575 5225
Wire Wire Line
	2575 5425 2575 5475
Wire Wire Line
	2250 5475 2250 5525
Text Notes 1625 4975 0    25   ~ 0
Isolated floating power supply
$Comp
L PMSM_Driver_LV_Board_IC:ADUM5000ARWZ U?
U 1 1 60352D85
P 1925 4400
AR Path="/5EA5E155/60352D85" Ref="U?"  Part="1" 
AR Path="/5ED47D19/60352D85" Ref="U51"  Part="1" 
F 0 "U51" H 1625 4900 50  0000 C CNN
F 1 "ADUM5000ARWZ" H 1925 3900 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:SOIC-16W_7.5x10.3mm_P1.27mm" H 1025 4950 50  0001 C CNN
F 3 "~" H 1025 4950 50  0001 C CNN
F 4 "ADUM5000ARWZ -  DC-DC Switching Regulator, Adjustable, 2.7V to 5.5V in, 3.3V to 5.5V/100 mA out, 180 MHz, WSOIC-16" H 1925 3800 31  0001 C CNN "Description"
F 5 "https://uk.farnell.com/analog-devices/adum5000arwz/ic-isolated-dc-dc-conv-180mhz/dp/2102522" H 1925 3750 31  0001 C CNN "Distributor Link"
F 6 "https://www.mouser.co.uk/ProductDetail/Analog-Devices/ADUM5000ARWZ?qs=sGAEpiMZZMtitjHzVIkrqYThiPHWFX9aYDreduI%2FIuI%3D" H 1925 3700 31  0001 C CNN "Distributor Link 2"
	1    1925 4400
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG020
U 1 1 60352D91
P 3475 4150
F 0 "#FLG020" H 3475 4225 50  0001 C CNN
F 1 "PWR_FLAG" H 3475 4323 50  0000 C CNN
F 2 "" H 3475 4150 50  0001 C CNN
F 3 "~" H 3475 4150 50  0001 C CNN
	1    3475 4150
	-1   0    0    1   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:ISO_DGND #PWR?
U 1 1 60352D97
P 2850 4850
AR Path="/5EA5D9E8/60352D97" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/60352D97" Ref="#PWR0402"  Part="1" 
F 0 "#PWR0402" H 2850 4600 50  0001 C CNN
F 1 "ISO_DGND" H 2855 4677 50  0000 C CNN
F 2 "" H 2850 4850 50  0001 C CNN
F 3 "" H 2850 4850 50  0001 C CNN
	1    2850 4850
	1    0    0    -1  
$EndComp
Wire Notes Line
	600  3300 600  5925
Wire Notes Line
	3850 3300 3850 5925
Wire Wire Line
	3150 4050 3475 4050
Connection ~ 3475 4050
Wire Notes Line
	600  3300 3850 3300
Wire Notes Line
	600  5925 3850 5925
Wire Wire Line
	2850 4750 2850 4850
Wire Wire Line
	1325 4150 1325 4750
Text Notes 4000 3525 0    100  ~ 0
Isolated 5V Supply for Digital Isolators
Wire Wire Line
	6175 4150 6175 4750
Wire Wire Line
	6800 4050 6800 3950
$Comp
L PMSM_Driver_LV_Board_Power:ISO_DGND #PWR?
U 1 1 6057D716
P 5575 5525
AR Path="/5EA5D9E8/6057D716" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/6057D716" Ref="#PWR0413"  Part="1" 
F 0 "#PWR0413" H 5575 5275 50  0001 C CNN
F 1 "ISO_DGND" H 5580 5352 50  0000 C CNN
F 2 "" H 5575 5525 50  0001 C CNN
F 3 "" H 5575 5525 50  0001 C CNN
	1    5575 5525
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_DGND #PWR?
U 1 1 6057D71C
P 4250 5525
AR Path="/5EA5D9E8/6057D71C" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/6057D71C" Ref="#PWR0408"  Part="1" 
F 0 "#PWR0408" H 4250 5275 50  0001 C CNN
F 1 "CTRL_DGND" H 4255 5352 50  0000 C CNN
F 2 "" H 4250 5525 50  0001 C CNN
F 3 "" H 4250 5525 50  0001 C CNN
	1    4250 5525
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_DGND #PWR?
U 1 1 6057D722
P 4650 4850
AR Path="/5EA5D9E8/6057D722" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/6057D722" Ref="#PWR0411"  Part="1" 
F 0 "#PWR0411" H 4650 4600 50  0001 C CNN
F 1 "CTRL_DGND" H 4655 4677 50  0000 C CNN
F 2 "" H 4650 4850 50  0001 C CNN
F 3 "" H 4650 4850 50  0001 C CNN
	1    4650 4850
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_5V0 #PWR0407
U 1 1 6057D728
P 4250 5075
F 0 "#PWR0407" H 4250 5325 50  0001 C CNN
F 1 "CTRL_5V0" H 4251 5248 50  0000 C CNN
F 2 "" H 4250 5075 50  0001 C CNN
F 3 "" H 4250 5075 50  0001 C CNN
	1    4250 5075
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_5V0 #PWR0410
U 1 1 6057D72E
P 4550 3950
F 0 "#PWR0410" H 4550 4200 50  0001 C CNN
F 1 "CTRL_5V0" H 4551 4123 50  0000 C CNN
F 2 "" H 4550 3950 50  0001 C CNN
F 3 "" H 4550 3950 50  0001 C CNN
	1    4550 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	5800 4050 5900 4050
Wire Wire Line
	5900 4050 6275 4050
Connection ~ 5900 4050
Wire Wire Line
	5900 4050 5900 4025
$Comp
L power:PWR_FLAG #FLG?
U 1 1 6057D738
P 5900 4025
AR Path="/5EA5E06A/6057D738" Ref="#FLG?"  Part="1" 
AR Path="/5EA5E155/6057D738" Ref="#FLG?"  Part="1" 
AR Path="/5ED47D19/6057D738" Ref="#FLG022"  Part="1" 
F 0 "#FLG022" H 5900 4100 50  0001 C CNN
F 1 "PWR_FLAG" H 5900 4179 25  0000 C CNN
F 2 "" H 5900 4025 50  0001 C CNN
F 3 "~" H 5900 4025 50  0001 C CNN
	1    5900 4025
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5800 4350 5800 4650
Connection ~ 5800 4350
Wire Wire Line
	5700 4350 5800 4350
Wire Wire Line
	5800 4650 5800 5175
Connection ~ 5800 4650
Wire Wire Line
	5700 4650 5800 4650
Wire Wire Line
	5700 4150 6175 4150
Wire Wire Line
	5700 4750 6175 4750
Wire Wire Line
	5900 5175 6225 5175
Wire Wire Line
	5800 5175 5900 5175
Connection ~ 5800 5175
Connection ~ 5800 4050
Wire Wire Line
	5800 4050 5800 4350
NoConn ~ 5700 4550
NoConn ~ 5700 4450
NoConn ~ 5700 4250
Connection ~ 4250 5175
Wire Wire Line
	4250 5175 4250 5075
Wire Wire Line
	4575 5175 4900 5175
Connection ~ 4900 5475
Connection ~ 4900 5175
Connection ~ 4575 5475
Connection ~ 4575 5175
Wire Wire Line
	4900 5175 5225 5175
Wire Wire Line
	4900 5475 5225 5475
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 6057D759
P 5225 5325
AR Path="/5E9C6910/6057D759" Ref="C?"  Part="1" 
AR Path="/5EBC6654/6057D759" Ref="C?"  Part="1" 
AR Path="/5EBC681B/6057D759" Ref="C?"  Part="1" 
AR Path="/5F05E355/6057D759" Ref="C?"  Part="1" 
AR Path="/6068445B/6057D759" Ref="C?"  Part="1" 
AR Path="/60E816A3/6057D759" Ref="C?"  Part="1" 
AR Path="/5EFE605D/6057D759" Ref="C?"  Part="1" 
AR Path="/5EA5E155/6057D759" Ref="C?"  Part="1" 
AR Path="/5ED47D19/6057D759" Ref="C213"  Part="1" 
F 0 "C213" H 5025 5400 50  0000 L CNN
F 1 "10uF" H 5025 5250 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 5225 5325 50  0001 C CNN
F 3 "~" H 5225 5325 50  0001 C CNN
F 4 "LMK107BBJ106MALT -  SMD Multilayer Ceramic Capacitor, 10 µF, 10 V, 0603 [1608 Metric], ± 20%, X5R, M Series" H 5225 4925 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/taiyo-yuden/lmk107bbj106malt/cap-10-f-10v-20-x5r-0603/dp/2113070" H 5225 4825 50  0001 C CNN "Distributor Link"
	1    5225 5325
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5225 5175 5225 5225
Wire Wire Line
	5225 5425 5225 5475
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 6057D763
P 4900 5325
AR Path="/5E9C6910/6057D763" Ref="C?"  Part="1" 
AR Path="/5EBC6654/6057D763" Ref="C?"  Part="1" 
AR Path="/5EBC681B/6057D763" Ref="C?"  Part="1" 
AR Path="/5F05E355/6057D763" Ref="C?"  Part="1" 
AR Path="/5EA41912/6057D763" Ref="C?"  Part="1" 
AR Path="/5EFE605D/6057D763" Ref="C?"  Part="1" 
AR Path="/5EA5E155/6057D763" Ref="C?"  Part="1" 
AR Path="/5ED47D19/6057D763" Ref="C212"  Part="1" 
F 0 "C212" H 4700 5400 50  0000 L CNN
F 1 "100nF" H 4650 5250 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 4900 5325 50  0001 C CNN
F 3 "~" H 4900 5325 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 4900 4925 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 4900 4825 50  0001 C CNN "Distributor Link"
	1    4900 5325
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4900 5175 4900 5225
Wire Wire Line
	4900 5425 4900 5475
Wire Wire Line
	4575 5475 4900 5475
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 6057D76E
P 4250 5325
AR Path="/5E9C6910/6057D76E" Ref="C?"  Part="1" 
AR Path="/5EBC6654/6057D76E" Ref="C?"  Part="1" 
AR Path="/5EBC681B/6057D76E" Ref="C?"  Part="1" 
AR Path="/5F05E355/6057D76E" Ref="C?"  Part="1" 
AR Path="/5EA41912/6057D76E" Ref="C?"  Part="1" 
AR Path="/5EFE605D/6057D76E" Ref="C?"  Part="1" 
AR Path="/5EA5E155/6057D76E" Ref="C?"  Part="1" 
AR Path="/5ED47D19/6057D76E" Ref="C209"  Part="1" 
F 0 "C209" H 4050 5400 50  0000 L CNN
F 1 "100nF" H 4000 5250 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 4250 5325 50  0001 C CNN
F 3 "~" H 4250 5325 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 4250 4925 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 4250 4825 50  0001 C CNN "Distributor Link"
	1    4250 5325
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4250 5175 4575 5175
Wire Wire Line
	4250 5475 4575 5475
Connection ~ 4250 5475
Wire Wire Line
	4250 5175 4250 5225
Wire Wire Line
	4250 5425 4250 5475
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 6057D77B
P 4575 5325
AR Path="/5E9C6910/6057D77B" Ref="C?"  Part="1" 
AR Path="/5EBC6654/6057D77B" Ref="C?"  Part="1" 
AR Path="/5EBC681B/6057D77B" Ref="C?"  Part="1" 
AR Path="/5F05E355/6057D77B" Ref="C?"  Part="1" 
AR Path="/6068445B/6057D77B" Ref="C?"  Part="1" 
AR Path="/60E816A3/6057D77B" Ref="C?"  Part="1" 
AR Path="/5EFE605D/6057D77B" Ref="C?"  Part="1" 
AR Path="/5EA5E155/6057D77B" Ref="C?"  Part="1" 
AR Path="/5ED47D19/6057D77B" Ref="C211"  Part="1" 
F 0 "C211" H 4375 5400 50  0000 L CNN
F 1 "10uF" H 4375 5250 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4575 5325 50  0001 C CNN
F 3 "~" H 4575 5325 50  0001 C CNN
F 4 "LMK107BBJ106MALT -  SMD Multilayer Ceramic Capacitor, 10 µF, 10 V, 0603 [1608 Metric], ± 20%, X5R, M Series" H 4575 4925 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/taiyo-yuden/lmk107bbj106malt/cap-10-f-10v-20-x5r-0603/dp/2113070" H 4575 4825 50  0001 C CNN "Distributor Link"
	1    4575 5325
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4575 5175 4575 5225
Wire Wire Line
	4575 5425 4575 5475
Wire Wire Line
	4250 5475 4250 5525
Connection ~ 6225 5475
Connection ~ 6225 5175
Connection ~ 5900 5475
Connection ~ 5900 5175
Wire Wire Line
	5700 4050 5800 4050
Wire Wire Line
	6225 5175 6550 5175
Wire Wire Line
	6225 5475 6550 5475
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 6057D78D
P 6550 5325
AR Path="/5E9C6910/6057D78D" Ref="C?"  Part="1" 
AR Path="/5EBC6654/6057D78D" Ref="C?"  Part="1" 
AR Path="/5EBC681B/6057D78D" Ref="C?"  Part="1" 
AR Path="/5F05E355/6057D78D" Ref="C?"  Part="1" 
AR Path="/6068445B/6057D78D" Ref="C?"  Part="1" 
AR Path="/60E816A3/6057D78D" Ref="C?"  Part="1" 
AR Path="/5EFE605D/6057D78D" Ref="C?"  Part="1" 
AR Path="/5EA5E155/6057D78D" Ref="C?"  Part="1" 
AR Path="/5ED47D19/6057D78D" Ref="C219"  Part="1" 
F 0 "C219" H 6350 5400 50  0000 L CNN
F 1 "10uF" H 6350 5250 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 6550 5325 50  0001 C CNN
F 3 "~" H 6550 5325 50  0001 C CNN
F 4 "LMK107BBJ106MALT -  SMD Multilayer Ceramic Capacitor, 10 µF, 10 V, 0603 [1608 Metric], ± 20%, X5R, M Series" H 6550 4925 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/taiyo-yuden/lmk107bbj106malt/cap-10-f-10v-20-x5r-0603/dp/2113070" H 6550 4825 50  0001 C CNN "Distributor Link"
	1    6550 5325
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6550 5175 6550 5225
Wire Wire Line
	6550 5425 6550 5475
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 6057D797
P 6225 5325
AR Path="/5E9C6910/6057D797" Ref="C?"  Part="1" 
AR Path="/5EBC6654/6057D797" Ref="C?"  Part="1" 
AR Path="/5EBC681B/6057D797" Ref="C?"  Part="1" 
AR Path="/5F05E355/6057D797" Ref="C?"  Part="1" 
AR Path="/5EA41912/6057D797" Ref="C?"  Part="1" 
AR Path="/5EFE605D/6057D797" Ref="C?"  Part="1" 
AR Path="/5EA5E155/6057D797" Ref="C?"  Part="1" 
AR Path="/5ED47D19/6057D797" Ref="C217"  Part="1" 
F 0 "C217" H 6025 5400 50  0000 L CNN
F 1 "100nF" H 5975 5250 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 6225 5325 50  0001 C CNN
F 3 "~" H 6225 5325 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 6225 4925 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 6225 4825 50  0001 C CNN "Distributor Link"
	1    6225 5325
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6225 5175 6225 5225
Wire Wire Line
	6225 5425 6225 5475
Wire Wire Line
	5900 5475 6225 5475
Wire Wire Line
	6800 4050 6800 4150
$Comp
L PMSM_Driver_LV_Board_Device:Ferrite_Bead_Small FB?
U 1 1 6057D7A4
P 6375 4050
AR Path="/5EA5E155/6057D7A4" Ref="FB?"  Part="1" 
AR Path="/5ED47D19/6057D7A4" Ref="FB6"  Part="1" 
F 0 "FB6" V 6500 3975 50  0000 L CNN
F 1 "Z0805C420APWST" V 6250 3875 25  0000 L CNN
F 2 "PMSM_Driver_LV_Board:L_0805_2012Metric" V 6305 4050 50  0001 C CNN
F 3 "~" H 6375 4050 50  0001 C CNN
F 4 "Z0805C420APWST -  Ferrite Bead, 0805 [2012 Metric], 42 ohm, 4 A, Z-PWS Series, 0.008 ohm, ± 25%" H 6375 4050 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/kemet/z0805c420apwst/ferrite-bead-42-ohm-4a-0805/dp/2846741" H 6375 4050 50  0001 C CNN "Distributor Link"
F 6 "42 Ohms at 100 MHz" V 6200 3825 25  0000 L CNN "Impedance"
	1    6375 4050
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4550 4550 4550 4050
Connection ~ 4550 4550
Wire Wire Line
	4750 4550 4550 4550
Connection ~ 4550 4050
Wire Wire Line
	4550 4650 4550 4550
Wire Wire Line
	4750 4650 4550 4650
Wire Wire Line
	4550 4050 4750 4050
Wire Wire Line
	4550 3950 4550 4050
Connection ~ 4650 4750
Wire Wire Line
	4650 4150 4750 4150
Connection ~ 6175 4750
Wire Wire Line
	4650 4750 4750 4750
Wire Wire Line
	4650 4850 4650 4750
NoConn ~ 4750 4450
NoConn ~ 4750 4350
NoConn ~ 4750 4250
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 6057D7BC
P 5575 5325
AR Path="/5E9C6910/6057D7BC" Ref="C?"  Part="1" 
AR Path="/5EBC6654/6057D7BC" Ref="C?"  Part="1" 
AR Path="/5EBC681B/6057D7BC" Ref="C?"  Part="1" 
AR Path="/5F05E355/6057D7BC" Ref="C?"  Part="1" 
AR Path="/5EA41912/6057D7BC" Ref="C?"  Part="1" 
AR Path="/5EFE605D/6057D7BC" Ref="C?"  Part="1" 
AR Path="/5EA5E155/6057D7BC" Ref="C?"  Part="1" 
AR Path="/5ED47D19/6057D7BC" Ref="C214"  Part="1" 
F 0 "C214" H 5375 5400 50  0000 L CNN
F 1 "100nF" H 5325 5250 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 5575 5325 50  0001 C CNN
F 3 "~" H 5575 5325 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 5575 4925 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 5575 4825 50  0001 C CNN "Distributor Link"
	1    5575 5325
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5575 5175 5800 5175
Wire Wire Line
	5575 5475 5900 5475
Connection ~ 5575 5475
Wire Wire Line
	5575 5175 5575 5225
Wire Wire Line
	5575 5425 5575 5475
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 6057D7C9
P 5900 5325
AR Path="/5E9C6910/6057D7C9" Ref="C?"  Part="1" 
AR Path="/5EBC6654/6057D7C9" Ref="C?"  Part="1" 
AR Path="/5EBC681B/6057D7C9" Ref="C?"  Part="1" 
AR Path="/5F05E355/6057D7C9" Ref="C?"  Part="1" 
AR Path="/6068445B/6057D7C9" Ref="C?"  Part="1" 
AR Path="/60E816A3/6057D7C9" Ref="C?"  Part="1" 
AR Path="/5EFE605D/6057D7C9" Ref="C?"  Part="1" 
AR Path="/5EA5E155/6057D7C9" Ref="C?"  Part="1" 
AR Path="/5ED47D19/6057D7C9" Ref="C215"  Part="1" 
F 0 "C215" H 5700 5400 50  0000 L CNN
F 1 "10uF" H 5700 5250 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 5900 5325 50  0001 C CNN
F 3 "~" H 5900 5325 50  0001 C CNN
F 4 "LMK107BBJ106MALT -  SMD Multilayer Ceramic Capacitor, 10 µF, 10 V, 0603 [1608 Metric], ± 20%, X5R, M Series" H 5900 4925 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/taiyo-yuden/lmk107bbj106malt/cap-10-f-10v-20-x5r-0603/dp/2113070" H 5900 4825 50  0001 C CNN "Distributor Link"
	1    5900 5325
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5900 5175 5900 5225
Wire Wire Line
	5900 5425 5900 5475
Wire Wire Line
	5575 5475 5575 5525
Text Notes 4950 4975 0    25   ~ 0
Isolated floating power supply
$Comp
L PMSM_Driver_LV_Board_IC:ADUM5000ARWZ U?
U 1 1 6057D7D6
P 5250 4400
AR Path="/5EA5E155/6057D7D6" Ref="U?"  Part="1" 
AR Path="/5ED47D19/6057D7D6" Ref="U53"  Part="1" 
F 0 "U53" H 4950 4900 50  0000 C CNN
F 1 "ADUM5000ARWZ" H 5250 3900 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:SOIC-16W_7.5x10.3mm_P1.27mm" H 4350 4950 50  0001 C CNN
F 3 "~" H 4350 4950 50  0001 C CNN
F 4 "ADUM5000ARWZ -  DC-DC Switching Regulator, Adjustable, 2.7V to 5.5V in, 3.3V to 5.5V/100 mA out, 180 MHz, WSOIC-16" H 5250 3800 31  0001 C CNN "Description"
F 5 "https://uk.farnell.com/analog-devices/adum5000arwz/ic-isolated-dc-dc-conv-180mhz/dp/2102522" H 5250 3750 31  0001 C CNN "Distributor Link"
F 6 "https://www.mouser.co.uk/ProductDetail/Analog-Devices/ADUM5000ARWZ?qs=sGAEpiMZZMtitjHzVIkrqYThiPHWFX9aYDreduI%2FIuI%3D" H 5250 3700 31  0001 C CNN "Distributor Link 2"
	1    5250 4400
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG023
U 1 1 6057D7DC
P 6800 4150
F 0 "#FLG023" H 6800 4225 50  0001 C CNN
F 1 "PWR_FLAG" H 6800 4323 50  0000 C CNN
F 2 "" H 6800 4150 50  0001 C CNN
F 3 "~" H 6800 4150 50  0001 C CNN
	1    6800 4150
	-1   0    0    1   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:ISO_DGND #PWR?
U 1 1 6057D7E2
P 6175 4850
AR Path="/5EA5D9E8/6057D7E2" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/6057D7E2" Ref="#PWR0414"  Part="1" 
F 0 "#PWR0414" H 6175 4600 50  0001 C CNN
F 1 "ISO_DGND" H 6180 4677 50  0000 C CNN
F 2 "" H 6175 4850 50  0001 C CNN
F 3 "" H 6175 4850 50  0001 C CNN
	1    6175 4850
	1    0    0    -1  
$EndComp
Wire Notes Line
	3925 3300 3925 5925
Wire Notes Line
	7175 3300 7175 5925
Wire Wire Line
	6475 4050 6800 4050
Connection ~ 6800 4050
Wire Notes Line
	3925 3300 7175 3300
Wire Notes Line
	3925 5925 7175 5925
Wire Wire Line
	6175 4750 6175 4850
Wire Wire Line
	4650 4150 4650 4750
Text Notes 7325 3525 0    100  ~ 0
Isolated 5V Supply for RDC
Wire Wire Line
	9500 4150 9500 4750
Wire Wire Line
	10125 4050 10125 3950
$Comp
L PMSM_Driver_LV_Board_Power:ISO_DGND #PWR?
U 1 1 6061F0A5
P 8900 5525
AR Path="/5EA5D9E8/6061F0A5" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/6061F0A5" Ref="#PWR0427"  Part="1" 
F 0 "#PWR0427" H 8900 5275 50  0001 C CNN
F 1 "ISO_DGND" H 8905 5352 50  0000 C CNN
F 2 "" H 8900 5525 50  0001 C CNN
F 3 "" H 8900 5525 50  0001 C CNN
	1    8900 5525
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_DGND #PWR?
U 1 1 6061F0AB
P 7575 5525
AR Path="/5EA5D9E8/6061F0AB" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/6061F0AB" Ref="#PWR0422"  Part="1" 
F 0 "#PWR0422" H 7575 5275 50  0001 C CNN
F 1 "CTRL_DGND" H 7580 5352 50  0000 C CNN
F 2 "" H 7575 5525 50  0001 C CNN
F 3 "" H 7575 5525 50  0001 C CNN
	1    7575 5525
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_DGND #PWR?
U 1 1 6061F0B1
P 7975 4850
AR Path="/5EA5D9E8/6061F0B1" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/6061F0B1" Ref="#PWR0424"  Part="1" 
F 0 "#PWR0424" H 7975 4600 50  0001 C CNN
F 1 "CTRL_DGND" H 7980 4677 50  0000 C CNN
F 2 "" H 7975 4850 50  0001 C CNN
F 3 "" H 7975 4850 50  0001 C CNN
	1    7975 4850
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_5V0 #PWR0421
U 1 1 6061F0B7
P 7575 5075
F 0 "#PWR0421" H 7575 5325 50  0001 C CNN
F 1 "CTRL_5V0" H 7576 5248 50  0000 C CNN
F 2 "" H 7575 5075 50  0001 C CNN
F 3 "" H 7575 5075 50  0001 C CNN
	1    7575 5075
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_5V0 #PWR0423
U 1 1 6061F0BD
P 7875 3950
F 0 "#PWR0423" H 7875 4200 50  0001 C CNN
F 1 "CTRL_5V0" H 7876 4123 50  0000 C CNN
F 2 "" H 7875 3950 50  0001 C CNN
F 3 "" H 7875 3950 50  0001 C CNN
	1    7875 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	9125 4050 9225 4050
Wire Wire Line
	9225 4050 9600 4050
Connection ~ 9225 4050
Wire Wire Line
	9225 4050 9225 4025
$Comp
L power:PWR_FLAG #FLG?
U 1 1 6061F0C7
P 9225 4025
AR Path="/5EA5E06A/6061F0C7" Ref="#FLG?"  Part="1" 
AR Path="/5EA5E155/6061F0C7" Ref="#FLG?"  Part="1" 
AR Path="/5ED47D19/6061F0C7" Ref="#FLG027"  Part="1" 
F 0 "#FLG027" H 9225 4100 50  0001 C CNN
F 1 "PWR_FLAG" H 9225 4179 25  0000 C CNN
F 2 "" H 9225 4025 50  0001 C CNN
F 3 "~" H 9225 4025 50  0001 C CNN
	1    9225 4025
	-1   0    0    -1  
$EndComp
Wire Wire Line
	9125 4350 9125 4650
Connection ~ 9125 4350
Wire Wire Line
	9025 4350 9125 4350
Wire Wire Line
	9125 4650 9125 5175
Connection ~ 9125 4650
Wire Wire Line
	9025 4650 9125 4650
Wire Wire Line
	9025 4150 9500 4150
Wire Wire Line
	9025 4750 9500 4750
Wire Wire Line
	9225 5175 9550 5175
Wire Wire Line
	9125 5175 9225 5175
Connection ~ 9125 5175
Connection ~ 9125 4050
Wire Wire Line
	9125 4050 9125 4350
NoConn ~ 9025 4550
NoConn ~ 9025 4450
NoConn ~ 9025 4250
Connection ~ 7575 5175
Wire Wire Line
	7575 5175 7575 5075
Wire Wire Line
	7900 5175 8225 5175
Connection ~ 8225 5475
Connection ~ 8225 5175
Connection ~ 7900 5475
Connection ~ 7900 5175
Wire Wire Line
	8225 5175 8550 5175
Wire Wire Line
	8225 5475 8550 5475
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 6061F0E8
P 8550 5325
AR Path="/5E9C6910/6061F0E8" Ref="C?"  Part="1" 
AR Path="/5EBC6654/6061F0E8" Ref="C?"  Part="1" 
AR Path="/5EBC681B/6061F0E8" Ref="C?"  Part="1" 
AR Path="/5F05E355/6061F0E8" Ref="C?"  Part="1" 
AR Path="/6068445B/6061F0E8" Ref="C?"  Part="1" 
AR Path="/60E816A3/6061F0E8" Ref="C?"  Part="1" 
AR Path="/5EFE605D/6061F0E8" Ref="C?"  Part="1" 
AR Path="/5EA5E155/6061F0E8" Ref="C?"  Part="1" 
AR Path="/5ED47D19/6061F0E8" Ref="C229"  Part="1" 
F 0 "C229" H 8350 5400 50  0000 L CNN
F 1 "10uF" H 8350 5250 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 8550 5325 50  0001 C CNN
F 3 "~" H 8550 5325 50  0001 C CNN
F 4 "LMK107BBJ106MALT -  SMD Multilayer Ceramic Capacitor, 10 µF, 10 V, 0603 [1608 Metric], ± 20%, X5R, M Series" H 8550 4925 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/taiyo-yuden/lmk107bbj106malt/cap-10-f-10v-20-x5r-0603/dp/2113070" H 8550 4825 50  0001 C CNN "Distributor Link"
	1    8550 5325
	-1   0    0    -1  
$EndComp
Wire Wire Line
	8550 5175 8550 5225
Wire Wire Line
	8550 5425 8550 5475
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 6061F0F2
P 8225 5325
AR Path="/5E9C6910/6061F0F2" Ref="C?"  Part="1" 
AR Path="/5EBC6654/6061F0F2" Ref="C?"  Part="1" 
AR Path="/5EBC681B/6061F0F2" Ref="C?"  Part="1" 
AR Path="/5F05E355/6061F0F2" Ref="C?"  Part="1" 
AR Path="/5EA41912/6061F0F2" Ref="C?"  Part="1" 
AR Path="/5EFE605D/6061F0F2" Ref="C?"  Part="1" 
AR Path="/5EA5E155/6061F0F2" Ref="C?"  Part="1" 
AR Path="/5ED47D19/6061F0F2" Ref="C227"  Part="1" 
F 0 "C227" H 8025 5400 50  0000 L CNN
F 1 "100nF" H 7975 5250 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 8225 5325 50  0001 C CNN
F 3 "~" H 8225 5325 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 8225 4925 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 8225 4825 50  0001 C CNN "Distributor Link"
	1    8225 5325
	-1   0    0    -1  
$EndComp
Wire Wire Line
	8225 5175 8225 5225
Wire Wire Line
	8225 5425 8225 5475
Wire Wire Line
	7900 5475 8225 5475
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 6061F0FD
P 7575 5325
AR Path="/5E9C6910/6061F0FD" Ref="C?"  Part="1" 
AR Path="/5EBC6654/6061F0FD" Ref="C?"  Part="1" 
AR Path="/5EBC681B/6061F0FD" Ref="C?"  Part="1" 
AR Path="/5F05E355/6061F0FD" Ref="C?"  Part="1" 
AR Path="/5EA41912/6061F0FD" Ref="C?"  Part="1" 
AR Path="/5EFE605D/6061F0FD" Ref="C?"  Part="1" 
AR Path="/5EA5E155/6061F0FD" Ref="C?"  Part="1" 
AR Path="/5ED47D19/6061F0FD" Ref="C223"  Part="1" 
F 0 "C223" H 7375 5400 50  0000 L CNN
F 1 "100nF" H 7325 5250 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 7575 5325 50  0001 C CNN
F 3 "~" H 7575 5325 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 7575 4925 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 7575 4825 50  0001 C CNN "Distributor Link"
	1    7575 5325
	-1   0    0    -1  
$EndComp
Wire Wire Line
	7575 5175 7900 5175
Wire Wire Line
	7575 5475 7900 5475
Connection ~ 7575 5475
Wire Wire Line
	7575 5175 7575 5225
Wire Wire Line
	7575 5425 7575 5475
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 6061F10A
P 7900 5325
AR Path="/5E9C6910/6061F10A" Ref="C?"  Part="1" 
AR Path="/5EBC6654/6061F10A" Ref="C?"  Part="1" 
AR Path="/5EBC681B/6061F10A" Ref="C?"  Part="1" 
AR Path="/5F05E355/6061F10A" Ref="C?"  Part="1" 
AR Path="/6068445B/6061F10A" Ref="C?"  Part="1" 
AR Path="/60E816A3/6061F10A" Ref="C?"  Part="1" 
AR Path="/5EFE605D/6061F10A" Ref="C?"  Part="1" 
AR Path="/5EA5E155/6061F10A" Ref="C?"  Part="1" 
AR Path="/5ED47D19/6061F10A" Ref="C225"  Part="1" 
F 0 "C225" H 7700 5400 50  0000 L CNN
F 1 "10uF" H 7700 5250 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 7900 5325 50  0001 C CNN
F 3 "~" H 7900 5325 50  0001 C CNN
F 4 "LMK107BBJ106MALT -  SMD Multilayer Ceramic Capacitor, 10 µF, 10 V, 0603 [1608 Metric], ± 20%, X5R, M Series" H 7900 4925 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/taiyo-yuden/lmk107bbj106malt/cap-10-f-10v-20-x5r-0603/dp/2113070" H 7900 4825 50  0001 C CNN "Distributor Link"
	1    7900 5325
	-1   0    0    -1  
$EndComp
Wire Wire Line
	7900 5175 7900 5225
Wire Wire Line
	7900 5425 7900 5475
Wire Wire Line
	7575 5475 7575 5525
Connection ~ 9550 5475
Connection ~ 9550 5175
Connection ~ 9225 5475
Connection ~ 9225 5175
Wire Wire Line
	9025 4050 9125 4050
Wire Wire Line
	9550 5175 9875 5175
Wire Wire Line
	9550 5475 9875 5475
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 6061F11C
P 9875 5325
AR Path="/5E9C6910/6061F11C" Ref="C?"  Part="1" 
AR Path="/5EBC6654/6061F11C" Ref="C?"  Part="1" 
AR Path="/5EBC681B/6061F11C" Ref="C?"  Part="1" 
AR Path="/5F05E355/6061F11C" Ref="C?"  Part="1" 
AR Path="/6068445B/6061F11C" Ref="C?"  Part="1" 
AR Path="/60E816A3/6061F11C" Ref="C?"  Part="1" 
AR Path="/5EFE605D/6061F11C" Ref="C?"  Part="1" 
AR Path="/5EA5E155/6061F11C" Ref="C?"  Part="1" 
AR Path="/5ED47D19/6061F11C" Ref="C233"  Part="1" 
F 0 "C233" H 9675 5400 50  0000 L CNN
F 1 "10uF" H 9675 5250 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 9875 5325 50  0001 C CNN
F 3 "~" H 9875 5325 50  0001 C CNN
F 4 "LMK107BBJ106MALT -  SMD Multilayer Ceramic Capacitor, 10 µF, 10 V, 0603 [1608 Metric], ± 20%, X5R, M Series" H 9875 4925 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/taiyo-yuden/lmk107bbj106malt/cap-10-f-10v-20-x5r-0603/dp/2113070" H 9875 4825 50  0001 C CNN "Distributor Link"
	1    9875 5325
	-1   0    0    -1  
$EndComp
Wire Wire Line
	9875 5175 9875 5225
Wire Wire Line
	9875 5425 9875 5475
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 6061F126
P 9550 5325
AR Path="/5E9C6910/6061F126" Ref="C?"  Part="1" 
AR Path="/5EBC6654/6061F126" Ref="C?"  Part="1" 
AR Path="/5EBC681B/6061F126" Ref="C?"  Part="1" 
AR Path="/5F05E355/6061F126" Ref="C?"  Part="1" 
AR Path="/5EA41912/6061F126" Ref="C?"  Part="1" 
AR Path="/5EFE605D/6061F126" Ref="C?"  Part="1" 
AR Path="/5EA5E155/6061F126" Ref="C?"  Part="1" 
AR Path="/5ED47D19/6061F126" Ref="C232"  Part="1" 
F 0 "C232" H 9350 5400 50  0000 L CNN
F 1 "100nF" H 9300 5250 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 9550 5325 50  0001 C CNN
F 3 "~" H 9550 5325 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 9550 4925 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 9550 4825 50  0001 C CNN "Distributor Link"
	1    9550 5325
	-1   0    0    -1  
$EndComp
Wire Wire Line
	9550 5175 9550 5225
Wire Wire Line
	9550 5425 9550 5475
Wire Wire Line
	9225 5475 9550 5475
Wire Wire Line
	10125 4050 10125 4150
$Comp
L PMSM_Driver_LV_Board_Device:Ferrite_Bead_Small FB?
U 1 1 6061F133
P 9700 4050
AR Path="/5EA5E155/6061F133" Ref="FB?"  Part="1" 
AR Path="/5ED47D19/6061F133" Ref="FB8"  Part="1" 
F 0 "FB8" V 9825 3975 50  0000 L CNN
F 1 "Z0805C420APWST" V 9575 3875 25  0000 L CNN
F 2 "PMSM_Driver_LV_Board:L_0805_2012Metric" V 9630 4050 50  0001 C CNN
F 3 "~" H 9700 4050 50  0001 C CNN
F 4 "Z0805C420APWST -  Ferrite Bead, 0805 [2012 Metric], 42 ohm, 4 A, Z-PWS Series, 0.008 ohm, ± 25%" H 9700 4050 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/kemet/z0805c420apwst/ferrite-bead-42-ohm-4a-0805/dp/2846741" H 9700 4050 50  0001 C CNN "Distributor Link"
F 6 "42 Ohms at 100 MHz" V 9525 3825 25  0000 L CNN "Impedance"
	1    9700 4050
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7875 4550 7875 4050
Connection ~ 7875 4550
Wire Wire Line
	8075 4550 7875 4550
Connection ~ 7875 4050
Wire Wire Line
	7875 4650 7875 4550
Wire Wire Line
	8075 4650 7875 4650
Wire Wire Line
	7875 4050 8075 4050
Wire Wire Line
	7875 3950 7875 4050
Connection ~ 7975 4750
Wire Wire Line
	7975 4150 8075 4150
Connection ~ 9500 4750
Wire Wire Line
	7975 4750 8075 4750
Wire Wire Line
	7975 4850 7975 4750
NoConn ~ 8075 4450
NoConn ~ 8075 4350
NoConn ~ 8075 4250
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 6061F14B
P 8900 5325
AR Path="/5E9C6910/6061F14B" Ref="C?"  Part="1" 
AR Path="/5EBC6654/6061F14B" Ref="C?"  Part="1" 
AR Path="/5EBC681B/6061F14B" Ref="C?"  Part="1" 
AR Path="/5F05E355/6061F14B" Ref="C?"  Part="1" 
AR Path="/5EA41912/6061F14B" Ref="C?"  Part="1" 
AR Path="/5EFE605D/6061F14B" Ref="C?"  Part="1" 
AR Path="/5EA5E155/6061F14B" Ref="C?"  Part="1" 
AR Path="/5ED47D19/6061F14B" Ref="C230"  Part="1" 
F 0 "C230" H 8700 5400 50  0000 L CNN
F 1 "100nF" H 8650 5250 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 8900 5325 50  0001 C CNN
F 3 "~" H 8900 5325 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 8900 4925 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 8900 4825 50  0001 C CNN "Distributor Link"
	1    8900 5325
	-1   0    0    -1  
$EndComp
Wire Wire Line
	8900 5175 9125 5175
Wire Wire Line
	8900 5475 9225 5475
Connection ~ 8900 5475
Wire Wire Line
	8900 5175 8900 5225
Wire Wire Line
	8900 5425 8900 5475
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 6061F158
P 9225 5325
AR Path="/5E9C6910/6061F158" Ref="C?"  Part="1" 
AR Path="/5EBC6654/6061F158" Ref="C?"  Part="1" 
AR Path="/5EBC681B/6061F158" Ref="C?"  Part="1" 
AR Path="/5F05E355/6061F158" Ref="C?"  Part="1" 
AR Path="/6068445B/6061F158" Ref="C?"  Part="1" 
AR Path="/60E816A3/6061F158" Ref="C?"  Part="1" 
AR Path="/5EFE605D/6061F158" Ref="C?"  Part="1" 
AR Path="/5EA5E155/6061F158" Ref="C?"  Part="1" 
AR Path="/5ED47D19/6061F158" Ref="C231"  Part="1" 
F 0 "C231" H 9025 5400 50  0000 L CNN
F 1 "10uF" H 9025 5250 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 9225 5325 50  0001 C CNN
F 3 "~" H 9225 5325 50  0001 C CNN
F 4 "LMK107BBJ106MALT -  SMD Multilayer Ceramic Capacitor, 10 µF, 10 V, 0603 [1608 Metric], ± 20%, X5R, M Series" H 9225 4925 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/taiyo-yuden/lmk107bbj106malt/cap-10-f-10v-20-x5r-0603/dp/2113070" H 9225 4825 50  0001 C CNN "Distributor Link"
	1    9225 5325
	-1   0    0    -1  
$EndComp
Wire Wire Line
	9225 5175 9225 5225
Wire Wire Line
	9225 5425 9225 5475
Wire Wire Line
	8900 5475 8900 5525
Text Notes 8275 4975 0    25   ~ 0
Isolated floating power supply
$Comp
L PMSM_Driver_LV_Board_IC:ADUM5000ARWZ U?
U 1 1 6061F165
P 8575 4400
AR Path="/5EA5E155/6061F165" Ref="U?"  Part="1" 
AR Path="/5ED47D19/6061F165" Ref="U55"  Part="1" 
F 0 "U55" H 8275 4900 50  0000 C CNN
F 1 "ADUM5000ARWZ" H 8575 3900 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:SOIC-16W_7.5x10.3mm_P1.27mm" H 7675 4950 50  0001 C CNN
F 3 "~" H 7675 4950 50  0001 C CNN
F 4 "ADUM5000ARWZ -  DC-DC Switching Regulator, Adjustable, 2.7V to 5.5V in, 3.3V to 5.5V/100 mA out, 180 MHz, WSOIC-16" H 8575 3800 31  0001 C CNN "Description"
F 5 "https://uk.farnell.com/analog-devices/adum5000arwz/ic-isolated-dc-dc-conv-180mhz/dp/2102522" H 8575 3750 31  0001 C CNN "Distributor Link"
F 6 "https://www.mouser.co.uk/ProductDetail/Analog-Devices/ADUM5000ARWZ?qs=sGAEpiMZZMtitjHzVIkrqYThiPHWFX9aYDreduI%2FIuI%3D" H 8575 3700 31  0001 C CNN "Distributor Link 2"
	1    8575 4400
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG028
U 1 1 6061F16B
P 10125 4150
F 0 "#FLG028" H 10125 4225 50  0001 C CNN
F 1 "PWR_FLAG" H 10125 4323 50  0000 C CNN
F 2 "" H 10125 4150 50  0001 C CNN
F 3 "~" H 10125 4150 50  0001 C CNN
	1    10125 4150
	-1   0    0    1   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:ISO_DGND #PWR?
U 1 1 6061F171
P 9500 4850
AR Path="/5EA5D9E8/6061F171" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/6061F171" Ref="#PWR0428"  Part="1" 
F 0 "#PWR0428" H 9500 4600 50  0001 C CNN
F 1 "ISO_DGND" H 9505 4677 50  0000 C CNN
F 2 "" H 9500 4850 50  0001 C CNN
F 3 "" H 9500 4850 50  0001 C CNN
	1    9500 4850
	1    0    0    -1  
$EndComp
Wire Notes Line
	7250 3300 7250 5925
Wire Notes Line
	10500 3300 10500 5925
Wire Wire Line
	9800 4050 10125 4050
Connection ~ 10125 4050
Wire Notes Line
	7250 3300 10500 3300
Wire Notes Line
	7250 5925 10500 5925
Wire Wire Line
	9500 4750 9500 4850
Wire Wire Line
	7975 4150 7975 4750
Text Notes 7350 3600 0    25   ~ 0
Separate isolated 5V supply for resolver-to-digital converter
Connection ~ 925  5475
Wire Wire Line
	925  5475 925  5525
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_DGND #PWR?
U 1 1 60352CB8
P 925 5525
AR Path="/5EA5D9E8/60352CB8" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/60352CB8" Ref="#PWR0392"  Part="1" 
F 0 "#PWR0392" H 925 5275 50  0001 C CNN
F 1 "CTRL_DGND" H 930 5352 50  0000 C CNN
F 2 "" H 925 5525 50  0001 C CNN
F 3 "" H 925 5525 50  0001 C CNN
	1    925  5525
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C202
U 1 1 5FF39BEC
P 2100 2500
F 0 "C202" H 2192 2591 50  0000 L CNN
F 1 "10pF" H 2192 2500 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 2100 2500 50  0001 C CNN
F 3 "~" H 2100 2500 50  0001 C CNN
F 4 "MC0402N100J500CT -  SMD Multilayer Ceramic Capacitor, 10 pF, 50 V, 0402 [1005 Metric], ± 5%, C0G / NP0, MC Series" H 2100 2100 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402n100j500ct/cap-10pf-50v-5-c0g-np0-0402/dp/2320761" H 2100 2000 50  0001 C CNN "Distributor Link"
F 6 "5%" H 2192 2409 50  0000 L CNN "Tolerance"
	1    2100 2500
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG?
U 1 1 6140E877
P 2500 1500
AR Path="/5EA5E06A/6140E877" Ref="#FLG?"  Part="1" 
AR Path="/5ED47D19/6140E877" Ref="#FLG018"  Part="1" 
F 0 "#FLG018" H 2500 1575 50  0001 C CNN
F 1 "PWR_FLAG" H 2500 1654 25  0000 C CNN
F 2 "" H 2500 1500 50  0001 C CNN
F 3 "~" H 2500 1500 50  0001 C CNN
	1    2500 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	2500 1500 2500 1550
Connection ~ 2500 1550
Wire Wire Line
	2500 1550 2250 1550
Wire Wire Line
	2500 1550 2700 1550
$Comp
L PMSM_Driver_LV_Board_Power:ISO_DI1_5V0 #PWR0509
U 1 1 602972C7
P 3475 3950
F 0 "#PWR0509" H 3475 4200 50  0001 C CNN
F 1 "ISO_DI1_5V0" H 3476 4123 50  0000 C CNN
F 2 "" H 3475 3950 50  0001 C CNN
F 3 "" H 3475 3950 50  0001 C CNN
	1    3475 3950
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:ISO_DI2_5V0 #PWR0510
U 1 1 60297FDB
P 6800 3950
F 0 "#PWR0510" H 6800 4200 50  0001 C CNN
F 1 "ISO_DI2_5V0" H 6801 4123 50  0000 C CNN
F 2 "" H 6800 3950 50  0001 C CNN
F 3 "" H 6800 3950 50  0001 C CNN
	1    6800 3950
	1    0    0    -1  
$EndComp
Connection ~ 8150 2100
$EndSCHEMATC
