EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 9 16
Title "PMSM_Driver_LV_Board"
Date "2020-03-30"
Rev "A"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L PMSM_Driver_LV_Board_IC:SE555 U?
U 1 1 5EA96A4F
P 1900 1825
AR Path="/5EA41912/5EA96A4F" Ref="U?"  Part="1" 
AR Path="/5EA90258/5EA96A4F" Ref="U?"  Part="1" 
AR Path="/5EA5E0CA/5EA96A4F" Ref="U29"  Part="1" 
F 0 "U29" H 1575 2125 50  0000 C CNN
F 1 "SE555" H 2175 1275 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:SOIC-8_3.9x4.9mm_P1.27mm" H 1900 775 31  0001 C CNN
F 3 "" H 2250 525 31  0001 C CNN
F 4 "SE555D -  Precision Timer IC, Timing From Microseconds to Hours, Astable, Monostable, 4.5 V to 18 V, SOIC-8 " H 1900 725 31  0001 C CNN "Description"
F 5 "https://uk.farnell.com/texas-instruments/se555d/ic-precision-timer-8soic/dp/3006911" H 1900 675 31  0001 C CNN "Distributor Link"
	1    1900 1825
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_PGND #PWR?
U 1 1 5EA96A55
P 1900 2525
AR Path="/5EA41912/5EA96A55" Ref="#PWR?"  Part="1" 
AR Path="/5EA90258/5EA96A55" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E0CA/5EA96A55" Ref="#PWR0237"  Part="1" 
F 0 "#PWR0237" H 1900 2275 50  0001 C CNN
F 1 "MOT_PGND" H 1905 2352 50  0000 C CNN
F 2 "" H 1900 2525 50  0001 C CNN
F 3 "" H 1900 2525 50  0001 C CNN
	1    1900 2525
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_15V0 #PWR?
U 1 1 5EA96A5B
P 1900 1275
AR Path="/5EA41912/5EA96A5B" Ref="#PWR?"  Part="1" 
AR Path="/5EA90258/5EA96A5B" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E0CA/5EA96A5B" Ref="#PWR0236"  Part="1" 
F 0 "#PWR0236" H 1900 1525 50  0001 C CNN
F 1 "MOT_15V0" H 1901 1448 50  0000 C CNN
F 2 "" H 1900 1275 50  0001 C CNN
F 3 "" H 1900 1275 50  0001 C CNN
	1    1900 1275
	1    0    0    -1  
$EndComp
Wire Wire Line
	1800 1375 1800 1475
Wire Wire Line
	2000 1375 2000 1475
Wire Wire Line
	1800 1375 1900 1375
Wire Wire Line
	1900 1275 1900 1375
Connection ~ 1900 1375
Wire Wire Line
	1900 1375 2000 1375
Wire Wire Line
	1900 2425 1900 2525
$Comp
L PMSM_Driver_LV_Board_Device:D_Zener_Small D?
U 1 1 5EA96A6A
P 2500 3100
AR Path="/5EA41912/5EA96A6A" Ref="D?"  Part="1" 
AR Path="/5EA90258/5EA96A6A" Ref="D?"  Part="1" 
AR Path="/5EA5E0CA/5EA96A6A" Ref="D18"  Part="1" 
F 0 "D18" V 2475 3225 50  0000 C CNN
F 1 "1N5352BRLG" V 2550 3400 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:D_DO-201_P15.24mm_Horizontal" V 2500 3100 50  0001 C CNN
F 3 "~" V 2500 3100 50  0001 C CNN
F 4 "1N5352BRLG -  Zener Single Diode, 15 V, 5 W, Axial Leaded, 5 %, 2 Pins, 200 °C" H 2500 2900 31  0001 C CNN "Description"
F 5 "https://uk.farnell.com/on-semiconductor/1n5352brlg/zener-diode-5w-15v-axial-leaded/dp/2533352" H 2500 2800 31  0001 C CNN "Distributor Link"
	1    2500 3100
	0    1    1    0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_PGND #PWR?
U 1 1 5EA96A70
P 2500 3300
AR Path="/5EA41912/5EA96A70" Ref="#PWR?"  Part="1" 
AR Path="/5EA90258/5EA96A70" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E0CA/5EA96A70" Ref="#PWR0240"  Part="1" 
F 0 "#PWR0240" H 2500 3050 50  0001 C CNN
F 1 "MOT_PGND" H 2505 3127 50  0000 C CNN
F 2 "" H 2500 3300 50  0001 C CNN
F 3 "" H 2500 3300 50  0001 C CNN
	1    2500 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	2500 3200 2500 3300
Wire Wire Line
	1300 2900 1300 3000
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 5EA96A7A
P 1900 2900
AR Path="/5F05E355/5EA96A7A" Ref="R?"  Part="1" 
AR Path="/6068445B/5EA96A7A" Ref="R?"  Part="1" 
AR Path="/60E816A3/5EA96A7A" Ref="R?"  Part="1" 
AR Path="/60EB5E32/5EA96A7A" Ref="R?"  Part="1" 
AR Path="/5EA41912/5EA96A7A" Ref="R?"  Part="1" 
AR Path="/5EA90258/5EA96A7A" Ref="R?"  Part="1" 
AR Path="/5EA5E0CA/5EA96A7A" Ref="R147"  Part="1" 
F 0 "R147" V 2000 2900 50  0000 C CNN
F 1 "18k" V 1775 2900 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 1900 2900 50  0001 C CNN
F 3 "~" H 1900 2900 50  0001 C CNN
F 4 "MCWR04X1802FTL -  SMD Chip Resistor, 0402 [1005 Metric], 18 kohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 1900 2500 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x1802ftl/res-18k-1-0-0625w-0402-thick-film/dp/2447118" H 1900 2400 50  0001 C CNN "Distributor Link"
	1    1900 2900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2500 2900 2500 3000
Wire Wire Line
	2000 2900 2500 2900
Wire Wire Line
	1300 3200 1300 3300
$Comp
L PMSM_Driver_LV_Board_Power:MOT_PGND #PWR?
U 1 1 5EA96A83
P 1300 3300
AR Path="/5EA41912/5EA96A83" Ref="#PWR?"  Part="1" 
AR Path="/5EA90258/5EA96A83" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E0CA/5EA96A83" Ref="#PWR0234"  Part="1" 
F 0 "#PWR0234" H 1300 3050 50  0001 C CNN
F 1 "MOT_PGND" H 1305 3127 50  0000 C CNN
F 2 "" H 1300 3300 50  0001 C CNN
F 3 "" H 1300 3300 50  0001 C CNN
	1    1300 3300
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5EA96A8B
P 1300 3100
AR Path="/5E9C6910/5EA96A8B" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5EA96A8B" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5EA96A8B" Ref="C?"  Part="1" 
AR Path="/5F05E355/5EA96A8B" Ref="C?"  Part="1" 
AR Path="/6068445B/5EA96A8B" Ref="C?"  Part="1" 
AR Path="/60EB5E32/5EA96A8B" Ref="C?"  Part="1" 
AR Path="/5EA41912/5EA96A8B" Ref="C?"  Part="1" 
AR Path="/5EA90258/5EA96A8B" Ref="C?"  Part="1" 
AR Path="/5EA5E0CA/5EA96A8B" Ref="C86"  Part="1" 
F 0 "C86" H 1375 3125 50  0000 L CNN
F 1 "1nF" H 1375 3050 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 1300 3100 50  0001 C CNN
F 3 "~" H 1300 3100 50  0001 C CNN
F 4 "0402B102K250CT -  SMD Multilayer Ceramic Capacitor, 1000 pF, 25 V, 0402 [1005 Metric], ± 10%, X7R, Walsin MLCC " H 1300 2700 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/walsin/0402b102k250ct/cap-1000pf-25v-10-x7r-0402/dp/2496766" H 1300 2600 50  0001 C CNN "Distributor Link"
	1    1300 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	1300 2900 1800 2900
Wire Wire Line
	2500 2900 2500 1825
Wire Wire Line
	2500 1825 2400 1825
Connection ~ 2500 2900
Wire Wire Line
	1300 2900 1300 2125
Wire Wire Line
	1300 2125 1400 2125
Connection ~ 1300 2900
Wire Wire Line
	1300 2125 1300 1825
Wire Wire Line
	1300 1825 1400 1825
Connection ~ 1300 2125
$Comp
L PMSM_Driver_LV_Board_Device:CP1_Small C?
U 1 1 5EA96A9D
P 2775 1825
AR Path="/5EA41912/5EA96A9D" Ref="C?"  Part="1" 
AR Path="/5EA90258/5EA96A9D" Ref="C?"  Part="1" 
AR Path="/5EA5E0CA/5EA96A9D" Ref="C90"  Part="1" 
F 0 "C90" V 2650 1825 50  0000 C CNN
F 1 "1uF" V 2875 1825 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:CP_Radial_D5.0mm_P2.00mm" H 2775 1825 50  0001 C CNN
F 3 "~" H 2775 1825 50  0001 C CNN
F 4 "MCGLR100V105M5X11 -  Electrolytic Capacitor, 1 µF, 100 V, MCGLR Series, ± 20%, Radial Leaded, 6 mm" H 2775 1425 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcglr100v105m5x11/cap-1-f-100v-20/dp/1903005" H 2775 1325 50  0001 C CNN "Distributor Link"
F 6 "100V" V 2950 1825 50  0000 C CNN "Voltage"
	1    2775 1825
	0    1    1    0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:D_Small D?
U 1 1 5EA96AA5
P 3175 1825
AR Path="/5EA41912/5EA96AA5" Ref="D?"  Part="1" 
AR Path="/5EA90258/5EA96AA5" Ref="D?"  Part="1" 
AR Path="/5EA5E0CA/5EA96AA5" Ref="D24"  Part="1" 
F 0 "D24" H 3175 1925 50  0000 C CNN
F 1 "ES1C" H 3175 2000 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:D_SMA" V 3175 1825 50  0001 C CNN
F 3 "~" V 3175 1825 50  0001 C CNN
F 4 "ES1C -  Fast / Ultrafast Diode, 150 V, 1 A, Single, 920 mV, 15 ns, 30 A" H 3175 1825 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/on-semiconductor/es1c/rect-ultrafast-1a-150v-do-214ac/dp/2453261" H 3175 1825 50  0001 C CNN "Distributor Link"
	1    3175 1825
	-1   0    0    1   
$EndComp
Wire Wire Line
	2875 1825 2975 1825
Wire Wire Line
	3275 1825 3375 1825
Wire Wire Line
	2975 1725 2975 1825
Connection ~ 2975 1825
Wire Wire Line
	2975 1825 3075 1825
$Comp
L PMSM_Driver_LV_Board_Device:CP1_Small C?
U 1 1 5EA96AB9
P 2975 1325
AR Path="/5EA41912/5EA96AB9" Ref="C?"  Part="1" 
AR Path="/5EA90258/5EA96AB9" Ref="C?"  Part="1" 
AR Path="/5EA5E0CA/5EA96AB9" Ref="C92"  Part="1" 
F 0 "C92" H 3125 1375 50  0000 C CNN
F 1 "22uF" H 3150 1300 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:CP_Radial_D8.0mm_P3.50mm" H 2975 1325 50  0001 C CNN
F 3 "~" H 2975 1325 50  0001 C CNN
F 4 "100RX3022M8X11.5 -  Electrolytic Capacitor, Miniature, 22 µF, 100 V, RX30 Series, ± 20%, Radial Leaded, 8 mm" H 2975 925 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/rubycon/100rx3022m8x11-5/cap-22-f-100v-20/dp/2342137" H 2975 825 50  0001 C CNN "Distributor Link"
F 6 "100V" H 3100 1225 50  0000 C CNN "Voltage"
	1    2975 1325
	1    0    0    -1  
$EndComp
Wire Wire Line
	2975 1225 2975 1125
Wire Wire Line
	2975 1125 3375 1125
Wire Wire Line
	3375 1125 3375 1825
Wire Wire Line
	3375 1125 3475 1125
Connection ~ 3375 1125
Text GLabel 3475 1125 2    50   Output ~ 0
MOT_CHARGE_PUMP_A
Text Notes 700  950  0    25   ~ 0
Similar circuit is described in Drive Techniques for High Side N-Channel MOSFETs by Motorola Semiconductor Products Inc.:\nhttps://archive.org/details/Motorola-SeminarsandApplicationBooksAR194DriveTechniquesForHighSideN-ChannelMOSFETsOCR/page/n1/mode/2up
Text Notes 675  800  0    100  ~ 0
Charge Pump for Phase A
Wire Notes Line
	4450 600  4450 3625
Wire Notes Line
	600  600  600  3625
Text Notes 1550 3350 0    25   ~ 0
555 timer is configured to operate \nas a simple oscillator at 35 kHz frequency.\nThe 1nF cap is charged and discharged\nthrough the same path - the 18k resistor\nand the output pin. Therefore, the duty\ncycle is 50%.
Text Notes 3500 1525 0    25   ~ 0
This output is approximately 15V\nabove MOT_OUT_A. Actually, it\nis rather 12V above it due to the\nvoltage drops accross the diodes.\nThe expected maximum current\ndraw is 4 mA. Thus, this should \nprovide enough current and keep\nthe voltage high enough.
Wire Wire Line
	1200 1375 1200 1425
Wire Wire Line
	1300 1375 1200 1375
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5EABB8DF
P 1400 1375
AR Path="/5E9C6910/5EABB8DF" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5EABB8DF" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5EABB8DF" Ref="C?"  Part="1" 
AR Path="/5F05E355/5EABB8DF" Ref="C?"  Part="1" 
AR Path="/60EB5E32/5EABB8DF" Ref="C?"  Part="1" 
AR Path="/5EA90258/5EABB8DF" Ref="C?"  Part="1" 
AR Path="/5EA5E0CA/5EABB8DF" Ref="C88"  Part="1" 
F 0 "C88" V 1525 1325 50  0000 L CNN
F 1 "100nF" V 1450 1425 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 1400 1375 50  0001 C CNN
F 3 "~" H 1400 1375 50  0001 C CNN
F 4 "MC0402X104K250CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 25 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 1400 975 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k250ct/cap-0-1-f-25v-10-x5r-0402/dp/2320760" H 1400 875 50  0001 C CNN "Distributor Link"
	1    1400 1375
	0    1    -1   0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_PGND #PWR?
U 1 1 5EABC4A9
P 1200 1425
AR Path="/5EA41912/5EABC4A9" Ref="#PWR?"  Part="1" 
AR Path="/5EA90258/5EABC4A9" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E0CA/5EABC4A9" Ref="#PWR0232"  Part="1" 
F 0 "#PWR0232" H 1200 1175 50  0001 C CNN
F 1 "MOT_PGND" H 1205 1252 50  0000 C CNN
F 2 "" H 1200 1425 50  0001 C CNN
F 3 "" H 1200 1425 50  0001 C CNN
	1    1200 1425
	1    0    0    -1  
$EndComp
Wire Wire Line
	1500 1375 1800 1375
Connection ~ 1800 1375
Wire Wire Line
	900  1975 900  2025
Wire Wire Line
	1000 1975 900  1975
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5EABE547
P 1100 1975
AR Path="/5E9C6910/5EABE547" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5EABE547" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5EABE547" Ref="C?"  Part="1" 
AR Path="/5F05E355/5EABE547" Ref="C?"  Part="1" 
AR Path="/60EB5E32/5EABE547" Ref="C?"  Part="1" 
AR Path="/5EA90258/5EABE547" Ref="C?"  Part="1" 
AR Path="/5EA5E0CA/5EABE547" Ref="C84"  Part="1" 
F 0 "C84" V 1225 1925 50  0000 L CNN
F 1 "10nF" V 975 1875 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 1100 1975 50  0001 C CNN
F 3 "~" H 1100 1975 50  0001 C CNN
F 4 "MC0402B103K250CT -  SMD Multilayer Ceramic Capacitor, 10000 pF, 25 V, 0402 [1005 Metric], ± 10%, X7R, MC Series" H 1100 1575 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402b103k250ct/cap-0-01-f-25v-10-x7r-0402/dp/1758924" H 1100 1475 50  0001 C CNN "Distributor Link"
	1    1100 1975
	0    1    -1   0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_PGND #PWR?
U 1 1 5EABE54D
P 900 2025
AR Path="/5EA41912/5EABE54D" Ref="#PWR?"  Part="1" 
AR Path="/5EA90258/5EABE54D" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E0CA/5EABE54D" Ref="#PWR0230"  Part="1" 
F 0 "#PWR0230" H 900 1775 50  0001 C CNN
F 1 "MOT_PGND" H 905 1852 50  0000 C CNN
F 2 "" H 900 2025 50  0001 C CNN
F 3 "" H 900 2025 50  0001 C CNN
	1    900  2025
	1    0    0    -1  
$EndComp
Wire Wire Line
	1200 1975 1400 1975
NoConn ~ 2400 2125
$Comp
L PMSM_Driver_LV_Board_Device:D_Small D?
U 1 1 5EACA530
P 2975 1625
AR Path="/5EA41912/5EACA530" Ref="D?"  Part="1" 
AR Path="/5EA90258/5EACA530" Ref="D?"  Part="1" 
AR Path="/5EA5E0CA/5EACA530" Ref="D22"  Part="1" 
F 0 "D22" V 3000 1500 50  0000 C CNN
F 1 "ES1C" V 2925 1475 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:D_SMA" V 2975 1625 50  0001 C CNN
F 3 "~" V 2975 1625 50  0001 C CNN
F 4 "ES1C -  Fast / Ultrafast Diode, 150 V, 1 A, Single, 920 mV, 15 ns, 30 A" H 2975 1625 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/on-semiconductor/es1c/rect-ultrafast-1a-150v-do-214ac/dp/2453261" H 2975 1625 50  0001 C CNN "Distributor Link"
	1    2975 1625
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2975 1425 2975 1475
$Comp
L PMSM_Driver_LV_Board_Device:D_Small D?
U 1 1 5EACC488
P 2775 1475
AR Path="/5EA41912/5EACC488" Ref="D?"  Part="1" 
AR Path="/5EA90258/5EACC488" Ref="D?"  Part="1" 
AR Path="/5EA5E0CA/5EACC488" Ref="D20"  Part="1" 
F 0 "D20" H 2775 1400 50  0000 C CNN
F 1 "ES1C" H 2775 1575 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:D_SMA" V 2775 1475 50  0001 C CNN
F 3 "~" V 2775 1475 50  0001 C CNN
F 4 "ES1C -  Fast / Ultrafast Diode, 150 V, 1 A, Single, 920 mV, 15 ns, 30 A" H 2775 1475 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/on-semiconductor/es1c/rect-ultrafast-1a-150v-do-214ac/dp/2453261" H 2775 1475 50  0001 C CNN "Distributor Link"
	1    2775 1475
	-1   0    0    1   
$EndComp
Wire Wire Line
	2875 1475 2975 1475
Connection ~ 2975 1475
Wire Wire Line
	2975 1475 2975 1525
Text GLabel 2575 1475 0    50   Input ~ 0
MOT_OUT_A
Wire Wire Line
	2675 1475 2575 1475
Wire Wire Line
	2675 1825 2500 1825
Connection ~ 2500 1825
Wire Notes Line
	600  600  4450 600 
Wire Notes Line
	600  3625 4450 3625
$Comp
L PMSM_Driver_LV_Board_IC:SE555 U?
U 1 1 5EAE368F
P 5825 1825
AR Path="/5EA41912/5EAE368F" Ref="U?"  Part="1" 
AR Path="/5EA90258/5EAE368F" Ref="U?"  Part="1" 
AR Path="/5EA5E0CA/5EAE368F" Ref="U31"  Part="1" 
F 0 "U31" H 5500 2125 50  0000 C CNN
F 1 "SE555" H 6100 1275 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:SOIC-8_3.9x4.9mm_P1.27mm" H 5825 775 31  0001 C CNN
F 3 "" H 6175 525 31  0001 C CNN
F 4 "SE555D -  Precision Timer IC, Timing From Microseconds to Hours, Astable, Monostable, 4.5 V to 18 V, SOIC-8 " H 5825 725 31  0001 C CNN "Description"
F 5 "https://uk.farnell.com/texas-instruments/se555d/ic-precision-timer-8soic/dp/3006911" H 5825 675 31  0001 C CNN "Distributor Link"
	1    5825 1825
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_PGND #PWR?
U 1 1 5EAE3695
P 5825 2525
AR Path="/5EA41912/5EAE3695" Ref="#PWR?"  Part="1" 
AR Path="/5EA90258/5EAE3695" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E0CA/5EAE3695" Ref="#PWR0246"  Part="1" 
F 0 "#PWR0246" H 5825 2275 50  0001 C CNN
F 1 "MOT_PGND" H 5830 2352 50  0000 C CNN
F 2 "" H 5825 2525 50  0001 C CNN
F 3 "" H 5825 2525 50  0001 C CNN
	1    5825 2525
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_15V0 #PWR?
U 1 1 5EAE369B
P 5825 1275
AR Path="/5EA41912/5EAE369B" Ref="#PWR?"  Part="1" 
AR Path="/5EA90258/5EAE369B" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E0CA/5EAE369B" Ref="#PWR0245"  Part="1" 
F 0 "#PWR0245" H 5825 1525 50  0001 C CNN
F 1 "MOT_15V0" H 5826 1448 50  0000 C CNN
F 2 "" H 5825 1275 50  0001 C CNN
F 3 "" H 5825 1275 50  0001 C CNN
	1    5825 1275
	1    0    0    -1  
$EndComp
Wire Wire Line
	5725 1375 5725 1475
Wire Wire Line
	5925 1375 5925 1475
Wire Wire Line
	5725 1375 5825 1375
Wire Wire Line
	5825 1275 5825 1375
Connection ~ 5825 1375
Wire Wire Line
	5825 1375 5925 1375
Wire Wire Line
	5825 2425 5825 2525
$Comp
L PMSM_Driver_LV_Board_Device:D_Zener_Small D?
U 1 1 5EAE36AA
P 6425 3100
AR Path="/5EA41912/5EAE36AA" Ref="D?"  Part="1" 
AR Path="/5EA90258/5EAE36AA" Ref="D?"  Part="1" 
AR Path="/5EA5E0CA/5EAE36AA" Ref="D26"  Part="1" 
F 0 "D26" V 6400 3225 50  0000 C CNN
F 1 "1N5352BRLG" V 6475 3400 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:D_DO-201_P15.24mm_Horizontal" V 6425 3100 50  0001 C CNN
F 3 "~" V 6425 3100 50  0001 C CNN
F 4 "1N5352BRLG -  Zener Single Diode, 15 V, 5 W, Axial Leaded, 5 %, 2 Pins, 200 °C" H 6425 2900 31  0001 C CNN "Description"
F 5 "https://uk.farnell.com/on-semiconductor/1n5352brlg/zener-diode-5w-15v-axial-leaded/dp/2533352" H 6425 2800 31  0001 C CNN "Distributor Link"
	1    6425 3100
	0    1    1    0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_PGND #PWR?
U 1 1 5EAE36B0
P 6425 3300
AR Path="/5EA41912/5EAE36B0" Ref="#PWR?"  Part="1" 
AR Path="/5EA90258/5EAE36B0" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E0CA/5EAE36B0" Ref="#PWR0247"  Part="1" 
F 0 "#PWR0247" H 6425 3050 50  0001 C CNN
F 1 "MOT_PGND" H 6430 3127 50  0000 C CNN
F 2 "" H 6425 3300 50  0001 C CNN
F 3 "" H 6425 3300 50  0001 C CNN
	1    6425 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	6425 3200 6425 3300
Wire Wire Line
	5225 2900 5225 3000
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 5EAE36BA
P 5825 2900
AR Path="/5F05E355/5EAE36BA" Ref="R?"  Part="1" 
AR Path="/6068445B/5EAE36BA" Ref="R?"  Part="1" 
AR Path="/60E816A3/5EAE36BA" Ref="R?"  Part="1" 
AR Path="/60EB5E32/5EAE36BA" Ref="R?"  Part="1" 
AR Path="/5EA41912/5EAE36BA" Ref="R?"  Part="1" 
AR Path="/5EA90258/5EAE36BA" Ref="R?"  Part="1" 
AR Path="/5EA5E0CA/5EAE36BA" Ref="R149"  Part="1" 
F 0 "R149" V 5925 2900 50  0000 C CNN
F 1 "18k" V 5700 2900 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 5825 2900 50  0001 C CNN
F 3 "~" H 5825 2900 50  0001 C CNN
F 4 "MCWR04X1802FTL -  SMD Chip Resistor, 0402 [1005 Metric], 18 kohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 5825 2500 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x1802ftl/res-18k-1-0-0625w-0402-thick-film/dp/2447118" H 5825 2400 50  0001 C CNN "Distributor Link"
	1    5825 2900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6425 2900 6425 3000
Wire Wire Line
	5925 2900 6425 2900
Wire Wire Line
	5225 3200 5225 3300
$Comp
L PMSM_Driver_LV_Board_Power:MOT_PGND #PWR?
U 1 1 5EAE36C3
P 5225 3300
AR Path="/5EA41912/5EAE36C3" Ref="#PWR?"  Part="1" 
AR Path="/5EA90258/5EAE36C3" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E0CA/5EAE36C3" Ref="#PWR0244"  Part="1" 
F 0 "#PWR0244" H 5225 3050 50  0001 C CNN
F 1 "MOT_PGND" H 5230 3127 50  0000 C CNN
F 2 "" H 5225 3300 50  0001 C CNN
F 3 "" H 5225 3300 50  0001 C CNN
	1    5225 3300
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5EAE36CB
P 5225 3100
AR Path="/5E9C6910/5EAE36CB" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5EAE36CB" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5EAE36CB" Ref="C?"  Part="1" 
AR Path="/5F05E355/5EAE36CB" Ref="C?"  Part="1" 
AR Path="/6068445B/5EAE36CB" Ref="C?"  Part="1" 
AR Path="/60EB5E32/5EAE36CB" Ref="C?"  Part="1" 
AR Path="/5EA41912/5EAE36CB" Ref="C?"  Part="1" 
AR Path="/5EA90258/5EAE36CB" Ref="C?"  Part="1" 
AR Path="/5EA5E0CA/5EAE36CB" Ref="C95"  Part="1" 
F 0 "C95" H 5300 3125 50  0000 L CNN
F 1 "1nF" H 5300 3050 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 5225 3100 50  0001 C CNN
F 3 "~" H 5225 3100 50  0001 C CNN
F 4 "0402B102K250CT -  SMD Multilayer Ceramic Capacitor, 1000 pF, 25 V, 0402 [1005 Metric], ± 10%, X7R, Walsin MLCC " H 5225 2700 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/walsin/0402b102k250ct/cap-1000pf-25v-10-x7r-0402/dp/2496766" H 5225 2600 50  0001 C CNN "Distributor Link"
	1    5225 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	5225 2900 5725 2900
Wire Wire Line
	6425 2900 6425 1825
Wire Wire Line
	6425 1825 6325 1825
Connection ~ 6425 2900
Wire Wire Line
	5225 2900 5225 2125
Wire Wire Line
	5225 2125 5325 2125
Connection ~ 5225 2900
Wire Wire Line
	5225 2125 5225 1825
Wire Wire Line
	5225 1825 5325 1825
Connection ~ 5225 2125
$Comp
L PMSM_Driver_LV_Board_Device:CP1_Small C?
U 1 1 5EAE36DE
P 6700 1825
AR Path="/5EA41912/5EAE36DE" Ref="C?"  Part="1" 
AR Path="/5EA90258/5EAE36DE" Ref="C?"  Part="1" 
AR Path="/5EA5E0CA/5EAE36DE" Ref="C97"  Part="1" 
F 0 "C97" V 6575 1825 50  0000 C CNN
F 1 "1uF" V 6800 1825 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:CP_Radial_D5.0mm_P2.00mm" H 6700 1825 50  0001 C CNN
F 3 "~" H 6700 1825 50  0001 C CNN
F 4 "MCGLR100V105M5X11 -  Electrolytic Capacitor, 1 µF, 100 V, MCGLR Series, ± 20%, Radial Leaded, 6 mm" H 6700 1425 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcglr100v105m5x11/cap-1-f-100v-20/dp/1903005" H 6700 1325 50  0001 C CNN "Distributor Link"
F 6 "100V" V 6875 1825 50  0000 C CNN "Voltage"
	1    6700 1825
	0    1    1    0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:D_Small D?
U 1 1 5EAE36E6
P 7100 1825
AR Path="/5EA41912/5EAE36E6" Ref="D?"  Part="1" 
AR Path="/5EA90258/5EAE36E6" Ref="D?"  Part="1" 
AR Path="/5EA5E0CA/5EAE36E6" Ref="D29"  Part="1" 
F 0 "D29" H 7100 1925 50  0000 C CNN
F 1 "ES1C" H 7100 2000 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:D_SMA" V 7100 1825 50  0001 C CNN
F 3 "~" V 7100 1825 50  0001 C CNN
F 4 "ES1C -  Fast / Ultrafast Diode, 150 V, 1 A, Single, 920 mV, 15 ns, 30 A" H 7100 1825 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/on-semiconductor/es1c/rect-ultrafast-1a-150v-do-214ac/dp/2453261" H 7100 1825 50  0001 C CNN "Distributor Link"
	1    7100 1825
	-1   0    0    1   
$EndComp
Wire Wire Line
	6800 1825 6900 1825
Wire Wire Line
	7200 1825 7300 1825
Wire Wire Line
	6900 1725 6900 1825
Connection ~ 6900 1825
Wire Wire Line
	6900 1825 7000 1825
$Comp
L PMSM_Driver_LV_Board_Device:CP1_Small C?
U 1 1 5EAE36F4
P 6900 1325
AR Path="/5EA41912/5EAE36F4" Ref="C?"  Part="1" 
AR Path="/5EA90258/5EAE36F4" Ref="C?"  Part="1" 
AR Path="/5EA5E0CA/5EAE36F4" Ref="C98"  Part="1" 
F 0 "C98" H 7050 1375 50  0000 C CNN
F 1 "22uF" H 7075 1300 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:CP_Radial_D8.0mm_P3.50mm" H 6900 1325 50  0001 C CNN
F 3 "~" H 6900 1325 50  0001 C CNN
F 4 "100RX3022M8X11.5 -  Electrolytic Capacitor, Miniature, 22 µF, 100 V, RX30 Series, ± 20%, Radial Leaded, 8 mm" H 6900 925 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/rubycon/100rx3022m8x11-5/cap-22-f-100v-20/dp/2342137" H 6900 825 50  0001 C CNN "Distributor Link"
F 6 "100V" H 7025 1225 50  0000 C CNN "Voltage"
	1    6900 1325
	1    0    0    -1  
$EndComp
Wire Wire Line
	6900 1225 6900 1125
Wire Wire Line
	6900 1125 7300 1125
Wire Wire Line
	7300 1125 7300 1825
Wire Wire Line
	7300 1125 7400 1125
Connection ~ 7300 1125
Text GLabel 7400 1125 2    50   Output ~ 0
MOT_CHARGE_PUMP_C
Text Notes 4600 800  0    100  ~ 0
Charge Pump for Phase C
Wire Notes Line
	8375 600  8375 3625
Wire Notes Line
	4525 600  4525 3625
Wire Wire Line
	5125 1375 5125 1425
Wire Wire Line
	5225 1375 5125 1375
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5EAE370A
P 5325 1375
AR Path="/5E9C6910/5EAE370A" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5EAE370A" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5EAE370A" Ref="C?"  Part="1" 
AR Path="/5F05E355/5EAE370A" Ref="C?"  Part="1" 
AR Path="/60EB5E32/5EAE370A" Ref="C?"  Part="1" 
AR Path="/5EA90258/5EAE370A" Ref="C?"  Part="1" 
AR Path="/5EA5E0CA/5EAE370A" Ref="C96"  Part="1" 
F 0 "C96" V 5450 1325 50  0000 L CNN
F 1 "100nF" V 5375 1425 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 5325 1375 50  0001 C CNN
F 3 "~" H 5325 1375 50  0001 C CNN
F 4 "MC0402X104K250CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 25 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 5325 975 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k250ct/cap-0-1-f-25v-10-x5r-0402/dp/2320760" H 5325 875 50  0001 C CNN "Distributor Link"
	1    5325 1375
	0    1    -1   0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_PGND #PWR?
U 1 1 5EAE3710
P 5125 1425
AR Path="/5EA41912/5EAE3710" Ref="#PWR?"  Part="1" 
AR Path="/5EA90258/5EAE3710" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E0CA/5EAE3710" Ref="#PWR0243"  Part="1" 
F 0 "#PWR0243" H 5125 1175 50  0001 C CNN
F 1 "MOT_PGND" H 5130 1252 50  0000 C CNN
F 2 "" H 5125 1425 50  0001 C CNN
F 3 "" H 5125 1425 50  0001 C CNN
	1    5125 1425
	1    0    0    -1  
$EndComp
Wire Wire Line
	5425 1375 5725 1375
Connection ~ 5725 1375
Wire Wire Line
	4825 1975 4825 2025
Wire Wire Line
	4925 1975 4825 1975
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5EAE371C
P 5025 1975
AR Path="/5E9C6910/5EAE371C" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5EAE371C" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5EAE371C" Ref="C?"  Part="1" 
AR Path="/5F05E355/5EAE371C" Ref="C?"  Part="1" 
AR Path="/60EB5E32/5EAE371C" Ref="C?"  Part="1" 
AR Path="/5EA90258/5EAE371C" Ref="C?"  Part="1" 
AR Path="/5EA5E0CA/5EAE371C" Ref="C94"  Part="1" 
F 0 "C94" V 5150 1925 50  0000 L CNN
F 1 "10nF" V 4900 1875 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 5025 1975 50  0001 C CNN
F 3 "~" H 5025 1975 50  0001 C CNN
F 4 "MC0402B103K250CT -  SMD Multilayer Ceramic Capacitor, 10000 pF, 25 V, 0402 [1005 Metric], ± 10%, X7R, MC Series" H 5025 1575 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402b103k250ct/cap-0-01-f-25v-10-x7r-0402/dp/1758924" H 5025 1475 50  0001 C CNN "Distributor Link"
	1    5025 1975
	0    1    -1   0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_PGND #PWR?
U 1 1 5EAE3722
P 4825 2025
AR Path="/5EA41912/5EAE3722" Ref="#PWR?"  Part="1" 
AR Path="/5EA90258/5EAE3722" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E0CA/5EAE3722" Ref="#PWR0242"  Part="1" 
F 0 "#PWR0242" H 4825 1775 50  0001 C CNN
F 1 "MOT_PGND" H 4830 1852 50  0000 C CNN
F 2 "" H 4825 2025 50  0001 C CNN
F 3 "" H 4825 2025 50  0001 C CNN
	1    4825 2025
	1    0    0    -1  
$EndComp
Wire Wire Line
	5125 1975 5325 1975
NoConn ~ 6325 2125
$Comp
L PMSM_Driver_LV_Board_Device:D_Small D?
U 1 1 5EAE372C
P 6900 1625
AR Path="/5EA41912/5EAE372C" Ref="D?"  Part="1" 
AR Path="/5EA90258/5EAE372C" Ref="D?"  Part="1" 
AR Path="/5EA5E0CA/5EAE372C" Ref="D28"  Part="1" 
F 0 "D28" V 6925 1500 50  0000 C CNN
F 1 "ES1C" V 6850 1475 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:D_SMA" V 6900 1625 50  0001 C CNN
F 3 "~" V 6900 1625 50  0001 C CNN
F 4 "ES1C -  Fast / Ultrafast Diode, 150 V, 1 A, Single, 920 mV, 15 ns, 30 A" H 6900 1625 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/on-semiconductor/es1c/rect-ultrafast-1a-150v-do-214ac/dp/2453261" H 6900 1625 50  0001 C CNN "Distributor Link"
	1    6900 1625
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6900 1425 6900 1475
$Comp
L PMSM_Driver_LV_Board_Device:D_Small D?
U 1 1 5EAE3735
P 6700 1475
AR Path="/5EA41912/5EAE3735" Ref="D?"  Part="1" 
AR Path="/5EA90258/5EAE3735" Ref="D?"  Part="1" 
AR Path="/5EA5E0CA/5EAE3735" Ref="D27"  Part="1" 
F 0 "D27" H 6700 1400 50  0000 C CNN
F 1 "ES1C" H 6700 1575 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:D_SMA" V 6700 1475 50  0001 C CNN
F 3 "~" V 6700 1475 50  0001 C CNN
F 4 "ES1C -  Fast / Ultrafast Diode, 150 V, 1 A, Single, 920 mV, 15 ns, 30 A" H 6700 1475 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/on-semiconductor/es1c/rect-ultrafast-1a-150v-do-214ac/dp/2453261" H 6700 1475 50  0001 C CNN "Distributor Link"
	1    6700 1475
	-1   0    0    1   
$EndComp
Wire Wire Line
	6800 1475 6900 1475
Connection ~ 6900 1475
Wire Wire Line
	6900 1475 6900 1525
Text GLabel 6500 1475 0    50   Input ~ 0
MOT_OUT_C
Wire Wire Line
	6600 1475 6500 1475
Wire Wire Line
	6600 1825 6425 1825
Connection ~ 6425 1825
Wire Notes Line
	4525 600  8375 600 
Wire Notes Line
	4525 3625 8375 3625
$Comp
L PMSM_Driver_LV_Board_IC:SE555 U?
U 1 1 5EAF1F18
P 1900 4925
AR Path="/5EA41912/5EAF1F18" Ref="U?"  Part="1" 
AR Path="/5EA90258/5EAF1F18" Ref="U?"  Part="1" 
AR Path="/5EA5E0CA/5EAF1F18" Ref="U30"  Part="1" 
F 0 "U30" H 1575 5225 50  0000 C CNN
F 1 "SE555" H 2175 4375 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:SOIC-8_3.9x4.9mm_P1.27mm" H 1900 3875 31  0001 C CNN
F 3 "" H 2250 3625 31  0001 C CNN
F 4 "SE555D -  Precision Timer IC, Timing From Microseconds to Hours, Astable, Monostable, 4.5 V to 18 V, SOIC-8 " H 1900 3825 31  0001 C CNN "Description"
F 5 "https://uk.farnell.com/texas-instruments/se555d/ic-precision-timer-8soic/dp/3006911" H 1900 3775 31  0001 C CNN "Distributor Link"
	1    1900 4925
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_PGND #PWR?
U 1 1 5EAF1F1E
P 1900 5625
AR Path="/5EA41912/5EAF1F1E" Ref="#PWR?"  Part="1" 
AR Path="/5EA90258/5EAF1F1E" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E0CA/5EAF1F1E" Ref="#PWR0239"  Part="1" 
F 0 "#PWR0239" H 1900 5375 50  0001 C CNN
F 1 "MOT_PGND" H 1905 5452 50  0000 C CNN
F 2 "" H 1900 5625 50  0001 C CNN
F 3 "" H 1900 5625 50  0001 C CNN
	1    1900 5625
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_15V0 #PWR?
U 1 1 5EAF1F24
P 1900 4375
AR Path="/5EA41912/5EAF1F24" Ref="#PWR?"  Part="1" 
AR Path="/5EA90258/5EAF1F24" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E0CA/5EAF1F24" Ref="#PWR0238"  Part="1" 
F 0 "#PWR0238" H 1900 4625 50  0001 C CNN
F 1 "MOT_15V0" H 1901 4548 50  0000 C CNN
F 2 "" H 1900 4375 50  0001 C CNN
F 3 "" H 1900 4375 50  0001 C CNN
	1    1900 4375
	1    0    0    -1  
$EndComp
Wire Wire Line
	1800 4475 1800 4575
Wire Wire Line
	2000 4475 2000 4575
Wire Wire Line
	1800 4475 1900 4475
Wire Wire Line
	1900 4375 1900 4475
Connection ~ 1900 4475
Wire Wire Line
	1900 4475 2000 4475
Wire Wire Line
	1900 5525 1900 5625
$Comp
L PMSM_Driver_LV_Board_Device:D_Zener_Small D?
U 1 1 5EAF1F33
P 2500 6200
AR Path="/5EA41912/5EAF1F33" Ref="D?"  Part="1" 
AR Path="/5EA90258/5EAF1F33" Ref="D?"  Part="1" 
AR Path="/5EA5E0CA/5EAF1F33" Ref="D19"  Part="1" 
F 0 "D19" V 2475 6325 50  0000 C CNN
F 1 "1N5352BRLG" V 2550 6500 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:D_DO-201_P15.24mm_Horizontal" V 2500 6200 50  0001 C CNN
F 3 "~" V 2500 6200 50  0001 C CNN
F 4 "1N5352BRLG -  Zener Single Diode, 15 V, 5 W, Axial Leaded, 5 %, 2 Pins, 200 °C" H 2500 6000 31  0001 C CNN "Description"
F 5 "https://uk.farnell.com/on-semiconductor/1n5352brlg/zener-diode-5w-15v-axial-leaded/dp/2533352" H 2500 5900 31  0001 C CNN "Distributor Link"
	1    2500 6200
	0    1    1    0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_PGND #PWR?
U 1 1 5EAF1F39
P 2500 6400
AR Path="/5EA41912/5EAF1F39" Ref="#PWR?"  Part="1" 
AR Path="/5EA90258/5EAF1F39" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E0CA/5EAF1F39" Ref="#PWR0241"  Part="1" 
F 0 "#PWR0241" H 2500 6150 50  0001 C CNN
F 1 "MOT_PGND" H 2505 6227 50  0000 C CNN
F 2 "" H 2500 6400 50  0001 C CNN
F 3 "" H 2500 6400 50  0001 C CNN
	1    2500 6400
	1    0    0    -1  
$EndComp
Wire Wire Line
	2500 6300 2500 6400
Wire Wire Line
	1300 6000 1300 6100
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 5EAF1F43
P 1900 6000
AR Path="/5F05E355/5EAF1F43" Ref="R?"  Part="1" 
AR Path="/6068445B/5EAF1F43" Ref="R?"  Part="1" 
AR Path="/60E816A3/5EAF1F43" Ref="R?"  Part="1" 
AR Path="/60EB5E32/5EAF1F43" Ref="R?"  Part="1" 
AR Path="/5EA41912/5EAF1F43" Ref="R?"  Part="1" 
AR Path="/5EA90258/5EAF1F43" Ref="R?"  Part="1" 
AR Path="/5EA5E0CA/5EAF1F43" Ref="R148"  Part="1" 
F 0 "R148" V 2000 6000 50  0000 C CNN
F 1 "18k" V 1775 6000 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 1900 6000 50  0001 C CNN
F 3 "~" H 1900 6000 50  0001 C CNN
F 4 "MCWR04X1802FTL -  SMD Chip Resistor, 0402 [1005 Metric], 18 kohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 1900 5600 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x1802ftl/res-18k-1-0-0625w-0402-thick-film/dp/2447118" H 1900 5500 50  0001 C CNN "Distributor Link"
	1    1900 6000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2500 6000 2500 6100
Wire Wire Line
	2000 6000 2500 6000
Wire Wire Line
	1300 6300 1300 6400
$Comp
L PMSM_Driver_LV_Board_Power:MOT_PGND #PWR?
U 1 1 5EAF1F4C
P 1300 6400
AR Path="/5EA41912/5EAF1F4C" Ref="#PWR?"  Part="1" 
AR Path="/5EA90258/5EAF1F4C" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E0CA/5EAF1F4C" Ref="#PWR0235"  Part="1" 
F 0 "#PWR0235" H 1300 6150 50  0001 C CNN
F 1 "MOT_PGND" H 1305 6227 50  0000 C CNN
F 2 "" H 1300 6400 50  0001 C CNN
F 3 "" H 1300 6400 50  0001 C CNN
	1    1300 6400
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5EAF1F54
P 1300 6200
AR Path="/5E9C6910/5EAF1F54" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5EAF1F54" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5EAF1F54" Ref="C?"  Part="1" 
AR Path="/5F05E355/5EAF1F54" Ref="C?"  Part="1" 
AR Path="/6068445B/5EAF1F54" Ref="C?"  Part="1" 
AR Path="/60EB5E32/5EAF1F54" Ref="C?"  Part="1" 
AR Path="/5EA41912/5EAF1F54" Ref="C?"  Part="1" 
AR Path="/5EA90258/5EAF1F54" Ref="C?"  Part="1" 
AR Path="/5EA5E0CA/5EAF1F54" Ref="C87"  Part="1" 
F 0 "C87" H 1375 6225 50  0000 L CNN
F 1 "1nF" H 1375 6150 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 1300 6200 50  0001 C CNN
F 3 "~" H 1300 6200 50  0001 C CNN
F 4 "0402B102K250CT -  SMD Multilayer Ceramic Capacitor, 1000 pF, 25 V, 0402 [1005 Metric], ± 10%, X7R, Walsin MLCC " H 1300 5800 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/walsin/0402b102k250ct/cap-1000pf-25v-10-x7r-0402/dp/2496766" H 1300 5700 50  0001 C CNN "Distributor Link"
	1    1300 6200
	1    0    0    -1  
$EndComp
Wire Wire Line
	1300 6000 1800 6000
Wire Wire Line
	2500 6000 2500 4925
Wire Wire Line
	2500 4925 2400 4925
Connection ~ 2500 6000
Wire Wire Line
	1300 6000 1300 5225
Wire Wire Line
	1300 5225 1400 5225
Connection ~ 1300 6000
Wire Wire Line
	1300 5225 1300 4925
Wire Wire Line
	1300 4925 1400 4925
Connection ~ 1300 5225
$Comp
L PMSM_Driver_LV_Board_Device:CP1_Small C?
U 1 1 5EAF1F67
P 2775 4925
AR Path="/5EA41912/5EAF1F67" Ref="C?"  Part="1" 
AR Path="/5EA90258/5EAF1F67" Ref="C?"  Part="1" 
AR Path="/5EA5E0CA/5EAF1F67" Ref="C91"  Part="1" 
F 0 "C91" V 2650 4925 50  0000 C CNN
F 1 "1uF" V 2875 4925 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:CP_Radial_D5.0mm_P2.00mm" H 2775 4925 50  0001 C CNN
F 3 "~" H 2775 4925 50  0001 C CNN
F 4 "MCGLR100V105M5X11 -  Electrolytic Capacitor, 1 µF, 100 V, MCGLR Series, ± 20%, Radial Leaded, 6 mm" H 2775 4525 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcglr100v105m5x11/cap-1-f-100v-20/dp/1903005" H 2775 4425 50  0001 C CNN "Distributor Link"
F 6 "100V" V 2950 4925 50  0000 C CNN "Voltage"
	1    2775 4925
	0    1    1    0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:D_Small D?
U 1 1 5EAF1F6F
P 3175 4925
AR Path="/5EA41912/5EAF1F6F" Ref="D?"  Part="1" 
AR Path="/5EA90258/5EAF1F6F" Ref="D?"  Part="1" 
AR Path="/5EA5E0CA/5EAF1F6F" Ref="D25"  Part="1" 
F 0 "D25" H 3175 5025 50  0000 C CNN
F 1 "ES1C" H 3175 5100 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:D_SMA" V 3175 4925 50  0001 C CNN
F 3 "~" V 3175 4925 50  0001 C CNN
F 4 "ES1C -  Fast / Ultrafast Diode, 150 V, 1 A, Single, 920 mV, 15 ns, 30 A" H 3175 4925 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/on-semiconductor/es1c/rect-ultrafast-1a-150v-do-214ac/dp/2453261" H 3175 4925 50  0001 C CNN "Distributor Link"
	1    3175 4925
	-1   0    0    1   
$EndComp
Wire Wire Line
	2875 4925 2975 4925
Wire Wire Line
	3275 4925 3375 4925
Wire Wire Line
	2975 4825 2975 4925
Connection ~ 2975 4925
Wire Wire Line
	2975 4925 3075 4925
$Comp
L PMSM_Driver_LV_Board_Device:CP1_Small C?
U 1 1 5EAF1F7D
P 2975 4425
AR Path="/5EA41912/5EAF1F7D" Ref="C?"  Part="1" 
AR Path="/5EA90258/5EAF1F7D" Ref="C?"  Part="1" 
AR Path="/5EA5E0CA/5EAF1F7D" Ref="C93"  Part="1" 
F 0 "C93" H 3125 4475 50  0000 C CNN
F 1 "22uF" H 3150 4400 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:CP_Radial_D8.0mm_P3.50mm" H 2975 4425 50  0001 C CNN
F 3 "~" H 2975 4425 50  0001 C CNN
F 4 "100RX3022M8X11.5 -  Electrolytic Capacitor, Miniature, 22 µF, 100 V, RX30 Series, ± 20%, Radial Leaded, 8 mm" H 2975 4025 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/rubycon/100rx3022m8x11-5/cap-22-f-100v-20/dp/2342137" H 2975 3925 50  0001 C CNN "Distributor Link"
F 6 "100V" H 3100 4325 50  0000 C CNN "Voltage"
	1    2975 4425
	1    0    0    -1  
$EndComp
Wire Wire Line
	2975 4325 2975 4225
Wire Wire Line
	2975 4225 3375 4225
Wire Wire Line
	3375 4225 3375 4925
Wire Wire Line
	3375 4225 3475 4225
Connection ~ 3375 4225
Text GLabel 3475 4225 2    50   Output ~ 0
MOT_CHARGE_PUMP_B
Text Notes 675  3900 0    100  ~ 0
Charge Pump for Phase B
Wire Notes Line
	4450 3700 4450 6725
Wire Notes Line
	600  3700 600  6725
Wire Wire Line
	1200 4475 1200 4525
Wire Wire Line
	1300 4475 1200 4475
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5EAF1F93
P 1400 4475
AR Path="/5E9C6910/5EAF1F93" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5EAF1F93" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5EAF1F93" Ref="C?"  Part="1" 
AR Path="/5F05E355/5EAF1F93" Ref="C?"  Part="1" 
AR Path="/60EB5E32/5EAF1F93" Ref="C?"  Part="1" 
AR Path="/5EA90258/5EAF1F93" Ref="C?"  Part="1" 
AR Path="/5EA5E0CA/5EAF1F93" Ref="C89"  Part="1" 
F 0 "C89" V 1525 4425 50  0000 L CNN
F 1 "100nF" V 1450 4525 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 1400 4475 50  0001 C CNN
F 3 "~" H 1400 4475 50  0001 C CNN
F 4 "MC0402X104K250CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 25 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 1400 4075 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k250ct/cap-0-1-f-25v-10-x5r-0402/dp/2320760" H 1400 3975 50  0001 C CNN "Distributor Link"
	1    1400 4475
	0    1    -1   0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_PGND #PWR?
U 1 1 5EAF1F99
P 1200 4525
AR Path="/5EA41912/5EAF1F99" Ref="#PWR?"  Part="1" 
AR Path="/5EA90258/5EAF1F99" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E0CA/5EAF1F99" Ref="#PWR0233"  Part="1" 
F 0 "#PWR0233" H 1200 4275 50  0001 C CNN
F 1 "MOT_PGND" H 1205 4352 50  0000 C CNN
F 2 "" H 1200 4525 50  0001 C CNN
F 3 "" H 1200 4525 50  0001 C CNN
	1    1200 4525
	1    0    0    -1  
$EndComp
Wire Wire Line
	1500 4475 1800 4475
Connection ~ 1800 4475
Wire Wire Line
	900  5075 900  5125
Wire Wire Line
	1000 5075 900  5075
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5EAF1FA5
P 1100 5075
AR Path="/5E9C6910/5EAF1FA5" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5EAF1FA5" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5EAF1FA5" Ref="C?"  Part="1" 
AR Path="/5F05E355/5EAF1FA5" Ref="C?"  Part="1" 
AR Path="/60EB5E32/5EAF1FA5" Ref="C?"  Part="1" 
AR Path="/5EA90258/5EAF1FA5" Ref="C?"  Part="1" 
AR Path="/5EA5E0CA/5EAF1FA5" Ref="C85"  Part="1" 
F 0 "C85" V 1225 5025 50  0000 L CNN
F 1 "10nF" V 975 4975 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 1100 5075 50  0001 C CNN
F 3 "~" H 1100 5075 50  0001 C CNN
F 4 "MC0402B103K250CT -  SMD Multilayer Ceramic Capacitor, 10000 pF, 25 V, 0402 [1005 Metric], ± 10%, X7R, MC Series" H 1100 4675 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402b103k250ct/cap-0-01-f-25v-10-x7r-0402/dp/1758924" H 1100 4575 50  0001 C CNN "Distributor Link"
	1    1100 5075
	0    1    -1   0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_PGND #PWR?
U 1 1 5EAF1FAB
P 900 5125
AR Path="/5EA41912/5EAF1FAB" Ref="#PWR?"  Part="1" 
AR Path="/5EA90258/5EAF1FAB" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E0CA/5EAF1FAB" Ref="#PWR0231"  Part="1" 
F 0 "#PWR0231" H 900 4875 50  0001 C CNN
F 1 "MOT_PGND" H 905 4952 50  0000 C CNN
F 2 "" H 900 5125 50  0001 C CNN
F 3 "" H 900 5125 50  0001 C CNN
	1    900  5125
	1    0    0    -1  
$EndComp
Wire Wire Line
	1200 5075 1400 5075
NoConn ~ 2400 5225
$Comp
L PMSM_Driver_LV_Board_Device:D_Small D?
U 1 1 5EAF1FB5
P 2975 4725
AR Path="/5EA41912/5EAF1FB5" Ref="D?"  Part="1" 
AR Path="/5EA90258/5EAF1FB5" Ref="D?"  Part="1" 
AR Path="/5EA5E0CA/5EAF1FB5" Ref="D23"  Part="1" 
F 0 "D23" V 3000 4600 50  0000 C CNN
F 1 "ES1C" V 2925 4575 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:D_SMA" V 2975 4725 50  0001 C CNN
F 3 "~" V 2975 4725 50  0001 C CNN
F 4 "ES1C -  Fast / Ultrafast Diode, 150 V, 1 A, Single, 920 mV, 15 ns, 30 A" H 2975 4725 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/on-semiconductor/es1c/rect-ultrafast-1a-150v-do-214ac/dp/2453261" H 2975 4725 50  0001 C CNN "Distributor Link"
	1    2975 4725
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2975 4525 2975 4575
$Comp
L PMSM_Driver_LV_Board_Device:D_Small D?
U 1 1 5EAF1FBE
P 2775 4575
AR Path="/5EA41912/5EAF1FBE" Ref="D?"  Part="1" 
AR Path="/5EA90258/5EAF1FBE" Ref="D?"  Part="1" 
AR Path="/5EA5E0CA/5EAF1FBE" Ref="D21"  Part="1" 
F 0 "D21" H 2775 4500 50  0000 C CNN
F 1 "ES1C" H 2775 4675 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:D_SMA" V 2775 4575 50  0001 C CNN
F 3 "~" V 2775 4575 50  0001 C CNN
F 4 "ES1C -  Fast / Ultrafast Diode, 150 V, 1 A, Single, 920 mV, 15 ns, 30 A" H 2775 4575 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/on-semiconductor/es1c/rect-ultrafast-1a-150v-do-214ac/dp/2453261" H 2775 4575 50  0001 C CNN "Distributor Link"
	1    2775 4575
	-1   0    0    1   
$EndComp
Wire Wire Line
	2875 4575 2975 4575
Connection ~ 2975 4575
Wire Wire Line
	2975 4575 2975 4625
Text GLabel 2575 4575 0    50   Input ~ 0
MOT_OUT_B
Wire Wire Line
	2675 4575 2575 4575
Wire Wire Line
	2675 4925 2500 4925
Connection ~ 2500 4925
Wire Notes Line
	600  3700 4450 3700
Wire Notes Line
	600  6725 4450 6725
$EndSCHEMATC
