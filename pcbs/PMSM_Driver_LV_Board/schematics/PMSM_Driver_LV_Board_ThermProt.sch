EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 13 16
Title "PMSM_Driver_LV_Board"
Date "2020-03-30"
Rev "A"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L PMSM_Driver_LV_Board_IC:AD8468WBKSZ U?
U 1 1 6237D242
P 2725 1700
AR Path="/5FD7CF82/6237D242" Ref="U?"  Part="1" 
AR Path="/62375798/6237D242" Ref="U?"  Part="1" 
AR Path="/5EA5E358/6237D242" Ref="U50"  Part="1" 
F 0 "U50" H 2825 1875 50  0000 L CNN
F 1 "AD8468WBKSZ" H 2875 1600 25  0000 L CNN
F 2 "PMSM_Driver_LV_Board:SC70-6" H 2725 1050 31  0001 C CNN
F 3 "~" H 2175 2300 50  0001 C CNN
F 4 "AD8468WBKSZ -  Analogue Comparator, Rail to Rail, Low Power, 1 Comparator, 60 ns, 2.5V to 5.5V, SC-70, 6 Pins" H 2725 1000 31  0001 C CNN "Description"
F 5 "https://uk.farnell.com/analog-devices/ad8468wbksz/ic-comparator-ttl-cmos-sc70-6/dp/1864677" H 2725 950 31  0001 C CNN "Distributor Link"
F 6 "https://www.mouser.co.uk/ProductDetail/Analog-Devices/AD8468WBKSZ-R7?qs=sGAEpiMZZMuayl%2FEk2kXcXratI4i45FvTyGpHTQUrPU%3D" H 2775 900 31  0001 C CNN "Distributor Link 2"
	1    2725 1700
	1    0    0    -1  
$EndComp
Wire Wire Line
	2725 2100 2725 2000
Wire Wire Line
	2825 2000 2825 2100
Wire Wire Line
	3225 2100 3225 2050
Wire Wire Line
	2825 2100 3225 2100
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 6237D25A
P 1225 2150
AR Path="/5F05E355/6237D25A" Ref="R?"  Part="1" 
AR Path="/6068445B/6237D25A" Ref="R?"  Part="1" 
AR Path="/60E816A3/6237D25A" Ref="R?"  Part="1" 
AR Path="/60EB5E32/6237D25A" Ref="R?"  Part="1" 
AR Path="/5FD7CF82/6237D25A" Ref="R?"  Part="1" 
AR Path="/62375798/6237D25A" Ref="R?"  Part="1" 
AR Path="/5EA5E358/6237D25A" Ref="R194"  Part="1" 
F 0 "R194" H 1075 2150 50  0000 C CNN
F 1 "10k" H 1125 2225 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 1225 2150 50  0001 C CNN
F 3 "~" H 1225 2150 50  0001 C CNN
F 4 "WF06R1002BTL -  SMD Chip Resistor, 0603 [1608 Metric], 10 kohm, WF06R Series, 75 V, Thin Film, 100 mW" H 1225 1750 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/walsin/wf06r1002btl/res-10k-0-1-75v-0603-thin-film/dp/2502926" H 1225 1650 50  0001 C CNN "Distributor Link"
	1    1225 2150
	-1   0    0    1   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 6237D263
P 1500 2450
AR Path="/5E9C6910/6237D263" Ref="C?"  Part="1" 
AR Path="/5EBC6654/6237D263" Ref="C?"  Part="1" 
AR Path="/5EBC681B/6237D263" Ref="C?"  Part="1" 
AR Path="/5F05E355/6237D263" Ref="C?"  Part="1" 
AR Path="/5EA41912/6237D263" Ref="C?"  Part="1" 
AR Path="/5FD7CF82/6237D263" Ref="C?"  Part="1" 
AR Path="/62375798/6237D263" Ref="C?"  Part="1" 
AR Path="/5EA5E358/6237D263" Ref="C192"  Part="1" 
F 0 "C192" H 1525 2375 50  0000 L CNN
F 1 "1uF" H 1525 2525 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0805_2012Metric" H 1500 2450 50  0001 C CNN
F 3 "~" H 1500 2450 50  0001 C CNN
F 4 "08051C105K4T2A -  SMD Multilayer Ceramic Capacitor, 1 µF, 100 V, 0805 [2012 Metric], ± 10%, X7R" H 1500 2050 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/avx/08051c105k4t2a/cap-1-f-100v-10-x7r-0805/dp/2834935" H 1500 1950 50  0001 C CNN "Distributor Link"
	1    1500 2450
	1    0    0    1   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 6237D26B
P 1225 2450
AR Path="/5F05E355/6237D26B" Ref="R?"  Part="1" 
AR Path="/6068445B/6237D26B" Ref="R?"  Part="1" 
AR Path="/60E816A3/6237D26B" Ref="R?"  Part="1" 
AR Path="/60EB5E32/6237D26B" Ref="R?"  Part="1" 
AR Path="/5FD7CF82/6237D26B" Ref="R?"  Part="1" 
AR Path="/62375798/6237D26B" Ref="R?"  Part="1" 
AR Path="/5EA5E358/6237D26B" Ref="R195"  Part="1" 
F 0 "R195" H 1100 2375 50  0000 C CNN
F 1 "1k" H 1150 2525 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 1225 2450 50  0001 C CNN
F 3 "~" H 1225 2450 50  0001 C CNN
F 4 "MCWR04X1001FTL -  SMD Chip Resistor, 0402 [1005 Metric], 1 kohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 1225 2050 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x1001ftl/res-1k-1-0-0625w-thick-film/dp/2447120" H 1225 1950 50  0001 C CNN "Distributor Link"
	1    1225 2450
	-1   0    0    1   
$EndComp
Wire Wire Line
	1225 2350 1225 2300
Wire Wire Line
	1225 2300 1500 2300
Wire Wire Line
	1500 2300 1500 2350
Connection ~ 1225 2300
Wire Wire Line
	1225 2300 1225 2250
Wire Wire Line
	1225 2550 1225 2600
Wire Wire Line
	1500 2550 1500 2600
Wire Wire Line
	1500 2600 1225 2600
Connection ~ 1225 2600
Wire Wire Line
	1225 2600 1225 2650
Wire Wire Line
	1225 2000 1225 2050
Wire Wire Line
	3125 1700 3175 1700
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 6237D295
P 3075 1125
AR Path="/5E9C6910/6237D295" Ref="C?"  Part="1" 
AR Path="/5EBC6654/6237D295" Ref="C?"  Part="1" 
AR Path="/5EBC681B/6237D295" Ref="C?"  Part="1" 
AR Path="/5F05E355/6237D295" Ref="C?"  Part="1" 
AR Path="/5EA41912/6237D295" Ref="C?"  Part="1" 
AR Path="/5FD7CF82/6237D295" Ref="C?"  Part="1" 
AR Path="/62375798/6237D295" Ref="C?"  Part="1" 
AR Path="/5EA5E358/6237D295" Ref="C193"  Part="1" 
F 0 "C193" V 3125 900 50  0000 L CNN
F 1 "1uF" V 3125 1150 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0805_2012Metric" H 3075 1125 50  0001 C CNN
F 3 "~" H 3075 1125 50  0001 C CNN
F 4 "08051C105K4T2A -  SMD Multilayer Ceramic Capacitor, 1 µF, 100 V, 0805 [2012 Metric], ± 10%, X7R" H 3075 725 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/avx/08051c105k4t2a/cap-1-f-100v-10-x7r-0805/dp/2834935" H 3075 625 50  0001 C CNN "Distributor Link"
	1    3075 1125
	0    1    -1   0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 6237D29D
P 3075 1350
AR Path="/5E9C6910/6237D29D" Ref="C?"  Part="1" 
AR Path="/5EBC6654/6237D29D" Ref="C?"  Part="1" 
AR Path="/5EBC681B/6237D29D" Ref="C?"  Part="1" 
AR Path="/5F05E355/6237D29D" Ref="C?"  Part="1" 
AR Path="/5FD7CF82/6237D29D" Ref="C?"  Part="1" 
AR Path="/62375798/6237D29D" Ref="C?"  Part="1" 
AR Path="/5EA5E358/6237D29D" Ref="C194"  Part="1" 
F 0 "C194" V 3125 1375 50  0000 L CNN
F 1 "100nF" V 3125 1075 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 3075 1350 50  0001 C CNN
F 3 "~" H 3075 1350 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 3075 950 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 3075 850 50  0001 C CNN "Distributor Link"
	1    3075 1350
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2725 1075 2725 1125
Wire Wire Line
	2975 1125 2725 1125
Connection ~ 2725 1125
Wire Wire Line
	2725 1125 2725 1350
Wire Wire Line
	2975 1350 2725 1350
Connection ~ 2725 1350
Wire Wire Line
	2725 1350 2725 1400
Wire Wire Line
	3175 1350 3400 1350
Wire Wire Line
	3400 1350 3400 1400
Wire Wire Line
	3175 1125 3400 1125
Wire Wire Line
	3400 1125 3400 1350
Connection ~ 3400 1350
Text Notes 675  800  0    100  ~ 0
Detect Overheating
Wire Notes Line
	600  600  3875 600 
Wire Notes Line
	600  2975 3875 2975
Text GLabel 3175 1700 2    50   Output ~ 0
ISO_TEMP
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR?
U 1 1 6237F61D
P 2725 2100
AR Path="/5EA41912/6237F61D" Ref="#PWR?"  Part="1" 
AR Path="/5FD7CF82/6237F61D" Ref="#PWR?"  Part="1" 
AR Path="/62375798/6237F61D" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E358/6237F61D" Ref="#PWR0388"  Part="1" 
F 0 "#PWR0388" H 2725 1850 50  0001 C CNN
F 1 "MOT_DGND" H 2730 1927 50  0000 C CNN
F 2 "" H 2725 2100 50  0001 C CNN
F 3 "" H 2725 2100 50  0001 C CNN
	1    2725 2100
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR?
U 1 1 6238069B
P 3400 1400
AR Path="/5EA41912/6238069B" Ref="#PWR?"  Part="1" 
AR Path="/5FD7CF82/6238069B" Ref="#PWR?"  Part="1" 
AR Path="/62375798/6238069B" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E358/6238069B" Ref="#PWR0390"  Part="1" 
F 0 "#PWR0390" H 3400 1150 50  0001 C CNN
F 1 "MOT_DGND" H 3405 1227 50  0000 C CNN
F 2 "" H 3400 1400 50  0001 C CNN
F 3 "" H 3400 1400 50  0001 C CNN
	1    3400 1400
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR?
U 1 1 62380E30
P 1225 2650
AR Path="/5EA41912/62380E30" Ref="#PWR?"  Part="1" 
AR Path="/5FD7CF82/62380E30" Ref="#PWR?"  Part="1" 
AR Path="/62375798/62380E30" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E358/62380E30" Ref="#PWR0384"  Part="1" 
F 0 "#PWR0384" H 1225 2400 50  0001 C CNN
F 1 "MOT_DGND" H 1230 2477 50  0000 C CNN
F 2 "" H 1225 2650 50  0001 C CNN
F 3 "" H 1225 2650 50  0001 C CNN
	1    1225 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	1825 1600 1875 1600
Wire Wire Line
	1825 1700 1875 1700
Text GLabel 1825 1700 0    50   Output ~ 0
MOT_THERMAL_PROTECTION-
Text GLabel 1825 1600 0    50   Output ~ 0
MOT_THERMAL_PROTECTION+
Wire Wire Line
	1875 1300 1875 1350
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR?
U 1 1 62556649
P 1875 2000
AR Path="/5EA41912/62556649" Ref="#PWR?"  Part="1" 
AR Path="/5FD7CF82/62556649" Ref="#PWR?"  Part="1" 
AR Path="/62375798/62556649" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E358/62556649" Ref="#PWR0386"  Part="1" 
F 0 "#PWR0386" H 1875 1750 50  0001 C CNN
F 1 "MOT_DGND" H 1880 1827 50  0000 C CNN
F 2 "" H 1875 2000 50  0001 C CNN
F 3 "" H 1875 2000 50  0001 C CNN
	1    1875 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	1875 1950 1875 2000
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 62557834
P 1875 1450
AR Path="/5F05E355/62557834" Ref="R?"  Part="1" 
AR Path="/6068445B/62557834" Ref="R?"  Part="1" 
AR Path="/60E816A3/62557834" Ref="R?"  Part="1" 
AR Path="/60EB5E32/62557834" Ref="R?"  Part="1" 
AR Path="/5FD7CF82/62557834" Ref="R?"  Part="1" 
AR Path="/62375798/62557834" Ref="R?"  Part="1" 
AR Path="/5EA5E358/62557834" Ref="R196"  Part="1" 
F 0 "R196" H 1725 1450 50  0000 C CNN
F 1 "10k" H 1775 1525 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 1875 1450 50  0001 C CNN
F 3 "~" H 1875 1450 50  0001 C CNN
F 4 "WF06R1002BTL -  SMD Chip Resistor, 0603 [1608 Metric], 10 kohm, WF06R Series, 75 V, Thin Film, 100 mW" H 1875 1050 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/walsin/wf06r1002btl/res-10k-0-1-75v-0603-thin-film/dp/2502926" H 1875 950 50  0001 C CNN "Distributor Link"
	1    1875 1450
	-1   0    0    1   
$EndComp
Wire Wire Line
	1875 1600 1875 1550
Wire Wire Line
	1875 1600 2425 1600
Connection ~ 1875 1600
Wire Wire Line
	1500 2300 2325 2300
Wire Wire Line
	2325 2300 2325 1800
Wire Wire Line
	2325 1800 2425 1800
Connection ~ 1500 2300
Wire Notes Line
	600  600  600  2975
Wire Notes Line
	3875 600  3875 2975
Text Notes 700  950  0    25   ~ 0
This can be used with a thermal protector acting as normally closed\nthermal switch or with a thermistor. Rise of resistance above 1kOhm\nis detected.
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R?
U 1 1 60973CF8
P 1875 1850
AR Path="/5F05E355/60973CF8" Ref="R?"  Part="1" 
AR Path="/6068445B/60973CF8" Ref="R?"  Part="1" 
AR Path="/60E816A3/60973CF8" Ref="R?"  Part="1" 
AR Path="/60EB5E32/60973CF8" Ref="R?"  Part="1" 
AR Path="/5FD7CF82/60973CF8" Ref="R?"  Part="1" 
AR Path="/62375798/60973CF8" Ref="R?"  Part="1" 
AR Path="/5EA5E358/60973CF8" Ref="R236"  Part="1" 
F 0 "R236" H 1725 1850 50  0000 C CNN
F 1 "0" H 1775 1925 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 1875 1850 50  0001 C CNN
F 3 "~" H 1875 1850 50  0001 C CNN
F 4 "..." H 1875 1450 50  0001 C CNN "Description"
F 5 "..." H 1875 1350 50  0001 C CNN "Distributor Link"
	1    1875 1850
	-1   0    0    1   
$EndComp
Wire Wire Line
	1875 1750 1875 1700
Text Notes 1575 1900 0    25   ~ 0
Connect with\nsolder blob.
$Comp
L PMSM_Driver_LV_Board_Power:ISO_SENS_5V0 #PWR?
U 1 1 5F6D4D22
P 2725 1075
AR Path="/6068445B/5F6D4D22" Ref="#PWR?"  Part="1" 
AR Path="/60EB5E32/5F6D4D22" Ref="#PWR?"  Part="1" 
AR Path="/5EA5DFF7/5F6D4D22" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E358/5F6D4D22" Ref="#PWR0337"  Part="1" 
F 0 "#PWR0337" H 2725 1325 50  0001 C CNN
F 1 "ISO_SENS_5V0" H 2726 1248 50  0000 C CNN
F 2 "" H 2725 1075 50  0001 C CNN
F 3 "" H 2725 1075 50  0001 C CNN
	1    2725 1075
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:ISO_SENS_5V0 #PWR?
U 1 1 5F6D53BE
P 1875 1300
AR Path="/6068445B/5F6D53BE" Ref="#PWR?"  Part="1" 
AR Path="/60EB5E32/5F6D53BE" Ref="#PWR?"  Part="1" 
AR Path="/5EA5DFF7/5F6D53BE" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E358/5F6D53BE" Ref="#PWR0383"  Part="1" 
F 0 "#PWR0383" H 1875 1550 50  0001 C CNN
F 1 "ISO_SENS_5V0" H 1876 1473 50  0000 C CNN
F 2 "" H 1875 1300 50  0001 C CNN
F 3 "" H 1875 1300 50  0001 C CNN
	1    1875 1300
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:ISO_SENS_5V0 #PWR?
U 1 1 5F6D62E2
P 1225 2000
AR Path="/6068445B/5F6D62E2" Ref="#PWR?"  Part="1" 
AR Path="/60EB5E32/5F6D62E2" Ref="#PWR?"  Part="1" 
AR Path="/5EA5DFF7/5F6D62E2" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E358/5F6D62E2" Ref="#PWR0385"  Part="1" 
F 0 "#PWR0385" H 1225 2250 50  0001 C CNN
F 1 "ISO_SENS_5V0" H 1226 2173 50  0000 C CNN
F 2 "" H 1225 2000 50  0001 C CNN
F 3 "" H 1225 2000 50  0001 C CNN
	1    1225 2000
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:ISO_SENS_5V0 #PWR?
U 1 1 5F6D9553
P 3225 2050
AR Path="/6068445B/5F6D9553" Ref="#PWR?"  Part="1" 
AR Path="/60EB5E32/5F6D9553" Ref="#PWR?"  Part="1" 
AR Path="/5EA5DFF7/5F6D9553" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E358/5F6D9553" Ref="#PWR0387"  Part="1" 
F 0 "#PWR0387" H 3225 2300 50  0001 C CNN
F 1 "ISO_SENS_5V0" H 3226 2223 50  0000 C CNN
F 2 "" H 3225 2050 50  0001 C CNN
F 3 "" H 3225 2050 50  0001 C CNN
	1    3225 2050
	1    0    0    -1  
$EndComp
$EndSCHEMATC
