EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 15 16
Title "PMSM_Driver_LV_Board"
Date "2020-03-30"
Rev "A"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L PMSM_Driver_LV_Board_Power:MOT_5V0 #PWR?
U 1 1 60A94E63
P 10000 4750
AR Path="/5EFE605D/60A94E63" Ref="#PWR?"  Part="1" 
AR Path="/5EFE7331/60A94E63" Ref="#PWR?"  Part="1" 
AR Path="/5EA5E1C2/60A94E63" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/60A94E63" Ref="#PWR?"  Part="1" 
AR Path="/5FE92F3B/60A94E63" Ref="#PWR0467"  Part="1" 
F 0 "#PWR0467" H 10000 5000 50  0001 C CNN
F 1 "MOT_5V0" H 10001 4923 50  0000 C CNN
F 2 "" H 10000 4750 50  0001 C CNN
F 3 "" H 10000 4750 50  0001 C CNN
	1    10000 4750
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG?
U 1 1 60A94E6F
P 9600 4750
AR Path="/5ED47D19/60A94E6F" Ref="#FLG?"  Part="1" 
AR Path="/5FE92F3B/60A94E6F" Ref="#FLG031"  Part="1" 
F 0 "#FLG031" H 9600 4825 50  0001 C CNN
F 1 "PWR_FLAG" H 9600 4923 50  0000 C CNN
F 2 "" H 9600 4750 50  0001 C CNN
F 3 "~" H 9600 4750 50  0001 C CNN
	1    9600 4750
	-1   0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_15V0 #PWR?
U 1 1 60A94E87
P 7225 1725
AR Path="/5EA5E06A/60A94E87" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/60A94E87" Ref="#PWR?"  Part="1" 
AR Path="/5FE92F3B/60A94E87" Ref="#PWR0461"  Part="1" 
F 0 "#PWR0461" H 7225 1975 50  0001 C CNN
F 1 "MOT_15V0" H 7226 1898 50  0000 C CNN
F 2 "" H 7225 1725 50  0001 C CNN
F 3 "" H 7225 1725 50  0001 C CNN
	1    7225 1725
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG?
U 1 1 60A94E8D
P 6775 1725
AR Path="/5ED47D19/60A94E8D" Ref="#FLG?"  Part="1" 
AR Path="/5FE92F3B/60A94E8D" Ref="#FLG030"  Part="1" 
F 0 "#FLG030" H 6775 1800 50  0001 C CNN
F 1 "PWR_FLAG" H 6775 1898 50  0000 C CNN
F 2 "" H 6775 1725 50  0001 C CNN
F 3 "~" H 6775 1725 50  0001 C CNN
	1    6775 1725
	-1   0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_VIN_HDR #PWR?
U 1 1 614D973F
P 975 1125
AR Path="/5EA5E295/614D973F" Ref="#PWR?"  Part="1" 
AR Path="/5FE92F3B/614D973F" Ref="#PWR0430"  Part="1" 
F 0 "#PWR0430" H 975 1375 50  0001 C CNN
F 1 "MOT_VIN_HDR" H 976 1298 50  0000 C CNN
F 2 "" H 975 1125 50  0001 C CNN
F 3 "" H 975 1125 50  0001 C CNN
	1    975  1125
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:D_Schottky_Small D33
U 1 1 614D9971
P 1175 1225
F 0 "D33" H 1175 1125 50  0000 C CNN
F 1 "MBRS190T3G" H 1175 1325 25  0000 C CNN
F 2 "PMSM_Driver_LV_Board:D_SMB" V 1175 1225 50  0001 C CNN
F 3 "~" V 1175 1225 50  0001 C CNN
F 4 "MBRS190T3G -  Schottky Rectifier, General Purpose, 90 V, 1 A, Single, DO-214AA (SMB), 2 Pins, 750 mV" H 1175 1025 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/on-semiconductor/mbrs190t3g/diode-schottky-1a-smb/dp/1459071" H 1175 925 50  0001 C CNN "Distributor Link"
	1    1175 1225
	-1   0    0    1   
$EndComp
Wire Wire Line
	975  1125 975  1225
Wire Wire Line
	975  1225 1075 1225
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C234
U 1 1 614DBCE7
P 1375 1425
F 0 "C234" H 1425 1500 50  0000 L CNN
F 1 "2.2uF" H 1450 1425 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 1375 1425 50  0001 C CNN
F 3 "~" H 1375 1425 50  0001 C CNN
F 4 "C3216X7S2A225K160AB -  SMD Multilayer Ceramic Capacitor, 2.2 µF, 100 V, 1206 [3216 Metric], ± 10%, X7S, C Series TDK " H 1375 1025 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/tdk/c3216x7s2a225k160ab/cap-2-2-f-100v-10-x7s-1206/dp/2112717" H 1375 925 50  0001 C CNN "Distributor Link"
F 6 "100V" H 1400 1350 50  0000 L CNN "Voltage"
F 7 "DNI" H 1400 1275 50  0000 L CNN "DNI"
	1    1375 1425
	1    0    0    -1  
$EndComp
Wire Wire Line
	1375 1325 1375 1225
Wire Wire Line
	1375 1225 1275 1225
$Comp
L PMSM_Driver_LV_Board_Power:MOT_PGND #PWR?
U 1 1 614E017C
P 1375 1625
AR Path="/5EA5D9E8/614E017C" Ref="#PWR?"  Part="1" 
AR Path="/5FE92F3B/614E017C" Ref="#PWR0432"  Part="1" 
F 0 "#PWR0432" H 1375 1375 50  0001 C CNN
F 1 "MOT_PGND" H 1380 1452 50  0000 C CNN
F 2 "" H 1375 1625 50  0001 C CNN
F 3 "" H 1375 1625 50  0001 C CNN
	1    1375 1625
	1    0    0    -1  
$EndComp
Wire Wire Line
	1375 1525 1375 1625
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C236
U 1 1 614E1069
P 1825 1425
F 0 "C236" H 1875 1500 50  0000 L CNN
F 1 "2.2uF" H 1900 1425 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 1825 1425 50  0001 C CNN
F 3 "~" H 1825 1425 50  0001 C CNN
F 4 "C3216X7S2A225K160AB -  SMD Multilayer Ceramic Capacitor, 2.2 µF, 100 V, 1206 [3216 Metric], ± 10%, X7S, C Series TDK " H 1825 1025 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/tdk/c3216x7s2a225k160ab/cap-2-2-f-100v-10-x7s-1206/dp/2112717" H 1825 925 50  0001 C CNN "Distributor Link"
F 6 "100V" H 1850 1350 50  0000 L CNN "Voltage"
	1    1825 1425
	1    0    0    -1  
$EndComp
Wire Wire Line
	1825 1325 1825 1225
$Comp
L PMSM_Driver_LV_Board_Power:MOT_PGND #PWR?
U 1 1 614E1071
P 1825 1625
AR Path="/5EA5D9E8/614E1071" Ref="#PWR?"  Part="1" 
AR Path="/5FE92F3B/614E1071" Ref="#PWR0435"  Part="1" 
F 0 "#PWR0435" H 1825 1375 50  0001 C CNN
F 1 "MOT_PGND" H 1830 1452 50  0000 C CNN
F 2 "" H 1825 1625 50  0001 C CNN
F 3 "" H 1825 1625 50  0001 C CNN
	1    1825 1625
	1    0    0    -1  
$EndComp
Wire Wire Line
	1825 1525 1825 1625
Wire Wire Line
	1375 1225 1825 1225
Connection ~ 1375 1225
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C238
U 1 1 614E1F43
P 2275 1425
F 0 "C238" H 2325 1500 50  0000 L CNN
F 1 "2.2uF" H 2350 1425 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 2275 1425 50  0001 C CNN
F 3 "~" H 2275 1425 50  0001 C CNN
F 4 "C3216X7S2A225K160AB -  SMD Multilayer Ceramic Capacitor, 2.2 µF, 100 V, 1206 [3216 Metric], ± 10%, X7S, C Series TDK " H 2275 1025 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/tdk/c3216x7s2a225k160ab/cap-2-2-f-100v-10-x7s-1206/dp/2112717" H 2275 925 50  0001 C CNN "Distributor Link"
F 6 "100V" H 2300 1350 50  0000 L CNN "Voltage"
	1    2275 1425
	1    0    0    -1  
$EndComp
Wire Wire Line
	2275 1325 2275 1225
$Comp
L PMSM_Driver_LV_Board_Power:MOT_PGND #PWR?
U 1 1 614E1F4A
P 2275 1625
AR Path="/5EA5D9E8/614E1F4A" Ref="#PWR?"  Part="1" 
AR Path="/5FE92F3B/614E1F4A" Ref="#PWR0437"  Part="1" 
F 0 "#PWR0437" H 2275 1375 50  0001 C CNN
F 1 "MOT_PGND" H 2280 1452 50  0000 C CNN
F 2 "" H 2275 1625 50  0001 C CNN
F 3 "" H 2275 1625 50  0001 C CNN
	1    2275 1625
	1    0    0    -1  
$EndComp
Wire Wire Line
	2275 1525 2275 1625
Wire Wire Line
	1825 1225 2275 1225
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R203
U 1 1 614E206F
P 2725 1425
F 0 "R203" H 2793 1471 50  0000 L CNN
F 1 "56k2" H 2793 1380 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2725 1425 50  0001 C CNN
F 3 "~" H 2725 1425 50  0001 C CNN
F 4 "MCWR08X5622FTL -  SMD Chip Resistor, 0805 [2012 Metric], 56.2 kohm, MCWR Series, 150 V, Thick Film, 125 mW" H 2725 1025 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr08x5622ftl/res-56k2-1-0-125w-thick-film/dp/2447688" H 2725 925 50  0001 C CNN "Distributor Link"
	1    2725 1425
	1    0    0    -1  
$EndComp
Wire Wire Line
	2725 1325 2725 1225
Wire Wire Line
	2275 1225 2725 1225
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R205
U 1 1 614E4FC4
P 3300 1425
F 0 "R205" H 3368 1471 50  0000 L CNN
F 1 "750" H 3368 1380 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 3300 1425 50  0001 C CNN
F 3 "~" H 3300 1425 50  0001 C CNN
F 4 "MCWR08X7500FTL -  SMD Chip Resistor, 0805 [2012 Metric], 750 ohm, MCWR Series, 150 V, Thick Film, 125 mW" H 3300 1025 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr08x7500ftl/res-750r-1-0-125w-thick-film/dp/2447719" H 3300 925 50  0001 C CNN "Distributor Link"
	1    3300 1425
	1    0    0    -1  
$EndComp
Wire Wire Line
	3300 1325 3300 1225
Connection ~ 1825 1225
Connection ~ 2275 1225
Connection ~ 2725 1225
$Comp
L PMSM_Driver_LV_Board_Device:D_Zener_Small D34
U 1 1 614E930B
P 2725 1925
F 0 "D34" V 2699 1993 50  0000 L CNN
F 1 "DDZ9688-7" V 2771 1993 25  0000 L CNN
F 2 "PMSM_Driver_LV_Board:D_SOD-123" V 2725 1925 50  0001 C CNN
F 3 "~" V 2725 1925 50  0001 C CNN
F 4 "DDZ9688-7 - Zener diode, 4.7V" H 2725 1725 31  0001 C CNN "Description"
F 5 "https://www.mouser.co.uk/ProductDetail/Diodes-Incorporated/DDZ9688-7?qs=%2Fha2pyFadugfY8qYd5of2W3cOOVcraA71otjfs7nlnE%3D" H 2725 1625 31  0001 C CNN "Distributor Link"
	1    2725 1925
	0    1    1    0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:Q_NPN_BEC Q16
U 1 1 614E628C
P 3200 1775
F 0 "Q16" H 3391 1801 50  0000 L CNN
F 1 "MMBTA06-7-F" H 3391 1729 25  0000 L CNN
F 2 "PMSM_Driver_LV_Board:SOT-23" H 3400 1875 50  0001 C CNN
F 3 "~" H 3200 1775 50  0001 C CNN
F 4 "MMBTA06-7-F -  Bipolar (BJT) Single Transistor, NPN, 80 V, 100 MHz, 300 mW, 500 mA, 100 hFE" H 3200 1775 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/diodes-inc/mmbta06-7-f/transistor-npn-sot23/dp/1773631" H 3200 1775 50  0001 C CNN "Distributor Link"
	1    3200 1775
	1    0    0    -1  
$EndComp
Wire Wire Line
	3300 1575 3300 1525
Wire Wire Line
	2725 1525 2725 1775
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR?
U 1 1 614EBA27
P 2725 2075
AR Path="/5EA5E06A/614EBA27" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/614EBA27" Ref="#PWR?"  Part="1" 
AR Path="/5FE92F3B/614EBA27" Ref="#PWR0440"  Part="1" 
F 0 "#PWR0440" H 2725 1825 50  0001 C CNN
F 1 "MOT_DGND" H 2730 1902 50  0000 C CNN
F 2 "" H 2725 2075 50  0001 C CNN
F 3 "" H 2725 2075 50  0001 C CNN
	1    2725 2075
	1    0    0    -1  
$EndComp
Wire Wire Line
	2725 1775 2725 1825
Connection ~ 2725 1775
Wire Wire Line
	2725 2025 2725 2075
$Comp
L PMSM_Driver_LV_Board_IC:ADP1621ARMZ U56
U 1 1 614EE5B7
P 3200 2975
F 0 "U56" H 2850 3375 50  0000 L CNN
F 1 "ADP1621ARMZ" H 3375 2550 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:MSOP-10_3x3mm_P0.5mm" H 3200 1275 50  0001 C CNN
F 3 "~" H 2650 3525 50  0001 C CNN
F 4 "ADP1621ARMZ-R7 -  DC/DC Controller, Boost, Step Up, 2.9 V to 5.5 V Supply, 97 % Duty Cycle, 1.5 MHz, MSOP-10" H 3200 1175 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/analog-devices/adp1621armz-r7/dc-dc-ctrl-boost-1-5mhz-msop-10/dp/2727553?st=ADP1621ARMZ" H 3200 1075 50  0001 C CNN "Distributor Link"
F 6 "https://www.mouser.co.uk/ProductDetail/Analog-Devices/ADP1621ARMZ-R7?qs=%2Fha2pyFaduiBuZlfYCqztUstcnxGZxOzS%252B6tXD4%252Bklgvhp1dJM4vFA%3D%3D" H 3200 975 50  0001 C CNN "Distributor Link 2"
	1    3200 2975
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R204
U 1 1 614F01FD
P 3100 2225
F 0 "R204" H 3100 2150 50  0000 L CNN
F 1 "1" H 3150 2225 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 3100 2225 50  0001 C CNN
F 3 "~" H 3100 2225 50  0001 C CNN
F 4 "WF08P1R00FTL -  SMD Chip Resistor, 0805 [2012 Metric], 1 ohm, WF08P Series, 150 V, Thick Film, 250 mW" H 3100 1825 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/walsin/wf08p1r00ftl/res-1r-1-0-25w-0805-thick-film/dp/2670070" H 3100 1725 50  0001 C CNN "Distributor Link"
	1    3100 2225
	1    0    0    -1  
$EndComp
Wire Wire Line
	3100 2125 3100 2025
Wire Wire Line
	3100 2025 3300 2025
Connection ~ 3300 2025
Wire Wire Line
	3300 2025 3300 1975
Wire Wire Line
	2725 1775 3000 1775
Wire Wire Line
	2725 1225 3300 1225
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 614F9877
P 2900 2425
AR Path="/5E9C6910/614F9877" Ref="C?"  Part="1" 
AR Path="/5EBC6654/614F9877" Ref="C?"  Part="1" 
AR Path="/5EBC681B/614F9877" Ref="C?"  Part="1" 
AR Path="/5F05E355/614F9877" Ref="C?"  Part="1" 
AR Path="/5EA41912/614F9877" Ref="C?"  Part="1" 
AR Path="/5FD7CF82/614F9877" Ref="C?"  Part="1" 
AR Path="/5EA5E295/614F9877" Ref="C?"  Part="1" 
AR Path="/5FE92F3B/614F9877" Ref="C240"  Part="1" 
F 0 "C240" V 2950 2200 50  0000 L CNN
F 1 "1uF" V 2950 2450 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0805_2012Metric" H 2900 2425 50  0001 C CNN
F 3 "~" H 2900 2425 50  0001 C CNN
F 4 "08051C105K4T2A -  SMD Multilayer Ceramic Capacitor, 1 µF, 100 V, 0805 [2012 Metric], ± 10%, X7R" H 2900 2025 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/avx/08051c105k4t2a/cap-1-f-100v-10-x7r-0805/dp/2834935" H 2900 1925 50  0001 C CNN "Distributor Link"
	1    2900 2425
	0    1    -1   0   
$EndComp
Wire Wire Line
	3100 2325 3100 2425
Wire Wire Line
	3300 2025 3300 2125
Wire Wire Line
	3000 2425 3100 2425
$Comp
L PMSM_Driver_LV_Board_Power:MOT_PGND #PWR?
U 1 1 614FC872
P 2600 2475
AR Path="/5EA5D9E8/614FC872" Ref="#PWR?"  Part="1" 
AR Path="/5FE92F3B/614FC872" Ref="#PWR0439"  Part="1" 
F 0 "#PWR0439" H 2600 2225 50  0001 C CNN
F 1 "MOT_PGND" H 2605 2302 50  0000 C CNN
F 2 "" H 2600 2475 50  0001 C CNN
F 3 "" H 2600 2475 50  0001 C CNN
	1    2600 2475
	1    0    0    -1  
$EndComp
Wire Wire Line
	2600 2475 2600 2425
Wire Wire Line
	2600 2425 2800 2425
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 614FE00F
P 3500 2125
AR Path="/5E9C6910/614FE00F" Ref="C?"  Part="1" 
AR Path="/5EBC6654/614FE00F" Ref="C?"  Part="1" 
AR Path="/5EBC681B/614FE00F" Ref="C?"  Part="1" 
AR Path="/5F05E355/614FE00F" Ref="C?"  Part="1" 
AR Path="/5EA41912/614FE00F" Ref="C?"  Part="1" 
AR Path="/5FD7CF82/614FE00F" Ref="C?"  Part="1" 
AR Path="/5EA5E295/614FE00F" Ref="C?"  Part="1" 
AR Path="/5FE92F3B/614FE00F" Ref="C243"  Part="1" 
F 0 "C243" V 3550 1875 50  0000 L CNN
F 1 "1uF" V 3550 2150 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0805_2012Metric" H 3500 2125 50  0001 C CNN
F 3 "~" H 3500 2125 50  0001 C CNN
F 4 "08051C105K4T2A -  SMD Multilayer Ceramic Capacitor, 1 µF, 100 V, 0805 [2012 Metric], ± 10%, X7R" H 3500 1725 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/avx/08051c105k4t2a/cap-1-f-100v-10-x7r-0805/dp/2834935" H 3500 1625 50  0001 C CNN "Distributor Link"
	1    3500 2125
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3400 2125 3300 2125
Wire Wire Line
	3700 2175 3700 2125
Wire Wire Line
	3600 2125 3700 2125
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR?
U 1 1 615031B6
P 3700 2175
AR Path="/5EA5E06A/615031B6" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/615031B6" Ref="#PWR?"  Part="1" 
AR Path="/5FE92F3B/615031B6" Ref="#PWR0445"  Part="1" 
F 0 "#PWR0445" H 3700 1925 50  0001 C CNN
F 1 "MOT_DGND" H 3705 2002 50  0000 C CNN
F 2 "" H 3700 2175 50  0001 C CNN
F 3 "" H 3700 2175 50  0001 C CNN
	1    3700 2175
	1    0    0    -1  
$EndComp
Connection ~ 3300 2125
Text GLabel 3350 2475 2    50   Output ~ 0
MOT_15V_SUP_SDSN
Wire Wire Line
	3350 2475 3300 2475
Connection ~ 3300 2475
Wire Wire Line
	3300 2475 3300 2525
Text GLabel 2700 2825 0    50   Input ~ 0
MOT_15V_SUP_SDSN
Wire Wire Line
	2700 2825 2750 2825
$Comp
L PMSM_Driver_LV_Board_Device:FDMC86244 Q18
U 1 1 61516E08
P 4825 2425
F 0 "Q18" H 4550 2675 50  0000 C CNN
F 1 "FDMC86244" H 4900 2175 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:FDMC86244" H 4825 2025 50  0001 C CNN
F 3 "~" H 4825 2425 50  0001 C CNN
F 4 "FDMC86244 -  Power MOSFET, N Channel, 150 V, 9.4 A, 0.105 ohm, MLP, Surface Mount" H 4825 1925 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/on-semiconductor/fdmc86244/mosfet-n-ch-150v-9-4a-mlp-3-3x3/dp/2083270" H 4825 1825 50  0001 C CNN "Distributor Link"
	1    4825 2425
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:L_Core_Ferrite_Coupled_Small L3
U 1 1 6151ECED
P 4325 1425
F 0 "L3" V 4279 1308 50  0000 R CNN
F 1 "18uH" V 4370 1308 50  0000 R CNN
F 2 "PMSM_Driver_LV_Board:L_MSD1583-183MEB" H 4325 1425 50  0001 C CNN
F 3 "~" H 4325 1425 50  0001 C CNN
F 4 "MSD1583-183MEB -  Inductor, Power, 18 µH, 20%, 0.024 ohm, 3.04 A, 14.8mm x 14.8mm x 8.6mm" H 4325 1175 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/coilcraft/msd1583-183meb/inductor-18uh-4-3a-20-pwr-11-5mhz/dp/2288153?st=MSD1583-183ME" H 4325 1075 50  0001 C CNN "Distributor Link"
F 6 "https://www.mouser.co.uk/ProductDetail/Coilcraft/MSD1583-183MEB?qs=sGAEpiMZZMu3JGptLTgkAk3FHkd28dsGTB3I2XqdUvo%3D" V 4325 1425 50  0001 C CNN "Distributor Link 2"
	1    4325 1425
	0    -1   1    0   
$EndComp
Wire Wire Line
	4275 1325 4275 1225
Wire Wire Line
	4275 1225 4175 1225
Connection ~ 3300 1225
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C245
U 1 1 6152EAE8
P 4325 925
F 0 "C245" V 4425 875 50  0000 L CNN
F 1 "2.2uF" V 4200 825 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 4325 925 50  0001 C CNN
F 3 "~" H 4325 925 50  0001 C CNN
F 4 "C3216X7S2A225K160AB -  SMD Multilayer Ceramic Capacitor, 2.2 µF, 100 V, 1206 [3216 Metric], ± 10%, X7S, C Series TDK " H 4325 525 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/tdk/c3216x7s2a225k160ab/cap-2-2-f-100v-10-x7s-1206/dp/2112717" H 4325 425 50  0001 C CNN "Distributor Link"
F 6 "100V" V 4125 850 50  0000 L CNN "Voltage"
	1    4325 925 
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4375 1325 4375 1225
Wire Wire Line
	4375 1225 4475 1225
Wire Wire Line
	4425 925  4475 925 
Wire Wire Line
	4475 925  4475 1225
Wire Wire Line
	4225 925  4175 925 
Wire Wire Line
	4175 925  4175 1225
Connection ~ 4175 1225
Wire Wire Line
	4175 1225 3300 1225
$Comp
L PMSM_Driver_LV_Board_Power:MOT_PGND #PWR?
U 1 1 615336F4
P 4975 1325
AR Path="/5EA5D9E8/615336F4" Ref="#PWR?"  Part="1" 
AR Path="/5FE92F3B/615336F4" Ref="#PWR0450"  Part="1" 
F 0 "#PWR0450" H 4975 1075 50  0001 C CNN
F 1 "MOT_PGND" H 4980 1152 50  0000 C CNN
F 2 "" H 4975 1325 50  0001 C CNN
F 3 "" H 4975 1325 50  0001 C CNN
	1    4975 1325
	1    0    0    -1  
$EndComp
Wire Wire Line
	4975 1325 4975 1225
Connection ~ 4475 1225
Wire Wire Line
	4275 1525 4275 1825
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C247
U 1 1 61537B7C
P 4475 1825
F 0 "C247" V 4575 1775 50  0000 L CNN
F 1 "2.2uF" V 4350 1725 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 4475 1825 50  0001 C CNN
F 3 "~" H 4475 1825 50  0001 C CNN
F 4 "C3216X7S2A225K160AB -  SMD Multilayer Ceramic Capacitor, 2.2 µF, 100 V, 1206 [3216 Metric], ± 10%, X7S, C Series TDK " H 4475 1425 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/tdk/c3216x7s2a225k160ab/cap-2-2-f-100v-10-x7s-1206/dp/2112717" H 4475 1325 50  0001 C CNN "Distributor Link"
F 6 "100V" V 4275 1750 50  0000 L CNN "Voltage"
	1    4475 1825
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4475 1225 4975 1225
Wire Wire Line
	4375 1525 4375 1625
Wire Wire Line
	4375 1625 4675 1625
Wire Wire Line
	4675 1625 4675 1825
Wire Wire Line
	4675 1825 4575 1825
Wire Wire Line
	4375 1825 4275 1825
Connection ~ 4275 1825
Wire Wire Line
	4275 2275 4375 2275
Wire Wire Line
	4275 1825 4275 2275
Wire Wire Line
	4275 2275 4275 2375
Wire Wire Line
	4275 2375 4375 2375
Connection ~ 4275 2275
Wire Wire Line
	4275 2375 4275 2475
Wire Wire Line
	4275 2475 4375 2475
Connection ~ 4275 2375
Wire Wire Line
	4275 2475 4275 2575
Wire Wire Line
	4275 2575 4375 2575
Connection ~ 4275 2475
$Comp
L PMSM_Driver_LV_Board_Device:D_Schottky_Small D35
U 1 1 61543F7A
P 4875 1825
F 0 "D35" H 4875 1725 50  0000 C CNN
F 1 "MBRS190T3G" H 4875 1925 25  0000 C CNN
F 2 "PMSM_Driver_LV_Board:D_SMB" V 4875 1825 50  0001 C CNN
F 3 "~" V 4875 1825 50  0001 C CNN
F 4 "MBRS190T3G -  Schottky Rectifier, General Purpose, 90 V, 1 A, Single, DO-214AA (SMB), 2 Pins, 750 mV" H 4875 1625 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/on-semiconductor/mbrs190t3g/diode-schottky-1a-smb/dp/1459071" H 4875 1525 50  0001 C CNN "Distributor Link"
	1    4875 1825
	-1   0    0    1   
$EndComp
Wire Wire Line
	4675 1825 4775 1825
Connection ~ 4675 1825
Wire Wire Line
	5225 2275 5325 2275
Wire Wire Line
	5325 2275 5325 2375
Wire Wire Line
	5325 2475 5225 2475
Wire Wire Line
	5225 2375 5325 2375
Connection ~ 5325 2375
Wire Wire Line
	5325 2375 5325 2475
Wire Wire Line
	5225 2575 5325 2575
Wire Wire Line
	5325 2575 5325 3075
Wire Wire Line
	5325 3075 3650 3075
Connection ~ 3100 2425
Wire Wire Line
	3100 2425 3100 2525
$Comp
L PMSM_Driver_LV_Board_Device:CP1_Small C?
U 1 1 6154DC84
P 5600 2025
AR Path="/5EA41912/6154DC84" Ref="C?"  Part="1" 
AR Path="/5EA90258/6154DC84" Ref="C?"  Part="1" 
AR Path="/5EA5E06A/6154DC84" Ref="C?"  Part="1" 
AR Path="/5FE92F3B/6154DC84" Ref="C250"  Part="1" 
F 0 "C250" H 5750 2075 50  0000 C CNN
F 1 "470uF" H 5800 2000 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:CP_Elec_16x17.5" H 5600 2025 50  0001 C CNN
F 3 "~" H 5600 2025 50  0001 C CNN
F 4 "MCVFZ063M471JB7L -  SMD Aluminium Electrolytic Capacitor, Radial Can - SMD, 470 µF, 63 V, FZ, V-Chip Series" H 5600 1625 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcvfz063m471jb7l/cap-470-f-63v-smd/dp/2630481" H 5600 1525 50  0001 C CNN "Distributor Link"
F 6 "63V" H 5775 1925 50  0000 C CNN "Voltage"
	1    5600 2025
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_PGND #PWR?
U 1 1 6155338E
P 5600 2225
AR Path="/5EA5D9E8/6155338E" Ref="#PWR?"  Part="1" 
AR Path="/5FE92F3B/6155338E" Ref="#PWR0453"  Part="1" 
F 0 "#PWR0453" H 5600 1975 50  0001 C CNN
F 1 "MOT_PGND" H 5605 2052 50  0000 C CNN
F 2 "" H 5600 2225 50  0001 C CNN
F 3 "" H 5600 2225 50  0001 C CNN
	1    5600 2225
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 2225 5600 2125
Wire Wire Line
	4975 1825 5600 1825
Wire Wire Line
	5600 1825 5600 1925
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C251
U 1 1 6155817D
P 6150 2025
F 0 "C251" H 6200 2100 50  0000 L CNN
F 1 "OPTIONAL" H 6225 2025 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 6150 2025 50  0001 C CNN
F 3 "~" H 6150 2025 50  0001 C CNN
F 4 "..." H 6150 1625 50  0001 C CNN "Description"
F 5 "..." H 6150 1525 50  0001 C CNN "Distributor Link"
F 6 "DNI" H 6200 1950 50  0000 L CNN "DNI"
	1    6150 2025
	1    0    0    -1  
$EndComp
Wire Wire Line
	6150 1925 6150 1825
$Comp
L PMSM_Driver_LV_Board_Power:MOT_PGND #PWR?
U 1 1 6155ADA3
P 6150 2225
AR Path="/5EA5D9E8/6155ADA3" Ref="#PWR?"  Part="1" 
AR Path="/5FE92F3B/6155ADA3" Ref="#PWR0454"  Part="1" 
F 0 "#PWR0454" H 6150 1975 50  0001 C CNN
F 1 "MOT_PGND" H 6155 2052 50  0000 C CNN
F 2 "" H 6150 2225 50  0001 C CNN
F 3 "" H 6150 2225 50  0001 C CNN
	1    6150 2225
	1    0    0    -1  
$EndComp
Wire Wire Line
	6150 2225 6150 2125
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C252
U 1 1 6155F596
P 6700 2025
F 0 "C252" H 6750 2100 50  0000 L CNN
F 1 "OPTIONAL" H 6775 2025 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 6700 2025 50  0001 C CNN
F 3 "~" H 6700 2025 50  0001 C CNN
F 4 "..." H 6700 1625 50  0001 C CNN "Description"
F 5 "..." H 6700 1525 50  0001 C CNN "Distributor Link"
F 6 "DNI" H 6750 1950 50  0000 L CNN "DNI"
	1    6700 2025
	1    0    0    -1  
$EndComp
Wire Wire Line
	6700 1925 6700 1825
$Comp
L PMSM_Driver_LV_Board_Power:MOT_PGND #PWR?
U 1 1 6155F59D
P 6700 2225
AR Path="/5EA5D9E8/6155F59D" Ref="#PWR?"  Part="1" 
AR Path="/5FE92F3B/6155F59D" Ref="#PWR0456"  Part="1" 
F 0 "#PWR0456" H 6700 1975 50  0001 C CNN
F 1 "MOT_PGND" H 6705 2052 50  0000 C CNN
F 2 "" H 6700 2225 50  0001 C CNN
F 3 "" H 6700 2225 50  0001 C CNN
	1    6700 2225
	1    0    0    -1  
$EndComp
Wire Wire Line
	6700 2225 6700 2125
Wire Wire Line
	5600 1825 6150 1825
Connection ~ 5600 1825
Connection ~ 6150 1825
Wire Wire Line
	7225 1825 7225 1725
Wire Wire Line
	6775 1725 6775 1825
Wire Wire Line
	6150 1825 6700 1825
Wire Wire Line
	6700 1825 6775 1825
Connection ~ 6700 1825
Wire Wire Line
	6775 1825 7225 1825
Connection ~ 6775 1825
Wire Wire Line
	5325 2475 5425 2475
Wire Wire Line
	5425 2475 5425 2650
Connection ~ 5325 2475
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R209
U 1 1 6157C82C
P 5425 2850
F 0 "R209" H 5475 2850 50  0000 L CNN
F 1 "0.022" H 5450 2775 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5425 2850 50  0001 C CNN
F 3 "~" H 5425 2850 50  0001 C CNN
F 4 "UCR10EVHFSR022 -  SMD Chip Resistor" H 5425 2450 50  0001 C CNN "Description"
F 5 "https://www.mouser.co.uk/ProductDetail/ROHM-Semiconductor/UCR10EVHFSR022?qs=sGAEpiMZZMtlubZbdhIBICz%2FtfrlArmdZy42O9CJ%2Fjc%3D" H 5425 2350 50  0001 C CNN "Distributor Link"
	1    5425 2850
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_PGND #PWR?
U 1 1 6157E68A
P 5425 3050
AR Path="/5EA5D9E8/6157E68A" Ref="#PWR?"  Part="1" 
AR Path="/5FE92F3B/6157E68A" Ref="#PWR0452"  Part="1" 
F 0 "#PWR0452" H 5425 2800 50  0001 C CNN
F 1 "MOT_PGND" H 5430 2877 50  0000 C CNN
F 2 "" H 5425 3050 50  0001 C CNN
F 3 "" H 5425 3050 50  0001 C CNN
	1    5425 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	5425 3050 5425 2950
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R210
U 1 1 61580C43
P 5775 2650
F 0 "R210" V 5850 2550 50  0000 L CNN
F 1 "649" V 5675 2575 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 5775 2650 50  0001 C CNN
F 3 "~" H 5775 2650 50  0001 C CNN
F 4 "MCWR06X6490FTL -  SMD Chip Resistor, 0603 [1608 Metric], 649 ohm, MCWR Series, 75 V, Thick Film, 100 mW" H 5775 2250 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr06x6490ftl/res-649r-1-0-1w-thick-film/dp/2447416" H 5775 2150 50  0001 C CNN "Distributor Link"
	1    5775 2650
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5675 2650 5425 2650
Connection ~ 5425 2650
Wire Wire Line
	5425 2650 5425 2750
Wire Wire Line
	5975 2650 5875 2650
Text GLabel 5975 2650 2    50   Output ~ 0
MOT_15V_SUP_CS
Text GLabel 2700 3125 0    50   Input ~ 0
MOT_15V_SUP_CS
Wire Wire Line
	2700 3125 2750 3125
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR?
U 1 1 6158EAAB
P 3500 3625
AR Path="/5EA5E06A/6158EAAB" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/6158EAAB" Ref="#PWR?"  Part="1" 
AR Path="/5FE92F3B/6158EAAB" Ref="#PWR0444"  Part="1" 
F 0 "#PWR0444" H 3500 3375 50  0001 C CNN
F 1 "MOT_DGND" H 3505 3452 50  0000 C CNN
F 2 "" H 3500 3625 50  0001 C CNN
F 3 "" H 3500 3625 50  0001 C CNN
	1    3500 3625
	1    0    0    -1  
$EndComp
Wire Wire Line
	3500 3525 3300 3525
Wire Wire Line
	3300 3525 3300 3425
Wire Wire Line
	3500 3525 3500 3625
Wire Wire Line
	2900 3525 3100 3525
Wire Wire Line
	3100 3525 3100 3425
Wire Wire Line
	2900 3525 2900 3625
$Comp
L PMSM_Driver_LV_Board_Power:MOT_PGND #PWR?
U 1 1 61597327
P 2900 3625
AR Path="/5EA5D9E8/61597327" Ref="#PWR?"  Part="1" 
AR Path="/5FE92F3B/61597327" Ref="#PWR0441"  Part="1" 
F 0 "#PWR0441" H 2900 3375 50  0001 C CNN
F 1 "MOT_PGND" H 2905 3452 50  0000 C CNN
F 2 "" H 2900 3625 50  0001 C CNN
F 3 "" H 2900 3625 50  0001 C CNN
	1    2900 3625
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C246
U 1 1 615A7100
P 4425 3425
F 0 "C246" H 4500 3425 50  0000 L CNN
F 1 "12pF" H 4450 3325 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0805_2012Metric" H 4425 3425 50  0001 C CNN
F 3 "~" H 4425 3425 50  0001 C CNN
F 4 "C0805C120J5GACTU -  SMD Multilayer Ceramic Capacitor, 12 pF, 50 V, 0805 [2012 Metric], ± 5%, C0G / NP0, C Series KEMET" H 4425 3025 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/kemet/c0805c120j5gactu/cap-12pf-50v-5-c0g-np0-0805/dp/1414666" H 4425 2925 50  0001 C CNN "Distributor Link"
	1    4425 3425
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR?
U 1 1 615A95BA
P 4425 3625
AR Path="/5EA5E06A/615A95BA" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/615A95BA" Ref="#PWR?"  Part="1" 
AR Path="/5FE92F3B/615A95BA" Ref="#PWR0448"  Part="1" 
F 0 "#PWR0448" H 4425 3375 50  0001 C CNN
F 1 "MOT_DGND" H 4430 3452 50  0000 C CNN
F 2 "" H 4425 3625 50  0001 C CNN
F 3 "" H 4425 3625 50  0001 C CNN
	1    4425 3625
	1    0    0    -1  
$EndComp
Wire Wire Line
	4425 3525 4425 3625
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C248
U 1 1 615B5170
P 4900 3425
F 0 "C248" H 4975 3425 50  0000 L CNN
F 1 "10nF" H 4925 3325 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4900 3425 50  0001 C CNN
F 3 "~" H 4900 3425 50  0001 C CNN
F 4 "C0603C103K5RAC7081 -  SMD Multilayer Ceramic Capacitor, 10000 pF, 50 V, 0603 [1608 Metric], ± 10%, X7R" H 4900 3025 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/kemet/c0603c103k5rac7081/cap-0-01-f-50v-10-x7r-0603/dp/2522596" H 4900 2925 50  0001 C CNN "Distributor Link"
	1    4900 3425
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR?
U 1 1 615B5176
P 4900 3925
AR Path="/5EA5E06A/615B5176" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/615B5176" Ref="#PWR?"  Part="1" 
AR Path="/5FE92F3B/615B5176" Ref="#PWR0449"  Part="1" 
F 0 "#PWR0449" H 4900 3675 50  0001 C CNN
F 1 "MOT_DGND" H 4905 3752 50  0000 C CNN
F 2 "" H 4900 3925 50  0001 C CNN
F 3 "" H 4900 3925 50  0001 C CNN
	1    4900 3925
	1    0    0    -1  
$EndComp
Wire Wire Line
	4900 3825 4900 3925
Wire Wire Line
	3650 2875 4425 2875
Wire Wire Line
	4425 2875 4425 3325
Wire Wire Line
	4425 2875 4900 2875
Wire Wire Line
	4900 2875 4900 3325
Connection ~ 4425 2875
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R208
U 1 1 615BF547
P 4900 3725
F 0 "R208" H 4950 3725 50  0000 L CNN
F 1 "536k" H 4925 3625 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4900 3725 50  0001 C CNN
F 3 "~" H 4900 3725 50  0001 C CNN
F 4 "MCWR06X5363FTL -  SMD Chip Resistor, 0603 [1608 Metric], 536 kohm, MCWR06 Series, 75 V, Thick Film, 100 mW" H 4900 3325 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr06x5363ftl/res-536k-1-0-1w-0603-thick-film/dp/2694894" H 4900 3225 50  0001 C CNN "Distributor Link"
	1    4900 3725
	1    0    0    -1  
$EndComp
Wire Wire Line
	4900 3625 4900 3525
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR?
U 1 1 615C9865
P 1800 3425
AR Path="/5EA5E06A/615C9865" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/615C9865" Ref="#PWR?"  Part="1" 
AR Path="/5FE92F3B/615C9865" Ref="#PWR0434"  Part="1" 
F 0 "#PWR0434" H 1800 3175 50  0001 C CNN
F 1 "MOT_DGND" H 1805 3252 50  0000 C CNN
F 2 "" H 1800 3425 50  0001 C CNN
F 3 "" H 1800 3425 50  0001 C CNN
	1    1800 3425
	1    0    0    -1  
$EndComp
Wire Wire Line
	1800 3325 1800 3425
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R202
U 1 1 615C986E
P 1800 3225
F 0 "R202" H 1850 3225 50  0000 L CNN
F 1 "60k4" H 1825 3125 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 1800 3225 50  0001 C CNN
F 3 "~" H 1800 3225 50  0001 C CNN
F 4 "MCWR06X6042FTL -  SMD Chip Resistor, 0603 [1608 Metric], 60.4 kohm, MCWR06 Series, 75 V, Thick Film, 100 mW" H 1800 2825 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr06x6042ftl/res-60k4-1-0-1w-0603-thick-film/dp/2694911" H 1800 2725 50  0001 C CNN "Distributor Link"
	1    1800 3225
	1    0    0    -1  
$EndComp
Wire Wire Line
	1800 3125 1800 3025
Wire Wire Line
	1800 3025 2750 3025
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR?
U 1 1 615CF5F1
P 7225 3200
AR Path="/5EA5E06A/615CF5F1" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/615CF5F1" Ref="#PWR?"  Part="1" 
AR Path="/5FE92F3B/615CF5F1" Ref="#PWR0462"  Part="1" 
F 0 "#PWR0462" H 7225 2950 50  0001 C CNN
F 1 "MOT_DGND" H 7230 3027 50  0000 C CNN
F 2 "" H 7225 3200 50  0001 C CNN
F 3 "" H 7225 3200 50  0001 C CNN
	1    7225 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	7225 3100 7225 3200
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R213
U 1 1 615CF5FA
P 7225 3000
F 0 "R213" H 7275 3000 50  0000 L CNN
F 1 "4k87" H 7250 2900 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 7225 3000 50  0001 C CNN
F 3 "~" H 7225 3000 50  0001 C CNN
F 4 "MCWR04X4871FTL -  SMD Chip Resistor, 0402 [1005 Metric], 4.87 kohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 7225 2600 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp-pro/mcwr04x4871ftl/res-4k87-1-0-0625w-thick-film/dp/2447189" H 7225 2500 50  0001 C CNN "Distributor Link"
	1    7225 3000
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R212
U 1 1 615D28B7
P 7225 2700
F 0 "R212" H 7275 2700 50  0000 L CNN
F 1 "47k5" H 7250 2600 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 7225 2700 50  0001 C CNN
F 3 "~" H 7225 2700 50  0001 C CNN
F 4 "MCWR04X4752FTL -  SMD Chip Resistor, 0402 [1005 Metric], 47.5 kohm, MCWR04 Series, 50 V, Thick Film, 62.5 mW" H 7225 2300 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x4752ftl/res-47k5-1-0-0625w-0402-thick/dp/2694622" H 7225 2200 50  0001 C CNN "Distributor Link"
	1    7225 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	7225 2900 7225 2850
Wire Wire Line
	7225 2600 7225 1825
Connection ~ 7225 1825
Text GLabel 7125 2850 0    50   Output ~ 0
MOT_15V_SUP_FB
Wire Wire Line
	7125 2850 7225 2850
Connection ~ 7225 2850
Wire Wire Line
	7225 2850 7225 2800
Text GLabel 2700 2925 0    50   Input ~ 0
MOT_15V_SUP_FB
Wire Wire Line
	2700 2925 2750 2925
Text Notes 675  800  0    100  ~ 0
15V Supply on the Motor Side
Wire Notes Line
	600  600  600  4250
Wire Notes Line
	7625 600  7625 4250
Text Notes 675  4550 0    100  ~ 0
5V Supply on the Motor Side
$Comp
L PMSM_Driver_LV_Board_Device:Ferrite_Bead_Small FB?
U 1 1 616830A7
P 1200 5075
AR Path="/5EA5E155/616830A7" Ref="FB?"  Part="1" 
AR Path="/5ED47D19/616830A7" Ref="FB?"  Part="1" 
AR Path="/5FE92F3B/616830A7" Ref="FB9"  Part="1" 
F 0 "FB9" V 1325 5000 50  0000 L CNN
F 1 "Z0805C420APWST" V 1075 4900 25  0000 L CNN
F 2 "PMSM_Driver_LV_Board:L_0805_2012Metric" V 1130 5075 50  0001 C CNN
F 3 "~" H 1200 5075 50  0001 C CNN
F 4 "Z0805C420APWST -  Ferrite Bead, 0805 [2012 Metric], 42 ohm, 4 A, Z-PWS Series, 0.008 ohm, ± 25%" H 1200 5075 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/kemet/z0805c420apwst/ferrite-bead-42-ohm-4a-0805/dp/2846741" H 1200 5075 50  0001 C CNN "Distributor Link"
F 6 "42 Ohms at 100 MHz" V 1025 4850 25  0000 L CNN "Impedance"
	1    1200 5075
	0    -1   -1   0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_15V0 #PWR?
U 1 1 61686192
P 1000 4975
AR Path="/5EA5E06A/61686192" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/61686192" Ref="#PWR?"  Part="1" 
AR Path="/5FE92F3B/61686192" Ref="#PWR0431"  Part="1" 
F 0 "#PWR0431" H 1000 5225 50  0001 C CNN
F 1 "MOT_15V0" H 1001 5148 50  0000 C CNN
F 2 "" H 1000 4975 50  0001 C CNN
F 3 "" H 1000 4975 50  0001 C CNN
	1    1000 4975
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 616B7D6E
P 1550 5275
AR Path="/5E9C6910/616B7D6E" Ref="C?"  Part="1" 
AR Path="/5EBC6654/616B7D6E" Ref="C?"  Part="1" 
AR Path="/5EBC681B/616B7D6E" Ref="C?"  Part="1" 
AR Path="/5F05E355/616B7D6E" Ref="C?"  Part="1" 
AR Path="/6068445B/616B7D6E" Ref="C?"  Part="1" 
AR Path="/60E816A3/616B7D6E" Ref="C?"  Part="1" 
AR Path="/5EFE605D/616B7D6E" Ref="C?"  Part="1" 
AR Path="/5EFE7331/616B7D6E" Ref="C?"  Part="1" 
AR Path="/5FD7CF82/616B7D6E" Ref="C?"  Part="1" 
AR Path="/5EA5E295/616B7D6E" Ref="C?"  Part="1" 
AR Path="/5FE92F3B/616B7D6E" Ref="C235"  Part="1" 
F 0 "C235" H 1350 5350 50  0000 L CNN
F 1 "10uF" H 1350 5200 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0805_2012Metric" H 1550 5275 50  0001 C CNN
F 3 "~" H 1550 5275 50  0001 C CNN
F 4 "TMK212BBJ106KG-T - 10uF ceramic cap" H 1550 4875 50  0001 C CNN "Description"
F 5 "https://www.mouser.co.uk/ProductDetail/Taiyo-Yuden/TMK212BBJ106KG-T?qs=sGAEpiMZZMs0AnBnWHyRQAEIN6r3SS%2FOSnPCSGjIcIU%3D" H 1550 4775 50  0001 C CNN "Distributor Link"
	1    1550 5275
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1000 4975 1000 5075
Wire Wire Line
	1000 5075 1100 5075
Wire Wire Line
	1300 5075 1425 5075
Wire Wire Line
	1550 5075 1550 5175
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR?
U 1 1 616CA6E7
P 1550 5475
AR Path="/5EA5E06A/616CA6E7" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/616CA6E7" Ref="#PWR?"  Part="1" 
AR Path="/5FE92F3B/616CA6E7" Ref="#PWR0433"  Part="1" 
F 0 "#PWR0433" H 1550 5225 50  0001 C CNN
F 1 "MOT_DGND" H 1555 5302 50  0000 C CNN
F 2 "" H 1550 5475 50  0001 C CNN
F 3 "" H 1550 5475 50  0001 C CNN
	1    1550 5475
	1    0    0    -1  
$EndComp
Wire Wire Line
	1550 5375 1550 5475
Wire Wire Line
	2000 5075 2000 5175
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR?
U 1 1 616CEFE2
P 2000 5475
AR Path="/5EA5E06A/616CEFE2" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/616CEFE2" Ref="#PWR?"  Part="1" 
AR Path="/5FE92F3B/616CEFE2" Ref="#PWR0436"  Part="1" 
F 0 "#PWR0436" H 2000 5225 50  0001 C CNN
F 1 "MOT_DGND" H 2005 5302 50  0000 C CNN
F 2 "" H 2000 5475 50  0001 C CNN
F 3 "" H 2000 5475 50  0001 C CNN
	1    2000 5475
	1    0    0    -1  
$EndComp
Wire Wire Line
	2000 5375 2000 5475
Wire Wire Line
	1550 5075 2000 5075
Connection ~ 1550 5075
Wire Wire Line
	2450 5075 2450 5175
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR?
U 1 1 616D76B2
P 2450 5475
AR Path="/5EA5E06A/616D76B2" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/616D76B2" Ref="#PWR?"  Part="1" 
AR Path="/5FE92F3B/616D76B2" Ref="#PWR0438"  Part="1" 
F 0 "#PWR0438" H 2450 5225 50  0001 C CNN
F 1 "MOT_DGND" H 2455 5302 50  0000 C CNN
F 2 "" H 2450 5475 50  0001 C CNN
F 3 "" H 2450 5475 50  0001 C CNN
	1    2450 5475
	1    0    0    -1  
$EndComp
Wire Wire Line
	2450 5375 2450 5475
Wire Wire Line
	2000 5075 2450 5075
Wire Wire Line
	2900 5075 2900 5175
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR?
U 1 1 616DC3FE
P 2900 5475
AR Path="/5EA5E06A/616DC3FE" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/616DC3FE" Ref="#PWR?"  Part="1" 
AR Path="/5FE92F3B/616DC3FE" Ref="#PWR0442"  Part="1" 
F 0 "#PWR0442" H 2900 5225 50  0001 C CNN
F 1 "MOT_DGND" H 2905 5302 50  0000 C CNN
F 2 "" H 2900 5475 50  0001 C CNN
F 3 "" H 2900 5475 50  0001 C CNN
	1    2900 5475
	1    0    0    -1  
$EndComp
Wire Wire Line
	2900 5375 2900 5475
Wire Wire Line
	2450 5075 2900 5075
Wire Wire Line
	3350 5075 3350 5175
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR?
U 1 1 616E1305
P 3350 5475
AR Path="/5EA5E06A/616E1305" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/616E1305" Ref="#PWR?"  Part="1" 
AR Path="/5FE92F3B/616E1305" Ref="#PWR0443"  Part="1" 
F 0 "#PWR0443" H 3350 5225 50  0001 C CNN
F 1 "MOT_DGND" H 3355 5302 50  0000 C CNN
F 2 "" H 3350 5475 50  0001 C CNN
F 3 "" H 3350 5475 50  0001 C CNN
	1    3350 5475
	1    0    0    -1  
$EndComp
Wire Wire Line
	3350 5375 3350 5475
Wire Wire Line
	2900 5075 3350 5075
Wire Wire Line
	3800 5075 3800 5175
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR?
U 1 1 616E5FB7
P 3800 5475
AR Path="/5EA5E06A/616E5FB7" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/616E5FB7" Ref="#PWR?"  Part="1" 
AR Path="/5FE92F3B/616E5FB7" Ref="#PWR0446"  Part="1" 
F 0 "#PWR0446" H 3800 5225 50  0001 C CNN
F 1 "MOT_DGND" H 3805 5302 50  0000 C CNN
F 2 "" H 3800 5475 50  0001 C CNN
F 3 "" H 3800 5475 50  0001 C CNN
	1    3800 5475
	1    0    0    -1  
$EndComp
Wire Wire Line
	3800 5375 3800 5475
Wire Wire Line
	3350 5075 3800 5075
Connection ~ 2000 5075
Connection ~ 2450 5075
Connection ~ 2900 5075
Connection ~ 3350 5075
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 617168ED
P 2000 5275
AR Path="/5E9C6910/617168ED" Ref="C?"  Part="1" 
AR Path="/5EBC6654/617168ED" Ref="C?"  Part="1" 
AR Path="/5EBC681B/617168ED" Ref="C?"  Part="1" 
AR Path="/5F05E355/617168ED" Ref="C?"  Part="1" 
AR Path="/6068445B/617168ED" Ref="C?"  Part="1" 
AR Path="/60E816A3/617168ED" Ref="C?"  Part="1" 
AR Path="/5EFE605D/617168ED" Ref="C?"  Part="1" 
AR Path="/5EFE7331/617168ED" Ref="C?"  Part="1" 
AR Path="/5FD7CF82/617168ED" Ref="C?"  Part="1" 
AR Path="/5EA5E295/617168ED" Ref="C?"  Part="1" 
AR Path="/5FE92F3B/617168ED" Ref="C237"  Part="1" 
F 0 "C237" H 1800 5350 50  0000 L CNN
F 1 "10uF" H 1800 5200 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0805_2012Metric" H 2000 5275 50  0001 C CNN
F 3 "~" H 2000 5275 50  0001 C CNN
F 4 "TMK212BBJ106KG-T - 10uF ceramic cap" H 2000 4875 50  0001 C CNN "Description"
F 5 "https://www.mouser.co.uk/ProductDetail/Taiyo-Yuden/TMK212BBJ106KG-T?qs=sGAEpiMZZMs0AnBnWHyRQAEIN6r3SS%2FOSnPCSGjIcIU%3D" H 2000 4775 50  0001 C CNN "Distributor Link"
	1    2000 5275
	-1   0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 6171B67B
P 2450 5275
AR Path="/5E9C6910/6171B67B" Ref="C?"  Part="1" 
AR Path="/5EBC6654/6171B67B" Ref="C?"  Part="1" 
AR Path="/5EBC681B/6171B67B" Ref="C?"  Part="1" 
AR Path="/5F05E355/6171B67B" Ref="C?"  Part="1" 
AR Path="/6068445B/6171B67B" Ref="C?"  Part="1" 
AR Path="/60E816A3/6171B67B" Ref="C?"  Part="1" 
AR Path="/5EFE605D/6171B67B" Ref="C?"  Part="1" 
AR Path="/5EFE7331/6171B67B" Ref="C?"  Part="1" 
AR Path="/5FD7CF82/6171B67B" Ref="C?"  Part="1" 
AR Path="/5EA5E295/6171B67B" Ref="C?"  Part="1" 
AR Path="/5FE92F3B/6171B67B" Ref="C239"  Part="1" 
F 0 "C239" H 2250 5350 50  0000 L CNN
F 1 "10uF" H 2250 5200 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0805_2012Metric" H 2450 5275 50  0001 C CNN
F 3 "~" H 2450 5275 50  0001 C CNN
F 4 "TMK212BBJ106KG-T - 10uF ceramic cap" H 2450 4875 50  0001 C CNN "Description"
F 5 "https://www.mouser.co.uk/ProductDetail/Taiyo-Yuden/TMK212BBJ106KG-T?qs=sGAEpiMZZMs0AnBnWHyRQAEIN6r3SS%2FOSnPCSGjIcIU%3D" H 2450 4775 50  0001 C CNN "Distributor Link"
	1    2450 5275
	-1   0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 61720334
P 2900 5275
AR Path="/5E9C6910/61720334" Ref="C?"  Part="1" 
AR Path="/5EBC6654/61720334" Ref="C?"  Part="1" 
AR Path="/5EBC681B/61720334" Ref="C?"  Part="1" 
AR Path="/5F05E355/61720334" Ref="C?"  Part="1" 
AR Path="/6068445B/61720334" Ref="C?"  Part="1" 
AR Path="/60E816A3/61720334" Ref="C?"  Part="1" 
AR Path="/5EFE605D/61720334" Ref="C?"  Part="1" 
AR Path="/5EFE7331/61720334" Ref="C?"  Part="1" 
AR Path="/5FD7CF82/61720334" Ref="C?"  Part="1" 
AR Path="/5EA5E295/61720334" Ref="C?"  Part="1" 
AR Path="/5FE92F3B/61720334" Ref="C241"  Part="1" 
F 0 "C241" H 2700 5350 50  0000 L CNN
F 1 "10uF" H 2700 5200 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0805_2012Metric" H 2900 5275 50  0001 C CNN
F 3 "~" H 2900 5275 50  0001 C CNN
F 4 "TMK212BBJ106KG-T - 10uF ceramic cap" H 2900 4875 50  0001 C CNN "Description"
F 5 "https://www.mouser.co.uk/ProductDetail/Taiyo-Yuden/TMK212BBJ106KG-T?qs=sGAEpiMZZMs0AnBnWHyRQAEIN6r3SS%2FOSnPCSGjIcIU%3D" H 2900 4775 50  0001 C CNN "Distributor Link"
	1    2900 5275
	-1   0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 61724FC2
P 3350 5275
AR Path="/5E9C6910/61724FC2" Ref="C?"  Part="1" 
AR Path="/5EBC6654/61724FC2" Ref="C?"  Part="1" 
AR Path="/5EBC681B/61724FC2" Ref="C?"  Part="1" 
AR Path="/5F05E355/61724FC2" Ref="C?"  Part="1" 
AR Path="/6068445B/61724FC2" Ref="C?"  Part="1" 
AR Path="/60E816A3/61724FC2" Ref="C?"  Part="1" 
AR Path="/5EFE605D/61724FC2" Ref="C?"  Part="1" 
AR Path="/5EFE7331/61724FC2" Ref="C?"  Part="1" 
AR Path="/5FD7CF82/61724FC2" Ref="C?"  Part="1" 
AR Path="/5EA5E295/61724FC2" Ref="C?"  Part="1" 
AR Path="/5FE92F3B/61724FC2" Ref="C242"  Part="1" 
F 0 "C242" H 3150 5350 50  0000 L CNN
F 1 "10uF" H 3150 5200 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0805_2012Metric" H 3350 5275 50  0001 C CNN
F 3 "~" H 3350 5275 50  0001 C CNN
F 4 "TMK212BBJ106KG-T - 10uF ceramic cap" H 3350 4875 50  0001 C CNN "Description"
F 5 "https://www.mouser.co.uk/ProductDetail/Taiyo-Yuden/TMK212BBJ106KG-T?qs=sGAEpiMZZMs0AnBnWHyRQAEIN6r3SS%2FOSnPCSGjIcIU%3D" H 3350 4775 50  0001 C CNN "Distributor Link"
	1    3350 5275
	-1   0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 61729D31
P 3800 5275
AR Path="/5E9C6910/61729D31" Ref="C?"  Part="1" 
AR Path="/5EBC6654/61729D31" Ref="C?"  Part="1" 
AR Path="/5EBC681B/61729D31" Ref="C?"  Part="1" 
AR Path="/5F05E355/61729D31" Ref="C?"  Part="1" 
AR Path="/6068445B/61729D31" Ref="C?"  Part="1" 
AR Path="/60E816A3/61729D31" Ref="C?"  Part="1" 
AR Path="/5EFE605D/61729D31" Ref="C?"  Part="1" 
AR Path="/5EFE7331/61729D31" Ref="C?"  Part="1" 
AR Path="/5FD7CF82/61729D31" Ref="C?"  Part="1" 
AR Path="/5EA5E295/61729D31" Ref="C?"  Part="1" 
AR Path="/5FE92F3B/61729D31" Ref="C244"  Part="1" 
F 0 "C244" H 3600 5350 50  0000 L CNN
F 1 "10uF" H 3600 5200 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0805_2012Metric" H 3800 5275 50  0001 C CNN
F 3 "~" H 3800 5275 50  0001 C CNN
F 4 "TMK212BBJ106KG-T - 10uF ceramic cap" H 3800 4875 50  0001 C CNN "Description"
F 5 "https://www.mouser.co.uk/ProductDetail/Taiyo-Yuden/TMK212BBJ106KG-T?qs=sGAEpiMZZMs0AnBnWHyRQAEIN6r3SS%2FOSnPCSGjIcIU%3D" H 3800 4775 50  0001 C CNN "Distributor Link"
	1    3800 5275
	-1   0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_IC:ADP2303ARDZ U58
U 1 1 6178BEE1
P 5200 5375
F 0 "U58" H 4925 5675 50  0000 C CNN
F 1 "ADP2303ARDZ" H 5625 5025 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:SOIC-8-1EP_3.9x4.9mm_P1.27mm_EP2.29x3mm" H 5200 4525 50  0001 C CNN
F 3 "~" H 5200 5325 50  0001 C CNN
F 4 "ADP2303ARDZ -  DC-DC Switching Buck (Step Down) Regulator, Adjustable, 3V-20Vin, 800mV-16Vout, 3Aout, NSOIC-8 " H 5200 4425 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/analog-devices/adp2303ardz/ic-buck-reg-3a-8soic/dp/1843647" H 5200 4325 50  0001 C CNN "Distributor Link"
F 6 "https://www.mouser.co.uk/ProductDetail/Analog-Devices/ADP2303ARDZ?qs=sGAEpiMZZMtitjHzVIkrqYThiPHWFX9aO3OsTR4UYw0%3D" H 5200 4225 50  0001 C CNN "Distributor Link 2"
	1    5200 5375
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR?
U 1 1 6178CCEF
P 5200 5875
AR Path="/5EA5E06A/6178CCEF" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/6178CCEF" Ref="#PWR?"  Part="1" 
AR Path="/5FE92F3B/6178CCEF" Ref="#PWR0451"  Part="1" 
F 0 "#PWR0451" H 5200 5625 50  0001 C CNN
F 1 "MOT_DGND" H 5205 5702 50  0000 C CNN
F 2 "" H 5200 5875 50  0001 C CNN
F 3 "" H 5200 5875 50  0001 C CNN
	1    5200 5875
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 5775 5200 5825
Wire Wire Line
	5300 5775 5300 5825
Wire Wire Line
	5300 5825 5200 5825
Connection ~ 5200 5825
Wire Wire Line
	5200 5825 5200 5875
NoConn ~ 5100 5775
NoConn ~ 5650 5225
$Comp
L PMSM_Driver_LV_Board_Power:MOT_PGND #PWR?
U 1 1 617B0EE6
P 6425 3925
AR Path="/5EA5D9E8/617B0EE6" Ref="#PWR?"  Part="1" 
AR Path="/5FE92F3B/617B0EE6" Ref="#PWR0457"  Part="1" 
F 0 "#PWR0457" H 6425 3675 50  0001 C CNN
F 1 "MOT_PGND" H 6430 3752 50  0000 C CNN
F 2 "" H 6425 3925 50  0001 C CNN
F 3 "" H 6425 3925 50  0001 C CNN
	1    6425 3925
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR?
U 1 1 617B6087
P 6875 3925
AR Path="/5EA5E06A/617B6087" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/617B6087" Ref="#PWR?"  Part="1" 
AR Path="/5FE92F3B/617B6087" Ref="#PWR0460"  Part="1" 
F 0 "#PWR0460" H 6875 3675 50  0001 C CNN
F 1 "MOT_DGND" H 6880 3752 50  0000 C CNN
F 2 "" H 6875 3925 50  0001 C CNN
F 3 "" H 6875 3925 50  0001 C CNN
	1    6875 3925
	1    0    0    -1  
$EndComp
Wire Wire Line
	6425 3925 6425 3825
Wire Wire Line
	6425 3825 6875 3825
Wire Wire Line
	6875 3825 6875 3925
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R206
U 1 1 617D6F0F
P 4250 5275
F 0 "R206" H 4300 5300 50  0000 L CNN
F 1 "576k" H 4300 5225 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 4250 5275 50  0001 C CNN
F 3 "~" H 4250 5275 50  0001 C CNN
F 4 "MCMR04X5763FTL -  SMD Chip Resistor, Ceramic, 0402 [1005 Metric], 576 kohm, MCMR Series, 50 V, Thick Film, 62.5 mW" H 4250 4875 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcmr04x5763ftl/res-576k-1-0-0625w-0402-ceramic/dp/2073149" H 4250 4775 50  0001 C CNN "Distributor Link"
	1    4250 5275
	1    0    0    -1  
$EndComp
Wire Wire Line
	4250 5075 4250 5175
Wire Wire Line
	3800 5075 4250 5075
Connection ~ 3800 5075
Wire Wire Line
	4250 5375 4250 5425
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R207
U 1 1 617F6BDC
P 4250 5575
F 0 "R207" H 4300 5600 50  0000 L CNN
F 1 "100k" H 4300 5525 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 4250 5575 50  0001 C CNN
F 3 "~" H 4250 5575 50  0001 C CNN
F 4 "MCWR04X1003FTL -  SMD Chip Resistor, 0402 [1005 Metric], 100 kohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 4250 5175 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x1003ftl/res-100k-1-0-0625w-0402-thick/dp/2447094" H 4250 5075 50  0001 C CNN "Distributor Link"
	1    4250 5575
	1    0    0    -1  
$EndComp
Wire Wire Line
	4250 5675 4250 5775
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR?
U 1 1 618018A0
P 4250 5775
AR Path="/5EA5E06A/618018A0" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/618018A0" Ref="#PWR?"  Part="1" 
AR Path="/5FE92F3B/618018A0" Ref="#PWR0447"  Part="1" 
F 0 "#PWR0447" H 4250 5525 50  0001 C CNN
F 1 "MOT_DGND" H 4255 5602 50  0000 C CNN
F 2 "" H 4250 5775 50  0001 C CNN
F 3 "" H 4250 5775 50  0001 C CNN
	1    4250 5775
	1    0    0    -1  
$EndComp
Wire Wire Line
	4250 5075 4550 5075
Connection ~ 4250 5075
Wire Wire Line
	4750 5425 4250 5425
Connection ~ 4250 5425
Wire Wire Line
	4250 5425 4250 5475
Wire Wire Line
	4750 5325 4550 5325
Wire Wire Line
	4550 5325 4550 5075
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 6184BB0F
P 5200 4850
AR Path="/5E9C6910/6184BB0F" Ref="C?"  Part="1" 
AR Path="/5EBC6654/6184BB0F" Ref="C?"  Part="1" 
AR Path="/5EBC681B/6184BB0F" Ref="C?"  Part="1" 
AR Path="/5F05E355/6184BB0F" Ref="C?"  Part="1" 
AR Path="/6068445B/6184BB0F" Ref="C?"  Part="1" 
AR Path="/60E816A3/6184BB0F" Ref="C?"  Part="1" 
AR Path="/5EFE605D/6184BB0F" Ref="C?"  Part="1" 
AR Path="/5EFE7331/6184BB0F" Ref="C?"  Part="1" 
AR Path="/5FD7CF82/6184BB0F" Ref="C?"  Part="1" 
AR Path="/5EA5E295/6184BB0F" Ref="C?"  Part="1" 
AR Path="/5FE92F3B/6184BB0F" Ref="C249"  Part="1" 
F 0 "C249" V 5075 4800 50  0000 L CNN
F 1 "100nF" V 5150 4900 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 5200 4850 50  0001 C CNN
F 3 "~" H 5200 4850 50  0001 C CNN
F 4 "MC0603B104K500CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 50 V, 0603 [1608 Metric], ± 10%, X7R, MC Series " H 5200 4450 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0603b104k500ct/cap-0-1-f-50v-10-x7r-0603/dp/1759122" H 5200 4350 50  0001 C CNN "Distributor Link"
	1    5200 4850
	0    1    1    0   
$EndComp
Wire Wire Line
	5100 4850 4650 4850
Wire Wire Line
	4650 4850 4650 5225
Wire Wire Line
	4650 5225 4750 5225
Wire Wire Line
	5650 5425 5750 5425
Wire Wire Line
	5750 5425 5750 4850
Wire Wire Line
	5750 4850 5300 4850
$Comp
L PMSM_Driver_LV_Board_Device:D_Schottky_Small D36
U 1 1 6187B518
P 6200 5000
F 0 "D36" V 6200 5125 50  0000 C CNN
F 1 "B340A-13-F" V 6275 5125 25  0000 C CNN
F 2 "PMSM_Driver_LV_Board:D_SMA" V 6200 5000 50  0001 C CNN
F 3 "~" V 6200 5000 50  0001 C CNN
F 4 "B340A-13-F -  Schottky Rectifier, 40 V, 3 A, Single, DO-214AC (SMA), 2 Pins, 500 mV" H 6200 4800 50  0001 C CNN "Description"
F 5 "https://www.mouser.co.uk/ProductDetail/Diodes-Incorporated/B340A-13-F?qs=%2Fha2pyFaduigxeZuOMhhpl6qAMGjzQx61tI0Tf9cf%252BI%3D" H 6200 4700 50  0001 C CNN "Distributor Link"
	1    6200 5000
	0    1    1    0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR?
U 1 1 618817B1
P 6200 5150
AR Path="/5EA5E06A/618817B1" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/618817B1" Ref="#PWR?"  Part="1" 
AR Path="/5FE92F3B/618817B1" Ref="#PWR0455"  Part="1" 
F 0 "#PWR0455" H 6200 4900 50  0001 C CNN
F 1 "MOT_DGND" H 6205 4977 50  0000 C CNN
F 2 "" H 6200 5150 50  0001 C CNN
F 3 "" H 6200 5150 50  0001 C CNN
	1    6200 5150
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 5150 6200 5100
Wire Wire Line
	6200 4900 6200 4850
Wire Wire Line
	6200 4850 5750 4850
Connection ~ 5750 4850
$Comp
L PMSM_Driver_LV_Board_Device:L_Small L4
U 1 1 618A78ED
P 6550 4850
F 0 "L4" V 6625 4850 50  0000 C CNN
F 1 "3.3uH" V 6475 4850 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:L_Coilcraft_XxL4030" H 6550 4850 50  0001 C CNN
F 3 "~" H 6550 4850 50  0001 C CNN
F 4 "XAL4030-332MEC -  Power Inductor (SMD), 3.3 µH, 6.6 A, Shielded, 5.5 A, XAL40xx Series, 4mm x 4mm x 3.1mm" H 6550 4450 50  0001 C CNN "Description"
F 5 "https://www.mouser.co.uk/ProductDetail/Coilcraft/XAL4030-332MEC?qs=%2Fha2pyFadujuWdOuo3OJjSCihP2QEx1va%252BdhIBTR8W%2FeTRC167RNCg%3D%3D" H 6550 4350 50  0001 C CNN "Distributor Link"
	1    6550 4850
	0    -1   -1   0   
$EndComp
Connection ~ 6200 4850
Wire Wire Line
	6200 4850 6450 4850
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 618B7941
P 6900 5000
AR Path="/5E9C6910/618B7941" Ref="C?"  Part="1" 
AR Path="/5EBC6654/618B7941" Ref="C?"  Part="1" 
AR Path="/5EBC681B/618B7941" Ref="C?"  Part="1" 
AR Path="/5F05E355/618B7941" Ref="C?"  Part="1" 
AR Path="/6068445B/618B7941" Ref="C?"  Part="1" 
AR Path="/60E816A3/618B7941" Ref="C?"  Part="1" 
AR Path="/5EFE605D/618B7941" Ref="C?"  Part="1" 
AR Path="/5EA5E155/618B7941" Ref="C?"  Part="1" 
AR Path="/5FE92F3B/618B7941" Ref="C253"  Part="1" 
F 0 "C253" H 6700 5075 50  0000 L CNN
F 1 "10uF" H 6700 4925 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 6900 5000 50  0001 C CNN
F 3 "~" H 6900 5000 50  0001 C CNN
F 4 "LMK107BBJ106MALT -  SMD Multilayer Ceramic Capacitor, 10 µF, 10 V, 0603 [1608 Metric], ± 20%, X5R, M Series" H 6900 4600 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/taiyo-yuden/lmk107bbj106malt/cap-10-f-10v-20-x5r-0603/dp/2113070" H 6900 4500 50  0001 C CNN "Distributor Link"
	1    6900 5000
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6650 4850 6900 4850
Wire Wire Line
	6900 4900 6900 4850
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR?
U 1 1 618DE5B1
P 6900 5150
AR Path="/5EA5E06A/618DE5B1" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/618DE5B1" Ref="#PWR?"  Part="1" 
AR Path="/5FE92F3B/618DE5B1" Ref="#PWR0458"  Part="1" 
F 0 "#PWR0458" H 6900 4900 50  0001 C CNN
F 1 "MOT_DGND" H 6905 4977 50  0000 C CNN
F 2 "" H 6900 5150 50  0001 C CNN
F 3 "" H 6900 5150 50  0001 C CNN
	1    6900 5150
	1    0    0    -1  
$EndComp
Wire Wire Line
	6900 5150 6900 5100
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 618E511A
P 7375 5000
AR Path="/5E9C6910/618E511A" Ref="C?"  Part="1" 
AR Path="/5EBC6654/618E511A" Ref="C?"  Part="1" 
AR Path="/5EBC681B/618E511A" Ref="C?"  Part="1" 
AR Path="/5F05E355/618E511A" Ref="C?"  Part="1" 
AR Path="/6068445B/618E511A" Ref="C?"  Part="1" 
AR Path="/60E816A3/618E511A" Ref="C?"  Part="1" 
AR Path="/5EFE605D/618E511A" Ref="C?"  Part="1" 
AR Path="/5EA5E155/618E511A" Ref="C?"  Part="1" 
AR Path="/5FE92F3B/618E511A" Ref="C254"  Part="1" 
F 0 "C254" H 7175 5075 50  0000 L CNN
F 1 "10uF" H 7175 4925 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 7375 5000 50  0001 C CNN
F 3 "~" H 7375 5000 50  0001 C CNN
F 4 "LMK107BBJ106MALT -  SMD Multilayer Ceramic Capacitor, 10 µF, 10 V, 0603 [1608 Metric], ± 20%, X5R, M Series" H 7375 4600 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/taiyo-yuden/lmk107bbj106malt/cap-10-f-10v-20-x5r-0603/dp/2113070" H 7375 4500 50  0001 C CNN "Distributor Link"
	1    7375 5000
	-1   0    0    -1  
$EndComp
Wire Wire Line
	7375 4900 7375 4850
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR?
U 1 1 618E5122
P 7375 5150
AR Path="/5EA5E06A/618E5122" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/618E5122" Ref="#PWR?"  Part="1" 
AR Path="/5FE92F3B/618E5122" Ref="#PWR0463"  Part="1" 
F 0 "#PWR0463" H 7375 4900 50  0001 C CNN
F 1 "MOT_DGND" H 7380 4977 50  0000 C CNN
F 2 "" H 7375 5150 50  0001 C CNN
F 3 "" H 7375 5150 50  0001 C CNN
	1    7375 5150
	1    0    0    -1  
$EndComp
Wire Wire Line
	7375 5150 7375 5100
Wire Wire Line
	6900 4850 7375 4850
Connection ~ 6900 4850
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 618F33A7
P 7850 5000
AR Path="/5E9C6910/618F33A7" Ref="C?"  Part="1" 
AR Path="/5EBC6654/618F33A7" Ref="C?"  Part="1" 
AR Path="/5EBC681B/618F33A7" Ref="C?"  Part="1" 
AR Path="/5F05E355/618F33A7" Ref="C?"  Part="1" 
AR Path="/6068445B/618F33A7" Ref="C?"  Part="1" 
AR Path="/60E816A3/618F33A7" Ref="C?"  Part="1" 
AR Path="/5EFE605D/618F33A7" Ref="C?"  Part="1" 
AR Path="/5EA5E155/618F33A7" Ref="C?"  Part="1" 
AR Path="/5FE92F3B/618F33A7" Ref="C255"  Part="1" 
F 0 "C255" H 7650 5075 50  0000 L CNN
F 1 "10uF" H 7650 4925 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 7850 5000 50  0001 C CNN
F 3 "~" H 7850 5000 50  0001 C CNN
F 4 "LMK107BBJ106MALT -  SMD Multilayer Ceramic Capacitor, 10 µF, 10 V, 0603 [1608 Metric], ± 20%, X5R, M Series" H 7850 4600 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/taiyo-yuden/lmk107bbj106malt/cap-10-f-10v-20-x5r-0603/dp/2113070" H 7850 4500 50  0001 C CNN "Distributor Link"
	1    7850 5000
	-1   0    0    -1  
$EndComp
Wire Wire Line
	7850 4900 7850 4850
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR?
U 1 1 618F33AE
P 7850 5150
AR Path="/5EA5E06A/618F33AE" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/618F33AE" Ref="#PWR?"  Part="1" 
AR Path="/5FE92F3B/618F33AE" Ref="#PWR0464"  Part="1" 
F 0 "#PWR0464" H 7850 4900 50  0001 C CNN
F 1 "MOT_DGND" H 7855 4977 50  0000 C CNN
F 2 "" H 7850 5150 50  0001 C CNN
F 3 "" H 7850 5150 50  0001 C CNN
	1    7850 5150
	1    0    0    -1  
$EndComp
Wire Wire Line
	7850 5150 7850 5100
Wire Wire Line
	7375 4850 7625 4850
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 618FABD7
P 8325 5000
AR Path="/5E9C6910/618FABD7" Ref="C?"  Part="1" 
AR Path="/5EBC6654/618FABD7" Ref="C?"  Part="1" 
AR Path="/5EBC681B/618FABD7" Ref="C?"  Part="1" 
AR Path="/5F05E355/618FABD7" Ref="C?"  Part="1" 
AR Path="/6068445B/618FABD7" Ref="C?"  Part="1" 
AR Path="/60E816A3/618FABD7" Ref="C?"  Part="1" 
AR Path="/5EFE605D/618FABD7" Ref="C?"  Part="1" 
AR Path="/5EA5E155/618FABD7" Ref="C?"  Part="1" 
AR Path="/5FE92F3B/618FABD7" Ref="C256"  Part="1" 
F 0 "C256" H 8125 5075 50  0000 L CNN
F 1 "10uF" H 8125 4925 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 8325 5000 50  0001 C CNN
F 3 "~" H 8325 5000 50  0001 C CNN
F 4 "LMK107BBJ106MALT -  SMD Multilayer Ceramic Capacitor, 10 µF, 10 V, 0603 [1608 Metric], ± 20%, X5R, M Series" H 8325 4600 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/taiyo-yuden/lmk107bbj106malt/cap-10-f-10v-20-x5r-0603/dp/2113070" H 8325 4500 50  0001 C CNN "Distributor Link"
	1    8325 5000
	-1   0    0    -1  
$EndComp
Wire Wire Line
	8325 4900 8325 4850
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR?
U 1 1 618FABDE
P 8325 5150
AR Path="/5EA5E06A/618FABDE" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/618FABDE" Ref="#PWR?"  Part="1" 
AR Path="/5FE92F3B/618FABDE" Ref="#PWR0465"  Part="1" 
F 0 "#PWR0465" H 8325 4900 50  0001 C CNN
F 1 "MOT_DGND" H 8330 4977 50  0000 C CNN
F 2 "" H 8325 5150 50  0001 C CNN
F 3 "" H 8325 5150 50  0001 C CNN
	1    8325 5150
	1    0    0    -1  
$EndComp
Wire Wire Line
	8325 5150 8325 5100
Wire Wire Line
	7850 4850 8325 4850
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 6190244D
P 8800 5000
AR Path="/5E9C6910/6190244D" Ref="C?"  Part="1" 
AR Path="/5EBC6654/6190244D" Ref="C?"  Part="1" 
AR Path="/5EBC681B/6190244D" Ref="C?"  Part="1" 
AR Path="/5F05E355/6190244D" Ref="C?"  Part="1" 
AR Path="/6068445B/6190244D" Ref="C?"  Part="1" 
AR Path="/60E816A3/6190244D" Ref="C?"  Part="1" 
AR Path="/5EFE605D/6190244D" Ref="C?"  Part="1" 
AR Path="/5EA5E155/6190244D" Ref="C?"  Part="1" 
AR Path="/5FE92F3B/6190244D" Ref="C257"  Part="1" 
F 0 "C257" H 8600 5075 50  0000 L CNN
F 1 "10uF" H 8550 5000 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 8800 5000 50  0001 C CNN
F 3 "~" H 8800 5000 50  0001 C CNN
F 4 "LMK107BBJ106MALT -  SMD Multilayer Ceramic Capacitor, 10 µF, 10 V, 0603 [1608 Metric], ± 20%, X5R, M Series" H 8800 4600 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/taiyo-yuden/lmk107bbj106malt/cap-10-f-10v-20-x5r-0603/dp/2113070" H 8800 4500 50  0001 C CNN "Distributor Link"
F 6 "DNI" H 8675 4925 50  0000 C CNN "DNI"
	1    8800 5000
	-1   0    0    -1  
$EndComp
Wire Wire Line
	8800 4900 8800 4850
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR?
U 1 1 61902454
P 8800 5150
AR Path="/5EA5E06A/61902454" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/61902454" Ref="#PWR?"  Part="1" 
AR Path="/5FE92F3B/61902454" Ref="#PWR0466"  Part="1" 
F 0 "#PWR0466" H 8800 4900 50  0001 C CNN
F 1 "MOT_DGND" H 8805 4977 50  0000 C CNN
F 2 "" H 8800 5150 50  0001 C CNN
F 3 "" H 8800 5150 50  0001 C CNN
	1    8800 5150
	1    0    0    -1  
$EndComp
Wire Wire Line
	8800 5150 8800 5100
Wire Wire Line
	8325 4850 8800 4850
$Comp
L PMSM_Driver_LV_Board_Device:Ferrite_Bead_Small FB?
U 1 1 6190AA3F
P 9325 4850
AR Path="/5EA5E155/6190AA3F" Ref="FB?"  Part="1" 
AR Path="/5ED47D19/6190AA3F" Ref="FB?"  Part="1" 
AR Path="/5FE92F3B/6190AA3F" Ref="FB10"  Part="1" 
F 0 "FB10" V 9450 4775 50  0000 L CNN
F 1 "Z0805C420APWST" V 9200 4675 25  0000 L CNN
F 2 "PMSM_Driver_LV_Board:L_0805_2012Metric" V 9255 4850 50  0001 C CNN
F 3 "~" H 9325 4850 50  0001 C CNN
F 4 "Z0805C420APWST -  Ferrite Bead, 0805 [2012 Metric], 42 ohm, 4 A, Z-PWS Series, 0.008 ohm, ± 25%" H 9325 4850 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/kemet/z0805c420apwst/ferrite-bead-42-ohm-4a-0805/dp/2846741" H 9325 4850 50  0001 C CNN "Distributor Link"
F 6 "42 Ohms at 100 MHz" V 9150 4625 25  0000 L CNN "Impedance"
	1    9325 4850
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8800 4850 9225 4850
Connection ~ 7375 4850
Connection ~ 7850 4850
Connection ~ 8325 4850
Connection ~ 8800 4850
Wire Wire Line
	9600 4750 9600 4850
Wire Wire Line
	9600 4850 9425 4850
Wire Wire Line
	10000 4750 10000 4850
Wire Wire Line
	10000 4850 9600 4850
Connection ~ 9600 4850
Connection ~ 7625 4850
Wire Wire Line
	7625 4850 7850 4850
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R214
U 1 1 61976C81
P 7425 5525
F 0 "R214" V 7500 5425 50  0000 L CNN
F 1 "53k6" V 7325 5425 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 7425 5525 50  0001 C CNN
F 3 "~" H 7425 5525 50  0001 C CNN
F 4 "MCMR04X5362FTL -  SMD Chip Resistor, Ceramic, 0402 [1005 Metric], 53.6 kohm, MCMR Series, 50 V, Thick Film, 62.5 mW" H 7425 5125 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcmr04x5362ftl/res-53k6-1-0-0625w-0402-ceramic/dp/2073119" H 7425 5025 50  0001 C CNN "Distributor Link"
	1    7425 5525
	0    -1   -1   0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R211
U 1 1 61976C8A
P 7050 5700
F 0 "R211" H 7100 5725 50  0000 L CNN
F 1 "10k" H 7100 5650 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 7050 5700 50  0001 C CNN
F 3 "~" H 7050 5700 50  0001 C CNN
F 4 "MCWR04X1002FTL -  SMD Chip Resistor, 0402 [1005 Metric], 10 kohm, MCWR Series, 50 V, Thick Film, 62.5 mW " H 7050 5300 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x1002ftl/res-10k-1-0-0625w-thick-film/dp/2447096" H 7050 5200 50  0001 C CNN "Distributor Link"
	1    7050 5700
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:MOT_DGND #PWR?
U 1 1 61976C91
P 7050 5875
AR Path="/5EA5E06A/61976C91" Ref="#PWR?"  Part="1" 
AR Path="/5ED47D19/61976C91" Ref="#PWR?"  Part="1" 
AR Path="/5FE92F3B/61976C91" Ref="#PWR0459"  Part="1" 
F 0 "#PWR0459" H 7050 5625 50  0001 C CNN
F 1 "MOT_DGND" H 7055 5702 50  0000 C CNN
F 2 "" H 7050 5875 50  0001 C CNN
F 3 "" H 7050 5875 50  0001 C CNN
	1    7050 5875
	1    0    0    -1  
$EndComp
Wire Wire Line
	7525 5525 7625 5525
Wire Wire Line
	7625 4850 7625 5525
Wire Wire Line
	7050 5600 7050 5525
Wire Wire Line
	7050 5525 7325 5525
Wire Wire Line
	7050 5875 7050 5800
Wire Wire Line
	7050 5525 5850 5525
Wire Wire Line
	5850 5525 5850 5325
Wire Wire Line
	5850 5325 5650 5325
Connection ~ 7050 5525
Wire Notes Line
	10250 4325 10250 6200
Wire Notes Line
	10250 6200 600  6200
Wire Notes Line
	600  4325 600  6200
Wire Notes Line
	600  4325 10250 4325
Text Notes 700  875  0    25   ~ 0
MOT_15V0 is actually 13.07V in this set up.
Wire Notes Line
	600  4250 7625 4250
Wire Notes Line
	600  600  7625 600 
$Comp
L power:PWR_FLAG #FLG?
U 1 1 61CAD894
P 1425 5050
AR Path="/5EA5E06A/61CAD894" Ref="#FLG?"  Part="1" 
AR Path="/5EA5E155/61CAD894" Ref="#FLG?"  Part="1" 
AR Path="/5FE92F3B/61CAD894" Ref="#FLG029"  Part="1" 
F 0 "#FLG029" H 1425 5125 50  0001 C CNN
F 1 "PWR_FLAG" H 1425 5204 25  0000 C CNN
F 2 "" H 1425 5050 50  0001 C CNN
F 3 "~" H 1425 5050 50  0001 C CNN
	1    1425 5050
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1425 5075 1425 5050
Connection ~ 1425 5075
Wire Wire Line
	1425 5075 1550 5075
Wire Wire Line
	3300 2125 3300 2475
$Comp
L PMSM_Driver_LV_Board_Power:ISO_DGND #PWR0336
U 1 1 5F111A09
P 7325 3925
F 0 "#PWR0336" H 7325 3675 50  0001 C CNN
F 1 "ISO_DGND" H 7330 3752 50  0000 C CNN
F 2 "" H 7325 3925 50  0001 C CNN
F 3 "" H 7325 3925 50  0001 C CNN
	1    7325 3925
	1    0    0    -1  
$EndComp
Wire Wire Line
	6875 3825 7325 3825
Wire Wire Line
	7325 3825 7325 3925
Connection ~ 6875 3825
$EndSCHEMATC
