EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 16
Title "PMSM_Driver_LV_Board"
Date "2020-03-30"
Rev "A"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text GLabel 5725 1850 0    50   Input ~ 0
CTRL_ADC_CLK
Text GLabel 1800 1850 0    50   Input ~ 0
CTRL_VT_ENABLE
Text GLabel 5725 1950 0    50   Input ~ 0
CTRL_PWM_AH
Text GLabel 5725 2050 0    50   Input ~ 0
CTRL_PWM_AL
Text GLabel 5725 2150 0    50   Input ~ 0
CTRL_PWM_BH
Text GLabel 5725 2250 0    50   Input ~ 0
CTRL_PWM_BL
Text GLabel 5725 2350 0    50   Input ~ 0
CTRL_PWM_CH
Text GLabel 5725 2450 0    50   Input ~ 0
CTRL_PWM_CL
Text GLabel 1800 1950 0    50   Input ~ 0
CTRL_MOT_EN
Text GLabel 1800 2050 0    50   Input ~ 0
CTRL_RST_OVR_CUR
Text GLabel 1800 2150 0    50   Output ~ 0
CTRL_NOT_OVR_CUR
Text GLabel 2350 2650 0    50   Input ~ 0
CTRL_POWER_GOOD
Text GLabel 1800 4150 0    50   Output ~ 0
CTRL_V_A
Text GLabel 1800 4250 0    50   Output ~ 0
CTRL_V_B
Text GLabel 1800 4350 0    50   Output ~ 0
CTRL_V_C
Text GLabel 1800 4450 0    50   Output ~ 0
CTRL_CUR_A
Text GLabel 1800 4550 0    50   Output ~ 0
CTRL_CUR_B
Text GLabel 1800 4650 0    50   Output ~ 0
CTRL_CUR_T
Text GLabel 5725 4150 0    50   Output ~ 0
CTRL_ENC_I
Text GLabel 5725 4250 0    50   Output ~ 0
CTRL_RES_A
Text GLabel 5725 4350 0    50   Output ~ 0
CTRL_RES_B
Text GLabel 5725 4550 0    50   Output ~ 0
CTRL_HAL_A
Text GLabel 5725 4650 0    50   Output ~ 0
CTRL_HAL_B
Text GLabel 1800 6175 0    50   Output ~ 0
CTRL_TEMP
Text GLabel 5725 4450 0    50   Output ~ 0
CTRL_RES_I
Text GLabel 5725 4050 0    50   Output ~ 0
CTRL_ENC_B
Text GLabel 1800 4750 0    50   Output ~ 0
CTRL_ENC_A
Text GLabel 1800 4050 0    50   Output ~ 0
CTRL_V_SUP
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_VADJ #PWR?
U 1 1 5EBCAE3A
P 2450 1400
AR Path="/5E9C6910/5EBCAE3A" Ref="#PWR?"  Part="1" 
AR Path="/5EBC6654/5EBCAE3A" Ref="#PWR?"  Part="1" 
AR Path="/5EBC681B/5EBCAE3A" Ref="#PWR?"  Part="1" 
AR Path="/5EA5DDD6/5EBCAE3A" Ref="#PWR047"  Part="1" 
F 0 "#PWR047" H 2450 1650 50  0001 C CNN
F 1 "CTRL_VADJ" H 2451 1573 50  0000 C CNN
F 2 "" H 2450 1400 50  0001 C CNN
F 3 "" H 2450 1400 50  0001 C CNN
	1    2450 1400
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5EBCD792
P 3550 1450
AR Path="/5E9C6910/5EBCD792" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5EBCD792" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5EBCD792" Ref="C?"  Part="1" 
AR Path="/5EA5DDD6/5EBCD792" Ref="C11"  Part="1" 
F 0 "C11" V 3675 1400 50  0000 L CNN
F 1 "100nF" V 3600 1175 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 3550 1450 50  0001 C CNN
F 3 "~" H 3550 1450 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 3550 1050 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 3550 950 50  0001 C CNN "Distributor Link"
	1    3550 1450
	0    -1   -1   0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_IC:ADG3308BRUZ U1
U 1 1 5EBD2301
P 2900 2200
F 0 "U1" H 2700 2800 50  0000 C CNN
F 1 "ADG3308BRUZ" H 2900 1600 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:TSSOP-20_4.4x6.5mm_P0.65mm" H 2950 1500 31  0001 C CNN
F 3 "~" H 2500 2850 31  0001 C CNN
F 4 "ADG3308BRUZ -  Voltage Level Translator, Bidirectional, 8 Input, 6 ns, 60 Mbps, 1.15 V to 5.5 V, TSSOP-20" H 2900 1450 31  0001 C CNN "Description"
F 5 "https://uk.farnell.com/analog-devices/adg3308bruz/ic-translator-smd-3308-tssop20/dp/9453083" H 2900 1400 31  0001 C CNN "Distributor Link"
F 6 "https://www.mouser.co.uk/ProductDetail/Analog-Devices/ADG3308BRUZ?qs=sGAEpiMZZMsty6Jaj0%252BBBupSmhSNoeUMOTLXgC%252BMoYY%3D" H 2900 1350 31  0001 C CNN "Distributor Link 2"
	1    2900 2200
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_3V3 #PWR050
U 1 1 5EBD4C70
P 3350 1400
F 0 "#PWR050" H 3350 1650 50  0001 C CNN
F 1 "CTRL_3V3" H 3351 1573 50  0000 C CNN
F 2 "" H 3350 1400 50  0001 C CNN
F 3 "" H 3350 1400 50  0001 C CNN
	1    3350 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3250 1750 3350 1750
Wire Wire Line
	3350 1450 3450 1450
Wire Wire Line
	3350 1400 3350 1450
Wire Wire Line
	3650 1450 3750 1450
Wire Wire Line
	3750 1450 3750 1500
Wire Wire Line
	2550 1750 2450 1750
Wire Wire Line
	2450 1450 2350 1450
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R37
U 1 1 5EBE037B
P 3500 1850
F 0 "R37" V 3450 1725 50  0000 C CNN
F 1 "33" V 3450 1950 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 3500 1850 50  0001 C CNN
F 3 "~" H 3500 1850 50  0001 C CNN
F 4 "MCWR04X33R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 33 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 3500 1450 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x33r0ftl/res-33r-1-0-0625w-thick-film/dp/2447161" H 3500 1350 50  0001 C CNN "Distributor Link"
	1    3500 1850
	0    1    1    0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R47
U 1 1 5EBE6414
P 3750 1950
F 0 "R47" V 3700 1825 50  0000 C CNN
F 1 "33" V 3700 2050 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 3750 1950 50  0001 C CNN
F 3 "~" H 3750 1950 50  0001 C CNN
F 4 "MCWR04X33R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 33 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 3750 1550 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x33r0ftl/res-33r-1-0-0625w-thick-film/dp/2447161" H 3750 1450 50  0001 C CNN "Distributor Link"
	1    3750 1950
	0    1    1    0   
$EndComp
Wire Wire Line
	3350 1450 3350 1750
Connection ~ 3350 1450
Wire Wire Line
	2050 1450 2050 1500
Wire Wire Line
	2150 1450 2050 1450
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5EBDE5A2
P 2250 1450
AR Path="/5E9C6910/5EBDE5A2" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5EBDE5A2" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5EBDE5A2" Ref="C?"  Part="1" 
AR Path="/5EA5DDD6/5EBDE5A2" Ref="C8"  Part="1" 
F 0 "C8" V 2375 1400 50  0000 L CNN
F 1 "100nF" V 2300 1175 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 2250 1450 50  0001 C CNN
F 3 "~" H 2250 1450 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 2250 1050 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 2250 950 50  0001 C CNN "Distributor Link"
	1    2250 1450
	0    1    -1   0   
$EndComp
Wire Wire Line
	2450 1450 2450 1750
Wire Wire Line
	2450 1450 2450 1400
Connection ~ 2450 1450
Wire Wire Line
	3400 1850 3250 1850
Wire Wire Line
	3250 1950 3650 1950
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R38
U 1 1 5EBEF2B8
P 3500 2050
F 0 "R38" V 3450 1900 50  0000 C CNN
F 1 "33" V 3450 2150 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 3500 2050 50  0001 C CNN
F 3 "~" H 3500 2050 50  0001 C CNN
F 4 "MCWR04X33R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 33 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 3500 1650 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x33r0ftl/res-33r-1-0-0625w-thick-film/dp/2447161" H 3500 1550 50  0001 C CNN "Distributor Link"
	1    3500 2050
	0    1    1    0   
$EndComp
Wire Wire Line
	3400 2050 3250 2050
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R48
U 1 1 5EBEFA19
P 3750 2150
F 0 "R48" V 3700 2025 50  0000 C CNN
F 1 "33" V 3700 2250 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 3750 2150 50  0001 C CNN
F 3 "~" H 3750 2150 50  0001 C CNN
F 4 "MCWR04X33R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 33 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 3750 1750 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x33r0ftl/res-33r-1-0-0625w-thick-film/dp/2447161" H 3750 1650 50  0001 C CNN "Distributor Link"
	1    3750 2150
	0    1    1    0   
$EndComp
Wire Wire Line
	3250 2150 3650 2150
Wire Wire Line
	3350 2650 3250 2650
Wire Wire Line
	3350 2650 3350 2750
Wire Wire Line
	4000 1950 3850 1950
Wire Wire Line
	4000 2150 3850 2150
Wire Wire Line
	3600 1850 4000 1850
Wire Wire Line
	3600 2050 4000 2050
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R25
U 1 1 5EC02413
P 2300 1850
F 0 "R25" V 2250 1725 50  0000 C CNN
F 1 "33" V 2250 1950 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 2300 1850 50  0001 C CNN
F 3 "~" H 2300 1850 50  0001 C CNN
F 4 "MCWR04X33R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 33 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 2300 1450 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x33r0ftl/res-33r-1-0-0625w-thick-film/dp/2447161" H 2300 1350 50  0001 C CNN "Distributor Link"
	1    2300 1850
	0    1    1    0   
$EndComp
Wire Wire Line
	1800 1850 2200 1850
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R13
U 1 1 5EC0241D
P 2050 1950
F 0 "R13" V 2000 1825 50  0000 C CNN
F 1 "33" V 2000 2075 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 2050 1950 50  0001 C CNN
F 3 "~" H 2050 1950 50  0001 C CNN
F 4 "MCWR04X33R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 33 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 2050 1550 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x33r0ftl/res-33r-1-0-0625w-thick-film/dp/2447161" H 2050 1450 50  0001 C CNN "Distributor Link"
	1    2050 1950
	0    1    1    0   
$EndComp
Wire Wire Line
	1950 1950 1800 1950
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R26
U 1 1 5EC02426
P 2300 2050
F 0 "R26" V 2250 1925 50  0000 C CNN
F 1 "33" V 2250 2150 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 2300 2050 50  0001 C CNN
F 3 "~" H 2300 2050 50  0001 C CNN
F 4 "MCWR04X33R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 33 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 2300 1650 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x33r0ftl/res-33r-1-0-0625w-thick-film/dp/2447161" H 2300 1550 50  0001 C CNN "Distributor Link"
	1    2300 2050
	0    1    1    0   
$EndComp
Wire Wire Line
	1800 2050 2200 2050
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R14
U 1 1 5EC0242F
P 2050 2150
F 0 "R14" V 2000 2025 50  0000 C CNN
F 1 "33" V 2000 2250 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 2050 2150 50  0001 C CNN
F 3 "~" H 2050 2150 50  0001 C CNN
F 4 "MCWR04X33R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 33 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 2050 1750 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x33r0ftl/res-33r-1-0-0625w-thick-film/dp/2447161" H 2050 1650 50  0001 C CNN "Distributor Link"
	1    2050 2150
	0    1    1    0   
$EndComp
Wire Wire Line
	1950 2150 1800 2150
Wire Wire Line
	2550 1850 2400 1850
Wire Wire Line
	2550 2050 2400 2050
Wire Wire Line
	2150 1950 2550 1950
Wire Wire Line
	2150 2150 2550 2150
Text Notes 1575 2800 0    25   ~ 0
CTRL_POWER_GOOD is VCCY (i.e. CTRL_3V3)\nsignal. See ADG3308BRUZ datasheet.
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_VADJ #PWR?
U 1 1 5EC41DD4
P 6375 1400
AR Path="/5E9C6910/5EC41DD4" Ref="#PWR?"  Part="1" 
AR Path="/5EBC6654/5EC41DD4" Ref="#PWR?"  Part="1" 
AR Path="/5EBC681B/5EC41DD4" Ref="#PWR?"  Part="1" 
AR Path="/5EA5DDD6/5EC41DD4" Ref="#PWR062"  Part="1" 
F 0 "#PWR062" H 6375 1650 50  0001 C CNN
F 1 "CTRL_VADJ" H 6376 1573 50  0000 C CNN
F 2 "" H 6375 1400 50  0001 C CNN
F 3 "" H 6375 1400 50  0001 C CNN
	1    6375 1400
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5EC41DDC
P 7475 1450
AR Path="/5E9C6910/5EC41DDC" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5EC41DDC" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5EC41DDC" Ref="C?"  Part="1" 
AR Path="/5EA5DDD6/5EC41DDC" Ref="C16"  Part="1" 
F 0 "C16" V 7600 1400 50  0000 L CNN
F 1 "100nF" V 7525 1175 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 7475 1450 50  0001 C CNN
F 3 "~" H 7475 1450 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 7475 1050 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 7475 950 50  0001 C CNN "Distributor Link"
	1    7475 1450
	0    -1   -1   0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_IC:ADG3308BRUZ U4
U 1 1 5EC41DEB
P 6825 2200
F 0 "U4" H 6625 2800 50  0000 C CNN
F 1 "ADG3308BRUZ" H 6825 1600 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:TSSOP-20_4.4x6.5mm_P0.65mm" H 6875 1500 31  0001 C CNN
F 3 "~" H 6425 2850 31  0001 C CNN
F 4 "ADG3308BRUZ -  Voltage Level Translator, Bidirectional, 8 Input, 6 ns, 60 Mbps, 1.15 V to 5.5 V, TSSOP-20" H 6825 1450 31  0001 C CNN "Description"
F 5 "https://uk.farnell.com/analog-devices/adg3308bruz/ic-translator-smd-3308-tssop20/dp/9453083" H 6825 1400 31  0001 C CNN "Distributor Link"
F 6 "https://www.mouser.co.uk/ProductDetail/Analog-Devices/ADG3308BRUZ?qs=sGAEpiMZZMsty6Jaj0%252BBBupSmhSNoeUMOTLXgC%252BMoYY%3D" H 6825 1350 31  0001 C CNN "Distributor Link 2"
	1    6825 2200
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_3V3 #PWR064
U 1 1 5EC41DF1
P 7275 1400
F 0 "#PWR064" H 7275 1650 50  0001 C CNN
F 1 "CTRL_3V3" H 7276 1573 50  0000 C CNN
F 2 "" H 7275 1400 50  0001 C CNN
F 3 "" H 7275 1400 50  0001 C CNN
	1    7275 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	7175 1750 7275 1750
Wire Wire Line
	7275 1450 7375 1450
Wire Wire Line
	7275 1400 7275 1450
Wire Wire Line
	7575 1450 7675 1450
Wire Wire Line
	7675 1450 7675 1500
Wire Wire Line
	6475 1750 6375 1750
Wire Wire Line
	6375 1450 6275 1450
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R72
U 1 1 5EC41E01
P 7425 1850
F 0 "R72" V 7375 1725 50  0000 C CNN
F 1 "33" V 7375 1950 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 7425 1850 50  0001 C CNN
F 3 "~" H 7425 1850 50  0001 C CNN
F 4 "MCWR04X33R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 33 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 7425 1450 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x33r0ftl/res-33r-1-0-0625w-thick-film/dp/2447161" H 7425 1350 50  0001 C CNN "Distributor Link"
	1    7425 1850
	0    1    1    0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R80
U 1 1 5EC41E09
P 7675 1950
F 0 "R80" V 7625 1825 50  0000 C CNN
F 1 "33" V 7625 2050 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 7675 1950 50  0001 C CNN
F 3 "~" H 7675 1950 50  0001 C CNN
F 4 "MCWR04X33R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 33 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 7675 1550 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x33r0ftl/res-33r-1-0-0625w-thick-film/dp/2447161" H 7675 1450 50  0001 C CNN "Distributor Link"
	1    7675 1950
	0    1    1    0   
$EndComp
Wire Wire Line
	7275 1450 7275 1750
Connection ~ 7275 1450
Wire Wire Line
	5975 1450 5975 1500
Wire Wire Line
	6075 1450 5975 1450
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5EC41E1B
P 6175 1450
AR Path="/5E9C6910/5EC41E1B" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5EC41E1B" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5EC41E1B" Ref="C?"  Part="1" 
AR Path="/5EA5DDD6/5EC41E1B" Ref="C14"  Part="1" 
F 0 "C14" V 6300 1400 50  0000 L CNN
F 1 "100nF" V 6225 1175 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 6175 1450 50  0001 C CNN
F 3 "~" H 6175 1450 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 6175 1050 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 6175 950 50  0001 C CNN "Distributor Link"
	1    6175 1450
	0    1    -1   0   
$EndComp
Wire Wire Line
	6375 1450 6375 1750
Wire Wire Line
	6375 1450 6375 1400
Connection ~ 6375 1450
Wire Wire Line
	7325 1850 7175 1850
Wire Wire Line
	7175 1950 7575 1950
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R73
U 1 1 5EC41E28
P 7425 2050
F 0 "R73" V 7375 1925 50  0000 C CNN
F 1 "33" V 7375 2150 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 7425 2050 50  0001 C CNN
F 3 "~" H 7425 2050 50  0001 C CNN
F 4 "MCWR04X33R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 33 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 7425 1650 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x33r0ftl/res-33r-1-0-0625w-thick-film/dp/2447161" H 7425 1550 50  0001 C CNN "Distributor Link"
	1    7425 2050
	0    1    1    0   
$EndComp
Wire Wire Line
	7325 2050 7175 2050
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R81
U 1 1 5EC41E31
P 7675 2150
F 0 "R81" V 7625 2025 50  0000 C CNN
F 1 "33" V 7625 2250 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 7675 2150 50  0001 C CNN
F 3 "~" H 7675 2150 50  0001 C CNN
F 4 "MCWR04X33R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 33 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 7675 1750 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x33r0ftl/res-33r-1-0-0625w-thick-film/dp/2447161" H 7675 1650 50  0001 C CNN "Distributor Link"
	1    7675 2150
	0    1    1    0   
$EndComp
Wire Wire Line
	7175 2150 7575 2150
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R74
U 1 1 5EC41E3A
P 7425 2250
F 0 "R74" V 7375 2125 50  0000 C CNN
F 1 "33" V 7375 2350 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 7425 2250 50  0001 C CNN
F 3 "~" H 7425 2250 50  0001 C CNN
F 4 "MCWR04X33R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 33 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 7425 1850 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x33r0ftl/res-33r-1-0-0625w-thick-film/dp/2447161" H 7425 1750 50  0001 C CNN "Distributor Link"
	1    7425 2250
	0    1    1    0   
$EndComp
Wire Wire Line
	7325 2250 7175 2250
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R82
U 1 1 5EC41E43
P 7675 2350
F 0 "R82" V 7625 2225 50  0000 C CNN
F 1 "33" V 7625 2450 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 7675 2350 50  0001 C CNN
F 3 "~" H 7675 2350 50  0001 C CNN
F 4 "MCWR04X33R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 33 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 7675 1950 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x33r0ftl/res-33r-1-0-0625w-thick-film/dp/2447161" H 7675 1850 50  0001 C CNN "Distributor Link"
	1    7675 2350
	0    1    1    0   
$EndComp
Wire Wire Line
	7175 2350 7575 2350
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R75
U 1 1 5EC41E4C
P 7425 2450
F 0 "R75" V 7375 2325 50  0000 C CNN
F 1 "33" V 7375 2550 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 7425 2450 50  0001 C CNN
F 3 "~" H 7425 2450 50  0001 C CNN
F 4 "MCWR04X33R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 33 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 7425 2050 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x33r0ftl/res-33r-1-0-0625w-thick-film/dp/2447161" H 7425 1950 50  0001 C CNN "Distributor Link"
	1    7425 2450
	0    1    1    0   
$EndComp
Wire Wire Line
	7325 2450 7175 2450
Wire Wire Line
	7275 2650 7175 2650
Wire Wire Line
	7275 2650 7275 2750
Wire Wire Line
	7925 1950 7775 1950
Wire Wire Line
	7925 2350 7775 2350
Wire Wire Line
	7925 2150 7775 2150
Wire Wire Line
	7525 1850 7925 1850
Wire Wire Line
	7525 2050 7925 2050
Wire Wire Line
	7525 2250 7925 2250
Wire Wire Line
	7525 2450 7925 2450
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R64
U 1 1 5EC41E76
P 6225 1850
F 0 "R64" V 6175 1725 50  0000 C CNN
F 1 "33" V 6175 1950 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 6225 1850 50  0001 C CNN
F 3 "~" H 6225 1850 50  0001 C CNN
F 4 "MCWR04X33R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 33 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 6225 1450 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x33r0ftl/res-33r-1-0-0625w-thick-film/dp/2447161" H 6225 1350 50  0001 C CNN "Distributor Link"
	1    6225 1850
	0    1    1    0   
$EndComp
Wire Wire Line
	5725 1850 6125 1850
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R56
U 1 1 5EC41E80
P 5975 1950
F 0 "R56" V 5925 1825 50  0000 C CNN
F 1 "33" V 5925 2050 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 5975 1950 50  0001 C CNN
F 3 "~" H 5975 1950 50  0001 C CNN
F 4 "MCWR04X33R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 33 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 5975 1550 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x33r0ftl/res-33r-1-0-0625w-thick-film/dp/2447161" H 5975 1450 50  0001 C CNN "Distributor Link"
	1    5975 1950
	0    1    1    0   
$EndComp
Wire Wire Line
	5875 1950 5725 1950
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R65
U 1 1 5EC41E89
P 6225 2050
F 0 "R65" V 6175 1925 50  0000 C CNN
F 1 "33" V 6175 2150 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 6225 2050 50  0001 C CNN
F 3 "~" H 6225 2050 50  0001 C CNN
F 4 "MCWR04X33R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 33 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 6225 1650 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x33r0ftl/res-33r-1-0-0625w-thick-film/dp/2447161" H 6225 1550 50  0001 C CNN "Distributor Link"
	1    6225 2050
	0    1    1    0   
$EndComp
Wire Wire Line
	5725 2050 6125 2050
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R57
U 1 1 5EC41E92
P 5975 2150
F 0 "R57" V 5925 2025 50  0000 C CNN
F 1 "33" V 5925 2250 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 5975 2150 50  0001 C CNN
F 3 "~" H 5975 2150 50  0001 C CNN
F 4 "MCWR04X33R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 33 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 5975 1750 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x33r0ftl/res-33r-1-0-0625w-thick-film/dp/2447161" H 5975 1650 50  0001 C CNN "Distributor Link"
	1    5975 2150
	0    1    1    0   
$EndComp
Wire Wire Line
	5875 2150 5725 2150
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R66
U 1 1 5EC41E9B
P 6225 2250
F 0 "R66" V 6175 2125 50  0000 C CNN
F 1 "33" V 6175 2350 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 6225 2250 50  0001 C CNN
F 3 "~" H 6225 2250 50  0001 C CNN
F 4 "MCWR04X33R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 33 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 6225 1850 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x33r0ftl/res-33r-1-0-0625w-thick-film/dp/2447161" H 6225 1750 50  0001 C CNN "Distributor Link"
	1    6225 2250
	0    1    1    0   
$EndComp
Wire Wire Line
	5725 2250 6125 2250
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R58
U 1 1 5EC41EA4
P 5975 2350
F 0 "R58" V 5925 2225 50  0000 C CNN
F 1 "33" V 5925 2450 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 5975 2350 50  0001 C CNN
F 3 "~" H 5975 2350 50  0001 C CNN
F 4 "MCWR04X33R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 33 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 5975 1950 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x33r0ftl/res-33r-1-0-0625w-thick-film/dp/2447161" H 5975 1850 50  0001 C CNN "Distributor Link"
	1    5975 2350
	0    1    1    0   
$EndComp
Wire Wire Line
	5875 2350 5725 2350
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R67
U 1 1 5EC41EAD
P 6225 2450
F 0 "R67" V 6175 2325 50  0000 C CNN
F 1 "33" V 6175 2550 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 6225 2450 50  0001 C CNN
F 3 "~" H 6225 2450 50  0001 C CNN
F 4 "MCWR04X33R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 33 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 6225 2050 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x33r0ftl/res-33r-1-0-0625w-thick-film/dp/2447161" H 6225 1950 50  0001 C CNN "Distributor Link"
	1    6225 2450
	0    1    1    0   
$EndComp
Wire Wire Line
	5725 2450 6125 2450
Wire Wire Line
	6475 1850 6325 1850
Wire Wire Line
	6475 2450 6325 2450
Wire Wire Line
	6475 2250 6325 2250
Wire Wire Line
	6475 2050 6325 2050
Wire Wire Line
	6075 1950 6475 1950
Wire Wire Line
	6075 2150 6475 2150
Wire Wire Line
	6075 2350 6475 2350
Wire Wire Line
	2350 2650 2550 2650
NoConn ~ 3250 2550
NoConn ~ 3250 2450
NoConn ~ 3250 2350
NoConn ~ 3250 2250
Text GLabel 4000 1850 2    50   Output ~ 0
TR_VT_ENABLE
Text GLabel 4000 1950 2    50   Output ~ 0
TR_MOT_EN
Text GLabel 4000 2050 2    50   Output ~ 0
TR_RST_OVR_CUR
Text GLabel 4000 2150 2    50   Input ~ 0
TR_NOT_OVR_CUR
Text GLabel 6275 2650 0    50   Input ~ 0
TR_VT_ENABLE
Wire Wire Line
	6275 2650 6475 2650
Text GLabel 7925 1850 2    50   Output ~ 0
TR_ADC_CLK
Text GLabel 7925 1950 2    50   Output ~ 0
TR_PWM_AH
Text GLabel 7925 2050 2    50   Output ~ 0
TR_PWM_AL
Text GLabel 7925 2150 2    50   Output ~ 0
TR_PWM_BH
Text GLabel 7925 2250 2    50   Output ~ 0
TR_PWM_BL
Text GLabel 7925 2350 2    50   Output ~ 0
TR_PWM_CH
Text GLabel 7925 2450 2    50   Output ~ 0
TR_PWM_CL
NoConn ~ 7175 2550
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_VADJ #PWR?
U 1 1 5ECEAEA1
P 2450 3600
AR Path="/5E9C6910/5ECEAEA1" Ref="#PWR?"  Part="1" 
AR Path="/5EBC6654/5ECEAEA1" Ref="#PWR?"  Part="1" 
AR Path="/5EBC681B/5ECEAEA1" Ref="#PWR?"  Part="1" 
AR Path="/5EA5DDD6/5ECEAEA1" Ref="#PWR048"  Part="1" 
F 0 "#PWR048" H 2450 3850 50  0001 C CNN
F 1 "CTRL_VADJ" H 2451 3773 50  0000 C CNN
F 2 "" H 2450 3600 50  0001 C CNN
F 3 "" H 2450 3600 50  0001 C CNN
	1    2450 3600
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5ECEAEA9
P 3550 3650
AR Path="/5E9C6910/5ECEAEA9" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5ECEAEA9" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5ECEAEA9" Ref="C?"  Part="1" 
AR Path="/5EA5DDD6/5ECEAEA9" Ref="C12"  Part="1" 
F 0 "C12" V 3675 3600 50  0000 L CNN
F 1 "100nF" V 3600 3375 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 3550 3650 50  0001 C CNN
F 3 "~" H 3550 3650 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 3550 3250 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 3550 3150 50  0001 C CNN "Distributor Link"
	1    3550 3650
	0    -1   -1   0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_IC:ADG3308BRUZ U2
U 1 1 5ECEAEB8
P 2900 4400
F 0 "U2" H 2700 5000 50  0000 C CNN
F 1 "ADG3308BRUZ" H 2900 3800 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:TSSOP-20_4.4x6.5mm_P0.65mm" H 2950 3700 31  0001 C CNN
F 3 "~" H 2500 5050 31  0001 C CNN
F 4 "ADG3308BRUZ -  Voltage Level Translator, Bidirectional, 8 Input, 6 ns, 60 Mbps, 1.15 V to 5.5 V, TSSOP-20" H 2900 3650 31  0001 C CNN "Description"
F 5 "https://uk.farnell.com/analog-devices/adg3308bruz/ic-translator-smd-3308-tssop20/dp/9453083" H 2900 3600 31  0001 C CNN "Distributor Link"
F 6 "https://www.mouser.co.uk/ProductDetail/Analog-Devices/ADG3308BRUZ?qs=sGAEpiMZZMsty6Jaj0%252BBBupSmhSNoeUMOTLXgC%252BMoYY%3D" H 2900 3550 31  0001 C CNN "Distributor Link 2"
	1    2900 4400
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_3V3 #PWR052
U 1 1 5ECEAEBE
P 3350 3600
F 0 "#PWR052" H 3350 3850 50  0001 C CNN
F 1 "CTRL_3V3" H 3351 3773 50  0000 C CNN
F 2 "" H 3350 3600 50  0001 C CNN
F 3 "" H 3350 3600 50  0001 C CNN
	1    3350 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	3250 3950 3350 3950
Wire Wire Line
	3350 3650 3450 3650
Wire Wire Line
	3350 3600 3350 3650
Wire Wire Line
	3650 3650 3750 3650
Wire Wire Line
	3750 3650 3750 3700
Wire Wire Line
	2550 3950 2450 3950
Wire Wire Line
	2450 3650 2350 3650
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R39
U 1 1 5ECEAECD
P 3500 4050
F 0 "R39" V 3450 3925 50  0000 C CNN
F 1 "33" V 3450 4150 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 3500 4050 50  0001 C CNN
F 3 "~" H 3500 4050 50  0001 C CNN
F 4 "MCWR04X33R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 33 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 3500 3650 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x33r0ftl/res-33r-1-0-0625w-thick-film/dp/2447161" H 3500 3550 50  0001 C CNN "Distributor Link"
	1    3500 4050
	0    1    1    0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R49
U 1 1 5ECEAED5
P 3750 4150
F 0 "R49" V 3700 4025 50  0000 C CNN
F 1 "33" V 3700 4250 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 3750 4150 50  0001 C CNN
F 3 "~" H 3750 4150 50  0001 C CNN
F 4 "MCWR04X33R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 33 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 3750 3750 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x33r0ftl/res-33r-1-0-0625w-thick-film/dp/2447161" H 3750 3650 50  0001 C CNN "Distributor Link"
	1    3750 4150
	0    1    1    0   
$EndComp
Wire Wire Line
	3350 3650 3350 3950
Connection ~ 3350 3650
Wire Wire Line
	2050 3650 2050 3700
Wire Wire Line
	2150 3650 2050 3650
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5ECEAEE7
P 2250 3650
AR Path="/5E9C6910/5ECEAEE7" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5ECEAEE7" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5ECEAEE7" Ref="C?"  Part="1" 
AR Path="/5EA5DDD6/5ECEAEE7" Ref="C9"  Part="1" 
F 0 "C9" V 2375 3600 50  0000 L CNN
F 1 "100nF" V 2300 3375 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 2250 3650 50  0001 C CNN
F 3 "~" H 2250 3650 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 2250 3250 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 2250 3150 50  0001 C CNN "Distributor Link"
	1    2250 3650
	0    1    -1   0   
$EndComp
Wire Wire Line
	2450 3650 2450 3950
Wire Wire Line
	2450 3650 2450 3600
Connection ~ 2450 3650
Wire Wire Line
	3400 4050 3250 4050
Wire Wire Line
	3250 4150 3650 4150
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R40
U 1 1 5ECEAEF4
P 3500 4250
F 0 "R40" V 3450 4125 50  0000 C CNN
F 1 "33" V 3450 4350 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 3500 4250 50  0001 C CNN
F 3 "~" H 3500 4250 50  0001 C CNN
F 4 "MCWR04X33R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 33 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 3500 3850 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x33r0ftl/res-33r-1-0-0625w-thick-film/dp/2447161" H 3500 3750 50  0001 C CNN "Distributor Link"
	1    3500 4250
	0    1    1    0   
$EndComp
Wire Wire Line
	3400 4250 3250 4250
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R50
U 1 1 5ECEAEFD
P 3750 4350
F 0 "R50" V 3700 4225 50  0000 C CNN
F 1 "33" V 3700 4450 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 3750 4350 50  0001 C CNN
F 3 "~" H 3750 4350 50  0001 C CNN
F 4 "MCWR04X33R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 33 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 3750 3950 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x33r0ftl/res-33r-1-0-0625w-thick-film/dp/2447161" H 3750 3850 50  0001 C CNN "Distributor Link"
	1    3750 4350
	0    1    1    0   
$EndComp
Wire Wire Line
	3250 4350 3650 4350
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R41
U 1 1 5ECEAF06
P 3500 4450
F 0 "R41" V 3450 4325 50  0000 C CNN
F 1 "33" V 3450 4550 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 3500 4450 50  0001 C CNN
F 3 "~" H 3500 4450 50  0001 C CNN
F 4 "MCWR04X33R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 33 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 3500 4050 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x33r0ftl/res-33r-1-0-0625w-thick-film/dp/2447161" H 3500 3950 50  0001 C CNN "Distributor Link"
	1    3500 4450
	0    1    1    0   
$EndComp
Wire Wire Line
	3400 4450 3250 4450
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R51
U 1 1 5ECEAF0F
P 3750 4550
F 0 "R51" V 3700 4425 50  0000 C CNN
F 1 "33" V 3700 4650 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 3750 4550 50  0001 C CNN
F 3 "~" H 3750 4550 50  0001 C CNN
F 4 "MCWR04X33R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 33 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 3750 4150 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x33r0ftl/res-33r-1-0-0625w-thick-film/dp/2447161" H 3750 4050 50  0001 C CNN "Distributor Link"
	1    3750 4550
	0    1    1    0   
$EndComp
Wire Wire Line
	3250 4550 3650 4550
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R42
U 1 1 5ECEAF18
P 3500 4650
F 0 "R42" V 3450 4525 50  0000 C CNN
F 1 "33" V 3450 4750 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 3500 4650 50  0001 C CNN
F 3 "~" H 3500 4650 50  0001 C CNN
F 4 "MCWR04X33R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 33 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 3500 4250 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x33r0ftl/res-33r-1-0-0625w-thick-film/dp/2447161" H 3500 4150 50  0001 C CNN "Distributor Link"
	1    3500 4650
	0    1    1    0   
$EndComp
Wire Wire Line
	3400 4650 3250 4650
Wire Wire Line
	3350 4850 3250 4850
Wire Wire Line
	3350 4850 3350 4950
Wire Wire Line
	4000 4150 3850 4150
Wire Wire Line
	4000 4550 3850 4550
Wire Wire Line
	4000 4350 3850 4350
Wire Wire Line
	3600 4050 4000 4050
Wire Wire Line
	3600 4250 4000 4250
Wire Wire Line
	3600 4450 4000 4450
Wire Wire Line
	3600 4650 4000 4650
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R29
U 1 1 5ECEAF30
P 2300 4050
F 0 "R29" V 2250 3925 50  0000 C CNN
F 1 "33" V 2250 4150 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 2300 4050 50  0001 C CNN
F 3 "~" H 2300 4050 50  0001 C CNN
F 4 "MCWR04X33R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 33 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 2300 3650 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x33r0ftl/res-33r-1-0-0625w-thick-film/dp/2447161" H 2300 3550 50  0001 C CNN "Distributor Link"
	1    2300 4050
	0    1    1    0   
$EndComp
Wire Wire Line
	1800 4050 2200 4050
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R17
U 1 1 5ECEAF39
P 2050 4150
F 0 "R17" V 2000 4025 50  0000 C CNN
F 1 "33" V 2000 4250 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 2050 4150 50  0001 C CNN
F 3 "~" H 2050 4150 50  0001 C CNN
F 4 "MCWR04X33R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 33 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 2050 3750 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x33r0ftl/res-33r-1-0-0625w-thick-film/dp/2447161" H 2050 3650 50  0001 C CNN "Distributor Link"
	1    2050 4150
	0    1    1    0   
$EndComp
Wire Wire Line
	1950 4150 1800 4150
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R30
U 1 1 5ECEAF42
P 2300 4250
F 0 "R30" V 2250 4125 50  0000 C CNN
F 1 "33" V 2250 4350 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 2300 4250 50  0001 C CNN
F 3 "~" H 2300 4250 50  0001 C CNN
F 4 "MCWR04X33R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 33 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 2300 3850 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x33r0ftl/res-33r-1-0-0625w-thick-film/dp/2447161" H 2300 3750 50  0001 C CNN "Distributor Link"
	1    2300 4250
	0    1    1    0   
$EndComp
Wire Wire Line
	1800 4250 2200 4250
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R18
U 1 1 5ECEAF4B
P 2050 4350
F 0 "R18" V 2000 4225 50  0000 C CNN
F 1 "33" V 2000 4450 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 2050 4350 50  0001 C CNN
F 3 "~" H 2050 4350 50  0001 C CNN
F 4 "MCWR04X33R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 33 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 2050 3950 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x33r0ftl/res-33r-1-0-0625w-thick-film/dp/2447161" H 2050 3850 50  0001 C CNN "Distributor Link"
	1    2050 4350
	0    1    1    0   
$EndComp
Wire Wire Line
	1950 4350 1800 4350
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R31
U 1 1 5ECEAF54
P 2300 4450
F 0 "R31" V 2250 4325 50  0000 C CNN
F 1 "33" V 2250 4550 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 2300 4450 50  0001 C CNN
F 3 "~" H 2300 4450 50  0001 C CNN
F 4 "MCWR04X33R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 33 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 2300 4050 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x33r0ftl/res-33r-1-0-0625w-thick-film/dp/2447161" H 2300 3950 50  0001 C CNN "Distributor Link"
	1    2300 4450
	0    1    1    0   
$EndComp
Wire Wire Line
	1800 4450 2200 4450
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R19
U 1 1 5ECEAF5D
P 2050 4550
F 0 "R19" V 2000 4425 50  0000 C CNN
F 1 "33" V 2000 4650 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 2050 4550 50  0001 C CNN
F 3 "~" H 2050 4550 50  0001 C CNN
F 4 "MCWR04X33R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 33 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 2050 4150 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x33r0ftl/res-33r-1-0-0625w-thick-film/dp/2447161" H 2050 4050 50  0001 C CNN "Distributor Link"
	1    2050 4550
	0    1    1    0   
$EndComp
Wire Wire Line
	1950 4550 1800 4550
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R32
U 1 1 5ECEAF66
P 2300 4650
F 0 "R32" V 2250 4525 50  0000 C CNN
F 1 "33" V 2250 4750 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 2300 4650 50  0001 C CNN
F 3 "~" H 2300 4650 50  0001 C CNN
F 4 "MCWR04X33R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 33 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 2300 4250 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x33r0ftl/res-33r-1-0-0625w-thick-film/dp/2447161" H 2300 4150 50  0001 C CNN "Distributor Link"
	1    2300 4650
	0    1    1    0   
$EndComp
Wire Wire Line
	1800 4650 2200 4650
Wire Wire Line
	2550 4050 2400 4050
Wire Wire Line
	2550 4650 2400 4650
Wire Wire Line
	2550 4450 2400 4450
Wire Wire Line
	2550 4250 2400 4250
Wire Wire Line
	2150 4150 2550 4150
Wire Wire Line
	2150 4350 2550 4350
Wire Wire Line
	2150 4550 2550 4550
Text GLabel 2350 4850 0    50   Input ~ 0
TR_VT_ENABLE
Wire Wire Line
	2350 4850 2550 4850
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R20
U 1 1 5ED13779
P 2050 4750
F 0 "R20" V 2000 4625 50  0000 C CNN
F 1 "33" V 2000 4850 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 2050 4750 50  0001 C CNN
F 3 "~" H 2050 4750 50  0001 C CNN
F 4 "MCWR04X33R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 33 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 2050 4350 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x33r0ftl/res-33r-1-0-0625w-thick-film/dp/2447161" H 2050 4250 50  0001 C CNN "Distributor Link"
	1    2050 4750
	0    1    1    0   
$EndComp
Wire Wire Line
	1950 4750 1800 4750
Wire Wire Line
	2150 4750 2550 4750
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R52
U 1 1 5ED1A949
P 3750 4750
F 0 "R52" V 3700 4625 50  0000 C CNN
F 1 "33" V 3700 4850 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 3750 4750 50  0001 C CNN
F 3 "~" H 3750 4750 50  0001 C CNN
F 4 "MCWR04X33R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 33 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 3750 4350 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x33r0ftl/res-33r-1-0-0625w-thick-film/dp/2447161" H 3750 4250 50  0001 C CNN "Distributor Link"
	1    3750 4750
	0    1    1    0   
$EndComp
Wire Wire Line
	3250 4750 3650 4750
Wire Wire Line
	4000 4750 3850 4750
Text GLabel 4000 4150 2    50   Input ~ 0
TR_V_A
Text GLabel 4000 4250 2    50   Input ~ 0
TR_V_B
Text GLabel 4000 4350 2    50   Input ~ 0
TR_V_C
Text GLabel 4000 4450 2    50   Input ~ 0
TR_CUR_A
Text GLabel 4000 4550 2    50   Input ~ 0
TR_CUR_B
Text GLabel 4000 4650 2    50   Input ~ 0
TR_CUR_T
Text GLabel 4000 6175 2    50   Input ~ 0
TR_TEMP
Text GLabel 4000 4050 2    50   Input ~ 0
TR_V_SUP
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_VADJ #PWR?
U 1 1 5ED35A3B
P 6375 3600
AR Path="/5E9C6910/5ED35A3B" Ref="#PWR?"  Part="1" 
AR Path="/5EBC6654/5ED35A3B" Ref="#PWR?"  Part="1" 
AR Path="/5EBC681B/5ED35A3B" Ref="#PWR?"  Part="1" 
AR Path="/5EA5DDD6/5ED35A3B" Ref="#PWR063"  Part="1" 
F 0 "#PWR063" H 6375 3850 50  0001 C CNN
F 1 "CTRL_VADJ" H 6376 3773 50  0000 C CNN
F 2 "" H 6375 3600 50  0001 C CNN
F 3 "" H 6375 3600 50  0001 C CNN
	1    6375 3600
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5ED35A43
P 7475 3650
AR Path="/5E9C6910/5ED35A43" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5ED35A43" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5ED35A43" Ref="C?"  Part="1" 
AR Path="/5EA5DDD6/5ED35A43" Ref="C17"  Part="1" 
F 0 "C17" V 7600 3600 50  0000 L CNN
F 1 "100nF" V 7525 3375 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 7475 3650 50  0001 C CNN
F 3 "~" H 7475 3650 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 7475 3250 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 7475 3150 50  0001 C CNN "Distributor Link"
	1    7475 3650
	0    -1   -1   0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_IC:ADG3308BRUZ U5
U 1 1 5ED35A52
P 6825 4400
F 0 "U5" H 6625 5000 50  0000 C CNN
F 1 "ADG3308BRUZ" H 6825 3800 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:TSSOP-20_4.4x6.5mm_P0.65mm" H 6875 3700 31  0001 C CNN
F 3 "~" H 6425 5050 31  0001 C CNN
F 4 "ADG3308BRUZ -  Voltage Level Translator, Bidirectional, 8 Input, 6 ns, 60 Mbps, 1.15 V to 5.5 V, TSSOP-20" H 6825 3650 31  0001 C CNN "Description"
F 5 "https://uk.farnell.com/analog-devices/adg3308bruz/ic-translator-smd-3308-tssop20/dp/9453083" H 6825 3600 31  0001 C CNN "Distributor Link"
F 6 "https://www.mouser.co.uk/ProductDetail/Analog-Devices/ADG3308BRUZ?qs=sGAEpiMZZMsty6Jaj0%252BBBupSmhSNoeUMOTLXgC%252BMoYY%3D" H 6825 3550 31  0001 C CNN "Distributor Link 2"
	1    6825 4400
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_3V3 #PWR066
U 1 1 5ED35A58
P 7275 3600
F 0 "#PWR066" H 7275 3850 50  0001 C CNN
F 1 "CTRL_3V3" H 7276 3773 50  0000 C CNN
F 2 "" H 7275 3600 50  0001 C CNN
F 3 "" H 7275 3600 50  0001 C CNN
	1    7275 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	7175 3950 7275 3950
Wire Wire Line
	7275 3650 7375 3650
Wire Wire Line
	7275 3600 7275 3650
Wire Wire Line
	7575 3650 7675 3650
Wire Wire Line
	7675 3650 7675 3700
Wire Wire Line
	6475 3950 6375 3950
Wire Wire Line
	6375 3650 6275 3650
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R76
U 1 1 5ED35A67
P 7425 4050
F 0 "R76" V 7375 3925 50  0000 C CNN
F 1 "33" V 7375 4150 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 7425 4050 50  0001 C CNN
F 3 "~" H 7425 4050 50  0001 C CNN
F 4 "MCWR04X33R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 33 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 7425 3650 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x33r0ftl/res-33r-1-0-0625w-thick-film/dp/2447161" H 7425 3550 50  0001 C CNN "Distributor Link"
	1    7425 4050
	0    1    1    0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R83
U 1 1 5ED35A6F
P 7675 4150
F 0 "R83" V 7625 4025 50  0000 C CNN
F 1 "33" V 7625 4250 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 7675 4150 50  0001 C CNN
F 3 "~" H 7675 4150 50  0001 C CNN
F 4 "MCWR04X33R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 33 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 7675 3750 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x33r0ftl/res-33r-1-0-0625w-thick-film/dp/2447161" H 7675 3650 50  0001 C CNN "Distributor Link"
	1    7675 4150
	0    1    1    0   
$EndComp
Wire Wire Line
	7275 3650 7275 3950
Connection ~ 7275 3650
Wire Wire Line
	5975 3650 5975 3700
Wire Wire Line
	6075 3650 5975 3650
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5ED35A81
P 6175 3650
AR Path="/5E9C6910/5ED35A81" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5ED35A81" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5ED35A81" Ref="C?"  Part="1" 
AR Path="/5EA5DDD6/5ED35A81" Ref="C15"  Part="1" 
F 0 "C15" V 6300 3600 50  0000 L CNN
F 1 "100nF" V 6225 3375 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 6175 3650 50  0001 C CNN
F 3 "~" H 6175 3650 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 6175 3250 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 6175 3150 50  0001 C CNN "Distributor Link"
	1    6175 3650
	0    1    -1   0   
$EndComp
Wire Wire Line
	6375 3650 6375 3950
Wire Wire Line
	6375 3650 6375 3600
Connection ~ 6375 3650
Wire Wire Line
	7325 4050 7175 4050
Wire Wire Line
	7175 4150 7575 4150
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R77
U 1 1 5ED35A8E
P 7425 4250
F 0 "R77" V 7375 4125 50  0000 C CNN
F 1 "33" V 7375 4350 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 7425 4250 50  0001 C CNN
F 3 "~" H 7425 4250 50  0001 C CNN
F 4 "MCWR04X33R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 33 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 7425 3850 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x33r0ftl/res-33r-1-0-0625w-thick-film/dp/2447161" H 7425 3750 50  0001 C CNN "Distributor Link"
	1    7425 4250
	0    1    1    0   
$EndComp
Wire Wire Line
	7325 4250 7175 4250
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R84
U 1 1 5ED35A97
P 7675 4350
F 0 "R84" V 7625 4225 50  0000 C CNN
F 1 "33" V 7625 4450 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 7675 4350 50  0001 C CNN
F 3 "~" H 7675 4350 50  0001 C CNN
F 4 "MCWR04X33R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 33 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 7675 3950 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x33r0ftl/res-33r-1-0-0625w-thick-film/dp/2447161" H 7675 3850 50  0001 C CNN "Distributor Link"
	1    7675 4350
	0    1    1    0   
$EndComp
Wire Wire Line
	7175 4350 7575 4350
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R78
U 1 1 5ED35AA0
P 7425 4450
F 0 "R78" V 7375 4325 50  0000 C CNN
F 1 "33" V 7375 4550 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 7425 4450 50  0001 C CNN
F 3 "~" H 7425 4450 50  0001 C CNN
F 4 "MCWR04X33R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 33 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 7425 4050 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x33r0ftl/res-33r-1-0-0625w-thick-film/dp/2447161" H 7425 3950 50  0001 C CNN "Distributor Link"
	1    7425 4450
	0    1    1    0   
$EndComp
Wire Wire Line
	7325 4450 7175 4450
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R85
U 1 1 5ED35AA9
P 7675 4550
F 0 "R85" V 7625 4425 50  0000 C CNN
F 1 "33" V 7625 4650 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 7675 4550 50  0001 C CNN
F 3 "~" H 7675 4550 50  0001 C CNN
F 4 "MCWR04X33R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 33 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 7675 4150 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x33r0ftl/res-33r-1-0-0625w-thick-film/dp/2447161" H 7675 4050 50  0001 C CNN "Distributor Link"
	1    7675 4550
	0    1    1    0   
$EndComp
Wire Wire Line
	7175 4550 7575 4550
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R79
U 1 1 5ED35AB2
P 7425 4650
F 0 "R79" V 7375 4525 50  0000 C CNN
F 1 "33" V 7375 4750 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 7425 4650 50  0001 C CNN
F 3 "~" H 7425 4650 50  0001 C CNN
F 4 "MCWR04X33R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 33 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 7425 4250 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x33r0ftl/res-33r-1-0-0625w-thick-film/dp/2447161" H 7425 4150 50  0001 C CNN "Distributor Link"
	1    7425 4650
	0    1    1    0   
$EndComp
Wire Wire Line
	7325 4650 7175 4650
Wire Wire Line
	7275 4850 7175 4850
Wire Wire Line
	7275 4850 7275 4950
Wire Wire Line
	7925 4150 7775 4150
Wire Wire Line
	7925 4550 7775 4550
Wire Wire Line
	7925 4350 7775 4350
Wire Wire Line
	7525 4050 7925 4050
Wire Wire Line
	7525 4250 7925 4250
Wire Wire Line
	7525 4450 7925 4450
Wire Wire Line
	7525 4650 7925 4650
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R68
U 1 1 5ED35ACA
P 6225 4050
F 0 "R68" V 6175 3925 50  0000 C CNN
F 1 "33" V 6175 4150 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 6225 4050 50  0001 C CNN
F 3 "~" H 6225 4050 50  0001 C CNN
F 4 "MCWR04X33R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 33 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 6225 3650 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x33r0ftl/res-33r-1-0-0625w-thick-film/dp/2447161" H 6225 3550 50  0001 C CNN "Distributor Link"
	1    6225 4050
	0    1    1    0   
$EndComp
Wire Wire Line
	5725 4050 6125 4050
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R60
U 1 1 5ED35AD3
P 5975 4150
F 0 "R60" V 5925 4025 50  0000 C CNN
F 1 "33" V 5925 4250 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 5975 4150 50  0001 C CNN
F 3 "~" H 5975 4150 50  0001 C CNN
F 4 "MCWR04X33R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 33 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 5975 3750 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x33r0ftl/res-33r-1-0-0625w-thick-film/dp/2447161" H 5975 3650 50  0001 C CNN "Distributor Link"
	1    5975 4150
	0    1    1    0   
$EndComp
Wire Wire Line
	5875 4150 5725 4150
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R69
U 1 1 5ED35ADC
P 6225 4250
F 0 "R69" V 6175 4125 50  0000 C CNN
F 1 "33" V 6175 4350 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 6225 4250 50  0001 C CNN
F 3 "~" H 6225 4250 50  0001 C CNN
F 4 "MCWR04X33R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 33 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 6225 3850 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x33r0ftl/res-33r-1-0-0625w-thick-film/dp/2447161" H 6225 3750 50  0001 C CNN "Distributor Link"
	1    6225 4250
	0    1    1    0   
$EndComp
Wire Wire Line
	5725 4250 6125 4250
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R61
U 1 1 5ED35AE5
P 5975 4350
F 0 "R61" V 5925 4225 50  0000 C CNN
F 1 "33" V 5925 4450 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 5975 4350 50  0001 C CNN
F 3 "~" H 5975 4350 50  0001 C CNN
F 4 "MCWR04X33R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 33 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 5975 3950 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x33r0ftl/res-33r-1-0-0625w-thick-film/dp/2447161" H 5975 3850 50  0001 C CNN "Distributor Link"
	1    5975 4350
	0    1    1    0   
$EndComp
Wire Wire Line
	5875 4350 5725 4350
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R70
U 1 1 5ED35AEE
P 6225 4450
F 0 "R70" V 6175 4325 50  0000 C CNN
F 1 "33" V 6175 4550 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 6225 4450 50  0001 C CNN
F 3 "~" H 6225 4450 50  0001 C CNN
F 4 "MCWR04X33R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 33 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 6225 4050 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x33r0ftl/res-33r-1-0-0625w-thick-film/dp/2447161" H 6225 3950 50  0001 C CNN "Distributor Link"
	1    6225 4450
	0    1    1    0   
$EndComp
Wire Wire Line
	5725 4450 6125 4450
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R62
U 1 1 5ED35AF7
P 5975 4550
F 0 "R62" V 5925 4425 50  0000 C CNN
F 1 "33" V 5925 4650 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 5975 4550 50  0001 C CNN
F 3 "~" H 5975 4550 50  0001 C CNN
F 4 "MCWR04X33R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 33 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 5975 4150 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x33r0ftl/res-33r-1-0-0625w-thick-film/dp/2447161" H 5975 4050 50  0001 C CNN "Distributor Link"
	1    5975 4550
	0    1    1    0   
$EndComp
Wire Wire Line
	5875 4550 5725 4550
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R71
U 1 1 5ED35B00
P 6225 4650
F 0 "R71" V 6175 4525 50  0000 C CNN
F 1 "33" V 6175 4750 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 6225 4650 50  0001 C CNN
F 3 "~" H 6225 4650 50  0001 C CNN
F 4 "MCWR04X33R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 33 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 6225 4250 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x33r0ftl/res-33r-1-0-0625w-thick-film/dp/2447161" H 6225 4150 50  0001 C CNN "Distributor Link"
	1    6225 4650
	0    1    1    0   
$EndComp
Wire Wire Line
	5725 4650 6125 4650
Wire Wire Line
	6475 4050 6325 4050
Wire Wire Line
	6475 4650 6325 4650
Wire Wire Line
	6475 4450 6325 4450
Wire Wire Line
	6475 4250 6325 4250
Wire Wire Line
	6075 4150 6475 4150
Wire Wire Line
	6075 4350 6475 4350
Wire Wire Line
	6075 4550 6475 4550
Text GLabel 6275 4850 0    50   Input ~ 0
TR_VT_ENABLE
Wire Wire Line
	6275 4850 6475 4850
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R63
U 1 1 5ED35B12
P 5975 4750
F 0 "R63" V 5925 4625 50  0000 C CNN
F 1 "33" V 5925 4850 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 5975 4750 50  0001 C CNN
F 3 "~" H 5975 4750 50  0001 C CNN
F 4 "MCWR04X33R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 33 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 5975 4350 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x33r0ftl/res-33r-1-0-0625w-thick-film/dp/2447161" H 5975 4250 50  0001 C CNN "Distributor Link"
	1    5975 4750
	0    1    1    0   
$EndComp
Wire Wire Line
	5875 4750 5725 4750
Wire Wire Line
	6075 4750 6475 4750
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R86
U 1 1 5ED35B1C
P 7675 4750
F 0 "R86" V 7625 4625 50  0000 C CNN
F 1 "33" V 7625 4850 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 7675 4750 50  0001 C CNN
F 3 "~" H 7675 4750 50  0001 C CNN
F 4 "MCWR04X33R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 33 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 7675 4350 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x33r0ftl/res-33r-1-0-0625w-thick-film/dp/2447161" H 7675 4250 50  0001 C CNN "Distributor Link"
	1    7675 4750
	0    1    1    0   
$EndComp
Wire Wire Line
	7175 4750 7575 4750
Wire Wire Line
	7925 4750 7775 4750
Text GLabel 7925 4150 2    50   Input ~ 0
TR_ENC_I
Text GLabel 7925 4250 2    50   Input ~ 0
TR_RES_A
Text GLabel 7925 4350 2    50   Input ~ 0
TR_RES_B
Text GLabel 7925 4550 2    50   Input ~ 0
TR_HAL_A
Text GLabel 7925 4650 2    50   Input ~ 0
TR_HAL_B
Text GLabel 7925 4450 2    50   Input ~ 0
TR_RES_I
Text GLabel 7925 4050 2    50   Input ~ 0
TR_ENC_B
Text GLabel 4000 4750 2    50   Input ~ 0
TR_ENC_A
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_VADJ #PWR?
U 1 1 5ED98971
P 2450 5725
AR Path="/5E9C6910/5ED98971" Ref="#PWR?"  Part="1" 
AR Path="/5EBC6654/5ED98971" Ref="#PWR?"  Part="1" 
AR Path="/5EBC681B/5ED98971" Ref="#PWR?"  Part="1" 
AR Path="/5EA5DDD6/5ED98971" Ref="#PWR049"  Part="1" 
F 0 "#PWR049" H 2450 5975 50  0001 C CNN
F 1 "CTRL_VADJ" H 2451 5898 50  0000 C CNN
F 2 "" H 2450 5725 50  0001 C CNN
F 3 "" H 2450 5725 50  0001 C CNN
	1    2450 5725
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5ED98979
P 3550 5775
AR Path="/5E9C6910/5ED98979" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5ED98979" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5ED98979" Ref="C?"  Part="1" 
AR Path="/5EA5DDD6/5ED98979" Ref="C13"  Part="1" 
F 0 "C13" V 3675 5725 50  0000 L CNN
F 1 "100nF" V 3600 5500 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 3550 5775 50  0001 C CNN
F 3 "~" H 3550 5775 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 3550 5375 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 3550 5275 50  0001 C CNN "Distributor Link"
	1    3550 5775
	0    -1   -1   0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_IC:ADG3308BRUZ U3
U 1 1 5ED98988
P 2900 6525
F 0 "U3" H 2700 7125 50  0000 C CNN
F 1 "ADG3308BRUZ" H 2900 5925 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:TSSOP-20_4.4x6.5mm_P0.65mm" H 2950 5825 31  0001 C CNN
F 3 "~" H 2500 7175 31  0001 C CNN
F 4 "ADG3308BRUZ -  Voltage Level Translator, Bidirectional, 8 Input, 6 ns, 60 Mbps, 1.15 V to 5.5 V, TSSOP-20" H 2900 5775 31  0001 C CNN "Description"
F 5 "https://uk.farnell.com/analog-devices/adg3308bruz/ic-translator-smd-3308-tssop20/dp/9453083" H 2900 5725 31  0001 C CNN "Distributor Link"
F 6 "https://www.mouser.co.uk/ProductDetail/Analog-Devices/ADG3308BRUZ?qs=sGAEpiMZZMsty6Jaj0%252BBBupSmhSNoeUMOTLXgC%252BMoYY%3D" H 2900 5675 31  0001 C CNN "Distributor Link 2"
	1    2900 6525
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_3V3 #PWR054
U 1 1 5ED9898E
P 3350 5725
F 0 "#PWR054" H 3350 5975 50  0001 C CNN
F 1 "CTRL_3V3" H 3351 5898 50  0000 C CNN
F 2 "" H 3350 5725 50  0001 C CNN
F 3 "" H 3350 5725 50  0001 C CNN
	1    3350 5725
	1    0    0    -1  
$EndComp
Wire Wire Line
	3250 6075 3350 6075
Wire Wire Line
	3350 5775 3450 5775
Wire Wire Line
	3350 5725 3350 5775
Wire Wire Line
	3650 5775 3750 5775
Wire Wire Line
	3750 5775 3750 5825
Wire Wire Line
	2550 6075 2450 6075
Wire Wire Line
	2450 5775 2350 5775
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R43
U 1 1 5ED9899D
P 3500 6175
F 0 "R43" V 3450 6050 50  0000 C CNN
F 1 "33" V 3450 6275 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 3500 6175 50  0001 C CNN
F 3 "~" H 3500 6175 50  0001 C CNN
F 4 "MCWR04X33R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 33 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 3500 5775 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x33r0ftl/res-33r-1-0-0625w-thick-film/dp/2447161" H 3500 5675 50  0001 C CNN "Distributor Link"
	1    3500 6175
	0    1    1    0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R53
U 1 1 5ED989A5
P 3750 6275
F 0 "R53" V 3700 6150 50  0000 C CNN
F 1 "33" V 3700 6375 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 3750 6275 50  0001 C CNN
F 3 "~" H 3750 6275 50  0001 C CNN
F 4 "MCWR04X33R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 33 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 3750 5875 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x33r0ftl/res-33r-1-0-0625w-thick-film/dp/2447161" H 3750 5775 50  0001 C CNN "Distributor Link"
	1    3750 6275
	0    1    1    0   
$EndComp
Wire Wire Line
	3350 5775 3350 6075
Connection ~ 3350 5775
Wire Wire Line
	2050 5775 2050 5825
Wire Wire Line
	2150 5775 2050 5775
$Comp
L PMSM_Driver_LV_Board_Device:C_Small C?
U 1 1 5ED989B7
P 2250 5775
AR Path="/5E9C6910/5ED989B7" Ref="C?"  Part="1" 
AR Path="/5EBC6654/5ED989B7" Ref="C?"  Part="1" 
AR Path="/5EBC681B/5ED989B7" Ref="C?"  Part="1" 
AR Path="/5EA5DDD6/5ED989B7" Ref="C10"  Part="1" 
F 0 "C10" V 2375 5725 50  0000 L CNN
F 1 "100nF" V 2300 5500 50  0000 L CNN
F 2 "PMSM_Driver_LV_Board:C_0402_1005Metric" H 2250 5775 50  0001 C CNN
F 3 "~" H 2250 5775 50  0001 C CNN
F 4 "MC0402X104K6R3CT -  SMD Multilayer Ceramic Capacitor, 0.1 µF, 6.3 V, 0402 [1005 Metric], ± 10%, X5R, MC Series" H 2250 5375 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mc0402x104k6r3ct/cap-0-1-f-6-3v-10-x5r-0402/dp/2320773" H 2250 5275 50  0001 C CNN "Distributor Link"
	1    2250 5775
	0    1    -1   0   
$EndComp
Wire Wire Line
	2450 5775 2450 6075
Wire Wire Line
	2450 5775 2450 5725
Connection ~ 2450 5775
Wire Wire Line
	3400 6175 3250 6175
Wire Wire Line
	3250 6275 3650 6275
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R44
U 1 1 5ED989C4
P 3500 6375
F 0 "R44" V 3450 6250 50  0000 C CNN
F 1 "33" V 3450 6475 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 3500 6375 50  0001 C CNN
F 3 "~" H 3500 6375 50  0001 C CNN
F 4 "MCWR04X33R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 33 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 3500 5975 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x33r0ftl/res-33r-1-0-0625w-thick-film/dp/2447161" H 3500 5875 50  0001 C CNN "Distributor Link"
	1    3500 6375
	0    1    1    0   
$EndComp
Wire Wire Line
	3400 6375 3250 6375
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R54
U 1 1 5ED989CD
P 3750 6475
F 0 "R54" V 3700 6350 50  0000 C CNN
F 1 "33" V 3700 6575 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 3750 6475 50  0001 C CNN
F 3 "~" H 3750 6475 50  0001 C CNN
F 4 "MCWR04X33R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 33 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 3750 6075 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x33r0ftl/res-33r-1-0-0625w-thick-film/dp/2447161" H 3750 5975 50  0001 C CNN "Distributor Link"
	1    3750 6475
	0    1    1    0   
$EndComp
Wire Wire Line
	3250 6475 3650 6475
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R45
U 1 1 5ED989D6
P 3500 6575
F 0 "R45" V 3450 6450 50  0000 C CNN
F 1 "33" V 3450 6675 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 3500 6575 50  0001 C CNN
F 3 "~" H 3500 6575 50  0001 C CNN
F 4 "MCWR04X33R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 33 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 3500 6175 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x33r0ftl/res-33r-1-0-0625w-thick-film/dp/2447161" H 3500 6075 50  0001 C CNN "Distributor Link"
	1    3500 6575
	0    1    1    0   
$EndComp
Wire Wire Line
	3400 6575 3250 6575
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R55
U 1 1 5ED989DF
P 3750 6675
F 0 "R55" V 3700 6550 50  0000 C CNN
F 1 "33" V 3700 6775 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 3750 6675 50  0001 C CNN
F 3 "~" H 3750 6675 50  0001 C CNN
F 4 "MCWR04X33R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 33 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 3750 6275 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x33r0ftl/res-33r-1-0-0625w-thick-film/dp/2447161" H 3750 6175 50  0001 C CNN "Distributor Link"
	1    3750 6675
	0    1    1    0   
$EndComp
Wire Wire Line
	3250 6675 3650 6675
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R46
U 1 1 5ED989E8
P 3500 6775
F 0 "R46" V 3450 6650 50  0000 C CNN
F 1 "33" V 3450 6875 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 3500 6775 50  0001 C CNN
F 3 "~" H 3500 6775 50  0001 C CNN
F 4 "MCWR04X33R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 33 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 3500 6375 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x33r0ftl/res-33r-1-0-0625w-thick-film/dp/2447161" H 3500 6275 50  0001 C CNN "Distributor Link"
	1    3500 6775
	0    1    1    0   
$EndComp
Wire Wire Line
	3400 6775 3250 6775
Wire Wire Line
	3350 6975 3250 6975
Wire Wire Line
	3350 6975 3350 7075
Wire Wire Line
	4000 6275 3850 6275
Wire Wire Line
	4000 6675 3850 6675
Wire Wire Line
	4000 6475 3850 6475
Wire Wire Line
	3600 6175 4000 6175
Wire Wire Line
	3600 6375 4000 6375
Wire Wire Line
	3600 6575 4000 6575
Wire Wire Line
	3600 6775 4000 6775
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R33
U 1 1 5ED98A00
P 2300 6175
F 0 "R33" V 2250 6050 50  0000 C CNN
F 1 "33" V 2250 6275 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 2300 6175 50  0001 C CNN
F 3 "~" H 2300 6175 50  0001 C CNN
F 4 "MCWR04X33R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 33 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 2300 5775 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x33r0ftl/res-33r-1-0-0625w-thick-film/dp/2447161" H 2300 5675 50  0001 C CNN "Distributor Link"
	1    2300 6175
	0    1    1    0   
$EndComp
Wire Wire Line
	1800 6175 2200 6175
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R21
U 1 1 5ED98A09
P 2050 6275
F 0 "R21" V 2000 6150 50  0000 C CNN
F 1 "33" V 2000 6375 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 2050 6275 50  0001 C CNN
F 3 "~" H 2050 6275 50  0001 C CNN
F 4 "MCWR04X33R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 33 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 2050 5875 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x33r0ftl/res-33r-1-0-0625w-thick-film/dp/2447161" H 2050 5775 50  0001 C CNN "Distributor Link"
	1    2050 6275
	0    1    1    0   
$EndComp
Wire Wire Line
	1950 6275 1800 6275
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R34
U 1 1 5ED98A12
P 2300 6375
F 0 "R34" V 2250 6250 50  0000 C CNN
F 1 "33" V 2250 6475 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 2300 6375 50  0001 C CNN
F 3 "~" H 2300 6375 50  0001 C CNN
F 4 "MCWR04X33R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 33 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 2300 5975 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x33r0ftl/res-33r-1-0-0625w-thick-film/dp/2447161" H 2300 5875 50  0001 C CNN "Distributor Link"
	1    2300 6375
	0    1    1    0   
$EndComp
Wire Wire Line
	1800 6375 2200 6375
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R22
U 1 1 5ED98A1B
P 2050 6475
F 0 "R22" V 2000 6350 50  0000 C CNN
F 1 "33" V 2000 6575 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 2050 6475 50  0001 C CNN
F 3 "~" H 2050 6475 50  0001 C CNN
F 4 "MCWR04X33R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 33 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 2050 6075 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x33r0ftl/res-33r-1-0-0625w-thick-film/dp/2447161" H 2050 5975 50  0001 C CNN "Distributor Link"
	1    2050 6475
	0    1    1    0   
$EndComp
Wire Wire Line
	1950 6475 1800 6475
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R35
U 1 1 5ED98A24
P 2300 6575
F 0 "R35" V 2250 6450 50  0000 C CNN
F 1 "33" V 2250 6675 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 2300 6575 50  0001 C CNN
F 3 "~" H 2300 6575 50  0001 C CNN
F 4 "MCWR04X33R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 33 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 2300 6175 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x33r0ftl/res-33r-1-0-0625w-thick-film/dp/2447161" H 2300 6075 50  0001 C CNN "Distributor Link"
	1    2300 6575
	0    1    1    0   
$EndComp
Wire Wire Line
	1800 6575 2200 6575
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R23
U 1 1 5ED98A2D
P 2050 6675
F 0 "R23" V 2000 6550 50  0000 C CNN
F 1 "33" V 2000 6775 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 2050 6675 50  0001 C CNN
F 3 "~" H 2050 6675 50  0001 C CNN
F 4 "MCWR04X33R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 33 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 2050 6275 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x33r0ftl/res-33r-1-0-0625w-thick-film/dp/2447161" H 2050 6175 50  0001 C CNN "Distributor Link"
	1    2050 6675
	0    1    1    0   
$EndComp
Wire Wire Line
	1950 6675 1800 6675
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R36
U 1 1 5ED98A36
P 2300 6775
F 0 "R36" V 2250 6650 50  0000 C CNN
F 1 "33" V 2250 6875 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 2300 6775 50  0001 C CNN
F 3 "~" H 2300 6775 50  0001 C CNN
F 4 "MCWR04X33R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 33 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 2300 6375 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x33r0ftl/res-33r-1-0-0625w-thick-film/dp/2447161" H 2300 6275 50  0001 C CNN "Distributor Link"
	1    2300 6775
	0    1    1    0   
$EndComp
Wire Wire Line
	1800 6775 2200 6775
Wire Wire Line
	2550 6175 2400 6175
Wire Wire Line
	2550 6775 2400 6775
Wire Wire Line
	2550 6575 2400 6575
Wire Wire Line
	2550 6375 2400 6375
Wire Wire Line
	2150 6275 2550 6275
Wire Wire Line
	2150 6475 2550 6475
Wire Wire Line
	2150 6675 2550 6675
Text GLabel 2350 6975 0    50   Input ~ 0
TR_VT_ENABLE
Wire Wire Line
	2350 6975 2550 6975
Text GLabel 1800 6675 0    50   Output ~ 0
CTRL_SPI_MISO
Text GLabel 1800 6775 0    50   Input ~ 0
CTRL_SPI_MOSI
Text GLabel 1800 6575 0    50   Input ~ 0
CTRL_SPI_SCK
Text GLabel 1800 6275 0    50   Input ~ 0
CTRL_RDC_SAMPLE_N
Text GLabel 5725 4750 0    50   Output ~ 0
CTRL_HAL_C
NoConn ~ 3250 6875
Wire Wire Line
	1625 7075 1625 6875
Text Notes 750  875  0    100  ~ 0
Voltage Translation
Text GLabel 4000 6475 2    50   Output ~ 0
TR_SPI_SEL_AUX
Text GLabel 4000 6675 2    50   Input ~ 0
TR_SPI_MISO
Text GLabel 4000 6775 2    50   Output ~ 0
TR_SPI_MOSI
Text GLabel 4000 6575 2    50   Output ~ 0
TR_SPI_SCK
Text GLabel 4000 6275 2    50   Output ~ 0
TR_RDC_SAMPLE_N
Text GLabel 4000 6375 2    50   Output ~ 0
TR_SPI_SEL_RDC
Text GLabel 7925 4750 2    50   Input ~ 0
TR_HAL_C
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_DGND #PWR042
U 1 1 5EE30ACB
P 1425 2750
F 0 "#PWR042" H 1425 2500 50  0001 C CNN
F 1 "CTRL_DGND" H 1430 2577 50  0000 C CNN
F 2 "" H 1425 2750 50  0001 C CNN
F 3 "" H 1425 2750 50  0001 C CNN
	1    1425 2750
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_DGND #PWR051
U 1 1 5EE443E1
P 3350 2750
F 0 "#PWR051" H 3350 2500 50  0001 C CNN
F 1 "CTRL_DGND" H 3355 2577 50  0000 C CNN
F 2 "" H 3350 2750 50  0001 C CNN
F 3 "" H 3350 2750 50  0001 C CNN
	1    3350 2750
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_DGND #PWR056
U 1 1 5EE57D7B
P 3750 1500
F 0 "#PWR056" H 3750 1250 50  0001 C CNN
F 1 "CTRL_DGND" H 3755 1327 50  0000 C CNN
F 2 "" H 3750 1500 50  0001 C CNN
F 3 "" H 3750 1500 50  0001 C CNN
	1    3750 1500
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_DGND #PWR044
U 1 1 5EE6B6FC
P 2050 1500
F 0 "#PWR044" H 2050 1250 50  0001 C CNN
F 1 "CTRL_DGND" H 2055 1327 50  0000 C CNN
F 2 "" H 2050 1500 50  0001 C CNN
F 3 "" H 2050 1500 50  0001 C CNN
	1    2050 1500
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_DGND #PWR057
U 1 1 5EEA5CB4
P 3750 3700
F 0 "#PWR057" H 3750 3450 50  0001 C CNN
F 1 "CTRL_DGND" H 3755 3527 50  0000 C CNN
F 2 "" H 3750 3700 50  0001 C CNN
F 3 "" H 3750 3700 50  0001 C CNN
	1    3750 3700
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_DGND #PWR045
U 1 1 5EEB95D4
P 2050 3700
F 0 "#PWR045" H 2050 3450 50  0001 C CNN
F 1 "CTRL_DGND" H 2055 3527 50  0000 C CNN
F 2 "" H 2050 3700 50  0001 C CNN
F 3 "" H 2050 3700 50  0001 C CNN
	1    2050 3700
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_DGND #PWR053
U 1 1 5EEE0455
P 3350 4950
F 0 "#PWR053" H 3350 4700 50  0001 C CNN
F 1 "CTRL_DGND" H 3355 4777 50  0000 C CNN
F 2 "" H 3350 4950 50  0001 C CNN
F 3 "" H 3350 4950 50  0001 C CNN
	1    3350 4950
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_DGND #PWR058
U 1 1 5EF41869
P 3750 5825
F 0 "#PWR058" H 3750 5575 50  0001 C CNN
F 1 "CTRL_DGND" H 3755 5652 50  0000 C CNN
F 2 "" H 3750 5825 50  0001 C CNN
F 3 "" H 3750 5825 50  0001 C CNN
	1    3750 5825
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_DGND #PWR046
U 1 1 5EF55201
P 2050 5825
F 0 "#PWR046" H 2050 5575 50  0001 C CNN
F 1 "CTRL_DGND" H 2055 5652 50  0000 C CNN
F 2 "" H 2050 5825 50  0001 C CNN
F 3 "" H 2050 5825 50  0001 C CNN
	1    2050 5825
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_DGND #PWR043
U 1 1 5EF68C18
P 1625 7075
F 0 "#PWR043" H 1625 6825 50  0001 C CNN
F 1 "CTRL_DGND" H 1630 6902 50  0000 C CNN
F 2 "" H 1625 7075 50  0001 C CNN
F 3 "" H 1625 7075 50  0001 C CNN
	1    1625 7075
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_DGND #PWR055
U 1 1 5EF7C486
P 3350 7075
F 0 "#PWR055" H 3350 6825 50  0001 C CNN
F 1 "CTRL_DGND" H 3355 6902 50  0000 C CNN
F 2 "" H 3350 7075 50  0001 C CNN
F 3 "" H 3350 7075 50  0001 C CNN
	1    3350 7075
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_DGND #PWR060
U 1 1 5EFDDACC
P 5975 1500
F 0 "#PWR060" H 5975 1250 50  0001 C CNN
F 1 "CTRL_DGND" H 5980 1327 50  0000 C CNN
F 2 "" H 5975 1500 50  0001 C CNN
F 3 "" H 5975 1500 50  0001 C CNN
	1    5975 1500
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_DGND #PWR068
U 1 1 5EFF1539
P 7675 1500
F 0 "#PWR068" H 7675 1250 50  0001 C CNN
F 1 "CTRL_DGND" H 7680 1327 50  0000 C CNN
F 2 "" H 7675 1500 50  0001 C CNN
F 3 "" H 7675 1500 50  0001 C CNN
	1    7675 1500
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_DGND #PWR059
U 1 1 5F004F1B
P 5550 2750
F 0 "#PWR059" H 5550 2500 50  0001 C CNN
F 1 "CTRL_DGND" H 5555 2577 50  0000 C CNN
F 2 "" H 5550 2750 50  0001 C CNN
F 3 "" H 5550 2750 50  0001 C CNN
	1    5550 2750
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_DGND #PWR065
U 1 1 5F018836
P 7275 2750
F 0 "#PWR065" H 7275 2500 50  0001 C CNN
F 1 "CTRL_DGND" H 7280 2577 50  0000 C CNN
F 2 "" H 7275 2750 50  0001 C CNN
F 3 "" H 7275 2750 50  0001 C CNN
	1    7275 2750
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_DGND #PWR061
U 1 1 5F0666C4
P 5975 3700
F 0 "#PWR061" H 5975 3450 50  0001 C CNN
F 1 "CTRL_DGND" H 5980 3527 50  0000 C CNN
F 2 "" H 5975 3700 50  0001 C CNN
F 3 "" H 5975 3700 50  0001 C CNN
	1    5975 3700
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_DGND #PWR069
U 1 1 5F079FDD
P 7675 3700
F 0 "#PWR069" H 7675 3450 50  0001 C CNN
F 1 "CTRL_DGND" H 7680 3527 50  0000 C CNN
F 2 "" H 7675 3700 50  0001 C CNN
F 3 "" H 7675 3700 50  0001 C CNN
	1    7675 3700
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Power:CTRL_DGND #PWR067
U 1 1 5F08D90C
P 7275 4950
F 0 "#PWR067" H 7275 4700 50  0001 C CNN
F 1 "CTRL_DGND" H 7280 4777 50  0000 C CNN
F 2 "" H 7275 4950 50  0001 C CNN
F 3 "" H 7275 4950 50  0001 C CNN
	1    7275 4950
	1    0    0    -1  
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R27
U 1 1 5F2F1489
P 2300 2250
F 0 "R27" V 2250 2125 50  0000 C CNN
F 1 "33" V 2250 2350 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 2300 2250 50  0001 C CNN
F 3 "~" H 2300 2250 50  0001 C CNN
F 4 "MCWR04X33R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 33 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 2300 1850 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x33r0ftl/res-33r-1-0-0625w-thick-film/dp/2447161" H 2300 1750 50  0001 C CNN "Distributor Link"
	1    2300 2250
	0    1    1    0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R15
U 1 1 5F2F1492
P 2050 2350
F 0 "R15" V 2000 2225 50  0000 C CNN
F 1 "33" V 2000 2475 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 2050 2350 50  0001 C CNN
F 3 "~" H 2050 2350 50  0001 C CNN
F 4 "MCWR04X33R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 33 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 2050 1950 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x33r0ftl/res-33r-1-0-0625w-thick-film/dp/2447161" H 2050 1850 50  0001 C CNN "Distributor Link"
	1    2050 2350
	0    1    1    0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R28
U 1 1 5F2F149B
P 2300 2450
F 0 "R28" V 2250 2325 50  0000 C CNN
F 1 "33" V 2250 2550 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 2300 2450 50  0001 C CNN
F 3 "~" H 2300 2450 50  0001 C CNN
F 4 "MCWR04X33R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 33 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 2300 2050 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x33r0ftl/res-33r-1-0-0625w-thick-film/dp/2447161" H 2300 1950 50  0001 C CNN "Distributor Link"
	1    2300 2450
	0    1    1    0   
$EndComp
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R16
U 1 1 5F2F14A4
P 2050 2550
F 0 "R16" V 2000 2425 50  0000 C CNN
F 1 "33" V 2000 2650 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 2050 2550 50  0001 C CNN
F 3 "~" H 2050 2550 50  0001 C CNN
F 4 "MCWR04X33R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 33 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 2050 2150 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x33r0ftl/res-33r-1-0-0625w-thick-film/dp/2447161" H 2050 2050 50  0001 C CNN "Distributor Link"
	1    2050 2550
	0    1    1    0   
$EndComp
Wire Wire Line
	2550 2250 2400 2250
Wire Wire Line
	2550 2450 2400 2450
Wire Wire Line
	2150 2350 2550 2350
Wire Wire Line
	2150 2550 2550 2550
Wire Wire Line
	1425 2750 1425 2550
Wire Wire Line
	1425 2550 1950 2550
Wire Wire Line
	1425 2550 1425 2450
Wire Wire Line
	1425 2450 2200 2450
Connection ~ 1425 2550
Wire Wire Line
	1425 2450 1425 2350
Wire Wire Line
	1425 2350 1950 2350
Connection ~ 1425 2450
Wire Wire Line
	1425 2350 1425 2250
Wire Wire Line
	1425 2250 2200 2250
Connection ~ 1425 2350
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R59
U 1 1 5F3AE843
P 5975 2550
F 0 "R59" V 5925 2425 50  0000 C CNN
F 1 "33" V 5925 2650 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 5975 2550 50  0001 C CNN
F 3 "~" H 5975 2550 50  0001 C CNN
F 4 "MCWR04X33R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 33 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 5975 2150 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x33r0ftl/res-33r-1-0-0625w-thick-film/dp/2447161" H 5975 2050 50  0001 C CNN "Distributor Link"
	1    5975 2550
	0    1    1    0   
$EndComp
Wire Wire Line
	6075 2550 6475 2550
Wire Wire Line
	5550 2550 5550 2750
Wire Wire Line
	5550 2550 5875 2550
$Comp
L PMSM_Driver_LV_Board_Device:R_Small_US R24
U 1 1 5F403164
P 2050 6875
F 0 "R24" V 2000 6750 50  0000 C CNN
F 1 "33" V 2000 6975 50  0000 C CNN
F 2 "PMSM_Driver_LV_Board:R_0402_1005Metric" H 2050 6875 50  0001 C CNN
F 3 "~" H 2050 6875 50  0001 C CNN
F 4 "MCWR04X33R0FTL -  SMD Chip Resistor, 0402 [1005 Metric], 33 ohm, MCWR Series, 50 V, Thick Film, 62.5 mW" H 2050 6475 50  0001 C CNN "Description"
F 5 "https://uk.farnell.com/multicomp/mcwr04x33r0ftl/res-33r-1-0-0625w-thick-film/dp/2447161" H 2050 6375 50  0001 C CNN "Distributor Link"
	1    2050 6875
	0    1    1    0   
$EndComp
Wire Wire Line
	2150 6875 2550 6875
Wire Wire Line
	1625 6875 1950 6875
Text GLabel 1800 6475 0    50   Input ~ 0
CTRL_SPI_SEL_AUX
Text GLabel 1800 6375 0    50   Input ~ 0
CTRL_SPI_SEL_RDC
$EndSCHEMATC
