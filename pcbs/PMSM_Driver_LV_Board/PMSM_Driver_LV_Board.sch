EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 16
Title "PMSM_Driver_LV_Board"
Date "2020-03-30"
Rev "A"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 1050 1125 0    50   ~ 0
Page 2
Text Notes 3550 1125 0    50   ~ 0
Page 3
Text Notes 8550 1125 0    50   ~ 0
Page 5
Text Notes 6050 1125 0    50   ~ 0
Page 4
Text Notes 1050 5625 0    50   ~ 0
Page 14
Text Notes 8550 4125 0    50   ~ 0
Page 13
Text Notes 6050 4125 0    50   ~ 0
Page 12
Text Notes 3550 4125 0    50   ~ 0
Page 11
Text Notes 8550 2625 0    50   ~ 0
Page 9
Text Notes 6050 2625 0    50   ~ 0
Page 8
Text Notes 3550 2625 0    50   ~ 0
Page 7
Text Notes 1050 2625 0    50   ~ 0
Page 6
Text Notes 1050 4125 0    50   ~ 0
Page 10
Text Notes 3550 5625 0    50   ~ 0
Page 15
Text Notes 6050 5625 0    50   ~ 0
Page 16
$Sheet
S 1000 1000 2000 1000
U 5EA5D9E8
F0 "Connectors" 50
F1 "schematics/PMSM_Driver_LV_Board_Connectors.sch" 50
$EndSheet
$Sheet
S 3500 1000 2000 1000
U 5EA5DDD6
F0 "Voltage_Translation" 50
F1 "schematics/PMSM_Driver_LV_Board_VoltTran.sch" 50
$EndSheet
$Sheet
S 6000 1000 2000 1000
U 5EA5DEB5
F0 "Digital_Isolators" 50
F1 "schematics/PMSM_Driver_LV_Board_DigIso.sch" 50
$EndSheet
$Sheet
S 8500 1000 2000 1000
U 5EA5DF22
F0 "Encoder" 50
F1 "schematics/PMSM_Driver_LV_Board_Encoder.sch" 50
$EndSheet
$Sheet
S 1000 2500 2000 1000
U 5EA5DF8B
F0 "Hall_Sensor" 50
F1 "schematics/PMSM_Driver_LV_Board_Hall.sch" 50
$EndSheet
$Sheet
S 3500 2500 2000 1000
U 5EA5DFF7
F0 "Resolver" 50
F1 "schematics/PMSM_Driver_LV_Board_Resolver.sch" 50
$EndSheet
$Sheet
S 6000 2500 2000 1000
U 5EA5E06A
F0 "Power_Stage" 50
F1 "schematics/PMSM_Driver_LV_Board_Power_Stage.sch" 50
$EndSheet
$Sheet
S 8500 2500 2000 1000
U 5EA5E0CA
F0 "Charge_Pumps" 50
F1 "schematics/PMSM_Driver_LV_Board_Charge_Pumps.sch" 50
$EndSheet
$Sheet
S 1000 4000 2000 1000
U 5EA5E155
F0 "Current_Measurements" 50
F1 "schematics/PMSM_Driver_LV_Board_CurrMeas.sch" 50
$EndSheet
$Sheet
S 3500 4000 2000 1000
U 5EA5E1C2
F0 "Voltage_Measurements" 50
F1 "schematics/PMSM_Driver_LV_Board_VoltMeas.sch" 50
$EndSheet
$Sheet
S 6000 4000 2000 1000
U 5EA5E295
F0 "Overcurrent_Protection" 50
F1 "schematics/PMSM_Driver_LV_Board_OvercurrProt.sch" 50
$EndSheet
$Sheet
S 8500 4000 2000 1000
U 5EA5E358
F0 "Thermal_Protection" 50
F1 "schematics/PMSM_Driver_LV_Board_ThermProt.sch" 50
$EndSheet
$Sheet
S 1000 5500 2000 1000
U 5ED47D19
F0 "Power_Supplies_1" 50
F1 "schematics/PMSM_Driver_LV_Board_PowerSup1.sch" 50
$EndSheet
$Sheet
S 3500 5500 2000 1000
U 5FE92F3B
F0 "Power_Supplies_2" 50
F1 "schematics/PMSM_Driver_LV_Board_PowerSup2.sch" 50
$EndSheet
$Sheet
S 6000 5500 2000 925 
U 61A61EC8
F0 "Power_Good_LEDs" 50
F1 "schematics/PMSM_LV_Board_PowerGoodLED.sch" 50
$EndSheet
$EndSCHEMATC
